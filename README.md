# iOS Building

### Install all npm dependencies

`npm install`

### Install ios dependencies with CocoaPods

`cd ios && npx pod-install`

### Install ReactNativeWebRTC bitcode

Find this script and run it terminal from your project root directory

`/node_modules/react-native-webrtc/tools/downloadBitcode.sh`

### Finally open the xcode project from ios folder and start building
