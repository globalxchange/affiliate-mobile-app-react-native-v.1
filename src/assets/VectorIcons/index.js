import CloseIcon from './CloseIcon';
import ExpandIcon from './ExpandIcon';
import OTCBotsIcon from './OTCBotsIcon';
import CollapseIcon from './CollapseIcon';

export default {
  CloseIcon,
  ExpandIcon,
  OTCBotsIcon,
  CollapseIcon,
};
