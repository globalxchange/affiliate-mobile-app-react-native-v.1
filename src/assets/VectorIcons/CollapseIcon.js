import React from 'react';
import {SvgXml} from 'react-native-svg';
import ThemeData from '../../configs/ThemeData';

const CollapseIcon = ({
  color = ThemeData.APP_MAIN_COLOR,
  width,
  height,
  opacity,
}) => {
  const xml = `
  <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M1.77786 16L14.2221 16C15.1997 16 16 15.1997 16 14.2221V1.77786C16 0.800261 15.1997 -2.54313e-07 14.2221 -2.54313e-07H1.77786C0.800261 -2.54313e-07 -2.54313e-07 0.800261 -2.54313e-07 1.77786V14.2221C-2.54313e-07 15.1997 0.800261 16 1.77786 16ZM4.89068 8L4.8899 10.1685L11.5035 3.55547L12.4445 4.5L5.83411 11.1093L8.00078 11.1112V12.4445L4.44531 12.4445C3.95385 12.4435 3.55651 12.0461 3.55729 11.5555L3.55542 7.99922L4.89068 8Z" fill=${color}/>
  </svg>
  `;

  return <SvgXml xml={xml} width={width || '100%'} height={height || '100%'} />;
};

export default CollapseIcon;
