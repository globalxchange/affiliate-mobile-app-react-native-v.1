import React from 'react';
import {SvgXml} from 'react-native-svg';
import ThemeData from '../../configs/ThemeData';

const ExpandIcon = ({
  color = ThemeData.APP_MAIN_COLOR,
  width,
  height,
  opacity,
}) => {
  const xml = `
  <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M14.2221 0H1.77786C0.80026 0 0 0.80026 0 1.77786V14.2221C0 15.1997 0.80026 16 1.77786 16H14.2221C15.1997 16 16 15.1997 16 14.2221V1.77786C16 0.80026 15.1997 0 14.2221 0ZM11.1093 8L11.1101 5.83146L4.49651 12.4445L3.55547 11.5L10.1659 4.89068L7.99922 4.8888V3.55547H11.5547C12.0461 3.55651 12.4435 3.95385 12.4427 4.44453L12.4446 8.00078L11.1093 8Z" fill=${color}/>
  </svg>
  `;

  return <SvgXml xml={xml} width={width || '100%'} height={height || '100%'} />;
};

export default ExpandIcon;
