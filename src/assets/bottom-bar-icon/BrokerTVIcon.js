import React from 'react';
import {SvgXml} from 'react-native-svg';

const BrokerTVIcon = ({color, width, height, opacity}) => {
  const xml = `<svg width="31" height="28" viewBox="0 0 31 28" fill="none" fill-opacity="${opacity}" xmlns="http://www.w3.org/2000/svg">
  <path d="M20.3579 26.2203V23.2543H10.0307V26.2203H7.36089V28H23.0124V26.2203H20.3579Z" fill="#08152D"/>
  <path d="M0 0V21.4746H1.7797H28.5939H30.3736V0H0ZM13.5859 8.46247H11.4828V14.7648H9.70311V8.46247H7.60009V6.68278H13.5859V8.46247ZM19.0716 14.9288L15.3698 7.32471L16.97 6.54574L19.0716 10.8629L21.1733 6.54574L22.7735 7.32471L19.0716 14.9288Z" fill="#08152D"/>
  </svg>
  `;

  return <SvgXml xml={xml} width={width || '100%'} height={height || '100%'} />;
};

export default BrokerTVIcon;
