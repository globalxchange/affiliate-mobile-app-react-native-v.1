import React from 'react';
import {SvgXml} from 'react-native-svg';

const TeamsIcon = ({color, width, height, opacity}) => {
  const xml = `<svg width="28" height="28" viewBox="0 0 28 28" fill="none" fill-opacity="${opacity}" xmlns="http://www.w3.org/2000/svg">
  <path d="M20.5882 4.2L18.8726 8.58729L17.1569 4.2V0H0V28H8.92157V25.2H2.7451V2.8H14.4118V23.8H17.1569V11.0923L18.8726 15.4L20.5882 11.0923V23.8H23.3333V4.2H20.5882Z" fill="#4C71A3"/>
  <path d="M7.02287 23.8V11.0923L8.73856 15.4L10.4542 11.0923V28H27.6111V0H18.6895V2.8H24.866V25.2H13.1993V4.2H10.4542L8.73856 8.58729L7.02287 4.2H4.27777V23.8H7.02287Z" fill="#08152D"/>
  </svg>
  `;

  return <SvgXml xml={xml} width={width || '100%'} height={height || '100%'} />;
};

export default TeamsIcon;
