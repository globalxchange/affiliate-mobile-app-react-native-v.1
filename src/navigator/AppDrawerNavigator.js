/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
} from '@react-navigation/drawer';
import {createStackNavigator} from '@react-navigation/stack';
import CustomDrawer from '../components/CustomDrawer';
import QRScannerScreen from '../screens/QRScannerScreen';
import SupportScreen from '../screens/SupportScreen';
import {createSharedElementStackNavigator} from 'react-navigation-shared-element';
import TimelineScreen from '../screens/TimelineScreen';
import TimelineDetailsScreen from '../screens/TimelineDetailsScreen';
import LearnScreen from '../screens/LearnScreen';
import InviteScreen from '../screens/InviteScreen';
import InviteDetailsScreen from '../screens/InviteDetailsScreen';
import UpdateHistoryScreen from '../screens/UpdateHistoryScreen';
import BlockCheckScreen from '../screens/BlockCheckScreen';
import BrandsScreen from '../screens/BrandsScreen';
import EarnBottomBarNavigator from './EarnBottomBarNavigator';
import MoneyMarketScreen from '../screens/MoneyMarketScreen';
import NewChatScreen from '../screens/NewChatScreen';
import AdminScreen from '../screens/AdminScreen';
import RegisterProspectScreen from '../screens/RegisterProspectScreen';
import ProfileSettingsScreen from '../screens/ProfileSettings/ProfileSettingsScreen';
import Configure2FAScreen from '../screens/ProfileSettings/Configure2FAScreen';
import InterestSettingScreen from '../screens/ProfileSettings/InterestSettingScreen';
import ChangePasswordScreen from '../screens/ProfileSettings/ChangePasswordScreen';
import ChangeUsernameSettings from '../screens/ProfileSettings/ChangeUsernameSettings';
import ProfileScreen from '../screens/ProfileScreen';
import NetworkBottonBarNavigator from './NetworkBottonBarNavigator';
import GlobalSearchScreen from '../screens/GlobalSearchScreen';
import NewWalletScreen from '../screens/NewWalletScreen';
import UpdateAppScreen from '../screens/ProfileSettings/UpdateAppScreen';
import DeleteAccountScreen from '../screens/ProfileSettings/DeleteAccountScreen';

const Drawer = createDrawerNavigator();

const CustomDrawerContent = (props) => (
  <DrawerContentScrollView {...props} horizontal scrollEnabled={false} nav>
    <CustomDrawer />
  </DrawerContentScrollView>
);

const TimelineTransitionStack = createSharedElementStackNavigator();

const TimelineTransitionNavigator = () => (
  <TimelineTransitionStack.Navigator
    initialRouteName="List"
    screenOptions={{
      headerShown: false,

      cardStyleInterpolator: ({current: {progress}}) => ({
        cardStyle: {opacity: progress},
      }),
    }}>
    <TimelineTransitionStack.Screen name="List" component={TimelineScreen} />
    <TimelineTransitionStack.Screen
      name="Details"
      component={TimelineDetailsScreen}
      sharedElements={(route, otherRoute, showing) => {
        const {item} = route.params;
        return [
          `item.${item.key}.photo`,
          `item.${item.key}.icon`,
          `item.${item.key}.name`,
          `item.${item.key}.fab`,
          'item.appBar',
        ];
      }}
    />
  </TimelineTransitionStack.Navigator>
);

const InviteTransitionStack = createSharedElementStackNavigator();

const InviteTransitionNavigator = () => (
  <InviteTransitionStack.Navigator
    initialRouteName="List"
    screenOptions={{
      headerShown: false,

      cardStyleInterpolator: ({current: {progress}}) => ({
        cardStyle: {opacity: progress},
      }),
    }}>
    <InviteTransitionStack.Screen name="List" component={InviteScreen} />
    <InviteTransitionStack.Screen
      name="Details"
      component={InviteDetailsScreen}
      sharedElements={(route, otherRoute, showing) => {
        const {item, title} = route.params;
        return ['item.title', `item.${item.key}.item`, 'item.appBar'];
      }}
    />
  </InviteTransitionStack.Navigator>
);

const SettingsTransitionStack = createSharedElementStackNavigator();

const SettingsTransitionNavigator = () => (
  <SettingsTransitionStack.Navigator
    initialRouteName="List"
    screenOptions={{
      headerShown: false,
    }}>
    <SettingsTransitionStack.Screen
      name="List"
      component={ProfileSettingsScreen}
    />
    <SettingsTransitionStack.Screen
      name="2FASettings"
      component={Configure2FAScreen}
    />
    <SettingsTransitionStack.Screen
      name="InterestSettings"
      component={InterestSettingScreen}
    />
    <SettingsTransitionStack.Screen
      name="ChangePassword"
      component={ChangePasswordScreen}
    />
    <SettingsTransitionStack.Screen
      name="ChangeUsername"
      component={ChangeUsernameSettings}
    />
    <SettingsTransitionStack.Screen
      name="UpdateApp"
      component={UpdateAppScreen}
    />
    <SettingsTransitionStack.Screen
      name="DeleteAccount"
      component={DeleteAccountScreen}
    />
  </SettingsTransitionStack.Navigator>
);

const ChildStack = createStackNavigator();

const ChildNavigator = () => (
  <ChildStack.Navigator
    initialRouteName="Network"
    screenOptions={{headerShown: false}}>
    <ChildStack.Screen name="Brands" component={BrandsScreen} />
    <ChildStack.Screen name="Wallet" component={NewWalletScreen} />
    <ChildStack.Screen
      name="QRScanner"
      component={QRScannerScreen}
      options={{}}
    />
    <ChildStack.Screen name="Support" component={SupportScreen} />
    <ChildStack.Screen name="Lean" component={LearnScreen} />
    <ChildStack.Screen name="Invite" component={InviteTransitionNavigator} />
    <ChildStack.Screen
      name="RegisterProspect"
      component={RegisterProspectScreen}
    />
    <ChildStack.Screen name="UpdateHistory" component={UpdateHistoryScreen} />
    <ChildStack.Screen name="BlockCheck" component={BlockCheckScreen} />
    <ChildStack.Screen
      name="Timeline"
      component={TimelineTransitionNavigator}
    />
    <ChildStack.Screen name="Earn" component={EarnBottomBarNavigator} />
    <ChildStack.Screen name="MoneyMarket" component={MoneyMarketScreen} />
    <ChildStack.Screen name="ChatTest" component={NewChatScreen} />
    <ChildStack.Screen name="AdminPage" component={AdminScreen} />
    <ChildStack.Screen
      name="Settings"
      component={SettingsTransitionNavigator}
    />
    <ChildStack.Screen name="Network" component={NetworkBottonBarNavigator} />
    <ChildStack.Screen name="GlobalSearch" component={GlobalSearchScreen} />
    <ChildStack.Screen name="UserProfile" component={ProfileScreen} />
  </ChildStack.Navigator>
);

const AppDrawerNavigator = () => {
  return (
    <Drawer.Navigator
      initialRouteName="child"
      drawerContent={CustomDrawerContent}
      drawerStyle={{width: 'auto'}}
      backBehavior={'history'}>
      <Drawer.Screen name="child" component={ChildNavigator} />
    </Drawer.Navigator>
  );
};

export default AppDrawerNavigator;
