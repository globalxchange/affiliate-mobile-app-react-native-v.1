import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createSharedElementStackNavigator} from 'react-navigation-shared-element';
import CreateBrokerageScreen from '../screens/CreateBrokerageScreen';
import FeedsScreen from '../screens/FeedsScreen';
import BrokerScreen from '../screens/BrokerScreen';
import EventDetailsScreen from '../screens/EventDetailsScreen';
import ChannelScreen from '../screens/ChannelScreen';
import BrandsScreen from '../screens/BrandsScreen';
import LicenseScreen from '../screens/LicenseScreen';
import BrokerProfileScreen from '../screens/BrokerProfileScreen';
import BrokerEarningScreen from '../screens/BrokerEarningScreen';
import BrokerageController from '../screens/BrokerageController';
import ToolsScreen from '../screens/ToolsScreen';
import BrandControllerScreen from '../screens/BrandControllerScreen';
import BrandControllerSettingsScreen from '../screens/BrandControllerSettingsScreen';
import BrandEarningForecastScreen from '../screens/BrandEarningForecastScreen';
import InstaCryptoCustomPairFeesScreen from '../screens/InstaCryptoCustomPairFeesScreen';
import CouncilOverridesScreen from '../screens/CouncilOverridesScreen';
import BrokerTVScreen from '../screens/BrokerTVScreen';
import {View} from 'react-native';
import ExchangeFeePairScreen from '../screens/ExchangeFeePairScreen';
import MoneyMarketFeeScreen from '../screens/MoneyMarketFeeScreen';
import ExchangeFeeUpdateScreen from '../screens/ExchangeFeeUpdateScreen';
import MoneyMarketFeeUpdateScreen from '../screens/MoneyMarketFeeUpdateScreen';

const RootBrandStack = createSharedElementStackNavigator();

const RootBrandStackNavigator = () => (
  <RootBrandStack.Navigator
    initialRouteName="Feeds"
    screenOptions={{
      headerShown: false,
    }}>
    <RootBrandStack.Screen name="Feeds" component={FeedsScreen} />
    <RootBrandStack.Screen name="Brokers" component={BrokerScreen} />
    <RootBrandStack.Screen name="Brands" component={BrandsScreen} />
    <RootBrandStack.Screen name="License" component={LicenseScreen} />
    <RootBrandStack.Screen name="Channel" component={ChannelScreen} />
    <RootBrandStack.Screen name="Event" component={EventDetailsScreen} />
  </RootBrandStack.Navigator>
);

const RootEngageStack = createSharedElementStackNavigator();

const RootEngageStackNavigator = () => (
  <RootEngageStack.Navigator
    screenOptions={{
      headerShown: false,
    }}
    initialRouteName="Tools">
    <RootEngageStack.Screen name="Tools" component={ToolsScreen} />
  </RootEngageStack.Navigator>
);

const RootBankStack = createSharedElementStackNavigator();

const RootBankStackNavigator = () => (
  <RootBankStack.Navigator
    screenOptions={{
      headerShown: false,
    }}
    initialRouteName="AffiliateBank">
    <RootBankStack.Screen
      name="AffiliateBank"
      component={BrokerProfileScreen}
    />
    <RootBankStack.Screen
      name="BrokerEarning"
      component={BrokerEarningScreen}
    />
    <RootBankStack.Screen
      name="CouncilOverrides"
      component={CouncilOverridesScreen}
    />
  </RootBankStack.Navigator>
);

const CreateBrokerageStack = createSharedElementStackNavigator();

const CreateBrokerageNavigator = () => (
  <CreateBrokerageStack.Navigator
    screenOptions={{
      headerShown: false,
    }}
    initialRouteName="Controller">
    <CreateBrokerageStack.Screen
      name="Controller"
      component={BrokerageController}
    />
    <CreateBrokerageStack.Screen
      name="CreateBrokerage"
      component={CreateBrokerageScreen}
    />
    <CreateBrokerageStack.Screen
      name="BrandController"
      component={BrandControllerScreen}
    />
    <CreateBrokerageStack.Screen
      name="ExchangeFeePairScreen"
      component={ExchangeFeePairScreen}
    />
    <CreateBrokerageStack.Screen
      name="ExchangeFeeUpdateScreen"
      component={ExchangeFeeUpdateScreen}
    />
    <CreateBrokerageStack.Screen
      name="MoneyMarketFeeScreen"
      component={MoneyMarketFeeScreen}
    />
    <CreateBrokerageStack.Screen
      name="MoneyMarketFeeUpdateScreen"
      component={MoneyMarketFeeUpdateScreen}
    />
    <CreateBrokerageStack.Screen
      name="BrandSettings"
      component={BrandControllerSettingsScreen}
    />
    <CreateBrokerageStack.Screen
      name="BrandEarningForecast"
      component={BrandEarningForecastScreen}
    />
    <CreateBrokerageStack.Screen
      name="InstaCryptoCustomPairFees"
      component={InstaCryptoCustomPairFeesScreen}
    />
  </CreateBrokerageStack.Navigator>
);

const Tab = createBottomTabNavigator();

const EarnBottomBarNavigator = () => (
  <Tab.Navigator
    initialRouteName="BrokerTV"
    tabBarOptions={{
      activeTintColor: '#08152D',
      keyboardHidesTabBar: true,
      style: {margin: 0, padding: 0},
    }}
    tabBar={(props) => <View {...props} />}>
    <Tab.Screen name="BrokerTV" component={BrokerTVScreen} />
    <Tab.Screen name="Brands" component={RootBrandStackNavigator} />
    <Tab.Screen name="Engage" component={RootEngageStackNavigator} />
    <Tab.Screen name="AffliateBank" component={RootBankStackNavigator} />
    <Tab.Screen name="CreateBrokerage" component={CreateBrokerageNavigator} />
  </Tab.Navigator>
);

export default EarnBottomBarNavigator;
