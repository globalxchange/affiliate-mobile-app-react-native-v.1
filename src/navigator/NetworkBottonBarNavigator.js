import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import {View} from 'react-native';
import {createSharedElementStackNavigator} from 'react-navigation-shared-element';
import NetworkBottomTabs from '../components/CustomBottomTabs/NetworkBottomTabs';
import ChainViewScreen from '../screens/ChainViewScreen';
import ExploreScreen from '../screens/ExploreScreen';
import NetworkCenterScreen from '../screens/NetworkCenterScreen';
import NetworkLevelDetailsScreen from '../screens/NetworkLevelDetailsScreen';
import NetworkLevelScreen from '../screens/NetworkLevelScreen';
import NetworkPeopleScreen from '../screens/NetworkPeopleScreen';
import ProfileScreen from '../screens/ProfileScreen';
import TeamScreen from '../screens/TeamScreen';

const RootNetworkTransitionStack = createSharedElementStackNavigator();

const RootNetworkTransitionNavigator = () => (
  <RootNetworkTransitionStack.Navigator
    initialRouteName="Levels"
    screenOptions={{
      headerShown: false,
      cardStyleInterpolator: ({current: {progress}}) => ({
        cardStyle: {opacity: progress},
      }),
    }}>
    <RootNetworkTransitionStack.Screen
      name="Levels"
      component={NetworkLevelScreen}
    />
    <RootNetworkTransitionStack.Screen
      name="Details"
      component={NetworkLevelDetailsScreen}
      sharedElements={(route, otherRoute, showing) => {
        const {item} = route.params;
        return [
          `item.${item.ddlevel}.level`,
          'item.appBar',
          'item.avatar',
          'item.valuationTitle',
          'item.valuation',
        ];
      }}
    />
  </RootNetworkTransitionStack.Navigator>
);

const RootNetworkStack = createStackNavigator();

const RootNetworkNavigator = () => (
  <RootNetworkStack.Navigator
    initialRouteName="root"
    screenOptions={{
      headerShown: false,
    }}>
    <RootNetworkStack.Screen
      name="root"
      component={RootNetworkTransitionNavigator}
    />
    <RootNetworkStack.Screen name="People" component={NetworkPeopleScreen} />
    <RootNetworkStack.Screen name="ChainView" component={ChainViewScreen} />
  </RootNetworkStack.Navigator>
);

const Tab = createBottomTabNavigator();

const NetworkBottonBarNavigator = () => (
  <Tab.Navigator
    initialRouteName="Chain"
    tabBarOptions={{
      activeTintColor: '#08152D',
      keyboardHidesTabBar: true,
      style: {margin: 0, padding: 0},
    }}
    tabBar={(props) => <View {...props} />}>
    <Tab.Screen name="Explore" component={ExploreScreen} />
    <Tab.Screen name="Teams" component={TeamScreen} />
    <Tab.Screen name="Chain" component={RootNetworkNavigator} />
    <Tab.Screen name="Profile" component={ProfileScreen} />
    <Tab.Screen name="NetworkCenter" component={NetworkCenterScreen} />
  </Tab.Navigator>
);

export default NetworkBottonBarNavigator;
