import {useNavigation} from '@react-navigation/core';
import React, {useContext, useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ActionBar from '../components/ActionBar';
import ComingSoon from '../components/ComingSoon';
import {AppContext} from '../contexts/AppContextProvider';
import AppMainLayout from '../layouts/AppMainLayout';

const BrokerTVScreen = () => {
  const navigation = useNavigation();

  const {setActiveRoute} = useContext(AppContext);

  useEffect(() => {
    navigation.addListener('focus', onScreenFocus);
    return () => {
      navigation.removeListener('focus', onScreenFocus);
    };
  }, []);

  const onScreenFocus = (paylod) => {
    setActiveRoute('Earn');
  };

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      <ComingSoon />
    </AppMainLayout>
  );
};

export default BrokerTVScreen;

const styles = StyleSheet.create({});
