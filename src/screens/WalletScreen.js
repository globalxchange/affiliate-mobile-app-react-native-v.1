/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useContext, useRef} from 'react';
import {
  StyleSheet,
  Keyboard,
  Dimensions,
  View,
  BackHandler,
} from 'react-native';
import AppStatusBar from '../components/AppStatusBar';
import ActionBar from '../components/ActionBar';
import CryptoSwitcher from '../components/CryptoSwitcher';
import {AppContext} from '../contexts/AppContextProvider';
import WalletView from '../components/WalletView';
import TransactionList from '../components/TransactionList';
import Animated, {
  Transition,
  Transitioning,
  useCode,
  set,
  Clock,
  interpolate,
} from 'react-native-reanimated';
import WalletDeposit from '../components/WalletDeposit';
import WalletWithdraw from '../components/WalletWithdraw';
import AppMainLayout from '../layouts/AppMainLayout';
import {useNavigation, useRoute} from '@react-navigation/native';
import LoadingAnimation from '../components/LoadingAnimation';
import {DepositContext} from '../contexts/DepositContext';
import ReanimatedTimingHelper from '../utils/ReanimatedTimingHelper';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';
import Axios from 'axios';
import {APP_CODE, GX_API_ENDPOINT} from '../configs';
import WithdrawalContext from '../contexts/WithdrawalContext';
import TransactionAudit from '../components/TransactionAudit';
import CryptoSwitchSearch from '../components/CryptoSwitchSearch';

const {width, height} = Dimensions.get('window');

const WalletScreen = () => {
  const {
    cryptoTableData,
    setIsNumPadOpen,
    setActiveRoute,
    setWithdrawAddress,
    setWalletBalances,
  } = useContext(AppContext);

  const {
    isGXVaultSelectorExpanded,
    clearDepositState,
    setActiveWallet,
  } = useContext(DepositContext);

  const withdrawContext = useContext(WithdrawalContext);

  const expandAnimation = useRef(new Animated.Value(1));
  const transitionViewRef = useRef();

  const navigation = useNavigation();
  const {params} = useRoute();

  const [isLoading, setIsLoading] = useState(true);
  const [activeCrypto, setActiveCrypto] = useState();
  const [activeFragmentView, setActiveFragmentView] = useState('Transactions');
  const [isKeyBoardOpen, setIsKeyBoardOpen] = useState(false);
  const [isAuditOpen, setIsAuditOpen] = useState(false);
  const [selectedAuditTxn, setSelectedAuditTxn] = useState();
  const [isCoinSearchOpen, setIsCoinSearchOpen] = useState(false);

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', onKeyBoardShow);
    Keyboard.addListener('keyboardDidHide', onKeyBoardHidden);
    BackHandler.addEventListener('hardwareBackPress', handleBack);

    navigation.addListener('blur', onScreenBlur);
    navigation.addListener('focus', onScreenFocus);

    return () => {
      Keyboard.removeListener('keyboardDidShow', onKeyBoardShow);
      Keyboard.removeListener('keyboardDidHide', onKeyBoardHidden);
      BackHandler.removeEventListener('hardwareBackPress', handleBack);

      navigation.removeListener('blur', onScreenBlur);
      navigation.removeListener('focus', onScreenFocus);
    };
  }, []);

  useEffect(() => {
    if (!activeCrypto) {
      setIsLoading(true);
      setActiveCrypto(cryptoTableData[0]);
      setActiveWallet(cryptoTableData[0]);
      withdrawContext.setActiveWallet(cryptoTableData[0]);
      getWalletBalances();
    }
  }, [cryptoTableData]);

  useEffect(() => {
    if (activeCrypto) {
      setActiveWallet(activeCrypto);
      clearDepositState();
      withdrawContext.setActiveWallet(activeCrypto);
      withdrawContext.clearDepositState();
    }
  }, [activeCrypto]);

  useEffect(() => {
    setWithdrawAddress('');
    clearDepositState();
  }, [activeFragmentView]);

  useEffect(() => {
    if (params && params.coin && cryptoTableData) {
      const selectedAsset = cryptoTableData.find(
        (item) => item.coinSymbol === params.coin,
      );
      if (selectedAsset) {
        setActiveCrypto(selectedAsset);
      }
    }
  }, [params, cryptoTableData]);

  useCode(
    () =>
      isGXVaultSelectorExpanded || withdrawContext.isGXVaultSelectorExpanded
        ? [
            set(
              expandAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ]
        : [
            set(
              expandAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ],
    [isGXVaultSelectorExpanded, withdrawContext.isGXVaultSelectorExpanded],
  );

  const handleBack = () => {
    navigation.goBack();
    return true;
  };

  const onKeyBoardShow = () => {
    setIsKeyBoardOpen(true);
  };

  const onKeyBoardHidden = () => {
    setIsKeyBoardOpen(false);
  };

  const onScreenBlur = (payload) => {
    setIsNumPadOpen(false);
  };

  const onScreenFocus = (paylod) => {
    setActiveRoute('Wallet');
  };

  const openTxnAudit = (txn) => {
    setSelectedAuditTxn(txn);
    setIsAuditOpen(true);
  };

  const getWalletBalances = async () => {
    const profileId = await AsyncStorageHelper.getProfileId();

    Axios.post(`${GX_API_ENDPOINT}/coin/vault/service/balances/get`, {
      app_code: APP_CODE,
      profile_id: profileId,
    })
      .then((resp) => {
        const {data} = resp;
        if (data.status) {
          setWalletBalances(data.vault);
        }
      })
      .catch((error) => console.log('Error on getting Wallet Balance', error))
      .finally(() => setIsLoading(false));
  };

  const transition = (
    <Transition.Together>
      <Transition.In type="scale" durationMs={200} interpolation="easeInOut" />
    </Transition.Together>
  );

  const renderFragment = () => {
    if (transitionViewRef.current) {
      transitionViewRef.current.animateNextTransition();
    }
    switch (activeFragmentView) {
      case 'Transactions':
        return (
          <TransactionList
            openTxnAudit={openTxnAudit}
            activeWallet={activeCrypto}
          />
        );
      case 'Withdraw':
        return (
          <WalletWithdraw
            activeWallet={activeCrypto}
            isKeyBoardOpen={isKeyBoardOpen}
            openTxnAudit={openTxnAudit}
          />
        );
      case 'Deposit':
        return (
          <WalletDeposit
            activeWallet={activeCrypto}
            openTxnAudit={openTxnAudit}
            isKeyBoardOpen={isKeyBoardOpen}
          />
        );
      default:
        return (
          <TransactionList
            activeWallet={activeCrypto}
            openTxnAudit={openTxnAudit}
          />
        );
    }
  };

  return (
    <AppMainLayout>
      <ActionBar />
      <View style={styles.container}>
        <Animated.View
          style={{
            display: isCoinSearchOpen ? 'none' : 'flex',
            maxHeight: interpolate(expandAnimation.current, {
              inputRange: [0, 1],
              outputRange: [0, 300],
            }),
          }}>
          <CryptoSwitcher
            activeCrypto={activeCrypto}
            setActiveCrypto={setActiveCrypto}
            isCoinSearchOpen={isCoinSearchOpen}
            setIsCoinSearchOpen={setIsCoinSearchOpen}
          />
        </Animated.View>
        {isCoinSearchOpen && (
          <CryptoSwitchSearch
            setActiveCrypto={setActiveCrypto}
            onClose={() => setIsCoinSearchOpen(false)}
          />
        )}
        {isLoading && (
          <Animated.View style={[styles.loadingContainer]}>
            <LoadingAnimation />
          </Animated.View>
        )}
        {activeCrypto && (
          <View style={{flex: 1, display: isCoinSearchOpen ? 'none' : 'flex'}}>
            <WalletView
              activeFragment={activeFragmentView}
              changeFragment={setActiveFragmentView}
              activeWallet={activeCrypto}
              isKeyBoardOpen={isKeyBoardOpen}
            />
            <Transitioning.View
              style={styles.fragmentContainer}
              ref={transitionViewRef}
              transition={transition}>
              {renderFragment()}
            </Transitioning.View>
          </View>
        )}
        <TransactionAudit
          selectedAuditTxn={selectedAuditTxn}
          isAuditOpen={isAuditOpen}
          setIsAuditOpen={setIsAuditOpen}
        />
      </View>
    </AppMainLayout>
  );
};

export default WalletScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  fragmentContainer: {
    flex: 1,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    marginTop: -20,
    backgroundColor: 'white',
  },
  loadingContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    width,
    height,
    backgroundColor: 'white',
    zIndex: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
