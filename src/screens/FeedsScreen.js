/* eslint-disable react-native/no-inline-styles */
import {useNavigation, useRoute} from '@react-navigation/native';
import Axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {
  BackHandler,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import ActionBar from '../components/ActionBar';
import BrandList from '../components/BrandList';
import FeedController from '../components/FeedController';
import FeedsCategoryController from '../components/FeedsCategoryController';
import LeaderBoard from '../components/LeaderBoard';
import {GX_API_ENDPOINT} from '../configs';
import {AppContext} from '../contexts/AppContextProvider';
import AppMainLayout from '../layouts/AppMainLayout';

const FeedsScreen = () => {
  const {setActiveRoute} = useContext(AppContext);

  const navigation = useNavigation();
  const route = useRoute();

  const [selectedCategory, setSelectedCategory] = useState(carouselData[0]);
  const [activeBrandsList, setActiveBrandsList] = useState();
  const [activeBrand, setActiveBrand] = useState();
  const [isShowLeaderBoard, setIsShowLeaderBoard] = useState(false);
  const [isOtc, setIsOtc] = useState(true);

  const [otcLeads, setOtcLeads] = useState();
  const [tokensLeads, setTokensLeads] = useState();

  useEffect(() => {
    Axios.get(`${GX_API_ENDPOINT}/brokerage/get/top/revenue/brokers`)
      .then((resp) => {
        const {data} = resp;

        // console.log('Resp On Leads', data);

        if (data.status) {
          setOtcLeads(data.otcData);
          setTokensLeads(data.tokenData);
        } else {
          setOtcLeads([]);
          setTokensLeads([]);
        }
      })
      .catch((error) => {
        setOtcLeads([]);
        setTokensLeads([]);
        console.log('Error On', error);
      });
  }, []);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBack);
    navigation.addListener('focus', onScreenFocus);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBack);
      navigation.removeListener('focus', onScreenFocus);
    };
  }, []);

  useEffect(() => {
    setActiveBrandsList();
    if (selectedCategory.title === 'Financial') {
      Axios.get(
        'https://storeapi.apimachine.com/dynamic/BrokerApp/whatdoyouneedabrokerfor',
        {params: {key: '867e1ca2-8932-4eff-a3b8-e448c936821b'}},
      )
        .then((resp) => {
          const {data} = resp;
          if (data.success) {
            setActiveBrandsList(data.data);
            setActiveBrand(data.data[0]);
          } else {
            WToast.show({data: 'API Error', position: WToast.position.TOP});
          }
        })
        .catch((error) => {
          WToast.show({data: 'Network Error', position: WToast.position.TOP});
          console.log('Error getting brands list', error);
        });
    } else {
      setActiveBrandsList([]);
    }
    return () => {};
  }, [selectedCategory]);

  const handleBack = async () => {
    if (route.name === 'Feeds') {
      BackHandler.exitApp();
    }
    return true;
  };

  const onScreenFocus = (paylod) => {
    setActiveRoute('Earn');
  };

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      <FeedController
        carouselData={carouselData}
        setActiveItem={setSelectedCategory}
      />
      <FeedsCategoryController
        carouselData={activeBrandsList}
        setActiveItem={setActiveBrand}
      />
      {activeBrandsList &&
        (activeBrandsList.length > 0 ? (
          activeBrand?.formData?.Name === 'Cryptocurrency' ? (
            <View style={styles.listContainer}>
              <View style={styles.listSwitcherContainer}>
                <TouchableOpacity
                  style={styles.switch}
                  onPress={() => setIsShowLeaderBoard(false)}>
                  <Text
                    style={[
                      styles.switchText,
                      isShowLeaderBoard && {
                        opacity: 0.5,
                        fontFamily: 'Montserrat',
                      },
                    ]}>
                    Brands
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.switch}
                  onPress={() => setIsShowLeaderBoard(true)}>
                  <Text
                    style={[
                      styles.switchText,
                      !isShowLeaderBoard && {
                        opacity: 0.5,
                        fontFamily: 'Montserrat',
                      },
                    ]}>
                    Top Earners
                  </Text>
                </TouchableOpacity>
                {isShowLeaderBoard && (
                  <TouchableOpacity
                    style={styles.listType}
                    onPress={() => setIsOtc(!isOtc)}>
                    <Text style={styles.listTypeText}>
                      {isOtc ? 'OTC' : 'Token'}
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
              {isShowLeaderBoard ? (
                <LeaderBoard
                  activeBrand={activeBrand}
                  isOtc={isOtc}
                  tokensLeads={tokensLeads}
                  otcLeads={otcLeads}
                />
              ) : (
                <BrandList activeBrand={activeBrand} />
              )}
            </View>
          ) : (
            <View style={styles.listContainer}>
              <View style={styles.listSwitcherContainer}>
                <TouchableOpacity style={styles.switch}>
                  <Text
                    style={[
                      styles.switchText,
                      !isShowLeaderBoard && {opacity: 0.5},
                    ]}>
                    Top Earners
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          )
        ) : (
          <View style={styles.comingSoon}>
            <Text style={styles.comingSoonText}>Coming Soon</Text>
          </View>
        ))}
    </AppMainLayout>
  );
};

export default FeedsScreen;

const styles = StyleSheet.create({
  listContainer: {
    flex: 1,
    paddingHorizontal: 35,
    paddingTop: 30,
  },
  listSwitcherContainer: {
    flexDirection: 'row',
  },
  switch: {
    marginRight: 30,
  },
  switchText: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  comingSoon: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#EDEDED',
    borderWidth: 1,
    marginHorizontal: 35,
    borderRadius: 10,
    marginTop: 10,
    height: 160,
  },
  comingSoonText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
  },
  listType: {
    borderColor: '#08152D',
    borderWidth: 1,
    paddingHorizontal: 10,
    paddingVertical: 5,
    width: 60,
    marginLeft: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
  },
  listTypeText: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 9,
    color: '#08152D',
  },
});

const carouselData = [
  {
    image: require('../assets/broker-carosuel/globalxchange.png'),
    title: 'Financial',
  },
  {
    image: require('../assets/dummy-fill-carousel.png'),
    title: 'Gaming',
  },
  {
    image: require('../assets/dummy-fill-carousel.png'),
    title: 'Entertainment',
  },
  {
    image: require('../assets/dummy-fill-carousel.png'),
    title: 'Education',
  },
];
