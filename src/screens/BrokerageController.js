/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useState} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import ActionBar from '../components/ActionBar';
import ControllerLists from '../components/ControllerLists';
import ThemeData from '../configs/ThemeData';
import {AppContext} from '../contexts/AppContextProvider';
import AppMainLayout from '../layouts/AppMainLayout';

const BrokerageController = () => {
  const {userName} = useContext(AppContext);

  const [isGxExpanded, setIsGxExpanded] = useState(false);
  const [selectedMenu, setSelectedMenu] = useState(MENU_ITEMS[0].title);

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      <View style={[styles.container]}>
        <View
          style={[styles.coverContainer, isGxExpanded && {display: 'none'}]}>
          <Text style={styles.greetingText}>Welcome {userName},</Text>
          <Text style={styles.subHeader}>Select The CompPlan</Text>
        </View>
        {/* <View style={styles.searchContainer}>
          <TextInput
            style={styles.search}
            placeholder="Companies Who Offer Affiliate Plans..."
          />
          <Image
            source={require('../assets/search-icon.png')}
            style={styles.searchIcon}
          />
        </View> */}
        <View style={[styles.container, isGxExpanded && {marginTop: 20}]}>
          {/* <View style={styles.menuContainer}>
            {MENU_ITEMS.map((item) => (
              <TouchableOpacity
                key={item.title}
                disabled={item.disabled}
                style={[
                  styles.menuItem,
                  selectedMenu === item.title && {
                    backgroundColor: ThemeData.BORDER_COLOR,
                  },
                ]}
                onPress={() => setSelectedMenu(item.title)}>
                <Text
                  style={[
                    styles.menuTitle,
                    selectedMenu === item.title && {
                      opacity: 1,
                      fontFamily: ThemeData.FONT_BOLD,
                      fontSize: 13,
                    },
                  ]}>
                  {item.title}
                </Text>
              </TouchableOpacity>
            ))}
          </View> */}
       
          <ControllerLists
            selectedMenu={selectedMenu}
            setIsGxExpanded={setIsGxExpanded}
            isGxExpanded={isGxExpanded}
          />
        </View>
      </View>
    </AppMainLayout>
  );
};

export default BrokerageController;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  coverContainer: {
    paddingHorizontal: 30,
    paddingBottom: 30,
    paddingTop: 25,
    justifyContent: 'space-evenly',
  },
  greetingText: {
    color: ThemeData.TEXT_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    textTransform: 'capitalize',
  },
  subHeader: {
    color: ThemeData.TEXT_COLOR,
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 22,
    marginTop: 10,
  },
  brandSelector: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 30,
    borderColor: '#EBEBEB',
    borderWidth: 1,
    paddingVertical: 20,
  },
  bandName: {
    flex: 1,
    color: ThemeData.TEXT_COLOR,
    fontFamily: 'Montserrat-Bold',
    paddingHorizontal: 10,
    fontSize: 15,
  },
  brandImage: {
    width: 20,
    height: 20,
  },
  changeText: {
    color: ThemeData.TEXT_COLOR,
    fontFamily: 'Montserrat',
  },
  title: {
    color: ThemeData.TEXT_COLOR,
    fontFamily: 'Montserrat-Bold',
    fontSize: 19,
    paddingHorizontal: 30,
    marginBottom: 20,
  },
  expandButton: {
    width: 25,
    height: 25,
    padding: 5,
  },
  collapseIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  menuContainer: {
    flexDirection: 'row',
    marginTop: 30,
    marginHorizontal: 30,
  },
  menuItem: {
    borderRadius: 6,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 35,
    marginRight: 10,
    minWidth: 85,
    paddingHorizontal: 15,
  },
  menuTitle: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_MEDIUM,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
  },
  searchContainer: {
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 8,
    paddingHorizontal: 20,
    paddingVertical: 15,
    marginHorizontal: 30,
  },
  search: {
    flex: 1,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
  },
  searchIcon: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
  },
});

const MENU_ITEMS = [
  {title: 'Default'},
  {title: 'All'},
  {title: 'Brands You Follow'},
];
