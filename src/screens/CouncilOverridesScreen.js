import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ActionBar from '../components/ActionBar';
import ThemeData from '../configs/ThemeData';
import AppMainLayout from '../layouts/AppMainLayout';

const CouncilOverridesScreen = () => {
  const {navigate} = useNavigation();

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      <View style={styles.container}>
        <Text style={styles.header}>
          Here Are The Brands You Receive Overrides
        </Text>
        <TouchableOpacity
          style={styles.itemContainer}
          onPress={() => navigate('BrokerEarning', {type: 'Override'})}>
          <Image
            resizeMode="contain"
            style={styles.itemIcon}
            source={require('../assets/trading-revenue-icon.png')}
          />
          <Text style={styles.itemName}>Trading Revenue</Text>
        </TouchableOpacity>
      </View>
    </AppMainLayout>
  );
};

export default CouncilOverridesScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingTop: 30,
  },
  header: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 18,
    marginBottom: 30,
  },
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 25,
    paddingVertical: 25,
  },
  itemIcon: {
    marginRight: 10,
    height: 25,
    width: 25,
  },
  itemName: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
});
