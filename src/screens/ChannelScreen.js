import React, {useEffect, useRef, useState} from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ActionBar from '../components/ActionBar';
import CalenderCarousel from '../components/CalenderCarousel';
import EventCarousel from '../components/EventCarousel';
import AppMainLayout from '../layouts/AppMainLayout';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../configs';
import * as WebBrowser from 'expo-web-browser';
import Moment from 'moment-timezone';
import LoadingAnimation from '../components/LoadingAnimation';
import {useNavigation} from '@react-navigation/native';

const {width} = Dimensions.get('window');

const ChannelScreen = () => {
  const {navigate} = useNavigation();

  const [channelData, setChannelData] = useState(null);
  const [detailsContainerHeight, setDetailsContainerHeight] = useState(0);
  const [selectedDate, setSelectedDate] = useState('');
  const [activeEvent, setActiveEvent] = useState('');

  const axiosToken = useRef(null);

  useEffect(() => {
    (async () => {
      if (axiosToken.current) {
        axiosToken.current.cancel('Cancelled');
        // setIsLoading(false);
      }

      setChannelData(null);
      axiosToken.current = Axios.CancelToken.source();

      Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/calendar/event/get`, {
        params: {date: selectedDate},
        cancelToken: axiosToken.current.token,
      })
        .then((resp) => {
          const {data} = resp;

          // console.log('Events Data', data);

          if (data.status) {
            setChannelData(data.events || []);
          } else {
            setChannelData([]);
          }
        })
        .catch((error) => {
          // console.log('Error on Getting Events', error);
          if (error?.message !== 'Canceled') {
            setChannelData([]);
          }
        });
    })();
  }, [selectedDate]);

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />

      <View style={styles.container}>
        {channelData !== null ? (
          channelData.length > 0 ? (
            <View style={styles.mainView}>
              <View style={styles.coverContainer}>
                <Image
                  source={{
                    uri:
                      activeEvent?.cover_photo ||
                      'https://unsplash.com/photos/RRh6wyEU_4Q/download?force=true&w=640',
                  }}
                  style={styles.coverImage}
                  resizeMode="cover"
                />
                {activeEvent?.promo_Vid && (
                  <View
                    style={[StyleSheet.absoluteFill, styles.playButtonOverlay]}>
                    <TouchableOpacity
                      onPress={() =>
                        navigate('Event', {event: activeEvent, play: true})
                      }
                      style={styles.playButton}>
                      <Image
                        source={require('../assets/play-icon.png')}
                        style={styles.playIcon}
                        resizeMode="contain"
                      />
                    </TouchableOpacity>
                  </View>
                )}
              </View>
              <View
                style={[
                  styles.eventsDetailsContainer,
                  {marginTop: -(detailsContainerHeight / 2)},
                ]}
                onLayout={({
                  nativeEvent: {
                    layout: {height},
                  },
                }) => {
                  setDetailsContainerHeight(height);
                }}>
                <Text style={styles.eventTitle}>{activeEvent?.name}</Text>
                <Text style={styles.eventTime}>
                  {Moment(activeEvent?.time).format('hh:mm A')}
                </Text>
                <Text style={styles.eventVenue}>Zoom Call</Text>
                <View style={styles.eventActionContainer}>
                  <TouchableOpacity
                    onPress={() =>
                      WebBrowser.openBrowserAsync(activeEvent?.event_link)
                    }
                    style={styles.buttonFilled}>
                    <Text style={styles.buttonFilledText}>Register</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() =>
                      navigate('Event', {event: activeEvent, play: false})
                    }
                    style={styles.buttonOutline}>
                    <Text style={styles.buttonOutlineText}>More Details</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.eventsContainer}>
                <EventCarousel
                  setActiveEvent={setActiveEvent}
                  events={channelData}
                />
              </View>
            </View>
          ) : (
            <View style={styles.emptyContainer}>
              <Text style={styles.emptyText}>No Event Found For the Date</Text>
              <Text style={styles.emptyTextDate}>{selectedDate}</Text>
            </View>
          )
        ) : (
          <View style={styles.loadingContainer}>
            <LoadingAnimation />
          </View>
        )}
        <View style={styles.calenderContainer}>
          <CalenderCarousel setSelectedDate={setSelectedDate} />
        </View>
      </View>
    </AppMainLayout>
  );
};

export default ChannelScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainView: {
    flex: 1,
  },
  coverContainer: {
    width: width,
    height: 255,
  },
  coverImage: {
    flex: 1,
    width: null,
    height: null,
  },
  playButtonOverlay: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  playButton: {},
  playIcon: {
    width: 40,
    height: 40,
  },
  eventsDetailsContainer: {
    marginHorizontal: 40,
    paddingHorizontal: 30,
    paddingVertical: 20,
    backgroundColor: 'white',
    borderColor: '#F1F1F1',
    borderWidth: 1,
  },
  eventTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
    fontSize: 15,
  },
  eventTime: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
    fontSize: 22,
    marginTop: 5,
  },
  eventVenue: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    textAlign: 'center',
    fontSize: 10,
  },
  eventActionContainer: {
    flexDirection: 'row',
    marginTop: 20,
  },
  buttonFilled: {
    backgroundColor: '#08152D',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 30,
    marginRight: 15,
  },
  buttonFilledText: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
    color: 'white',
  },
  buttonOutline: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 30,
    borderColor: '#08152D',
    borderWidth: 1,
  },
  buttonOutlineText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
  eventsContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  calenderContainer: {
    paddingBottom: 30,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  emptyContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 45,
  },
  emptyText: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 16,
  },
  emptyTextDate: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 20,
    marginTop: 10,
  },
});
