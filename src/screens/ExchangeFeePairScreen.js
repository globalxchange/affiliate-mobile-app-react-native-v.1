import {KeyboardAvoidingView, Platform, StyleSheet, View} from 'react-native';
import React, {useContext, useState, useEffect} from 'react';
import {AppContext} from '../contexts/AppContextProvider';
import AppMainLayout from '../layouts/AppMainLayout';
import ActionBar from '../components/ActionBar';
import SearchHeader from '../components/SearchHeader';
import {useNavigation, useRoute} from '@react-navigation/native';
import ExchangePairList from '../components/ExchangePairList';
import ExchangeBaseFooter from '../components/ExchangeBaseFooter';
import {useExchangePairs} from '../utils/CustomHook';
import LoadingAnimation from '../components/LoadingAnimation';

const ExchangeFeePairScreen = () => {
  const {userEmail} = useContext(AppContext);
  const {goBack} = useNavigation();
  const {
    params: {type},
  } = useRoute();
  const {data: exchPairs = {}, isLoading, refetch} = useExchangePairs(
    userEmail,
    type,
  );
  const [coin, setCoin] = useState('');
  const [search, setSearch] = useState('');

  useEffect(() => {
    const tmpCoin = Object.keys(exchPairs)[0];
    setCoin(tmpCoin);
  }, [exchPairs]);

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      <SearchHeader search={search} setSearch={setSearch} onBack={goBack} />
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={styles.mainView}>
        <ExchangePairList
          pairsObject={exchPairs?.[coin] || {}}
          baseCoin={coin}
          search={search}
          refetch={refetch}
        />
        <ExchangeBaseFooter
          baseCoins={Object.keys(exchPairs)}
          coin={coin}
          setCoin={setCoin}
        />
      </KeyboardAvoidingView>
      {isLoading && (
        <View style={styles.loader}>
          <LoadingAnimation />
        </View>
      )}
    </AppMainLayout>
  );
};

export default ExchangeFeePairScreen;

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
  },
  loader: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
