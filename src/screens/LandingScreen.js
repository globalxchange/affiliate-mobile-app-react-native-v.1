/* eslint-disable react-native/no-inline-styles */
import {CommonActions, useNavigation} from '@react-navigation/native';
import React, {useContext, useEffect} from 'react';
import {
  BackHandler,
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import AppStatusBar from '../components/AppStatusBar';
import ThemeData from '../configs/ThemeData';
import {AppContext} from '../contexts/AppContextProvider';

const {width} = Dimensions.get('window');

const LandingScreen = () => {
  const navigation = useNavigation();

  const {isLoggedIn} = useContext(AppContext);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBack);
    };
  }, []);

  useEffect(() => {
    if (isLoggedIn) {
      navigation.replace('Drawer', {screen: 'Network'});
    }
  }, [isLoggedIn]);

  const handleBack = () => {
    if (navigation.canGoBack()) {
      navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [{name: 'Drawer'}],
        }),
      );
    } else {
      BackHandler.exitApp();
    }
    return true;
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <AppStatusBar backgroundColor="white" barStyle="dark-content" />
      <View style={styles.container}>
        <View style={styles.iconContainer}>
          <Image
            style={styles.appLogo}
            source={require('../assets/affliate-app-logo-dark.png')}
            resizeMode="contain"
          />
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            onPress={() => navigation.navigate('Login')}
            style={styles.filledButton}>
            <Text style={styles.filledButtonText}>Login</Text>
          </TouchableOpacity>
          <View style={styles.divider} />
          <TouchableOpacity
            onPress={() => navigation.navigate('Signup')}
            style={styles.outlinedButton}>
            <Text style={styles.outlinedButtonText}>Create</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity
          onPress={() => navigation.navigate('PreRegister')}
          style={styles.preButton}>
          <Text style={styles.preText}>Click Here If You Were</Text>
          <Text style={styles.preTextBold}>Pre-Registered</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default LandingScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    paddingVertical: 40,
  },
  iconContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  appLogo: {
    width: width * 0.7,
  },
  buttonContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  filledButton: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    justifyContent: 'center',
    alignItems: 'center',
    width: width * 0.7,
    height: 50,
  },
  filledButtonText: {
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  outlinedButton: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: ThemeData.APP_MAIN_COLOR,
    borderWidth: 1,
    width: width * 0.7,
    height: 50,
  },
  outlinedButtonText: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  divider: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    height: 1,
    width: width * 0.7,
    marginVertical: 20,
  },
  preButton: {
    flexDirection: 'row',
  },
  preText: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
  },
  preTextBold: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    marginLeft: 5,
  },
});
