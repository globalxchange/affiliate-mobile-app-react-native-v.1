import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import ActionBar from '../components/ActionBar';
import MoneyMarketsList from '../components/MoneyMarketsList';
import AppMainLayout from '../layouts/AppMainLayout';

const MoneyMarketScreen = () => {
  return (
    <AppMainLayout>
      <ActionBar />
      <View style={styles.headerContainer}>
        <Image
          source={require('../assets/money-markets-full-icon.png')}
          resizeMode="contain"
          style={styles.headerIcon}
        />
        <Text style={styles.headerText}>
          You Are Earning Interest Everyday Every Asset In Your Wallets
        </Text>
      </View>
      <MoneyMarketsList />
    </AppMainLayout>
  );
};

export default MoneyMarketScreen;

const styles = StyleSheet.create({
  headerContainer: {
    alignItems: 'center',
    marginTop: 50,
  },
  headerIcon: {
    width: 220,
  },
  headerText: {
    textAlign: 'center',
    color: '#788995',
    fontFamily: 'Montserrat-SemiBold',
    marginTop: 20,
    fontSize: 15,
    paddingHorizontal: 40,
  },
});
