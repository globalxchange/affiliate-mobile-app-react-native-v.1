import React, {useEffect, useContext} from 'react';
import {BackHandler} from 'react-native';
import AppMainLayout from '../layouts/AppMainLayout';
import {useNavigation, useRoute} from '@react-navigation/native';
import {AppContext} from '../contexts/AppContextProvider';
import ActionBar from '../components/ActionBar';
import SupportTabSwitcher from '../components/SupportTabSwitcher';
import {SupportContextProvider} from '../contexts/SupportContext';
import SupportFragment from '../components/SupportFragment';
import OnCallScreen from '../components/SupportFragment/OnHold/OnCallScreen';

const SupportScreen = () => {
  const navigation = useNavigation();
  const route = useRoute();

  const {setActiveRoute} = useContext(AppContext);

  const handleBack = () => {
    navigation.goBack();
    return true;
  };

  // console.log('route.params', route.params);

  const onScreenFocus = (paylod) => {
    setActiveRoute('Support');
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBack);

    navigation.addListener('focus', onScreenFocus);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBack);
      navigation.removeListener('focus', onScreenFocus);
    };
  }, []);

  return (
    <SupportContextProvider>
      <AppMainLayout>
        <ActionBar />
        <SupportFragment routeParams={route.params || {}} />
        {/* <SupportTabSwitcher /> */}
        <OnCallScreen />
      </AppMainLayout>
    </SupportContextProvider>
  );
};

export default SupportScreen;
