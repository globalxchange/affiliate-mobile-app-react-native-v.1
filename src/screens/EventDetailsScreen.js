/* eslint-disable react-native/no-inline-styles */
import {useRoute} from '@react-navigation/native';
import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import ActionBar from '../components/ActionBar';
import {GX_API_ENDPOINT} from '../configs';
import AppMainLayout from '../layouts/AppMainLayout';
import * as WebBrowser from 'expo-web-browser';
import Moment from 'moment-timezone';
import Clipboard from '@react-native-community/clipboard';
import VideoPlayer from '../components/VideoPlayer';
import {getUriImage} from '../utils';

const {width} = Dimensions.get('window');

const EventDetailsScreen = () => {
  const {params} = useRoute();

  const event = params?.event || '';

  const play = params?.play || false;

  const [isCopied, setIsCopied] = useState(false);
  const [hostDetails, setHostDetails] = useState('');
  const [isPlayerOpened, setIsPlayerOpened] = useState(false);

  useEffect(() => {
    if (event) {
      const email = event.event_created_by;

      Axios.get(`${GX_API_ENDPOINT}/user/details/get`, {
        params: {email},
      })
        .then((resp) => {
          const {data} = resp;

          if (data.status) {
            const hostName = data.user?.name || '';

            const formattedName = `${hostName.split(' ')[0]} ${
              hostName.split(' ')[1] ? hostName.split(' ')[1][0] : ''
            }`;

            const host = {
              name: formattedName,
              email,
              image: data.user?.profile_img || '',
            };

            setHostDetails(host);

            // console.log('Data', host);
          }
        })
        .catch((error) => {
          console.log('Error on getting user data', error);
        });
    }
  }, [event]);

  const onLinkCopy = () => {
    const copyText = event?.event_link || '';

    Clipboard.setString(copyText);

    setIsCopied(true);

    setTimeout(() => setIsCopied(false), 10000);
  };

  const timeParser = (time, type) => {
    const moment = Moment(time, 'MM/DD/YYYY, h:mm:ss A');

    const currentMoment = Moment();

    const diff = moment.diff(currentMoment, 'days');

    switch (type) {
      case 'date':
        let dateText = moment.format('MMM DD');

        if (diff === 1) {
          dateText = 'Tomorrow';
        } else if (diff === 0) {
          dateText = 'Today';
        } else if (diff === -1) {
          dateText === 'Yesterday';
        } else {
          dateText = moment.format('MMM DD');
        }
        return `${dateText}`;

      case 'time':
        const timeText = moment.format('hh:mm');
        return timeText || '';

      case 'am':
        const amText = moment.format('A');
        return amText || '';

      default:
        break;
    }
  };

  const openPlayer = () => {
    setIsPlayerOpened(true);
  };

  // console.log('event', event);

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      <View style={styles.container}>
        {isPlayerOpened ? (
          <VideoPlayer
            offsetHeight={0}
            videoId={event.promo_Vid}
            play={play}
            isBrainVideo
          />
        ) : (
          <View style={styles.coverContainer}>
            <FastImage
              source={{
                uri:
                  getUriImage(event?.cover_photo) ||
                  'https://unsplash.com/photos/RRh6wyEU_4Q/download?force=true&w=640',
              }}
              style={styles.coverImage}
              resizeMode="cover"
            />
            {event?.promo_Vid && (
              <View style={[StyleSheet.absoluteFill, styles.playButtonOverlay]}>
                <TouchableOpacity
                  onPress={openPlayer}
                  style={styles.playButton}>
                  <FastImage
                    source={require('../assets/play-icon.png')}
                    style={styles.playIcon}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
              </View>
            )}
          </View>
        )}
        <View style={styles.detailsContainer}>
          <Text style={styles.eventName}>{event?.name}</Text>
          <View style={styles.row}>
            <View style={[styles.timeContainer, styles.border]}>
              <Image
                source={require('../assets/clock-icon.png')}
                style={[styles.icon, {marginRight: 10}]}
                resizeMode="contain"
              />
              <Text style={styles.timeText}>
                {timeParser(event?.time, 'date')} At{' '}
              </Text>
              <Text style={styles.timeTextBold}>
                {timeParser(event?.time, 'time')}{' '}
              </Text>
              <Text style={styles.timeText}>
                {timeParser(event?.time, 'am')} EST
              </Text>
            </View>
            <View style={[styles.durationContainer, styles.border]}>
              <Text style={styles.durationText}>{event.duration}</Text>
            </View>
          </View>
          <View style={[styles.border, styles.linkContainer]}>
            <Image
              source={require('../assets/location-pin-icon.png')}
              style={[styles.icon, {marginRight: 10}]}
              resizeMode="contain"
            />
            <Text
              onPress={() => WebBrowser.openBrowserAsync(event?.event_link)}
              numberOfLines={1}
              style={[styles.timeText, {flex: 1, paddingRight: 10}]}>
              {isCopied ? 'Copied' : event?.event_link}
            </Text>
            <TouchableOpacity onPress={onLinkCopy}>
              <Image
                source={require('../assets/copy-icon.png')}
                style={[styles.icon, {marginLeft: 10}]}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
          <Text style={styles.title}>About This Event</Text>
          <Text numberOfLines={3} style={styles.dees}>
            {event?.event_description || ''}
          </Text>
          <Text style={styles.subTitle}>Hosted By</Text>
          <View style={styles.profileContainer}>
            <FastImage
              source={{
                uri: getUriImage(hostDetails.image),
              }}
              style={styles.profileImage}
              resizeMode="cover"
            />
            <View style={styles.profileDetails}>
              <Text style={styles.hostName}>{hostDetails.name}</Text>
              <Text style={styles.hostMail}>{hostDetails.email}</Text>
            </View>
          </View>
        </View>
      </View>
    </AppMainLayout>
  );
};

export default EventDetailsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  coverContainer: {
    width: width,
    height: 255,
  },
  coverImage: {
    flex: 1,
    width: null,
    height: null,
  },
  playButtonOverlay: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  playButton: {},
  playIcon: {
    width: 40,
    height: 40,
  },
  detailsContainer: {
    flex: 1,
    padding: 30,
  },
  eventName: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 25,
  },
  row: {
    flexDirection: 'row',
    marginTop: 15,
  },
  timeContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    overflow: 'hidden',
  },
  border: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  icon: {
    height: 20,
    width: 20,
  },
  timeText: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
  timeTextBold: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
  durationContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 15,
  },
  durationText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    textTransform: 'capitalize',
    fontSize: 12,
  },
  linkContainer: {
    flexDirection: 'row',
    marginTop: 15,
  },
  title: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    marginTop: 20,
  },
  dees: {
    color: '#828282',
    fontFamily: 'Montserrat',
    marginTop: 5,
    fontSize: 12,
  },
  subTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 16,
    marginTop: 20,
  },
  profileContainer: {
    flexDirection: 'row',
    marginTop: 10,
    alignItems: 'center',
  },
  profileImage: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    width: 70,
    height: 70,
  },
  profileDetails: {
    marginLeft: 10,
    flex: 1,
  },
  hostName: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 22,
    textTransform: 'capitalize',
  },
  hostMail: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 13,
    marginTop: 5,
  },
});
