import {CommonActions, useNavigation} from '@react-navigation/native';
import Axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import ActionBar from '../components/ActionBar';
import LoadingAnimation from '../components/LoadingAnimation';
import {APP_CODE, GX_API_ENDPOINT} from '../configs';
import ThemeData from '../configs/ThemeData';
import {AppContext} from '../contexts/AppContextProvider';
import SearchLayout from '../layouts/SearchLayout';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';

const AdminScreen = () => {
  const {forceRefreshApiData} = useContext(AppContext);

  const navigation = useNavigation();

  const [searchInput, setSearchInput] = useState('');
  const [usersList, setUsersList] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [activeSearchKey, setActiveSearchKey] = useState(SEARCH_OPTION[0]);

  useEffect(() => {
    (async () => {
      Axios.get(`${GX_API_ENDPOINT}/gxb/apps/users/get`, {
        params: {app_code: APP_CODE},
      })
        .then((resp) => {
          const {data} = resp;
          // console.log('Users List', data.users);
          setUsersList(data.users || []);
        })
        .catch((error) => {
          console.log('Error on getting AppList', error);
        });
    })();
  }, []);

  const onItemSelected = async (item) => {
    setIsLoading(true);
    try {
      const profileResp = await Axios.post(
        `${GX_API_ENDPOINT}/gxb/apps/register/user`,
        {
          email: item?.email || '',
          app_code: APP_CODE,
        },
      );

      if (profileResp.data.profile_id) {
        const success = await AsyncStorageHelper.setAdminView(
          item?.email || '',
          profileResp?.data?.profile_id || '',
        );

        if (success) {
          forceRefreshApiData();
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [{name: 'Drawer'}],
            }),
          );
        }
      }
    } catch (error) {
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <ActionBar />
      {isLoading ? (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      ) : (
        <SearchLayout
          value={searchInput}
          setValue={setSearchInput}
          list={usersList || []}
          isLoading={!usersList}
          onSubmit={(_, item) => onItemSelected(item)}
          showUserList
          onBack={navigation.goBack}
          filters={SEARCH_OPTION}
          selectedFilter={activeSearchKey}
          setSelectedFilter={setActiveSearchKey}
          placeholder={`Search User's ${activeSearchKey.title}`}
        />
      )}
    </SafeAreaView>
  );
};

export default AdminScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  headerContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 25,
  },
  headerLogo: {
    height: 45,
  },
  searchContainer: {
    flexDirection: 'row',
    marginTop: 10,
    height: 45,
    paddingHorizontal: 15,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  countryIcon: {
    height: 22,
    width: 22,
    marginTop: 'auto',
    marginBottom: 'auto',
    marginRight: 15,
  },
  searchTitle: {
    fontFamily: 'Montserrat',
    color: '#08152D',
    fontSize: 13,
    paddingRight: 15,
  },
  separator: {
    backgroundColor: '#CACACA',
    width: 1,
    height: '100%',
  },
  searchInput: {
    flexGrow: 1,
    width: 0,
    marginTop: 'auto',
    marginBottom: 'auto',
    paddingHorizontal: 15,
    fontFamily: 'Montserrat',
    fontWeight: 'normal',
    color: '#08152D',
  },
  searchIcon: {
    height: 15,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  viewContainer: {
    flex: 1,
    paddingHorizontal: 30,
    paddingVertical: 20,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  listContainer: {
    marginTop: 20,
  },
  item: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 25,
    backgroundColor: 'white',
    marginBottom: 10,
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  cryptoIcon: {
    width: 30,
    height: 30,
    borderRadius: 15,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  nameContainer: {
    flexGrow: 1,
    width: 0,
    marginLeft: 10,
  },
  countryName: {
    color: '#001D41',
    fontSize: 16,
    marginBottom: 2,
    fontFamily: 'Montserrat-Bold',
  },
  userEmail: {
    color: '#001D41',
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  priceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  methodsNo: {
    color: '#001D41',
    opacity: 0.5,
    marginBottom: 2,
    textAlign: 'right',
    fontFamily: 'Roboto-Bold',
    marginRight: 10,
    fontSize: 12,
  },
  searchOptions: {
    marginTop: 20,
  },
  optionItem: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 25,
    backgroundColor: 'white',
    marginBottom: 10,
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  optionText: {
    color: '#001D41',
    fontSize: 14,
    marginBottom: 2,
    fontFamily: 'Montserrat-SemiBold',
  },
});

const SEARCH_OPTION = [
  {title: 'Email', paramKey: 'email'},
  {title: 'Name', paramKey: 'name'},
  {title: 'Username', paramKey: 'name'},
];
