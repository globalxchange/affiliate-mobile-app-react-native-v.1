import React, {useEffect, useContext, useState} from 'react';
import {StyleSheet, Text, View, BackHandler} from 'react-native';
import AppMainLayout from '../layouts/AppMainLayout';
import ActionBar from '../components/ActionBar';
import ToolsCarousel from '../components/ToolsCarousel';
import ToolsViews from '../components/ToolsViews';
import {useNavigation} from '@react-navigation/native';
import {AppContext} from '../contexts/AppContextProvider';

const ToolsScreen = () => {
  const navigation = useNavigation();
  const {setActiveRoute} = useContext(AppContext);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBack);

    navigation.addListener('focus', onScreenFocus);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBack);

      navigation.removeListener('focus', onScreenFocus);
    };
  }, []);

  const handleBack = () => {
    navigation.goBack();
    return true;
  };

  const onScreenFocus = (paylod) => {
    setActiveRoute('Earn');
  };

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      <View style={styles.container}>
        <Text style={styles.header}>Grow Your Business With Tools </Text>
        <Text style={styles.subHeader}>Powered By AffiliateApp</Text>
        <ToolsCarousel />
        <ToolsViews />
      </View>
    </AppMainLayout>
  );
};

export default ToolsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    fontFamily: 'Montserrat-Bold',
    textAlign: 'center',
    color: '#08152D',
    fontSize: 18,
    marginTop: 20,
  },
  subHeader: {
    fontFamily: 'Montserrat',
    textAlign: 'center',
    color: '#08152D',
    fontSize: 14,
    marginBottom: 10,
  },
});
