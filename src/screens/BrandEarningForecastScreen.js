/* eslint-disable react-native/no-inline-styles */
import {useNavigation, useRoute} from '@react-navigation/native';
import React, {useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import VectorIcons from '../assets/VectorIcons';
import ActionBar from '../components/ActionBar';
import EarningForecast from '../components/EarningForecast';
import ThemeData from '../configs/ThemeData';
import AppMainLayout from '../layouts/AppMainLayout';
import PopupLayout from '../layouts/PopupLayout';

const BrandEarningForecastScreen = () => {
  const {goBack} = useNavigation();

  const {params} = useRoute();

  const [selectedFrequency, setSelectedFrequency] = useState(FREQUENCIES[0]);
  const [showFrequencyPopup, setShowFrequencyPopup] = useState(false);
  const [showSwitcher, setShowSwitcher] = useState(false);

  const {selectedBrand} = params;

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      <View style={[styles.container]}>
        <View style={styles.headerContainer}>
          <View style={styles.brandIconContainer}>
            <Image
              source={selectedBrand.icon}
              resizeMode="contain"
              style={styles.headerImage}
            />
          </View>
          {showSwitcher && (
            <>
              <TouchableOpacity
                onPress={() => setShowFrequencyPopup(true)}
                style={[styles.picker, {marginLeft: 15}]}>
                <Text style={styles.pickerText}>{selectedFrequency.title}</Text>
                <Image
                  style={styles.pickerIcon}
                  resizeMode="contain"
                  source={require('../assets/back-solid-icon.png')}
                />
              </TouchableOpacity>
              <PopupLayout
                headerBackground="#464B4E"
                autoHeight
                isOpen={showFrequencyPopup}
                headerTitle="Select Frequency"
                onClose={() => setShowFrequencyPopup(false)}>
                <View style={styles.frequencyList}>
                  {FREQUENCIES.map((item, index) => (
                    <TouchableOpacity
                      key={item.title}
                      style={[
                        styles.frequencyItem,
                        index === FREQUENCIES.length - 1 && {
                          borderBottomWidth: 0,
                        },
                      ]}
                      onPress={() => {
                        setSelectedFrequency(item);
                        setShowFrequencyPopup(false);
                      }}>
                      <Text style={styles.frequencyItemText}>{item.title}</Text>
                    </TouchableOpacity>
                  ))}
                </View>
              </PopupLayout>
            </>
          )}
        </View>
        <EarningForecast
          setShowSwitcher={setShowSwitcher}
          selectedFrequency={selectedFrequency}
        />
      </View>
    </AppMainLayout>
  );
};

export default BrandEarningForecastScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  brandIconContainer: {
    width: 170,
    height: 40,
    alignItems: 'flex-start',
  },
  headerImage: {
    width: 170,
    height: 40,
    aspectRatio: 3,
  },
  actionButton: {
    width: 35,
    height: 35,
    padding: 10,
  },
  actionIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  brandAction: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    padding: 25,
    marginBottom: 20,
  },
  brandActionIcon: {
    width: 25,
    height: 25,
    marginRight: 15,
  },
  brandActionText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  picker: {
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
  pickerText: {
    fontFamily: ThemeData.FONT_MEDIUM,
    color: '#464B4E',
    marginRight: 20,
    fontSize: 10,
  },
  pickerIcon: {
    width: 8,
    height: 8,
    transform: [{rotate: '270deg'}],
  },
  frequencyList: {
    marginVertical: -20,
  },
  frequencyItem: {
    paddingVertical: 20,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
  },
  frequencyItemText: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: '#464B4E',
  },
});

const FREQUENCIES = [
  {title: 'Daily', value: 1},
  {title: 'Weekly', value: 7},
  {title: 'Monthly', value: 30},
  {title: 'Quarterly', value: 90},
  {title: 'Annually', value: 365},
];
