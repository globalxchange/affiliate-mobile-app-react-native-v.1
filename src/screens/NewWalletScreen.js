/* eslint-disable react-native/no-inline-styles */
import {
  useFocusEffect,
  useNavigation,
  useRoute,
} from '@react-navigation/native';
import axios from 'axios';
import React, {useContext, useEffect, useRef, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import Animated, {
  Clock,
  interpolate,
  set,
  useCode,
} from 'react-native-reanimated';
import ActionBar from '../components/ActionBar';
import CryptoSwitcher from '../components/CryptoSwitcher';
import CryptoSwitchSearch from '../components/CryptoSwitchSearch';
import LoadingAnimation from '../components/LoadingAnimation';
import VaultFragmentView from '../components/VaultComponents/VaultFragmentView';
import {APP_CODE, GX_API_ENDPOINT} from '../configs';
import ThemeData from '../configs/ThemeData';
import {AppContext} from '../contexts/AppContextProvider';
import {DepositContext} from '../contexts/DepositContext';
import WithdrawalContext from '../contexts/WithdrawalContext';
import AppMainLayout from '../layouts/AppMainLayout';
import SearchLayout from '../layouts/SearchLayout';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';
import ReanimatedTimingHelper from '../utils/ReanimatedTimingHelper';

const NewWalletScreen = () => {
  const {params} = useRoute();

  const {navigate} = useNavigation();

  const {cryptoTableData, setActiveRoute} = useContext(AppContext);

  const {setActiveWallet} = useContext(DepositContext);
  const withdrawContext = useContext(WithdrawalContext);

  const [activeCrypto, setActiveCrypto] = useState();
  const [isCoinSearchOpen, setIsCoinSearchOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [activeTab, setActiveTab] = useState({name: 'Overview'});
  const [walletBalances, setWalletBalances] = useState();
  const [selectedApp, setSelectedApp] = useState();
  const [showBalanceIn, setShowBalanceIn] = useState('');
  const [vaultType, setVaultType] = useState(VAULT_LIST[0]);

  const [isSearchOpen, setIsSearchOpen] = useState(false);
  const [searchList, setSearchList] = useState();
  const [searchInput, setSearchInput] = useState('');

  const [isFragmentExpanded, setIsFragmentExpanded] = useState(false);
  const [isActionBlurEnabled, setIsActionBlurEnabled] = useState(false);

  const expandAnimation = useRef(new Animated.Value(1));
  const searchCallback = useRef(() => {});
  const axiosCancelRef = useRef();

  useFocusEffect(
    React.useCallback(() => {
      axios
        .get(`${GX_API_ENDPOINT}/gxb/apps/get`, {
          params: {
            app_code: APP_CODE,
          },
        })
        .then(({data}) => {
          if (data.status) {
            setSelectedApp(data.apps[0]);
          }
        })
        .catch((error) => {});
    }, []),
  );

  useEffect(() => {
    setWalletBalances();
    getWalletBalances(true);
  }, [params]);

  useEffect(() => {
    getWalletBalances();
  }, [selectedApp, showBalanceIn]);

  useCode(
    () =>
      isFragmentExpanded
        ? [
            set(
              expandAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ]
        : [
            set(
              expandAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ],
    [isFragmentExpanded],
  );

  useFocusEffect(
    React.useCallback(() => {
      setActiveRoute('Vaults');
    }, [setActiveRoute]),
  );

  const setCurrentVault = (vault) => {
    setActiveCrypto(vault);
    setActiveWallet(vault);
    withdrawContext.setActiveWallet(vault);
  };

  const getWalletBalances = async (forceUpdate) => {
    const email = await AsyncStorageHelper.getLoginEmail();

    if (axiosCancelRef.current) {
      axiosCancelRef.current.cancel();
    }

    axiosCancelRef.current = axios.CancelToken.source();

    if (params?.type === 'moneymarket') {
      axios
        .get(
          `${GX_API_ENDPOINT}/coin/vault/service/user/app/interest/balances/get`,
          {
            params: {
              app_code: selectedApp?.app_code || APP_CODE,
              email,
              displayCurrency: showBalanceIn?.coinSymbol || 'USD',
              orderby_dsc: true,
              with_balance: true,
            },
          },
        )
        .then(({data}) => {
          if (data.status) {
            const result = data.result[0];
            if (result) {
              const balances = result.balances[0];
              if (balances) {
                const liquid_balances = balances.liquid_balances;
                if (liquid_balances) {
                  if (!walletBalances || forceUpdate) {
                    setCurrentVault(liquid_balances[0]);
                  }
                  setWalletBalances(liquid_balances);
                }
              }
            }
          }
        })
        .catch((error) =>
          console.log('Error on getting MoneyMarket Balance', error),
        )
        .finally(() => setIsLoading(false));
    } else {
      axios
        .post(
          `${GX_API_ENDPOINT}/coin/vault/service/coins/get`,
          {
            app_code: selectedApp?.app_code || APP_CODE,
            email,
            displayCurrency: showBalanceIn?.coinSymbol || 'USD',
            type: params?.type || 'crypto',
            orderby_dsc: true,
            with_balance: true,
            post_app_prices: true,
          },
          {cancelToken: axiosCancelRef.current.token},
        )
        .then(({data}) => {
          // console.log('Data', data);

          if (data.status) {
            if (!walletBalances || forceUpdate) {
              setCurrentVault(data.coins_data[0]);
            }
            setWalletBalances(data.coins_data);
          }
        })
        .catch((error) => console.log('Error on getting Wallet Balance', error))
        .finally(() => setIsLoading(false));
    }
  };

  const setSearchCallback = (callback) => {
    searchCallback.current = callback || (() => {});
  };

  const toggleExpand = () => {
    setIsFragmentExpanded(!isFragmentExpanded);
  };

  const ACTIONS_LIST = [
    {
      title: 'Liquid Vaults',
      list: [
        {title: 'View Address', icon: require('../assets/qr-black.png')},
        {
          title: `Transfer ${activeCrypto?.coinName}`,
          icon: require('../assets/tranfer-coin-icon.png'),
          // onPress: () => withdrawContext.setIsConnectOpen(true),
        },
        {
          title: 'External Transfer',
          icon: require('../assets/external-tranfer-icon.png'),
        },
        {
          title: `Exchange ${activeCrypto?.coinName}`,
          icon: require('../assets/wallet-action-icon/waleet-icon.png'),
          onPress: () => navigate('Asset', {crypto: activeCrypto}),
        },
      ],
    },
  ];

  return (
    <AppMainLayout>
      <ActionBar />
      <View style={styles.container}>
        {!walletBalances && (
          <View
            style={[
              StyleSheet.absoluteFill,
              {
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'white',
                zIndex: 99,
              },
            ]}>
            <LoadingAnimation />
          </View>
        )}
        <Animated.View
          style={{
            maxHeight: interpolate(expandAnimation.current, {
              inputRange: [0, 1],
              outputRange: [0, 300],
            }),
            backgroundColor: 'white',
            overflow: 'hidden',
          }}>
          <View
            style={{
              display: isCoinSearchOpen ? 'none' : 'flex',
            }}>
            <CryptoSwitcher
              activeCrypto={activeCrypto}
              setActiveCrypto={setActiveCrypto}
              isCoinSearchOpen={isCoinSearchOpen}
              setIsCoinSearchOpen={setIsCoinSearchOpen}
              walletBalances={walletBalances}
              isActionBlurEnabled={isActionBlurEnabled}
            />
          </View>
        </Animated.View>
        {isCoinSearchOpen && (
          <CryptoSwitchSearch
            setActiveCrypto={setActiveCrypto}
            onClose={() => setIsCoinSearchOpen(false)}
          />
        )}
        {isLoading && (
          <Animated.View style={[styles.loadingContainer]}>
            <LoadingAnimation />
          </Animated.View>
        )}
        {activeCrypto && (
          <View style={{flex: 1, display: isCoinSearchOpen ? 'none' : 'flex'}}>
            <VaultFragmentView
              activeTab={activeTab}
              activeWallet={activeCrypto}
              setSearchList={setSearchList}
              openSearchList={() => setIsSearchOpen(true)}
              isSearchOpen={isSearchOpen}
              setSelectedApp={setSelectedApp}
              selectedApp={selectedApp}
              setVaultType={setVaultType}
              setShowBalanceIn={setShowBalanceIn}
              setSearchCallback={setSearchCallback}
              closeSearch={() => setIsSearchOpen(false)}
              vaultType={vaultType}
              walletTypes={VAULT_LIST}
              actionList={ACTIONS_LIST}
              setActiveTab={setActiveTab}
              toggleExpand={toggleExpand}
              isFragmentExpanded={isFragmentExpanded}
              onCloseActionTab={() => setActiveTab({name: 'Overview'})}
              walletBalances={walletBalances}
              showBalanceIn={showBalanceIn}
              isActionBlurEnabled={isActionBlurEnabled}
              setIsActionBlurEnabled={setIsActionBlurEnabled}
            />
          </View>
        )}
        {isSearchOpen && (
          <View style={styles.searchContainer}>
            <SearchLayout
              value={searchInput}
              setValue={setSearchInput}
              list={searchList || []}
              onBack={() => setIsSearchOpen(false)}
              disableFilter
              onSubmit={(_, item) =>
                searchCallback.current ? searchCallback.current(item) : null
              }
              showUserList
              placeholder=""
              keyboardOffset={100}
            />
          </View>
        )}
      </View>
    </AppMainLayout>
  );
};

export default NewWalletScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  fragmentContainer: {
    flex: 1,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    marginTop: -20,
    backgroundColor: 'white',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderBottomWidth: 0,
  },
  loadingContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'white',
    zIndex: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  searchContainer: {
    ...StyleSheet.absoluteFill,
    backgroundColor: 'white',
    zIndex: 999,
  },
});

const VAULT_LIST = [
  {
    title: 'Liquid Vaults',
    icon: require('../assets/liquid-icon-menu.png'),
  },
  {
    title: 'Spend Vaults',
    icon: require('../assets/spend-crypto-icon.png'),
  },
];
