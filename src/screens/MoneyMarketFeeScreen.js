import {KeyboardAvoidingView, Platform, StyleSheet, View} from 'react-native';
import React, {useContext, useState, useEffect} from 'react';
import {AppContext} from '../contexts/AppContextProvider';
import AppMainLayout from '../layouts/AppMainLayout';
import ActionBar from '../components/ActionBar';
import SearchHeader from '../components/SearchHeader';
import {useNavigation, useRoute} from '@react-navigation/native';
import {useMoneyMarketList} from '../utils/CustomHook';
import LoadingAnimation from '../components/LoadingAnimation';
import MoneyMarketCoinList from '../components/MoneyMarketCoinList';
import MoneyMarketTypeFooter from '../components/MoneyMarketTypeFooter';

const MoneyMarketFeeScreen = () => {
  const {userEmail} = useContext(AppContext);
  const {goBack} = useNavigation();
  const {
    params: {bonds, type},
  } = useRoute();
  const [typeSelected, setTypeSelected] = useState(type || 'crypto');
  const {data: feeList = [], isLoading, refetch} = useMoneyMarketList(
    userEmail,
    typeSelected,
  );
  const [search, setSearch] = useState('');

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      <SearchHeader search={search} setSearch={setSearch} onBack={goBack} />
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={styles.mainView}>
        <MoneyMarketCoinList
          feeList={feeList}
          search={search}
          refetch={refetch}
          bonds={bonds}
        />
        <MoneyMarketTypeFooter type={typeSelected} setType={setTypeSelected} />
      </KeyboardAvoidingView>
      {isLoading && (
        <View style={styles.loader}>
          <LoadingAnimation />
        </View>
      )}
    </AppMainLayout>
  );
};

export default MoneyMarketFeeScreen;

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
  },
  loader: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
