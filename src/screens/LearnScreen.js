import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ActionBar from '../components/ActionBar';
import LearnView from '../components/SupportFragment/Learn/LearnView';
import AppMainLayout from '../layouts/AppMainLayout';

const LearnScreen = () => {
  return (
    <AppMainLayout>
      <ActionBar />
      <View style={styles.container}>
        <LearnView />
      </View>
    </AppMainLayout>
  );
};

export default LearnScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#08152D',
  },
});
