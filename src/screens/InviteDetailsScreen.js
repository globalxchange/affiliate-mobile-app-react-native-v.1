import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {SharedElement} from 'react-navigation-shared-element';
import ActionBar from '../components/ActionBar';
import BrokerSyncConfigure from '../components/BrokerSyncConfigure';
import AppMainLayout from '../layouts/AppMainLayout';
import Clipboard from '@react-native-community/clipboard';
import {useRoute} from '@react-navigation/native';
import * as WebBrowser from 'expo-web-browser';
import InviteBottomSheet from '../components/InviteBottomSheet';
import {WToast} from 'react-native-smart-tip';
import ThemeData from '../configs/ThemeData';

const InviteDetailsScreen = () => {
  const {params} = useRoute();

  const [isCopied, setIsCopied] = useState(false);
  const [isInviteOpen, setIsInviteOpen] = useState(false);
  const [showNormalLink, setShowNormalLink] = useState(false);
  const [showAssistedLink, setShowAssistedLink] = useState(false);

  useEffect(() => {
    if (showAssistedLink) {
      setShowNormalLink(false);
    }
  }, [showAssistedLink]);

  useEffect(() => {
    if (showNormalLink) {
      setShowAssistedLink(false);
    }
  }, [showNormalLink]);

  const item = params.item || '';

  const title = params.title || '';

  const openLink = () => {
    if (item?.showSheet) {
      setIsInviteOpen(true);
    } else {
      const link = item?.appLink || '';
      WebBrowser.openBrowserAsync(link);
    }
  };

  return (
    <AppMainLayout disableBlockCheck>
      <SharedElement id={'item.appBar'}>
        <ActionBar />
      </SharedElement>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
          <SharedElement id={'item.title'}>
            <Text style={styles.header}>
              {'Your Network Is Your\nNet-Wort'}
            </Text>
          </SharedElement>
          <Text style={styles.subHeader}>
            Pick One Of The Following Options
          </Text>
          <Text style={styles.title}>{title}</Text>
          <View style={styles.item}>
            <SharedElement id={`item.${item.key}.item`}>
              <View style={styles.headerContainer}>
                <Image
                  source={item.icon}
                  style={styles.image}
                  resizeMode="contain"
                />
                <View style={styles.itemHeader}>
                  <Text style={styles.preHeading}>{item.headerPre}</Text>
                  <Text style={styles.itemHeading}>{item.header}</Text>
                </View>
              </View>
            </SharedElement>
            <Text style={styles.desc}>{item.intro}</Text>
            <View style={styles.appLinkContainer}>
              <TouchableOpacity onPress={openLink}>
                <View
                  style={[
                    styles.buttonFilled,
                    (item?.key === 'web' || item?.key === 'webMobile') && {
                      opacity: 0.3,
                    },
                  ]}>
                  <Text style={styles.buttonFilledText}>{item.buttons[0]}</Text>
                </View>
              </TouchableOpacity>
            </View>
            <Text style={styles.title}>Assisted Link</Text>
            <Text style={styles.desc}>{item.desc}</Text>
            <View style={{flexDirection: 'row', marginTop: 20}}>
              <TouchableOpacity
                onPress={() => setShowNormalLink(true)}
                style={[
                  showNormalLink ? styles.buttonFilled : styles.buttonOutlined,
                  {marginRight: 10},
                ]}>
                <Text
                  style={[
                    showNormalLink
                      ? styles.buttonFilledText
                      : styles.buttonOutlinedText,
                  ]}>
                  Normal Link
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => setShowAssistedLink(true)}
                style={[
                  showAssistedLink
                    ? styles.buttonFilled
                    : styles.buttonOutlined,
                ]}>
                <Text
                  style={[
                    showAssistedLink
                      ? styles.buttonFilledText
                      : styles.buttonOutlinedText,
                  ]}>
                  Assisted Link
                </Text>
              </TouchableOpacity>
            </View>
            {(showNormalLink || showAssistedLink) && (
              <LinkView
                link={
                  showAssistedLink
                    ? item?.assistedLink || ''
                    : item?.appLink || ''
                }
                onClose={() => {
                  setShowAssistedLink(false);
                  setShowNormalLink(false);
                }}
              />
            )}
            <View style={styles.additionalSharing}>
              <Text style={styles.title}>Additional Sharing Options</Text>
              <FlatList
                style={styles.list}
                horizontal
                showsHorizontalScrollIndicator={false}
                data={additionalShares}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item}) => (
                  <View style={styles.shareItem}>
                    <Image
                      style={styles.shareIcon}
                      source={item.icon}
                      resizeMode="contain"
                    />
                  </View>
                )}
              />
            </View>
          </View>
        </View>
      </ScrollView>
      <BrokerSyncConfigure />
      <InviteBottomSheet
        isIos={item.key === 'iphone'}
        isOpen={isInviteOpen}
        setIsOpen={setIsInviteOpen}
      />
    </AppMainLayout>
  );
};

const LinkView = ({link, onClose}) => {
  const onCopyHandler = () => {
    Clipboard.setString(link);

    WToast.show({
      data: 'Link Copied To Your Clipboard',
      position: WToast.position.TOP,
    });
  };

  return (
    <View style={styles.linkViewContainer}>
      <Text numberOfLines={1} style={styles.linkText}>
        {link || 'Not Available Right Now'}
      </Text>
      <TouchableOpacity style={styles.action} onPress={onCopyHandler}>
        <Image
          style={styles.copyIcon}
          resizeMode="contain"
          source={require('../assets/copy-icon.png')}
        />
      </TouchableOpacity>
      <TouchableOpacity style={styles.action} onPress={onClose}>
        <Image
          style={styles.copyIcon}
          resizeMode="contain"
          source={require('../assets/cancel-icon.png')}
        />
      </TouchableOpacity>
    </View>
  );
};

export default InviteDetailsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 25,
    paddingVertical: 20,
  },
  header: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    textAlign: 'center',
    fontSize: 26,
  },
  subHeader: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    textAlign: 'center',
    marginTop: 15,
    fontSize: 13,
  },
  list: {
    marginTop: 20,
  },
  title: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    marginTop: 25,
    marginBottom: 8,
    fontSize: 15,
  },
  item: {
    borderColor: '#EDEDED',
    borderWidth: 1,
    padding: 20,
    marginTop: 10,
  },
  headerContainer: {
    flexDirection: 'row',
  },
  image: {
    width: 40,
    height: 40,
  },
  itemHeader: {
    flex: 1,
    marginLeft: 15,
  },
  preHeading: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 13,
  },
  itemHeading: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 16,
  },
  desc: {
    marginTop: 15,
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 13,
  },
  appLinkContainer: {
    flexDirection: 'row',
    marginTop: 20,
  },
  buttonFilled: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 25,
    backgroundColor: '#08152D',
  },
  buttonFilledText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 11,
  },
  buttonOutlined: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 25,
    borderColor: '#08152D',
    borderWidth: 1,
  },
  buttonOutlinedText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 11,
  },
  additionalSharing: {
    opacity: 0.25,
  },
  shareItem: {
    marginRight: 15,
  },
  shareIcon: {
    width: 50,
    height: 50,
  },
  linkViewContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 0.5,
    borderColor: ThemeData.APP_MAIN_COLOR,
    marginTop: 20,
  },
  linkText: {
    flex: 1,
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 13,
    paddingHorizontal: 15,
  },
  action: {
    paddingHorizontal: 15,
    borderLeftColor: ThemeData.APP_MAIN_COLOR,
    borderLeftWidth: 1,
    paddingVertical: 15,
  },
  copyIcon: {
    width: 20,
    height: 20,
  },
});

const additionalShares = [
  {icon: require('../assets/share-icons/insta.png')},
  {icon: require('../assets/share-icons/youtube.png')},
  {icon: require('../assets/share-icons/twitter.png')},
  {icon: require('../assets/share-icons/snapchat.png')},
  {icon: require('../assets/share-icons/linkedin.png')},
];
