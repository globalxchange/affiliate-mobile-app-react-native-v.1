import React, {Component} from 'react';
import {
  View,
  Image,
  StyleSheet,
  Animated,
  Text,
  Dimensions,
} from 'react-native';

const {width, height} = Dimensions.get('window');
class SplashScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      logoAnim: new Animated.Value(0),
    };
  }

  componentDidMount() {
    const {logoAnim} = this.state;

    Animated.spring(logoAnim, {
      toValue: 1,
      tension: 10,
      friction: 2,
      duration: 1000,
      useNativeDriver: true,
    }).start(() => {
      setTimeout(() => {
        this.props.navigation.navigate('Drawer');
      }, 500);
    });
  }

  render() {
    const {logoAnim} = this.state;

    return (
      <View style={styles.container}>
        <Animated.View
          style={[
            styles.logoContainer,
            {
              opacity: logoAnim,
              transform: [
                {
                  translateY: logoAnim.interpolate({
                    inputRange: [0, 1],
                    outputRange: [80, 0],
                  }),
                },
              ],
            },
          ]}>
          <Image
            style={styles.image}
            source={require('../assets/app-logo.png')}
            resizeMode="contain"
          />
          <View style={styles.nameContainer}>
            <Text style={styles.nameBig}>Insta</Text>
            <Text style={styles.nameSmall}>Crypto Purchase</Text>
          </View>
        </Animated.View>
        <View style={styles.featureContinuer}>
          <Text style={styles.featureHeader}>Buy Cryptocurrency Using</Text>
          <View style={styles.featureList}>
            <View style={[styles.featureItem, {opacity: 0.5}]}>
              <Image
                style={styles.featureImage}
                resizeMode="contain"
                source={require('../assets/business-and-finance.png')}
              />
              <Text style={styles.featureText}>Bank Transfer</Text>
            </View>
            <View style={[styles.featureItem, {}]}>
              <Image
                resizeMode="contain"
                style={styles.featureMainImage}
                source={require('../assets/card-icon.png')}
              />
              <Text style={[styles.featureText, styles.featureTextMain]}>
                Credit Card
              </Text>
            </View>
            <View style={[styles.featureItem, {opacity: 0.5}]}>
              <Image
                resizeMode="contain"
                style={styles.featureImage}
                source={require('../assets/money-icon.png')}
              />
              <Text style={styles.featureText}>Cash</Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'flex-end',
  },
  logoContainer: {
    position: 'absolute',
    top: 0,
    height: height * 0.8,
    width,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
  },
  image: {
    height: 110,
    width: 110,
  },
  nameContainer: {marginLeft: 20},
  nameBig: {fontFamily: 'Montserrat-SemiBold', color: '#08152D', fontSize: 60},
  nameSmall: {fontFamily: 'Montserrat', color: '#08152D', fontSize: 20},
  featureContinuer: {},
  featureHeader: {
    fontFamily: 'Montserrat',
    color: '#08152D',
    textAlign: 'center',
    fontSize: 17,
  },
  featureList: {
    marginTop: 50,
    marginBottom: 50,
    paddingHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  featureItem: {alignItems: 'center'},
  featureImage: {height: 50},
  featureMainImage: {
    height: 100,
    marginTop: 35,
  },
  featureText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    fontSize: 8,
  },
  featureTextMain: {
    fontSize: 14,
  },
});
