import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ActionBar from '../components/ActionBar';
import ComingSoon from '../components/ComingSoon';
import AppMainLayout from '../layouts/AppMainLayout';

const TeamScreen = () => {
  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      <ComingSoon />
    </AppMainLayout>
  );
};

export default TeamScreen;

const styles = StyleSheet.create({});
