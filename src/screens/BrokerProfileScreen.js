import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ActionBar from '../components/ActionBar';
import AffiliateBank from '../components/BrokerViews/Fragments/AffiliateBank';
import AppMainLayout from '../layouts/AppMainLayout';

const BrokerProfileScreen = () => {
  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      <AffiliateBank />
    </AppMainLayout>
  );
};

export default BrokerProfileScreen;

const styles = StyleSheet.create({});
