import {useNavigation} from '@react-navigation/native';
import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  Image,
  SectionList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {SharedElement} from 'react-navigation-shared-element';
import ActionBar from '../components/ActionBar';
import BrokerSyncConfigure from '../components/BrokerSyncConfigure';
import ShareProfile from '../components/ShareProfile';
import ShareProfileCard from '../components/ShareProfile/ShareProfileCard';
import {APP_CODE, GX_API_ENDPOINT} from '../configs';
import AppMainLayout from '../layouts/AppMainLayout';

const InviteScreen = () => {
  const {navigate} = useNavigation();
  const [androidLink, setAndroidLink] = useState('');
  const [iosLink, setIosLink] = useState('');
  const [webLink, setWebLink] = useState('');
  const [iosAssistedLink, setIosAssistedLink] = useState('');
  const [androidAssistedLink, setAndroidAssistedLink] = useState('');
  const [webAssistedLink, setWebAssistedLink] = useState('');
  const [isProfileShareOpen, setIsProfileShareOpen] = useState(false);
  const [fullScreenShareOpen, setFullScreenShareOpen] = useState(false);
  const [allUsers, setAllUsers] = useState();

  const onItemClick = (data, title) => {
    navigate('Details', {item: data, title});
  };

  useEffect(() => {
    Axios.get(`${GX_API_ENDPOINT}/gxb/apps/get`, {
      params: {app_code: APP_CODE, group_id: '6fo58gkhni5x1c'},
    })
      .then((resp) => {
        const {data} = resp;

        if (data.status) {
          // console.log('data', data);

          const logs = data.apps || [];

          // logs.sort((a, b) => b.timestamp - a.timestamp);

          const latestUpdate = logs[0];

          if (latestUpdate) {
            setAndroidLink(latestUpdate.android_app_link);
            setIosLink(latestUpdate.ios_app_link);
            setWebLink(latestUpdate.registrationlink);
            setIosAssistedLink(latestUpdate.iosassistedlink);
            setAndroidAssistedLink(latestUpdate.androidassistedlink);
            setWebAssistedLink(latestUpdate.webassistedlink);
          }
        }
      })
      .catch((error) => {});

    Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/users/holdings/data/get`, {
      params: {app_code: APP_CODE, limit: 999999},
    })
      .then(({data}) => {
        const all = data?.users || [];
        const allData = all?.map((user) => user?.userData);

        setAllUsers(allData);
      })
      .catch((error) => {
        console.log('Error getting users', error);
      });
  }, []);

  const sections = [
    // {
    //   title: 'Mobile First',
    //   data: [
    //     {
    //       key: 'iphone',
    //       icon: require('../assets/apple-icon.png'),
    //       headerPre: 'Directly To',
    //       header: 'Their Iphone',
    //       intro:
    //         'This will get them to download the AffiliateApp onto their Iphone and register within your network from the app.',
    //       desc:
    //         'If Your Prospect Is Not Used To Testflight. Please Send Them This Link. It Will Walk Them Through Each Step Of Downloading The App.',
    //       appLink: iosLink,
    //       assistedLink: iosAssistedLink,
    //       buttons: ['Send Link To Their Iphone', 'App Store'],
    //       copyLink: 'https://brokerapp.io/iosassistedregistration',
    //       showSheet: true,
    //     },
    //     {
    //       key: 'android',
    //       icon: require('../assets/android-icon.png'),
    //       headerPre: 'Directly To',
    //       header: 'Their Android',
    //       intro:
    //         'This will get them to download the AffiliateApp onto their Android and register within your network from the app.',
    //       desc:
    //         'If Your Prospect Is Not Used To Brain.Stream. Please Send Them This Link. It Will Walk Them Through Each Step Of Downloading The App.',
    //       appLink: androidLink,
    //       assistedLink: androidAssistedLink,
    //       buttons: ['Send Link To Their Phone', 'Google Play'],
    //       copyLink: 'https://brokerapp.io/androidiosassitedregistration',
    //       showSheet: true,
    //     },
    //   ],
    // },
    {
      title: 'Web',
      data: [
        {
          key: 'web',
          icon: require('../assets/desktop-icon.png'),
          headerPre: 'Browser Based',
          header: 'Desktop Signup',
          intro:
            'Get them to register within your network by completing a simple form in any internet browser from their laptop or desktop.',
          desc:
            'If Your Prospect Needs Additional Instructions, Please Send Them This Link To A Detailed Walk Through.',
          appLink: webLink || 'https://brokerapp.io/register',
          assistedLink: webAssistedLink,
          buttons: ['Send Link To Their Email', 'App Store'],
          copyLink: 'https://brokerapp.io/assistedregistration',
        },
        {
          key: 'webMobile',
          icon: require('../assets/mobile-icon.png'),
          headerPre: 'Browser Based',
          header: 'Mobile Signup',
          assistedLink: webAssistedLink,
          intro:
            'Get them to register within your network by completing a simple form in any internet browser from their phone.',
          desc:
            'If Your Prospect Needs Additional Instructions, Please Send Them This Link To A Detailed Walk Through.',
          appLink: webLink || 'https://brokerapp.io/register',
          buttons: ['Send Link To Their Phone', 'App Store'],
          copyLink: 'https://brokerapp.io/assistedregistration',
        },
      ],
    },
  ];

  return (
    <AppMainLayout disableBlockCheck>
      <SharedElement id={'item.appBar'}>
        <ActionBar />
      </SharedElement>
      <View style={styles.container}>
        <SharedElement id={'item.title'}>
          <Text style={styles.header}>{'Your Network Is Your\nNet-Worth'}</Text>
        </SharedElement>
        <Text style={styles.subHeader}>
          {isProfileShareOpen
            ? 'Pick One Of The Following Options'
            : 'Step 1: As a AffiliateApp user, you have the ability to register them into your network by clicking right here.'}
        </Text>
        {isProfileShareOpen ? null : (
          <View style={styles.actionContainer}>
            <TouchableOpacity
              onPress={() => navigate('RegisterProspect')}
              style={styles.buttonOutlined}>
              <Text style={styles.buttonOutlinedText}>Invite Affiliate</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setIsProfileShareOpen(true)}
              style={styles.filledButton}>
              <Text style={styles.filledButtonText}>Share Profile</Text>
            </TouchableOpacity>
          </View>
        )}
        {isProfileShareOpen ? (
          <ShareProfileCard
            openOtherOptions={() => setFullScreenShareOpen(true)}
            onClose={() => setIsProfileShareOpen(false)}
          />
        ) : (
          <SectionList
            stickySectionHeadersEnabled={false}
            showsVerticalScrollIndicator={false}
            style={styles.list}
            keyExtractor={(item, index) => item + index}
            sections={sections}
            renderItem={({item, section}) => (
              <TouchableOpacity
                onPress={() => onItemClick(item, section.title)}>
                <View style={styles.item}>
                  <SharedElement id={`item.${item.key}.item`}>
                    <View style={styles.headerContainer}>
                      <Image
                        source={item.icon}
                        style={styles.image}
                        resizeMode="contain"
                      />
                      <View style={styles.itemHeader}>
                        <Text style={styles.preHeading}>{item.headerPre}</Text>
                        <Text style={styles.itemHeading}>{item.header}</Text>
                      </View>
                    </View>
                  </SharedElement>
                  <Text style={styles.desc}>{item.intro}</Text>
                </View>
              </TouchableOpacity>
            )}
            renderSectionHeader={({section: {title}}) => (
              <Text style={styles.title}>{title}</Text>
            )}
          />
        )}
        {fullScreenShareOpen ? (
          <ShareProfile
            onBack={() => setFullScreenShareOpen(false)}
            allUsers={allUsers}
          />
        ) : null}
      </View>
      {fullScreenShareOpen ? null : <BrokerSyncConfigure />}
    </AppMainLayout>
  );
};

export default InviteScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 25,
    paddingVertical: 20,
  },
  header: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    textAlign: 'center',
    fontSize: 26,
  },
  subHeader: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    textAlign: 'center',
    marginTop: 15,
    fontSize: 13,
    paddingHorizontal: 30,
    lineHeight: 25,
  },
  list: {
    marginTop: 20,
  },
  title: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    marginTop: 25,
    marginBottom: 8,
    fontSize: 15,
  },
  item: {
    borderColor: '#EDEDED',
    borderWidth: 1,
    padding: 20,
    marginTop: 10,
  },
  headerContainer: {
    flexDirection: 'row',
  },
  image: {
    width: 40,
    height: 40,
  },
  itemHeader: {
    flex: 1,
    marginLeft: 15,
  },
  preHeading: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 13,
  },
  itemHeading: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 16,
  },
  desc: {
    marginTop: 15,
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 13,
  },
  actionContainer: {
    flexDirection: 'row',
    marginTop: 25,
  },
  buttonOutlined: {
    flex: 1,
    borderColor: '#001D41',
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 45,
  },
  buttonOutlinedText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
  },
  filledButton: {
    flex: 1,
    backgroundColor: '#001D41',
    alignItems: 'center',
    justifyContent: 'center',
    height: 45,
    marginLeft: 15,
  },
  filledButtonText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
  },
});
