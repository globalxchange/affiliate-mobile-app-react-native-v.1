import {useNavigation} from '@react-navigation/native';
import {StyleSheet, View} from 'react-native';
import React, {useCallback, useMemo, useState} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useMutation} from 'react-query';
import axios from 'axios';
import RegisterStepOne from '../components/RegisterUserSteps/RegisterStepOne';
import AppStatusBar from '../components/AppStatusBar';
import RegisterAffiliateStepTwo from '../components/RegisterUserSteps/RegisterAffiliateStepTwo';
import RegisterAffiliateStepThree from '../components/RegisterUserSteps/RegisterAffiliateStepThree';
import RegisterAffiliateStepFour from '../components/RegisterUserSteps/RegisterAffiliateStepFour';
import RegisterAffiliateStepFive from '../components/RegisterUserSteps/RegisterAffiliateStepFive';
import RegisterAffiliateStepSix from '../components/RegisterUserSteps/RegisterAffiliateStepSix';
import RegisterAffiliateStepSeven from '../components/RegisterUserSteps/RegisterAffiliateStepSeven';
import RegisterAffiliateStepEight from '../components/RegisterUserSteps/RegisterAffiliateStepEight';
import {useRegisterdUsers} from '../utils/CustomHook';
import {emailValidator} from '../utils';
import LoadingAnimation from '../components/LoadingAnimation';
import {GX_API_ENDPOINT, GX_AUTH_URL} from '../configs';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';

const capRegex = new RegExp(/^.*[A-Z].*/);
const numRegex = new RegExp(/^.*[0-9].*/);
const speRegex = new RegExp(/^.*[!@#$%^&*()+=].*/);

async function register(paramData) {
  const {data} = await axios.post(
    'https://gxauth.apimachine.com/gx/user/signup',
    paramData,
  );
  return data;
}
async function registerValidate(paramData) {
  const {data} = await axios.post(
    'https://gxauth.apimachine.com/gx/user/confirm',
    paramData,
  );
  return data;
}

const RegisterUserScreen = () => {
  const navigation = useNavigation();
  const {data = {emails: [], usernames: []}} = useRegisterdUsers();
  const [step, setStep] = useState('home');
  const [affiliate, setAffiliate] = useState('');
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [cPassword, setCPassword] = useState('');
  const [otp, setOtp] = useState('');

  const {isLoading, mutate} = useMutation(register, {
    onSuccess: (data) => {
      if (data.status) {
      } else {
        WToast.show({
          data: data.message || 'Some Thing Went Wrong!',
          position: WToast.position.TOP,
        });
      }
    },
  });

  const loginHandler = () => {
    let postData = {
      email,
      password,
    };
    Axios.post(`${GX_AUTH_URL}/gx/user/login`, postData)
      .then((response) => {
        const {data} = response;
        if (data.status) {
          Axios.post(`${GX_API_ENDPOINT}/gxb/apps/register/user`, {
            email,
            app_code: APP_CODE,
          }).then((profileResp) => {
            if (profileResp.data.profile_id) {
              AsyncStorageHelper.setProfileId(profileResp.data.profile_id);
              setProfileId(profileResp.data.profile_id);
            }
          });
          WSnackBar.show({data: '✅ Logged in...'});
          AsyncStorageHelper.setIsLoggedIn(true);
          AsyncStorageHelper.setLoginEmail(email);
          AsyncStorageHelper.setAppToken(data.idToken);
          AsyncStorageHelper.setAccessToken(data.accessToken);
          AsyncStorageHelper.setRefreshToken(data.refreshToken);
          AsyncStorageHelper.setDeviceKey(data.device_key);
          getUserDetails(email, data.idToken);
        } else {
          setIsLoading(false);
          WSnackBar.show({data: `❌ ${data.message}`});
          if (show2FA) {
            setOtpInput('');
          } else {
            setPasswordInput('');
          }
        }
      })
      .catch((error) => {
        setIsLoading(false);
        console.log('Login Error', error);
        setPasswordInput('');
      });
  };

  const {isLoading: validating, mutate: validate} = useMutation(
    registerValidate,
    {
      onSuccess: (data) => {
        if (data.status) {
          navigation.navigate('Login');
          WToast.show({
            data: 'User Created',
            position: WToast.position.TOP,
          });
          loginHandler();
        } else {
          WToast.show({
            data: data.message || 'Some Thing Went Wrong!',
            position: WToast.position.TOP,
          });
        }
      },
    },
  );

  const onSignUp = useCallback(() => {
    mutate({
      username: username,
      email: email,
      password: password,
      ref_affiliate: affiliate,
      account_type: 'Personal',
      signedup_app: 'broker_app',
    });
  }, [affiliate, username, email, password, cPassword]);

  const getStep = useMemo(() => {
    switch (step) {
      case 'affiliateEight':
        return (
          <RegisterAffiliateStepEight
            setStep={setStep}
            otp={otp}
            setOtp={setOtp}
            isValid={otp.length === 6}
            validate={() => {
              validate({
                email,
                code: otp,
              });
            }}
          />
        );
      case 'affiliateSeven':
        return (
          <RegisterAffiliateStepSeven
            setStep={setStep}
            cPassword={cPassword}
            setCPassword={setCPassword}
            isValid={password === cPassword}
            signUp={() => onSignUp()}
          />
        );
      case 'affiliateSix':
        return (
          <RegisterAffiliateStepSix
            setStep={setStep}
            password={password}
            setPassword={setPassword}
            isValid={
              password.length >= 6 &&
              capRegex.test(password) &&
              numRegex.test(password) &&
              speRegex.test(password)
            }
          />
        );
      case 'affiliateFive':
        return (
          <RegisterAffiliateStepFive
            setStep={setStep}
            email={email}
            setEmail={setEmail}
            isValid={!data.emails.includes(email) && emailValidator(email)}
          />
        );
      case 'affiliateFour':
        return (
          <RegisterAffiliateStepFour
            setStep={setStep}
            username={username}
            setUsername={setUsername}
            isValid={!data.usernames.includes(username) && username.length > 3}
          />
        );
      case 'affiliateThree':
        return (
          <RegisterAffiliateStepThree setStep={setStep} affiliate={affiliate} />
        );
      case 'affiliateTwo':
        return (
          <RegisterAffiliateStepTwo
            setStep={setStep}
            affiliate={affiliate}
            setAffiliate={setAffiliate}
            isValid={data.usernames.includes(affiliate)}
          />
        );
      default:
        return <RegisterStepOne setStep={setStep} />;
    }
  }, [step, affiliate, username, email, password, cPassword, otp]);
  return (
    <SafeAreaView style={styles.container}>
      <AppStatusBar backgroundColor="white" barStyle="dark-content" />
      {getStep}
      {(isLoading || validating) && (
        <View style={styles.loadingAnimation}>
          <LoadingAnimation />
        </View>
      )}
    </SafeAreaView>
  );
};

export default RegisterUserScreen;

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: '#FFFFFF', position: 'relative'},
  loadingAnimation: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    backgroundColor: 'white',
  },
});
