import {useRoute} from '@react-navigation/native';
import React from 'react';
import {Image, StyleSheet, View} from 'react-native';
import ActionBar from '../components/ActionBar';
import BrandSettings from '../components/BrandSettings';
import ThemeData from '../configs/ThemeData';
import AppMainLayout from '../layouts/AppMainLayout';

const BrandControllerSettingsScreen = () => {
  const {params} = useRoute();

  const {selectedBrand} = params;

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      <View style={[styles.container]}>
        <View style={styles.headerContainer}>
          <View style={styles.brandIconContainer}>
            <Image
              source={selectedBrand.icon}
              resizeMode="contain"
              style={styles.headerImage}
            />
          </View>
        </View>
        <BrandSettings selectedBrand={selectedBrand} />
      </View>
    </AppMainLayout>
  );
};

export default BrandControllerSettingsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 30,
    paddingVertical: 20,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  brandIconContainer: {
    width: 170,
    height: 40,
    alignItems: 'flex-start',
  },
  headerImage: {
    width: 170,
    height: 40,
    aspectRatio: 3,
  },
  actionButton: {
    width: 35,
    height: 35,
    padding: 10,
  },
  actionIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  brandAction: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    padding: 25,
    marginBottom: 20,
  },
  brandActionIcon: {
    width: 25,
    height: 25,
    marginRight: 15,
  },
  brandActionText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
});
