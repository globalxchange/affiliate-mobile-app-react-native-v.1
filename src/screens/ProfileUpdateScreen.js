/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useEffect, useState} from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import AppStatusBar from '../components/AppStatusBar';
import AppMainLayout from '../layouts/AppMainLayout';
import {useNavigation, useRoute} from '@react-navigation/native';
import {Constants, FileSystem, Permissions} from 'react-native-unimodules';
import Axios from 'axios';
import {GX_API_ENDPOINT, S3_CONFIG} from '../configs';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';
import {AppContext} from '../contexts/AppContextProvider';
import {decode} from 'base64-arraybuffer';
import {S3} from 'aws-sdk';
import * as ImagePicker from 'expo-image-picker';
import {WToast} from 'react-native-smart-tip';
import LoadingAnimation from '../components/LoadingAnimation';
import PhotoPickerDialog from '../components/PhotoPickerDialog';

const {height} = Dimensions.get('window');

const ProfileUpdateScreen = () => {
  const {params} = useRoute();
  const navigation = useNavigation();

  const {setLoginData} = useContext(AppContext);

  const [imageUri, setImageUri] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [isShowPickerDialog, setIsShowPickerDialog] = useState(false);

  const name = params.name || '';
  const userName = params.userName || '';

  useEffect(() => {
    (async () => {
      if (Constants.platform.ios) {
        const {status} = await Permissions.askAsync(
          Permissions.CAMERA_ROLL,
          Permissions.CAMERA,
        );
        if (status !== 'granted') {
          // alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

  const openImageLibrary = async (useCamera) => {
    try {
      let result;
      const options = {
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 0.8,
      };

      if (useCamera) {
        result = await ImagePicker.launchCameraAsync(options);
      } else {
        result = await ImagePicker.launchImageLibraryAsync(options);
      }

      console.log('pickerResult', result);

      if (!result.cancelled) {
        setImageUri(result.uri);
      }
    } catch (E) {
      WToast.show({
        data: 'Please Check Your Permissions',
        position: WToast.position.TOP,
      });
      console.log(E);
      setIsLoading(false);
    }
  };

  const uploadImage = () => {
    if (!imageUri) {
      return WToast.show({
        data: 'Please Select An Image First',
        position: WToast.position.TOP,
      });
    }
    setIsLoading(true);

    const BUCKET_NAME = 'gxnitrousdata';

    const S3Client = new S3({
      ...S3_CONFIG,
      Bucket: BUCKET_NAME,
    });

    const fPath = imageUri;

    let options = {encoding: FileSystem.EncodingType.Base64};
    FileSystem.readAsStringAsync(fPath, options)
      .then((data) => {
        const arrayBuffer = decode(data);

        const uploadParams = {
          Bucket: BUCKET_NAME,
          Key: `brandlogos/${userName}${Date.now()}.jpg`,
          Body: arrayBuffer,
          ContentType: 'image/jpeg',
          ACL: 'public-read',
        };

        S3Client.upload(uploadParams, async (err, s3Data) => {
          if (err) {
            console.log('Uploading Profile Pic Error', err);
          }

          if (s3Data.Location) {
            const email = await AsyncStorageHelper.getLoginEmail();
            const token = await AsyncStorageHelper.getAppToken();
            Axios.post(`${GX_API_ENDPOINT}/user/details/edit`, {
              email,
              field: 'profile_img',
              value: s3Data.Location,
              accessToken: token,
            })
              .then((resp) => {
                // console.log('Profile Update Success', resp.data);
                getUserDetails(email);
                navigation.replace('Drawer', {screen: 'Network'});
              })
              .catch((error) => {
                WToast.show({
                  data: 'Profile Update Failed',
                  position: WToast.position.TOP,
                });
                console.log('Profile Update Failed', error.message);
              })
              .finally(() => setIsLoading(false));
          }
          // console.log('Upload Success', s3Data);
        });
      })
      .catch((err) => {
        console.log('​getFile -> err', err);
        setIsLoading(false);
      });
  };

  const clearData = () => {
    setImageUri();
    setLoginData(false);
  };

  const getUserDetails = (email) => {
    Axios.get(`${GX_API_ENDPOINT}/user/details/get`, {
      params: {email},
    })
      .then((res) => {
        const {data} = res;

        AsyncStorageHelper.setUserName(data.user.username);
        AsyncStorageHelper.setUserAvatarUrl(data.user.profile_img);
        setImageUri(data.user.profile_img);
        AsyncStorageHelper.setAffId(data.user.affiliate_id);
        setLoginData(data.user.username, true, data.user.profile_img);
      })
      .catch((error) => {
        console.log('getUserDetails Error', error);
      });
  };

  return (
    <AppMainLayout disableBlockCheck>
      <AppStatusBar backgroundColor="#08152D" barStyle="light-content" />
      <View style={styles.cover}>
        <Image
          source={require('../assets/affliate-app-logo-white.png')}
          style={styles.coverLogo}
          resizeMode="contain"
        />
      </View>
      <View style={styles.container}>
        <Image
          resizeMode="cover"
          style={styles.profileImage}
          source={
            imageUri
              ? {uri: imageUri}
              : require('../assets/profile-placeholder.png')
          }
        />
        {isLoading ? (
          <View style={styles.loadingContainer}>
            <LoadingAnimation />
          </View>
        ) : (
          <View style={styles.actionsContainer}>
            <Text style={styles.title}>
              {imageUri ? "Now We're Talking " : `Welcome, ${name}`}
            </Text>
            <Text style={[styles.subs, {opacity: imageUri ? 0 : 1}]}>
              We Believe In Brokers With Faces So WeCan’t Let You In Without A
              Profile Picture. Plus Your A BrokerStar In The Making.
            </Text>
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                style={styles.btnOutlined}
                onPress={imageUri ? clearData : () => openImageLibrary(true)}>
                <Text style={styles.btnOutlinedText}>
                  {imageUri ? 'Try Again' : 'Take Selfie'}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.btnFilled}
                onPress={
                  imageUri ? uploadImage : () => openImageLibrary(false)
                }>
                <Text style={styles.btnFilledText}>
                  {imageUri ? 'Proceed To App' : 'Upload Photo'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </View>
      <PhotoPickerDialog
        isOpen={isShowPickerDialog}
        setIsOpen={setIsShowPickerDialog}
        callBack={(isCamera) => openImageLibrary(isCamera)}
      />
    </AppMainLayout>
  );
};

export default ProfileUpdateScreen;

const styles = StyleSheet.create({
  cover: {
    backgroundColor: '#08152D',
    paddingHorizontal: 20,
    justifyContent: 'center',
    alignItems: 'center',
    height: height * 0.35,
  },
  coverLogo: {
    height: 60,
    width: 400,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    marginTop: -70,
  },
  profileImage: {
    height: 140,
    width: 140,
    borderRadius: 70,
  },
  actionsContainer: {
    flex: 1,
    justifyContent: 'space-evenly',
    paddingHorizontal: 40,
  },
  title: {
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
    fontSize: 18,
    textAlign: 'center',
  },
  subs: {
    fontFamily: 'Montserrat',
    color: '#08152D',
    fontSize: 14,
    textAlign: 'center',
  },
  buttonContainer: {
    paddingHorizontal: 30,
  },
  btnOutlined: {
    borderColor: '#08152D',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    marginBottom: 15,
  },
  btnOutlinedText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
  },
  btnFilled: {
    backgroundColor: '#08152D',
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
  },
  btnFilledText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
});
