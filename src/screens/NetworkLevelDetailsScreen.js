import React, {useContext, useEffect, useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import ActionBar from '../components/ActionBar';
import {AppContext} from '../contexts/AppContextProvider';
import {useNavigation, useRoute} from '@react-navigation/native';
import {SharedElement} from 'react-navigation-shared-element';
import {getUriImage, usdValueFormatter} from '../utils';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';
import {GX_API_ENDPOINT} from '../configs';
import Axios from 'axios';
import LoadingAnimation from '../components/LoadingAnimation';
import AppMainLayout from '../layouts/AppMainLayout';
import SkeltonItem from '../components/SkeltonItem';

const NetworkLevelDetailsScreen = () => {
  const {params} = useRoute();

  const {navigate} = useNavigation();

  const {
    avatar,
    isLoggedIn,
    walletBalances,
    profileId,
    updateWalletBalances,
    cryptoTableData,
    affiliateBalance,
    updateBrokerBalances,
  } = useContext(AppContext);

  const {item} = params;

  const [vaultBalance, setVaultBalance] = useState(0);
  const [netWorkValuation, setNetWorkValuation] = useState('');
  const [levelData, setLevelData] = useState();

  useEffect(() => {
    if (isLoggedIn && !walletBalances && profileId) {
      updateWalletBalances();
    }
  }, [isLoggedIn, profileId]);

  useEffect(() => {
    if (walletBalances && cryptoTableData) {
      let totalBalance = 0;

      // console.log('Wallet Balance', walletBalances);
      // console.log('cryptoTableData', cryptoTableData);

      cryptoTableData.forEach((x) => {
        totalBalance +=
          walletBalances[`${x.coinSymbol.toLowerCase()}_balance`] * x.price.USD;
      });
      setVaultBalance(totalBalance);
    }
  }, [walletBalances, cryptoTableData]);

  useEffect(() => {
    if (affiliateBalance === '') {
      updateBrokerBalances();
    }
  }, []);

  useEffect(() => {
    if (vaultBalance && affiliateBalance !== '') {
      setNetWorkValuation(vaultBalance + affiliateBalance);
    }
  }, [vaultBalance, affiliateBalance]);

  useEffect(() => {
    (async () => {
      if (item && isLoggedIn) {
        const email = await AsyncStorageHelper.getLoginEmail();

        Axios.get(`${GX_API_ENDPOINT}/brokerage/get/broker/users/by/levels`, {
          params: {
            email,
            dd_level: item.ddlevel,
            getAll: true,
          },
        })
          .then((resp) => {
            const {data} = resp;

            // console.log('Resp', data);

            if (data.status) {
              setLevelData(data.levelData);
            } else {
              setLevelData({});
            }
          })
          .catch((error) => {
            console.log('Error on getting Network data', error);
          });
      }
    })();
  }, [item]);

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      <View style={styles.coverContainer}>
        <SharedElement id={'item.avatar'}>
          <FastImage
            source={{uri: getUriImage(avatar)}}
            resizeMode="contain"
            style={styles.profileImage}
          />
        </SharedElement>
        <View style={styles.titleContainer}>
          <SharedElement id={'item.valuationTitle'}>
            <Text style={styles.title}>Your Network Valuation</Text>
          </SharedElement>
          <SharedElement id={'item.valuation'}>
            <Text style={styles.valuation}>
              {netWorkValuation === ''
                ? 'Calculating'
                : usdValueFormatter.format(netWorkValuation)}
            </Text>
          </SharedElement>
        </View>
      </View>
      <View style={styles.levelHeader}>
        <TouchableOpacity
          onPress={() => navigate('Levels')}
          style={styles.backButton}>
          <Image
            source={require('../assets/back-solid-icon.png')}
            style={styles.backIcon}
            resizeMode="contain"
          />
        </TouchableOpacity>
        <SharedElement id={`item.${item?.ddlevel}.level`}>
          <Text style={styles.levelName}>DD Level {item?.ddlevel}</Text>
        </SharedElement>
      </View>
      <View style={styles.controllerContainer}>
        <View style={styles.controllerPrimary}>
          <Text style={styles.controllerPrimaryText}>Stats</Text>
        </View>
        <TouchableOpacity
          onPress={() => navigate('People', {item})}
          style={styles.controller}>
          <Text style={styles.controllerText}>People</Text>
        </TouchableOpacity>
      </View>
      {levelData ? (
        <>
          <View style={styles.featureContainer}>
            <View style={styles.feature}>
              <Text style={styles.featureValue}>{levelData?.count || 0}</Text>
              <Text style={styles.featureLabel}>Users</Text>
            </View>
            <View style={styles.feature}>
              <Text style={styles.featureValue}>{levelData?.brands || 0}</Text>
              <Text style={styles.featureLabel}>Brands</Text>
            </View>
            <View style={styles.feature}>
              <Text style={styles.featureValue}>
                {levelData?.one_timers || 0}
              </Text>
              <Text style={styles.featureLabel}>One Timers</Text>
            </View>
          </View>
          <View style={styles.listContainer}>
            <View style={styles.listItem}>
              <Text style={styles.listLabel}>Gross Volume</Text>
              <Text style={styles.listValue}>
                {usdValueFormatter.format(levelData?.revenue_data?.volume || 0)}
              </Text>
            </View>
            <View style={styles.listItem}>
              <Text style={styles.listLabel}>Revenue Generated</Text>
              <Text style={styles.listValue}>
                {usdValueFormatter.format(
                  levelData?.revenue_data?.revenue_generated || 0,
                )}
              </Text>
            </View>
            <View style={styles.listItem}>
              <Text style={styles.listLabel}>You Have Earned</Text>
              <Text style={styles.listValue}>
                {usdValueFormatter.format(
                  levelData?.revenue_data?.revenue_earned || 0,
                )}
              </Text>
            </View>
            <View style={styles.listItem}>
              <Text style={styles.listLabel}>DD Level 0 Valuation</Text>
              <Text style={styles.listValue}>
                {usdValueFormatter.format(
                  levelData?.revenue_data?.revenue_generated || 0,
                )}
              </Text>
            </View>
          </View>
        </>
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingSkelton />
        </View>
      )}
    </AppMainLayout>
  );
};

const LoadingSkelton = () => (
  <>
    <View style={styles.featureContainer}>
      <View style={styles.feature}>
        <SkeltonItem
          itemWidth={50}
          itemHeight={50}
          style={[styles.featureValue]}
        />
        <SkeltonItem
          itemWidth={50}
          itemHeight={10}
          style={[styles.featureLabel]}
        />
      </View>
      <View style={styles.feature}>
        <SkeltonItem
          itemWidth={50}
          itemHeight={50}
          style={[styles.featureValue]}
        />
        <SkeltonItem
          itemWidth={50}
          itemHeight={10}
          style={[styles.featureLabel]}
        />
      </View>
      <View style={styles.feature}>
        <SkeltonItem
          itemWidth={50}
          itemHeight={50}
          style={[styles.featureValue]}
        />
        <SkeltonItem
          itemWidth={50}
          itemHeight={10}
          style={[styles.featureLabel]}
        />
      </View>
    </View>
    <View style={styles.listContainer}>
      <View style={styles.listItem}>
        <SkeltonItem
          itemWidth={100}
          itemHeight={10}
          style={[styles.listLabel]}
        />
        <SkeltonItem
          itemWidth={100}
          itemHeight={10}
          style={[styles.listValue]}
        />
      </View>
      <View style={styles.listItem}>
        <SkeltonItem
          itemWidth={100}
          itemHeight={10}
          style={[styles.listLabel]}
        />
        <SkeltonItem
          itemWidth={100}
          itemHeight={10}
          style={[styles.listValue]}
        />
      </View>
      <View style={styles.listItem}>
        <SkeltonItem
          itemWidth={100}
          itemHeight={10}
          style={[styles.listLabel]}
        />
        <SkeltonItem
          itemWidth={100}
          itemHeight={10}
          style={[styles.listValue]}
        />
      </View>
      <View style={styles.listItem}>
        <SkeltonItem
          itemWidth={100}
          itemHeight={10}
          style={[styles.listLabel]}
        />
        <SkeltonItem
          itemWidth={100}
          itemHeight={10}
          style={[styles.listValue]}
        />
      </View>
    </View>
  </>
);

export default NetworkLevelDetailsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  coverContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 40,
    paddingHorizontal: 30,
  },
  profileImage: {
    width: 70,
    height: 70,
    borderRadius: 5,
  },
  title: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 14,
    textAlign: 'right',
  },
  valuation: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 26,
    marginTop: 5,
    textAlign: 'right',
  },
  titleContainer: {
    flex: 1,
    alignItems: 'flex-end',
  },
  levelHeader: {
    height: 60,
    borderColor: '#EBEBEB',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  levelName: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
  },
  backButton: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    justifyContent: 'center',
    paddingLeft: 15,
    paddingVertical: 22,
  },
  backIcon: {
    flex: 1,
  },
  controllerContainer: {
    flexDirection: 'row',
  },
  controllerPrimary: {
    flex: 1,
    height: 65,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#EBEBEB',
  },
  controllerPrimaryText: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
  },
  controller: {
    flex: 1,
    height: 65,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#EBEBEB',
    borderBottomWidth: 1,
  },
  controllerText: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 15,
  },
  featureContainer: {
    flexDirection: 'row',
    paddingVertical: 25,
  },
  feature: {
    flex: 1,
    alignItems: 'center',
  },
  featureValue: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 30,
    marginBottom: 5,
  },
  featureLabel: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 13,
  },
  listContainer: {
    flex: 1,
    justifyContent: 'space-evenly',
    paddingBottom: 15,
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 35,
    alignItems: 'center',
    paddingVertical: 5,
  },
  listLabel: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 13,
  },
  listValue: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 16,
    opacity: 0.5,
  },
  loadingContainer: {
    flex: 1,
  },
});
