/* eslint-disable react-native/no-inline-styles */
import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  RefreshControl,
} from 'react-native';
import ActionBar from '../components/ActionBar';
import LoadingAnimation from '../components/LoadingAnimation';
import {APP_CODE, GX_API_ENDPOINT} from '../configs';
import AppMainLayout from '../layouts/AppMainLayout';
import Moment from 'moment-timezone';
import * as WebBrowser from 'expo-web-browser';

const UpdateHistoryScreen = () => {
  const [updatesList, setUpdatesList] = useState();

  const [isIosSelected, setIsIosSelected] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    gerLogs();
  }, []);

  const gerLogs = () => {
    setIsLoading(true);
    Axios.get(`${GX_API_ENDPOINT}/gxb/apps/mobile/app/links/logs/get`, {
      params: {app_code: APP_CODE},
    })
      .then((resp) => {
        const {data} = resp;

        if (data.status) {
          const logs = data.logs || [];

          // console.log('Logs', logs);
          // logs.sort((a, b) => a.timestamp - b.timestamp);

          setUpdatesList(logs);
        }
      })
      .catch((error) => {})
      .finally(() => setIsLoading(false));
  };

  const formatTime = (timestamp) => {
    const timeFormat = 'MMMM Do YYYY';

    const date = Moment(timestamp);

    return date.format(timeFormat);
  };

  const onItemClick = (link) => {
    WebBrowser.openBrowserAsync(link || '');
  };

  const onRefresh = () => {
    gerLogs();
  };

  return (
    <AppMainLayout>
      <ActionBar />
      <View style={styles.container}>
        <View style={styles.header}>
          <TouchableOpacity onPress={() => setIsIosSelected(true)}>
            <View style={[styles.type, {opacity: isIosSelected ? 1 : 0.4}]}>
              <Text style={styles.typeText}>iOS</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => setIsIosSelected(false)}>
            <View style={[styles.type, {opacity: isIosSelected ? 0.4 : 1}]}>
              <Text style={styles.typeText}>Android</Text>
            </View>
          </TouchableOpacity>
        </View>

        {updatesList ? (
          <FlatList
            showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl refreshing={isLoading} onRefresh={onRefresh} />
            }
            style={styles.list}
            data={updatesList}
            keyExtractor={(item, index) => item._id}
            renderItem={({item, index}) => (
              <TouchableOpacity
                onPress={() =>
                  onItemClick(
                    isIosSelected ? item.ios_app_link : item.android_app_link,
                  )
                }
                style={[
                  styles.updateItem,
                  index === 0 && {borderWidth: 0, backgroundColor: '#08152D'},
                ]}>
                <Image
                  style={styles.appIcon}
                  resizeMode="contain"
                  source={
                    index === 0
                      ? require('../assets/app-logo-inverted.png')
                      : require('../assets/app-logo.png')
                  }
                />
                <Text
                  style={[
                    styles.date,
                    index === 0 && {color: 'white', marginRight: 0},
                  ]}>
                  {formatTime(item.timestamp)}
                </Text>
                {index === 0 && (
                  <Image
                    style={styles.newIcon}
                    resizeMode="contain"
                    source={require('../assets/new-badge.png')}
                  />
                )}
              </TouchableOpacity>
            )}
            ListEmptyComponent={
              <View style={styles.emptyContainer}>
                <Text style={styles.emptyText}>
                  No Updates Are Available For{' '}
                  {isIosSelected ? 'iOS' : 'Android'} Right Now
                </Text>
              </View>
            }
          />
        ) : (
          <View style={styles.loadingContainer}>
            <LoadingAnimation />
          </View>
        )}
      </View>
    </AppMainLayout>
  );
};

export default UpdateHistoryScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 25,
    paddingVertical: 30,
  },
  header: {
    flexDirection: 'row',
  },
  type: {
    marginRight: 15,
  },
  typeText: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 24,
    color: '#08152D',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  list: {
    marginTop: 10,
  },
  updateItem: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    flexDirection: 'row',
    height: 60,
    alignItems: 'center',
    marginTop: 20,
  },
  appIcon: {
    width: 30,
    height: 30,
    marginLeft: 20,
  },
  date: {
    flex: 1,
    textAlign: 'right',
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    fontSize: 16,
    marginRight: 20,
  },
  newIcon: {
    height: 60,
    width: 40,
  },
  emptyContainer: {
    paddingTop: 30,
  },
  emptyText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    textAlign: 'center',
  },
});
