import {useNavigation} from '@react-navigation/core';
import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import ActionBar from '../components/ActionBar';
import {APP_CODE, GX_API_ENDPOINT} from '../configs';
import AppMainLayout from '../layouts/AppMainLayout';
import SearchLayout from '../layouts/SearchLayout';

const GlobalSearchScreen = () => {
  const {bottom} = useSafeAreaInsets();

  const {navigate, goBack} = useNavigation();

  const [searchInput, setSearchInput] = useState('');
  const [usersList, setUsersList] = useState('');
  const [activeSearchKey, setActiveSearchKey] = useState(SEARCH_OPTION[0]);
  const [brandList, setBrandList] = useState('');

  useEffect(() => {
    axios
      .get(`${GX_API_ENDPOINT}/gxb/apps/users/get`, {
        params: {app_code: APP_CODE},
      })
      .then((resp) => {
        const {data} = resp;
        // console.log('Users List', data.users);
        setUsersList(data.users || []);
      })
      .catch((error) => {
        console.log('Error on getting AppList', error);
      });

    axios
      .get('https://teller2.apimachine.com/admin/allBankers')
      .then((resp) => {
        const {data} = resp;
        if (data.status) {
          console.log('data', data);

          const brands = (data.data || []).map((item) => ({
            ...item,
            name: item.displayName,
            profile_img: item.profilePicURL,
          }));

          setBrandList(brands);
        } else {
          setBrandList([]);
        }
      })
      .catch((error) => {
        setBrandList([]);
      });
  }, []);

  const onItemSelected = async (item) => {
    if (activeSearchKey?.title === 'Brands') {
      navigate('Brands', {selectedItem: item});
    } else {
      navigate('UserProfile', {userEmail: item?.email});
    }
  };

  return (
    <AppMainLayout>
      <ActionBar />
      <SearchLayout
        value={searchInput}
        setValue={setSearchInput}
        list={
          (activeSearchKey?.title === 'Brands' ? brandList : usersList) || []
        }
        isLoading={!usersList}
        onSubmit={(_, item) => onItemSelected(item)}
        showUserList
        onBack={goBack}
        filters={SEARCH_OPTION}
        selectedFilter={activeSearchKey}
        setSelectedFilter={setActiveSearchKey}
        placeholder={
          activeSearchKey?.title === 'Brands'
            ? 'Search Brands'
            : `Search User's ${activeSearchKey.title}`
        }
        keyboardOffset={bottom || 20}
      />
    </AppMainLayout>
  );
};

export default GlobalSearchScreen;

const styles = StyleSheet.create({});

const SEARCH_OPTION = [
  {title: 'Email', paramKey: 'email'},
  {title: 'Name', paramKey: 'name'},
  {title: 'Username', paramKey: 'name'},
  {title: 'Brands', paramKey: 'name'},
];
