/* eslint-disable react-native/no-inline-styles */
import Axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {
  BackHandler,
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {SharedElement} from 'react-navigation-shared-element';
import ActionBar from '../components/ActionBar';
import LoadingAnimation from '../components/LoadingAnimation';
import {GX_API_ENDPOINT} from '../configs';
import {AppContext} from '../contexts/AppContextProvider';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';
import {useNavigation} from '@react-navigation/native';
import {getUriImage, numberFormatter, usdValueFormatter} from '../utils';
import AppMainLayout from '../layouts/AppMainLayout';
import ThemeData from '../configs/ThemeData';
import ToolTipContent from '../components/ToolTipContent';
import SkeltonItem from '../components/SkeltonItem';

const NetworkLevelScreen = () => {
  const navigation = useNavigation();

  const {
    avatar,
    setActiveRoute,
    isLoggedIn,
    walletBalances,
    updateWalletBalances,
    profileId,
    cryptoTableData,
    affiliateBalance,
    updateBrokerBalances,
  } = useContext(AppContext);

  const [networkData, setNetworkData] = useState();
  const [isLoading, setIsLoading] = useState(true);
  const [vaultBalance, setVaultBalance] = useState(0);
  const [netWorkValuation, setNetWorkValuation] = useState();
  const [totalDepth, setTotalDepth] = useState(0);
  const [totalUsers, setTotalUsers] = useState();
  const [isValue, setIsValue] = useState(false);
  const [isTooltipVisible, setIsTooltipVisible] = useState(false);
  const [isLevelTooltipVisible, setIsLevelTooltipVisible] = useState(false);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBack);
    };
  }, []);

  useEffect(() => {
    if (isLoggedIn && !walletBalances && profileId) {
      updateWalletBalances();
    }
  }, [isLoggedIn, profileId]);

  useEffect(() => {
    if (walletBalances && cryptoTableData) {
      let totalBalance = 0;

      // console.log('Wallet Balance', walletBalances);
      // console.log('cryptoTableData', cryptoTableData);

      cryptoTableData.forEach((item) => {
        totalBalance +=
          walletBalances[`${item.coinSymbol.toLowerCase()}_balance`] *
          item.price.USD;
      });
      setVaultBalance(totalBalance);
    }
  }, [walletBalances, cryptoTableData]);

  useEffect(() => {
    if (affiliateBalance === '') {
      updateBrokerBalances();
    }
  }, []);

  useEffect(() => {
    if (vaultBalance && affiliateBalance !== '') {
      setNetWorkValuation(vaultBalance + affiliateBalance);
    }
  }, [vaultBalance, affiliateBalance]);

  useEffect(() => {
    setTimeout(() => {
      if (!isLoggedIn) {
        navigation.navigate('Brands', {screen: 'Feeds'});
      } else {
        setIsLoading(false);
      }
    }, 1000);
  }, [isLoggedIn]);

  useEffect(() => {
    (async () => {
      if (isLoggedIn) {
        const email = await AsyncStorageHelper.getLoginEmail();

        Axios.get(`${GX_API_ENDPOINT}/brokerage/get/broker/users/by/levels`, {
          params: {
            email,
          },
        })
          .then((resp) => {
            const {data} = resp;

            // console.log('Resp', data);

            if (data.status) {
              const levels = data.levels || [];

              let total = 0;

              levels.forEach((item) => {
                total = total + (item.count || 0);
              });

              setTotalUsers(total);
              setTotalDepth(levels?.length || 0);
              setNetworkData(levels);
            } else {
              setNetworkData([]);
            }
          })
          .catch((error) => {
            console.log('Error on getting Network data', error);
          });
      }
    })();
  }, [isLoggedIn]);

  const handleBack = async () => {
    if (navigation.canGoBack()) {
      navigation.goBack();
    } else {
      BackHandler.exitApp();
    }
    return true;
  };

  const onNextItemClick = (item) => {
    navigation.navigate('Details', {item});
  };

  const onInviteClick = () => {
    navigation.navigate('Invite');
  };

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      {isLoading ? (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      ) : (
        <>
          <View style={styles.coverContainer}>
            <View style={styles.imageContainer}>
              <TouchableOpacity
                onPress={() => setIsValue(false)}
                style={[
                  styles.actionButton,
                  isValue || {borderColor: ThemeData.APP_MAIN_COLOR},
                ]}>
                <Text
                  style={[
                    styles.actionText,
                    isValue || {fontFamily: ThemeData.FONT_SEMI_BOLD},
                  ]}>
                  Size
                </Text>
              </TouchableOpacity>
              <SharedElement id={'item.avatar'}>
                <FastImage
                  source={{uri: getUriImage(avatar)}}
                  // resizeMode="contain"
                  style={styles.profileImage}
                />
              </SharedElement>
              <TouchableOpacity
                onPress={() => setIsValue(true)}
                style={[
                  styles.actionButton,
                  isValue && {borderColor: ThemeData.APP_MAIN_COLOR},
                ]}>
                <Text
                  style={[
                    styles.actionText,
                    isValue && {fontFamily: ThemeData.FONT_SEMI_BOLD},
                  ]}>
                  Value
                </Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              onPress={() =>
                !isValue ? navigation.navigate('ChainView') : null
              }
              style={styles.chainViewButton}>
              <View style={styles.titleContainer}>
                <SharedElement id={'item.valuationTitle'}>
                  <Text style={styles.title}>
                    {isValue ? 'Your Network Valuation' : 'Your Network Size'}
                  </Text>
                </SharedElement>
                <Image
                  source={require('../assets/forward-arrow-icon.png')}
                  style={styles.forwardArrow}
                  resizeMode="contain"
                />
              </View>
              <SharedElement id={'item.valuation'}>
                {(isValue ? netWorkValuation : totalUsers) !== undefined ? (
                  <Text style={styles.valuation}>
                    {isValue
                      ? netWorkValuation === ''
                        ? 'Calculating'
                        : usdValueFormatter.format(netWorkValuation || 0)
                      : `${numberFormatter.format(totalUsers || 0)} Contacts`}
                  </Text>
                ) : (
                  <SkeltonItem
                    itemWidth={180}
                    itemHeight={30}
                    style={[styles.valuation]}
                  />
                )}
              </SharedElement>
            </TouchableOpacity>
          </View>
          <View style={[styles.totalDepthContainer]}>
            <ToolTipContent
              isVisible={isTooltipVisible}
              onClose={() => setIsTooltipVisible(false)}
              desc="DD Level Stands For Depth Differential Level And Signifies How Many Levels Away From You The Selected User Is. A DD Level Of Zero Means The User Is Directly Attached To You.">
              <TouchableOpacity
                style={[styles.toolTopButton]}
                onPress={() => setIsTooltipVisible(true)}>
                <Text style={styles.depthText}>Total Depth</Text>
                <Image
                  source={require('../assets/i-button-icon.png')}
                  resizeMode="contain"
                  style={[styles.tooltipIcon]}
                />
              </TouchableOpacity>
            </ToolTipContent>
            <ToolTipContent
              isVisible={isLevelTooltipVisible}
              onClose={() => setIsLevelTooltipVisible(false)}
              desc="On A Given DD Level You Will Have Any Number Of Users. That Is What Is Displayed Here As Contacts Per DD Level.">
              <TouchableOpacity
                style={[styles.toolTopButton]}
                onPress={() => setIsLevelTooltipVisible(true)}>
                <Image
                  source={require('../assets/i-button-icon.png')}
                  resizeMode="contain"
                  style={[styles.tooltipIcon]}
                />
                {totalDepth ? (
                  <Text style={styles.depthText}>{totalDepth} Levels</Text>
                ) : (
                  <SkeltonItem
                    itemWidth={80}
                    itemHeight={10}
                    style={[styles.name]}
                  />
                )}
              </TouchableOpacity>
            </ToolTipContent>
          </View>
          {networkData ? (
            <>
              <FlatList
                style={styles.list}
                showsVerticalScrollIndicator={false}
                data={networkData}
                keyExtractor={(item, index) => `${item.ddlevel}`}
                renderItem={({item, index}) => (
                  <TouchableOpacity
                    onPress={() => onNextItemClick(item)}
                    style={[
                      styles.listItem,
                      index + 1 === networkData.length && {
                        borderBottomColor: '#EBEBEB',
                        borderBottomWidth: 1,
                      },
                    ]}>
                    <SharedElement id={`item.${item.ddlevel}.level`}>
                      <Text style={styles.itemText}>
                        DD Level {item.ddlevel}
                      </Text>
                    </SharedElement>
                    <Text style={styles.itemText}>{item.count} Contacts</Text>
                  </TouchableOpacity>
                )}
                ListEmptyComponent={
                  <View style={styles.emptyContainer}>
                    <TouchableOpacity
                      style={[
                        styles.listItem,
                        {
                          borderBottomColor: '#EBEBEB',
                          borderBottomWidth: 1,
                        },
                      ]}>
                      <Text style={styles.itemText}>DD Level 0</Text>
                      <Text style={styles.itemText}>0 Contacts</Text>
                    </TouchableOpacity>
                    <Image
                      style={styles.upArrow}
                      source={require('../assets/next-forward-icon.png')}
                      resizeMode="contain"
                    />
                    <View style={{marginHorizontal: 30}}>
                      <Text style={styles.emptyText}>
                        Don’t worry we all start somewhere. It is super easy to
                        start building your network.
                      </Text>
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginTop: 5,
                        marginHorizontal: 30,
                      }}>
                      <Text style={styles.emptyText}>Click</Text>
                      <TouchableOpacity
                        onPress={onInviteClick}
                        style={styles.button}>
                        <Text style={styles.buttonText}>Here</Text>
                      </TouchableOpacity>
                      <Text style={styles.emptyText}>
                        to add your first contact.
                      </Text>
                    </View>
                  </View>
                }
              />
            </>
          ) : (
            <View style={styles.loadingContainer}>
              <LevelSkelton />
              <LevelSkelton />
            </View>
          )}
        </>
      )}
    </AppMainLayout>
  );
};

const LevelSkelton = () => (
  <View style={styles.listItem}>
    <SkeltonItem itemWidth={100} itemHeight={10} style={[styles.name]} />
    <SkeltonItem itemWidth={100} itemHeight={10} style={[styles.name]} />
  </View>
);

export default NetworkLevelScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  coverContainer: {
    alignItems: 'center',
    paddingVertical: 40,
  },
  imageContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
  },
  actionButton: {
    marginLeft: 'auto',
    marginRight: 'auto',
    borderRadius: 4,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingVertical: 5,
    width: 80,
    justifyContent: 'center',
    alignItems: 'center',
  },
  actionText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
  },
  profileImage: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 25,
  },
  title: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 15,
  },
  forwardArrow: {
    width: 14,
    height: 14,
    marginLeft: 10,
  },
  valuation: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 28,
    marginTop: 10,
  },
  list: {
    flex: 1,
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 30,
    paddingHorizontal: 35,
    borderTopColor: '#EBEBEB',
    borderTopWidth: 1,
  },
  itemText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
  },
  emptyText: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 15,
  },
  loadingContainer: {
    flex: 1,
  },
  button: {
    borderColor: '#08152D',
    borderWidth: 1,
    paddingHorizontal: 8,
    paddingVertical: 2,
    marginHorizontal: 5,
  },
  buttonText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 15,
  },
  upArrow: {
    marginLeft: 'auto',
    marginRight: 'auto',
    transform: [{rotate: '270deg'}],
    width: 20,
    height: 20,
    marginVertical: 35,
  },
  totalDepthContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#F1F4F6',
    alignItems: 'center',
  },
  depthText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 12,
    color: ThemeData.APP_MAIN_COLOR,
  },
  chainViewButton: {
    alignItems: 'center',
  },
  toolTopButton: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#F1F4F6',
    height: 30,
    paddingHorizontal: 35,
  },
  tooltipIcon: {
    width: 13,
    height: 13,
    marginHorizontal: 8,
  },
});
