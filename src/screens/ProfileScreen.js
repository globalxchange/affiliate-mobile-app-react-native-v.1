import {useNavigation, useRoute} from '@react-navigation/native';
import axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ActionBar from '../components/ActionBar';
import ProfileCover from '../components/ProfileComponents/ProfileCover';
import ProfileFrament from '../components/ProfileComponents/ProfileFrament';
import ProfileTab from '../components/ProfileComponents/ProfileTab';
import {GX_API_ENDPOINT, TELLER_API2_ENDPOINT} from '../configs';
import {AppContext} from '../contexts/AppContextProvider';
import AppMainLayout from '../layouts/AppMainLayout';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';

const ProfileScreen = () => {
  const {params} = useRoute();

  const {setActiveRoute} = useContext(AppContext);

  const [activeTab, setActiveTab] = useState('');
  const [userData, setUserData] = useState();
  const [followings, setFollowings] = useState();
  const [contacts, setContacts] = useState();
  const [isFragmentExpanded, setIsFragmentExpanded] = useState(false);

  useEffect(() => {
    (async () => {
      const loggedMail = await AsyncStorageHelper.getLoginEmail();
      const email = params?.userEmail || loggedMail;
      axios
        .get(`${GX_API_ENDPOINT}/user/details/get`, {
          params: {email},
        })
        .then(({data}) => {
          setUserData(data?.user || '');
        })

        .catch((error) => {
          console.log('getUserDetails Error', error);
        });

      axios
        .get(`${TELLER_API2_ENDPOINT}/banker/followingList`, {
          params: {email},
        })
        .then(({data}) => {
          setFollowings(data?.data);
        })
        .catch((error) => {
          console.log('Error getting followers list', error);
        });

      axios
        .get(`${GX_API_ENDPOINT}/brokerage/get/broker/users/by/levels`, {
          params: {
            email,
          },
        })
        .then((resp) => {
          const {data} = resp;

          // console.log('Resp', data);

          if (data.status) {
            const levels = data.levels || [];

            let total = 0;

            levels.forEach((item) => {
              total = total + (item.count || 0);
            });

            setContacts(total);
          } else {
            setContacts(0);
          }
        })
        .catch((error) => {
          console.log('Error on getting Network data', error);
        });
    })();
  }, [params]);

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      <ProfileCover
        userData={userData}
        followings={followings?.count}
        contacts={contacts}
        openBrands={() => setActiveTab({title: 'Brands'})}
        isFragmentExpanded={isFragmentExpanded}
        setUserData={setUserData}
        isLoggedUsed={!params?.userEmail}
      />
      <ProfileTab
        setActiveTab={setActiveTab}
        activeTab={activeTab}
        isFragmentExpanded={isFragmentExpanded}
        setIsFragmentExpanded={setIsFragmentExpanded}
      />
      <ProfileFrament
        activeTab={activeTab}
        userData={userData}
        followings={followings}
      />
    </AppMainLayout>
  );
};

export default ProfileScreen;

const styles = StyleSheet.create({});
