/* eslint-disable react-native/no-inline-styles */
import CheckBox from '@react-native-community/checkbox';
import {useNavigation, useRoute} from '@react-navigation/native';
import axios from 'axios';
import React, {useContext, useEffect, useRef, useState} from 'react';
import {
  FlatList,
  Image,
  Keyboard,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {Transition, Transitioning} from 'react-native-reanimated';
import {WToast} from 'react-native-smart-tip';
import ActionBar from '../components/ActionBar';
import FilledButton from '../components/FilledButton';
import InstaCryptoDefaultFeeEditor from '../components/InstaCryptoDefaultFeeEditor';
import LoadingAnimation from '../components/LoadingAnimation';
import ProfileAvatar from '../components/ProfileAvatar';
import {GX_API_ENDPOINT} from '../configs';
import ThemeData from '../configs/ThemeData';
import {AppContext} from '../contexts/AppContextProvider';
import AppMainLayout from '../layouts/AppMainLayout';
import SearchLayout from '../layouts/SearchLayout';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';

const InstaCryptoCustomPairFeesScreen = () => {
  const navigation = useNavigation();

  const {params} = useRoute();

  const {user, isDefault} = params;

  const {cryptoTableData, setIsInstaCryptoFeeEditorHide} = useContext(
    AppContext,
  );

  const transitionViewRef = useRef();
  const toListRef = useRef();
  const fromListRef = useRef();

  const [fromList, setFromList] = useState();
  const [toList, setToList] = useState();
  const [selectedFrom, setSelectedFrom] = useState();
  const [selectedTo, setSelectedTo] = useState();
  const [defaultFee, setDefaultFee] = useState('');
  const [customFee, setCustomFee] = useState('');
  const [isInputFocused, setIsInputFocused] = useState(false);
  const [isSetAsDefault, setIsSetAsDefault] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isSearchOpen, setIsSearchOpen] = useState(false);
  const [searchText, setSearchText] = useState('');
  const [isToSearch, setIsToSearch] = useState(false);

  // console.log('cryptoTableData', cryptoTableData);

  useEffect(() => {
    navigation.addListener('blur', onNavigateBack);
    return () => {
      navigation.removeListener('blur', onNavigateBack);
    };
  }, []);

  useEffect(() => {
    setDefaultFee((user?.default_fee || 0).toString());
  }, [user]);

  useEffect(() => {
    if (!isSearchOpen) {
      setSearchText('');
    }
  }, [isSearchOpen]);

  useEffect(() => {
    if (cryptoTableData) {
      const filtered = [];

      cryptoTableData.map((item) => {
        if (item.coinSymbol !== selectedTo?.coinSymbol) {
          filtered.push({
            ...item,
            name: item.coinName,
            profile_img: item.coinImage,
            email: item.coinSymbol,
          });
        }
      });

      setFromList(filtered);
      if (!selectedFrom) {
        setSelectedFrom(filtered[0]);
      }
    }
  }, [cryptoTableData, selectedTo]);

  useEffect(() => {
    if (cryptoTableData) {
      const filtered = [];

      cryptoTableData.map((item) => {
        if (item.coinSymbol !== selectedFrom?.coinSymbol) {
          filtered.push({
            ...item,
            name: item.coinName,
            profile_img: item.coinImage,
            email: item.coinSymbol,
          });
        }
      });

      setToList(filtered);
      if (!selectedTo) {
        setSelectedTo(filtered[1]);
      }
    }
  }, [cryptoTableData, selectedFrom]);

  const onNavigateBack = () => {
    setIsInstaCryptoFeeEditorHide(false);
  };

  const transition = (
    <Transition.Together>
      <Transition.Change interpolation="linear" />
    </Transition.Together>
  );

  const onSubmitHandler = async () => {
    const fee = parseFloat(customFee);

    if (!fee) {
      return WToast.show({
        data: 'Please Enter A Valid Fee',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);
    Keyboard.dismiss();

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    const postData = {
      email,
      token,
      customer_email: user?.email,
      buy_coin: selectedTo?.coinSymbol,
      sell_coin: selectedFrom?.coinSymbol,
      fee_percentage: fee,
      default_fee: isSetAsDefault,
    };

    axios
      .post(
        `${GX_API_ENDPOINT}/coin/vault/service/set/fees/user/per/customer`,
        postData,
      )
      .then(({data}) => {
        // console.log('updating Fee', data);

        if (data.status) {
          navigation.goBack();
        } else {
          WToast.show({data: data.message, position: WToast.position.TOP});
        }
      })
      .catch((error) => {
        console.log('Error on updating Fee', error);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const onSearchItemClick = (_, itemObj) => {
    setIsSearchOpen(false);
    if (isToSearch) {
      setSelectedTo(itemObj);

      toListRef.current?.scrollToItem({animated: true, item: itemObj});
    } else {
      setSelectedFrom(itemObj);
      fromListRef.current?.scrollToItem({animated: true, item: itemObj});
    }
  };

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      {isDefault ? (
        <InstaCryptoDefaultFeeEditor user={user} setIsLoading={setIsLoading} />
      ) : (
        <Transitioning.View
          ref={transitionViewRef}
          style={[styles.container]}
          transition={transition}>
          <Text style={styles.currencyPickerTitle}>
            Adding Custom Fees For A New Pair For
          </Text>
          <View style={[styles.userHeader]}>
            <ProfileAvatar
              name={user?.name}
              avatar={user?.profile_img}
              size={40}
            />
            <View style={{flex: 1, marginLeft: 10}}>
              <Text style={styles.userName}>{user?.name}</Text>
              <Text
                numberOfLines={1}
                adjustsFontSizeToFit
                style={styles?.userEmail}>
                {user?.email}
              </Text>
            </View>
          </View>
          <View
            style={[
              styles.currencyPickerContainer,
              {display: isInputFocused ? 'none' : 'flex'},
            ]}>
            <View style={styles.groupHeader}>
              <Text style={[styles.currencyPickerTitle, {marginBottom: 0}]}>
                Select The Currency They Are Selling?
              </Text>
              <TouchableOpacity
                style={styles.searchButton}
                onPress={() => {
                  setIsSearchOpen(true);
                  setIsToSearch(false);
                }}>
                <Image
                  style={styles.searchIcon}
                  resizeMode="contain"
                  source={require('../assets/search-modern.png')}
                />
              </TouchableOpacity>
            </View>
            <FlatList
              ref={fromListRef}
              horizontal
              showsHorizontalScrollIndicator={false}
              data={fromList}
              onScrollToIndexFailed={() => {}}
              keyExtractor={(item) => item.coinSymbol}
              renderItem={({item}) => (
                <TouchableOpacity
                  style={[
                    styles.currencyItem,
                    selectedFrom?.coinSymbol === item.coinSymbol && {
                      borderColor: '#9A9A9A',
                    },
                  ]}
                  onPress={() => setSelectedFrom(item)}>
                  <Image
                    source={{uri: item.coinImage}}
                    resizeMode="contain"
                    style={styles.coinImage}
                  />
                  <Text style={styles.coinName}>{item.coinSymbol}</Text>
                </TouchableOpacity>
              )}
            />
          </View>
          <View
            style={[
              styles.currencyPickerContainer,
              {display: isInputFocused ? 'none' : 'flex'},
            ]}>
            <View style={styles.groupHeader}>
              <Text style={[styles.currencyPickerTitle, {marginBottom: 0}]}>
                Select The Currency They Are Buying?
              </Text>
              <TouchableOpacity
                style={styles.searchButton}
                onPress={() => {
                  setIsSearchOpen(true);
                  setIsToSearch(true);
                }}>
                <Image
                  style={styles.searchIcon}
                  resizeMode="contain"
                  source={require('../assets/search-modern.png')}
                />
              </TouchableOpacity>
            </View>
            <FlatList
              ref={toListRef}
              horizontal
              showsHorizontalScrollIndicator={false}
              onScrollToIndexFailed={() => {}}
              data={toList}
              keyExtractor={(item) => item.coinSymbol}
              renderItem={({item}) => (
                <TouchableOpacity
                  style={[
                    styles.currencyItem,
                    selectedTo?.coinSymbol === item.coinSymbol && {
                      borderColor: '#9A9A9A',
                    },
                  ]}
                  onPress={() => setSelectedTo(item)}>
                  <Image
                    source={{uri: item.coinImage}}
                    resizeMode="contain"
                    style={styles.coinImage}
                  />
                  <Text style={styles.coinName}>{item.coinSymbol}</Text>
                </TouchableOpacity>
              )}
            />
          </View>
          <View
            style={[
              styles.currencyPickerContainer,
              {display: isInputFocused ? 'none' : 'flex'},
            ]}>
            <Text style={styles.currencyPickerTitle}>
              Your Current Fee For {selectedTo?.coinSymbol}/
              {selectedFrom?.coinSymbol} For {user?.name}
            </Text>
            <View style={styles.inputContainer}>
              <TextInput
                placeholder="You Default Fee"
                placeholderTextColor="#9A9A9A"
                style={styles.input}
                value={defaultFee}
                editable={false}
                onChangeText={(text) => setDefaultFee(text)}
              />
              <Text style={styles.inputCurrency}>%</Text>
            </View>
          </View>
          <View style={styles.currencyPickerContainer}>
            <Text style={styles.currencyPickerTitle}>
              The New Fee For {selectedTo?.coinSymbol}/
              {selectedFrom?.coinSymbol} For {user?.name}
            </Text>
            <View style={styles.inputContainer}>
              <TextInput
                placeholder="0.00"
                placeholderTextColor="#9A9A9A"
                style={styles.input}
                value={customFee}
                onChangeText={(text) => setCustomFee(text)}
                onFocus={() => {
                  setIsInputFocused(true);
                  transitionViewRef.current.animateNextTransition();
                }}
                onBlur={() => {
                  setIsInputFocused(false);
                  transitionViewRef.current.animateNextTransition();
                }}
              />
              <Text style={styles.inputCurrency}>%</Text>
            </View>
          </View>
          <View
            style={[
              styles.checkBoxContainer,
              {display: isInputFocused ? 'flex' : 'none'},
            ]}>
            {isInputFocused ? (
              <CheckBox
                value={isSetAsDefault}
                onValueChange={(newValue) => setIsSetAsDefault(newValue)}
              />
            ) : null}
            <Text
              style={styles.checkBoxText}
              onPress={() => setIsSetAsDefault(!isSetAsDefault)}>
              Make This The Default Fee For All Currency Pairs Which Are Not
              Customized
            </Text>
          </View>
          <FilledButton
            onPress={onSubmitHandler}
            title="Submit"
            style={{
              flex: 0,
              width: 150,
              marginTop: 20,
              display: isInputFocused ? 'flex' : 'none',
            }}
          />
          {isSearchOpen && (
            <View style={[StyleSheet.absoluteFill, {backgroundColor: 'white'}]}>
              <SearchLayout
                disableFilter
                onBack={() => setIsSearchOpen(false)}
                placeholder="Search Asset"
                value={searchText}
                setValue={setSearchText}
                showUserList
                list={isToSearch ? toList : fromList}
                keyboardOffset={Platform.OS === 'ios' ? 100 : 0}
                onSubmit={onSearchItemClick}
              />
            </View>
          )}
        </Transitioning.View>
      )}
      {isLoading && (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      )}
    </AppMainLayout>
  );
};

export default InstaCryptoCustomPairFeesScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 30,
    paddingVertical: 20,
    backgroundColor: 'white',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  brandIconContainer: {
    width: 170,
    height: 40,
    alignItems: 'flex-start',
  },
  headerImage: {
    width: 170,
    height: 40,
    aspectRatio: 3,
  },
  breadCrumbsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 30,
  },
  prevBreadCrumb: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    color: '#464B4E',
  },
  breadCrumbArrow: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
  },
  currentBreadCrumb: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    textDecorationLine: 'underline',
    fontSize: 12,
    color: '#464B4E',
  },
  currencyPickerContainer: {
    marginTop: 40,
  },
  currencyPickerTitle: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    marginBottom: 20,
  },
  currencyItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginRight: 10,
  },
  coinImage: {
    width: 18,
    height: 18,
    marginRight: 5,
    borderRadius: 9,
  },
  coinName: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 25,
  },
  inputCurrency: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
  },
  input: {
    flex: 1,
    height: 40,
    borderRightColor: ThemeData.BORDER_COLOR,
    borderRightWidth: 1,
    marginRight: 25,
    fontFamily: ThemeData.FONT_NORMAL,
  },
  checkBoxContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
  },
  checkBoxText: {
    marginLeft: 10,
    fontFamily: ThemeData.FONT_NORMAL,
    color: '#001D41',
    fontSize: 12,
    flex: 1,
  },
  userHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  userName: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 16,
    color: ThemeData.APP_MAIN_COLOR,
    marginBottom: 3,
    textTransform: 'capitalize',
  },
  userEmail: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    color: '#9A9A9A',
  },
  loadingContainer: {
    ...StyleSheet.absoluteFill,
    justifyContent: 'center',
    backgroundColor: 'white',
    zIndex: 10,
  },
  groupHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20,
  },
  searchButton: {
    width: 30,
    height: 30,
    padding: 8,
    marginLeft: 20,
  },
  searchIcon: {
    flex: 1,
    height: null,
    width: null,
  },
});
