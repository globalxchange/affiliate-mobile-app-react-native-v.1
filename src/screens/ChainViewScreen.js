/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useRef, useState} from 'react';
import {
  FlatList,
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ActionBar from '../components/ActionBar';
import AppMainLayout from '../layouts/AppMainLayout';
import AppStatusBar from '../components/AppStatusBar';
import {useNavigation, useRoute} from '@react-navigation/native';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';
import Axios from 'axios';
import {APP_CODE, GX_API_ENDPOINT} from '../configs';
import ThemeData from '../configs/ThemeData';
import LoadingAnimation from '../components/LoadingAnimation';
import ChainViewItem from '../components/ChainViewItem';
import SearchLayout from '../layouts/SearchLayout';
import {getPlaceholderText} from '../utils';
import EarningsModal from '../components/PeopleCarousel/EarningsModal';
import LongLoading from '../components/LongLoading';

const ChainViewScreen = () => {
  const {params} = useRoute();

  const {goBack, push, navigate} = useNavigation();

  const deptListRef = useRef();
  const axiosTokenRef = useRef();

  const [userData, setUserData] = useState();
  const [depthData, setDepthData] = useState();
  const [allUsers, setAllUsers] = useState();
  const [loadingIndex, setLoadingIndex] = useState();

  const [activeFilter, setActiveFilter] = useState(INPUT_OPTIONS[0]);
  const [lastSelectedUser, setLastSelectedUser] = useState();
  const [isMoneyViewOpen, setIsMoneyViewOpen] = useState(false);

  const [isPopupOpen, setIsPopupOpen] = useState(false);
  const [selectedUser, setSelectedUser] = useState('');

  const [searchList, setSearchList] = useState('');
  const [isSearchOpen, setIsSearchOpen] = useState(false);
  const [searchInput, setSearchInput] = useState('');
  const [searchedLevel, setSearchedLevel] = useState(0);
  const [isOpenNewUser, setIsOpenNewUser] = useState(true);
  const [searchedUser, setSearchedUser] = useState(null);

  useEffect(() => {
    Axios.get(`${GX_API_ENDPOINT}/gxb/apps/users/get`, {
      params: {app_code: APP_CODE},
    })
      .then((resp) => {
        const {data} = resp;
        // console.log('Users List', data.users);
        setAllUsers(data.users || []);
      })
      .catch((error) => {
        console.log('Error on getting AppList', error);
      });

    // Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/users/holdings/data/get`, {
    //   params: {app_code: APP_CODE, limit: 999999},
    // })
    //   .then(({data}) => {
    //     const all = data?.users || [];
    //     const allData = all?.map((user) => user?.userData);

    //     setAllUsers(allData);
    //   })
    //   .catch((error) => {
    //     setAllUsers([]);
    //     console.log('Error getting users', error);
    //   });
    // return () => {};
  }, []);

  useEffect(() => {
    if (!isSearchOpen) {
      setSearchInput('');
    }
  }, [isSearchOpen]);

  useEffect(() => {
    (async () => {
      let email;
      if (params?.email) {
        email = params?.email;
      } else {
        email = await AsyncStorageHelper.getLoginEmail();
      }

      Axios.get(`${GX_API_ENDPOINT}/brokerage/com/earnings/get`, {
        params: {
          email,
          orderByLevel: true,
        },
      })
        .then((resp) => {
          const {data} = resp;

          // console.log('Resp', data);

          if (data.status) {
            setUserData(data.user);
            const levels = data.levelsData || [];

            const depths = [];

            levels.forEach((item, index) => {
              if (index === 0) {
                depths.push({ddLevel: item.level, list: item.users});
              } else {
                depths.push({ddLevel: item.level, list: undefined});
              }
            });

            setDepthData(depths);
          } else {
            setUserData(data?.userData || '');
            setDepthData([{ddLevel: 0, list: []}]);
          }
        })
        .catch((error) => {
          console.log('Error on getting Network data', error);
        });
    })();
  }, [params?.email]);

  const getUserDD = (email, index) => {
    if (axiosTokenRef.current) {
      axiosTokenRef.current.cancel('Cancelled');
      // setIsLoading(false);
    }

    axiosTokenRef.current = Axios.CancelToken.source();

    setLoadingIndex(index + 1);

    Axios.get(`${GX_API_ENDPOINT}/brokerage/com/earnings/get`, {
      params: {
        email,
        orderByLevel: true,
      },
      cancelToken: axiosTokenRef.current.token,
    })
      .then(({data}) => {
        // console.log('Resp User', data);
        const levels = data?.levelsData || [];

        const firstDepth = levels[0];

        const ddLevelCopy = [...depthData];

        const ddItem = ddLevelCopy[index + 1];

        if (ddItem) {
          ddLevelCopy[index + 1] = {...ddItem, list: firstDepth?.users || []};

          ddLevelCopy.forEach((item, i) => {
            if (i > index + 1) {
              ddLevelCopy[i] = {...item, list: undefined};
            }
          });

          setDepthData(ddLevelCopy);

          if (deptListRef.current) {
            deptListRef.current.scrollToIndex({index: index});
          }
        }
      })
      .catch((error) => {
        console.log('Error on getting Network data of the user', error);
      })
      .finally(() => setLoadingIndex());
  };

  const clearToIndex = (index) => {
    const ddLevelCopy = [...depthData];

    ddLevelCopy.forEach((item, i) => {
      if (i > index) {
        ddLevelCopy[i] = {...item, list: undefined};
      }
    });
    setDepthData(ddLevelCopy);
  };

  const onPopupOpen = (userEmail) => {
    setSelectedUser(userEmail);
    setIsPopupOpen(true);
  };

  const openNewUser = (email, userObj) => {
    push('ChainView', {email});
  };

  const selectUserInLevel = (email, userObj) => {
    setLastSelectedUser(userObj);
    setSearchedUser(userObj);
    getUserDD(email, searchedLevel);
    setIsSearchOpen(false);
  };

  return (
    <AppMainLayout isBottomNav>
      <AppStatusBar backgroundColor="white" barStyle={'dark-content'} />
      <ActionBar />
      <View style={{flex: 1}}>
        {isSearchOpen ? (
          <SearchLayout
            value={searchInput}
            setValue={setSearchInput}
            onBack={() => setIsSearchOpen(false)}
            showUserList
            onSubmit={isOpenNewUser ? openNewUser : selectUserInLevel}
            placeholder="Search User"
            keyboardOffset={Platform.OS === 'ios' ? 100 : 90}
            list={searchList || []}
            filters={INPUT_OPTIONS}
            setSelectedFilter={setActiveFilter}
            selectedFilter={activeFilter}
          />
        ) : (
          <>
            {userData ? (
              <>
                <View style={styles.userContainer}>
                  {userData?.profile_img ? (
                    <Image
                      style={styles.userImage}
                      resizeMode="contain"
                      source={{uri: userData?.profile_img}}
                    />
                  ) : (
                    <View style={styles.profilePlaceholder}>
                      <Text style={styles.placeholderText}>
                        {getPlaceholderText(userData?.name)}
                      </Text>
                    </View>
                  )}
                  <View style={styles.userDetail}>
                    <Text numberOfLines={1} style={styles.userName}>
                      {userData?.name}
                    </Text>
                    <Text numberOfLines={1} style={styles.userEmail}>
                      {userData?.email}
                    </Text>
                  </View>
                  <TouchableOpacity
                    style={[
                      styles.headerAction,
                      isMoneyViewOpen && {
                        borderColor: ThemeData.APP_MAIN_COLOR,
                      },
                    ]}
                    onPress={() => setIsMoneyViewOpen(!isMoneyViewOpen)}>
                    <Image
                      style={styles.backButton}
                      source={require('../assets/cash-stack-icon.png')}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      setSearchList(allUsers);
                      setIsOpenNewUser(true);
                      setIsSearchOpen(true);
                    }}
                    style={styles.headerAction}>
                    <Image
                      style={styles.backButton}
                      source={require('../assets/search-icon-colorful.png')}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                </View>
                {depthData ? (
                  <View style={styles.depthListContainer}>
                    <FlatList
                      ref={deptListRef}
                      onScrollToIndexFailed={() => {}}
                      showsVerticalScrollIndicator={false}
                      data={depthData}
                      keyExtractor={(item) => item?.ddLevel?.toString()}
                      renderItem={({item, index}) => (
                        <ChainViewItem
                          isMoneyViewOpen={isMoneyViewOpen}
                          level={item?.ddLevel}
                          userList={item?.list}
                          onUserSelected={getUserDD}
                          isLoading={loadingIndex === item?.ddLevel}
                          headerDesc={
                            index === 0
                              ? `Select One Of ${
                                  params?.email
                                    ? `${userData?.name?.split(' ')[0]}'s`
                                    : 'Your'
                                } Directs`
                              : `Select One Of ${
                                  lastSelectedUser?.name?.split(' ')[0]
                                }’s Directs`
                          }
                          setLastSelectedUser={setLastSelectedUser}
                          clearToIndex={clearToIndex}
                          emptyMessage={
                            index === 0
                              ? `Sorry, But ${
                                  params?.email
                                    ? `${userData?.name?.split(' ')[0]} Has`
                                    : 'You Have'
                                } No Directs`
                              : `Sorry, But ${
                                  lastSelectedUser?.name?.split(' ')[0]
                                } Has No Directs`
                          }
                          onTreeClick={() =>
                            navigate('People', {
                              ddLevel: item?.ddLevel || 0,
                              userEmail: params?.email,
                            })
                          }
                          onPopupOpen={onPopupOpen}
                          onSearchOpen={(list, level) => {
                            setSearchList(list);
                            setIsOpenNewUser(false);
                            setSearchedLevel(level);
                            setIsSearchOpen(true);
                          }}
                          searchedUser={
                            searchedLevel === index ? searchedUser : null
                          }
                        />
                      )}
                    />
                  </View>
                ) : null}
              </>
            ) : (
              <LongLoading loadingItemName="Your ChainView" />
            )}
          </>
        )}
      </View>
      <EarningsModal
        isOpen={isPopupOpen}
        onClose={() => setIsPopupOpen(false)}
        selectedCustomer={selectedUser}
        userEmail={params?.email}
      />
    </AppMainLayout>
  );
};

export default ChainViewScreen;

const styles = StyleSheet.create({
  userContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 12,
    marginVertical: 25,
  },
  userImage: {
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: 'white',
  },
  userDetail: {
    marginLeft: 10,
    flex: 1,
    marginRight: 20,
  },
  userName: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 20,
    color: ThemeData.APP_MAIN_COLOR,
  },
  userEmail: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    color: ThemeData.APP_MAIN_COLOR,
  },
  searchButton: {
    width: 30,
    height: 30,
    padding: 4,
  },
  searchIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  depthListContainer: {
    flex: 1,
  },
  headerContainer: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    alignItems: 'center',
    marginTop: 30,
  },
  headerAction: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    width: 40,
    height: 40,
    padding: 10,
    marginLeft: 10,
  },
  backButton: {
    flex: 1,
    height: null,
    width: null,
  },
  header: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 24,
    color: ThemeData.APP_MAIN_COLOR,
  },
  subHeader: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 11,
    color: ThemeData.APP_MAIN_COLOR,
  },
  emptyContainer: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 18,
    paddingHorizontal: 30,
  },
  profilePlaceholder: {
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    justifyContent: 'center',
    alignItems: 'center',
  },
  placeholderText: {
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    textTransform: 'uppercase',
  },
});

const INPUT_OPTIONS = [
  {title: 'Email', paramKey: 'email'},
  {title: 'Name', paramKey: 'name'},
  {title: 'Username', paramKey: 'username'},
];
