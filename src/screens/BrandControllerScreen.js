/* eslint-disable react-native/no-inline-styles */
import {useNavigation, useRoute} from '@react-navigation/native';
import React, {useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import VectorIcons from '../assets/VectorIcons';
import ActionBar from '../components/ActionBar';
import BrandController from '../components/BrandController';
import ThemeData from '../configs/ThemeData';
import AppMainLayout from '../layouts/AppMainLayout';
import PopupLayout from '../layouts/PopupLayout';

const BrandControllerScreen = () => {
  const {goBack, navigate} = useNavigation();

  const {params} = useRoute();

  const [isDescMoreOpen, setIsDescMoreOpen] = useState(false);

  const {selectedBrand} = params;

  const color = selectedBrand?.color || '#08152D';

  const desc = `Companies that opt into the ${selectedBrand.title} CompPlan allow their Affiliates to dictate and retain the exchange fees from`;

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      <View style={[styles.container]}>
        <View style={styles.headerContainer}>
          <View style={styles.brandIconContainer}>
            <Image
              source={selectedBrand.halfIcon}
              resizeMode="contain"
              style={styles.headerImage}
            />
            <Text style={styles.brandTitle}>{selectedBrand.title}</Text>
          </View>
          <TouchableOpacity onPress={goBack} style={styles.actionButton}>
            <VectorIcons.CloseIcon />
          </TouchableOpacity>
        </View>
        {/* <Text
          numberOfLines={2}
          onPress={() => setIsDescMoreOpen(!isDescMoreOpen)}
          style={styles.brandDesc}>
          {desc.substring(0, 100)}
          <Text style={styles.readMore}> (ReadMore)</Text>
        </Text> */}
        <PopupLayout
          autoHeight
          isOpen={isDescMoreOpen}
          headerTitle="About The Brand"
          onClose={() => setIsDescMoreOpen(false)}>
          <Text style={styles.popupText}>{desc}</Text>
        </PopupLayout>
        <BrandController selectedBrand={selectedBrand} />
      </View>
    </AppMainLayout>
  );
};

export default BrandControllerScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 25,
  },
  brandIconContainer: {
    width: 234,
    height: 33.04,
    flexDirection: 'row',
    alignItems: 'center',
    // justifyContent: 'flex-start',
    // backgroundColor:"yellow"
  },
  brandTitle: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 22,
    // lineHeight: 22,
    fontStyle: 'normal',
    color: '#383C41',
    marginLeft: 7.99,
  },
  headerImage: {
    width: 33.11,
    height: 33.04,

    // aspectRatio: 3,
  },
  actionButton: {
    width: 15,
    height: 15,
  },
  actionIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  brandDesc: {
    color: '#08152D',
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 11,
    marginTop: 30,
    lineHeight: 20,
    marginBottom: 30,
  },
  readMore: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  brandAction: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    padding: 25,
    marginBottom: 20,
  },
  brandActionIcon: {
    width: 25,
    height: 25,
    marginRight: 15,
  },
  brandActionText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  earningHeader: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 22,
    color: ThemeData.APP_MAIN_COLOR,
  },
  simulatorHeader: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
  },
  listContainer: {
    flex: 1,
    marginTop: 20,
    marginBottom: -30,
  },
  listItem: {
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginBottom: 15,
    height: 50,
    alignItems: 'center',
  },
  listLabel: {
    width: '40%',
    paddingHorizontal: 20,
    fontFamily: ThemeData.FONT_NORMAL,
    color: '#9A9A9A',
  },
  listValue: {
    flex: 1,
    paddingHorizontal: 20,
    fontFamily: ThemeData.FONT_NORMAL,
    color: '#9A9A9A',
  },
  divider: {
    height: '100%',
    width: 1,
    backgroundColor: ThemeData.BORDER_COLOR,
  },
  popupText: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 14,
    marginBottom: 20,
    lineHeight: 22,
  },
});
