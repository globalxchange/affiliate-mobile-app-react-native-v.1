import React, {useState, useEffect, useContext} from 'react';
import {StyleSheet, Text, Dimensions, Image} from 'react-native';
import {BarCodeScanner} from 'expo-barcode-scanner';
import {useNavigation} from '@react-navigation/native';
import {AppContext} from '../contexts/AppContextProvider';

const {width} = Dimensions.get('window');
const qrSize = width * 0.7;

const QRScannerScreen = () => {
  const navigation = useNavigation();

  const {withdrawAddress, setWithdrawAddress} = useContext(AppContext);

  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);

  useEffect(() => {
    (async () => {
      await getPermision();
    })();
  }, []);

  const getPermision = async () => {
    const {status} = await BarCodeScanner.requestPermissionsAsync();
    setHasPermission(status === 'granted');
  };

  const handleBarCodeScanned = ({type, data}) => {
    setScanned(true);
    setWithdrawAddress(data);
    closeScanner();
  };

  const closeScanner = () => {
    navigation.pop();
  };

  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  return (
    <BarCodeScanner
      barCodeTypes={[BarCodeScanner.Constants.BarCodeType.qr]}
      onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
      style={[StyleSheet.absoluteFill, styles.container]}>
      <Text style={styles.description}>Scan your QR code</Text>
      <Image style={styles.qr} source={require('../assets/qr-frame.png')} />
      <Text onPress={closeScanner} style={styles.cancel}>
        Cancel
      </Text>
    </BarCodeScanner>
  );
};

export default QRScannerScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'black',
  },
  qr: {
    marginTop: '20%',
    marginBottom: '20%',
    width: qrSize,
    height: qrSize,
  },
  description: {
    fontSize: width * 0.06,
    marginTop: '12%',
    textAlign: 'center',
    width: '70%',
    fontFamily: 'Montserrat-Bold',
    color: 'white',
  },
  cancel: {
    fontSize: width * 0.05,
    textAlign: 'center',
    width: '70%',
    marginBottom: 40,
    fontFamily: 'Montserrat-Bold',
    color: 'white',
    paddingVertical: 15,
  },
});
