import React, {useState} from 'react';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import ActionBar from '../components/ActionBar';
import ComingSoon from '../components/ComingSoon';
import ExploreCarousel from '../components/ExploreCarousel';
import ExplorePeopleList from '../components/ExplorePeopleList';
import AppMainLayout from '../layouts/AppMainLayout';

const ExploreScreen = () => {
  const [activeTab, setActiveTab] = useState();

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      {/* <View style={styles.container}>
        <ExploreCarousel setActiveCarousel={setActiveTab} />
        <ScrollView showsVerticalScrollIndicator={false}>
          <ExplorePeopleList title="Trending" />
          <ExplorePeopleList title="Newest" />
          <ExplorePeopleList
            title={
              activeTab?.title === 'Assistants' ? 'By Specialty' : 'By Brand'
            }
          />
        </ScrollView>
      </View> */}
      <ComingSoon />
    </AppMainLayout>
  );
};

export default ExploreScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: -35,
  },
});
