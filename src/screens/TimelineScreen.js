import React, {useContext, useEffect, useState} from 'react';
import {BackHandler, StyleSheet, Text, View} from 'react-native';
import {useNavigation, useRoute} from '@react-navigation/native';
import {AppContext} from '../contexts/AppContextProvider';
import AppMainLayout from '../layouts/AppMainLayout';
import AppStatusBar from '../components/AppStatusBar';
import ActionBar from '../components/ActionBar';
import TimeLine from '../components/TimeLine';
import Axios from 'axios';
import FloatingButton from '../components/FloatingButton';
import LoadingAnimation from '../components/LoadingAnimation';
import {SharedElement} from 'react-navigation-shared-element';

const TimelineScreen = () => {
  const navigation = useNavigation();

  const route = useRoute();

  const {setActiveRoute, pathData} = useContext(AppContext);

  const [currentPath, setCurrentPath] = useState();
  const [currentStep, setCurrentStep] = useState('');
  const [currentInstitution, setCurrentInstitution] = useState('');

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBack);

    navigation.addListener('focus', onScreenFocus);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBack);

      navigation.removeListener('focus', onScreenFocus);
    };
  }, []);

  useEffect(() => {
    if (pathData) {
      const {data} = route.params;

      const pathId = data.path_id;

      const selectedPath = pathData.find((item) => item.path_id === pathId);

      if (selectedPath) {
        const steps = selectedPath.total_steps;
        const stepsArray = [];

        if (steps) {
          const entries = Object.entries(steps);

          let found = false;

          for (const [key, stepData] of entries) {
            stepsArray.push({key, ...stepData});
            if (stepData.status === data.status && !found) {
              setCurrentStep(stepData);
            }
          }
        }

        setCurrentPath({...selectedPath, stepsArray});

        const instituteId = selectedPath.institution_id;

        Axios.get('https://accountingtool.apimachine.com/getlist-of-institutes')
          .then((resp) => {
            const instData = resp.data;
            if (instData.status) {
              instData.data.forEach((item) => {
                if (item._id === instituteId) {
                  setCurrentInstitution(item);
                  return;
                }
              });
            }
          })
          .catch((error) => {
            console.log('Error on getting institute details', error);
          });
      }
    }
  }, [pathData, route]);

  const handleBack = () => {
    navigation.goBack();
    return true;
  };

  const onScreenFocus = (paylod) => {
    setActiveRoute('Earn');
  };

  return (
    <AppMainLayout>
      <SharedElement id={'item.appBar'}>
        <ActionBar />
      </SharedElement>
      <View style={styles.container}>
        {currentPath === undefined ? (
          <View style={styles.errorContainer}>
            <LoadingAnimation />
          </View>
        ) : currentPath === null ? (
          <View style={styles.errorContainer}>
            <Text style={styles.errorText}>This Payment Path Was Deleted</Text>
          </View>
        ) : (
          <TimeLine
            currentPath={currentPath}
            currentStep={currentStep}
            currentInstitution={currentInstitution}
          />
        )}
      </View>
      <FloatingButton
        onPress={navigation.goBack}
        icon={require('../assets/cancel-icon-white.png')}
      />
    </AppMainLayout>
  );
};

export default TimelineScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  errorContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  errorText: {
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
    fontSize: 18,
    textAlign: 'center',
  },
});
