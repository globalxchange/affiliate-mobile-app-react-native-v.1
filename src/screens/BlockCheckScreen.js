/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useEffect, useState} from 'react';
import {FlatList, Image, StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import ActionBar from '../components/ActionBar';
import AppStatusBar from '../components/AppStatusBar';
import {GX_API_ENDPOINT} from '../configs';
import {AppContext} from '../contexts/AppContextProvider';
import AppMainLayout from '../layouts/AppMainLayout';
import {formatterHelper, timeParserExpanded, usdValueFormatter} from '../utils';
import Axios from 'axios';
import * as WebBrowser from 'expo-web-browser';
import LoadingAnimation from '../components/LoadingAnimation';

const BlockCheckScreen = () => {
  const {setIsBlockSheetOpen} = useContext(AppContext);

  const [isCash, setIsCash] = useState(false);
  const [cashChecks, setCashChecks] = useState();
  const [signChecks, setSignChecks] = useState();

  useEffect(() => {
    Axios.get(
      `${GX_API_ENDPOINT}/coin/vault/service/blockcheck/request/get`,
      {},
    )
      .then((resp) => {
        const {data} = resp;

        // console.log('BlockCheckScreen', data);

        if (data.status) {
          const list = data.bcrs || [];

          const cash = [];
          const signs = [];

          list.forEach((item) => {
            if (item.bcr_type === 'receiver') {
              cash.push(item);
            } else {
              signs.push(item);
            }
          });

          setCashChecks(cash);
          setSignChecks(signs);
        }
      })
      .catch((error) => {});
  }, []);

  return (
    <AppMainLayout>
      <AppStatusBar backgroundColor="#08152D" barStyle="light-content" />
      <ActionBar />
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <Image
            source={require('../assets/block-check-full-logo.png')}
            style={styles.headerIcon}
            resizeMode="contain"
          />
          <Text style={styles.headerText}>
            The Safest Way To Send Crypto To An External Wallet
          </Text>
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              onPress={() =>
                WebBrowser.openBrowserAsync('https://blockcheck.io/')
              }
              style={styles.buttonOutlined}>
              <Image
                source={require('../assets/default-breadcumb-icon/transact.png')}
                resizeMode="contain"
                style={styles.buttonIcon}
              />
              <Text style={styles.buttonOutlinedText}>Find</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setIsBlockSheetOpen(true)}
              style={[styles.buttonOutlined, {marginLeft: 10}]}>
              <Image
                source={require('../assets/block-check-create.png')}
                resizeMode="contain"
                style={styles.buttonIcon}
              />
              <Text style={styles.buttonOutlinedText}>Create</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.listHeader}>
          <Text style={styles.title}>{isCash ? 'Cash' : 'Sign'} Checks</Text>
          <Text onPress={() => setIsCash(!isCash)} style={styles.optionText}>
            {isCash ? 'Sign' : 'Cash'}
          </Text>
        </View>
        {(isCash ? cashChecks : signChecks) ? (
          <FlatList
            style={styles.list}
            showsVerticalScrollIndicator={false}
            data={isCash ? cashChecks : signChecks}
            keyExtractor={(item, index) => item._id}
            renderItem={({item}) => <ListItem item={item} />}
            ListEmptyComponent={
              <Text style={styles.emptyText}>
                No {isCash ? 'Sign' : 'Cash'} Checks Found
              </Text>
            }
          />
        ) : (
          <View style={styles.loadingContainer}>
            <LoadingAnimation />
          </View>
        )}
      </View>
      <TouchableOpacity style={styles.buttonFilled}>
        <Text style={styles.buttonFilledText}>My Checkbook</Text>
      </TouchableOpacity>
    </AppMainLayout>
  );
};

const ListItem = ({item}) => {
  const {cryptoTableData} = useContext(AppContext);

  let icon = require('../assets/bitcoin-icon.png');
  let name = 'BTC';

  if (cryptoTableData) {
    const coin = cryptoTableData.find((x) => x.coinSymbol === item.coin);
    if (coin) {
      icon = {uri: coin.coinImage};
      name = coin.coinName;
    }
  }

  return (
    <TouchableOpacity
      onPress={() =>
        WebBrowser.openBrowserAsync(`https://blockcheck.io/#/${item._id}`)
      }
      style={styles.listItem}>
      <View
        style={[
          styles.ribbon,
          {
            backgroundColor:
              item.bcr_type === 'receiver' ? '#30BC96' : '#464B4E',
          },
        ]}
      />
      <View style={styles.itemDetails}>
        <View style={styles.itemGroup}>
          <Image style={styles.itemIcon} source={icon} resizeMode="contain" />
          <Text style={styles.coinName}>{name}</Text>
          <Text style={styles.itemAmount}>
            {formatterHelper(item.amount || 0, item.coin)}
          </Text>
        </View>
        <View style={[styles.itemGroup, {marginTop: 5}]}>
          <Text style={[styles.dateText, {flex: 1}]}>
            {timeParserExpanded(
              (item?.status_logs
                ? item?.status_logs[0]?.timestamp
                : item.timestamp) || item.timestamp,
            )}
          </Text>
          <Text style={styles.dateText}>
            {usdValueFormatter.format(item?.current_usd_value || 0)} USD
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default BlockCheckScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 30,
  },
  headerContainer: {
    alignItems: 'center',
    marginTop: 35,
    paddingHorizontal: 20,
  },
  headerIcon: {
    height: 35,
  },
  headerText: {
    fontFamily: 'Montserrat',
    color: '#08152D',
    textAlign: 'center',
    marginVertical: 20,
  },
  buttonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonOutlined: {
    borderColor: '#08152D',
    borderWidth: 1,
    height: 40,
    width: 150,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  buttonIcon: {
    width: 18,
    height: 18,
    marginRight: 10,
  },
  buttonOutlinedText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
  or: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 22,
    paddingVertical: 15,
  },
  buttonFilled: {
    backgroundColor: '#08152D',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonFilledText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
  listHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 30,
  },
  title: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 20,
  },
  optionText: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    opacity: 0.25,
    fontSize: 20,
  },
  list: {flex: 1, marginTop: 20},
  listItem: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    marginBottom: 10,
    height: 90,
  },
  itemDetails: {
    padding: 20,
    flex: 1,
  },
  itemGroup: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemIcon: {
    height: 25,
    width: 25,
  },
  coinName: {
    flex: 1,
    color: '#4D4D4D',
    fontFamily: 'Montserrat-Bold',
    paddingHorizontal: 12,
    fontSize: 18,
  },
  itemAmount: {
    fontFamily: 'Montserrat-Bold',
    color: '#464B4E',
    fontSize: 18,
  },
  ribbon: {
    width: 10,
    height: '100%',
  },
  dateText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#4D4D4D',
    fontSize: 10,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  emptyText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 18,
    paddingHorizontal: 50,
  },
});
