import React, {useEffect, useContext, useState} from 'react';
import {StyleSheet, Text, View, BackHandler} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {AppContext} from '../contexts/AppContextProvider';
import AppMainLayout from '../layouts/AppMainLayout';
import BrokerCarousel from '../components/BrokerCarousel';
import BrokerViews from '../components/BrokerViews';
import ActionBar from '../components/ActionBar';

const BrokerScreen = () => {
  const {setActiveRoute} = useContext(AppContext);

  const navigation = useNavigation();

  const [activeCarousel, setActiveCarousel] = useState();

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBack);
    navigation.addListener('focus', onScreenFocus);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBack);
      navigation.removeListener('focus', onScreenFocus);
    };
  }, []);

  const handleBack = async () => {
    if (navigation.canGoBack()) {
      navigation.goBack();
    }
    return true;
  };

  const onScreenFocus = (paylod) => {
    setActiveRoute('Brokers');
  };

  return (
    <AppMainLayout isBottomNav>
      <ActionBar showSend />
      <View style={styles.container}>
        <Text style={styles.header}>Make Money In Cryptocurrency</Text>
        <Text style={styles.subHeader}>Here Are The Brands You Follow</Text>
        <BrokerCarousel setActiveCarousel={setActiveCarousel} />
        {activeCarousel &&
          (activeCarousel.displayName === 'Global X Change' ? (
            <BrokerViews activeBanker={activeCarousel} />
          ) : (
            <View style={styles.comingSoonContainer}>
              <Text style={styles.comingSoonHeader}>{`${
                activeCarousel ? activeCarousel.displayName : ''
              }'s AffiliateApp`}</Text>
              <Text style={styles.comingSoonSubHeader}>Is Coming Soon</Text>
            </View>
          ))}
      </View>
    </AppMainLayout>
  );
};

export default BrokerScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    fontFamily: 'Montserrat-Bold',
    textAlign: 'center',
    color: '#08152D',
    fontSize: 18,
    marginTop: 20,
  },
  subHeader: {
    fontFamily: 'Montserrat',
    textAlign: 'center',
    color: '#08152D',
    fontSize: 14,
    marginBottom: 10,
  },
  comingSoonContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  comingSoonHeader: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  comingSoonSubHeader: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
  },
});
