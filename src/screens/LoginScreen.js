/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useContext, useEffect, useRef} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  BackHandler,
  Platform,
  Image,
} from 'react-native';
import {WSnackBar, WToast} from 'react-native-smart-tip';
import Axios from 'axios';
import LoginInputField from '../components/LoginInputField';
import emailIcon from '../assets/email-icon.png';
import passwordIcon from '../assets/password-icon.png';
import {emailValidator} from '../utils';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';
import {GX_API_ENDPOINT, GX_AUTH_URL, APP_CODE, NEW_CHAT_API} from '../configs';
import {AppContext} from '../contexts/AppContextProvider';
import AppStatusBar from '../components/AppStatusBar';
import LoadingAnimation from '../components/LoadingAnimation';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useNavigation} from '@react-navigation/native';
import {Transitioning, Transition} from 'react-native-reanimated';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import KeyboardOffset from '../components/KeyboardOffset';

const LoginScreen = () => {
  const navigation = useNavigation();

  const {setLoginData, setProfileId, isLoggedIn} = useContext(AppContext);

  const [emailInput, setEmailInput] = useState('');
  const [passwordInput, setPasswordInput] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [loginButtonText, setLoginButtonText] = useState('Login');
  const [isEmailFocused, setIsEmailFocused] = useState(false);
  const [isPasswordFocused, setIsPasswordFocused] = useState(false);
  const [show2FA, setShow2FA] = useState(false);
  const [otpInput, setOtpInput] = useState('');
  const [emailValidityStatus, setEmailValidityStatus] = useState(false);
  const [passwordValidityStatus, setPasswordValidityStatus] = useState(false);

  const transitionViewRef = useRef();

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBack);
    };
  }, []);

  useEffect(() => {
    if (isLoggedIn) {
      navigation.replace('Drawer', {screen: 'Network'});
    }
  }, [isLoggedIn]);

  useEffect(() => {
    const email = emailInput.trim().toLowerCase();

    setEmailValidityStatus(emailValidator(email));
  }, [emailInput]);

  useEffect(() => {
    setPasswordValidityStatus(passwordInput.length >= 6);
  }, [passwordInput]);

  useEffect(() => {
    if (otpInput.trim().length === 6) {
      loginClickHandler();
    }
  }, [otpInput]);

  const getUserDetails = (email, token) => {
    Axios.get(`${GX_API_ENDPOINT}/user/details/get`, {
      params: {email},
    })
      .then((res) => {
        const {data} = res;

        // console.log('Data', data);
        if (data.status) {
          registerUserInChat(data, token);
          if (data.user.profile_img) {
            AsyncStorageHelper.setUserName(data.user.username);
            AsyncStorageHelper.setUserAvatarUrl(data.user.profile_img);
            AsyncStorageHelper.setAffId(data.user.affiliate_id);
            setLoginData(
              data.user.username,
              true,
              data.user.profile_img,
              email,
            );
            navigation.replace('Drawer', {screen: 'Network'});
          } else {
            navigation.replace('ProfileUpdate', {
              name: data.user.name,
              userName: data.user.username,
            });
          }
        } else {
          WSnackBar.show({data: `❌ ${data.message}`});
        }
      })
      .catch((error) => {
        console.log('getUserDetails Error', error);
      })
      .finally(() => setIsLoading(false));
  };

  const registerUserInChat = (userResp, token) => {
    Axios.post(
      `${NEW_CHAT_API}/get_application`,
      {code: APP_CODE},
      {headers: {email: userResp?.user?.email, token}},
    )
      .then(({data}) => {
        // console.log('Data', data);

        const appId = data?.payload?.id || '';

        // console.log('userResp', userResp);
        const registerData = {
          first_name: userResp?.user?.first_name || userResp?.user?.name,
          last_name: userResp?.user?.last_name || ' ',
          username: userResp?.user?.username,
          bio: userResp?.user?.bio || 'None',
          email: userResp?.user?.email || '',
          timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
          avatar: userResp?.user?.profile_img || ' ',
        };
        // console.log('registerData', registerData);
        Axios.post(`${NEW_CHAT_API}/register_with_chatsio`, registerData)
          .then(({data: chatData}) => {
            // console.log('Data', chatData);
            const postData = {
              email: userResp?.user?.email || '',
              app_id: appId,
            };
            Axios.post(`${NEW_CHAT_API}/register_user`, postData);
          })
          .catch((error) => {
            console.log('Error on registering user', error);
          });
      })
      .catch((error) => {
        console.log('Error on getting application', error);
      });
  };

  const loginClickHandler = () => {
    if (!isLoading) {
      const email = emailInput.trim().toLowerCase();

      if (!emailValidator(email)) {
        return WToast.show({
          data: 'Enter a valid Email',
          position: WToast.position.TOP,
        });
      }

      if (!passwordInput) {
        return WToast.show({
          data: 'Enter your password',
          position: WToast.position.TOP,
        });
      }

      setIsLoading(true);

      let postData = {
        email,
        password: passwordInput,
      };

      if (show2FA) {
        postData = {...postData, totp_code: otpInput};
      }

      Axios.post(`${GX_AUTH_URL}/gx/user/login`, postData)
        .then((response) => {
          const {data} = response;

          // console.log('Login Data', data);

          if (data.mfa) {
            setShow2FA(true);
            setIsLoading(false);
          } else {
            if (data.status) {
              Axios.post(`${GX_API_ENDPOINT}/gxb/apps/register/user`, {
                email,
                app_code: APP_CODE,
              }).then((profileResp) => {
                if (profileResp.data.profile_id) {
                  AsyncStorageHelper.setProfileId(profileResp.data.profile_id);
                  setProfileId(profileResp.data.profile_id);
                }
              });
              WSnackBar.show({data: '✅ Logged in...'});
              AsyncStorageHelper.setIsLoggedIn(true);
              AsyncStorageHelper.setLoginEmail(email);
              AsyncStorageHelper.setAppToken(data.idToken);
              AsyncStorageHelper.setAccessToken(data.accessToken);
              AsyncStorageHelper.setRefreshToken(data.refreshToken);
              AsyncStorageHelper.setDeviceKey(data.device_key);
              getUserDetails(email, data.idToken);
            } else {
              setIsLoading(false);
              WSnackBar.show({data: `❌ ${data.message}`});
              if (show2FA) {
                setOtpInput('');
              } else {
                setPasswordInput('');
              }
            }
          }
        })
        .catch((error) => {
          setIsLoading(false);
          console.log('Login Error', error);
          setPasswordInput('');
        });
    }
  };

  const handleBack = () => {
    if (navigation.canGoBack()) {
      navigation.goBack();
    } else {
      BackHandler.exitApp();
    }
    return true;
  };

  const transition = (
    <Transition.Together>
      <Transition.Change />
    </Transition.Together>
  );

  const executeTransform = () => {
    if (Platform.OS === 'ios') {
      transitionViewRef.current.animateNextTransition();
    }
  };

  const requestPasswordFocus = () => {
    if (!emailValidator(emailInput.trim().toLowerCase())) {
      return WToast.show({
        data: 'Enter a valid Email',
        position: WToast.position.TOP,
      });
    }

    executeTransform();
    setIsEmailFocused(false);
    setIsPasswordFocused(true);
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <AppStatusBar backgroundColor="white" barStyle="dark-content" />
      {isLoading && (
        <View style={[StyleSheet.absoluteFill, styles.loadingContainer]}>
          <LoadingAnimation />
        </View>
      )}
      <View style={styles.container}>
        <TouchableOpacity style={styles.backButton} onPress={navigation.goBack}>
          <Image
            style={styles.backIcon}
            source={require('../assets/back-arrow.png')}
            resizeMode="contain"
          />
        </TouchableOpacity>
        <Transitioning.View
          ref={transitionViewRef}
          transition={transition}
          style={styles.loginFrom}>
          <Text style={styles.loginTitle}>Login</Text>
          <Text style={styles.loginSubTitle}>
            {show2FA
              ? 'Enter Code From Google Authenticator'
              : '& Unlock Full Access'}
          </Text>

          {show2FA ? (
            <OTPInputView
              codeInputFieldStyle={styles.underlineStyleBase}
              codeInputHighlightStyle={styles.underlineStyleHighLighted}
              style={styles.otpInput}
              pinCount={6}
              autoFocusOnLoad
              code={otpInput}
              onCodeChanged={(code) => setOtpInput(code)}
            />
          ) : (
            <>
              <LoginInputField
                style={{display: isPasswordFocused ? 'none' : 'flex'}}
                placeholder="EMAIL"
                type="emailAddress"
                value={emailInput}
                editable={!isLoading}
                onChangeText={(text) => setEmailInput(text)}
                onFocus={() => {
                  executeTransform();
                  setIsEmailFocused(true);
                  setLoginButtonText('Next');
                }}
                onBlur={() => {
                  executeTransform();
                  setIsEmailFocused(false);
                  setLoginButtonText('Login');
                }}
                focus={isEmailFocused}
                validatorStatus={emailValidityStatus}
                showNext
                onNext={requestPasswordFocus}
              />
              <LoginInputField
                style={{display: isEmailFocused ? 'none' : 'flex'}}
                placeholder="PASSWORD"
                type="password"
                secureTextEntry
                value={passwordInput}
                editable={!isLoading}
                onChangeText={(text) => setPasswordInput(text)}
                onFocus={() => {
                  executeTransform();
                  setIsPasswordFocused(true);
                }}
                onBlur={() => {
                  executeTransform();
                  setIsPasswordFocused(false);
                }}
                focus={isPasswordFocused}
                validatorStatus={passwordValidityStatus}
                showNext
                onNext={loginClickHandler}
              />
            </>
          )}
          {isEmailFocused || isPasswordFocused || show2FA ? null : (
            <View style={styles.actionContainer}>
              <Text
                onPress={() => navigation.navigate('ForgotPassword')}
                style={styles.forgotText}>
                Forgot Password?
              </Text>
              <TouchableOpacity
                style={styles.loginButton}
                onPress={loginClickHandler}>
                <Text style={styles.loginButtonText}>Login</Text>
              </TouchableOpacity>
            </View>
          )}
        </Transitioning.View>
      </View>
      <View style={styles.singUpContainer}>
        <Text style={styles.signUpText}>
          {show2FA ? 'I Need To Reset My 2FA' : "I Don't Have An Account"}
        </Text>
        <TouchableOpacity
          style={styles.signUpBtn}
          onPress={() => (show2FA ? null : navigation.navigate('Signup'))}>
          <Text style={styles.signUpBtnText}>Click Here</Text>
        </TouchableOpacity>
      </View>
      <KeyboardOffset />
    </SafeAreaView>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: '#FFFFFF'},
  loadingContainer: {
    backgroundColor: 'white',
    zIndex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loginFrom: {
    flex: 1,
    paddingHorizontal: 30,
    justifyContent: 'center',
  },
  loginTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 50,
  },
  loginSubTitle: {
    color: '#999C9A',
    marginBottom: 40,
    fontFamily: 'Montserrat',
  },
  loginButton: {
    marginTop: 15,
    marginBottom: 30,
    backgroundColor: '#08152D',
    width: 150,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  loginButtonText: {
    color: 'white',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  singUpContainer: {
    flexDirection: 'row',
    paddingVertical: 25,
    paddingHorizontal: 30,
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
  },
  signUpText: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
  },
  signUpBtn: {
    borderColor: '#08152D',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    paddingVertical: 4,
    paddingHorizontal: 15,
    marginLeft: 15,
  },
  signUpBtnText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 10,
  },
  backButton: {
    marginLeft: 20,
    width: 40,
    height: 40,
  },
  backIcon: {
    flex: 1,
    width: null,
    height: null,
  },
  otpInput: {
    height: 60,
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
  },
  underlineStyleBase: {
    borderWidth: 1,
    color: '#08152D',
  },
  underlineStyleHighLighted: {
    borderColor: '#999C9A',
    color: '#08152D',
  },
  actionContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  forgotText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
  },
});
