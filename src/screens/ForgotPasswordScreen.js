import {useNavigation} from '@react-navigation/native';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import Axios from 'axios';
import React, {useContext, useEffect, useRef, useState} from 'react';
import {
  BackHandler,
  Image,
  Keyboard,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Animated, {
  Clock,
  interpolate,
  set,
  Transition,
  Transitioning,
  useCode,
} from 'react-native-reanimated';
import {SafeAreaView} from 'react-native-safe-area-context';
import {WToast} from 'react-native-smart-tip';
import AppStatusBar from '../components/AppStatusBar';
import LoadingAnimation from '../components/LoadingAnimation';
import LoginInputField from '../components/LoginInputField';
import {GX_AUTH_URL} from '../configs';
import {AppContext} from '../contexts/AppContextProvider';
import {emailValidator} from '../utils';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';
import ReanimatedTimingHelper from '../utils/ReanimatedTimingHelper';

const ForgotPasswordScreen = () => {
  const {isLoggedIn, removeLoginData} = useContext(AppContext);

  const [isLoading, setIsLoading] = useState(false);
  const [show2FA, setShow2FA] = useState(false);
  const [otpInput, setOtpInput] = useState('');
  const [emailInput, setEmailInput] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const [passwordInput, setPasswordInput] = useState('');

  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);

  const navigation = useNavigation();

  const transitionViewRef = useRef();

  const chatKeyboardAnimation = useRef(new Animated.Value(0));

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBack);
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
      onKeyBoardShow,
    );
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
      onKeyBoardHide,
    );
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBack);
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
        onKeyBoardShow,
      );
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
        onKeyBoardHide,
      );
    };
  }, []);

  useCode(
    () =>
      isKeyboardOpen
        ? [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ]
        : [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ],
    [isKeyboardOpen],
  );

  const onKeyBoardShow = (e) => {
    setKeyboardHeight(e.endCoordinates.height - 30);
    setIsKeyboardOpen(true);
  };

  const onKeyBoardHide = () => {
    setIsKeyboardOpen(false);
  };

  const handleBack = () => {
    if (navigation.canGoBack()) {
      navigation.goBack();
    } else {
      BackHandler.exitApp();
    }
    return true;
  };

  const transition = (
    <Transition.Together>
      <Transition.Change />
    </Transition.Together>
  );

  const executeTransform = () => {
    if (Platform.OS === 'ios') {
      transitionViewRef.current.animateNextTransition();
    }
  };

  const logoutHandler = () => {
    navigation.replace('Landing');
    AsyncStorageHelper.deleteAllUserInfo();
    removeLoginData();
    WToast.show({data: 'Please Login Again', position: WToast.position.TOP});
  };

  const verifyOTP = () => {
    if (!otpInput.length === 6) {
      return WToast.show({
        data: 'Please Enter A Valid OTP',
        position: WToast.position.TOP,
      });
    }

    setShowPassword(true);
  };

  const resetPassword = () => {
    const email = emailInput.toLowerCase().trim();

    const capRegex = new RegExp(/^.*[A-Z].*/);
    const numRegex = new RegExp(/^.*[0-9].*/);
    const speRegex = new RegExp(/^.*[!@#$%^&*()+=].*/);

    if (passwordInput.length < 6) {
      return WToast.show({
        data: 'Password Should Have At Least 6 Character',
        position: WToast.position.TOP,
      });
    }
    if (!capRegex.test(passwordInput)) {
      return WToast.show({
        data: 'Password Should Have At Least One Uppercase Character',
        position: WToast.position.TOP,
      });
    }
    if (!numRegex.test(passwordInput)) {
      return WToast.show({
        data: 'Password Should Have At Least One Numeric Character',
        position: WToast.position.TOP,
      });
    }
    if (!speRegex.test(passwordInput)) {
      return WToast.show({
        data: 'Password Should Have At Least One Special Character',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    const postData = {email, code: otpInput, newPassword: passwordInput};

    Axios.post(`${GX_AUTH_URL}/gx/user/password/forgot/confirm`, postData)
      .then((resp) => {
        const {data} = resp;

        // console.log('Data', data);

        if (data.status) {
          WToast.show({
            data: 'Password Successfully Changed',
            position: WToast.position.TOP,
          });
          if (isLoggedIn) {
            logoutHandler();
          } else {
            navigation.goBack();
          }
        } else {
          WToast.show({
            data: data.message || 'Error On Resetting Password',
            position: WToast.position.TOP,
          });
          setIsLoading(false);
        }
      })
      .catch((error) => {
        WToast.show({
          data: 'Error On Resetting Password',
          position: WToast.position.TOP,
        });
        console.log('Error On Resetting Password', error);
        setIsLoading(false);
      });
  };

  const requestOTP = () => {
    const email = emailInput.toLowerCase().trim();

    if (!emailValidator(email)) {
      return WToast.show({
        data: 'Please Enter A Valid Email ID',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    Axios.post(`${GX_AUTH_URL}/gx/user/password/forgot/request`, {
      email,
    })
      .then((resp) => {
        const {data} = resp;

        // console.log('Data', data);

        if (data.status) {
          WToast.show({
            data: `Code Sent To ${email}`,
            position: WToast.position.TOP,
          });
          setShow2FA(true);
        } else {
          WToast.show({
            data: data.message || 'Error on Sending OTP',
            position: WToast.position.TOP,
          });
        }
      })
      .catch((error) => {
        console.log('Error on Requesting OTP', error);
        WToast.show({
          data: 'Error on Sending OTP',
          position: WToast.position.TOP,
        });
      })
      .finally(() => setIsLoading(false));
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <AppStatusBar backgroundColor="white" barStyle="dark-content" />
      {isLoading && (
        <View style={[StyleSheet.absoluteFill, styles.loadingContainer]}>
          <LoadingAnimation />
        </View>
      )}
      <View style={styles.container}>
        <TouchableOpacity style={styles.backButton} onPress={navigation.goBack}>
          <Image
            style={styles.backIcon}
            source={require('../assets/back-arrow.png')}
            resizeMode="contain"
          />
        </TouchableOpacity>
        <Transitioning.View
          ref={transitionViewRef}
          transition={transition}
          style={styles.loginFrom}>
          <Text style={styles.loginTitle}>Reset Password</Text>
          <Text style={styles.loginSubTitle}>
            {showPassword
              ? 'Enter Your New Password'
              : show2FA
              ? 'Enter Code From Sent To You Email'
              : 'Enter Your Email'}
          </Text>

          {showPassword ? (
            <LoginInputField
              icon={require('../assets/password-icon.png')}
              placeholder="PASSWORD"
              type="password"
              secureTextEntry
              value={passwordInput}
              editable={!isLoading}
              onChangeText={(text) => setPasswordInput(text)}
            />
          ) : show2FA ? (
            <OTPInputView
              codeInputFieldStyle={styles.underlineStyleBase}
              codeInputHighlightStyle={styles.underlineStyleHighLighted}
              style={styles.otpInput}
              pinCount={6}
              autoFocusOnLoad
              code={otpInput}
              onCodeChanged={(code) => setOtpInput(code)}
            />
          ) : (
            <LoginInputField
              icon={require('../assets/email-icon.png')}
              placeholder="EMAIL"
              type="emailAddress"
              value={emailInput}
              editable={!isLoading}
              onChangeText={(text) => setEmailInput(text)}
            />
          )}
          <TouchableOpacity
            style={styles.loginButton}
            onPress={
              showPassword ? resetPassword : show2FA ? verifyOTP : requestOTP
            }>
            <Text style={styles.loginButtonText}>
              {showPassword ? 'Reset' : show2FA ? 'Submit' : 'Initiate'}
            </Text>
          </TouchableOpacity>
        </Transitioning.View>
        <Animated.View
          style={[
            {
              height: interpolate(chatKeyboardAnimation.current, {
                inputRange: [0, 1],
                outputRange: [0, keyboardHeight],
              }),
            },
          ]}
        />
      </View>
    </SafeAreaView>
  );
};

export default ForgotPasswordScreen;

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: '#FFFFFF'},
  loadingContainer: {
    backgroundColor: 'white',
    zIndex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backButton: {
    marginLeft: 20,
    width: 40,
    height: 40,
  },
  backIcon: {
    flex: 1,
    width: null,
    height: null,
  },
  loginFrom: {
    flex: 1,
    paddingHorizontal: 30,
    justifyContent: 'center',
  },
  loginTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 35,
  },
  loginSubTitle: {
    color: '#999C9A',
    marginBottom: 40,
    fontFamily: 'Montserrat',
  },
  loginButton: {
    marginTop: 15,
    marginBottom: 30,
    backgroundColor: '#08152D',
    width: 150,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginLeft: 'auto',
  },
  loginButtonText: {
    color: 'white',
    fontFamily: 'Montserrat-Bold',
    fontSize: 16,
  },
  otpInput: {
    height: 60,
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
  },
});
