/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useEffect, useRef, useState} from 'react';
import {
  BackHandler,
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ActionBar from '../components/ActionBar';
import LoadingAnimation from '../components/LoadingAnimation';
import AppMainLayout from '../layouts/AppMainLayout';
import {useNavigation, useRoute} from '@react-navigation/native';
import BrandDetails from '../components/BrandDetails';
import ThemeData from '../configs/ThemeData';
import SkeltonItem from '../components/SkeltonItem';
import axios from 'axios';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';
import {
  APP_CODE,
  GX_API_ENDPOINT,
  NEW_CHAT_API,
  TELLER_API2_ENDPOINT,
} from '../configs';
import PopupLayout from '../layouts/PopupLayout';
import {WToast} from 'react-native-smart-tip';
import {AppContext} from '../contexts/AppContextProvider';
import Animated, {
  Clock,
  interpolate,
  set,
  useCode,
} from 'react-native-reanimated';
import ReanimatedTimingHelper from '../utils/ReanimatedTimingHelper';

const {width} = Dimensions.get('window');

const BrandsScreen = () => {
  const {params} = useRoute();
  const navigation = useNavigation();

  const {isLoggedIn, chatAppId} = useContext(AppContext);

  const [events, setEvents] = useState();
  const [followers, setFollowers] = useState();
  const [showFollowSuccess, setShowFollowSuccess] = useState(false);
  const [isFollowLoading, setIsFollowLoading] = useState(false);
  const [isUserFollowed, setIsUserFollowed] = useState(false);
  const [isUnfollowPopupOpen, setIsUnfollowPopupOpen] = useState(false);
  const [isUnfollowLoading, setIsUnfollowLoading] = useState(false);
  const [unfollowSuccess, setUnfollowSuccess] = useState(false);
  const [isFragmentExpanded, setIsFragmentExpanded] = useState(false);
  const [chatData, setChatData] = useState();

  const expandAnimation = useRef(new Animated.Value(1));

  useCode(
    () =>
      isFragmentExpanded
        ? [
            set(
              expandAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ]
        : [
            set(
              expandAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ],
    [isFragmentExpanded],
  );

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBack);

    (async () => {
      const email = await AsyncStorageHelper.getLoginEmail();

      axios
        .get(`${GX_API_ENDPOINT}/coin/vault/service/calendar/event/get`, {
          params: {email},
        })
        .then(({data}) => {
          setEvents(data?.count || 0);
        })
        .catch((error) => {
          console.log('Eroor on geting events', error);
        });
    })();
    getFollowers();

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBack);
    };
  }, []);

  useEffect(() => {
    if (chatAppId) {
      (async () => {
        const email = await AsyncStorageHelper.getLoginEmail();
        const token = await AsyncStorageHelper.getAppToken();

        const postData = {email: selectedItem?.email || '', app_id: chatAppId};
        const headers = {email, token};

        axios
          .post(`${NEW_CHAT_API}/get_user`, postData, {headers})
          .then(({data}) => {
            // console.log('Chat Data', data);
            if (data.status) {
              setChatData(data?.payload || '');
            } else {
              setChatData();
            }
          })
          .catch((error) => {
            console.log('Error on getting user details', error);
          });
      })();
    }
  }, [selectedItem, chatAppId]);

  useEffect(() => {
    if (isUnfollowPopupOpen) {
      setUnfollowSuccess(false);
    }
  }, [isUnfollowPopupOpen]);

  const handleBack = async () => {
    if (navigation.canGoBack()) {
      navigation.goBack();
    }
    return true;
  };

  const navigateToLicense = () => {
    navigation.navigate('License', {activeBanker: selectedItem});
  };

  const getFollowers = async () => {
    const email = await AsyncStorageHelper.getLoginEmail();

    axios
      .get(`${TELLER_API2_ENDPOINT}/banker/followersList`, {
        params: {email: selectedItem?.email},
      })
      .then(({data}) => {
        // console.log('Followers', data);

        const list = data?.data?.followers || [];
        const findIndex = list.findIndex((x) => x.userEmail === email);
        setIsUserFollowed(findIndex >= 0);

        setFollowers(data?.data?.count || 0);
      })
      .catch((error) => {
        console.log('Error getting followers', error);
      });
  };

  const followCurrentBrand = async () => {
    if (!isLoggedIn) {
      return navigation.navigate('Landing');
    }

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    setShowFollowSuccess(true);
    setIsFollowLoading(true);

    axios
      .post(
        `${TELLER_API2_ENDPOINT}/banker/follow`,
        {
          appCode: APP_CODE,
          bankerEmail: selectedItem?.email,
        },
        {headers: {email, token}},
      )
      .then(({data}) => {
        // console.log('Follow Data', data);
        if (data.status) {
          setShowFollowSuccess(true);
          getFollowers();
        } else {
          WToast.show({data: data.message, position: WToast.position.TOP});
          setShowFollowSuccess(false);
        }
      })
      .catch((error) => {
        console.log('Error on following the brand', error);
        WToast.show({data: 'Network Error', position: WToast.position.TOP});
        setShowFollowSuccess(false);
      })
      .finally(() => {
        setIsFollowLoading(false);
      });
  };

  const unfollowCurrentBrand = async () => {
    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    setIsUnfollowLoading(true);

    axios
      .post(
        `${TELLER_API2_ENDPOINT}/banker/unfollow`,
        {
          appCode: APP_CODE,
          bankerEmail: selectedItem?.email,
        },
        {headers: {email, token}},
      )
      .then(({data}) => {
        // console.log('Follow Data', data);
        if (data.status) {
          setUnfollowSuccess(true);
          getFollowers();
        } else {
          WToast.show({data: data.message, position: WToast.position.TOP});
          setUnfollowSuccess(false);
        }
      })
      .catch((error) => {
        console.log('Error on following the brand', error);
        WToast.show({data: 'Network Error', position: WToast.position.TOP});
        setUnfollowSuccess(false);
      })
      .finally(() => {
        setIsUnfollowLoading(false);
      });
  };

  const naviagteToBrokerChat = async () => {
    if (chatData) {
      navigation.navigate('Support', {openUserChat: chatData});
    } else {
      WToast.show({
        data: 'Sorry. This User Not available in Chat',
        position: WToast.position.TOP,
      });
    }
  };

  const selectedItem = params.selectedItem || '';

  // console.log('selectedItem', selectedItem);

  const colorCode = `#${selectedItem?.colorCode || '186ab4'}`;

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      {selectedItem ? (
        <>
          <View style={styles.imageContainer}>
            <Image
              source={{uri: selectedItem?.coverPicURL || ''}}
              style={styles.coverImage}
              resizeMode="cover"
            />
          </View>
          <View style={styles.brandImageContainer}>
            <TouchableOpacity
              onPress={() => navigation.navigate('Channel')}
              style={[styles.action]}>
              {events === undefined ? (
                <SkeltonItem itemHeight={20} itemWidth={40} />
              ) : (
                <Text style={[styles.actionCount, {color: colorCode}]}>
                  {events}
                </Text>
              )}
              <Text style={[styles.actionText, {color: colorCode}]}>
                Events
              </Text>
            </TouchableOpacity>
            <Image
              source={{uri: selectedItem?.profilePicURL || ''}}
              style={styles.profileImage}
              resizeMode="cover"
            />
            <TouchableOpacity style={[styles.action]}>
              {followers === undefined ? (
                <SkeltonItem itemHeight={20} itemWidth={40} />
              ) : (
                <Text style={[styles.actionCount, {color: colorCode}]}>
                  {followers}
                </Text>
              )}
              <Text style={[styles.actionText, {color: colorCode}]}>
                Followers
              </Text>
            </TouchableOpacity>
          </View>
          <Animated.View
            style={{
              maxHeight: interpolate(expandAnimation.current, {
                inputRange: [0, 1],
                outputRange: [0, 300],
              }),
              backgroundColor: 'white',
              overflow: 'hidden',
            }}>
            <View style={styles.headerContainer}>
              <Text style={[styles.headerText, {color: colorCode}]}>
                {selectedItem?.displayName}
              </Text>
              <Text
                numberOfLines={1}
                style={[styles.website, {color: colorCode}]}>
                {selectedItem?.website || selectedItem?.email}
              </Text>
              <View style={styles.followActionContainer}>
                <TouchableOpacity
                  disabled={!chatData}
                  onPress={naviagteToBrokerChat}>
                  <View
                    style={[
                      styles.followAction,
                      {
                        borderColor: colorCode,
                        borderWidth: 1,
                        marginRight: 10,
                        opacity: chatData ? 1 : 0.3,
                      },
                    ]}>
                    <Text style={[styles.followActionText, {color: colorCode}]}>
                      Chat
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={
                    isUserFollowed
                      ? () => setIsUnfollowPopupOpen(true)
                      : followCurrentBrand
                  }
                  style={[
                    styles.followAction,
                    {
                      backgroundColor: colorCode,
                    },
                  ]}>
                  <Text style={styles.followActionText}>
                    {isUserFollowed ? 'Following' : 'Follow'}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </Animated.View>
          <BrandDetails
            selectedItem={selectedItem}
            color={colorCode}
            navigateToLicense={navigateToLicense}
            isFragmentExpanded={isFragmentExpanded}
            setIsFragmentExpanded={setIsFragmentExpanded}
          />
        </>
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      )}
      <PopupLayout
        isOpen={showFollowSuccess}
        onClose={() => setShowFollowSuccess(false)}
        noHeader
        noScrollView
        autoHeight
        containerStyles={{paddingBottom: 0}}>
        <View
          style={[
            styles.followPopupContainer,
            isFollowLoading && {paddingVertical: 50},
          ]}>
          {isFollowLoading ? (
            <LoadingAnimation />
          ) : (
            <>
              <Image
                style={styles.followIcon}
                resizeMode="contain"
                source={{uri: selectedItem?.profilePicURL}}
              />
              <Text style={[styles.statusText, {color: colorCode}]}>
                You Have Followed
              </Text>
              <Text style={[styles.statusBrandName, {color: colorCode}]}>
                {selectedItem?.displayName}
              </Text>
              <TouchableOpacity
                style={[styles.popupCloseButton, {backgroundColor: colorCode}]}
                onPress={() => setShowFollowSuccess(false)}>
                <Text style={styles.popupCloseText}>Close</Text>
              </TouchableOpacity>
            </>
          )}
        </View>
      </PopupLayout>

      <PopupLayout
        isOpen={isUnfollowPopupOpen}
        onClose={() => setIsUnfollowPopupOpen(false)}
        noHeader
        noScrollView
        autoHeight
        containerStyles={{paddingBottom: 0}}>
        <View
          style={[
            styles.followPopupContainer,
            isUnfollowLoading && {paddingVertical: 50},
          ]}>
          {isUnfollowLoading ? (
            <LoadingAnimation />
          ) : unfollowSuccess ? (
            <>
              <Image
                style={styles.followIcon}
                resizeMode="contain"
                source={{uri: selectedItem?.profilePicURL}}
              />
              <Text style={[styles.statusText, {color: colorCode}]}>
                You Have Unfollowed
              </Text>
              <Text style={[styles.statusBrandName, {color: colorCode}]}>
                {selectedItem?.displayName}
              </Text>
              <TouchableOpacity
                style={[styles.popupCloseButton, {backgroundColor: colorCode}]}
                onPress={() => setIsUnfollowPopupOpen(false)}>
                <Text style={styles.popupCloseText}>Close</Text>
              </TouchableOpacity>
            </>
          ) : (
            <>
              <Image
                style={styles.followIcon}
                resizeMode="contain"
                source={{uri: selectedItem?.profilePicURL}}
              />
              <Text
                style={[
                  styles.statusText,
                  {color: colorCode, fontSize: 17, paddingHorizontal: 40},
                ]}>
                Are You Sure You Want To Unfollow {selectedItem?.displayName}?
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 40,
                }}>
                <TouchableOpacity
                  style={{
                    flex: 1,
                    height: 50,
                    justifyContent: 'center',
                    alignItems: 'center',
                    backgroundColor: colorCode,
                  }}
                  onPress={() => setIsUnfollowPopupOpen(false)}>
                  <Text
                    style={{
                      fontFamily: ThemeData.FONT_SEMI_BOLD,
                      color: 'white',
                      textAlign: 'center',
                    }}>
                    Never Mind
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    flex: 1,
                    height: 50,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderColor: colorCode,
                    borderWidth: 0.5,
                  }}
                  onPress={unfollowCurrentBrand}>
                  <Text
                    style={{
                      fontFamily: ThemeData.FONT_SEMI_BOLD,
                      color: colorCode,
                      textAlign: 'center',
                    }}>
                    Unfollow
                  </Text>
                </TouchableOpacity>
              </View>
            </>
          )}
        </View>
      </PopupLayout>
    </AppMainLayout>
  );
};

export default BrandsScreen;

const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  imageContainer: {
    borderBottomColor: '#EBEBEB',
    borderBottomWidth: 1,
    backgroundColor: 'white',
  },
  coverImage: {
    height: 150,
    width,
  },
  headerContainer: {
    alignItems: 'center',
    paddingHorizontal: 40,
    marginBottom: 20,
  },
  brandImageContainer: {
    marginTop: -45,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  action: {
    backgroundColor: 'white',
    borderColor: '#E5E5E5',
    borderWidth: 1,
    height: 45,
    width: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  actionCount: {
    fontFamily: 'Montserrat-Bold',
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 20,
  },
  actionText: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 11,
  },
  profileImage: {
    width: 90,
    height: 90,
    backgroundColor: 'white',
  },
  headerText: {
    fontFamily: 'Montserrat-Bold',
    color: '#186AB4',
    textAlign: 'center',
    fontSize: 20,
    marginTop: 15,
  },
  website: {
    textAlign: 'center',
    fontFamily: 'Montserrat',
    color: '#186AB4',
    fontSize: 11,
    marginTop: 5,
  },
  checkIcon: {
    height: 20,
    width: 20,
    marginLeft: 15,
  },
  emptyContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyText: {
    color: '#303030',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 15,
    textAlign: 'center',
    paddingHorizontal: 50,
  },
  followActionContainer: {
    flexDirection: 'row',
    marginTop: 20,
  },
  followAction: {
    height: 35,
    width: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  followActionText: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: 'white',
    fontSize: 12,
  },
  followPopupContainer: {
    margin: -30,
    paddingTop: 40,
  },
  followIcon: {
    width: 80,
    height: 80,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  statusText: {
    textAlign: 'center',
    color: '#186AB4',
    fontFamily: ThemeData.FONT_NORMAL,
    marginBottom: 8,
    marginTop: 12,
  },
  statusBrandName: {
    textAlign: 'center',
    color: '#186AB4',
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 20,
  },
  popupCloseButton: {
    backgroundColor: '#186AB4',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: -30,
    marginTop: 40,
  },
  popupCloseText: {
    color: 'white',
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 18,
  },
});
