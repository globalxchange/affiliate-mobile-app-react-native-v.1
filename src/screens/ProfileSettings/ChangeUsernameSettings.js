import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import React, {useContext, useState} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import LoadingAnimation from '../../components/LoadingAnimation';
import {GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import ProfileSettingsLayout from '../../layouts/ProfileSettingsLayout';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';

const ChangeUsernameSettings = () => {
  const {userName, setLoginData} = useContext(AppContext);

  const {goBack} = useNavigation();

  const [userNameInput, setUserNameInput] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const updateUserDetails = (email) => {
    axios
      .get(`${GX_API_ENDPOINT}/user/details/get`, {
        params: {email},
      })
      .then((res) => {
        const {data} = res;

        // console.log('Data', data);
        if (data.status) {
          AsyncStorageHelper.setUserName(data.user.username);
          setLoginData(data.user.username, true, data.user.profile_img);
        }
      });
  };

  const updateUserName = async () => {
    const usernameTrimmed = userNameInput.trim();

    if (!usernameTrimmed) {
      return WToast.show({
        data: 'Please Input A Code',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();
    const accessToken = await AsyncStorageHelper.getAccessToken();

    const postData = {
      email,
      accessToken,
      field: 'username',
      value: usernameTrimmed,
    };

    // console.log('updateCode postData', postData);

    axios
      .post(`${GX_API_ENDPOINT}/user/details/edit`, postData)
      .then((resp) => {
        const {data} = resp;

        // console.log('updateCode Resp', data);

        if (data.status) {
          updateUserDetails(email);
          WToast.show({
            data: data.message || 'Username Updated',
            position: WToast.position.TOP,
          });
        } else {
          WToast.show({
            data: data.message || 'Error on updating AffiliateSync Code',
            position: WToast.position.TOP,
          });
        }
      })
      .catch((error) => {
        WToast.show({
          data: 'Error on updating AffiliateSync Code',
          position: WToast.position.TOP,
        });
        console.log('Error on updating AffiliateSync Code', error);
      })
      .finally(() => setIsLoading(false));
  };

  return (
    <ProfileSettingsLayout
      breadCrumbs={[
        {title: 'Settings', onPress: goBack},
        {title: 'Change Name', onPress: null},
      ]}>
      {isLoading ? (
        <View style={{flex: 1, justifyContent: 'center'}}>
          <LoadingAnimation />
        </View>
      ) : (
        <View style={styles.container}>
          <View style={styles.detailsItem}>
            <Text style={styles.detailLabel}>Current Username</Text>
            <Text style={styles.detailValue}>{userName}</Text>
          </View>
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.input}
              placeholderTextColor={'#878788'}
              value={userNameInput}
              onChangeText={(text) => setUserNameInput(text)}
              placeholder="Enter New Username"
            />
          </View>
          <TouchableOpacity
            style={styles.changeButton}
            onPress={updateUserName}>
            <Text style={styles.changeButtonText}>Change</Text>
          </TouchableOpacity>
        </View>
      )}
    </ProfileSettingsLayout>
  );
};

export default ChangeUsernameSettings;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginBottom: 20,
  },
  detailsItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 20,
    paddingHorizontal: 20,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
  },
  detailLabel: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
  },
  detailValue: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'right',
  },
  inputContainer: {
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginTop: 25,
    alignItems: 'center',
    paddingHorizontal: 20,
    marginHorizontal: 20,
  },
  input: {
    flex: 1,
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    height: 50,
  },
  changeButton: {
    marginLeft: 20,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    width: 150,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  changeButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
});
