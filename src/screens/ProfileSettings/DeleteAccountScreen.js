import {
  useNavigation,
  DrawerActions,
  CommonActions,
} from '@react-navigation/native';
import axios from 'axios';
import React, {useContext, useState} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import LoadingAnimation from '../../components/LoadingAnimation';
import {GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import ProfileSettingsLayout from '../../layouts/ProfileSettingsLayout';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';

const DeleteAccountScreen = () => {
  const {
    userName,
    setLoginData,
    userEmail,
    removeLoginData,
    setIsLoggedIn,
  } = useContext(AppContext);

  const {goBack} = useNavigation();
  const navigation = useNavigation();

  const [userNameInput, setUserNameInput] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [deleted, setDeleted] = useState(false);

  const updateUserDetails = (email) => {
    axios
      .get(`${GX_API_ENDPOINT}/user/details/get`, {
        params: {email},
      })
      .then((res) => {
        const {data} = res;

        // console.log('Data', data);
        if (data.status) {
          AsyncStorageHelper.setUserName(data.user.username);
          setLoginData(data.user.username, true, data.user.profile_img);
        }
      });
  };

  const logoutHandler = () => {
    // navigation.dispatch(DrawerActions.closeDrawer());
    // navigation.dispatch(
    //   CommonActions.reset({
    //     index: 1,
    //     routes: [{name: 'Landing'}],
    //   }),
    // );
    // AsyncStorageHelper.deleteAllUserInfo();
    // removeLoginData();
    // setIsLoggedIn(false);
    // goBack();
    // navigation.dispatch(DrawerActions.closeDrawer());
    removeLoginData();
    AsyncStorageHelper.deleteAllUserInfo();
    setIsLoggedIn(false);
    navigation.replace('Landing');
    // AsyncStorageHelper.deleteAllUserInfo();
    // removeLoginData();
    // WToast.show({data: 'Please Login Again', position: WToast.position.TOP});
  };

  const updateUserName = async () => {
    const usernameTrimmed = userNameInput.trim();

    if (!usernameTrimmed) {
      return WToast.show({
        data: 'Please Input A Code',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();
    const accessToken = await AsyncStorageHelper.getAccessToken();

    const postData = {
      email,
      accessToken,
      field: 'username',
      value: usernameTrimmed,
    };

    // console.log('updateCode postData', postData);

    axios
      .post(`${GX_API_ENDPOINT}/user/details/edit`, postData)
      .then((resp) => {
        const {data} = resp;

        // console.log('updateCode Resp', data);

        if (data.status) {
          updateUserDetails(email);
          WToast.show({
            data: data.message || 'Username Updated',
            position: WToast.position.TOP,
          });
        } else {
          WToast.show({
            data: data.message || 'Error on updating AffiliateSync Code',
            position: WToast.position.TOP,
          });
        }
      })
      .catch((error) => {
        WToast.show({
          data: 'Error on updating AffiliateSync Code',
          position: WToast.position.TOP,
        });
        console.log('Error on updating AffiliateSync Code', error);
      })
      .finally(() => setIsLoading(false));
  };

  const deleteAccount = async () => {
    const token = await AsyncStorageHelper.getAppToken();
    // console.log(token, 'token');
    setIsLoading(true);
    axios
      .post(`https://gxauth.apimachine.com/gx/user/delete`, {
        // adminEmail: userEmail, // admin email
        token: token, // admin token
        email: userEmail, // user email u want to delete
      })
      .then((res) => {
        if (res.data.status) {
          // console.log(res);

          setTimeout(() => {
            setIsLoading(false);
            setDeleted(true);
            logoutHandler();
          }, 2000);
        }
      });
  };

  return (
    <ProfileSettingsLayout
      breadCrumbs={[
        {title: 'Settings', onPress: goBack},
        {title: 'Delete Account', onPress: null},
      ]}>
      {isLoading ? (
        <View style={{flex: 1, justifyContent: 'center'}}>
          <LoadingAnimation />
        </View>
      ) : (
        <View style={styles.container}>
          <View style={styles.detailsItem}>
            {deleted ? (
              <Text style={styles.detailLabel}>
                Your Account has been deleted. You are being redirected to Login
                screen automatically
              </Text>
            ) : (
              <Text style={styles.detailLabel}>
                Do You Really Want To Delete Your Account {userName} ?
              </Text>
            )}
            {/* <Text style={styles.detailValue}>{userName}</Text> */}
          </View>
          {/* <View style={styles.inputContainer}>
            <TextInput
              style={styles.input}
              placeholderTextColor={'#878788'}
              value={userNameInput}
              onChangeText={(text) => setUserNameInput(text)}
              placeholder="Enter New Username"
            />
          </View> */}
          {!deleted ? (
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity style={styles.changeButton} onPress={goBack}>
                <Text style={styles.changeButtonText}>NO</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles.changeButton1]}
                onPress={deleteAccount}>
                <Text style={styles.changeButtonText1}>YES</Text>
              </TouchableOpacity>
            </View>
          ) : null}
        </View>
      )}
    </ProfileSettingsLayout>
  );
};

export default DeleteAccountScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginBottom: 20,
  },
  detailsItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 20,
    paddingHorizontal: 20,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
  },
  detailLabel: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
  },
  detailValue: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'right',
  },
  inputContainer: {
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginTop: 25,
    alignItems: 'center',
    paddingHorizontal: 20,
    marginHorizontal: 20,
  },
  input: {
    flex: 1,
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    height: 50,
  },
  changeButton: {
    marginLeft: 20,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    width: 150,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  changeButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  changeButton1: {
    marginLeft: 20,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    width: 150,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
  },
  changeButtonText1: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: 'white',
  },
});
