/* eslint-disable react-native/no-inline-styles */
import {useNavigation} from '@react-navigation/native';
import React, {useContext, useEffect} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import ProfileSettingsItem from '../../components/ProfileSettingsItem';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import ProfileSettingsLayout from '../../layouts/ProfileSettingsLayout';

const ProfileSettingsScreen = () => {
  const navigation = useNavigation();

  const {setActiveRoute} = useContext(AppContext);

  useEffect(() => {
    navigation.addListener('focus', onScreenFocus);
    return () => {
      navigation.removeListener('focus', onScreenFocus);
    };
  }, []);

  const onScreenFocus = (paylod) => {
    setActiveRoute('Settings');
  };

  return (
    <ProfileSettingsLayout>
      <View style={styles.container}>
        <View style={styles.navContainer}>
          <FlatList
            showsHorizontalScrollIndicator={false}
            data={OPTIONS}
            keyExtractor={(item) => item.title}
            renderItem={({item}) => (
              <ProfileSettingsItem
                isDisabled={item.disabled}
                onPress={() =>
                  item.route ? navigation.navigate(item.route) : null
                }
                title={item.title}
                subText={item.subText}
              />
            )}
          />
        </View>
      </View>
    </ProfileSettingsLayout>
  );
};

export default ProfileSettingsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  navContainer: {
    flex: 1,
    backgroundColor: 'white',
  },
  navItemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 35,
  },
  navItemLabel: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    paddingVertical: 20,
  },
  changeText: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    textDecorationLine: 'underline',
  },
  switchContainer: {},
  switchButton: {
    borderColor: '#08152D',
    borderWidth: 1,
    borderRadius: 14,
    paddingHorizontal: 5,
    paddingVertical: 4,
  },
  switchDot: {
    width: 14,
    height: 14,
    borderRadius: 9,
    backgroundColor: '#08152D',
  },
  switchText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 11,
  },
  bottomTab: {
    flexDirection: 'row',
    marginHorizontal: -35,
  },
  bottomTabItem: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopColor: ThemeData.APP_MAIN_COLOR,
  },
  bottomTabText: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
  },
});

const OPTIONS = [
  {
    title: 'Change Name',
    subText: 'Update First, Last, & Your Nick Name',
    route: 'ChangeUsername',
  },
  {
    title: 'Change Password',
    subText: 'Update First, Last, & Your Nick Name',
    route: 'ChangePassword',
  },
  {
    title: 'Configure 2FA',
    subText: 'Update First, Last, & Your Nick Name',
    route: '2FASettings',
  },
  {
    title: 'Interest Settings',
    subText: 'Update the destination of your daily earnings',
    route: 'InterestSettings',
  },
  {
    title: 'Update Application',
    subText: 'Update the destination of your daily earnings',
    route: 'UpdateApp',
  },
  {
    title: 'Images And Bio',
    subText: 'Update First, Last, & Your Nick Name',
    disabled: true,
  },
  {
    title: 'Update Phone Number',
    subText: 'Update First, Last, & Your Nick Name',
    disabled: true,
  },
  {
    title: 'Delete Account',
    subText: 'Deactivate or Delete your Afflitate Account',
    route: 'DeleteAccount',
    disabled: false,
  },
];
