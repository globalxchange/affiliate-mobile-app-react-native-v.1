import React, {useState, useEffect} from 'react';
import {StyleSheet, View, BackHandler} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import AppStatusBar from '../components/AppStatusBar';
import LoadingAnimation from '../components/LoadingAnimation';
import {useNavigation} from '@react-navigation/native';
import SignupSteps from '../components/SignupSteps';

const SignupScreen = () => {
  const navigation = useNavigation();

  const [isLoading, setIsLoading] = useState();

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBack);
    };
  }, []);

  const handleBack = () => {
    navigation.goBack();
    return true;
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#FFFFFF'}}>
      <AppStatusBar backgroundColor="white" barStyle="dark-content" />
      {isLoading && (
        <View style={[StyleSheet.absoluteFill, styles.loadingContainer]}>
          <LoadingAnimation />
        </View>
      )}
      <View style={styles.container}>
        <SignupSteps isLoading={isLoading} setIsLoading={setIsLoading} />
      </View>
    </SafeAreaView>
  );
};

export default SignupScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  loadingContainer: {
    backgroundColor: 'white',
    zIndex: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
