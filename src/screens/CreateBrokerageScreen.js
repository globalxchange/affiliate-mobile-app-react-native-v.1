import {useNavigation} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ActionBar from '../components/ActionBar';
import CreateBrokerage from '../components/CreateBrokerage';
import AppMainLayout from '../layouts/AppMainLayout';

const CreateBrokerageScreen = () => {
  const navigation = useNavigation();

  const [isFocused, setIsFocused] = useState(false);

  useEffect(() => {
    navigation.addListener('focus', onScreenFocus);
    navigation.addListener('blur', onScreenBlur);
    return () => {
      navigation.addListener('focus', onScreenFocus);
      navigation.removeListener('blur', onScreenBlur);
    };
  }, []);

  const onScreenFocus = (paylod) => {
    setIsFocused(true);
  };

  const onScreenBlur = (paylod) => {
    setIsFocused(false);
  };

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      {isFocused && <CreateBrokerage />}
    </AppMainLayout>
  );
};

export default CreateBrokerageScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});
