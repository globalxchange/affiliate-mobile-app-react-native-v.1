import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
} from 'react-native';
import AppMainLayout from '../layouts/AppMainLayout';
import AppStatusBar from '../components/AppStatusBar';
import ActionBar from '../components/ActionBar';
import {useNavigation, useRoute} from '@react-navigation/native';
import {SharedElement} from 'react-navigation-shared-element';
import * as WebBrowser from 'expo-web-browser';
import FloatingButton from '../components/FloatingButton';

const TimelineDetailsScreen = () => {
  const navigation = useNavigation();
  const {params} = useRoute();

  const {item} = params;

  return (
    <AppMainLayout>
      <SharedElement id={'item.appBar'}>
        <ActionBar />
      </SharedElement>
      <ScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
        style={styles.container}>
        <SharedElement id={`item.${item.key}.photo`}>
          <Image
            source={
              item.thumbnail
                ? {uri: item.thumbnail}
                : require('../assets/step-placeholder.png')
            }
            resizeMode="cover"
            style={styles.coverImage}
          />
        </SharedElement>
        <View style={styles.detailsContainer}>
          <SharedElement id={`item.${item.key}.icon`}>
            <Image
              source={
                item.icon
                  ? {uri: item.icon}
                  : require('../assets/step-icon-placeholder.png')
              }
              style={styles.bankerIcon}
            />
          </SharedElement>
          <Text style={styles.bankerName}>{item.name}</Text>
          <View style={styles.actionContainer}>
            <TouchableOpacity style={styles.actionButton}>
              <Image
                source={require('../assets/mail-icon-colored.png')}
                style={styles.actionIcon}
                resizeMode="contain"
              />
            </TouchableOpacity>
            <TouchableOpacity style={styles.actionButton}>
              <Image
                source={require('../assets/chats-io-icon-colored.png')}
                style={styles.actionIcon}
                resizeMode="contain"
              />
            </TouchableOpacity>
            <TouchableOpacity style={styles.actionButton}>
              <Image
                source={require('../assets/onhold-icon-colored.png')}
                style={styles.actionIcon}
                resizeMode="contain"
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                WebBrowser.openBrowserAsync(item.howtovideolink || '')
              }
              style={styles.actionButton}>
              <Image
                source={require('../assets/youtube-icon-colored.png')}
                style={styles.actionIcon}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
          <Text style={styles.bankerDetails}>{item.description || ''}</Text>
        </View>
        <View style={styles.relatedContainer}>
          <Text style={styles.relatedHeader}>Related Institution</Text>
          <View style={styles.relatedItem}>
            <View style={styles.imageContainer}>
              <Image
                source={require('../assets/step-icon-placeholder.png')}
                style={styles.images}
              />
            </View>
            <View style={styles.relatedDetailsContainer}>
              <Text style={styles.relatedBankerHeader}>Institution</Text>
              <Text style={styles.relatedBankerName}>Canada</Text>
              <Text style={styles.relatedLeanMore}>
                Learn More About The Involvement Of This Institution
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.screenshotContainer}>
          <Text style={styles.relatedHeader}>Screenshots</Text>
          <FlatList
            showsHorizontalScrollIndicator={false}
            horizontal
            data={[]}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => (
              <View style={styles.screenshotItem}>
                <Image
                  source={{uri: item}}
                  style={styles.screenshotText}
                  resizeMode="cover"
                />
              </View>
            )}
            ListEmptyComponent={
              <View style={styles.emptyContainer}>
                <Text style={styles.emptyText}>No Screenshots Available</Text>
              </View>
            }
          />
        </View>
      </ScrollView>
      <FloatingButton
        onPress={navigation.goBack}
        icon={require('../assets/cancel-icon-white.png')}
      />
    </AppMainLayout>
  );
};

export default TimelineDetailsScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  coverImage: {
    height: 250,
    width: '100%',
  },
  detailsContainer: {
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingBottom: 25,
  },
  bankerIcon: {
    height: 80,
    width: 80,
    marginTop: -40,
  },
  bankerName: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 22,
    marginBottom: 10,
    marginTop: 10,
    textAlign: 'center',
  },
  actionContainer: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  actionButton: {
    marginHorizontal: 10,
    height: 30,
    width: 30,
    padding: 5,
  },
  actionIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  bankerDetails: {
    textAlign: 'center',
    fontFamily: 'Montserrat',
    fontSize: 11,
    color: '#9A9A9A',
  },
  relatedContainer: {
    paddingHorizontal: 20,
    marginBottom: 15,
  },
  relatedHeader: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    marginBottom: 5,
  },
  relatedItem: {
    flexDirection: 'row',
    borderColor: '#EBEBEB',
    borderWidth: 1,
  },
  imageContainer: {
    height: '100%',
    width: 90,
    borderRightColor: '#EBEBEB',
    borderRightWidth: 1,
  },
  images: {
    flex: 1,
    height: null,
    width: null,
  },
  relatedDetailsContainer: {
    flex: 1,
    padding: 10,
  },
  relatedBankerHeader: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
  },
  relatedBankerName: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 10,
    marginBottom: 10,
  },
  relatedLeanMore: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 8,
    textDecorationLine: 'underline',
  },
  screenshotContainer: {
    paddingHorizontal: 20,
    marginBottom: 15,
  },
  screenshotItem: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    height: 100,
    width: 100,
    marginRight: 5,
  },
  screenshotText: {
    flex: 1,
    height: null,
    width: null,
  },
  emptyText: {
    fontSize: 10,
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    textAlign: 'center',
  },
});
