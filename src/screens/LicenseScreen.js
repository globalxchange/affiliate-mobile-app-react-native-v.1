import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ActionBar from '../components/ActionBar';
import AppMainLayout from '../layouts/AppMainLayout';
import {useRoute} from '@react-navigation/native';
import BrokerFragment from '../components/BrokerViews/Fragments/BrokerFragment';

const LicenseScreen = () => {
  const {params} = useRoute();

  const [activeLicense, setActiveLicense] = useState('');

  const {activeBanker} = params;

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      <View style={styles.container}>
        <BrokerFragment
          activeBanker={activeBanker}
          setActiveLicense={setActiveLicense}
        />
      </View>
    </AppMainLayout>
  );
};

export default LicenseScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
});
