import {
  KeyboardAvoidingView,
  StyleSheet,
  Image,
  Text,
  View,
  TextInput,
  Platform,
  TouchableOpacity,
} from 'react-native';
import axios from 'axios';
import React, {useContext, useState} from 'react';
import {useMutation} from 'react-query';
import {useNavigation, useRoute} from '@react-navigation/native';
import {WToast} from 'react-native-smart-tip';
import {AppContext} from '../contexts/AppContextProvider';
import AppMainLayout from '../layouts/AppMainLayout';
import ActionBar from '../components/ActionBar';
import ThemeData from '../configs/ThemeData';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';
import {GX_API_ENDPOINT} from '../configs';
import Exchange_Logo from '../assets/selectBrandSvgicons/exchange-fee-icon.svg';
async function updateRateApi(params) {
  const token = await AsyncStorageHelper.getAppToken();
  const {data} = await axios.post(
    `${GX_API_ENDPOINT}/coin/trade/user/fees/set`,
    {token, ...params},
  );
  return data;
}

const ExchangeFeeUpdateScreen = () => {
  const [value, setValue] = useState('');
  const {userEmail} = useContext(AppContext);
  const {goBack} = useNavigation();
  const {
    params: {selectedBrand, pair, refetchPairs},
  } = useRoute();
  const {mutate: updateRate, isLoading} = useMutation(updateRateApi, {
    onSuccess: () => {
      WToast.show({data: 'Rate Updated'});
      refetchPairs();
      goBack();
    },
  });
  console.log(selectedBrand);
  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      <KeyboardAvoidingView
        style={styles.mainView}
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
        <TouchableOpacity onPress={goBack} style={styles.iconWrap}>
          <Image source={selectedBrand.icon} style={styles.icon} />
        </TouchableOpacity>
        <View style={styles.inputWrap}>
          <View style={styles.pairWrap}>
            <Text style={styles.pairText}>{pair}</Text>
          </View>
          <TextInput
            style={styles.textInput}
            placeholder="0.00%"
            value={value}
            onChangeText={setValue}
            returnKeyType={Platform.OS === 'ios' ? 'done' : 'next'}
            keyboardType="decimal-pad"
            onSubmitEditing={() => {
              updateRate({
                email: userEmail,
                buy: pair.split('/')[0],
                sell: pair.split('/')[1],
                percentage: parseFloat(value),
                default: false,
              });
            }}
          />
        </View>
      </KeyboardAvoidingView>
      {isLoading && (
        <View style={styles.loader}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            {selectedBrand.svgicon}
            <Text style={[styles.pairText, {fontSize: 30, marginLeft: '2%'}]}>
              {selectedBrand.title}
            </Text>
          </View>
          <Text style={[styles.pairText, {position: 'absolute', bottom: '3%'}]}>
            Updating Fees For {pair}
          </Text>
        </View>
      )}
    </AppMainLayout>
  );
};

export default ExchangeFeeUpdateScreen;

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    alignItems: 'center',
  },
  loader: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  iconWrap: {
    height: 50,
    width: '70%',
    marginVertical: 40,
  },
  icon: {
    height: 50,
    width: '100%',
    resizeMode: 'contain',
  },
  inputWrap: {
    width: '90%',
    height: 65,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    flexDirection: 'row',
    borderRadius: 10,
    marginTop: '10%',
  },
  pairWrap: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    borderRightWidth: 1,
    borderRightColor: ThemeData.BORDER_COLOR,
  },
  pairText: {
    fontSize: 17,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.TEXT_COLOR,
  },
  textInput: {
    flex: 1,
    paddingHorizontal: 30,
    textAlign: 'right',
    fontSize: 18,
  },
});
