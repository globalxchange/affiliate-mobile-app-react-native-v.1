import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ActionBar from '../components/ActionBar';
import ComingSoon from '../components/ComingSoon';
import AppMainLayout from '../layouts/AppMainLayout';

const NetworkCenterScreen = () => {
  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      <ComingSoon />
    </AppMainLayout>
  );
};

export default NetworkCenterScreen;

const styles = StyleSheet.create({});
