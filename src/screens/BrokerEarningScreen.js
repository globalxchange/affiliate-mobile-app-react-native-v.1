/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useEffect, useState, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  BackHandler,
  Keyboard,
  TextInput,
  Image,
} from 'react-native';
import {useRoute, useNavigation} from '@react-navigation/native';
import {AppContext} from '../contexts/AppContextProvider';
import AppMainLayout from '../layouts/AppMainLayout';
import ActionBar from '../components/ActionBar';
import BrokerEarningCarousel from '../components/BrokerEarningCarousel';
import Animated, {
  Value,
  interpolate,
  useCode,
  set,
  Clock,
} from 'react-native-reanimated';
import ReanimatedTimingHelper from '../utils/ReanimatedTimingHelper';
import BrokerUsers from '../components/BrokerUsers';
import BrokerTransactionList from '../components/BrokerTransactionsList';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Axios from 'axios';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';
import {GX_API_ENDPOINT} from '../configs';
import {usdValueFormatter} from '../utils';

const BrokerEarningScreen = () => {
  const {params} = useRoute();
  const navigation = useNavigation();
  const {setActiveRoute} = useContext(AppContext);
  const [searchText, setSearchText] = useState('');
  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);
  const [selectedUser, setSelectedUser] = useState();
  const [isBrokers, setIsBrokers] = useState(true);
  const [totalTransaction, setTotalTransaction] = useState();
  const [transactionalVolume, setTransactionalVolume] = useState();
  const [transactionalFees, setTransactionalFees] = useState();
  const [transactionalRevenue, setTransactionalRevenue] = useState();

  const headerAnimation = useRef(new Value(1));

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBack);
    navigation.addListener('focus', onScreenFocus);
    Keyboard.addListener('keyboardDidShow', onKeyBoardShow);
    Keyboard.addListener('keyboardDidHide', onKeyBoardHidden);

    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBack);
      navigation.removeListener('focus', onScreenFocus);
      Keyboard.removeListener('keyboardDidShow', onKeyBoardShow);
      Keyboard.removeListener('keyboardDidHide', onKeyBoardHidden);
    };
  }, []);

  useEffect(() => {
    (async () => {
      const email = await AsyncStorageHelper.getLoginEmail();

      Axios.get(`${GX_API_ENDPOINT}/brokerage/otc/txn/stats/get`, {
        params: {
          email,
          type: params?.type ? params?.type.toLowerCase() : 'direct',
        },
      })
        .then((resp) => {
          const {data} = resp;

          console.log(params?.type, 'Resp', data);

          if (data.status) {
            // setTotalTransaction(data.logs);
            setTransactionalVolume(data.total_txn_volume);
            setTransactionalFees(data.total_txn_fees);
            setTransactionalRevenue(data.total_txn_revenue);
          }
        })
        .catch((error) => {
          console.log('Error on getting transactions');
        });
    })();
  }, [params]);

  useCode(
    () =>
      isKeyboardOpen
        ? [
            set(
              headerAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ]
        : [
            set(
              headerAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ],
    [isKeyboardOpen],
  );

  const handleBack = () => {
    navigation.goBack();
    return true;
  };

  const onScreenFocus = (paylod) => {
    setActiveRoute('Earn');
  };

  const onKeyBoardShow = () => {
    setIsKeyboardOpen(true);
  };

  const onKeyBoardHidden = () => {
    setIsKeyboardOpen(false);
  };

  const carouselData = [
    {
      title: `${params?.type || 'Direct'} Transactional Volume`,
      value: usdValueFormatter.format(transactionalVolume || 0),
    },
    {
      title: `${params?.type || 'Direct'} Transactional Fees`,
      value: usdValueFormatter.format(transactionalFees || 0),
    },
    {
      title: `${params?.type || 'Direct'} Transactional Revenue`,
      value: usdValueFormatter.format(transactionalRevenue || 0),
    },
  ];

  const onSelectUser = (item) => {
    Keyboard.dismiss();
    setSearchText('');
    setSelectedUser(item);
  };

  let isSearchOpen = isKeyboardOpen || searchText;

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      <View style={styles.container}>
        <Animated.View
          style={{
            maxHeight: interpolate(headerAnimation.current, {
              inputRange: [0, 1],
              outputRange: [0, 300],
            }),
            overflow: 'hidden',
          }}>
          <Text style={styles.header}>
            {params?.type || 'Direct'}{' '}
            {params?.type === 'Override' ? '' : 'Network '}Earnings
          </Text>
          <Text style={styles.subHeader}>Powered By AffiliateApp</Text>
          <BrokerEarningCarousel carouselData={carouselData} />
        </Animated.View>
        <View style={styles.searchContainer}>
          {params?.type === 'Indirect' && (
            <TouchableOpacity
              style={styles.switcher}
              onPress={() => setIsBrokers(!isBrokers)}>
              <Text style={styles.switchText}>
                {isBrokers ? 'Broker' : 'Customers'}
              </Text>
            </TouchableOpacity>
          )}

          <TextInput
            style={styles.searchInput}
            onChangeText={(text) => setSearchText(text)}
            value={searchText}
            autoFocus={false}
            placeholder={`Search Name Of ${params?.type || ''} ${
              params?.type ? (isBrokers ? 'Brokers' : 'Customers') : 'Customers'
            }`}
            placeholderTextColor={'#878788'}
          />
          <Image
            style={styles.searchIcon}
            source={require('../assets/search-icon.png')}
            resizeMode="contain"
          />
        </View>
        <BrokerUsers
          horizontal={!isSearchOpen}
          searchText={searchText}
          onItemSelected={onSelectUser}
          selectedItem={selectedUser}
          type={params?.type || 'Direct'}
          isBrokers={isBrokers}
        />
        <BrokerTransactionList
          hidden={isSearchOpen}
          selectedUser={selectedUser}
          type={params?.type || 'Direct'}
        />
      </View>
    </AppMainLayout>
  );
};

export default BrokerEarningScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    fontFamily: 'Montserrat-Bold',
    textAlign: 'center',
    color: '#08152D',
    fontSize: 18,
    marginTop: 20,
  },
  subHeader: {
    fontFamily: 'Montserrat',
    textAlign: 'center',
    color: '#08152D',
    fontSize: 14,
    marginBottom: 10,
  },
  searchContainer: {
    marginHorizontal: 30,
    flexDirection: 'row',
    marginTop: 30,
    height: 40,
    paddingHorizontal: 15,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.15,
    shadowRadius: 2,
    backgroundColor: 'white',
    borderRadius: 2,
    elevation: 12,
  },
  searchInput: {
    flexGrow: 1,
    width: 0,
    marginTop: 'auto',
    marginBottom: 'auto',
    paddingHorizontal: 15,
    fontFamily: 'Montserrat',
    fontWeight: 'normal',
    color: 'black',
    fontSize: 13,
  },
  searchIcon: {
    height: 15,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  switcher: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  switchText: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 10,
    borderRightColor: '#CACACA',
    borderRightWidth: 1,
    paddingRight: 15,
  },
});
