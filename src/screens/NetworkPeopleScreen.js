/* eslint-disable react-native/no-inline-styles */
import {useNavigation, useRoute} from '@react-navigation/native';
import Axios from 'axios';
import React, {useEffect, useRef, useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ActionBar from '../components/ActionBar';
import PeopleCarousel from '../components/PeopleCarousel';
import {GX_API_ENDPOINT} from '../configs';
import ThemeData from '../configs/ThemeData';
import AppMainLayout from '../layouts/AppMainLayout';
import SearchLayout from '../layouts/SearchLayout';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';

const NetworkPeopleScreen = () => {
  const {navigate} = useNavigation();
  const {params} = useRoute();

  const {item, ddLevel, userEmail} = params;

  const [users, setUsers] = useState();
  const [allUsersList, setAllUsersList] = useState();
  const [isSearchActive, setIsSearchActive] = useState(false);
  const [searchInput, setSearchInput] = useState('');
  const [activeFilter, setActiveFilter] = useState(INPUT_OPTIONS[0]);
  const [selectedUser, setSelectedUser] = useState();
  const [isPopupOpen, setIsPopupOpen] = useState(false);
  const [scrollIndex, setScrollIndex] = useState(0);

  useEffect(() => {
    (async () => {
      const affiliateId = await AsyncStorageHelper.getAffId();

      Axios.get(`${GX_API_ENDPOINT}/brokerage/stats/getUsers`, {
        params: {affiliate_id: affiliateId},
      })
        .then((resp) => {
          const {data} = resp;

          if (data.status) {
            const allUsers = data?.all?.users;

            // console.log('USers', allUsers);

            setAllUsersList(allUsers);
          }
        })
        .catch((error) => {
          console.log('Error on getting all users list', error);
        });
    })();
  }, []);

  useEffect(() => {
    if (item) {
      setUsers(item.users || []);
    }
  }, [item]);

  useEffect(() => {
    (async () => {
      if (params?.ddLevel !== undefined || params?.ddLevel !== null) {
        setUsers();

        let email;
        if (params?.userEmail) {
          email = params?.userEmail;
        } else {
          email = await AsyncStorageHelper.getLoginEmail();
        }

        Axios.get(`${GX_API_ENDPOINT}/brokerage/get/broker/users/by/levels`, {
          params: {
            email,
          },
        })
          .then(({data}) => {
            // console.log('Levels', data);

            const levels = data?.levels || [];

            const selectedLevel = levels.find(
              (x) => x.ddlevel === (params?.ddLevel || 0),
            );

            setUsers(selectedLevel?.users || []);
          })
          .catch((error) => {
            console.log('Error on getting Network data of the user', error);
          })
          .finally(() => {});
      }
    })();
  }, [params]);

  // useEffect(() => {
  //   if (allUsersList) {
  //     const searchQuery = searchInput.toLowerCase().trim();

  //     const filteredUsers = allUsersList.filter(
  //       (x) =>
  //         x.name.toLowerCase().includes(searchQuery) ||
  //         x.email.toLowerCase().includes(searchInput),
  //     );

  //     setUsers(filteredUsers);
  //   }
  // }, [allUsersList, searchInput]);

  const onSearchItemClick = (userEmail) => {
    setIsSearchActive(false);
    setTimeout(() => {
      setSelectedUser(userEmail);
      setIsPopupOpen(true);

      const index = users?.findIndex(
        (userItem) => userItem.email === userEmail,
      );
      setScrollIndex(index);
    }, 500);
  };

  return (
    <AppMainLayout isBottomNav>
      <ActionBar />
      {isSearchActive ? (
        <SearchLayout
          value={searchInput}
          setValue={setSearchInput}
          onBack={() => setIsSearchActive(false)}
          showUserList
          onSubmit={onSearchItemClick}
          placeholder="Search User"
          keyboardOffset={100}
          list={users}
          filters={INPUT_OPTIONS}
          setSelectedFilter={setActiveFilter}
          selectedFilter={activeFilter}
        />
      ) : (
        <>
          <Text style={styles.header}>
            You Have {users?.length || 0} Users On
          </Text>
          <Text style={styles.subHeader}>
            Depth Differential {item?.ddlevel || params?.ddLevel || 0}
          </Text>
          <View style={styles.actionsContainer}>
            <TouchableOpacity
              onPress={() => navigate('ChainView')}
              style={[styles.actionButton, {marginRight: 10}]}>
              <Image
                source={require('../assets/chain-view-icon.png')}
                resizeMode="contain"
                style={styles.actionIcon}
              />
              <Text style={styles.actionText}>ChainView™</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setIsSearchActive(true)}
              style={styles.actionButton}>
              <Image
                source={require('../assets/search-icon-colorful.png')}
                resizeMode="contain"
                style={styles.actionIcon}
              />
              <Text style={styles.actionText}>Search</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.carouselContainer}>
            <PeopleCarousel
              users={users}
              setSelectedUser={setSelectedUser}
              selectedUser={selectedUser}
              isPopupOpen={isPopupOpen}
              setIsPopupOpen={setIsPopupOpen}
              scrollIndex={scrollIndex}
            />
          </View>
        </>
      )}
    </AppMainLayout>
  );
};

export default NetworkPeopleScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    textAlign: 'center',
    fontSize: 24,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: '#08152D',
    marginTop: 30,
  },
  subHeader: {
    marginBottom: 30,
    textAlign: 'center',
    color: '#08152D',
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 13,
    marginTop: 5,
  },
  searchContainer: {
    flexDirection: 'row',
    marginHorizontal: 30,
    borderColor: '#EBEBEB',
    borderWidth: 1,
    alignItems: 'center',
    height: 50,
  },
  switchButton: {
    paddingHorizontal: 15,
    borderRightColor: '#EBEBEB',
    borderRightWidth: 1,
    height: '100%',
    justifyContent: 'center',
  },
  switchButtonText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    fontSize: 12,
  },
  input: {
    flex: 1,
    height: '100%',
    paddingHorizontal: 15,
    fontFamily: 'Montserrat',
    color: '#08152D',
  },
  searchIcon: {
    width: 15,
    height: 15,
    marginRight: 15,
  },
  carouselContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  list: {
    marginTop: 20,
  },
  userContainer: {
    flexDirection: 'row',
    paddingHorizontal: 40,
    paddingVertical: 25,
    backgroundColor: 'white',
    marginBottom: 10,
    alignItems: 'center',
  },
  cryptoIcon: {
    width: 30,
    height: 30,
    borderRadius: 15,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  nameContainer: {
    flexGrow: 1,
    width: 0,
    marginLeft: 10,
  },
  countryName: {
    color: '#001D41',
    fontSize: 16,
    marginBottom: 2,
    fontFamily: 'Montserrat-Bold',
    paddingRight: 10,
  },
  priceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  methodsNo: {
    color: '#001D41',
    opacity: 0.5,
    marginBottom: 2,
    textAlign: 'right',
    fontFamily: 'Roboto-Bold',
    marginRight: 10,
    fontSize: 14,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  actionsContainer: {
    flexDirection: 'row',
    paddingHorizontal: 50,
  },
  actionButton: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingVertical: 10,
  },
  actionIcon: {
    width: 16,
    height: 16,
    marginRight: 8,
  },
  actionText: {
    fontFamily: ThemeData.FONT_MEDIUM,
    fontSize: 12,
  },
});

const INPUT_OPTIONS = [
  {title: 'Email', paramKey: 'email'},
  {title: 'Name', paramKey: 'name'},
  {title: 'Username', paramKey: 'username'},
];
