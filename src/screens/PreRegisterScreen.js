import {useNavigation} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {BackHandler, StyleSheet, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import AppStatusBar from '../components/AppStatusBar';
import LoadingAnimation from '../components/LoadingAnimation';
import PreRegisterSteps from '../components/PreRegisterSteps';

const PreRegisterScreen = () => {
  const navigation = useNavigation();

  const [isLoading, setIsLoading] = useState();

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBack);
    };
  }, []);

  const handleBack = () => {
    navigation.goBack();
    return true;
  };

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <AppStatusBar backgroundColor="white" barStyle="dark-content" />
      {isLoading && (
        <View style={[StyleSheet.absoluteFill, styles.loadingContainer]}>
          <LoadingAnimation />
        </View>
      )}
      <View style={styles.container}>
        <PreRegisterSteps isLoading={isLoading} setIsLoading={setIsLoading} />
      </View>
    </SafeAreaView>
  );
};

export default PreRegisterScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
  backButton: {
    marginLeft: 20,
    width: 40,
    height: 40,
  },
  backIcon: {
    flex: 1,
    width: null,
    height: null,
  },
  loadingContainer: {
    backgroundColor: 'white',
    zIndex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
