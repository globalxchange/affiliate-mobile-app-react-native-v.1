/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useRef, useState} from 'react';
import {
  FlatList,
  Image,
  Keyboard,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeData from '../configs/ThemeData';
import Clipboard from '@react-native-community/clipboard';
import Animated, {
  Clock,
  interpolate,
  set,
  useCode,
} from 'react-native-reanimated';
import ReanimatedTimingHelper from '../utils/ReanimatedTimingHelper';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../configs';
import LoadingAnimation from '../components/LoadingAnimation';
import ProfileAvatar from '../components/ProfileAvatar';
import SkeltonItem from '../components/SkeltonItem';

const SearchLayout = ({
  placeholder,
  value,
  setValue,
  onBack,
  onSubmit,
  keyboardOffset = 0,
  filters,
  selectedFilter,
  setSelectedFilter,
  disableFilter,
  showUserList,
  list,
  isLoading,
}) => {
  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);
  const [usersList, setUsersList] = useState();
  const [filteredList, setFilteredList] = useState();

  const chatKeyboardAnimation = useRef(new Animated.Value(0));

  useEffect(() => {
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
      onKeyBoardShow,
    );
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
      onKeyBoardHide,
    );
    return () => {
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
        onKeyBoardShow,
      );
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
        onKeyBoardHide,
      );
    };
  }, []);

  useEffect(() => {
    if (list) {
      setUsersList(list);
    } else {
      (async () => {
        const affId = await AsyncStorageHelper.getAffId();

        Axios.post(`${GX_API_ENDPOINT}/get_broker_names`, {affiliate_id: affId})
          .then((resp) => {
            const {data} = resp;
            // console.log('Users List', data);
            setUsersList(data || []);
          })
          .catch((error) => {
            console.log('Error on getting AppList', error);
          });
      })();
    }
  }, [list]);

  useEffect(() => {
    const searchQuery = value.trim().toLowerCase();

    if (usersList) {
      if (selectedFilter) {
        const filterList = usersList.filter((item) => {
          let found = false;
          const searchData = item[selectedFilter.paramKey] || '';

          found = searchData?.toLowerCase().includes(searchQuery);

          return found;
        });
        setFilteredList(filterList);
      } else {
        const filterList = usersList.filter(
          (item) =>
            item?.email?.toLowerCase()?.includes(searchQuery) ||
            item?.name?.toLowerCase()?.includes(searchQuery) ||
            item?.coinName?.toLowerCase()?.includes(searchQuery) ||
            item?.coinSymbol?.toLowerCase()?.includes(searchQuery),
        );

        setFilteredList(filterList);
      }
    }
  }, [value, usersList, selectedFilter]);

  useCode(
    () =>
      isKeyboardOpen
        ? [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ]
        : [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ],
    [isKeyboardOpen],
  );

  const onKeyBoardShow = (e) => {
    setKeyboardHeight(e.endCoordinates.height);
    setIsKeyboardOpen(true);
  };

  const onKeyBoardHide = () => {
    setIsKeyboardOpen(false);
  };

  const onPasteHandler = async () => {
    const copiedText = await Clipboard.getString();

    setValue(copiedText.trim());
  };

  const onUserSelected = (selectedUser) => {
    onSubmit(selectedUser.email, selectedUser);
  };

  return (
    <View style={styles.container}>
      <View style={styles.searchContainer}>
        <TouchableOpacity onPress={onBack} style={styles.actionButton}>
          <Image
            style={styles.actionIcon}
            resizeMode="contain"
            source={require('../assets/back-short.png')}
          />
        </TouchableOpacity>
        <TextInput
          placeholderTextColor={'#878788'}
          placeholder={placeholder || ''}
          value={value}
          onChangeText={(text) => setValue(text)}
          style={styles.input}
          autoFocus
          returnKeyType="search"
          onEndEditing={showUserList ? null : () => onSubmit('')}
        />
        <TouchableOpacity onPress={onPasteHandler} style={styles.actionButton}>
          <Image
            style={styles.actionIcon}
            resizeMode="contain"
            source={require('../assets/paste-icon-blue.png')}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={showUserList ? null : () => onSubmit('')}
          style={styles.actionButton}>
          <Image
            style={styles.actionIcon}
            resizeMode="contain"
            source={require('../assets/search-modern.png')}
          />
        </TouchableOpacity>
      </View>
      {disableFilter ? null : (
        <View style={styles.filterContainer}>
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            {filters?.map((item) => (
              <TouchableOpacity
                disabled={disableFilter}
                key={item.title || item.coinSymbol}
                onPress={() => setSelectedFilter(item)}>
                <View
                  style={[
                    styles.inputType,
                    selectedFilter?.title === item.title && {opacity: 1},
                  ]}>
                  <Text
                    style={[
                      styles.inputTypeName,
                      selectedFilter?.title === item.title && {
                        fontFamily: ThemeData.FONT_SEMI_BOLD,
                      },
                    ]}>
                    {item.title}
                  </Text>
                </View>
              </TouchableOpacity>
            ))}
          </ScrollView>
        </View>
      )}
      <View style={{flex: 1, paddingHorizontal: 20}}>
        {isLoading || (showUserList && !usersList) ? (
          <SkeltonAnimation />
        ) : showUserList && usersList ? (
          <FlatList
            data={filteredList}
            showsVerticalScrollIndicator={false}
            key={(item) => item.id}
            keyExtractor={(item) => item._id || item.email}
            renderItem={({item}) => (
              <TouchableOpacity onPress={() => onUserSelected(item)}>
                <View style={styles.item}>
                  <ProfileAvatar
                    name={item.name}
                    avatar={item.profile_img || item.coinImage}
                    size={35}
                  />
                  <View style={styles.nameContainer}>
                    <Text style={styles.countryName}>
                      {item.name || item.coinName}
                    </Text>
                    {item.email ? (
                      <Text style={styles.userEmail}>{item.email}</Text>
                    ) : null}
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />
        ) : null}
      </View>
      <Animated.View
        style={[
          {
            height: interpolate(chatKeyboardAnimation.current, {
              inputRange: [0, 1],
              outputRange: [0, keyboardHeight - (keyboardOffset || 0)],
            }),
          },
        ]}
      />
    </View>
  );
};

const SkeltonAnimation = () => (
  <View style={{flex: 1, overflow: 'hidden'}}>
    {Array(8)
      .fill(0)
      .map((_, index) => (
        <View style={styles.item}>
          <SkeltonItem itemWidth={35} itemHeight={35} />

          <View style={styles.nameContainer}>
            <SkeltonItem
              itemWidth={150}
              itemHeight={20}
              style={styles.countryName}
            />
            <SkeltonItem
              itemWidth={100}
              itemHeight={10}
              style={{marginTop: 5}}
            />
          </View>
        </View>
      ))}
  </View>
);

export default SearchLayout;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  searchContainer: {
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  actionButton: {
    height: 60,
    width: 60,
    padding: 20,
    borderLeftColor: ThemeData.BORDER_COLOR,
    borderLeftWidth: 1,
  },
  actionIcon: {
    flex: 1,
    width: null,
    height: null,
  },
  input: {
    paddingVertical: 15,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 14,
    flex: 1,
  },
  filterContainer: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    paddingVertical: 20,
  },
  inputType: {
    paddingHorizontal: 20,
    paddingVertical: 8,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginRight: 10,
    opacity: 0.4,
    minWidth: 85,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputTypeName: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 11,
    color: ThemeData.APP_MAIN_COLOR,
  },
  item: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 25,
    backgroundColor: 'white',
    marginBottom: 10,
    alignItems: 'center',
  },
  cryptoIcon: {
    width: 25,
    height: 25,
    borderRadius: 12.5,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  nameContainer: {
    flexGrow: 1,
    width: 0,
    marginLeft: 10,
  },
  countryName: {
    color: '#001D41',
    fontSize: 16,
    marginBottom: 2,
    fontFamily: 'Montserrat-Bold',
  },
  userEmail: {
    color: '#001D41',
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  priceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
