/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useState} from 'react';
import {
  Dimensions,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ActionBar from '../components/ActionBar';
import ProfileAvatar from '../components/ProfileAvatar';
import SettingsScreenHeader from '../components/SettingsScreenHeader';
import ThemeData from '../configs/ThemeData';
import {AppContext} from '../contexts/AppContextProvider';
import AppMainLayout from './AppMainLayout';
import BackBendIcon from '../assets/VectorIcons/back-bend-icon.svg';
import {useNavigation} from '@react-navigation/native';

const {width} = Dimensions.get('window');

const ProfileSettingsLayout = ({
  children,
  header,
  subHeader,
  breadCrumbs,
  breadCrumbPos,
  isRoot,
}) => {
  const {goBack} = useNavigation();

  const {avatar, fullName, userEmail} = useContext(AppContext);

  const [activeBottomTab, setActiveBottomTab] = useState(BOTTOM_TABS[1]);

  return (
    <AppMainLayout>
      {isRoot ? (
        <ActionBar />
      ) : (
        <View style={styles.profileToolbar}>
          <ProfileAvatar avatar={avatar} name={fullName} size={50} />
          <View style={styles.profileData}>
            <Text style={styles.profileName}>{fullName}</Text>
            <Text style={styles.profileEmail}>{userEmail}</Text>
          </View>
          <TouchableOpacity style={styles.backButton} onPress={goBack}>
            <BackBendIcon
              width={16}
              height={16}
              color={ThemeData.APP_MAIN_COLOR}
            />
          </TouchableOpacity>
        </View>
      )}
      <SettingsScreenHeader
        header={header}
        subHeader={subHeader}
        breadCrumbs={breadCrumbs}
        breadCrumbPos={breadCrumbPos}
        isRoot={isRoot}
      />
      <View style={styles.container}>{children}</View>
      <View style={styles.bottomTab}>
        <FlatList
          data={BOTTOM_TABS}
          horizontal
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item) => item.title}
          renderItem={({item}) => (
            <TouchableOpacity
              key={item.title}
              disabled={item.disabled}
              onPress={() => setActiveBottomTab(item)}>
              <View
                style={[
                  styles.bottomTabItem,
                  item.disabled && {opacity: 0.4},
                  activeBottomTab?.title === item.title && {
                    borderTopWidth: 1,
                  },
                ]}>
                <Text
                  style={[
                    styles.bottomTabText,
                    activeBottomTab?.title === item.title && {
                      fontFamily: ThemeData.FONT_SEMI_BOLD,
                    },
                  ]}>
                  {item.title}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
    </AppMainLayout>
  );
};

export default ProfileSettingsLayout;

const styles = StyleSheet.create({
  container: {flex: 1, paddingHorizontal: 20, paddingBottom: 10},
  bottomTab: {
    flexDirection: 'row',
  },
  bottomTabItem: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopColor: ThemeData.APP_MAIN_COLOR,
    width: width / 4,
  },
  bottomTabText: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
  },
  profileToolbar: {
    flexDirection: 'row',
    height: 70,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    borderTopColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  profileData: {
    flex: 1,
    paddingHorizontal: 15,
  },
  profileName: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 20,
    textTransform: 'capitalize',
  },
  profileEmail: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 11,
  },
  backButton: {
    borderLeftColor: ThemeData.BORDER_COLOR,
    borderLeftWidth: 1,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 20,
  },
});

const BOTTOM_TABS = [
  {title: 'XID', disabled: true},
  {title: 'Settings'},
  {title: 'KYC', disabled: true},
  {title: 'Brain', disabled: true},
];
