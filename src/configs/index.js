export const listCategories = [
  {title: 'Crypto', icon: require('../assets/category-icons/crypto-icon.png')},
  {title: 'Fiat', icon: require('../assets/money-icon.png')},
  {
    title: 'Methods',
    icon: require('../assets/category-icons/vendors-icon.png'),
  },
  {
    title: 'Bankers',
    icon: require('../assets/category-icons/exchanges-icon.png'),
  },
  // {
  //   title: 'Tellers',
  //   icon: require('../assets/category-icons/tellers-icon.png'),
  // },
  {title: 'Add', icon: require('../assets/add-colored.png')},
];

export const GX_API_ENDPOINT = 'https://comms.globalxchange.io';

export const paymentMethod = [
  {
    name: 'Credit Card',
    nos: '2 Vendors',
    image: require('../assets/card-icon.png'),
    isEnabled: true,
  },
  {
    name: 'Bank Transfers',
    nos: '0 Vendors',
    image: require('../assets/business-and-finance.png'),
    isEnabled: false,
  },
  {
    name: 'Cash',
    nos: '0 Vendors',
    image: require('../assets/money-icon.png'),
    isEnabled: false,
  },
  {
    name: 'Other Vendors',
    nos: '0 Vendors',
    image: require('../assets/other-payment.png'),
    isEnabled: false,
  },
];

export const vendorsList = [
  {
    title: 'Koinal',
    image: require('../assets/koinal-logo.png'),
    isEnabled: true,
  },
  {
    title: 'Simplex',
    image: require('../assets/simplex-logo.png'),
    isEnabled: false,
  },
];

export const supportTabs = [
  {
    title: 'Learn',
    icon: require('../assets/support-category-icons/faq-icon.png'),
  },
  {
    title: 'Bots',
    icon: require('../assets/support-category-icons/bots-icon.png'),
    loginRequired: true,
  },
  {
    title: 'Chat',
    icon: require('../assets/one-to-one-chat-icon.png'),
    loginRequired: true,
  },
  {
    title: 'Ticket',
    icon: require('../assets/support-category-icons/ticket-icon.png'),
    loginRequired: true,
  },
];

const isDev = false;

export const supportSocketUrl = isDev
  ? 'https://715483d1316c.ngrok.io'
  : 'https://insta-webrtc-signal-server.herokuapp.com';

export const fiatCurrencies = [
  {currency: 'US Dollar', coinSymbol: 'USD'},
  {currency: 'Indian Rupee', coinSymbol: 'INR'},
  {currency: 'Australian Dollar', coinSymbol: 'AUD'},
  {currency: 'British Pound', coinSymbol: 'GBP'},
  {currency: 'Canadian Dollar', coinSymbol: 'CAD'},
  {currency: 'Euro', coinSymbol: 'EUR'},
  {currency: 'Chinese Yuan', coinSymbol: 'CNY'},
  {currency: 'Japanese Yen', coinSymbol: 'JPY'},
  {currency: 'Emirati Dirham', coinSymbol: 'AED'},
];

export const cryptoCurrencies = [
  {currency: 'Bitcoin', coinSymbol: 'BTC'},
  {currency: 'Ethereum', coinSymbol: 'ETH'},
  {currency: 'Tether', coinSymbol: 'USDT'},
  {currency: 'GX Token', coinSymbol: 'GXT'},
];

export const APP_CODE = 'broker_app';

export const TELLER_API_ENDPOINT = 'https://tellerapi.apimachine.com';

export const GX_CHAT_SOCKET_ENDPOINT = 'https://chatsapi.globalxchange.io';

export const supportedCoins = ['BTC', 'ETC', 'USDT', 'GXT', 'USD', 'LTC'];

export const COGNITO_USER_POOL_ID = 'us-east-2_ALARK1RAa';

export const GX_AUTH_URL = 'https://gxauth.apimachine.com';

export const SUPPORT_TICKET_API = 'https://nitrogen.apimachine.com';

export const FIN_HUB_KEY = 'bsq6cu8fkcbcavsjce80';

export const AGENCY_API_URL = 'https://fxagency.apimachine.com';

export const MAILSURP_KEY =
  '86cee2f39d56b3b5a6b2e4c827cc1382d1be6bad16a9d35cd0e659ef9272d02c';

export const S3_CONFIG = {
  accessKeyId: Buffer.from('QUtJQVFENFFZWFdGV1dTVEdFNjQ=', 'base64').toString(),
  secretAccessKey: Buffer.from(
    'WElmckJsTG42OG1Dc2MxV3pMaVZrTnU2dkRuR1hSOW5UM2xCZExHZw==',
    'base64',
  ).toString(),
};

export const NEW_CHAT_API = 'https://testchatsioapi.globalxchange.io';
export const NEW_CHAT_SOCKET = 'https://testsockchatsio.globalxchange.io';

export const TELLER_API2_ENDPOINT = 'https://teller2.apimachine.com';
