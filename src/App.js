import 'react-native-gesture-handler';
import 'react-native-get-random-values';
import '@react-native-firebase/crashlytics';
import React, {useEffect, useState, useRef, useContext} from 'react';
import 'intl';
import 'intl/locale-data/jsonp/en';
import AppContextProvider, {AppContext} from './contexts/AppContextProvider';
import {DrawerActions, NavigationContainer} from '@react-navigation/native';
import AppStackNavigator from './navigator/AppStackNavigator';
import * as Font from 'expo-font';
import {StatusBar, View} from 'react-native';
import DepositContextProvider from './contexts/DepositContext';
import {WithdrawalContextProvider} from './contexts/WithdrawalContext';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import AsyncStorageHelper from './utils/AsyncStorageHelper';
import Axios from 'axios';
import {GX_AUTH_URL, GX_API_ENDPOINT, COGNITO_USER_POOL_ID} from './configs';
import SplashScreen from 'react-native-splash-screen';
import {QueryClient, QueryClientProvider} from 'react-query';

const queryClient = new QueryClient();

const App = () => {
  const {setIsLoggedIn, removeLoginData} = useContext(AppContext);

  const [isLoaded, setIsLoaded] = useState(false);

  const isTokenCheckRunning = useRef(false);

  useEffect(() => {
    loadFonts();
  }, []);

  useEffect(() => {
    if (isLoaded) {
      setTimeout(() => SplashScreen.hide(), 400);
    }
  }, [isLoaded]);

  useEffect(() => {
    if (!isTokenCheckRunning.current) {
      checkTokenValidity();
    }
  });

  const checkTokenValidity = async () => {
    isTokenCheckRunning.current = true;

    const token = await AsyncStorageHelper.getAppToken();

    if (token) {
      Axios.post(`${GX_API_ENDPOINT}/brokerage/verify_token`, {
        token,
        userpoolid: COGNITO_USER_POOL_ID,
      })
        .then((tokenCheckResp) => {
          let user;
          if (tokenCheckResp.data) {
            user = tokenCheckResp.data;
          } else {
            user = null;
          }

          if (user !== null && Date.now() < user.exp * 1000) {
            console.log('Session valid!!');
            isTokenCheckRunning.current = false;
          } else {
            console.log('Invalid Session, Updating Token');
            updateToken();
          }
        })
        .catch((error) => {
          console.log('Error on verifying Token', error);
          isTokenCheckRunning.current = false;
          // setIsLoggedIn(false);
          // AsyncStorageHelper.deleteAllUserInfo();
        });
    } else {
      isTokenCheckRunning.current = false;
    }
  };

  const updateToken = async () => {
    const deviceKey = await AsyncStorageHelper.getDeviceKey();
    const refreshToken = await AsyncStorageHelper.getRefreshToken();

    if (deviceKey && refreshToken) {
      Axios.post(`${GX_AUTH_URL}/gx/user/refresh/tokens`, {
        refreshToken,
        device_key: deviceKey,
      })
        .then((resp) => {
          const {data} = resp;

          if (data.status) {
            console.log('Token Updated');
            AsyncStorageHelper.setAppToken(data?.idToken || '');
            AsyncStorageHelper.setAccessToken(data?.accessToken || '');
          } else {
            console.log('Refresh Token is Invalid');
            setIsLoggedIn(false);
            AsyncStorageHelper.deleteAllUserInfo();
            removeLoginData();
          }
        })
        .catch((error) => {
          console.log('Error on refreshing token', error);
          // setIsLoggedIn(false);
          // AsyncStorageHelper.deleteAllUserInfo();
        })
        .finally(() => {
          isTokenCheckRunning.current = false;
        });
    }
  };

  const loadFonts = async () => {
    await Font.loadAsync({
      Montserrat: require('./assets/fonts/Montserrat-Regular.ttf'),
      'Montserrat-Medium': require('./assets/fonts/Montserrat-Medium.ttf'),
      'Montserrat-Bold': require('./assets/fonts/Montserrat-Bold.ttf'),
      'Montserrat-SemiBold': require('./assets/fonts/Montserrat-SemiBold.ttf'),
      Roboto: require('./assets/fonts/Roboto-Regular.ttf'),
      'Roboto-Bold': require('./assets/fonts/Roboto-Bold.ttf'),
      'Roboto-Medium': require('./assets/fonts/Roboto-Medium.ttf'),
      'Roboto-Light': require('./assets/fonts/Roboto-Light.ttf'),
    });
    setIsLoaded(true);
  };

  return (
    <>
      <QueryClientProvider client={queryClient}>
        {isLoaded ? (
          <DepositContextProvider>
            <WithdrawalContextProvider>
              <SafeAreaProvider>
                <NavigationContainer>
                  <AppStackNavigator />
                </NavigationContainer>
              </SafeAreaProvider>
            </WithdrawalContextProvider>
          </DepositContextProvider>
        ) : (
          <View />
        )}
      </QueryClientProvider>
    </>
  );
};

const ContextWrap = () => (
  <AppContextProvider>
    <App />
  </AppContextProvider>
);

export default ContextWrap;
