import axios from 'axios';
import CryptoJS from 'crypto-js';
import React, {useState, useRef, useEffect} from 'react';
import {Keyboard} from 'react-native';
import {useQuery} from 'react-query';
import {APP_CODE, GX_API_ENDPOINT} from '../configs';

/**
 * Returns if the keyboard is open / closed
 *
 * @return {bool} isOpen
 */
export function useKeyboardStatus() {
  const [isOpen, setIsOpen] = useState(false);
  const keyboardShowListener = useRef(null);
  const keyboardHideListener = useRef(null);

  useEffect(() => {
    keyboardShowListener.current = Keyboard.addListener('keyboardDidShow', () =>
      setIsOpen(true),
    );
    keyboardHideListener.current = Keyboard.addListener('keyboardDidHide', () =>
      setIsOpen(false),
    );

    return () => {
      setIsOpen(false);
      keyboardShowListener.current.remove();
      keyboardHideListener.current.remove();
    };
  }, []);

  return isOpen;
}

const getAppData = async () => {
  const resp = await axios.get(`${GX_API_ENDPOINT}/gxb/apps/get`, {
    params: {app_code: APP_CODE},
  });

  const appData = resp.data?.apps ? resp.data?.apps[0] : '';

  return appData;
};
export const useAppData = () => {
  const appData = useQuery('appData', getAppData);

  return appData;
};

const getExchangePairs = async ({queryKey}) => {
  const [_key, {email, type}] = queryKey;
  const {data} = await axios.get(
    `${GX_API_ENDPOINT}/coin/trade/user/fees/get`,
    {
      params: {email, get_fees_type: type},
    },
  );
  return data?.data?.[0]?.trade_fees;
};

export const useExchangePairs = (email, type) =>
  useQuery(['exchangePairs', {email, type}], getExchangePairs);

const getMoneyMarketList = async ({queryKey}) => {
  const [_key, {email, type}] = queryKey;
  const {data} = await axios.get(
    `${GX_API_ENDPOINT}/coin/iced/user/get/interest/fees`,
    {
      params: {email, type: type},
    },
  );
  return data?.feesData?.fees;
};

export const useMoneyMarketList = (email, type) =>
  useQuery(['moneyMarketPairs', {email, type}], getMoneyMarketList);

// APIS For Register
const getUserDetails = async ({queryKey}) => {
  const [_key, username] = queryKey;
  const {data} = await axios.get(
    `https://comms.globalxchange.io/user/profile/data/get?username=${username}`,
  );
  return data?.usersData?.[0];
};

export const useUserDetails = (username) => {
  const query = useQuery(['getUserDetails', username], getUserDetails);
  return query;
};

const getRegisterdUsers = async () => {
  const {data} = await axios.get(
    'https://comms.globalxchange.io/listUsernames',
  );
  if (data.status) {
    let bytes = CryptoJS.Rabbit.decrypt(data.payload, 'gmBuuQ6Er8XqYBd');
    let jsonString = bytes.toString(CryptoJS.enc.Utf8);
    let result_obj = JSON.parse(jsonString);
    return result_obj;
  }
};

export const useRegisterdUsers = () =>
  useQuery(['getRegisterdUsers'], getRegisterdUsers);
