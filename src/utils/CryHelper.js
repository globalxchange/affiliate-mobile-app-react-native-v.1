import CryptoJS from 'crypto-js';

const secret = 'HUBQTVce7cUde4F';

const encryptPostData = (postData) => {
  const encryptedData = CryptoJS.Rabbit.encrypt(
    JSON.stringify(postData),
    secret,
  ).toString();

  return encryptedData;
};

const CryHelper = {encryptPostData};

export default CryHelper;
