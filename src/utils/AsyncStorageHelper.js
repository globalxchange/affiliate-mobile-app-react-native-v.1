import AsyncStorage from '@react-native-community/async-storage';

const setIsLoggedIn = (bool) => {
  try {
    AsyncStorage.setItem('isLoggedIn', JSON.stringify(bool));
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
};

const getIsLoggedIn = async () => {
  let isLoggedIn;
  try {
    isLoggedIn = (await AsyncStorage.getItem('isLoggedIn')) || false;
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
  return isLoggedIn;
};

const setLoginEmail = (email) => {
  try {
    AsyncStorage.setItem('loginEmail', email);
    AsyncStorage.setItem('adminLoginEmail', email);
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
};

const getLoginEmail = async () => {
  let email;
  try {
    email = (await AsyncStorage.getItem('loginEmail')) || '';
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
  return email;
};

const setAppToken = (token) => {
  try {
    AsyncStorage.setItem('appToken', token);
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
};

const getAppToken = async () => {
  let appToken;
  try {
    appToken = (await AsyncStorage.getItem('appToken')) || '';
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
  return appToken;
};

const setAccessToken = async (accessToken) => {
  try {
    AsyncStorage.setItem('accessToken', accessToken);
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
};

const getAccessToken = async () => {
  let accessToken;
  try {
    accessToken = (await AsyncStorage.getItem('accessToken')) || '';
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
  return accessToken;
};

const setUserName = (userName) => {
  try {
    AsyncStorage.setItem('userName', userName);
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
};
const getUserName = async () => {
  let userName;
  try {
    userName = (await AsyncStorage.getItem('userName')) || '';
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
  return userName;
};

const setRefreshToken = (refreshToken) => {
  try {
    AsyncStorage.setItem('appRefreshToken', refreshToken);
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
};

const getRefreshToken = async () => {
  let refreshToken;
  try {
    refreshToken = (await AsyncStorage.getItem('appRefreshToken')) || '';
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
  return refreshToken;
};

const deleteAllUserInfo = async () => {
  try {
    await AsyncStorage.removeItem('isLoggedIn');
    await AsyncStorage.removeItem('loginEmail');
    await AsyncStorage.removeItem('appToken');
    await AsyncStorage.removeItem('accessToken');
    await AsyncStorage.removeItem('userName');
    await AsyncStorage.removeItem('userAvatar');
    await AsyncStorage.removeItem('profileId');
    await AsyncStorage.removeItem('appRefreshToken');
    await AsyncStorage.removeItem('appDeviceKey');
    await AsyncStorage.removeItem('fullName');
    await AsyncStorage.removeItem('adminLoginEmail');
    await AsyncStorage.removeItem('adminProfileId');
  } catch (error) {
    // Error retrieving data
    console.log(error.message);
  }
};

const setDeviceKey = (deviceKey) => {
  try {
    AsyncStorage.setItem('appDeviceKey', deviceKey);
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
};

const getDeviceKey = async () => {
  let deviceKey;
  try {
    deviceKey = (await AsyncStorage.getItem('appDeviceKey')) || '';
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
  return deviceKey;
};

const setUserAvatarUrl = (url) => {
  try {
    AsyncStorage.setItem('userAvatar', url);
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
};

const getUserAvatarUrl = async () => {
  let url;
  try {
    url = (await AsyncStorage.getItem('userAvatar')) || '';
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
  return url;
};

const setProfileId = (profileId) => {
  try {
    AsyncStorage.setItem('profileId', profileId);
    AsyncStorage.setItem('adminProfileId', profileId);
  } catch (error) {
    console.log('Error on setting AsyncStorage profileId', error);
  }
};

const getProfileId = async () => {
  let profileId;
  try {
    profileId = (await AsyncStorage.getItem('profileId')) || '';
  } catch (error) {
    console.log('Error on getting AsyncStorage getProfileId', error);
  }
  return profileId;
};

const setAffId = (affId) => {
  try {
    AsyncStorage.setItem('affId', affId);
  } catch (error) {
    console.log('Error on setting AsyncStorage profileId', error);
  }
};

const getAffId = async () => {
  let affId;
  try {
    affId = (await AsyncStorage.getItem('affId')) || '';
  } catch (error) {
    console.log('Error on getting AsyncStorage affId', error);
  }
  return affId;
};

const setBCHelperStatus = (showBCHelper) => {
  try {
    AsyncStorage.setItem('showBCHelper', JSON.stringify(showBCHelper));
  } catch (error) {
    console.log('Error on setting AsyncStorage profileId', error);
  }
};

const getBCHelperStatus = async () => {
  let showBCHelper;
  try {
    showBCHelper =
      JSON.parse(await AsyncStorage.getItem('showBCHelper')) || false;
  } catch (error) {
    console.log('Error on getting AsyncStorage affId', error);
  }
  return showBCHelper;
};

const setUserFullName = (fullName) => {
  try {
    AsyncStorage.setItem('fullName', fullName);
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
};

const getUserFullName = async () => {
  let fullName;
  try {
    fullName = (await AsyncStorage.getItem('fullName')) || '';
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
  }
  return fullName;
};

const setAdminView = async (userEmail, userProfileId) => {
  let success = false;
  try {
    AsyncStorage.setItem('isAdminView', JSON.stringify(true));

    const adminMail = await AsyncStorage.getItem('adminLoginEmail');
    if (!adminMail) {
      const email = await getLoginEmail();
      await AsyncStorage.setItem('adminLoginEmail', email);
    }

    const adminProfileId = await AsyncStorage.getItem('adminProfileId');
    if (!adminProfileId) {
      const profileId = await getProfileId();
      await AsyncStorage.setItem('adminProfileId', profileId);
    }

    await AsyncStorage.setItem('loginEmail', userEmail);
    await AsyncStorage.setItem('profileId', userProfileId);
    success = true;
  } catch (error) {
    console.log('Error on setting AsyncStorage data', error);
    success = false;
  }
  return success;
};

const getIsAdminView = async () => {
  let isAdminView;
  try {
    isAdminView =
      JSON.parse(await AsyncStorage.getItem('isAdminView')) || false;
  } catch (error) {
    console.log('Error on getting AsyncStorage isAdminView', error);
  }
  return isAdminView;
};

const removeAdminView = async () => {
  let success = false;
  try {
    const email = await AsyncStorage.getItem('adminLoginEmail');
    const profileId = await AsyncStorage.getItem('adminProfileId');

    await AsyncStorage.setItem('isAdminView', JSON.stringify(false));
    await AsyncStorage.setItem('loginEmail', email);
    await AsyncStorage.setItem('profileId', profileId);
    success = true;
  } catch (error) {
    console.log('Error on removing admin login', error);
    success = false;
  }

  return success;
};

const AsyncStorageHelper = {
  setIsLoggedIn,
  getIsLoggedIn,
  setLoginEmail,
  getLoginEmail,
  setAppToken,
  getAppToken,
  setAccessToken,
  getAccessToken,
  setUserName,
  getUserName,
  setRefreshToken,
  getRefreshToken,
  deleteAllUserInfo,
  setUserAvatarUrl,
  getUserAvatarUrl,
  setProfileId,
  getProfileId,
  setDeviceKey,
  getDeviceKey,
  setAffId,
  getAffId,
  setBCHelperStatus,
  getBCHelperStatus,
  setUserFullName,
  getUserFullName,
  setAdminView,
  getIsAdminView,
  removeAdminView,
};

export default AsyncStorageHelper;
