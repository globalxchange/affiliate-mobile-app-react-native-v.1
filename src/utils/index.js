import moment from 'moment-timezone';
import TimeAgo from 'javascript-time-ago';

export const numberFormatter = new Intl.NumberFormat('en-US', {
  minimumFractionDigits: 0,
  maximumFractionDigits: 0,
});

export const usdValueFormatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 2,
  maximumFractionDigits: 2,
});

export const usdValueFormatterWithoutSign = new Intl.NumberFormat('en-US', {
  minimumFractionDigits: 2,
  maximumFractionDigits: 2,
});

export const cryptoVolumeFormatter = Intl.NumberFormat('en-US', {
  minimumFractionDigits: 2,
  maximumFractionDigits: 5,
});

export const cryptoFormatter = (value) => {
  const parsedFloatValue = parseFloat(value);
  const parsedIntValue = parseInt(value);

  const numberOfdigitsBeforDecimel = parsedIntValue.toString().length;

  const delta = 5 - numberOfdigitsBeforDecimel;

  return parsedFloatValue.toFixed(delta < 0 ? 0 : delta);
};

export const fiatFormatter = (value) => {
  const parsedFloatValue = parseFloat(value);

  return parsedFloatValue.toFixed(2);
};

export const percentageFormatter = new Intl.NumberFormat('en-US', {
  minimumFractionDigits: 2,
  maximumFractionDigits: 2,
});

export const emailValidator = (email) => {
  const re = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i;
  return re.test(String(email).toLowerCase());
};

const fiat = [
  'INR',
  'AUD',
  'GBP',
  'CAD',
  'EUR',
  'CNY',
  'JPY',
  'AED',
  'USD',
  'USDT',
  'GXT',
];
export const cryptoList = ['ETH', 'BTC', 'LTC'];

export const formatterHelper = (value, currency) => {
  if (!currency) {
    return usdValueFormatterWithoutSign.format(value);
  }

  if (currency === 'USD') {
    return usdValueFormatter.format(value);
  }

  if (cryptoList.includes(currency.toUpperCase())) {
    return cryptoFormatter(value);
  }

  return usdValueFormatterWithoutSign.format(value);
};

export const timestampParser = (timestamp) =>
  moment(timestamp).format('DD-MM-YYYY hh:mm A');

export const timeParserExpanded = (timestamp) => {
  const date = moment(timestamp).tz('America/New_York').format('MMM Do YYYY');
  const time = moment(timestamp).tz('America/New_York').format('hh:mm A');

  return `${date || ''} At ${time || ''} EST`;
};

export const urlValidatorRegex = new RegExp(
  /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i,
);

export const getFundingMethodIcon = (coin) => {
  switch (coin) {
    case 'BTC':
      return require('../assets/funding-methods-icons/btc.png');
    case 'ETH':
      return require('../assets/funding-methods-icons/eth.png');
    case 'USDT':
      return require('../assets/funding-methods-icons/tether.png');
    case 'GXT':
      return require('../assets/funding-methods-icons/gxt.png');
    case 'LTC':
      return require('../assets/funding-methods-icons/ltc.png');
    default:
      return require('../assets/funding-methods-icons/crypto.png');
  }
};
export const usernameRegex = /^(?=[a-z_\d]*[a-z])[a-z_\d]{6,}$/;

export const isCrypto = (coinSymbol) => {
  const fiatCurrencies = [
    'AUS',
    'GBP',
    'CAD',
    'USD',
    'CNY',
    'COP',
    'EUR',
    'INR',
    'IDR',
    'JPY',
    'MXN',
    'AED',
    'USD',
  ];

  return !fiatCurrencies.includes(coinSymbol || '');
};

export const roundHelper = (value, currency) => {
  if (!currency) {
    return fiatFormatter(value);
  }

  if (cryptoList.includes(currency.toUpperCase())) {
    return cryptoFormatter(value);
  }

  return fiatFormatter(value);
};

export const getUriImage = (uri) => {
  return uri !== null &&
    uri !== undefined &&
    uri.includes('/') &&
    uri.includes('.')
    ? uri
    : '';
};

export const getAssetData = (coinSymbol, walletCoinData) => {
  if (walletCoinData) {
    let coin = walletCoinData.find((x) => x.coinSymbol === coinSymbol);

    return coin;
  }
};

export const getPlaceholderText = (name = '') => {
  const splittedName = name?.toUpperCase().split(' ');

  const initials = splittedName.map((x) => x[0]);

  return initials.join('').substring(0, 2).trim();
};

export const timeAgoFormatter = (timestamp) => {
  let text = '';

  try {
    const timeAgo = new TimeAgo('en-US');
    text = timeAgo.format(timestamp);
  } catch (error) {}

  return text;
};

export const checkPasswordValidity = (password) => {
  const capRegex = new RegExp(/^.*[A-Z].*/);
  const numRegex = new RegExp(/^.*[0-9].*/);
  const speRegex = new RegExp(/^.*[!@#$%^&*()+=].*/);
  return (
    password.length >= 6 &&
    capRegex.test(password) &&
    speRegex.test(password) &&
    numRegex.test(password)
  );
};
