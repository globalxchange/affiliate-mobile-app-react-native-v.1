import React, {useRef, useState} from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import Carousel from 'react-native-snap-carousel';
import {getUriImage} from '../utils';
import {animatedStyles, scrollInterpolator} from '../utils/CarouselAnimation';
import LoadingAnimation from './LoadingAnimation';

const {width} = Dimensions.get('window');

const SLIDER_WIDTH = width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.8);
const ITEM_HEIGHT = Math.round((ITEM_WIDTH * 3) / 6);

const FeedsCategoryController = ({carouselData, setActiveItem}) => {
  const [currentPos, setCurrentPos] = useState(0);

  const carouselRef = useRef();

  const onSnapToItem = (index) => {
    setCurrentPos(index);
    if (carouselData) {
      setActiveItem(carouselData[index]);
    }
  };

  return (
    <View>
      {carouselData ? (
        <Carousel
          ref={carouselRef}
          data={carouselData}
          sliderWidth={SLIDER_WIDTH}
          itemWidth={ITEM_WIDTH}
          itemHeight={ITEM_HEIGHT}
          containerCustomStyle={styles.carouselContainer}
          inactiveSlideShift={0}
          onSnapToItem={onSnapToItem}
          scrollInterpolator={scrollInterpolator}
          slideInterpolatedStyle={animatedStyles}
          useScrollView={true}
          loop
          inactiveSlideOpacity={0.5}
          inactiveSlideScale={0.8}
          renderItem={({item}) => (
            <TouchableOpacity disabled style={styles.itemContainer}>
              <View style={styles.iconContainer}>
                <FastImage
                  source={{uri: getUriImage(item.formData.icon)}}
                  style={styles.icon}
                />
                <Text style={styles.title}>{item.formData.Name}</Text>
              </View>

              <Text numberOfLines={4} style={styles.itemLabel}>
                {item.formData.description}
              </Text>
            </TouchableOpacity>
          )}
        />
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation height={70} width={70} />
        </View>
      )}
    </View>
  );
};

export default FeedsCategoryController;

const styles = StyleSheet.create({
  carouselContainer: {
    marginTop: 30,
  },
  itemContainer: {
    zIndex: 3,
    borderColor: '#EDEDED',
    borderWidth: 1,
    height: ITEM_HEIGHT,
    borderRadius: 10,
    padding: 20,
  },
  itemLabel: {
    color: '#08152D',
    fontSize: 13,
    fontFamily: 'Montserrat',
    marginTop: 5,
    flex: 1,
  },
  loadingContainer: {
    height: ITEM_HEIGHT,
    justifyContent: 'center',
  },
  iconContainer: {
    flexDirection: 'row',
    marginBottom: 10,
    alignItems: 'center',
  },
  icon: {
    width: 30,
    height: 30,
  },
  title: {
    color: '#08152D',
    fontSize: 20,
    fontFamily: 'Montserrat-Bold',
    marginLeft: 20,
    flex: 1,
  },
});
