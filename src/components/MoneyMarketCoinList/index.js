import {FlatList, StyleSheet, Text, TouchableOpacity} from 'react-native';
import React, {useState} from 'react';
import {usdValueFormatterWithoutSign} from '../../utils';
import ThemeData from '../../configs/ThemeData';
import {Fragment} from 'react';
import MoneyMarketActionModal from './MoneyMarketActionModal';

const MoneyMarketCoinList = ({feeList, search, refetch, bonds}) => {
  const [coin, setCoin] = useState('');
  return (
    <Fragment>
      <FlatList
        style={styles.listView}
        contentContainerStyle={styles.listViewIn}
        data={feeList.filter((itm) =>
          itm?.coin?.toLowerCase().includes(search.toLowerCase()),
        )}
        keyExtractor={(item) => item.coin}
        showsHorizontalScrollIndicator={false}
        renderItem={({item}) => (
          <TouchableOpacity
            onPress={() => {
              setCoin(item.coin);
            }}
            style={[styles.pairItm]}>
            <Text style={styles.pair}>{item.coin}</Text>
            <Text style={styles.pair}>
              {usdValueFormatterWithoutSign.format(
                item?.[bonds ? 'iced' : 'liquid'],
              )}
              %
            </Text>
          </TouchableOpacity>
        )}
      />
      <MoneyMarketActionModal coin={coin} setCoin={setCoin} refetch={refetch} />
    </Fragment>
  );
};

export default MoneyMarketCoinList;

const styles = StyleSheet.create({
  listView: {
    flex: 1,
  },
  pairItm: {
    height: 90,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    marginHorizontal: 15,
  },
  pair: {
    fontSize: 18,
    fontFamily: ThemeData.ROBOTO_MEDIUM,
    color: ThemeData.TEXT_COLOR,
  },
});
