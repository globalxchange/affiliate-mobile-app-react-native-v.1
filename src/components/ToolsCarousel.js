import React, {useState, useRef} from 'react';
import {StyleSheet, Text, View, Dimensions, Image} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import {scrollInterpolator, animatedStyles} from '../utils/CarouselAnimation';
import LoadingAnimation from './LoadingAnimation';

const {width} = Dimensions.get('window');

const SLIDER_WIDTH = width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.5);
const ITEM_HEIGHT = Math.round((ITEM_WIDTH * 3) / 6);

const ToolsCarousel = () => {
  const [currentPos, setCurrentPos] = useState(0);

  const carouselRef = useRef();

  const onSnapToItem = (index) => {
    setCurrentPos(index);
    if (carouselData) {
      // setActiveFilterCurrency(carouselData[index].formData.coin);
    }
  };

  return (
    <View>
      {carouselData ? (
        <Carousel
          scrollEnabled={false}
          ref={carouselRef}
          data={carouselData}
          sliderWidth={SLIDER_WIDTH}
          itemWidth={ITEM_WIDTH}
          containerCustomStyle={styles.carouselContainer}
          inactiveSlideShift={0}
          onSnapToItem={onSnapToItem}
          scrollInterpolator={scrollInterpolator}
          slideInterpolatedStyle={animatedStyles}
          useScrollView={true}
          loop
          inactiveSlideOpacity={0.5}
          inactiveSlideScale={0.8}
          renderItem={({item}) => (
            <View style={styles.itemContainer}>
              <Image
                style={styles.carouselImage}
                source={item.image}
                resizeMode="cover"
              />
              <Text style={styles.itemLabel}>{item.title}</Text>
            </View>
          )}
        />
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation height={70} width={70} />
        </View>
      )}
    </View>
  );
};

export default ToolsCarousel;

const styles = StyleSheet.create({
  carouselContainer: {
    marginTop: 10,
  },
  itemContainer: {
    zIndex: 3,
  },
  carouselImage: {
    width: ITEM_WIDTH,
    height: ITEM_HEIGHT,
    overflow: 'hidden',
    borderRadius: 6,
    backgroundColor: '#C4C4C4',
  },
  itemLabel: {
    color: '#08152D',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
    marginTop: 5,
  },
  counter: {
    marginTop: 25,
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  loadingContainer: {
    height: ITEM_HEIGHT,
    justifyContent: 'center',
  },
});

const carouselData = [
  {
    image: require('../assets/broker-carosuel/insta-funnel-icon.png'),
    title: 'InstantFunnels',
  },
  {
    image: require('../assets/broker-carosuel/mail-king-icon.png'),
    title: 'MailKings',
  },
  {
    image: require('../assets/broker-carosuel/engagments-icon.png'),
    title: 'Engagements',
  },
];
