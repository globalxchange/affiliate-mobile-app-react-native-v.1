/* eslint-disable react-native/no-inline-styles */
import React, {useState, useRef, useEffect} from 'react';
import {
  View,
  Image,
  TextInput,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import ThemeData from '../configs/ThemeData';

const LoginInputField = ({
  icon,
  placeholder,
  type,
  secureTextEntry,
  autoFocus,
  value,
  onChangeText,
  editable,
  onFocus,
  onBlur,
  focus,
  style,
  showValidator,
  validatorStatus,
  showNext,
  onNext,
}) => {
  const [isFocused, setIsFocused] = useState(false);

  const inputRef = useRef();

  useEffect(() => {
    if (focus) {
      inputRef.current.focus();
    }
  }, [focus]);

  return (
    <View style={[styles.container, style]}>
      {icon ? (
        <Image style={styles.img} source={icon} resizeMode="contain" />
      ) : null}
      <TextInput
        ref={inputRef}
        style={styles.input}
        placeholder={placeholder}
        textContentType={type}
        secureTextEntry={secureTextEntry}
        value={value}
        onChangeText={onChangeText}
        editable={editable}
        onFocus={() => {
          setIsFocused(true);
          onFocus && onFocus();
        }}
        onBlur={() => {
          setIsFocused(false);
          onBlur && onBlur();
        }}
        placeholderTextColor={'#878788'}
      />
      {showValidator && (
        <View
          style={[
            styles.validatorSign,
            {backgroundColor: validatorStatus ? '#08152D' : '#D80027'},
          ]}
        />
      )}
      {showNext && isFocused && (
        <TouchableOpacity onPress={onNext}>
          <View
            style={[styles.nextButton, {opacity: validatorStatus ? 1 : 0.4}]}>
            <Image
              style={styles.nextImage}
              resizeMode="contain"
              source={require('../assets/right-arrow-carrot-white.png')}
            />
          </View>
        </TouchableOpacity>
      )}
    </View>
  );
};

export default LoginInputField;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: 2,
    height: 60,
    paddingHorizontal: 15,
    marginBottom: 20,
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  focusStyles: {
    borderBottomColor: 'transparent',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.15,
    shadowRadius: 2,
    backgroundColor: 'white',
    borderRadius: 2,
    elevation: 10,
  },
  img: {
    height: 15,
    width: 15,
  },
  input: {
    paddingHorizontal: 10,
    flexGrow: 1,
    width: 0,
    fontFamily: 'Montserrat',
    color: 'black',
    height: '100%',
  },
  validatorSign: {
    width: 10,
    height: 10,
    borderRadius: 5,
  },
  nextButton: {
    height: 30,
    width: 30,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    padding: 8,
    borderRadius: 6,
  },
  nextImage: {
    flex: 1,
    height: null,
    width: null,
  },
});
