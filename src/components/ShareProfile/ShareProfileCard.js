/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  FlatList,
  Image,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import ProfileAvatar from '../ProfileAvatar';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import Clipboard from '@react-native-community/clipboard';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faTimes} from '@fortawesome/free-solid-svg-icons';

const ShareProfileCard = ({profileData, openOtherOptions, onClose}) => {
  const {avatar} = useContext(AppContext);

  const [fullName, setFullName] = useState('');
  const [brokerId, setBrokerId] = useState('');
  const [userAvatar, setUserAvatar] = useState('');

  const [showProfileURL, setShowProfileURL] = useState(false);

  useEffect(() => {
    if (!profileData) {
      (async () => {
        setUserAvatar(avatar);
        const uname = await AsyncStorageHelper.getUserName();
        setBrokerId(uname || '');
        const fname = await AsyncStorageHelper.getUserFullName();
        setFullName(fname || '');
      })();
    } else {
      console.log('profileData', profileData);
      setFullName(profileData.name || '');
      setUserAvatar(profileData.profile_img || '');
      setBrokerId(profileData.username || '');
    }
  }, [profileData, avatar]);

  const copyURLMethodeClick = () => {
    setShowProfileURL(true);
    const url = `https://affiliate.app/brokerid/${brokerId}`;
    Clipboard.setString(url);
  };

  const onCopyClick = () => {
    const url = `https://affiliate.app/brokerid/${brokerId}`;
    Clipboard.setString(url);
  };

  return (
    <View style={styles.container}>
      {profileData ? null : (
        <Text style={styles.headerText}>Share Profile</Text>
      )}
      <View style={[styles.cardsContainer, {borderWidth: profileData ? 0 : 1}]}>
        <View style={styles.headerContainer}>
          <ProfileAvatar size={50} avatar={userAvatar} name={fullName} />
          <View style={styles.nameContainer}>
            <Text style={styles.userName}>{fullName}</Text>
            <Text style={styles.tag}>Affiliate Profile</Text>
          </View>
          <TouchableOpacity onPress={onClose} style={styles.closeButton}>
            <FontAwesomeIcon
              icon={faTimes}
              color={ThemeData.APP_MAIN_COLOR}
              size={20}
            />
          </TouchableOpacity>
        </View>
        <Text style={styles.profileDesc}>
          You can share your affiliate profile page as a link so that your
          receipients can learn about your business.
        </Text>
        <View style={styles.shareHeader}>
          <Text style={styles.shareHeaderText}>Select Sharing Options</Text>
          {profileData ? null : (
            <TouchableOpacity onPress={openOtherOptions}>
              <Text style={styles.otherButtonText}>Other</Text>
            </TouchableOpacity>
          )}
        </View>
        <FlatList
          style={styles.list}
          horizontal
          showsHorizontalScrollIndicator={false}
          data={SHARE_OPTIONS}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={copyURLMethodeClick}
              disabled={item.disabled}>
              <View
                style={[styles.shareItem, {opacity: item.disabled ? 0.3 : 1}]}>
                <Image
                  style={styles.shareIcon}
                  source={item.icon}
                  resizeMode="contain"
                />
              </View>
            </TouchableOpacity>
          )}
        />
        {showProfileURL ? (
          <View style={styles.profileURLContainer}>
            <Text style={styles.profileURL} numberOfLines={1}>
              {`https://affiliate.app/brokerid/${brokerId}`}
            </Text>
            <TouchableOpacity style={styles.actionButton} onPress={onCopyClick}>
              <Image
                style={styles.actionIcon}
                resizeMode="contain"
                source={require('../../assets/copy-icon-grey.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.actionButton}
              onPress={() => setShowProfileURL(false)}>
              <Image
                style={styles.actionIcon}
                resizeMode="contain"
                source={require('../../assets/close-icon-grey.png')}
              />
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    </View>
  );
};

export default ShareProfileCard;

const styles = StyleSheet.create({
  container: {},
  headerText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    marginBottom: 15,
    marginTop: 30,
  },
  cardsContainer: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    padding: 25,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  nameContainer: {
    marginLeft: 10,
    flex: 1,
  },
  userName: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 20,
  },
  tag: {
    fontFamily: ThemeData.FONT_MEDIUM,
    color: ThemeData.APP_MAIN_COLOR,
  },
  profileDesc: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    lineHeight: 20,
    marginVertical: 25,
  },
  shareHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  shareHeaderText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 13,
  },
  otherButtonText: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 13,
    textDecorationLine: 'underline',
  },
  list: {
    marginTop: 20,
  },
  shareItem: {
    marginRight: 15,
  },
  shareIcon: {
    width: 40,
    height: 40,
  },
  profileURLContainer: {
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginTop: 20,
    alignItems: 'center',
  },
  profileURL: {
    flex: 1,
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    paddingHorizontal: 15,
    fontSize: 12,
  },
  actionButton: {
    width: 40,
    height: 40,
    padding: 12,
    borderLeftColor: ThemeData.BORDER_COLOR,
    borderLeftWidth: 1,
  },
  actionIcon: {
    flex: 1,
    width: undefined,
    height: undefined,
  },
  closeButton: {
    padding: 5,
  },
});

const SHARE_OPTIONS = [
  {icon: require('../../assets/share-icons/profile-url-copy.png')},
  {icon: require('../../assets/share-icons/insta.png'), disabled: true},
  {icon: require('../../assets/share-icons/youtube.png'), disabled: true},
  {icon: require('../../assets/share-icons/twitter.png'), disabled: true},
  {icon: require('../../assets/share-icons/snapchat.png'), disabled: true},
  {icon: require('../../assets/share-icons/linkedin.png'), disabled: true},
];
