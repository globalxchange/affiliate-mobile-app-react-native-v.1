import React, {useState} from 'react';
import {
  Dimensions,
  Keyboard,
  Modal,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import SearchLayout from '../../layouts/SearchLayout';
import ShareProfileCard from './ShareProfileCard';

const {width, height} = Dimensions.get('window');

const ShareProfile = ({onBack, allUsers}) => {
  const [searchText, setSearchText] = useState('');
  const [selectedFilter, setSelectedFilter] = useState(FILTERS[0]);
  const [selectedProfile, setSelectedProfile] = useState();

  const onUserSelect = (user) => {
    Keyboard.dismiss();
    setSelectedProfile(user);
  };

  return (
    <View style={styles.container}>
      <SearchLayout
        value={searchText}
        setValue={setSearchText}
        onBack={onBack}
        showUserList
        filters={FILTERS}
        list={allUsers}
        onSubmit={(_, item) => onUserSelect(item)}
        placeholder="Search Profile"
        selectedFilter={selectedFilter}
        setSelectedFilter={setSelectedFilter}
      />
      <Modal
        animationType="fade"
        transparent
        visible={!!selectedProfile}
        hardwareAccelerated
        statusBarTranslucent
        onDismiss={() => setSelectedProfile(false)}
        onRequestClose={() => setSelectedProfile(false)}>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.overlay}
          onPress={() => setSelectedProfile(false)}
          onPressOut={() => {}}>
          <TouchableWithoutFeedback style={{flex: 1}}>
            <View style={[styles.modalContainer]}>
              <ShareProfileCard
                profileData={selectedProfile}
                onClose={() => setSelectedProfile(false)}
              />
            </View>
          </TouchableWithoutFeedback>
        </TouchableOpacity>
      </Modal>
    </View>
  );
};

export default ShareProfile;

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFill,
    backgroundColor: 'white',
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.8)',
    flex: 1,
    justifyContent: 'center',
    paddingVertical: height * 0.25,
  },
  modalContainer: {
    backgroundColor: 'white',
    width: width * 0.85,
    marginLeft: 'auto',
    marginRight: 'auto',
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: 'white',
  },
});

const FILTERS = [
  {title: 'Email', paramKey: 'email'},
  {title: 'Name', paramKey: 'name'},
  {title: 'Username', paramKey: 'username'},
];
