import React, {useRef, useState} from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import Carousel from 'react-native-snap-carousel';
import {animatedStyles, scrollInterpolator} from '../utils/CarouselAnimation';
import AgencyInstaller from './AgencyInstaller';
import AssetsIoInstaller from './AssetsIoInstaller';
import InstaCryptoInstaller from './InstaCryptoInstaller';
import LoadingAnimation from './LoadingAnimation';

const {width} = Dimensions.get('window');

const SLIDER_WIDTH = width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.75);
const ITEM_HEIGHT = Math.round((ITEM_WIDTH * 3) / 6);

const BrandCarousel = () => {
  const [currentPos, setCurrentPos] = useState(0);
  const [isInstaInstallerOpen, setIsInstaInstallerOpen] = useState(false);
  const [isAssetIoInstallerOpen, setIsAssetIoInstallerOpen] = useState(false);
  const [isAgencyInstallerOpen, setIsAgencyInstallerOpen] = useState(false);

  const carouselRef = useRef();

  const onSnapToItem = (index) => {
    setCurrentPos(index);
    if (carouselData) {
      // setActiveCarousel(carouselData[index]);
    }
  };

  const onItemClick = (item) => {
    if (item.id === 'InstaCrypto') {
      setIsAssetIoInstallerOpen(false);
      setIsAgencyInstallerOpen(false);
      setIsInstaInstallerOpen(true);
    }

    if (item.id === 'AssetsIo') {
      setIsInstaInstallerOpen(false);
      setIsAgencyInstallerOpen(false);
      setIsAssetIoInstallerOpen(true);
    }

    if (item.id === 'Agency') {
      setIsInstaInstallerOpen(false);
      setIsAssetIoInstallerOpen(false);
      setIsAgencyInstallerOpen(true);
    }
  };

  return (
    <View>
      {carouselData ? (
        <Carousel
          ref={carouselRef}
          data={carouselData}
          sliderWidth={SLIDER_WIDTH}
          itemWidth={ITEM_WIDTH}
          containerCustomStyle={styles.carouselContainer}
          inactiveSlideShift={0}
          onSnapToItem={onSnapToItem}
          scrollInterpolator={scrollInterpolator}
          slideInterpolatedStyle={animatedStyles}
          useScrollView={true}
          loop
          inactiveSlideOpacity={0.5}
          inactiveSlideScale={0.8}
          renderItem={({item, index}) => (
            <TouchableOpacity
              key={index}
              onPress={() => onItemClick(item)}
              style={styles.itemContainer}>
              <View
                style={[
                  styles.imageContainer,
                  {backgroundColor: `#${item.colorCode}`},
                ]}>
                <Image
                  style={styles.carouselImage}
                  source={item.image}
                  resizeMode="cover"
                />
              </View>
              <Text style={styles.itemLabel}>{item.text}</Text>
            </TouchableOpacity>
          )}
        />
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation height={70} width={70} />
        </View>
      )}
      <InstaCryptoInstaller
        isOpen={isInstaInstallerOpen}
        setIsOpen={setIsInstaInstallerOpen}
      />
      <AssetsIoInstaller
        isOpen={isAssetIoInstallerOpen}
        setIsOpen={setIsAssetIoInstallerOpen}
      />
      <AgencyInstaller
        isOpen={isAgencyInstallerOpen}
        setIsOpen={setIsAgencyInstallerOpen}
      />
    </View>
  );
};

export default BrandCarousel;

const styles = StyleSheet.create({
  carouselContainer: {
    marginTop: 10,
  },
  itemContainer: {
    zIndex: 3,
  },
  imageContainer: {
    width: ITEM_WIDTH,
    height: ITEM_HEIGHT,
    overflow: 'hidden',
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
  },
  carouselImage: {
    width: ITEM_WIDTH,
    height: ITEM_HEIGHT,
  },
  itemLabel: {
    color: '#08152D',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
    marginTop: 20,
  },
  counter: {
    marginTop: 25,
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  loadingContainer: {
    height: ITEM_HEIGHT,
    justifyContent: 'center',
  },
});

const carouselData = [
  {
    id: 'InstaCrypto',
    image: require('../assets/brands-carousel/insta-crypto.png'),
    text:
      'Get 2000 Free SEFCoin’s When You Join InstaCrypto Through AffiliateApp',
  },
  {
    id: 'Agency',
    image: require('../assets/brands-carousel/agency.png'),
    text: 'Refer A Brand Who Could Use Their Own Crypto Platform',
  },
  {
    id: 'AssetsIo',
    image: require('../assets/brands-carousel/assets-io.png'),
    text: 'Get 5.00 USDT For Joining Assets.io Through The AffiliateApp',
  },
];
