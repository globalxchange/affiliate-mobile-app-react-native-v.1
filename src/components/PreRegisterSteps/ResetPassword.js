import {useNavigation} from '@react-navigation/native';
import Axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {WSnackBar, WToast} from 'react-native-smart-tip';
import {APP_CODE, GX_API_ENDPOINT, GX_AUTH_URL} from '../../configs';
import {AppContext} from '../../contexts/AppContextProvider';
import {checkPasswordValidity} from '../../utils';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import LoginInputField from '../LoginInputField';

const ResetPassword = ({emailInput, tempPassword, isLoading, setIsLoading}) => {
  const navigation = useNavigation();

  const {setProfileId, setLoginData} = useContext(AppContext);

  const [passwordInput, setPasswordInput] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isShowConfirm, setIsShowConfirm] = useState(false);
  const [passwordValidityStatus, setPasswordValidityStatus] = useState(false);
  const [confrimPswdvalidityStatus, setConfrimPswdvalidityStatus] = useState(
    false,
  );
  const [isInputFocused, setIsInputFocused] = useState(false);

  useEffect(() => {
    setPasswordValidityStatus(checkPasswordValidity(passwordInput));
  }, [passwordInput]);

  useEffect(() => {
    setConfrimPswdvalidityStatus(passwordInput === confirmPassword);
  }, [confirmPassword, passwordInput]);

  const getUserDetails = (email) => {
    Axios.get(`${GX_API_ENDPOINT}/user/details/get`, {
      params: {email},
    })
      .then((res) => {
        const {data} = res;

        // console.log('Data', data);
        if (data.status) {
          if (data.user.profile_img) {
            AsyncStorageHelper.setUserName(data.user.username);
            AsyncStorageHelper.setUserAvatarUrl(data.user.profile_img);
            AsyncStorageHelper.setAffId(data.user.affiliate_id);
            setLoginData(data.user.username, true, data.user.profile_img);
            navigation.replace('Drawer', {screen: 'Network'});
          } else {
            navigation.replace('ProfileUpdate', {
              name: data.user.name,
              userName: data.user.username,
            });
          }
        } else {
          WSnackBar.show({data: `❌ ${data.message}`});
        }
      })
      .catch((error) => {
        console.log('getUserDetails Error', error);
      })
      .finally(() => setIsLoading(false));
  };

  const nextHandler = () => {
    if (!isShowConfirm) {
      return setIsShowConfirm(true);
    }

    // console.log('passwordInput', passwordInput);
    // console.log('confirmPassword', confirmPassword);

    if (passwordInput !== confirmPassword) {
      return WToast.show({
        data: "Passwords Doesn't Match",
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    const email = emailInput.trim().toLowerCase();

    const postData = {
      email,
      password: tempPassword,
      newPassword: passwordInput,
    };

    // console.log('postData', postData);

    Axios.post(`${GX_AUTH_URL}/gx/user/login`, postData)
      .then((resp) => {
        const {data} = resp;

        // console.log('Login Data', data);
        if (data.status) {
          Axios.post(`${GX_API_ENDPOINT}/gxb/apps/register/user`, {
            email,
            app_code: APP_CODE,
          }).then((profileResp) => {
            if (profileResp.data.profile_id) {
              AsyncStorageHelper.setProfileId(profileResp.data.profile_id);
              setProfileId(profileResp.data.profile_id);
            }
          });
          AsyncStorageHelper.setIsLoggedIn(true);
          AsyncStorageHelper.setLoginEmail(email);
          AsyncStorageHelper.setAppToken(data.idToken);
          AsyncStorageHelper.setRefreshToken(data.refreshToken);
          AsyncStorageHelper.setDeviceKey(data.device_key);
          getUserDetails(email);
        } else {
          setIsLoading(false);
          WToast.show({
            data: data.message || 'Login Error',
            position: WToast.position.TOP,
          });
          setPasswordInput('');
          setConfirmPassword('');
          setIsShowConfirm(false);
        }
      })
      .catch((error) => {
        setIsLoading(false);
        console.log('Error on Login', error);
        WToast.show({data: 'Network Error', position: WToast.position.TOP});
        setPasswordInput('');
        setConfirmPassword('');
        setIsShowConfirm(false);
      });
  };

  return (
    <View style={styles.container}>
      <View style={styles.loginContainer}>
        <Text style={styles.loginTitle}>
          {isShowConfirm ? 'Almost Done' : 'Great Job'}
        </Text>
        <Text style={styles.loginSubTitle}>
          {isShowConfirm
            ? 'Confirm Your New Password'
            : 'Now Create A New Password'}
        </Text>
        <LoginInputField
          placeholder="PASSWORD"
          type="password"
          secureTextEntry
          value={isShowConfirm ? confirmPassword : passwordInput}
          editable={!isLoading}
          onChangeText={(text) =>
            isShowConfirm ? setConfirmPassword(text) : setPasswordInput(text)
          }
          validatorStatus={
            isShowConfirm ? confrimPswdvalidityStatus : passwordValidityStatus
          }
          showNext
          onNext={nextHandler}
          onBlur={() => setIsInputFocused(false)}
          onFocus={() => setIsInputFocused(true)}
        />
        {isInputFocused || (
          <View style={styles.actionContainer}>
            <TouchableOpacity style={styles.loginButton} onPress={nextHandler}>
              <Text style={styles.loginButtonText}>Next</Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    </View>
  );
};

export default ResetPassword;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loginContainer: {
    flex: 1,
    paddingHorizontal: 30,
    justifyContent: 'center',
  },
  loginTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 50,
  },
  loginSubTitle: {
    color: '#999C9A',
    marginBottom: 40,
    fontFamily: 'Montserrat',
  },
  actionContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  loginButton: {
    marginTop: 15,
    marginBottom: 30,
    backgroundColor: '#08152D',
    width: 150,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  loginButtonText: {
    color: 'white',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
});
