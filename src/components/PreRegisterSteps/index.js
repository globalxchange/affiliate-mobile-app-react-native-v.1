import {useNavigation} from '@react-navigation/native';
import React, {useEffect, useRef, useState} from 'react';
import {
  Keyboard,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Animated, {
  Clock,
  interpolate,
  set,
  useCode,
} from 'react-native-reanimated';
import ThemeData from '../../configs/ThemeData';
import {emailValidator} from '../../utils';
import ReanimatedTimingHelper from '../../utils/ReanimatedTimingHelper';
import LoginForm from './LoginForm';
import ResetPassword from './ResetPassword';

const PreRegisterSteps = ({isLoading, setIsLoading}) => {
  const navigation = useNavigation();

  const [activeRegStep, setActiveRegStep] = useState('');

  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);
  const [emailInput, setEmailInput] = useState('');
  const [tempPassword, setTempPassword] = useState('');
  const [emailValidityStatus, setEmailValidityStatus] = useState(false);
  const [passwordValidityStatus, setPasswordValidityStatus] = useState(false);

  const chatKeyboardAnimation = useRef(new Animated.Value(0));

  useEffect(() => {
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
      onKeyBoardShow,
    );
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
      onKeyBoardHide,
    );
    return () => {
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
        onKeyBoardShow,
      );
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
        onKeyBoardHide,
      );
    };
  }, []);

  useEffect(() => {
    const email = emailInput.trim().toLowerCase();

    setEmailValidityStatus(emailValidator(email));
  }, [emailInput]);

  useEffect(() => {
    setPasswordValidityStatus(tempPassword.length >= 6);
  }, [tempPassword]);

  useCode(
    () =>
      isKeyboardOpen
        ? [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ]
        : [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ],
    [isKeyboardOpen],
  );

  const onKeyBoardShow = (e) => {
    setKeyboardHeight(e.endCoordinates.height - 30);
    setIsKeyboardOpen(true);
  };

  const onKeyBoardHide = () => {
    setIsKeyboardOpen(false);
  };

  const onBackClick = () => {
    if (!activeRegStep || activeRegStep === 'TempLogin') {
      return navigation.goBack();
    }

    if (activeRegStep === 'RestPassword') {
      return setActiveRegStep('TempLogin');
    }
  };

  let activeView;

  switch (activeRegStep) {
    case 'TempLogin':
      activeView = (
        <LoginForm
          isLoading={isLoading}
          setIsLoading={setIsLoading}
          emailInput={emailInput}
          setEmailInput={setEmailInput}
          onNext={() => setActiveRegStep('RestPassword')}
          passwordInput={tempPassword}
          setPasswordInput={setTempPassword}
          emailValidityStatus={emailValidityStatus}
          passwordValidityStatus={passwordValidityStatus}
        />
      );
      break;

    case 'RestPassword':
      activeView = (
        <ResetPassword
          isLoading={isLoading}
          setIsLoading={setIsLoading}
          emailInput={emailInput}
          tempPassword={tempPassword}
          onNext={() => setActiveRegStep('RestPassword')}
        />
      );
      break;

    default:
      activeView = (
        <LoginForm
          isLoading={isLoading}
          setIsLoading={setIsLoading}
          emailInput={emailInput}
          setEmailInput={setEmailInput}
          onNext={() => setActiveRegStep('RestPassword')}
          passwordInput={tempPassword}
          setPasswordInput={setTempPassword}
          emailValidityStatus={emailValidityStatus}
          passwordValidityStatus={passwordValidityStatus}
        />
      );
  }

  return (
    <View style={styles.container}>
      <View style={styles.fragmentView}>{activeView}</View>
      <Animated.View
        style={[
          {
            height: interpolate(chatKeyboardAnimation.current, {
              inputRange: [0, 1],
              outputRange: [0, keyboardHeight],
            }),
          },
        ]}
      />
      <View style={styles.goBackContainer}>
        <Text style={styles.goBackText}>Go Back</Text>
        <TouchableOpacity onPress={onBackClick} style={styles.outlinedButton}>
          <Text style={styles.outlinedText}>Click Here</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default PreRegisterSteps;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  fragmentView: {
    flex: 1,
  },
  goBackContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 30,
    marginBottom: 30,
  },
  goBackText: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_BOLD,
  },
  outlinedButton: {
    borderColor: ThemeData.APP_MAIN_COLOR,
    borderWidth: 1,
    marginLeft: 10,
    paddingHorizontal: 15,
    paddingVertical: 5,
  },
  outlinedText: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 12,
  },
});
