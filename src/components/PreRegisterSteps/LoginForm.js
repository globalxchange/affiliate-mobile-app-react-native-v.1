/* eslint-disable react-native/no-inline-styles */
import Axios from 'axios';
import React, {useRef, useState} from 'react';
import {Platform, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Transition, Transitioning} from 'react-native-reanimated';
import {WToast} from 'react-native-smart-tip';
import {GX_AUTH_URL} from '../../configs';
import {emailValidator} from '../../utils';
import LoginInputField from '../LoginInputField';

const LoginForm = ({
  isLoading,
  setIsLoading,
  emailInput,
  setEmailInput,
  onNext,
  passwordInput,
  setPasswordInput,
  emailValidityStatus,
  passwordValidityStatus,
}) => {
  const [isEmailFocused, setIsEmailFocused] = useState(false);
  const [isPasswordFocused, setIsPasswordFocused] = useState(false);

  const transitionViewRef = useRef();

  const transition = (
    <Transition.Together>
      <Transition.Change />
    </Transition.Together>
  );

  const executeTransform = () => {
    if (Platform.OS === 'ios') {
      transitionViewRef.current.animateNextTransition();
    }
  };

  const loginClickHandler = () => {
    if (!isLoading) {
      if (!emailValidator(emailInput)) {
        return WToast.show({
          data: 'Enter a valid Email',
          position: WToast.position.TOP,
        });
      }

      if (!passwordInput) {
        return WToast.show({
          data: 'Enter your password',
          position: WToast.position.TOP,
        });
      }

      setIsLoading(true);
      const email = emailInput.trim().toLowerCase();

      const postData = {email, password: passwordInput};

      Axios.post(`${GX_AUTH_URL}/gx/user/login`, postData)
        .then((resp) => {
          const {data} = resp;

          // console.log('Temp Login Data', data);

          if (data.resetPassword) {
            onNext();
          } else {
            WToast.show({
              data: data.message || 'API Error',
              position: WToast.position.TOP,
            });
          }
        })
        .catch((error) => {
          console.log('Error on Login', error);
          WToast.show({data: 'Network Error', position: WToast.position.TOP});
        })
        .finally(() => setIsLoading(false));
    }
  };

  const requestPasswordFocus = () => {
    if (!emailValidator(emailInput.trim().toLowerCase())) {
      return WToast.show({
        data: 'Enter a valid Email',
        position: WToast.position.TOP,
      });
    }

    executeTransform();
    setIsEmailFocused(false);
    setIsPasswordFocused(true);
  };

  return (
    <View style={styles.container}>
      <Transitioning.View
        ref={transitionViewRef}
        transition={transition}
        style={styles.loginContainer}>
        <Text style={styles.loginTitle}>Step 1</Text>
        <Text style={styles.loginSubTitle}>
          Enter The Temporary Credentials You Got In Your Email
        </Text>
        <LoginInputField
          style={{display: isPasswordFocused ? 'none' : 'flex'}}
          placeholder="EMAIL"
          type="emailAddress"
          value={emailInput}
          editable={!isLoading}
          onChangeText={(text) => setEmailInput(text)}
          onFocus={() => {
            executeTransform();
            setIsEmailFocused(true);
          }}
          onBlur={() => {
            executeTransform();
            setIsEmailFocused(false);
          }}
          focus={isEmailFocused}
          validatorStatus={emailValidityStatus}
          showNext
          onNext={requestPasswordFocus}
        />
        <LoginInputField
          style={{display: isEmailFocused ? 'none' : 'flex'}}
          placeholder="PASSWORD"
          type="password"
          secureTextEntry
          value={passwordInput}
          editable={!isLoading}
          onChangeText={(text) => setPasswordInput(text)}
          onFocus={() => {
            executeTransform();
            setIsPasswordFocused(true);
          }}
          onBlur={() => {
            executeTransform();
            setIsPasswordFocused(false);
          }}
          focus={isPasswordFocused}
          validatorStatus={passwordValidityStatus}
          showNext
          onNext={loginClickHandler}
        />
        {isEmailFocused || isPasswordFocused || (
          <View style={styles.actionContainer}>
            <TouchableOpacity
              style={styles.loginButton}
              onPress={loginClickHandler}>
              <Text style={styles.loginButtonText}>Next</Text>
            </TouchableOpacity>
          </View>
        )}
      </Transitioning.View>
    </View>
  );
};

export default LoginForm;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loginContainer: {
    flex: 1,
    paddingHorizontal: 30,
    justifyContent: 'center',
  },
  loginTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 50,
  },
  loginSubTitle: {
    color: '#999C9A',
    marginBottom: 40,
    fontFamily: 'Montserrat',
  },
  loginButton: {
    marginTop: 15,
    marginBottom: 30,
    backgroundColor: '#08152D',
    width: 150,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  loginButtonText: {
    color: 'white',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  actionContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
