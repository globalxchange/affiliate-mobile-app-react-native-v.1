/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {GX_API_ENDPOINT} from '../configs';
import LoadingAnimation from './LoadingAnimation';
import * as WebBrowser from 'expo-web-browser';
import Axios from 'axios';
import BottomSheetLayout from '../layouts/BottomSheetLayout';

const InstaCryptoInstaller = ({isOpen, setIsOpen}) => {
  const [links, setLinks] = useState();

  useEffect(() => {
    Axios.get(`${GX_API_ENDPOINT}/gxb/apps/mobile/app/links/logs/get`, {
      params: {app_code: 'instacrypto'},
    })
      .then((resp) => {
        const {data} = resp;

        // console.log('Data', data);

        if (data.status) {
          const latestUpdate = data.logs[0];

          if (latestUpdate) {
            setLinks({
              android: latestUpdate.android_app_link,
              ios: latestUpdate.ios_app_link,
            });
          }
        }
      })
      .catch((error) => {});
  }, []);

  const onButtonClick = (isIos) => {
    WebBrowser.openBrowserAsync(`${isIos ? links?.ios : links?.android}`);
  };

  return (
    <BottomSheetLayout isOpen={isOpen} onClose={() => setIsOpen(false)}>
      {links ? (
        <View style={styles.fragmentContainer}>
          <Image
            style={styles.headerLogo}
            source={require('../assets/insta-crypto-full-icon.png')}
            resizeMode="contain"
          />
          <Text style={styles.headerText}>Install InstaCrypto On</Text>
          <TouchableOpacity
            onPress={() => onButtonClick(true)}
            style={styles.actionButton}>
            <Image
              source={require('../assets/apple-icon.png')}
              resizeMode="contain"
              style={styles.actionIcon}
            />
            <Text style={styles.actionText}>iOS</Text>
          </TouchableOpacity>
          {/* <TouchableOpacity
            onPress={() => onButtonClick(false)}
            style={styles.actionButton}>
            <Image
              source={require('../assets/android-icon.png')}
              resizeMode="contain"
              style={styles.actionIcon}
            />
            <Text style={[styles.actionText, {color: '#8BC34A'}]}>Android</Text>
          </TouchableOpacity> */}
          <TouchableOpacity
            onPress={() => setIsOpen(false)}
            style={styles.closeButton}>
            <Text style={styles.closeText}>Close</Text>
          </TouchableOpacity>
        </View>
      ) : (
        <View style={styles.loadingAnimation}>
          <LoadingAnimation />
        </View>
      )}
    </BottomSheetLayout>
  );
};

export default InstaCryptoInstaller;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    backgroundColor: '#08152D',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    overflow: 'hidden',
  },
  fragmentContainer: {
    backgroundColor: 'white',
    paddingTop: 30,
    alignItems: 'center',
  },
  headerLogo: {
    height: 60,
  },
  headerText: {
    textAlign: 'center',
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    marginTop: 20,
  },
  actionButton: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#EDEDED',
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 15,
    width: 200,
    justifyContent: 'center',
    marginTop: 25,
  },
  actionIcon: {
    height: 25,
    width: 25,
  },
  actionText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#555555',
    fontSize: 16,
    marginLeft: 15,
  },
  closeButton: {
    marginTop: 40,
    backgroundColor: '#186AB4',
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  closeText: {
    fontFamily: 'Montserrat-Bold',
    color: 'white',
  },
  loadingAnimation: {height: 350, justifyContent: 'center'},
});
