import React, {useState, useRef} from 'react';
import {StyleSheet, Text, View, Dimensions} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import LoadingAnimation from './LoadingAnimation';
import {scrollInterpolator, animatedStyles} from '../utils/CarouselAnimation';
import {usdValueFormatter} from '../utils';

const {width} = Dimensions.get('window');

const SLIDER_WIDTH = width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.5);
const ITEM_HEIGHT = Math.round((ITEM_WIDTH * 3) / 6);

const BrokerEarningCarousel = ({carouselData}) => {
  const [currentPos, setCurrentPos] = useState(0);

  const carouselRef = useRef();

  const onSnapToItem = (index) => {
    setCurrentPos(index);
    if (carouselData) {
      // setActiveFilterCurrency(carouselData[index].formData.coin);
    }
  };

  return (
    <View style={styles.container}>
      {carouselData ? (
        <Carousel
          ref={carouselRef}
          data={carouselData}
          sliderWidth={SLIDER_WIDTH}
          itemWidth={ITEM_WIDTH}
          containerCustomStyle={styles.carouselContainer}
          inactiveSlideShift={0}
          onSnapToItem={onSnapToItem}
          scrollInterpolator={scrollInterpolator}
          slideInterpolatedStyle={animatedStyles}
          useScrollView={true}
          loop
          inactiveSlideOpacity={0.5}
          inactiveSlideScale={0.8}
          renderItem={({item}) => (
            <View style={styles.itemContainer}>
              <Text style={styles.itemValue}>{item.value}</Text>
              <Text style={styles.itemLabel}>{item.title}</Text>
            </View>
          )}
        />
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation height={70} width={70} />
        </View>
      )}
    </View>
  );
};

export default BrokerEarningCarousel;

const styles = StyleSheet.create({
  container: {
    height: ITEM_HEIGHT + 10,
    justifyContent: 'center',
  },
  carouselContainer: {
    marginTop: 10,
  },
  itemContainer: {
    zIndex: 3,
    width: ITEM_WIDTH,
    height: ITEM_HEIGHT,
    borderRadius: 6,
    backgroundColor: '#08152D',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 15,
  },
  itemLabel: {
    color: 'white',
    fontSize: 8,
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
    marginTop: 5,
  },
  itemValue: {
    color: 'white',
    fontSize: 22,
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
  },
  counter: {
    marginTop: 25,
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  loadingContainer: {
    height: ITEM_HEIGHT,
    justifyContent: 'center',
  },
});
