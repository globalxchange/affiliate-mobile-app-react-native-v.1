import React from 'react';
import {StyleSheet, Text, View, Image, Dimensions} from 'react-native';

const {width, height} = Dimensions.get('window');

const CustomPicker = ({placeHolder, icon, value, emptyText}) => {
  return (
    <View style={styles.container}>
      <View style={styles.inputContainer}>
        <Text style={styles.placeHolder}>{placeHolder}</Text>
        <Text style={styles.input}>{value || emptyText}</Text>
      </View>
      <View style={styles.iconContainer}>
        <Image style={styles.icon} source={icon} resizeMode="contain" />
      </View>
    </View>
  );
};

export default CustomPicker;

const styles = StyleSheet.create({
  container: {
    borderColor: '#E9E8E8',
    borderWidth: 1,
    borderRadius: 12,
    paddingHorizontal: 15,
    paddingVertical: 6,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  inputContainer: {
    flex: 1,
  },
  placeHolder: {
    color: '#858585',
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
  input: {
    color: '#000000',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 14,
    paddingRight: 10,
    backgroundColor: 'white',
    paddingVertical: 3,
  },
  iconContainer: {
    borderColor: '#F6F6F6',
    borderWidth: 1,
    width: 40,
    height: 40,
    padding: 10,
    borderRadius: 20,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  icon: {
    flex: 1,
    height: null,
    width: null,
  },
  modalView: {
    width,
    height,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  modalHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  closeButton: {
    width: 28,
    height: 28,
    padding: 6,
    marginRight: 10,
  },
  closeIcon: {
    flex: 1,
    width: null,
    height: null,
  },
  optionItem: {
    paddingVertical: 12,
    borderBottomColor: '#E9E8E8',
    borderBottomWidth: 1,
    paddingHorizontal: 10,
  },
  optionText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'black',
  },
  emptyContainer: {
    marginTop: 40,
  },
  emptyText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'black',
    textAlign: 'center',
    fontSize: 20,
  },
});
