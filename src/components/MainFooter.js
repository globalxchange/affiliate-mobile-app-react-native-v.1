import React, {useState} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import FastImage from 'react-native-fast-image';
import {useNavigation} from '@react-navigation/native';
import BottomSheetLayout from '../layouts/BottomSheetLayout';

const MainFooter = () => {
  const {navigate} = useNavigation();

  const [isSheetOpen, setIsSheetOpen] = useState(false);

  const openSefWallet = () => {
    setIsSheetOpen(false);
    navigate('Wallet', {coin: 'SEF'});
  };

  return (
    <TouchableOpacity
      style={styles.footerContainer}
      onPress={() => setIsSheetOpen(true)}>
      <FastImage
        style={styles.footerIcon}
        source={require('../assets/sef-icon-full-icon.png')}
        resizeMode="contain"
      />
      {/* <TouchableOpacity
        style={styles.buttonContainer}
        onPress={() => setIsSheetOpen(true)}>
        <Text style={styles.buttonText}>Learn More</Text>
      </TouchableOpacity> */}
      <BottomSheetLayout
        isOpen={isSheetOpen}
        onClose={() => setIsSheetOpen(false)}>
        <View style={styles.viewContainer}>
          <Image
            style={styles.headerImage}
            source={require('../assets/sef-icon-full-icon-inverted.png')}
            resizeMode="contain"
          />
          <Text style={styles.desc}>
            SEF which stands for “Save Exchange Fees Coin” is the official
            cryptocurrency of the AffiliateApp platform. Keeping a balance of
            SEF allows you to avoid all fees charged by the platform.
          </Text>
          <Text style={styles.note}>
            Note: SEF Does Not Eleviate You From Fees Charged By The Bankers.
          </Text>
          <View style={styles.feeItemContainer}>
            <Image
              source={require('../assets/sef-mini-icon.png')}
              style={styles.feeImage}
              resizeMode="contain"
            />
            <Text style={styles.sefAmount}>20,000 SEF</Text>
            <Text style={styles.offAmount}>20% Off Fees</Text>
          </View>
          <View style={styles.feeItemContainer}>
            <Image
              source={require('../assets/sef-mini-icon.png')}
              style={styles.feeImage}
              resizeMode="contain"
            />
            <Text style={styles.sefAmount}>50,000 SEF</Text>
            <Text style={styles.offAmount}>50% Off Fees</Text>
          </View>
          <View style={styles.feeItemContainer}>
            <Image
              source={require('../assets/sef-mini-icon.png')}
              style={styles.feeImage}
              resizeMode="contain"
            />
            <Text style={styles.sefAmount}>100,000 SEF</Text>
            <Text style={styles.offAmount}>100% Off Fees</Text>
          </View>
        </View>
        <TouchableOpacity style={styles.goToButton} onPress={openSefWallet}>
          <Text style={styles.gotoButtonText}>Go To SEF Wallet</Text>
        </TouchableOpacity>
      </BottomSheetLayout>
    </TouchableOpacity>
  );
};

export default MainFooter;

const styles = StyleSheet.create({
  footerContainer: {
    backgroundColor: '#08152D',
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 30,
    justifyContent: 'center',
  },
  footerIcon: {
    width: 120,
    height: 30,
  },
  buttonContainer: {
    borderColor: '#FFFFFF',
    borderWidth: 1,
    height: 30,
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  buttonText: {
    color: '#FFFFFF',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 11,
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.2)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    // flex: 1,
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
  },
  viewContainer: {
    padding: 45,
  },
  headerImage: {
    height: 40,
    width: 200,
  },
  desc: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat',
    marginVertical: 20,
    fontSize: 12,
  },
  note: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
    marginBottom: 10,
  },
  feeItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 8,
    paddingVertical: 10,
    paddingHorizontal: 20,
    marginTop: 20,
  },
  feeImage: {
    height: 25,
    width: 25,
  },
  sefAmount: {
    flex: 1,
    paddingHorizontal: 15,
    fontFamily: 'Montserrat',
    color: '#788995',
  },
  offAmount: {
    fontFamily: 'Montserrat',
    color: '#788995',
  },
  goToButton: {
    backgroundColor: '#08152D',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  gotoButtonText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
    fontSize: 15,
  },
});
