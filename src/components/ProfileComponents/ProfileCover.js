/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useRef, useState} from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import Animated, {
  Clock,
  interpolate,
  set,
  useCode,
} from 'react-native-reanimated';
import ThemeData from '../../configs/ThemeData';
import ReanimatedTimingHelper from '../../utils/ReanimatedTimingHelper';
import PhotoPickerDialog from '../PhotoPickerDialog';
import ProfileAvatar from '../ProfileAvatar';
import SkeltonItem from '../SkeltonItem';
import * as ImagePicker from 'expo-image-picker';
import {FileSystem} from 'react-native-unimodules';
import {decode} from 'base64-arraybuffer';
import {S3} from 'aws-sdk';
import axios from 'axios';
import {GX_API_ENDPOINT, S3_CONFIG} from '../../configs';
import {AppContext} from '../../contexts/AppContextProvider';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import {useNavigation, useRoute} from '@react-navigation/core';

const {width} = Dimensions.get('window');

const ProfileCover = ({
  userData,
  followings,
  contacts,
  openBrands,
  isFragmentExpanded,
  setUserData,
  isLoggedUsed,
}) => {
  const {navigate} = useNavigation();
  const {params} = useRoute();

  const {userName, setLoginData} = useContext(AppContext);

  const [showAlert, setShowAlert] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const expandAnimation = useRef(new Animated.Value(1));

  useCode(
    () =>
      isFragmentExpanded
        ? [
            set(
              expandAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ]
        : [
            set(
              expandAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ],
    [isFragmentExpanded],
  );

  const changeProfilePic = async (useCamera) => {
    try {
      let result;
      const options = {
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 0.8,
      };

      if (useCamera) {
        result = await ImagePicker.launchCameraAsync(options);
      } else {
        result = await ImagePicker.launchImageLibraryAsync(options);
      }

      // console.log(result);

      setShowAlert(false);

      if (!result.cancelled) {
        setIsLoading(true);

        const BUCKET_NAME = 'gxnitrousdata';

        const S3Client = new S3({
          ...S3_CONFIG,
          Bucket: BUCKET_NAME,
        });

        const fPath = result.uri;

        let fileOptions = {encoding: FileSystem.EncodingType.Base64};
        FileSystem.readAsStringAsync(fPath, fileOptions)
          .then((data) => {
            const arrayBuffer = decode(data);

            const params = {
              Bucket: BUCKET_NAME,
              Key: `brandlogos/${userName}${Date.now()}.jpg`,
              Body: arrayBuffer,
              ContentType: 'image/jpeg',
              ACL: 'public-read',
            };

            S3Client.upload(params, async (err, s3Data) => {
              if (err) {
                console.log('Uploading Profile Pic Error', err);
              }

              if (s3Data.Location) {
                const email = await AsyncStorageHelper.getLoginEmail();
                const token = await AsyncStorageHelper.getAppToken();
                axios
                  .post(`${GX_API_ENDPOINT}/user/details/edit`, {
                    email,
                    field: 'profile_img',
                    value: s3Data.Location,
                    accessToken: token,
                  })
                  .then((resp) => {
                    // console.log('Profile Update Success', resp.data);
                    getUserDetails(email);
                  })
                  .catch((error) => {
                    console.log('Profile Update Failed', error);
                  })
                  .finally(() => setIsLoading(false));
              }
              // console.log('Upload Success', s3Data);
            });
          })
          .catch((err) => {
            console.log('​getFile -> err', err);
            setIsLoading(false);
          });
      }
    } catch (E) {
      console.log(E);
      setIsLoading(false);
      setShowAlert(false);
    }
  };

  const getUserDetails = (email) => {
    axios
      .get(`${GX_API_ENDPOINT}/user/details/get`, {
        params: {email},
      })
      .then((res) => {
        const {data} = res;

        AsyncStorageHelper.setUserName(data.user.username);
        AsyncStorageHelper.setUserAvatarUrl(data.user.profile_img);
        AsyncStorageHelper.setAffId(data.user.affiliate_id);
        setLoginData(data.user.username, true, data.user.profile_img, email);
        setUserData(data?.user || '');
      })
      .catch((error) => {
        console.log('getUserDetails Error', error);
      });
  };

  const openChainview = () => {
    // console.log('params?.userEmail', params?.userEmail);
    navigate('Chain', {
      screen: 'ChainView',
      params: {email: params?.userEmail || ''},
    });
  };

  return (
    <Animated.View
      style={{
        maxHeight: interpolate(expandAnimation.current, {
          inputRange: [0, 1],
          outputRange: [0, 300],
        }),
        backgroundColor: 'white',
        overflow: 'hidden',
      }}>
      <View style={styles.container}>
        <View style={[styles.coverContainer, {width, height: width * 0.4}]}>
          {userData ? (
            <Image
              style={styles.coverImage}
              resizeMode="cover"
              defaultSource={require('../../assets/default-profile-cover.png')}
            />
          ) : (
            <SkeltonItem itemWidth={width} itemHeight={width * 0.4} />
          )}
        </View>
        <View style={styles.profileDetailsContainer}>
          <View style={styles.profilePicContainer}>
            <TouchableOpacity
              activeOpacity={1}
              onPress={openChainview}
              style={styles.followersCotainer}>
              {contacts === undefined ? (
                <SkeltonItem
                  itemWidth={40}
                  itemHeight={25}
                  style={styles.followersCount}
                />
              ) : (
                <Text style={styles.followersCount}>{contacts || 0}</Text>
              )}
              <Text style={styles.followersText}>Contacts</Text>
            </TouchableOpacity>
            {userData && !isLoading ? (
              <TouchableOpacity
                disabled={!isLoggedUsed}
                onPress={() => setShowAlert(true)}>
                <ProfileAvatar
                  size={80}
                  avatar={userData?.profile_img}
                  name={userData?.name}
                />
              </TouchableOpacity>
            ) : (
              <SkeltonItem
                itemWidth={80}
                itemHeight={80}
                style={{borderRadius: 40}}
              />
            )}
            <TouchableOpacity
              onPress={openBrands}
              activeOpacity={1}
              style={styles.followersCotainer}>
              {followings === undefined ? (
                <SkeltonItem
                  itemWidth={40}
                  itemHeight={25}
                  style={styles.followersCount}
                />
              ) : (
                <Text style={styles.followersCount}>{followings || 0}</Text>
              )}
              <Text style={styles.followersText}>Following</Text>
            </TouchableOpacity>
          </View>

          {userData ? (
            <Text style={styles.profileName}>{userData?.name}</Text>
          ) : (
            <SkeltonItem
              itemWidth={150}
              itemHeight={30}
              style={styles.profileName}
            />
          )}
          {userData ? (
            <Text style={styles.profileBio}>{userData?.bio}</Text>
          ) : (
            <SkeltonItem
              itemWidth={100}
              itemHeight={10}
              style={styles.profileBio}
            />
          )}
        </View>
      </View>
      <PhotoPickerDialog
        isOpen={showAlert}
        setIsOpen={setShowAlert}
        callBack={(isCamera) => changeProfilePic(isCamera)}
      />
    </Animated.View>
  );
};

export default ProfileCover;

const styles = StyleSheet.create({
  container: {
    marginBottom: 25,
  },
  coverContainer: {},
  coverImage: {
    flex: 1,
    height: null,
    width: null,
  },
  profileDetailsContainer: {
    alignItems: 'center',
    marginTop: -40,
  },
  profilePicContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    alignItems: 'center',
  },
  followersCotainer: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    backgroundColor: 'white',
    height: 55,
    width: 110,
    justifyContent: 'center',
    alignItems: 'center',
  },
  followersCount: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 19,
    textAlign: 'center',
  },
  followersText: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    fontSize: 11,
  },
  profileName: {
    fontFamily: ThemeData.FONT_BOLD,
    marginTop: 10,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 22,
    textTransform: 'capitalize',
    textAlign: 'center',
  },
  profileBio: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
    marginTop: 5,
    textAlign: 'center',
  },
});
