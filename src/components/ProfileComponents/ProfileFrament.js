import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import FollowedBrands from './FollowedBrands';

const ProfileFrament = ({userData, activeTab, followings}) => {
  let fragment;

  switch (activeTab?.title) {
    case 'Brands':
      fragment = (
        <FollowedBrands
          key={Date.now()}
          userData={userData}
          followings={followings}
        />
      );
      break;
  }

  return <View style={styles.container}>{fragment}</View>;
};

export default ProfileFrament;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
