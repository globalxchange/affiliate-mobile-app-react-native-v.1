import React from 'react';
import {FlatList, Image, StyleSheet, Text, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';

const FollowedBrandDetails = ({selectecBrand, onBack, userData}) => {
  const SCORE_LIST = [
    {
      title: 'Products',
      score: 7.1,
      subTitle: `How Does ${userData?.name} Rank There Products?`,
    },
    {
      title: 'People',
      score: 7.1,
      subTitle: `How Does ${userData?.name} Rank There Leadership?`,
    },
    {
      title: 'Profits',
      score: 7.1,
      subTitle: `How Does ${userData?.name} Rank There Earning Potential?`,
    },
  ];

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Image
          style={styles.brandIcon}
          resizeMode="contain"
          source={{uri: selectecBrand?.bankerDetails?.profilePicURL}}
        />
        <View style={styles.brandDeatils}>
          <Text style={styles.brandTitle}>
            {selectecBrand?.bankerDetails?.displayName}
          </Text>
          <Text numberOfLines={1} style={styles.brandSubtitle}>
            How Does {userData?.name} Rank The {selectecBrand?.title}?
          </Text>
        </View>
        <Text style={styles.brandScore}>8.1</Text>
      </View>
      <View style={styles.listContainer}>
        <FlatList
          contentContainerStyle={{paddingVertical: 10}}
          data={SCORE_LIST}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => item.title}
          renderItem={({item}) => (
            <View style={styles.scoreItem}>
              <View style={styles.scoreItemDetails}>
                <Text style={styles.scoreItemTitle}>{item.title}</Text>
                <Text numberOfLines={1} style={styles.scoreItemSubtitle}>
                  {item.subTitle}
                </Text>
              </View>
              <Text style={styles.itemScore}>{item.score}</Text>
            </View>
          )}
        />
      </View>
    </View>
  );
};

export default FollowedBrandDetails;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    paddingHorizontal: 30,
    paddingVertical: 25,
  },
  brandIcon: {
    width: 50,
    height: 50,
  },
  brandDeatils: {
    flex: 1,
    paddingHorizontal: 15,
  },
  brandTitle: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 22,
    marginBottom: 5,
  },
  brandSubtitle: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 11,
  },
  brandScore: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 22,
  },
  listContainer: {
    flex: 1,
    paddingTop: 10,
  },
  scoreItem: {
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginHorizontal: 30,
    marginBottom: 15,
    alignItems: 'center',
    paddingHorizontal: 30,
    paddingVertical: 20,
  },
  scoreItemDetails: {
    flex: 1,
    paddingRight: 15,
  },
  scoreItemTitle: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 17,
    marginBottom: 4,
  },
  scoreItemSubtitle: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 10,
  },
  itemScore: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 19,
  },
});
