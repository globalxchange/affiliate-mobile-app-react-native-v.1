/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import SkeltonItem from '../SkeltonItem';
import FollowedBrandDetails from './FollowedBrandDetails';

const {width} = Dimensions.get('window');

const FollowedBrands = ({userData, followings}) => {
  const [selectecBrand, setSelectecBrand] = useState();

  if (selectecBrand) {
    return (
      <FollowedBrandDetails
        selectecBrand={selectecBrand}
        onBack={() => setSelectecBrand()}
        userData={userData}
      />
    );
  }

  return (
    <View style={styles.container}>
      {userData ? (
        <>
          <Text style={styles.listHeader}>{`${userData?.name}'s Brands`}</Text>
          <Text style={styles.listSubheader}>
            Click To See {`${userData?.name}'s`} Opinion On Them
          </Text>
        </>
      ) : (
        <>
          <SkeltonItem
            style={styles.listHeader}
            itemHeight={22}
            itemWidth={150}
          />
          <SkeltonItem
            style={styles.listSubheader}
            itemHeight={12}
            itemWidth={250}
          />
        </>
      )}
      {followings ? (
        <FlatList
          contentContainerStyle={{
            paddingHorizontal: 10,
          }}
          data={followings?.bankers}
          numColumns={3}
          keyExtractor={(item, index) => item.title + index}
          showsVerticalScrollIndicator={false}
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() => setSelectecBrand(item)}
              style={styles.itemContainer}>
              <View style={styles.itemImageContainer}>
                <Image
                  style={styles.itemImage}
                  source={{uri: item?.bankerDetails?.profilePicURL}}
                  resizeMode="contain"
                />
              </View>
              <Text style={styles.itemName}>
                {item?.bankerDetails?.displayName}
              </Text>
            </TouchableOpacity>
          )}
          ListEmptyComponent={
            <View style={styles.emptyContainer}>
              <Text style={styles.emptyText}>
                You Are Not Following Any Brand
              </Text>
            </View>
          }
        />
      ) : (
        <FlatList
          contentContainerStyle={{paddingHorizontal: 10}}
          data={Array(6).fill(1)}
          numColumns={3}
          keyExtractor={(_, index) => index}
          showsVerticalScrollIndicator={false}
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() => setSelectecBrand(item)}
              style={styles.itemContainer}>
              <View
                style={[
                  styles.itemImageContainer,
                  {padding: 0, justifyContent: 'center', alignItems: 'center'},
                ]}>
                <SkeltonItem
                  itemWidth={60}
                  itemHeight={60}
                  style={{borderRadius: 30}}
                />
              </View>
              <Text style={styles.itemName}>
                {item?.bankerDetails?.displayName}
              </Text>
              <SkeltonItem itemWidth={80} itemHeight={8} />
            </TouchableOpacity>
          )}
        />
      )}
    </View>
  );
};

export default FollowedBrands;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 10,
    paddingTop: 10,
  },
  listHeader: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    paddingHorizontal: 20,
    fontSize: 20,
    marginTop: 10,
    marginBottom: 5,
    textTransform: 'capitalize',
  },
  listSubheader: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    paddingHorizontal: 20,
    fontSize: 14,
    marginBottom: 10,
    textTransform: 'capitalize',
  },
  itemContainer: {
    alignItems: 'center',
    paddingVertical: 10,
    marginHorizontal: 10,
  },
  itemImageContainer: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    // flex: 0.333,
    // width: '33.333%',
    height: width / 3 - 30,
    width: width / 3 - 30,
    padding: 30,
  },
  itemImage: {
    flex: 1,
    height: null,
    width: null,
  },
  itemName: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 10,
    textAlign: 'center',
    marginTop: 10,
  },
  emptyContainer: {
    marginTop: 50,
  },
  emptyText: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 18,
  },
});

const BRANDS = [
  {
    title: 'Vault',
    icon: require('../../assets/liquid-icon.png'),
  },
  {
    title: 'Agency',
    icon: require('../../assets/agency-icon-small.png'),
  },
  {
    title: 'MoneyMarkets',
    icon: require('../../assets/liquid-earnings-icon.png'),
  },
  {
    title: 'Vault',
    icon: require('../../assets/liquid-icon.png'),
  },
  {
    title: 'Agency',
    icon: require('../../assets/agency-icon-small.png'),
  },
  {
    title: 'MoneyMarkets',
    icon: require('../../assets/liquid-earnings-icon.png'),
  },
  {
    title: 'Vault',
    icon: require('../../assets/liquid-icon.png'),
  },
  {
    title: 'Agency',
    icon: require('../../assets/agency-icon-small.png'),
  },
  {
    title: 'MoneyMarkets',
    icon: require('../../assets/liquid-earnings-icon.png'),
  },
];
