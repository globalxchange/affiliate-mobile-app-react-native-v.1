import {faChevronDown, faChevronUp} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import React, {useEffect, useRef} from 'react';
import {
  Dimensions,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import ThemeData from '../../configs/ThemeData';
import {
  animatedStyles,
  scrollInterpolator,
} from '../../utils/CarouselAnimation';

const {width} = Dimensions.get('window');

const SLIDER_WIDTH = width;
const ITEM_WIDTH = SLIDER_WIDTH / 3;

const ProfileTab = ({
  activeTab,
  setActiveTab,
  isFragmentExpanded,
  setIsFragmentExpanded,
}) => {
  const carouselRef = useRef();

  useEffect(() => {
    setTimeout(() => {
      carouselRef.current?.snapToItem(1, true, true);
    }, 200);
    setActiveTab(TABS[1]);
  }, []);

  const onSnapToItem = (index) => {
    const item = TABS[index];
    setActiveTab({...item, key: Date.now()});
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={[styles.expandToggle]}
        onPress={() => setIsFragmentExpanded(!isFragmentExpanded)}>
        <FontAwesomeIcon
          icon={isFragmentExpanded ? faChevronDown : faChevronUp}
          color={ThemeData.APP_MAIN_COLOR}
          size={18}
        />
      </TouchableOpacity>

      <Carousel
        ref={carouselRef}
        data={TABS}
        sliderWidth={SLIDER_WIDTH}
        itemWidth={ITEM_WIDTH}
        containerCustomStyle={styles.carouselContainer}
        inactiveSlideShift={0}
        onSnapToItem={onSnapToItem}
        scrollInterpolator={scrollInterpolator}
        slideInterpolatedStyle={animatedStyles}
        useScrollView={true}
        loop
        scrollEnabled={false}
        renderItem={({item}) => (
          <View
            style={[
              styles.tabItem,
              activeTab?.title === item.title && styles.tabItemActive,
              {width: width / 3},
            ]}>
            <Text
              style={[
                styles.tabItemText,
                activeTab?.title === item.title && styles.tabItemActiveText,
              ]}>
              {item.title}
            </Text>
          </View>
        )}
      />

      {/* <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        data={TABS}
        keyExtractor={(item) => item.title}
        renderItem={({item}) => (
          <TouchableOpacity
            disabled={item.disabled}
            onPress={() => setActiveTab({...item, key: Date.now()})}>
            <View
              style={[
                styles.tabItem,
                activeTab?.title === item.title && styles.tabItemActive,
                {width: (width - 45) / 3},
              ]}>
              <Text
                style={[
                  styles.tabItemText,
                  activeTab?.title === item.title && styles.tabItemActiveText,
                ]}>
                {item.title}
              </Text>
            </View>
          </TouchableOpacity>
        )}
      /> */}
    </View>
  );
};

export default ProfileTab;

const styles = StyleSheet.create({
  container: {
    borderTopColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderBottomColor: ThemeData.BORDER_COLOR,
    flexDirection: 'row',
  },
  tabItem: {
    width: width / 4,
    paddingVertical: 15,
  },
  tabItemText: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    fontSize: 11,
    opacity: 0.5,
  },
  tabItemActive: {
    borderBottomColor: ThemeData.APP_MAIN_COLOR,
    borderBottomWidth: 1,
  },
  tabItemActiveText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 12,
    opacity: 1,
  },
  expandToggle: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    borderRightColor: ThemeData.BORDER_COLOR,
    borderRightWidth: 1,
    width: 45,
    position: 'absolute',
    left: 0,
    bottom: 0,
    top: 0,
    zIndex: 5,
  },
});

const TABS = [
  {title: 'Influence', disabled: true},
  {title: 'Brands'},
  {title: 'Services', disabled: true},
  {title: 'Milestones', disabled: true},
];
