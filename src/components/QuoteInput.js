import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  ActivityIndicator,
} from 'react-native';

const QuoteInput = ({
  title,
  unit,
  image,
  placeholder,
  value,
  setValue,
  enabled,
  isLoading,
  onFocus,
  onBlur,
}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.textInput}
          keyboardType="numeric"
          placeholder={placeholder}
          value={value}
          editable={enabled}
          returnKeyType="done"
          onChangeText={(text) => (enabled ? setValue(text) : null)}
          placeholderTextColor="#878788"
          onFocus={onFocus}
          onBlur={onBlur}
        />
        {isLoading && (
          <View style={[StyleSheet.absoluteFill, styles.loadingContainer]}>
            <ActivityIndicator size="small" color="#08152D" />
          </View>
        )}
        <View style={styles.unitContainer}>
          <Text style={styles.unitValue}>{unit}</Text>
          <Image source={image} style={styles.unitImage} resizeMode="cover" />
        </View>
      </View>
    </View>
  );
};

export default QuoteInput;

const styles = StyleSheet.create({
  container: {marginBottom: 10},
  title: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  inputContainer: {
    flexDirection: 'row',
    borderWidth: 2,
    borderRadius: 8,
    borderColor: '#EBEBEB',
    height: 50,
  },
  textInput: {
    flexGrow: 1,
    width: 0,
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    paddingHorizontal: 20,
    color: 'black',
  },
  unitContainer: {
    width: '45%',
    flexDirection: 'row',
    alignItems: 'center',
    borderLeftColor: '#EBEBEB',
    borderLeftWidth: 2,
    justifyContent: 'center',
  },
  unitValue: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
  },
  unitImage: {
    height: 24,
    width: 24,
    borderRadius: 12,
    marginLeft: 15,
  },
  loadingContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(221, 221, 221, 0.7)',
    borderRadius: 8,
    zIndex: 1,
  },
  doneButton: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingRight: 10,
  },
  checkIcon: {
    height: 20,
    width: 20,
  },
});
