import React, {Component} from 'react';
import {StyleSheet, Image, Dimensions, TouchableOpacity} from 'react-native';
import {PanGestureHandler, State} from 'react-native-gesture-handler';
import Animated, {
  Value,
  event,
  block,
  cond,
  set,
  eq,
  add,
  interpolate,
  Extrapolate,
} from 'react-native-reanimated';
import {withSafeAreaInsets} from 'react-native-safe-area-context';
import {AppContext} from '../contexts/AppContextProvider';

const {width} = Dimensions.get('window');

const BUTTON_WIDTH = 55;
const CONTAINER_WIDTH = width - 60;
const MAX_X_VALUE = CONTAINER_WIDTH - BUTTON_WIDTH;

class FloatingButton extends Component {
  constructor(props) {
    super();

    this.state = {};
    this.translateX = new Value(MAX_X_VALUE);

    const offsetX = new Value(MAX_X_VALUE);

    this.onGesturesEvent = event([
      {
        nativeEvent: ({translationX: x, translationY: y, state}) =>
          block([
            cond(
              eq(state, State.UNDETERMINED),
              [set(this.translateX, MAX_X_VALUE)],
              [set(this.translateX, add(x, offsetX))],
            ),
            cond(eq(state, State.END), [set(offsetX, add(offsetX, x))]),
          ]),
      },
    ]);
  }

  render() {
    const {
      icon,
      onPress,
      insets: {bottom},
    } = this.props;

    return (
      <AppContext.Consumer>
        {(context) => {
          const {isVideoFullScreen} = context;

          if (isVideoFullScreen) {
            return null;
          }

          return (
            <PanGestureHandler
              onHandlerStateChange={this.onGesturesEvent}
              onGestureEvent={this.onGesturesEvent}>
              <Animated.View
                style={[
                  styles.container,
                  {bottom: bottom + 40},
                  {
                    transform: [
                      {
                        translateX: interpolate(this.translateX, {
                          inputRange: [0, MAX_X_VALUE],
                          outputRange: [0, MAX_X_VALUE],
                          extrapolate: Extrapolate.CLAMP,
                        }),
                      },
                    ],
                  },
                ]}>
                <TouchableOpacity
                  style={styles.floatingButton}
                  onPress={onPress}>
                  <Image
                    style={styles.floatingIcon}
                    source={icon}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
              </Animated.View>
            </PanGestureHandler>
          );
        }}
      </AppContext.Consumer>
    );
  }
}

export default withSafeAreaInsets(FloatingButton);

const styles = StyleSheet.create({
  container: {
    zIndex: 2,
    position: 'absolute',
    right: 30,
    bottom: 30,
    width: CONTAINER_WIDTH,
  },
  floatingButton: {
    width: BUTTON_WIDTH,
    height: BUTTON_WIDTH,
    borderRadius: BUTTON_WIDTH / 2,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#08152D',
  },
  floatingIcon: {
    width: 20,
    height: 20,
  },
});
