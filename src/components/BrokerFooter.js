import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import FastImage from 'react-native-fast-image';
import {useNavigation} from '@react-navigation/native';

const BrokerFooter = ({showBack, OverrideComponent}) => {
  const {goBack} = useNavigation();

  const onBackPress = () => {
    goBack();
  };

  return (
    <View style={styles.footerContainer}>
      {OverrideComponent || (
        <>
          {showBack && (
            <TouchableOpacity
              onPress={onBackPress}
              style={styles.buttonContainer}>
              <Image
                style={styles.backIcon}
                source={require('../assets/back-icon-white.png')}
                resizeMode="contain"
              />
            </TouchableOpacity>
          )}
          <FastImage
            style={styles.footerIcon}
            source={require('../assets/gx-token-icon.png')}
            resizeMode="contain"
          />
        </>
      )}
    </View>
  );
};

export default BrokerFooter;

const styles = StyleSheet.create({
  footerContainer: {
    backgroundColor: '#08152D',
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 30,
    justifyContent: 'center',
  },
  footerIcon: {
    width: 120,
    height: 30,
  },
  buttonContainer: {
    height: 30,
    justifyContent: 'center',
    paddingHorizontal: 20,
    position: 'absolute',
    left: 0,
  },
  buttonText: {
    color: '#FFFFFF',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 11,
  },
  backIcon: {
    height: 15,
    width: 20,
  },
});
