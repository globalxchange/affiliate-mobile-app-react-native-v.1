import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {formatterHelper, roundHelper} from '../../../utils';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';
import {WToast} from 'react-native-smart-tip';
import CustomNumField from '../../CustomNumField';
import WithdrawalContext from '../../../contexts/WithdrawalContext';
import ProceedButton from '../../WalletDeposit/ProceedButton';

class WithdrawForm extends Component {
  static contextType = WithdrawalContext;

  constructor(props) {
    super(props);

    this.state = {conversionRate: 0};
  }

  componentDidMount() {
    this.getConversionRate();
  }

  componentDidUpdate(prevProps, prevState, snapShot) {
    // Getting conversion  rate when wallet changing
    if (this.props.activeWallet !== prevProps.activeWallet) {
      this.getConversionRate();
      this.clearForms();
    }

    if (this.state.conversionRate !== prevState.conversionRate) {
      this.onFiatInputChange(this.state.fiatInput);
    }
  }

  clearForms = () => {};

  getConversionRate = () => {
    const {activeWallet} = this.props;
    const {selectedGxVault} = this.context;

    if (!activeWallet) {
      return;
    }

    Axios.get(`${GX_API_ENDPOINT}/forex/convert`, {
      params: {buy: selectedGxVault.coinSymbol, from: activeWallet.coinSymbol},
    })
      .then((resp) => {
        const {data} = resp;

        if (data.status) {
          const conversionRate =
            data[
              `${activeWallet.coinSymbol.toLowerCase()}_${selectedGxVault.coinSymbol.toLowerCase()}`
            ];

          this.setState({conversionRate});
        } else {
          this.setState({conversionRate: 0});
        }
      })
      .catch((error) => console.log('Conversion Rate error', error));
  };

  onCryptoInputChange = (input) => {
    const {
      cryptoInput,
      setCryptoInput,
      setFiatInput,
      selectedGxVault,
    } = this.context;
    const {conversionRate} = this.state;

    console.log('selectedGxVault', selectedGxVault);

    if (input) {
      // let newText;
      // if (input === 'BACK') {
      //   newText = cryptoInput.toString().slice(0, -1);
      // } else if (input === 'CLEAR') {
      //   newText = '';
      // } else if (input === '.') {
      //   if (!cryptoInput.toString().includes('.')) {
      //     newText = cryptoInput.toString() + input;
      //   } else {
      //     newText = cryptoInput.toString();
      //   }
      // } else {
      //   newText = cryptoInput.toString().concat(input);
      // }

      const parseInputValue = parseFloat(input || 0);

      const updatedFiatValue = parseInputValue * conversionRate;

      setCryptoInput(input);
      setFiatInput(
        roundHelper(
          isNaN(parseFloat(updatedFiatValue)) ? 0 : updatedFiatValue,
          selectedGxVault.coinSymbol,
        ),
      );
    } else {
      setFiatInput('');
      setCryptoInput('');
    }
  };

  onFiatInputChange = (input) => {
    const {
      fiatInput,
      setCryptoInput,
      setFiatInput,
      activeWallet,
    } = this.context;
    const {conversionRate} = this.state;

    if (input) {
      // let newText;
      // if (input === 'BACK') {
      //   newText = fiatInput.toString().slice(0, -1);
      // } else if (input === 'CLEAR') {
      //   newText = '';
      // } else if (input === '.') {
      //   if (!fiatInput.toString().includes('.')) {
      //     newText = fiatInput.toString() + input;
      //   } else {
      //     newText = fiatInput.toString();
      //   }
      // } else {
      //   newText = fiatInput.toString() + input;
      // }

      const parseInputValue = parseFloat(input || 0);

      const updatedCryptoValue = parseInputValue / conversionRate;
      setFiatInput(input);
      setCryptoInput(
        roundHelper(
          isNaN(parseFloat(updatedCryptoValue)) ? '' : updatedCryptoValue,
          activeWallet.coinSymbol,
        ),
      );
    } else {
      setFiatInput('');
      setCryptoInput('');
    }
  };

  onProceed = () => {
    const {setVaultStep, cryptoInput} = this.context;

    const {walletBalances, activeWallet} = this.props;

    if (!cryptoInput) {
      return WToast.show({data: 'Please input the amount'});
    }

    if (isNaN(parseFloat(cryptoInput))) {
      return WToast.show({
        data: 'Please input a valid Value',
      });
    }

    if (parseFloat(cryptoInput) <= 0) {
      return WToast.show({data: 'Enter amount to withdraw'});
    }

    const icpBalance =
      walletBalances[`${activeWallet.coinSymbol.toLowerCase()}_balance`];

    if (cryptoInput <= 0 || cryptoInput > icpBalance) {
      return WToast.show({
        data: `Not Enough Balance in ${activeWallet.coinSymbol} ICP Vault`,
      });
    }

    setVaultStep(2);
  };

  onHalfClicked = () => {
    const {activeWallet, walletBalances} = this.props;

    if (walletBalances) {
      const balance =
        walletBalances[`${activeWallet.coinSymbol.toLowerCase()}_balance`] || 0;

      const halfBalance = balance / 2;
      this.onCryptoInputChange(halfBalance);
    }
  };

  onFullClicked = () => {
    const {activeWallet, walletBalances} = this.props;

    if (walletBalances) {
      const balance =
        walletBalances[`${activeWallet.coinSymbol.toLowerCase()}_balance`] || 0;

      this.onCryptoInputChange(balance);
    }
  };

  render() {
    const {
      activeWallet,
      isCryptoFocused,
      setIsCryptoFocused,
      cryptoInput,
      fiatInput,
      selectedGxVault,
      setVaultStep,
      setCryptoInput,
      setFiatInput,
    } = this.context;

    const {isKeyBoardOpen} = this.props;

    return (
      <View style={styles.container}>
        <Text style={styles.header}>Withdrawal Amount</Text>
        <View style={styles.formContainer}>
          <View style={styles.inputForm}>
            <View style={styles.inputContainer}>
              {/* <CustomNumField
                style={[styles.input]}
                placeholder={formatterHelper('0.00', activeWallet.coinSymbol)}
                value={
                  isNaN(parseFloat(cryptoInput))
                    ? cryptoInput
                    : formatterHelper(
                        parseFloat(cryptoInput),
                        activeWallet.coinSymbol,
                      )
                }
                focused={isCryptoFocused}
                onChange={this.onCryptoInputChange}
                onFocus={() => setIsCryptoFocused(true)}
              /> */}
              <TextInput
                style={[styles.input, isCryptoFocused && styles.focusedText]}
                placeholder={formatterHelper('0.00', activeWallet.coinSymbol)}
                onChangeText={(text) => this.onCryptoInputChange(text)}
                keyboardType="numeric"
                returnKeyType="done"
                value={cryptoInput}
                onFocus={() => setIsCryptoFocused(true)}
                placeholderTextColor="#878788"
              />
              <TouchableOpacity
                onPress={this.onHalfClicked}
                style={styles.inputButton}>
                <Text style={styles.inputButtonText}>HALF</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={this.onFullClicked}
                style={styles.inputButton}>
                <Text style={styles.inputButtonText}>ALL</Text>
              </TouchableOpacity>
              <Text
                style={[
                  styles.cryptoName,
                  isCryptoFocused && styles.focusedText,
                ]}>
                {activeWallet.coinSymbol}
              </Text>
            </View>
            <View style={styles.divider} />
            <View style={styles.inputContainer}>
              {/* <CustomNumField
                style={[styles.input]}
                placeholder={formatterHelper(
                  '0.00',
                  selectedGxVault.coinSymbol,
                )}
                value={
                  isNaN(parseFloat(fiatInput))
                    ? fiatInput
                    : formatterHelper(
                        parseFloat(fiatInput),
                        selectedGxVault.coinSymbol,
                      )
                }
                focused={!isCryptoFocused}
                onChange={this.onFiatInputChange}
                onFocus={() => setIsCryptoFocused(false)}
              /> */}
              <TextInput
                style={[styles.input, isCryptoFocused || styles.focusedText]}
                placeholder={formatterHelper(
                  '0.00',
                  selectedGxVault.coinSymbol,
                )}
                onChangeText={(text) => this.onFiatInputChange(text)}
                keyboardType="numeric"
                returnKeyType="done"
                value={fiatInput}
                onFocus={() => setIsCryptoFocused(false)}
                placeholderTextColor="#878788"
              />
              <Text
                style={[
                  styles.cryptoName,
                  isCryptoFocused || styles.focusedText,
                ]}>
                {selectedGxVault.coinSymbol}
              </Text>
            </View>
          </View>
        </View>
        <ProceedButton
          onPress={this.onProceed}
          title="Proceed"
          onBack={() => {
            setVaultStep(0);
            setCryptoInput('');
            setFiatInput('');
          }}
        />
      </View>
    );
  }
}

export default WithdrawForm;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    marginBottom: 15,
  },
  formContainer: {
    flex: 1,
    paddingHorizontal: 40,
  },
  inputForm: {marginTop: 20},
  inputContainer: {flexDirection: 'row', alignItems: 'center'},
  input: {
    flexGrow: 1,
    width: 0,
    color: '#041939',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    paddingHorizontal: 10,
  },
  focusedInput: {
    color: '#08152D',
  },
  inputButton: {
    borderWidth: 1,
    borderColor: '#EDEDED',
    justifyContent: 'center',
    paddingHorizontal: 8,
    paddingVertical: 2,
    marginLeft: 5,
  },
  inputButtonText: {
    color: '#B4BBC4',
    fontFamily: 'Montserrat',
    fontSize: 10,
  },
  cryptoName: {
    marginLeft: 10,
    color: '#041939',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  focusedText: {
    color: '#08152D',
  },
  divider: {
    backgroundColor: '#08152D',
    height: 1,
    marginVertical: 5,
  },
});
