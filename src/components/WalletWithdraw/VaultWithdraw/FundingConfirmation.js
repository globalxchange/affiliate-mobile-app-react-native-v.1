import React, {useContext} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {formatterHelper} from '../../../utils';
import WithdrawalContext from '../../../contexts/WithdrawalContext';
import ProceedButton from '../../WalletDeposit/ProceedButton';

const FundingConfirmation = () => {
  const {
    selectedGxVault,
    activeWallet,
    setVaultStep,
    cryptoInput,
    fiatInput,
  } = useContext(WithdrawalContext);

  console.log('cryptoInput', cryptoInput);

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Confirm Withdraw</Text>
      <View style={styles.confirmationContainer}>
        <View style={styles.itemContainer}>
          <Text style={styles.title}>
            Your AffiliateApp {activeWallet.coinSymbol} Vault Will Be Debited
          </Text>
          <View style={styles.confirmation}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image
                style={styles.icon}
                source={require('../../../assets/bitcoin-icon.png')}
                resizeMode="contain"
              />
              <Text style={styles.walletName}>{activeWallet.coinName}</Text>
            </View>
            <Text style={styles.walletName}>
              {formatterHelper(
                isNaN(parseFloat(cryptoInput)) ? 0 : parseFloat(cryptoInput),
                activeWallet.coinSymbol,
              )}
            </Text>
          </View>
        </View>
        <View style={styles.itemContainer}>
          <Text style={styles.title}>
            Your GX {selectedGxVault.coinSymbol} Vault Will Be Credited
          </Text>
          <View style={styles.confirmation}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image
                style={styles.icon}
                source={require('../../../assets/bitcoin-icon.png')}
                resizeMode="contain"
              />
              <Text style={styles.walletName}>{selectedGxVault.coinName}</Text>
            </View>
            <Text style={styles.walletName}>
              {formatterHelper(
                isNaN(parseFloat(fiatInput)) ? 0 : parseFloat(fiatInput),
                selectedGxVault.coinSymbol,
              )}
            </Text>
          </View>
        </View>
      </View>
      <ProceedButton
        title={`Confirm Deposit of ${formatterHelper(
          cryptoInput || 0,
          activeWallet.coinSymbol,
        )} ${activeWallet.coinSymbol}`}
        onPress={() => setVaultStep(3)}
        onBack={() => setVaultStep(1)}
      />
    </View>
  );
};

export default FundingConfirmation;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  confirmationContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 40,
  },
  itemContainer: {
    marginBottom: 40,
  },
  title: {
    fontFamily: 'Montserrat',
    color: '#08152D',
    marginBottom: 10,
  },
  confirmation: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 8,
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  icon: {
    width: 24,
    height: 24,
    marginRight: 15,
  },
  walletName: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
  },
});
