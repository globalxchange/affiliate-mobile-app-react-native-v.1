import React, {useContext, useEffect, useRef} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {AppContext} from '../../../contexts/AppContextProvider';
import WithdrawalContext from '../../../contexts/WithdrawalContext';

const WithdrawSummary = () => {
  const navigation = useNavigation();

  const {activeWallet, cryptoInput} = useContext(WithdrawalContext);

  const {walletBalances} = useContext(AppContext);

  const walletRef = useRef();

  useEffect(() => {
    walletRef.current = activeWallet;
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Withdrawal Complete</Text>
      <View style={styles.mainContainer}>
        <Text style={styles.message}>
          Congratulations Your Have Withdrew {cryptoInput}{' '}
          {activeWallet.coinSymbol} From Your ICP Vault
        </Text>
        <View style={styles.confirmation}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image
              style={styles.icon}
              source={require('../../../assets/bitcoin-icon.png')}
              resizeMode="contain"
            />
            <Text style={styles.walletName}>
              New ICP {walletRef.current && walletRef.current.coinSymbol}{' '}
              Balance
            </Text>
          </View>
          <Text style={styles.walletName}>
            {walletRef.current &&
              walletBalances[
                `${walletRef.current.coinSymbol.toLowerCase()}_balance`
              ]}
          </Text>
        </View>
        <View style={styles.actionContainer}>
          <TouchableOpacity
            style={styles.action}
            onPress={() => navigation.navigate('Earn')}>
            <Image
              style={styles.actionIcon}
              source={require('../../../assets/back-to-feed-icon.png')}
              resizeMode="contain"
            />
            <Text style={styles.actionText}>Back To Feed</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.action, {marginLeft: 25}]}>
            <Image
              style={styles.actionIcon}
              source={require('../../../assets/bitcoin-icon.png')}
              resizeMode="contain"
            />
            <Text style={styles.actionText}>Invest Bitcoin</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default WithdrawSummary;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  mainContainer: {
    flex: 1,
    paddingHorizontal: 40,
    paddingVertical: 40,
    justifyContent: 'space-between',
  },
  message: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat',
  },
  confirmation: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 8,
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  icon: {
    width: 24,
    height: 24,
    marginRight: 15,
  },
  walletName: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
  },
  actionContainer: {
    flexDirection: 'row',
  },
  action: {
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    backgroundColor: 'white',
    borderRadius: 8,
    padding: 20,
    flex: 1,
  },
  actionIcon: {
    width: 45,
    height: 45,
  },
  actionText: {
    marginTop: 10,
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
  },
});
