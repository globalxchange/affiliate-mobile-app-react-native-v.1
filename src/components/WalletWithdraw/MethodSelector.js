/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useState, useEffect} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {DepositContext} from '../../contexts/DepositContext';
import WithdrawalContext from '../../contexts/WithdrawalContext';
import {getFundingMethodIcon} from '../../utils';
import {AppContext} from '../../contexts/AppContextProvider';

const MethodSelector = ({setPathId}) => {
  const {activeWallet} = useContext(DepositContext);
  const {setStep} = useContext(WithdrawalContext);
  const {pathData} = useContext(AppContext);

  const [isCryptoDepositAvailable, setIsCryptoDepositAvailable] = useState(
    false,
  );

  useEffect(() => {
    if (activeWallet.asset_type === 'Crypto') {
      const path = pathData.find(
        (x) =>
          x.select_type === 'withdraw' &&
          x.to_currency === activeWallet.coinSymbol,
      );
      // console.log('Path', path);
      if (path) {
        setPathId(path.path_id);
        setIsCryptoDepositAvailable(true);
      } else {
        setPathId('');
        setIsCryptoDepositAvailable(false);
      }
    }
  }, [pathData, activeWallet]);

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Select Withdrawal Method</Text>
      <View style={styles.methodList}>
        {activeWallet.asset_type === 'Crypto' && isCryptoDepositAvailable ? (
          <TouchableOpacity
            onPress={() => setStep('CryptoWithdraw')}
            style={styles.menuItem}>
            <Image
              style={styles.menuItemImage}
              source={getFundingMethodIcon(activeWallet.coinSymbol)}
              resizeMode="contain"
            />
          </TouchableOpacity>
        ) : null}
        <TouchableOpacity
          onPress={() => setStep('VaultWithdraw')}
          style={styles.menuItem}>
          <Image
            style={styles.menuItemImage}
            source={require('../../assets/vault-logo.png')}
            resizeMode="contain"
          />
        </TouchableOpacity>
        {activeWallet.asset_type === 'Fiat' ? (
          <TouchableOpacity
            disabled
            style={[styles.menuItem, {borderBottomWidth: 0}]}>
            <Image
              style={styles.menuItemImage}
              source={require('../../assets/snappy-logo.png')}
              resizeMode="contain"
            />
          </TouchableOpacity>
        ) : null}
      </View>
    </View>
  );
};

export default MethodSelector;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  methodList: {flex: 1},
  menuItem: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    borderBottomColor: '#EBEBEB',
    borderBottomWidth: 1,
  },
  menuItemImage: {
    width: 100,
  },
  smallMenuIcon: {
    width: 20,
    height: 20,
  },
  methodName: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#001D41',
    marginLeft: 20,
  },
});
