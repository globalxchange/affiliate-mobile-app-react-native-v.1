import React, {Component, createRef} from 'react';
import Axios from 'axios';
import {GX_API_ENDPOINT, APP_CODE} from '../../../configs';
import {AppContext} from '../../../contexts/AppContextProvider';
import WithdrawForm from './WithdrawForm';
import {Transitioning, Transition} from 'react-native-reanimated';
import {StyleSheet, View, Text} from 'react-native';
import WithdrawalSummary from './WithdrawalSummary';
import WithdrawConfirmation from './WithdrawConfirmation';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import {v4 as uuidv4} from 'uuid';
import {WToast} from 'react-native-smart-tip';
import LoadingAnimation from '../../LoadingAnimation';
import WithdrawComplete from './WithdrawComplete';
import CryHelper from '../../../utils/CryHelper';
import {roundHelper} from '../../../utils';

class WalletTransfer extends Component {
  static contextType = AppContext;

  constructor(props) {
    super(props);

    this.state = {
      isCryptoFocused: true,
      cryptoInput: '',
      fiatInput: '',
      conversionRate: 0,
      asPermission: null,
      isScanned: true,
      step: 0,
      actualWithdrawAmount: 0,
      isLoading: false,
    };
    this.setShowSummary = this.setShowSummary.bind(this);
    this.onCryptoInputChange = this.onCryptoInputChange.bind(this);
    this.onFiatInputChange = this.onFiatInputChange.bind(this);
    this.setIsCryptoFocused = this.setIsCryptoFocused.bind(this);
    this.goBackToEdit = this.goBackToEdit.bind(this);
    this.setShowConfirmation = this.setShowConfirmation.bind(this);
    this.setActualWithdrawAmount = this.setActualWithdrawAmount.bind(this);
    this.executeTransaction = this.executeTransaction.bind(this);

    this.transitionViewRef = createRef();
  }

  componentDidMount() {
    this.getConversionRate();
  }

  componentDidUpdate(prevProps, prevState, snapShot) {
    // Getting conversion  rate when wallet changing
    if (this.props.activeWallet !== prevProps.activeWallet) {
      this.getConversionRate();
      this.clearForms();
    }

    if (this.state.conversionRate !== prevState.conversionRate) {
      this.onFiatInputChange(this.state.fiatInput);
    }
  }

  setIsCryptoFocused = (isCryptoFocused) => {
    this.setState({isCryptoFocused});
  };

  setCryptoInput = (cryptoInput) => {
    this.setState({cryptoInput: cryptoInput.toString()});
  };

  setFiatInput = (fiatInput) => {
    this.setState({fiatInput: fiatInput.toString()});
  };

  clearForms = () => {
    if (this.transitionViewRef.current) {
      this.transitionViewRef.current.animateNextTransition();
    }

    this.setState({
      isCryptoFocused: true,
      cryptoInput: '',
      fiatInput: '',
      conversionRate: 0,
      asPermission: null,
      isScanned: true,
      step: 0,
      actualWithdrawAmount: 0,
    });
  };

  getConversionRate = () => {
    const {activeWallet} = this.props;

    if (!activeWallet) {
      return;
    }

    Axios.get(`${GX_API_ENDPOINT}/forex/convert`, {
      params: {buy: activeWallet.coinSymbol, from: 'USD'},
    })
      .then((resp) => {
        const {data} = resp;

        if (data.status) {
          const conversionRate =
            data[`${activeWallet.coinSymbol.toLowerCase()}_usd`];

          this.setState({conversionRate});
        } else {
          this.setState({conversionRate: 0});
        }
      })
      .catch((error) => console.log('Conversion Rate error', error));
  };

  onCryptoInputChange = (input) => {
    const {cryptoInput, conversionRate} = this.state;

    if (input) {
      // let newText;
      // if (input === 'BACK') {
      //   newText = cryptoInput.toString().slice(0, -1);
      // } else if (input === 'CLEAR') {
      //   newText = '';
      // } else {
      //   newText = cryptoInput.toString().concat(input);
      // }

      const updatedFiatValue = parseFloat(input || 0) * conversionRate;

      this.setCryptoInput(input);
      this.setFiatInput(
        roundHelper(
          isNaN(parseFloat(updatedFiatValue)) ? '' : updatedFiatValue,
          'USD',
        ),
      );
    } else {
      this.setCryptoInput('');
      this.setFiatInput('');
    }
  };

  onFiatInputChange = (input) => {
    const {fiatInput, conversionRate} = this.state;
    const {activeWallet} = this.props;

    if (input) {
      // let newText;
      // if (input === 'BACK') {
      //   newText = fiatInput.toString().slice(0, -1);
      // } else if (input === 'CLEAR') {
      //   newText = '';
      // } else {
      //   newText = fiatInput.toString() + input;
      // }
      const updatedCryptoValue = parseFloat(input || 0) / conversionRate;

      this.setFiatInput(input);
      this.setCryptoInput(
        roundHelper(
          isNaN(parseFloat(updatedCryptoValue)) ? '' : updatedCryptoValue,
          activeWallet.coinSymbol,
        ),
      );
    } else {
      this.setCryptoInput('');
      this.setFiatInput('');
    }
  };

  setShowSummary = () => {
    if (this.transitionViewRef.current) {
      this.transitionViewRef.current.animateNextTransition();
    }
    this.setState({step: 1});
  };

  setShowConfirmation = () => {
    if (this.transitionViewRef.current) {
      this.transitionViewRef.current.animateNextTransition();
    }
    this.setState({step: 2});
  };

  goBackToEdit = () => {
    if (this.transitionViewRef.current) {
      this.transitionViewRef.current.animateNextTransition();
    }
    this.setState({step: 0});
  };

  setActualWithdrawAmount = (actualWithdrawAmount) => {
    this.setState({actualWithdrawAmount});
  };

  executeTransaction = async () => {
    if (!this.state.isLoading) {
      const {cryptoInput} = this.state;
      const {activeWallet, pathId} = this.props;
      const {withdrawAddress, getBlockCheckData} = this.context;

      const email = await AsyncStorageHelper.getLoginEmail();
      const token = await AsyncStorageHelper.getAppToken();
      const profileId = await AsyncStorageHelper.getProfileId();

      const postData = {
        email,
        token,
        app_code: APP_CODE,
        profile_id: profileId,
        stats: false,
        identifier: uuidv4(),
        path_id: pathId,
        purchased_from: activeWallet.coinSymbol,
        from_amount: parseFloat(cryptoInput),
        friendId: withdrawAddress,
      };

      // console.log('Post Data', postData);

      const encryptedData = CryHelper.encryptPostData(postData);

      this.setState({isLoading: true});

      Axios.post(`${GX_API_ENDPOINT}/coin/vault/service/trade/execute`, {
        data: encryptedData,
      })
        .then((resp) => {
          const {data} = resp;
          // console.log('walletWithdraw', data);

          if (data.status) {
            getBlockCheckData();
            WToast.show({data: 'Withdraw to your wallet is initiated'});
            this.setState({step: 3});
          } else {
            WToast.show({data: data.message});
          }
        })
        .catch((error) =>
          console.log('Error on withdrawing to external Wallet', error),
        )
        .finally(() => this.setState({isLoading: false}));
    }
  };

  transition = (
    <Transition.Sequence>
      <Transition.Out type="fade" durationMs={200} interpolation="easeIn" />
      <Transition.Change />
      <Transition.Together>
        <Transition.In
          type="slide-bottom"
          durationMs={300}
          interpolation="easeOut"
          propagation="bottom"
        />
        <Transition.In type="fade" durationMs={300} delayMs={100} />
      </Transition.Together>
    </Transition.Sequence>
  );

  renderActiveComponent = () => {
    const {activeWallet, isKeyBoardOpen, openTxnAudit, pathId} = this.props;

    const {
      cryptoInput,
      fiatInput,
      isCryptoFocused,
      step,
      actualWithdrawAmount,
    } = this.state;

    switch (step) {
      case 0:
        return (
          <WithdrawForm
            activeWallet={activeWallet}
            isKeyBoardOpen={isKeyBoardOpen}
            cryptoInput={cryptoInput}
            fiatInput={fiatInput}
            isCryptoFocused={isCryptoFocused}
            setShowSummary={this.setShowSummary}
            onCryptoInputChange={this.onCryptoInputChange}
            onFiatInputChange={this.onFiatInputChange}
            setIsCryptoFocused={this.setIsCryptoFocused}
          />
        );
      case 1:
        return (
          <WithdrawalSummary
            cryptoInput={cryptoInput}
            activeWallet={activeWallet}
            goBackToEdit={this.goBackToEdit}
            setShowConfirmation={this.setShowConfirmation}
            actualWithdrawAmount={actualWithdrawAmount}
            setActualWithdrawAmount={this.setActualWithdrawAmount}
            pathId={pathId}
          />
        );
      case 2:
        return (
          <WithdrawConfirmation
            goBackToEdit={this.goBackToEdit}
            actualWithdrawAmount={actualWithdrawAmount}
            executeTransaction={this.executeTransaction}
          />
        );
      case 3:
        return (
          <WithdrawComplete
            amount={actualWithdrawAmount}
            coin={activeWallet.coinSymbol}
            onClose={() => this.clearForms()}
            openTxnAudit={openTxnAudit}
          />
        );
      default:
        return (
          <WithdrawForm
            activeWallet={activeWallet}
            isKeyBoardOpen={isKeyBoardOpen}
            cryptoInput={cryptoInput}
            fiatInput={fiatInput}
            isCryptoFocused={isCryptoFocused}
            setShowSummary={this.setShowSummary}
            onCryptoInputChange={this.onCryptoInputChange}
            onFiatInputChange={this.onFiatInputChange}
            setIsCryptoFocused={this.setIsCryptoFocused}
          />
        );
    }
  };

  render() {
    return (
      <Transitioning.View
        ref={this.transitionViewRef}
        style={styles.container}
        transition={this.transition}>
        {this.state.isLoading ? (
          <View style={styles.loadingContainer}>
            <LoadingAnimation />
            <Text style={styles.loadingText}>Initiating Transfer...</Text>
          </View>
        ) : (
          this.renderActiveComponent()
        )}
      </Transitioning.View>
    );
  }
}

export default WalletTransfer;

const styles = StyleSheet.create({
  container: {flex: 1},
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loadingText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
  },
});
