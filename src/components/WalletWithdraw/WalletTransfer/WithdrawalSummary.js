import React, {useState, useEffect, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {cryptoFormatter, formatterHelper} from '../../../utils';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import {APP_CODE, GX_API_ENDPOINT} from '../../../configs';
import {v4 as uuidv4} from 'uuid';
import CryHelper from '../../../utils/CryHelper';
import Axios from 'axios';
import LoadingAnimation from '../../LoadingAnimation';

const WithdrawalSummary = ({
  activeWallet,
  goBackToEdit,
  setShowConfirmation,
  cryptoInput,
  pathId,
  actualWithdrawAmount,
  setActualWithdrawAmount,
}) => {
  const [finalAmount, setFinalAmount] = useState(0);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    getQuote();
  }, [cryptoInput, activeWallet, pathId]);

  const getQuote = async () => {
    setIsLoading(true);
    const profileId = await AsyncStorageHelper.getProfileId();
    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    const postData = {
      email,
      token,
      app_code: APP_CODE,
      profile_id: profileId,
      stats: true,
      identifier: uuidv4(),
      path_id: pathId,
      purchased_from: activeWallet.coinSymbol,
      from_amount: parseFloat(cryptoInput),
    };

    console.log('Post Data', postData);

    const encryptedData = CryHelper.encryptPostData(postData);

    Axios.post(`${GX_API_ENDPOINT}/coin/vault/service/trade/execute`, {
      data: encryptedData,
    })
      .then((resp) => {
        const {data} = resp;

        console.log('Quote Resp', data);

        if (data.status) {
          setActualWithdrawAmount(parseFloat(cryptoInput) - data.finalToAmount);
          setFinalAmount(data.finalToAmount);
          setIsLoading(false);
        }
      })
      .catch((error) => {
        console.log('Error getting quote', error);
      });
  };

  if (isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <LoadingAnimation />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Withdrawal Summary</Text>
      <ScrollView
        bounces={false}
        contentContainerStyle={styles.summaryContainer}
        showsVerticalScrollIndicator={false}>
        <View style={styles.summaryItem}>
          <Text style={styles.summaryMainHeader}>Amount Before Fees</Text>
          <Text style={styles.summaryMainValue}>
            {cryptoFormatter(parseFloat(cryptoInput))} {activeWallet.coinSymbol}
          </Text>
        </View>
        <View style={styles.summaryItem}>
          <Text style={styles.summaryHeader}>Wallet Fees</Text>
          <Text style={styles.summaryValue}>
            {cryptoFormatter(actualWithdrawAmount)} {activeWallet.coinSymbol}
          </Text>
        </View>
        <View style={styles.summaryItem}>
          <Text style={styles.summaryMainHeader}>
            Your Recipient Will Receive
          </Text>
          <Text style={styles.summaryMainValue}>
            {cryptoFormatter(finalAmount)} {activeWallet.coinSymbol}
          </Text>
        </View>
      </ScrollView>
      <View style={styles.actionContainer}>
        <TouchableOpacity
          style={[styles.sendButton, {marginRight: 15}]}
          onPress={goBackToEdit}>
          <Text style={styles.buttonText}>Edit</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.sendButton}
          onPress={setShowConfirmation}>
          <Text style={styles.buttonText}>Continue</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default WithdrawalSummary;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
  },
  header: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  summaryContainer: {
    // flex: 1,
    paddingVertical: 20,
    paddingHorizontal: 50,
    // justifyContent: 'space-evenly',
  },
  summaryItem: {
    marginVertical: 5,
  },
  summaryMainHeader: {
    color: '#08152D',
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  summaryMainValue: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 16,
  },
  summaryHeader: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 11,
    opacity: 0.8,
  },
  summaryValue: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 13,
    opacity: 0.8,
  },
  actionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginBottom: 30,
    paddingHorizontal: 50,
  },
  sendButton: {
    backgroundColor: '#08152D',
    borderRadius: 6,
    paddingHorizontal: 25,
    paddingVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Montserrat-Bold',
    textAlign: 'center',
    fontSize: 12,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
});
