import React, {useContext} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import {cryptoFormatter} from '../../../utils';
import {AppContext} from '../../../contexts/AppContextProvider';

const WithdrawConfirmation = ({
  goBackToEdit,
  actualWithdrawAmount,
  executeTransaction,
}) => {
  const {withdrawAddress} = useContext(AppContext);

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Withdrawal Confirmation</Text>
      <View style={styles.confirmationContainer}>
        <View style={styles.messageBody}>
          <Text style={styles.addressHeader}>Recipient's Wallet Address</Text>
          <Text style={styles.address}>{withdrawAddress}</Text>
          <TouchableOpacity style={styles.blockCheckButton}>
            <Image
              style={styles.blockCheckIcon}
              source={require('../../../assets/block-check-icon.png')}
            />
            <Text style={styles.blokCheckText}>LOCKCHECK</Text>
          </TouchableOpacity>
        </View>
      </View>
      <Text style={styles.termsText}>
        By Clicking, You are Confirming This Address As Your Intended
        Destination of {cryptoFormatter(actualWithdrawAmount)} BTC
      </Text>
      <View style={styles.actionContainer}>
        <TouchableOpacity
          style={[styles.sendButton, {marginRight: 15}]}
          onPress={goBackToEdit}>
          <Text style={styles.buttonText}>Edit</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.sendButton}
          onPress={executeTransaction}>
          <Text style={styles.buttonText}>Continue</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default WithdrawConfirmation;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    paddingHorizontal: 50,
  },
  header: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  confirmationContainer: {flex: 1, justifyContent: 'center'},
  messageBody: {},
  addressHeader: {
    fontFamily: 'Montserrat',
    color: '#08152D',
    fontSize: 12,
    marginBottom: 5,
  },
  address: {
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
    fontSize: 12,
    marginBottom: 15,
  },
  blockCheckButton: {
    backgroundColor: '#08152D',
    flexDirection: 'row',
    paddingVertical: 5,
    paddingHorizontal: 8,
    justifyContent: 'center',
    borderRadius: 4,
    width: 140,
  },
  blockCheckIcon: {
    width: 16,
    height: 16,
  },
  blokCheckText: {
    fontFamily: 'Montserrat-Bold',
    color: 'white',
    fontSize: 13,
    marginLeft: 2,
  },
  termsText: {
    fontFamily: 'Montserrat',
    color: '#08152D',
    fontSize: 10,
    marginBottom: 10,
  },
  actionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginBottom: 30,
  },
  sendButton: {
    backgroundColor: '#08152D',
    borderRadius: 6,
    paddingHorizontal: 25,
    paddingVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Montserrat-Bold',
    textAlign: 'center',
  },
});
