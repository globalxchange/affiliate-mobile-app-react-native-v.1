import React, {useContext, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Image,
} from 'react-native';
import {
  cryptoVolumeFormatter,
  formatterHelper,
  usdValueFormatterWithoutSign,
} from '../../../utils';
import {AppContext} from '../../../contexts/AppContextProvider';
import {useNavigation} from '@react-navigation/native';
import {WToast} from 'react-native-smart-tip';
import CustomNumField from '../../CustomNumField';
import Clipboard from '@react-native-community/clipboard';

const WithdrawForm = ({
  activeWallet,
  isKeyBoardOpen,
  isCryptoFocused,
  cryptoInput,
  fiatInput,
  onCryptoInputChange,
  setIsCryptoFocused,
  onFiatInputChange,
  setShowSummary,
}) => {
  const {
    isCustomNumPadOpen,
    withdrawAddress,
    setWithdrawAddress,
    walletBalances,
  } = useContext(AppContext);

  const navigation = useNavigation();

  const [addressFocused, setAddressFocused] = useState(false);

  const openQRScanner = () => {
    navigation.navigate('QRScanner');
  };

  const pasteAddressHandler = async () => {
    const address = await Clipboard.getString();

    setWithdrawAddress(address);
  };

  const continueHandler = () => {
    if (!fiatInput || !cryptoInput) {
      return WToast.show({data: 'Enter amount to withdraw'});
    }

    if (isNaN(parseFloat(fiatInput))) {
      return WToast.show({data: 'Enter amount to withdraw'});
    }

    if (parseFloat(fiatInput) <= 0) {
      return WToast.show({data: 'Enter amount to withdraw'});
    }

    if (!withdrawAddress) {
      return WToast.show({data: 'Enter the address to withdraw'});
    }

    return setShowSummary();
  };

  const onHalfClicked = () => {
    if (walletBalances) {
      const balance =
        walletBalances[`${activeWallet.coinSymbol.toLowerCase()}_balance`] || 0;

      const halfBalance = balance / 2;
      onCryptoInputChange(halfBalance);
    }
  };

  const onFullClicked = () => {
    if (walletBalances) {
      const balance =
        walletBalances[`${activeWallet.coinSymbol.toLowerCase()}_balance`] || 0;

      onCryptoInputChange(balance);
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>
        {activeWallet.coinName.length < 10
          ? `Withdraw ${activeWallet.coinName}`
          : `Withdraw ${activeWallet.coinSymbol}`}
      </Text>
      <View style={styles.withdrawContainer}>
        {!addressFocused && (
          <View style={styles.inputForm}>
            <View style={styles.inputContainer}>
              {/* <CustomNumField
                style={[styles.input]}
                placeholder="0.00"
                value={cryptoVolumeFormatter.format(
                  parseFloat(cryptoInput || 0),
                )}
                focused={isCryptoFocused}
                onChange={onCryptoInputChange}
                onFocus={() => setIsCryptoFocused(true)}
              /> */}
              <TextInput
                style={[styles.input, isCryptoFocused && styles.focusedText]}
                placeholder={formatterHelper('0.00', activeWallet.coinSymbol)}
                onChangeText={(text) => onCryptoInputChange(text)}
                keyboardType="numeric"
                returnKeyType="done"
                value={cryptoInput}
                onFocus={() => setIsCryptoFocused(true)}
                placeholderTextColor="#878788"
              />
              <TouchableOpacity
                onPress={onHalfClicked}
                style={styles.inputButton}>
                <Text style={styles.inputButtonText}>HALF</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={onFullClicked}
                style={styles.inputButton}>
                <Text style={styles.inputButtonText}>ALL</Text>
              </TouchableOpacity>
              <Text
                style={[
                  styles.cryptoName,
                  isCryptoFocused && styles.focusedText,
                ]}>
                {activeWallet.coinSymbol}
              </Text>
            </View>
            <View style={styles.divider} />
            <View style={styles.inputContainer}>
              {/* <CustomNumField
                style={[styles.input]}
                placeholder="0.00"
                value={usdValueFormatterWithoutSign.format(
                  parseFloat(fiatInput || 0),
                )}
                focused={!isCryptoFocused}
                onChange={onFiatInputChange}
                onFocus={() => setIsCryptoFocused(false)}
              /> */}
              <TextInput
                style={[styles.input, isCryptoFocused || styles.focusedText]}
                placeholder={formatterHelper('0.00', 'USD')}
                onChangeText={(text) => onFiatInputChange(text)}
                keyboardType="numeric"
                returnKeyType="done"
                value={fiatInput}
                onFocus={() => setIsCryptoFocused(false)}
                placeholderTextColor="#878788"
              />
              <Text
                style={[
                  styles.cryptoName,
                  isCryptoFocused || styles.focusedText,
                ]}>
                USD
              </Text>
            </View>
          </View>
        )}
        {!isCustomNumPadOpen && (
          <View style={styles.addressTextContainer}>
            <View style={styles.textContainer}>
              <TextInput
                style={styles.addressInput}
                placeholder={`Enter ${activeWallet.coinSymbol} Address`}
                value={withdrawAddress}
                onChangeText={(text) => setWithdrawAddress(text)}
                placeholderTextColor="#878788"
                onFocus={() => setAddressFocused(true)}
                onBlur={() => setAddressFocused(false)}
              />
            </View>
            <TouchableOpacity
              style={styles.qrContainer}
              onPress={pasteAddressHandler}>
              <Image
                style={styles.qrIcon}
                source={require('../../../assets/address-copy-icon.png')}
                resizeMode="contain"
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.qrContainer}
              onPress={openQRScanner}>
              <Image
                style={styles.qrIcon}
                source={require('../../../assets/qr-code-icon.png')}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
        )}
        {!isKeyBoardOpen && (
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              style={styles.sendButton}
              onPress={continueHandler}>
              <Text style={styles.buttonText}>Continue</Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    </View>
  );
};

export default WithdrawForm;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
    paddingHorizontal: 40,
  },
  header: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    marginBottom: 15,
  },
  withdrawContainer: {justifyContent: 'space-between', flex: 1},
  inputForm: {marginTop: 20},
  inputContainer: {flexDirection: 'row', alignItems: 'center'},
  input: {
    flexGrow: 1,
    width: 0,
    color: '#041939',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    paddingHorizontal: 10,
  },
  focusedInput: {
    color: '#08152D',
  },
  inputButton: {
    borderWidth: 1,
    borderColor: '#EDEDED',
    justifyContent: 'center',
    paddingHorizontal: 8,
    paddingVertical: 2,
    marginLeft: 5,
  },
  inputButtonText: {
    color: '#B4BBC4',
    fontFamily: 'Montserrat',
    fontSize: 10,
  },
  cryptoName: {
    marginLeft: 10,
    color: '#041939',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  focusedText: {
    color: '#08152D',
  },
  divider: {
    backgroundColor: '#08152D',
    height: 1,
    marginVertical: 5,
  },
  addressTextContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textContainer: {
    borderColor: '#EDEDED',
    borderWidth: 2,
    borderRadius: 6,
    flexGrow: 1,
    width: 0,
    justifyContent: 'center',
    height: 40,
  },
  addressInput: {
    fontFamily: 'Montserrat',
    paddingHorizontal: 15,
    color: 'black',
  },
  qrContainer: {
    borderColor: '#EDEDED',
    borderWidth: 2,
    borderRadius: 6,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    width: 40,
    marginLeft: 5,
  },
  qrIcon: {width: 20, height: 24},
  sendButton: {
    backgroundColor: '#08152D',
    flexDirection: 'row',
    borderRadius: 6,
    paddingHorizontal: 25,
    paddingVertical: 10,
    alignItems: 'center',
  },
  buttonContainer: {alignItems: 'center', marginBottom: 30},
  buttonText: {
    color: 'white',
    fontFamily: 'Montserrat-Bold',
  },
});
