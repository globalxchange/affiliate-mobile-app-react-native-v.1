/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import * as WebBrowser from 'expo-web-browser';
import {AppContext} from '../../../contexts/AppContextProvider';
import {formatterHelper} from '../../../utils';
import Axios from 'axios';
import {GX_API_ENDPOINT, APP_CODE} from '../../../configs';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import LoadingAnimation from '../../LoadingAnimation';

const WithdrawComplete = ({amount, coin, onClose, openTxnAudit}) => {
  const navigation = useNavigation();
  const [isLoading, setIsLoading] = useState(false);

  const {walletBalances, walletCoinData} = useContext(AppContext);

  useEffect(() => {
    return () => {
      onClose();
    };
  }, []);

  const trackClick = async () => {
    setIsLoading(true);

    const profileId = await AsyncStorageHelper.getProfileId();

    const postData = {
      app_code: APP_CODE,
      profile_id: profileId,
      coin: coin,
    };

    Axios.post(`${GX_API_ENDPOINT}/coin/vault/service/txns/get`, postData)
      .then((resp) => {
        const {data} = resp;
        if (data.status) {
          openTxnAudit(data.txns[0]);
        }
        setIsLoading(false);
        // console.log('TXN List', data.txns);
      })
      .catch((error) => console.log('Error getting txn list', error));
  };

  if (isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <LoadingAnimation />
      </View>
    );
  }

  let coinData = '';

  if (walletCoinData) {
    coinData = walletCoinData?.find((x) => x.coinSymbol === coin);
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Withdrawal Initiated</Text>
      <Text style={styles.message}>
        Your Have Withdrew {formatterHelper(amount, coin)} {coin}
      </Text>
      <View style={styles.mainContainer}>
        <View style={styles.confirmation}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image
              style={styles.icon}
              source={{uri: coinData?.coinImage}}
              resizeMode="contain"
            />
            <Text style={styles.walletName}>{coin} Wallet Balance</Text>
          </View>
          <Text style={styles.walletName}>
            {coin &&
              formatterHelper(
                walletBalances[`${coin.toLowerCase()}_balance`],
                coin,
              )}
          </Text>
        </View>
        <View style={styles.actionContainer}>
          <ScrollView
            style={styles.scrollView}
            horizontal
            showsHorizontalScrollIndicator={false}>
            <TouchableOpacity
              style={[styles.action, {marginLeft: 10}]}
              onPress={() => navigation.navigate('Earn')}>
              <Image
                style={styles.actionIcon}
                source={require('../../../assets/app-logo.png')}
                resizeMode="contain"
              />
              <Text style={styles.actionText}>Back To Feed</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.action, {marginLeft: 25}]}
              onPress={() =>
                WebBrowser.openBrowserAsync('https://iceprotocol.com/')
              }>
              <Image
                style={styles.actionIcon}
                source={require('../../../assets/iced-icon.png')}
                resizeMode="contain"
              />
              <Text style={[styles.actionText, styles.actionTextBlack]}>
                Invest Bitcoin
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.action, {marginLeft: 25, marginRight: 10}]}
              onPress={() =>
                WebBrowser.openBrowserAsync('https://cryptolottery.com/')
              }>
              <Image
                style={styles.actionIcon}
                source={require('../../../assets/lottery-icon.png')}
                resizeMode="contain"
              />
              <Text style={[styles.actionText, styles.actionTextBlack]}>
                Play Lottery
              </Text>
            </TouchableOpacity>
          </ScrollView>
        </View>
        <TouchableOpacity style={styles.trackButton} onPress={trackClick}>
          <Text style={styles.trackButtonText}>Track Transaction</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default WithdrawComplete;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    marginBottom: 15,
  },
  mainContainer: {
    flex: 1,
    paddingHorizontal: 40,
    paddingVertical: 40,
    justifyContent: 'space-between',
  },
  message: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat',
  },
  confirmation: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 8,
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  icon: {
    width: 24,
    height: 24,
    marginRight: 15,
  },
  walletName: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
  },
  actionContainer: {
    flexDirection: 'row',
  },
  scrollView: {
    marginHorizontal: -10,
  },
  action: {
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    backgroundColor: 'white',
    borderRadius: 8,
    paddingVertical: 12,
    paddingHorizontal: 20,
    flex: 1,
    marginVertical: 10,
  },
  actionIcon: {
    width: 35,
    height: 35,
  },
  actionText: {
    marginTop: 10,
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    textAlign: 'center',
    fontSize: 12,
  },
  actionTextBlack: {
    color: '#464B4E',
  },
  trackButton: {
    backgroundColor: '#08152D',
    alignItems: 'center',
    paddingVertical: 15,
    marginHorizontal: -40,
    marginBottom: -40,
  },
  trackButtonText: {
    fontFamily: 'Montserrat-Bold',
    color: 'white',
  },
  linkText: {
    fontFamily: 'Montserrat',
    color: '#08152D',
    textAlign: 'center',
    textDecorationLine: 'underline',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
});
