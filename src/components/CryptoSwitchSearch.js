import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {AppContext} from '../contexts/AppContextProvider';
import SearchLayout from '../layouts/SearchLayout';
import {formatterHelper} from '../utils';

const CryptoSwitchSearch = ({setActiveCrypto, onClose}) => {
  const {walletCoinData} = useContext(AppContext);

  const [searchInput, setSearchInput] = useState('');
  const [coinList, setCoinList] = useState();

  useEffect(() => {
    if (walletCoinData) {
      const list = walletCoinData.map((item) => ({
        ...item,
        email: `${formatterHelper(item.coinValue, item.coinSymbol)} ${
          item.coinSymbol
        }`,
      }));
      setCoinList(list);
    }
  }, [walletCoinData, searchInput]);

  const onItemSelected = (item) => {
    setActiveCrypto(item);
    onClose();
  };

  return (
    <View style={styles.container}>
      <SearchLayout
        value={searchInput}
        setValue={setSearchInput}
        list={coinList || []}
        onBack={onClose}
        disableFilter
        onSubmit={(_, item) => onItemSelected(item)}
        showUserList
        placeholder=""
      />
    </View>
  );
};

export default CryptoSwitchSearch;

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFill,
    zIndex: 100,
    backgroundColor: 'white',
  },
});
