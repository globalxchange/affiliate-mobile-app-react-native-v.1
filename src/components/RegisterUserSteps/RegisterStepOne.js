import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import React from 'react';
import ThemeData from '../../configs/ThemeData';

const RegisterStepOne = ({setStep}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Step 1</Text>
      <Text style={styles.subTitle}>How Did You Hear About Us?</Text>
      <ScrollView style={styles.scrollView}>
        <TouchableOpacity
          style={styles.card}
          onPress={() => {
            setStep('affiliateTwo');
          }}>
          <Image
            source={require('../../assets/icons/affiliate.png')}
            style={styles.cardIcon}
          />
          <View style={styles.texts}>
            <Text style={styles.cardTitle}>Affiliate</Text>
            <Text style={styles.cardSubTitle}>
              An Existing Affiliate App User Told You
            </Text>
          </View>
        </TouchableOpacity>
        <View style={[styles.card, styles.dis]}>
          <Image
            source={require('../../assets/icons/other.png')}
            style={styles.cardIcon}
          />
          <View style={styles.texts}>
            <Text style={styles.cardTitle}>Other</Text>
            <Text style={styles.cardSubTitle}>You Found Us By Yourself</Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default RegisterStepOne;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
    position: 'relative',
  },
  dis: {
    opacity: 0.5,
  },
  title: {
    fontSize: 40,
    color: ThemeData.TEXT_COLOR,
    fontFamily: ThemeData.FONT_BOLD,
    marginTop: 100,
  },
  subTitle: {
    fontSize: 18,
    color: ThemeData.TEXT_COLOR,
    marginBottom: '20%',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  scrollView: {},
  card: {
    height: 100,
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
    marginVertical: 10,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  cardIcon: {
    height: 60,
    width: 60,
    resizeMode: 'contain',
    marginRight: 10,
  },
  cardTitle: {
    fontSize: 20,
    color: ThemeData.TEXT_COLOR,
    fontFamily: ThemeData.FONT_BOLD,
  },
  cardSubTitle: {
    fontSize: 12,
    color: ThemeData.TEXT_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
  },
});
