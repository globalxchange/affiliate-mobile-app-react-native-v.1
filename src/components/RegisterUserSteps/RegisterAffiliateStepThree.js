import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import React, {useCallback} from 'react';
import ThemeData from '../../configs/ThemeData';
import {useUserDetails} from '../../utils/CustomHook';
import SkeltonItem from '../SkeltonItem';

const RegisterAffiliateStepThree = ({setStep, affiliate}) => {
  const {data, isLoading} = useUserDetails(affiliate);
  console.log(
    'data?.dynamic?.[0]?.data?.profile_img',
    data?.dynamic?.[0]?.data?.profile_img,
  );
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.container}>
      <Text style={styles.title}>Step 3</Text>
      <Text style={styles.subTitle}>Confirm Affiliate</Text>
      {isLoading ? (
        <View style={styles.card}>
          <SkeltonItem style={styles.cardIcon} itemHeight={60} itemWidth={60} />
          <View style={styles.texts}>
            <SkeltonItem
              style={[styles.cardTitle, {marginBottom: 2}]}
              itemHeight={20}
              itemWidth={200}
            />
            <SkeltonItem
              style={styles.cardSubTitle}
              itemHeight={12}
              itemWidth={160}
            />
          </View>
        </View>
      ) : (
        <View style={styles.card}>
          <Image
            source={{uri: data?.dynamic?.[0]?.data?.profile_img}}
            style={styles.cardIcon}
          />
          <View style={styles.texts}>
            <Text style={styles.cardTitle}>
              {data?.hardCoded?.[0]?.data?.username}
            </Text>
            <Text style={styles.cardSubTitle}>{data?.email}</Text>
          </View>
        </View>
      )}
      <View style={styles.btns}>
        <TouchableOpacity
          style={styles.btnBack}
          onPress={() => setStep('affiliateTwo')}>
          <Text style={styles.btnTextBack}>Back</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.btnNext}
          onPress={() => setStep('affiliateFour')}>
          <Text style={styles.btnText}>Next</Text>
        </TouchableOpacity>
      </View>
    </KeyboardAvoidingView>
  );
};

export default RegisterAffiliateStepThree;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
    position: 'relative',
  },
  dis: {
    opacity: 0.5,
  },
  title: {
    fontSize: 40,
    color: ThemeData.TEXT_COLOR,
    fontFamily: ThemeData.FONT_BOLD,
    marginTop: 100,
  },
  subTitle: {
    fontSize: 18,
    color: ThemeData.TEXT_COLOR,
    marginBottom: '10%',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  card: {
    height: 100,
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
    marginVertical: 10,
    borderRadius: 10,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  cardIcon: {
    height: 60,
    width: 60,
    resizeMode: 'contain',
    marginRight: 10,
  },
  cardTitle: {
    fontSize: 20,
    color: ThemeData.TEXT_COLOR,
    fontFamily: ThemeData.FONT_BOLD,
  },
  cardSubTitle: {
    fontSize: 12,
    color: ThemeData.TEXT_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
  },
  btns: {
    height: 60,
    flexDirection: 'row',
    marginTop: 'auto',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  btnBack: {
    flexBasis: '48%',
    height: 60,
    borderRadius: 10,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
  },
  btnTextBack: {
    fontSize: 20,
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_BOLD,
  },
  btnNext: {
    flexBasis: '48%',
    height: 60,
    borderRadius: 10,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    fontSize: 20,
    color: 'white',
    fontFamily: ThemeData.FONT_BOLD,
  },
});
