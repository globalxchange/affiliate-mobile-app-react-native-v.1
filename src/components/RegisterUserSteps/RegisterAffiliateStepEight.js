import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import React from 'react';
import ThemeData from '../../configs/ThemeData';
import OTPInputView from '@twotalltotems/react-native-otp-input';

const RegisterAffiliateStepEight = ({
  setStep,
  otp,
  setOtp,
  isValid,
  validate,
}) => {
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.container}>
      <Text style={styles.title}>Step 8</Text>
      <Text style={styles.subTitle}>Confirm Email</Text>
      <OTPInputView
        codeInputFieldStyle={styles.underlineStyleBase}
        codeInputHighlightStyle={styles.underlineStyleHighLighted}
        style={styles.otpInput}
        pinCount={6}
        autoFocusOnLoad
        code={otp}
        onCodeChanged={(code) => setOtp(code)}
      />
      <TouchableOpacity style={styles.passCheckBtn}>
        <Text style={styles.passCheckText}>Resend Code</Text>
      </TouchableOpacity>
      <View style={styles.btns}>
        <TouchableOpacity
          style={styles.btnBack}
          onPress={() => setStep('affiliateSix')}>
          <Text style={styles.btnTextBack}>Back</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.btnNext}
          onPress={() => {
            if (isValid) validate();
          }}>
          <Text style={styles.btnText}>Sign Up</Text>
        </TouchableOpacity>
      </View>
    </KeyboardAvoidingView>
  );
};

export default RegisterAffiliateStepEight;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
    position: 'relative',
  },
  dis: {
    opacity: 0.5,
  },
  title: {
    fontSize: 40,
    color: ThemeData.TEXT_COLOR,
    fontFamily: ThemeData.FONT_BOLD,
    marginTop: 100,
  },
  subTitle: {
    fontSize: 18,
    color: ThemeData.TEXT_COLOR,
    marginBottom: '10%',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  otpInput: {
    height: 80,
    color: ThemeData.TEXT_COLOR,
    fontFamily: ThemeData.FONT_BOLD,
  },
  underlineStyleBase: {
    borderWidth: 1,
    color: '#08152D',
  },
  underlineStyleHighLighted: {
    borderColor: '#999C9A',
    color: '#08152D',
  },
  passCheckBtn: {
    marginVertical: 15,
  },
  passCheckText: {
    fontSize: 16,
    textDecorationStyle: 'solid',
    textDecorationLine: 'underline',
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.TEXT_COLOR,
  },
  btns: {
    height: 60,
    flexDirection: 'row',
    marginTop: 'auto',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  btnBack: {
    flexBasis: '48%',
    height: 60,
    borderRadius: 10,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
  },
  btnTextBack: {
    fontSize: 20,
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_BOLD,
  },
  btnNext: {
    flexBasis: '48%',
    height: 60,
    borderRadius: 10,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    fontSize: 20,
    color: 'white',
    fontFamily: ThemeData.FONT_BOLD,
  },
});
