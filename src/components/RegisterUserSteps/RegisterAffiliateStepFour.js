import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import React from 'react';
import ThemeData from '../../configs/ThemeData';
import Svg, {Circle} from 'react-native-svg';

const RegisterAffiliateStepFour = ({
  setStep,
  username,
  setUsername,
  isValid,
}) => {
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.container}>
      <Text style={styles.title}>Step 4</Text>
      <Text style={styles.subTitle}>Create Username</Text>
      <View style={styles.searchInputWrap}>
        <TextInput
          style={styles.searchInput}
          placeholder="Enter Username..."
          returnKeyType="next"
          placeholderTextColor="#CACACA"
          value={username}
          onChangeText={setUsername}
          onSubmitEditing={() => {
            if (isValid) setStep('affiliateFive');
          }}
          autoCapitalize="none"
        />
        <View style={styles.btnAction}>
          <Svg
            style={styles.backImg}
            viewBox="0 0 12 12"
            fill="none"
            xmlns="http://www.w3.org/2000/svg">
            <Circle
              cx="6"
              cy="6"
              r="6"
              fill={isValid ? '#5ABEA0' : '#D80027'}
            />
          </Svg>
        </View>
      </View>
      <View style={styles.btns}>
        <TouchableOpacity
          style={styles.btnBack}
          onPress={() => setStep('affiliateThree')}>
          <Text style={styles.btnTextBack}>Back</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.btnNext}
          onPress={() => {
            if (isValid) setStep('affiliateFive');
          }}>
          <Text style={styles.btnText}>Next</Text>
        </TouchableOpacity>
      </View>
    </KeyboardAvoidingView>
  );
};

export default RegisterAffiliateStepFour;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
    position: 'relative',
  },
  dis: {
    opacity: 0.5,
  },
  title: {
    fontSize: 40,
    color: ThemeData.TEXT_COLOR,
    fontFamily: ThemeData.FONT_BOLD,
    marginTop: 100,
  },
  subTitle: {
    fontSize: 18,
    color: ThemeData.TEXT_COLOR,
    marginBottom: '10%',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  searchInputWrap: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    marginTop: 30,
    borderRadius: 10,
    flexDirection: 'row',
  },
  searchInput: {
    flex: 1,
    paddingHorizontal: 20,
    height: 70,
    color: ThemeData.TEXT_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
  },
  btnAction: {
    width: 60,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backImg: {
    height: 15,
    width: 15,
    resizeMode: 'contain',
  },
  btns: {
    height: 60,
    flexDirection: 'row',
    marginTop: 'auto',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  btnBack: {
    flexBasis: '48%',
    height: 60,
    borderRadius: 10,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
  },
  btnTextBack: {
    fontSize: 20,
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_BOLD,
  },
  btnNext: {
    flexBasis: '48%',
    height: 60,
    borderRadius: 10,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    fontSize: 20,
    color: 'white',
    fontFamily: ThemeData.FONT_BOLD,
  },
});
