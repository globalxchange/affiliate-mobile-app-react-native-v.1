import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import React, {useCallback} from 'react';
import ThemeData from '../../configs/ThemeData';
import {WToast} from 'react-native-smart-tip';

const RegisterAffiliateStepTwo = ({
  setStep,
  affiliate,
  setAffiliate,
  isValid,
}) => {
  const onPasteClick = useCallback(async () => {
    const pastedString = await Clipboard.getString();
    setAffiliate(pastedString);
  }, [setAffiliate]);

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.container}>
      <Text style={styles.title}>Step 2</Text>
      <Text style={styles.subTitle}>Who Referred You?</Text>
      <View style={styles.searchInputWrap}>
        <TextInput
          style={styles.searchInput}
          placeholder="Enter AffiliateSync Code.."
          returnKeyType="next"
          placeholderTextColor="#CACACA"
          value={affiliate}
          onChangeText={(text) => setAffiliate(text)}
          onSubmitEditing={() => {
            if (affiliate.length > 3) setStep('affiliateThree');
          }}
          autoCapitalize="none"
        />
        <TouchableOpacity style={styles.btnAction} onPress={onPasteClick}>
          <Image
            source={require('../../assets/paste-icon-blue.png')}
            style={styles.backImg}
          />
        </TouchableOpacity>
      </View>
      <View style={styles.note}>
        <Text style={styles.noteLabel}>NOTE</Text>
        <Text style={styles.noteText}>
          {' '}
          AffiliateSync Codes Are Case Sensitive.{' '}
        </Text>
      </View>
      <TouchableOpacity
        style={styles.btnNext}
        onPress={() => {
          if (isValid) setStep('affiliateThree');
          else
            WToast.show({
              data: 'Invalid Affiliate',
              position: WToast.position.TOP,
            });
        }}>
        <Text style={styles.btnText}>Next</Text>
      </TouchableOpacity>
    </KeyboardAvoidingView>
  );
};

export default RegisterAffiliateStepTwo;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
    position: 'relative',
  },
  dis: {
    opacity: 0.5,
  },
  title: {
    fontSize: 40,
    color: ThemeData.TEXT_COLOR,
    fontFamily: ThemeData.FONT_BOLD,
    marginTop: 100,
  },
  subTitle: {
    fontSize: 18,
    color: ThemeData.TEXT_COLOR,
    marginBottom: '10%',
    fontFamily: ThemeData.FONT_NORMAL,
  },
  searchInputWrap: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    marginTop: 30,
    borderRadius: 10,
    flexDirection: 'row',
  },
  searchInput: {
    flex: 1,
    paddingHorizontal: 20,
    height: 70,
    color: ThemeData.TEXT_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
  },
  btnAction: {
    width: 70,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backImg: {
    height: 25,
    width: 25,
    resizeMode: 'contain',
  },
  note: {
    flexDirection: 'row',
    marginVertical: '10%',
  },
  noteLabel: {
    color: ThemeData.TEXT_COLOR,
    fontFamily: ThemeData.FONT_BOLD,
  },
  noteText: {
    color: ThemeData.TEXT_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
  },
  btnNext: {
    height: 60,
    borderRadius: 10,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 'auto',
    marginBottom: 20,
  },
  btnText: {
    fontSize: 20,
    color: 'white',
    fontFamily: ThemeData.FONT_BOLD,
  },
});
