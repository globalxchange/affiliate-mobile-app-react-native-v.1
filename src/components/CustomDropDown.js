/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useRef, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  Image,
  FlatList,
  TextInput,
  Keyboard,
} from 'react-native';
import {urlValidatorRegex} from '../utils';
import FastImage from 'react-native-fast-image';
import PopupLayout from '../layouts/PopupLayout';
import ThemeData from '../configs/ThemeData';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

const {width, height} = Dimensions.get('window');

const CustomDropDown = ({
  selectedItem,
  items,
  onDropDownSelect,
  label,
  placeHolder,
  placeholderIcon,
  numberLabel,
  subTextKey,
  onNext,
  headerIcon,
  searchPlaceHolder,
  headerIconHeight,
  headerTitle,
}) => {
  const {top, bottom} = useSafeAreaInsets();

  const [isDropDownOpen, setIsDropDownOpen] = useState(false);
  const [isSearchFocused, setIsSearchFocused] = useState(false);
  const [searchInput, setSearchInput] = useState('');
  const [filteredList, setFilteredList] = useState('');

  const inputRef = useRef();

  useEffect(() => {
    if (items) {
      const searchQuery = searchInput.toLowerCase().trim();

      const list = items.filter((x) =>
        x?.name?.toLowerCase().includes(searchQuery),
      );

      setFilteredList(list);
    }
  }, [items, searchInput]);

  useEffect(() => {
    if (isDropDownOpen) {
      setSearchInput('');
      setIsSearchFocused(false);
    }
  }, [isDropDownOpen]);

  const onItemSelected = (item) => {
    onDropDownSelect(item);
    setIsDropDownOpen(false);
    if (onNext) {
      onNext();
    }
  };

  if (items && items.length === 1) {
    onDropDownSelect(items[0]);
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>{label}</Text>
      <TouchableOpacity onPress={() => setIsDropDownOpen(!isDropDownOpen)}>
        <View style={styles.dropdownContainer}>
          <FastImage
            style={styles.image}
            source={
              selectedItem
                ? urlValidatorRegex.test(selectedItem.image)
                  ? {uri: selectedItem.image}
                  : selectedItem.image
                : placeholderIcon || require('../assets/country-icon.png')
            }
            resizeMode="contain"
          />
          <Text style={styles.text}>
            {selectedItem
              ? selectedItem.name
              : items && numberLabel
              ? `${items.length} Available ${numberLabel}`
              : placeHolder || 'Select An Option'}
          </Text>
          <Image
            style={styles.dropIcon}
            source={require('../assets/dropdown-icon.png')}
            resizeMode="contain"
          />
        </View>
      </TouchableOpacity>
      <PopupLayout
        noScrollView
        headerImageHeight={headerIconHeight}
        headerImage={headerIcon}
        headerTitle={headerTitle}
        isOpen={isDropDownOpen}
        onClose={() => setIsDropDownOpen(false)}
        containerStyles={
          isSearchFocused && {
            height,
            width,
            paddingTop: top,
            paddingBottom: bottom,
            backgroundColor: 'transparent',
            borderWidth: 0,
          }
        }>
        <View style={styles.modalContainer}>
          <View style={styles.searchContainer}>
            {isSearchFocused && (
              <>
                <TouchableOpacity
                  style={styles.backButton}
                  onPress={() => {
                    Keyboard.dismiss();
                    inputRef.current.blur();
                    setIsSearchFocused(false);
                  }}>
                  <Image
                    style={styles.backIcon}
                    source={require('../assets/back-icon-colored.png')}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
                <View style={styles.divider} />
              </>
            )}
            <TextInput
              ref={inputRef}
              style={styles.searchInput}
              placeholderTextColor={'#9A9A9A'}
              placeholder={searchPlaceHolder || `Search ${numberLabel || ''}`}
              value={searchInput}
              onChangeText={(text) => setSearchInput(text)}
              onFocus={() => setIsSearchFocused(true)}
              onBlur={() => setIsSearchFocused(false)}
            />
          </View>
          <FlatList
            showsVerticalScrollIndicator={false}
            style={styles.dropDownList}
            data={filteredList}
            keyExtractor={(item) => item.name}
            renderItem={({item}) => (
              <TouchableOpacity
                disabled={item.disabled}
                onPress={() => onItemSelected(item)}
                style={[styles.dropDownItem, item.disabled && {opacity: 0.5}]}>
                <FastImage
                  style={styles.countryImage}
                  source={
                    urlValidatorRegex.test(item.image)
                      ? {uri: item.image}
                      : item.image
                  }
                  resizeMode="contain"
                />
                <Text style={styles.itemText}>{item.name}</Text>
                {subTextKey && (
                  <Text style={styles.itemSubText}>{item[subTextKey]}</Text>
                )}
              </TouchableOpacity>
            )}
            ListEmptyComponent={
              <Text style={styles.emptyText}>No Option Found...</Text>
            }
          />
        </View>
      </PopupLayout>
    </View>
  );
};

export default CustomDropDown;

const styles = StyleSheet.create({
  container: {marginBottom: 20},
  header: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  dropdownContainer: {
    flexDirection: 'row',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    paddingHorizontal: 15,
    alignItems: 'center',
    height: 50,
  },
  image: {
    height: 24,
    width: 24,
  },
  text: {
    flexGrow: 1,
    width: 0,
    paddingHorizontal: 15,
    fontFamily: 'Montserrat',
    color: '#788995',
  },
  dropIcon: {
    width: 13,
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.2)',
    flex: 1,
    justifyContent: 'center',
    paddingVertical: height * 0.25,
  },
  modalContainer: {
    flex: 1,
    backgroundColor: 'white',
    marginHorizontal: -5,
  },
  dropDownList: {
    // paddingHorizontal: 30,
  },
  dropDownItem: {
    flexDirection: 'row',
    paddingVertical: 20,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  countryImage: {height: 24, width: 24},
  itemText: {
    flex: 1,
    marginLeft: 15,
    fontFamily: 'Montserrat',
    color: '#9A9A9A',
  },
  itemSubText: {
    fontFamily: 'Roboto',
    color: '#788995',
    fontSize: 12,
  },
  emptyText: {
    fontFamily: 'Montserrat-SemiBold',
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    paddingVertical: 20,
    fontSize: 18,
  },
  searchContainer: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 15,
  },
  searchInput: {
    height: 40,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    color: '#788995',
    flex: 1,
  },
  backButton: {
    height: 40,
    justifyContent: 'center',
    marginLeft: -15,
    paddingLeft: 15,
    paddingRight: 15,
  },
  backIcon: {
    width: 20,
    height: 20,
  },
  divider: {
    marginRight: 15,
    height: '100%',
    width: 1,
    backgroundColor: ThemeData.BORDER_COLOR,
  },
});
