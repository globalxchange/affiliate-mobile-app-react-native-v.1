import {
  StyleSheet,
  Image,
  TouchableOpacity,
  View,
  TextInput,
} from 'react-native';
import React from 'react';
import ThemeData from '../configs/ThemeData';
import {useCallback} from 'react';
import Clipboard from '@react-native-community/clipboard';

const SearchHeader = ({search, setSearch, onBack}) => {
  const onPasteClick = useCallback(async () => {
    const pastedString = await Clipboard.getString();
    setSearch(pastedString);
  }, [setSearch]);

  return (
    <View style={styles.searchBar}>
      <TouchableOpacity style={styles.btnBack} onPress={onBack}>
        <Image
          source={require('../assets/back-short.png')}
          style={styles.backImg}
        />
      </TouchableOpacity>
      <TextInput
        style={styles.searchInput}
        value={search}
        onChangeText={setSearch}
      />
      <TouchableOpacity style={styles.btnAction} onPress={onPasteClick}>
        <Image
          source={require('../assets/paste-icon-blue.png')}
          style={styles.backImg}
        />
      </TouchableOpacity>
      <TouchableOpacity style={styles.btnAction} onPress={() => {}}>
        <Image
          source={require('../assets/search-modern.png')}
          style={styles.backImg}
        />
      </TouchableOpacity>
    </View>
  );
};

export default SearchHeader;

const styles = StyleSheet.create({
  searchBar: {
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    height: 60,
    flexDirection: 'row',
  },
  btnBack: {
    width: 60,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backImg: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
  btnAction: {
    width: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderLeftColor: ThemeData.BORDER_COLOR,
    borderLeftWidth: 1,
  },
  searchInput: {
    height: 60,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 16,
    color: ThemeData.TEXT_COLOR,
    flex: 1,
  },
});
