import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import Animated, {
  Easing,
  interpolate,
  set,
  useCode,
} from 'react-native-reanimated';
import {loop, useValue} from 'react-native-redash';
import ThemeData from '../configs/ThemeData';

const LongLoading = ({time = 1, loadingItemName = ''}) => {
  const animatedValue = useValue(0);

  useCode(
    () => [
      set(
        animatedValue,
        loop({
          duration: 1000,
          easing: Easing.linear,
          autoStart: true,
          boomerang: true,
        }),
      ),
    ],
    [],
  );

  const scale = interpolate(animatedValue, {
    inputRange: [0, 1],
    outputRange: [0.5, 1],
  });

  return (
    <View style={styles.container}>
      <Animated.Image
        style={[styles.appLogo, {transform: [{scale}]}]}
        resizeMode="contain"
        source={require('../assets/app-logo.png')}
      />
      <Text style={styles.header}>Loading {loadingItemName}</Text>
      <Text style={styles.subHeader}>This May Take A Couple Minutes</Text>
    </View>
  );
};

export default LongLoading;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  appLogo: {
    width: 80,
    height: 80,
    marginBottom: 20,
  },
  header: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 18,
    marginBottom: 10,
  },
  subHeader: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
  },
});
