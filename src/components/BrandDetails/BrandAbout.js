import React, {useState} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import PopupLayout from '../../layouts/PopupLayout';
import BrandAddress from './BrandAddress';

const BrandAbout = ({selectedItem, color, navigateToLicense}) => {
  const [selectedOption, setSelectedOption] = useState();
  const [isPopupOpen, setIsPopupOpen] = useState(false);

  // console.log('selectedItem', selectedItem);

  if (selectedOption) {
    if (selectedOption === 'Company') {
      return (
        <BrandAddress
          title="About The Company"
          subtitle={`Learn About ${selectedItem.bankerTag}`}
          color={color}
          selectedItem={selectedItem}
        />
      );
    }
    if (selectedOption === 'Community') {
      return (
        <BrandAddress
          title="Join The Community"
          subtitle={`Get Plugged In With ${selectedItem.bankerTag}`}
          color={color}
          selectedItem={selectedItem}
        />
      );
    }
  }

  const ACTIONS = [
    {title: 'Company', icon: require('../../assets/money-icon.png')},
    {
      title: 'Licenses',
      icon: {
        uri: 'https://i.ibb.co/ggYnSwr/world.png',
      },
      onPress: navigateToLicense,
    },
  ];

  return (
    <View style={styles.container}>
      <ScrollView bounces={false} showsVerticalScrollIndicator={false}>
        <Text onPress={() => setIsPopupOpen(true)} style={styles.description}>
          {selectedItem?.description?.substring(0, 125)}...{' '}
          <Text style={{fontFamily: ThemeData.FONT_SEMI_BOLD}}>
            Read Full Bio
          </Text>
        </Text>
        <PopupLayout
          autoHeight
          isOpen={isPopupOpen}
          headerTitle="About The Brand"
          onClose={() => setIsPopupOpen(false)}>
          <Text style={styles.popupText}>{selectedItem?.description}</Text>
        </PopupLayout>
        {ACTIONS.map((item) => (
          <TouchableOpacity
            key={item.title}
            onPress={() =>
              item.onPress ? item.onPress() : setSelectedOption(item.title)
            }
            style={styles.action}>
            <Image
              source={item.icon}
              resizeMode="contain"
              style={styles.actionIcon}
            />
            <Text style={styles.actionText}>{item.title}</Text>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  );
};

export default BrandAbout;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 30,
    marginTop: 30,
  },
  description: {
    textAlign: 'center',
    color: '#292934',
    opacity: 0.5,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginBottom: 20,
    lineHeight: 20,
  },
  action: {
    flexDirection: 'row',
    borderColor: '#E7E7E7',
    borderWidth: 1,
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 15,
    marginTop: 15,
  },
  actionIcon: {
    width: 30,
    height: 30,
    marginRight: 15,
  },
  actionText: {
    color: '#292934',
    opacity: 0.5,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 15,
  },
  popupText: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 14,
    marginBottom: 20,
    lineHeight: 22,
  },
});
