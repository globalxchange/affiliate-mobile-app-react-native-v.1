/* eslint-disable react-native/no-inline-styles */
import {faChevronDown, faChevronUp} from '@fortawesome/free-solid-svg-icons';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import React, {useEffect} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';

const ControllerMenu = ({
  activeMenu,
  setActiveMenu,
  color,
  isFragmentExpanded,
  setIsFragmentExpanded,
}) => {
  useEffect(() => {
    setActiveMenu(MENUS[1]);
  }, []);

  return (
    <View style={styles.container}>
      {MENUS.map((item) => (
        <TouchableOpacity
          key={item}
          onPress={() => setActiveMenu(item)}
          style={[
            styles.itemContainer,
            activeMenu === item && {
              borderBottomColor: color,
            },
          ]}>
          <Text
            style={[
              styles.itemLabel,
              activeMenu === item && {
                color,
                borderBottomColor: color,
                fontSize: 14,
              },
            ]}>
            {item}
          </Text>
        </TouchableOpacity>
      ))}
      <TouchableOpacity
        style={[styles.expandToggle, {backgroundColor: color}]}
        onPress={() => setIsFragmentExpanded(!isFragmentExpanded)}>
        <FontAwesomeIcon
          icon={isFragmentExpanded ? faChevronDown : faChevronUp}
          color={'white'}
          size={20}
        />
      </TouchableOpacity>
    </View>
  );
};

export default ControllerMenu;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 10,
  },
  itemContainer: {
    zIndex: 3,
    flex: 1,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    borderTopColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    paddingVertical: 10,
  },
  itemLabel: {
    color: '#979797',
    fontSize: 12,
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
    marginTop: 5,
  },
  expandToggle: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    justifyContent: 'center',
    paddingHorizontal: 12,
    paddingVertical: 10,
    alignContent: 'center',
  },
});

const MENUS = ['Offers', 'Profile', 'Opportunity'];
