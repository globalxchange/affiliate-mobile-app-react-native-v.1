import axios from 'axios';
import React, {useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {GX_AUTH_URL} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import LoadingAnimation from '../LoadingAnimation';
import SuccessPage from './SuccessPage';

const DisableConfirmation = ({goBack}) => {
  const [isLoading, setIsLoading] = useState(false);
  const [showSuccess, setShowSuccess] = useState(false);
  const [showError, setShowError] = useState(false);

  const disable2FA = async () => {
    setIsLoading(true);
    const email = await AsyncStorageHelper.getLoginEmail();
    const accessToken = await AsyncStorageHelper.getAccessToken();

    axios
      .post(`${GX_AUTH_URL}/gx/user/mfa/disable`, {
        email,
        accessToken,
      })
      .then(({data}) => {
        // console.log('Disable data', data);
        if (data.status) {
          setShowSuccess(true);
        } else {
          setShowError(true);
        }
      })
      .catch((error) => {
        setShowError(false);
        console.log('Error on disabling 2fa', error);
      })
      .finally(() => setIsLoading(false));
  };

  if (showSuccess) {
    return (
      <SuccessPage message="You Have Successfully Disabled 2FA For This Account" />
    );
  }

  if (showError) {
    return (
      <SuccessPage
        isError
        message="We Are Unable To Disable Your 2FA At This TIme. Please Contact Support Or Try Again Later"
      />
    );
  }

  if (isLoading) {
    return (
      <View style={{flex: 1, justifyContent: 'center'}}>
        <LoadingAnimation />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.headerText}>Disable 2FA</Text>
      </View>
      <Text style={styles.subText}>
        Are You Sure You Want To Remove 2FA For This Account. This May Expose
        Your Accounts To Higher Levels Of Risk
      </Text>
      <View style={{height: 60}} />
      <View style={styles.actionContainer}>
        <TouchableOpacity style={styles.outlinedButton} onPress={goBack}>
          <Text style={styles.outlinedButtonText}>Go Back</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.filledButton} onPress={disable2FA}>
          <Text style={styles.filledButtonText}>Disable</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default DisableConfirmation;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 30,
    color: ThemeData.APP_MAIN_COLOR,
  },
  authenticatorIcon: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  actionContainer: {
    flexDirection: 'row',
    paddingHorizontal: 30,
  },
  outlinedButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  outlinedButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  filledButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    marginLeft: 15,
  },
  filledButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: 'white',
  },
  subText: {
    textAlign: 'center',
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 20,
  },
});
