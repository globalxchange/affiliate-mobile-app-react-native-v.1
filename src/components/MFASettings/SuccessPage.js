import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';

const SuccessPage = ({header, message, isError}) => {
  const {navigate, goBack} = useNavigation();

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.headerText}>
          {header || (isError ? 'Unsuccessful' : 'Success')}
        </Text>
      </View>
      <Text style={styles.subText}>
        {message || 'You Have Successfully Enabled 2FA For This Account'}
      </Text>
      <View style={{height: 60}} />
      <View style={styles.actionContainer}>
        <TouchableOpacity
          style={styles.outlinedButton}
          onPress={
            isError
              ? () =>
                  navigate('Support', {
                    openMessage: true,
                    messageData: '',
                  })
              : goBack
          }>
          <Text style={styles.outlinedButtonText}>
            {isError ? 'Support' : 'Settings'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.filledButton}
          onPress={() => navigate('Earn')}>
          <Text style={styles.filledButtonText}>Home</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default SuccessPage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 30,
    color: ThemeData.APP_MAIN_COLOR,
  },
  authenticatorIcon: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  actionContainer: {
    flexDirection: 'row',
    paddingHorizontal: 30,
  },
  outlinedButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  outlinedButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  filledButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    marginLeft: 15,
  },
  filledButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: 'white',
  },
  subText: {
    textAlign: 'center',
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 20,
  },
});
