import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import FastImage from 'react-native-fast-image';
import {getUriImage} from '../../utils';

const VerticalItem = ({item, onItemSelected, selectedItem}) => {
  return (
    <TouchableOpacity onPress={onItemSelected}>
      <View style={styles.item}>
        <FastImage
          style={styles.cryptoIcon}
          resizeMode="contain"
          source={
            item.image || {
              uri:
                getUriImage(item.profile_image) || 'https://i.pravatar.cc/110',
            }
          }
        />
        <View style={styles.nameContainer}>
          <Text style={styles.countryName}>{item.name || item.email}</Text>
        </View>
        <View style={styles.priceContainer}>
          <Text style={styles.methodsNo}>
            {`${item.txns || 0} Transactions`}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default VerticalItem;

const styles = StyleSheet.create({
  item: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 25,
    backgroundColor: 'white',
    marginBottom: 10,
    alignItems: 'center',
  },
  cryptoIcon: {
    width: 25,
    height: 25,
    borderRadius: 10,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  nameContainer: {
    flexGrow: 1,
    width: 0,
    marginLeft: 10,
  },
  countryName: {
    color: '#001D41',
    fontSize: 14,
    marginBottom: 2,
    fontFamily: 'Montserrat-Bold',
  },
  priceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  methodsNo: {
    color: '#001D41',
    opacity: 0.5,
    marginBottom: 2,
    textAlign: 'right',
    fontFamily: 'Roboto-Bold',
    marginRight: 10,
    fontSize: 12,
  },
});
