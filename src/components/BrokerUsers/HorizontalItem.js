import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Dimensions,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {getUriImage} from '../../utils';

const {width} = Dimensions.get('window');

const HorizontalItem = ({item, onItemSelected, selectedItem}) => {
  return (
    <TouchableWithoutFeedback onPress={onItemSelected}>
      <View
        style={[
          styles.item,
          selectedItem
            ? selectedItem.email === item.email
              ? styles.itemActive
              : {}
            : {},
        ]}>
        <FastImage
          style={styles.icon}
          source={
            item.image || {
              uri:
                getUriImage(item.profile_image) || 'https://i.pravatar.cc/110',
            }
          }
          resizeMode="contain"
        />
        <Text style={styles.title}>{item.name || item.email}</Text>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default HorizontalItem;

const styles = StyleSheet.create({
  item: {
    paddingHorizontal: 5,
    width: (width - 40) / 4,
    paddingVertical: 15,
    alignItems: 'center',
  },
  itemActive: {
    opacity: 0.4,
  },
  icon: {height: 30, width: 30, borderRadius: 15},
  title: {
    textAlign: 'center',
    color: '#788995',
    marginTop: 5,
    fontSize: 9,
    fontFamily: 'Montserrat',
  },
});
