import {CommonActions, useNavigation} from '@react-navigation/native';
import React, {useContext, useEffect, useState} from 'react';
import {
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import {AppContext} from '../contexts/AppContextProvider';
import BottomSheetLayout from '../layouts/BottomSheetLayout';
import AdminMenu from './AdminMenu';
import Connect from './Connect';
import InitiatePlanB from './InitiatePlanB';
import * as WebBrowser from 'expo-web-browser';
import ThemeData from '../configs/ThemeData';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

const {height} = Dimensions.get('window');

const LivePlugins = ({isOpen, setIsOpen}) => {
  const {bottom} = useSafeAreaInsets();

  const navigation = useNavigation();

  const {forceRefreshApiData} = useContext(AppContext);

  const [selectedView, setSelectedView] = useState();
  const [isPlanBOpen, setIsPlanBOpen] = useState(false);
  const [isAdminMenuOpen, setIsAdminMenuOpen] = useState(false);

  useEffect(() => {
    if (!isOpen) {
      resetState();
    }
  }, [isOpen]);

  const resetState = () => {
    setSelectedView();
  };

  const onItemClick = (clickedItem) => {
    switch (clickedItem) {
      case 'BlockCheck':
        setIsOpen(false);
        navigation.navigate('BlockCheck');
        break;
      case 'UpdateHistory':
        setIsOpen(false);
        navigation.navigate('UpdateHistory');
        break;
      case 'Connect':
        setSelectedView(<Connect setIsOpen={setIsOpen} />);
        break;
      case 'Settings':
        setIsOpen(false);
        navigation.navigate('Settings');
        break;
      case 'MoneyMarkets':
        setIsOpen(false);
        navigation.navigate('MoneyMarket');
        break;
      case 'RefreshBalances':
        WToast.show({
          data: 'Refreshing App Data',
          position: WToast.position.TOP,
        });
        setIsOpen(false);
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [{name: 'Drawer'}],
          }),
        );
        forceRefreshApiData();
        break;
      case 'OnHold':
        setIsOpen(false);
        setIsAdminMenuOpen(true);
        break;
      case 'ChatsIo':
        setIsOpen(false);
        navigation.navigate('Support', {openUserChat: true});
        break;
      case 'CryptoCoupen':
        WebBrowser.openBrowserAsync('https://cryptocoupon.com/');
        break;
      case 'ATM':
        WebBrowser.openBrowserAsync('https://atms.app/');
        break;
      case 'PlanB':
        setIsOpen(false);
        setIsPlanBOpen(true);
    }
  };

  const onSearchPress = () => {
    setIsOpen(false);
    navigation.navigate('GlobalSearch');
  };

  return (
    <>
      <BottomSheetLayout
        isOpen={isOpen}
        onClose={() => setIsOpen(false)}
        reactToKeyboard>
        <View style={styles.fragmentContainer}>
          {selectedView ? (
            selectedView
          ) : (
            <View style={styles.categorySelector}>
              <Image
                source={require('../assets/live-plugins-icon.png')}
                style={styles.headerImage}
                resizeMode="contain"
              />
              <ScrollView
                showsVerticalScrollIndicator={false}
                style={styles.listContainer}>
                {items.map((item) => (
                  <TouchableOpacity
                    key={item.title}
                    disabled={item.disabled}
                    onPress={() => onItemClick(item.title)}>
                    <View
                      style={[
                        styles.listItem,
                        item.disabled && {opacity: 0.5},
                      ]}>
                      <Image
                        source={item.image}
                        resizeMode="contain"
                        style={styles.itemIcon}
                      />
                      {/* <Text style={styles.itemName}>{item.title}</Text> */}
                    </View>
                  </TouchableOpacity>
                ))}
              </ScrollView>
              <TouchableOpacity
                onPress={onSearchPress}
                style={[
                  styles.action,
                  {
                    marginBottom: -bottom,
                    paddingBottom: bottom,
                    height: 55 + bottom,
                  },
                ]}>
                <Text style={styles.searchAction}>Search</Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </BottomSheetLayout>
      <InitiatePlanB isOpen={isPlanBOpen} setIsOpen={setIsPlanBOpen} />
      <AdminMenu isOpen={isAdminMenuOpen} setIsOpen={setIsAdminMenuOpen} />
    </>
  );
};

export default LivePlugins;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    backgroundColor: '#F1F4F6',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    overflow: 'hidden',
  },
  fragmentContainer: {
    backgroundColor: 'white',
  },
  categorySelector: {
    // flex: 1,
    paddingHorizontal: 40,
    paddingTop: 20,
    // paddingBottom: 20,
    maxHeight: height * 0.8,
  },
  headerImage: {
    height: 50,
    width: 200,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 25,
  },
  headerText: {
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
    textAlign: 'center',
  },
  listContainer: {
    // marginBottom: 60,
  },
  listItem: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 8,
    paddingVertical: 18,
    paddingHorizontal: 25,
    marginBottom: 20,
    justifyContent: 'center',
  },
  itemIcon: {
    height: 30,
    width: 150,
  },
  itemName: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    paddingHorizontal: 10,
    flex: 1,
  },
  action: {
    backgroundColor: '#F1F4F6',
    marginHorizontal: -40,
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
  },
  actionText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
  },
  updatesNumberContainer: {},
  updateNumberIcon: {
    height: 30,
  },
  updateNumber: {
    position: 'absolute',
    bottom: -6,
    right: -6,
    backgroundColor: '#FF5245',
    width: 22,
    height: 22,
    borderRadius: 11,
    justifyContent: 'center',
    alignItems: 'center',
  },
  updateNumberText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
    fontSize: 13,
  },
  searchAction: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 18,
  },
});

const items = [
  {
    image: require('../assets/block-check-full-logo.png'),
    title: 'BlockCheck',
  },
  {image: require('../assets/connect-icon.png'), title: 'Connect'},
  {image: require('../assets/crypto-coupen-icon.png'), title: 'CryptoCoupen'},
  {image: require('../assets/atm-plugin-icon.png'), title: 'ATM'},
  {image: require('../assets/settings-plugin-icon.png'), title: 'Settings'},
  {
    image: require('../assets/money-markets-full-icon.png'),
    title: 'MoneyMarkets',
  },
  {
    image: require('../assets/app-updates-icon.png'),
    title: 'UpdateHistory',
  },
  {
    image: require('../assets/force-update-icon.png'),
    title: 'RefreshBalances',
  },
  {
    image: require('../assets/chats-io-full.png'),
    title: 'ChatsIo',
  },
  {
    image: require('../assets/on-hold-full-icon.png'),
    title: 'OnHold',
  },
  {
    image: require('../assets/initaite-plan-b-icon.png'),
    title: 'PlanB',
  },
];
