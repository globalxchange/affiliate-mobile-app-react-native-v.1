/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useEffect, useState} from 'react';
import {
  View,
  StyleSheet,
  Image,
  Text,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import DrawerCover from './DrawerCover';
import {useNavigation, CommonActions} from '@react-navigation/native';
import {AppContext} from '../../contexts/AppContextProvider';
import DrawerMenu from './DrawerMenu';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {WToast} from 'react-native-smart-tip';
import FastImage from 'react-native-fast-image';
import {getUriImage} from '../../utils';
import ThemeData from '../../configs/ThemeData';
import TabSwitcher from './TabSwitcher';

const {width, height} = Dimensions.get('window');

const CustomDrawer = () => {
  const navigation = useNavigation();
  const {bottom, top} = useSafeAreaInsets();

  const {
    isLoggedIn,
    isAdminLoggedIn,
    userName,
    forceRefreshApiData,
    avatar,
  } = useContext(AppContext);

  const [isLoading, setIsLoading] = useState(false);
  const [emailId, setEmailId] = useState('');
  const [activeTab, setActiveTab] = useState(TABS[0]);

  useEffect(() => {
    (async () => {
      const email = await AsyncStorageHelper.getLoginEmail();
      setEmailId(email);
    })();

    return () => {};
  }, [isLoggedIn]);

  const removeAdminLogin = async () => {
    try {
      const success = await AsyncStorageHelper.removeAdminView();

      if (success) {
        forceRefreshApiData();
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [{name: 'Drawer'}],
          }),
        );
      } else {
        WToast.show({
          data: 'Error on removing Admin Login',
          position: WToast.position.TOP,
        });
      }
    } catch (error) {
      WToast.show({
        data: 'Error on removing Admin Login',
        position: WToast.position.TOP,
      });
    }
  };

  return (
    <View style={[styles.container]}>
      <TabSwitcher
        tabs={TABS}
        activeTab={activeTab}
        setActiveTab={setActiveTab}
      />
      <View style={[styles.drawer]}>
        {/* <TouchableOpacity
          onPress={() => navigation.dispatch(DrawerActions.closeDrawer())}>
          <View style={[styles.closeDrawerBtn]}>
            <Image
              style={styles.closeIcon}
              source={require('../../assets/cancel-icon-colored.png')}
              resizeMode="contain"
            />
          </View>
        </TouchableOpacity> */}
        <View style={[styles.viewContainer]}>
          <DrawerCover isLoading={isLoading} setIsLoading={setIsLoading} />
          <DrawerMenu setIsLoading={setIsLoading} menu={activeTab?.menu} />
          {isAdminLoggedIn && (
            <View style={[styles.adminView]}>
              <View style={styles.userContainer}>
                {avatar ? (
                  <FastImage
                    style={styles.avatar}
                    source={{uri: getUriImage(avatar)}}
                    resizeMode={'cover'}
                  />
                ) : (
                  <Image
                    style={[
                      styles.avatar,
                      {width: 30, height: 30, borderRadius: 0},
                    ]}
                    source={require('../../assets/app-logo.png')}
                    resizeMode="contain"
                  />
                )}
                <View style={styles.nameContainer}>
                  <Text numberOfLines={1} style={styles.userName}>
                    {userName}
                  </Text>
                  <Text numberOfLines={1} style={styles.userMail}>
                    {emailId}
                  </Text>
                </View>
              </View>
              <View style={styles.actionContainer}>
                <TouchableOpacity
                  onPress={() => navigation.navigate('AdminPage')}
                  style={[styles.buttonFilled, {marginRight: 15}]}>
                  <Text style={styles.buttonFilledText}>Change User</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => removeAdminLogin()}
                  style={styles.outlinedButton}>
                  <Text style={styles.outlinedButtonText}>Leave</Text>
                </TouchableOpacity>
              </View>
            </View>
          )}
          <View
            style={{
              height: bottom + (isAdminLoggedIn ? 0 : 50),
            }}
          />
        </View>
      </View>
    </View>
  );
};

export default CustomDrawer;

const styles = StyleSheet.create({
  container: {
    height: height,
    width: width,
    flexDirection: 'row',
  },
  drawer: {
    flex: 1,
    paddingTop: 20,
  },
  viewContainer: {
    flex: 1,
    // paddingVertical: 50,
  },
  loginButton: {
    borderColor: ThemeData.TEXT_COLOR,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    paddingVertical: 10,
    marginVertical: 20,
    width: 150,
    marginLeft: 'auto',
    marginRight: 'auto',
    flexDirection: 'row',
  },
  buttonText: {
    fontFamily: 'Montserrat-SemiBold',
    color: ThemeData.TEXT_COLOR,
    textAlign: 'center',
  },
  logoutButton: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
  },
  logoutButtonText: {
    color: 'white',
    textAlign: 'center',
  },
  closeDrawerBtn: {
    marginLeft: 20,
    marginTop: 10,
  },
  closeIcon: {
    width: 22,
    height: 22,
  },
  coverContainer: {
    marginTop: 50 - 22,
  },
  actionIcon: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
  adminView: {
    paddingHorizontal: 30,
  },
  userContainer: {
    flexDirection: 'row',
    marginBottom: 20,
    alignItems: 'center',
  },
  avatar: {
    width: 45,
    height: 45,
    borderRadius: 22.5,
  },
  nameContainer: {
    flex: 1,
    marginLeft: 10,
  },
  userName: {
    fontFamily: 'Montserrat-Bold',
    color: ThemeData.TEXT_COLOR,
    fontSize: 18,
    textTransform: 'capitalize',
  },
  userMail: {
    fontFamily: 'Montserrat',
    color: ThemeData.TEXT_COLOR,
    fontSize: 10,
  },
  actionContainer: {
    flexDirection: 'row',
  },
  buttonFilled: {
    backgroundColor: ThemeData.TEXT_COLOR,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    height: 40,
  },
  buttonFilledText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
    textAlign: 'center',
    fontSize: 12,
  },
  outlinedButton: {
    borderColor: ThemeData.TEXT_COLOR,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    height: 40,
  },
  outlinedButtonText: {
    fontFamily: 'Montserrat-SemiBold',
    color: ThemeData.TEXT_COLOR,
    textAlign: 'center',
    fontSize: 12,
  },
});

const TABS = [
  {
    tabName: 'Affilate',
    tabIcon: require('../../assets/MenuIcons/affiliate-logo.png'),
    menu: [
      {
        name: 'Network',
        color: '#CF1A38',
        menuIcon: require('../../assets/MenuIcons/network-chain.png'),
        route: 'Network',
        params: {screen: 'Chain'},
      },
      // {
      //   name: 'MetaBrands',
      //   menuIcon: require('../../assets/MenuIcons/meta-brands.png'),
      //   route: 'Earn',
      //   params: {screen: 'Brands', params: {screen: 'Feeds'}},
      // },
      {
        name: 'AffilateBank',
        menuIcon: require('../../assets/MenuIcons/affilate-bank.png'),
        route: 'Earn',
        params: {screen: 'AffliateBank'},
      },
      {
        name: 'CompPlans',
        menuIcon: require('../../assets/MenuIcons/comp-plans.png'),
        route: 'Earn',
        params: {screen: 'CreateBrokerage'},
      },
      // {
      //   name: 'Influencer',
      //   menuIcon: require('../../assets/MenuIcons/influencer.png'),
      //   route: 'UserProfile',
      //   loginRequired: true,
      // },
    ],
  },
  // {
  //   tabName: 'Recruiting',
  //   tabIcon: require('../../assets/MenuIcons/r-logo.png'),
  //   menu: [
  //     {
  //       name: 'InstantFunnels',
  //       menuIcon: require('../../assets/MenuIcons/instant-funnel.png'),
  //       route: 'Earn',
  //       params: {screen: 'Engage', params: {screen: 'Tools'}},
  //     },
  //     {
  //       name: 'Plan B',
  //       menuIcon: require('../../assets/MenuIcons/plan-b-icon.png'),
  //       route: 'Plan',
  //     },
  //   ],
  // },
  // {
  //   tabName: 'Vault',
  //   tabIcon: require('../../assets/MenuIcons/vault-logo.png'),
  //   menu: [
  //     {
  //       name: 'Crypto',
  //       menuIcon: require('../../assets/MenuIcons/crypto.png'),
  //       route: 'Wallet',
  //       params: {type: 'crypto'},
  //       loginRequired: true,
  //     },
  //     {
  //       name: 'Forex',
  //       menuIcon: require('../../assets/MenuIcons/forex.png'),
  //       route: 'Wallet',
  //       params: {type: 'fiat'},
  //       loginRequired: true,
  //     },
  //     {
  //       name: 'MoneyMarkets',
  //       menuIcon: require('../../assets/MenuIcons/money-markets.png'),
  //       color: '#186AB4',
  //       route: 'Wallet',
  //       params: {type: 'moneymarket'},
  //       loginRequired: true,
  //     },
  //     // {
  //     //   name: 'BlockCheck',
  //     //   menuIcon: require('../../assets/MenuIcons/block-check.png'),
  //     //   color: '#186AB4',
  //     //   route: 'BlockCheck',
  //     // },
  //     // {
  //     //   name: 'TokenHash',
  //     //   menuIcon: require('../../assets/MenuIcons/token-hash.png'),
  //     //   route: 'MoneyMarket',
  //     // },
  //   ],
  // },
  {
    tabName: 'Support',
    tabIcon: require('../../assets/MenuIcons/support.png'),
    menu: [
      {
        name: 'Bot',
        menuIcon: require('../../assets/MenuIcons/bots.png'),
        route: 'Support',
        params: {
          openBots: true,
          openMessage: null,
          openLearn: null,
          openUserChat: null,
        },
      },
      // {
      //   name: 'Chat',
      //   menuIcon: require('../../assets/MenuIcons/chat.png'),
      //   route: 'Support',
      //   params: {openMessage: true, openBots: null},
      // },
      // {
      //   name: 'Call',
      //   menuIcon: require('../../assets/MenuIcons/call.png'),
      //   disabled: true,
      // },
      // {
      //   name: 'Tickets',
      //   menuIcon: require('../../assets/MenuIcons/tickets.png'),
      //   disabled: true,
      // },
      {
        name: 'OnHold',
        menuIcon: require('../../assets/MenuIcons/onhold.png'),
        color: '#6AE7C2',
        route: 'OnHold',
      },
    ],
  },
  // {
  //   tabName: 'Stream',
  //   tabIcon: require('../../assets/MenuIcons/stream.png'),
  //   menu: [
  //     {
  //       name: 'Classrooms',
  //       menuIcon: require('../../assets/MenuIcons/classrooms.png'),
  //       route: 'Support',
  //       params: {
  //         openLearn: true,
  //         openUserChat: null,
  //         openMessage: null,
  //         openBots: null,
  //       },
  //     },
  //     {
  //       name: 'Chats',
  //       menuIcon: require('../../assets/MenuIcons/chats-one.png'),
  //       color: '#186AB4',
  //       route: 'Support',
  //       params: {
  //         openUserChat: true,
  //         openLearn: null,
  //         openMessage: null,
  //         openBots: null,
  //       },
  //     },
  //   ],
  // },
];
