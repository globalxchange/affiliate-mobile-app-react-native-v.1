/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useState} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import {DrawerActions, useNavigation} from '@react-navigation/native';
import {AppContext} from '../../contexts/AppContextProvider';
import ThemeData from '../../configs/ThemeData';
import InitiatePlanB from '../InitiatePlanB';
import AdminMenu from '../AdminMenu';

const DrawerMenu = ({menu}) => {
  const {activeRoute, isLoggedIn} = useContext(AppContext);

  const [isPlanBOpen, setIsPlanBOpen] = useState(false);
  const [isAdminMenuOpen, setIsAdminMenuOpen] = useState(false);

  const {navigate, dispatch} = useNavigation();

  const onMenuClick = (item) => {
    if (item.name === 'Plan B') {
      dispatch(DrawerActions.closeDrawer());
      return setIsPlanBOpen(true);
    }

    if (item.name === 'OnHold') {
      dispatch(DrawerActions.closeDrawer());
      return setIsAdminMenuOpen(true);
    }

    if (!menu.loginRequired) {
      return navigate(item.route, item.params);
    }

    if (!isLoggedIn) {
      return navigate('Landing');
    }

    return navigate(item.route, item.params);
  };

  return (
    <View style={styles.navContainer}>
      {menu?.map((item) => (
        <View
          key={item.name}
          style={[
            styles.navLink,
            (item.disabled || !item.route) && {opacity: 0.3},
          ]}>
          <TouchableOpacity
            disabled={item.disabled || !item.route}
            style={styles.buttonOverlay}
            onPress={() => onMenuClick(item)}>
            <Image source={item.menuIcon} style={styles.menuIcon} />
            <Text
              style={[
                styles.navText,
                activeRoute === item.route && styles.navActive,
                item.color && {color: item.color},
              ]}>
              {item.name}
            </Text>
          </TouchableOpacity>
        </View>
      ))}
      <InitiatePlanB isOpen={isPlanBOpen} setIsOpen={setIsPlanBOpen} />
      <AdminMenu isOpen={isAdminMenuOpen} setIsOpen={setIsAdminMenuOpen} />
    </View>
  );
};

export default DrawerMenu;

const styles = StyleSheet.create({
  navContainer: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 25,
  },
  navLink: {
    height: 75,
    alignItems: 'center',
  },
  buttonOverlay: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
  },
  navText: {
    flex: 1,
    color: ThemeData.TEXT_COLOR,
    fontSize: 19,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  navActive: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 20,
  },
  menuIcon: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
    marginRight: 10,
  },
});

const MENU_ITEMS = [
  {
    title: 'Network',
    route: 'Network',
    params: {screen: 'Chain'},
  },
  {
    title: 'Earn',
    route: 'Earn',
    params: {screen: 'BrokerTV'},
  },
  {
    title: 'Wallet',
    route: 'Wallet',
    loginRequired: true,
  },
  {
    title: 'Learn',
    route: 'Support',
  },
  {
    title: 'Settings',
    route: 'Settings',
    loginRequired: true,
  },
];
