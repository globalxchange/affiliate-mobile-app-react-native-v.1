/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {AppContext} from '../../contexts/AppContextProvider';
import PhotoPickerDialog from '../PhotoPickerDialog';
import * as ImagePicker from 'expo-image-picker';
import {FileSystem} from 'react-native-unimodules';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import Axios from 'axios';
import {GX_API_ENDPOINT, S3_CONFIG} from '../../configs';
import {decode} from 'base64-arraybuffer';
import {S3} from 'aws-sdk';
import {DrawerActions, useNavigation} from '@react-navigation/native';
import ThemeData from '../../configs/ThemeData';

const ProfileMenu = ({setIsLoading}) => {
  const {navigate, dispatch} = useNavigation();

  const {setBCHelperStatus, showBCHelper, userName, setLoginData} =
    useContext(AppContext);

  const [showAlert, setShowAlert] = useState(false);
  const [activeBottomTab, setActiveBottomTab] = useState(BOTTOM_TABS[1]);

  const changeProfilePic = async (useCamera) => {
    try {
      let result;
      const options = {
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 0.8,
      };

      if (useCamera) {
        result = await ImagePicker.launchCameraAsync(options);
      } else {
        result = await ImagePicker.launchImageLibraryAsync(options);
      }

      if (!result.cancelled) {
        setIsLoading(true);

        const BUCKET_NAME = 'gxnitrousdata';

        const S3Client = new S3({
          ...S3_CONFIG,
          Bucket: BUCKET_NAME,
        });

        const fPath = result.uri;

        let options = {encoding: FileSystem.EncodingType.Base64};
        FileSystem.readAsStringAsync(fPath, options)
          .then((data) => {
            const arrayBuffer = decode(data);

            const params = {
              Bucket: BUCKET_NAME,
              Key: `brandlogos/${userName}${Date.now()}.jpg`,
              Body: arrayBuffer,
              ContentType: 'image/jpeg',
              ACL: 'public-read',
            };

            S3Client.upload(params, async (err, s3Data) => {
              if (err) {
                console.log('Uploading Profile Pic Error', err);
              }

              if (s3Data.Location) {
                const email = await AsyncStorageHelper.getLoginEmail();
                const token = await AsyncStorageHelper.getAppToken();
                Axios.post(`${GX_API_ENDPOINT}/user/details/edit`, {
                  email,
                  field: 'profile_img',
                  value: s3Data.Location,
                  accessToken: token,
                })
                  .then((resp) => {
                    // console.log('Profile Update Success', resp.data);
                    getUserDetails(email);
                  })
                  .catch((error) => {
                    console.log('Profile Update Failed', error);
                  })
                  .finally(() => setIsLoading(false));
              }
              // console.log('Upload Success', s3Data);
            });
          })
          .catch((err) => {
            console.log('​getFile -> err', err);
            setIsLoading(false);
          });
      }
    } catch (E) {
      console.log(E);
      setIsLoading(false);
    }
  };

  const getUserDetails = (email) => {
    Axios.get(`${GX_API_ENDPOINT}/user/details/get`, {
      params: {email},
    })
      .then((res) => {
        const {data} = res;

        AsyncStorageHelper.setUserName(data.user.username);
        AsyncStorageHelper.setUserAvatarUrl(data.user.profile_img);
        AsyncStorageHelper.setAffId(data.user.affiliate_id);
        setLoginData(data.user.username, true, data.user.profile_img);
      })
      .catch((error) => {
        console.log('getUserDetails Error', error);
      });
  };

  const forgotPasswordHandler = () => {
    dispatch(DrawerActions.closeDrawer());
    navigate('ForgotPassword');
  };

  return (
    <View style={styles.navContainer}>
      <Text style={styles.header}>Settings</Text>
      <Text style={styles.subHeader}>Select One Of The Following Options</Text>
      <FlatList
        showsHorizontalScrollIndicator={false}
        data={OPTIONS}
        keyExtractor={(item) => item.title}
        renderItem={({item}) => (
          <TouchableOpacity disabled={item.disabled}>
            <View style={[styles.optionItem, item.disabled && {opacity: 0.4}]}>
              <View style={styles.optionDetails}>
                <Text style={styles.optionTitle}>{item.title}</Text>
                <Text style={styles.optionDesc}>{item.subText}</Text>
              </View>
              <Image
                source={require('../../assets/chevron-up.png')}
                resizeMode="contain"
                style={styles.arrowIcon}
              />
            </View>
          </TouchableOpacity>
        )}
      />
      <View style={styles.bottomTab}>
        {BOTTOM_TABS.map((item) => (
          <TouchableOpacity
            key={item.title}
            disabled={item.disabled}
            style={{flex: 1}}
            onPress={() => setActiveBottomTab(item)}>
            <View
              style={[
                styles.bottomTabItem,
                item.disabled && {opacity: 0.4},
                activeBottomTab?.title === item.title && {
                  borderTopWidth: 1,
                },
              ]}>
              <Text
                style={[
                  styles.bottomTabText,
                  activeBottomTab?.title === item.title && {
                    fontFamily: ThemeData.FONT_SEMI_BOLD,
                  },
                ]}>
                {item.title}
              </Text>
            </View>
          </TouchableOpacity>
        ))}
      </View>
      <PhotoPickerDialog
        isOpen={showAlert}
        setIsOpen={setShowAlert}
        callBack={(isCamera) => changeProfilePic(isCamera)}
      />
    </View>
  );
};

export default ProfileMenu;

const styles = StyleSheet.create({
  navContainer: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 20,
    paddingTop: 20,
  },
  header: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 25,
  },
  subHeader: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
    marginBottom: 30,
  },
  navItemContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 35,
  },
  navItemLabel: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    paddingVertical: 20,
  },
  changeText: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    textDecorationLine: 'underline',
  },
  switchContainer: {},
  switchButton: {
    borderColor: '#08152D',
    borderWidth: 1,
    borderRadius: 14,
    paddingHorizontal: 5,
    paddingVertical: 4,
  },
  switchDot: {
    width: 14,
    height: 14,
    borderRadius: 9,
    backgroundColor: '#08152D',
  },
  switchText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 11,
  },
  optionItem: {
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    padding: 20,
    alignItems: 'center',
    marginBottom: 15,
  },
  optionDetails: {
    flex: 1,
  },
  optionTitle: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    marginBottom: 5,
  },
  optionDesc: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 10,
    opacity: 0.75,
  },
  arrowIcon: {
    width: 20,
    height: 20,
    transform: [{rotate: '90deg'}],
  },
  bottomTab: {
    flexDirection: 'row',
    marginHorizontal: -35,
  },
  bottomTabItem: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopColor: ThemeData.APP_MAIN_COLOR,
  },
  bottomTabText: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
  },
});

const OPTIONS = [
  {
    title: 'Change Name',
    subText: 'Update First, Last, & Your Nick Name',
  },
  {title: 'Change Password', subText: 'Update First, Last, & Your Nick Name'},
  {title: 'Configure 2FA', subText: 'Update First, Last, & Your Nick Name'},
  {
    title: 'Interest Settings',
    subText: 'Update the destination of your daily earnings',
  },
  {
    title: 'Images And Bio',
    subText: 'Update First, Last, & Your Nick Name',
    disabled: true,
  },
  {
    title: 'Update Phone Number',
    subText: 'Update First, Last, & Your Nick Name',
    disabled: true,
  },
];

const BOTTOM_TABS = [
  {title: 'XID', disabled: true},
  {title: 'Settings'},
  {title: 'KYC', disabled: true},
  {title: 'Brain', disabled: true},
];
