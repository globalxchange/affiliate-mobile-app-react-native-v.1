/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useState} from 'react';
import {
  Image,
  StyleSheet,
  View,
  TouchableOpacity,
  ActivityIndicator,
  TouchableWithoutFeedback,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {AppContext} from '../../contexts/AppContextProvider';
import {getUriImage} from '../../utils';
import LogoutIcon from '../../assets/VectorIcons/logout-icon.svg';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {
  useNavigation,
  DrawerActions,
  CommonActions,
} from '@react-navigation/native';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import {GX_API_ENDPOINT, S3_CONFIG} from '../../configs';
import axios from 'axios';
import * as ImagePicker from 'expo-image-picker';
import {FileSystem} from 'react-native-unimodules';
import {decode} from 'base64-arraybuffer';
import {S3} from 'aws-sdk';
import ThemeData from '../../configs/ThemeData';
import PhotoPickerDialog from '../PhotoPickerDialog';

const TabSwitcher = ({tabs, activeTab, setActiveTab}) => {
  const {bottom} = useSafeAreaInsets();
  const navigation = useNavigation();

  const {
    isLoggedIn,
    removeLoginData,
    userName,
    avatar,
    setLoginData,
  } = useContext(AppContext);

  const [showAlert, setShowAlert] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const loginHandler = () => {
    navigation.dispatch(DrawerActions.closeDrawer());
    navigation.navigate('Landing');
  };

  const logoutHandler = () => {
    navigation.dispatch(DrawerActions.closeDrawer());
    navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [{name: 'Landing'}],
      }),
    );
    AsyncStorageHelper.deleteAllUserInfo();
    removeLoginData();
  };

  const changeProfilePic = async (useCamera) => {
    try {
      let result;
      const options = {
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 0.8,
      };

      if (useCamera) {
        result = await ImagePicker.launchCameraAsync(options);
      } else {
        result = await ImagePicker.launchImageLibraryAsync(options);
      }

      // console.log(result);

      setShowAlert(false);

      if (!result.cancelled) {
        setIsLoading(true);

        const BUCKET_NAME = 'gxnitrousdata';

        const S3Client = new S3({
          ...S3_CONFIG,
          Bucket: BUCKET_NAME,
        });

        const fPath = result.uri;

        let fileOptions = {encoding: FileSystem.EncodingType.Base64};
        FileSystem.readAsStringAsync(fPath, fileOptions)
          .then((data) => {
            const arrayBuffer = decode(data);

            const params = {
              Bucket: BUCKET_NAME,
              Key: `brandlogos/${userName}${Date.now()}.jpg`,
              Body: arrayBuffer,
              ContentType: 'image/jpeg',
              ACL: 'public-read',
            };

            S3Client.upload(params, async (err, s3Data) => {
              if (err) {
                console.log('Uploading Profile Pic Error', err);
              }

              if (s3Data.Location) {
                const email = await AsyncStorageHelper.getLoginEmail();
                const token = await AsyncStorageHelper.getAppToken();
                axios
                  .post(`${GX_API_ENDPOINT}/user/details/edit`, {
                    email,
                    field: 'profile_img',
                    value: s3Data.Location,
                    accessToken: token,
                  })
                  .then((resp) => {
                    // console.log('Profile Update Success', resp.data);
                    getUserDetails(email);
                  })
                  .catch((error) => {
                    console.log('Profile Update Failed', error);
                  })
                  .finally(() => setIsLoading(false));
              }
              // console.log('Upload Success', s3Data);
            });
          })
          .catch((err) => {
            console.log('​getFile -> err', err);
            setIsLoading(false);
          });
      }
    } catch (E) {
      console.log(E);
      setIsLoading(false);
      setShowAlert(false);
    }
  };

  const getUserDetails = (email) => {
    axios
      .get(`${GX_API_ENDPOINT}/user/details/get`, {
        params: {email},
      })
      .then((res) => {
        const {data} = res;

        AsyncStorageHelper.setUserName(data.user.username);
        AsyncStorageHelper.setUserAvatarUrl(data.user.profile_img);
        AsyncStorageHelper.setAffId(data.user.affiliate_id);
        setLoginData(data.user.username, true, data.user.profile_img, email);
      })
      .catch((error) => {
        console.log('getUserDetails Error', error);
      });
  };

  return (
    <View style={styles.container}>
      <View style={styles.profileContainer}>
        {avatar ? (
          <TouchableWithoutFeedback
            onPress={() => navigation.navigate('Settings')}>
            <FastImage
              style={styles.avatar}
              source={{uri: getUriImage(avatar)}}
              resizeMode={'cover'}
            />
          </TouchableWithoutFeedback>
        ) : (
          <Image
            style={[styles.avatar, {width: 30, height: 30, borderRadius: 0}]}
            source={require('../../assets/app-logo.png')}
            resizeMode="contain"
          />
        )}
        {isLoading && (
          <View style={styles.loadingContainer}>
            <ActivityIndicator size="large" color={ThemeData.TEXT_COLOR} />
          </View>
        )}
      </View>
      <View style={styles.tabContainer}>
        {/* <View style={[styles.tabItem]}>
          <Image
            style={[
              styles.tabIcon,
              {
                width: 35,
                height: 35,
              },
            ]}
            source={activeTab.tabIcon}
          />
        </View> */}
        {tabs?.map((item) => (
          <TouchableWithoutFeedback
            disabled={item.disabled}
            key={item.tabName}
            onPress={() => setActiveTab(item)}>
            <View style={[styles.tabItem, [item.disabled && {opacity: 0.3}]]}>
              <Image
                style={[
                  styles.tabIcon,
                  {
                    width: item.tabName === activeTab?.tabName ? 35 : 25,

                    height: item.tabName === activeTab?.tabName ? 35 : 25,
                  },
                ]}
                source={item.tabIcon}
              />
            </View>
          </TouchableWithoutFeedback>
        ))}
      </View>
      <TouchableOpacity
        onPress={isLoggedIn ? logoutHandler : loginHandler}
        style={styles.logoutButton}>
        <LogoutIcon color={ThemeData.TEXT_COLOR} />
      </TouchableOpacity>
      <View
        style={{
          height: bottom + 50,
        }}
      />
      <PhotoPickerDialog
        isOpen={showAlert}
        setIsOpen={setShowAlert}
        callBack={(isCamera) => changeProfilePic(isCamera)}
      />
    </View>
  );
};

export default TabSwitcher;

const styles = StyleSheet.create({
  container: {
    width: 80,
    alignItems: 'center',
    borderRightColor: ThemeData.BORDER_COLOR,
    borderRightWidth: 1,
    paddingTop: 20,
  },
  profileContainer: {
    width: 45,
    height: 45,
  },
  avatar: {
    width: 45,
    height: 45,
    borderRadius: 60,
  },
  logoutButton: {
    marginTop: 'auto',
  },
  loadingContainer: {
    ...StyleSheet.absoluteFill,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    zIndex: 10,
  },
  tabContainer: {
    flex: 1,
    marginTop: 40,
    alignItems: 'center',
  },
  tabItem: {
    marginBottom: 40,
  },
  tabIcon: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
  },
});
