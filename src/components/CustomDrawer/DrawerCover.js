/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useEffect, useState} from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {AppContext} from '../../contexts/AppContextProvider';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import Constants from 'expo-constants';
import {Permissions} from 'react-native-unimodules';
import {CommonActions, useNavigation} from '@react-navigation/native';
import Clipboard from '@react-native-community/clipboard';
import AddIcon from '../../assets/VectorIcons/add.svg';
import RefreshIcon from '../../assets/VectorIcons/refreshIcon.svg';
import ThemeData from '../../configs/ThemeData';
import {WToast} from 'react-native-smart-tip';
import AddNewModal from './AddNewModal';

const DrawerCover = ({isLoading, setIsLoading}) => {
  const {
    isLoggedIn,
    walletBalances,
    cryptoTableData,
    updateWalletBalances,
    profileId,
    userName,
    forceRefreshApiData,
  } = useContext(AppContext);

  const [vaultBalance, setVaultBalance] = useState(0);
  const [userEmail, setUserEmail] = useState('');
  const [isCopied, setIsCopied] = useState(false);
  const [isAddModalOpen, setIsAddModalOpen] = useState(false);

  const {dispatch} = useNavigation();

  useEffect(() => {
    (async () => {
      if (Constants.platform.ios) {
        const {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
          // alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

  useEffect(() => {
    if (isLoggedIn && !walletBalances && profileId) {
      updateWalletBalances();
    }
    (async () => {
      const email = await AsyncStorageHelper.getLoginEmail();
      setUserEmail(email);
    })();
  }, [isLoggedIn, profileId]);

  useEffect(() => {
    // console.log('Wallet Balance', walletBalances);
    // console.log('cryptoTableData', cryptoTableData);

    if (walletBalances && cryptoTableData) {
      let totalBalance = 0;

      cryptoTableData.forEach((item) => {
        const coinBalance =
          walletBalances[`${item.coinSymbol.toLowerCase()}_balance`] *
            item.price.USD || 0;

        totalBalance += coinBalance;
      });
      setVaultBalance(totalBalance);
    }
  }, [walletBalances, cryptoTableData]);

  const onCopyHandler = async () => {
    Clipboard.setString(userEmail);

    setIsCopied(true);

    setTimeout(() => setIsCopied(false), 3000);

    // navigate('Invite');
  };

  const onRefreshHandler = () => {
    WToast.show({
      data: 'Refreshing App Data',
      position: WToast.position.TOP,
    });
    dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{name: 'Drawer'}],
      }),
    );
    forceRefreshApiData();
  };

  return (
    <View style={styles.container}>
      <View style={styles.nameContainer}>
        <Text
          onPress={onCopyHandler}
          numberOfLines={1}
          style={[
            styles.userName,
            {textTransform: isLoggedIn ? 'capitalize' : 'none'},
          ]}>
          {isLoggedIn ? userName : 'Affiliate'}
        </Text>
        {isLoggedIn ? (
          <TouchableOpacity style={styles.copyBtn} onPress={onCopyHandler}>
            <Text style={[styles.copyLink]}>
              {isCopied ? 'NetworkSync Copied' : 'Copy NetworkSync'}
            </Text>
          </TouchableOpacity>
        ) : (
          <Text style={styles.walletBalance}>Monetize Everything</Text>
        )}
      </View>
      <TouchableOpacity style={{marginRight: 20}} onPress={onRefreshHandler}>
        <RefreshIcon color={ThemeData.TEXT_COLOR} />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => setIsAddModalOpen(true)}>
        <AddIcon color={ThemeData.TEXT_COLOR} />
      </TouchableOpacity>
      <AddNewModal
        isOpen={isAddModalOpen}
        onClose={() => setIsAddModalOpen(false)}
      />
    </View>
  );
};

export default DrawerCover;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: 10,
    paddingHorizontal: 25,
  },
  nameContainer: {
    flex: 1,
  },
  copyBtn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  userName: {
    fontSize: 24,
    color: ThemeData.TEXT_COLOR,
    fontFamily: 'Montserrat-Bold',
    textTransform: 'capitalize',
    marginRight: 10,
  },
  walletBalance: {
    color: ThemeData.TEXT_COLOR,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  copyLink: {
    color: ThemeData.TEXT_COLOR,
    fontSize: 12,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    marginRight: 10,
    opacity: 0.3,
  },
  backButton: {
    width: 30,
    height: 30,
    padding: 6,
  },
  backButtonIcon: {
    flex: 1,
    height: null,
    width: null,
  },
});
