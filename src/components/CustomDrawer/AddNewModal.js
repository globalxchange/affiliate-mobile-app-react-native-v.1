import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import PopupLayout from '../../layouts/PopupLayout';
import {useNavigation} from '@react-navigation/native';
import ThemeData from '../../configs/ThemeData';

const AddNewModal = ({isOpen, onClose}) => {
  const {navigate} = useNavigation();

  const onInviteNavigate = () => {
    onClose();
    navigate('Invite');
  };

  const onNewDeal = () => {
    onClose();
    navigate('Earn', {
      screen: 'CreateBrokerage',
      params: {screen: 'CreateBrokerage'},
    });
  };

  return (
    <PopupLayout
      isOpen={isOpen}
      onClose={onClose}
      autoHeight
      headerImage={require('../../assets/affliate-app-logo-white.png')}>
      <View style={styles.container}>
        <TouchableOpacity style={styles.addItem} onPress={onInviteNavigate}>
          <Image
            style={styles.addImage}
            source={require('../../assets/invite-user.png')}
          />
          <Text style={styles.addText}>New User</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.addItem} onPress={onNewDeal}>
          <Image
            style={styles.addImage}
            source={require('../../assets/deal-icon.png')}
          />
          <Text style={styles.addText}>New Deal</Text>
        </TouchableOpacity>
      </View>
    </PopupLayout>
  );
};

export default AddNewModal;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: 30,
    justifyContent: 'space-around',
  },
  addItem: {
    alignItems: 'center',
  },
  addImage: {
    width: 55,
    height: 55,
    resizeMode: 'contain',
  },
  addText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    textAlign: 'center',
    color: ThemeData.TEXT_COLOR,
  },
});
