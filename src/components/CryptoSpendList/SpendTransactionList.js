/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {FlatList, Image, StyleSheet, Text, TextInput, View} from 'react-native';
import Moment from 'moment-timezone';
import {TouchableOpacity} from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image';
import LoadingAnimation from '../LoadingAnimation';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import {APP_CODE, GX_API_ENDPOINT} from '../../configs';
import Axios from 'axios';
import {usdValueFormatter} from '../../utils';

const SpendTransactionList = ({
  isListExpanded,
  setIsListExpanded,
  activeVault,
  isMenuOpened,
  setIsSearchOpen,
  isSearchOpen,
}) => {
  const [searchInput, setSearchInput] = useState('');
  const [transactionsList, setTransactionsList] = useState();
  const [filteredList, setFilteredList] = useState();

  useEffect(() => {
    if (activeVault) {
      setTransactionsList();
      (async () => {
        const profileId = await AsyncStorageHelper.getProfileId();

        const postData = {
          app_code: APP_CODE,
          profile_id: profileId,
          vault_id: activeVault.vault_id,
          coin: activeVault.coin,
        };

        Axios.post(`${GX_API_ENDPOINT}/coin/vault/service/txns/get`, postData)
          .then((resp) => {
            const {data} = resp;

            // console.log('Tranactions', data);

            setTransactionsList(data?.txns || []);
          })
          .catch((error) => {
            console.log('Error on getting Transaction List');
          });
      })();
    }
  }, [activeVault]);

  useEffect(() => {
    if (!isListExpanded) {
      setSearchInput('');
    }
  }, [isListExpanded]);

  useEffect(() => {
    if (transactionsList) {
      const searchQuery = searchInput.trim().toLowerCase();

      const filtered = transactionsList.filter((item) =>
        item.pid.toLowerCase().includes(searchQuery),
      );

      setFilteredList(filtered || []);
    }
  }, [searchInput, transactionsList]);

  if (isMenuOpened) {
    return null;
  }

  if (!transactionsList) {
    return <LoadingAnimation />;
  }

  return (
    <View
      style={[styles.container, isListExpanded && styles.expandedContainer]}>
      {isSearchOpen && (
        <View style={styles.searchContainer}>
          <TouchableOpacity onPress={() => setIsSearchOpen(false)}>
            <FastImage
              style={styles.countryIcon}
              source={require('../../assets/back-arrow.png')}
              resizeMode="contain"
            />
          </TouchableOpacity>
          <View style={styles.separator} />
          <TextInput
            style={styles.searchInput}
            onChangeText={(text) => setSearchInput(text)}
            value={searchInput}
            autoFocus={false}
            placeholder={'Search Transaction'}
            placeholderTextColor={'#878788'}
          />
          <Image
            style={styles.searchIcon}
            source={require('../../assets/search-icon.png')}
            resizeMode="contain"
          />
        </View>
      )}
      <View style={[styles.titleContainer, isSearchOpen && {display: 'none'}]}>
        <Text style={styles.title}>Funding Transactions</Text>
        <Text
          onPress={() => {
            setIsListExpanded(true);
            setIsSearchOpen(true);
          }}
          style={styles.seekAllButton}>
          All Transactions
        </Text>
      </View>
      <View
        style={[
          styles.listContainer,
          isListExpanded && {marginHorizontal: -20},
        ]}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={filteredList}
          keyExtractor={(item) => item._id}
          renderItem={({item}) => (
            <ListItem
              name={item.pid}
              amount={usdValueFormatter.format(item.amount || 0)}
              date={Moment(item.timestamp).format('MMM Do YYYY')}
              type={item.deposit ? 'Credit' : 'Debit'}
              icon={
                item.deposit
                  ? require('../../assets/spend-credit.png')
                  : require('../../assets/spend-debit.png')
              }
            />
          )}
          ListEmptyComponent={
            <ListItem
              name="No Transactions"
              amount="$0.00"
              date={Moment().format('MMM Do YYYY')}
              type="Fund"
            />
          }
        />
      </View>
    </View>
  );
};

const ListItem = ({name, amount, date, type, icon}) => {
  return (
    <TouchableOpacity disabled style={styles.itemContainer}>
      <View style={styles.logoContainer}>
        <Image
          source={icon || require('../../assets/icon.android.png')}
          style={[styles.itemLogo]}
          resizeMode="contain"
        />
      </View>
      <View style={styles.itemDetails}>
        <View style={[styles.itemDetailsContainer, {marginBottom: 2}]}>
          <Text numberOfLines={1} style={styles.itemName}>
            {name}
          </Text>
          <Text style={styles.itemValue}>{amount}</Text>
        </View>
        <View style={styles.itemDetailsContainer}>
          <Text style={styles.itemDate}>{date}</Text>
          <Text style={styles.itemDesc}>{type}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default SpendTransactionList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // marginTop: 15,
  },
  expandedContainer: {
    marginTop: -20,
    paddingHorizontal: 8,
  },
  listContainer: {
    paddingHorizontal: 20,
    flex: 1,
  },
  itemContainer: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  logoContainer: {
    borderRadius: 10,
    overflow: 'hidden',
  },
  itemLogo: {
    width: 40,
    height: 40,
  },
  itemDetails: {
    flex: 1,
    justifyContent: 'center',
  },
  itemDetailsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 10,
    alignItems: 'center',
  },
  itemName: {
    color: '#464B4E',
    fontFamily: 'Montserrat-Bold',
    fontSize: 13,
    flex: 1,
    marginRight: 20,
  },
  itemValue: {
    color: '#464B4E',
    fontFamily: 'Montserrat-Bold',
    fontSize: 13,
  },
  itemDesc: {
    color: '#9EA1AD',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 11,
  },
  itemDate: {
    color: '#9EA1AD',
    fontFamily: 'Montserrat',
    fontSize: 11,
  },
  searchContainer: {
    flexDirection: 'row',
    marginTop: 40,
    height: 45,
    paddingHorizontal: 15,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.15,
    shadowRadius: 2,
    backgroundColor: 'white',
    borderRadius: 2,
    elevation: 12,
    alignItems: 'center',
    marginBottom: 30,
  },
  countryIcon: {
    height: 22,
    width: 22,
    marginTop: 'auto',
    marginBottom: 'auto',
    marginRight: 15,
  },
  separator: {
    backgroundColor: '#CACACA',
    width: 1,
    marginVertical: 8,
  },
  searchInput: {
    flexGrow: 1,
    width: 0,
    marginTop: 'auto',
    marginBottom: 'auto',
    paddingHorizontal: 15,
    fontFamily: 'Montserrat',
    fontWeight: 'normal',
    color: 'black',
  },
  searchIcon: {
    height: 15,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  emptyText: {
    color: '#464B4E',
    fontFamily: 'Montserrat-Bold',
    textAlign: 'center',
    marginTop: 20,
  },
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    marginBottom: 20,
  },
  title: {
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 14,
  },
  seekAllButton: {
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    opacity: 0.5,
    fontSize: 11,
  },
});
