/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {usdValueFormatter} from '../../utils';
import SpendTransactionList from './SpendTransactionList';
import MenuList from './MenuList';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../configs';

const CryptoSpendList = ({
  isListExpanded,
  setIsListExpanded,
  activeVault,
  setIsAddFundOpen,
  setIsSpendableOpen,
  processingBalance,
  setProcessingBalance,
}) => {
  const [isMenuOpened, setIsMenuOpened] = useState(false);
  const [isLoadSpendBalance, setIsLoadSpendBalance] = useState(false);
  const [isSearchOpen, setIsSearchOpen] = useState(false);
  const [isLoadPendingBalance, setIsLoadPendingBalance] = useState(false);
  const [isLoadCardBalance, setIsLoadCardBalance] = useState(false);
  const [processingList, setProcessingList] = useState();

  useEffect(() => {
    (async () => {
      const email = await AsyncStorageHelper.getLoginEmail();

      Axios.get(
        `${GX_API_ENDPOINT}/coin/vault/service/spending/processing/balance/get`,
        {params: {email}},
      )
        .then((resp) => {
          const {data} = resp;

          if (data.status) {
            const balance = data.balances[0];
            setProcessingBalance(balance?.amount || 0);
            setProcessingList(balance?.logs || []);
          }
        })
        .catch((error) => {});
    })();
  }, []);

  useEffect(() => {
    if (!isMenuOpened) {
      setIsLoadSpendBalance(false);
    }
  }, [isMenuOpened]);

  const toggleMenu = () => {
    setIsMenuOpened(!isMenuOpened);

    if (isMenuOpened) {
      setIsLoadCardBalance(false);
      setIsLoadPendingBalance(false);
      setIsLoadSpendBalance(false);
    }
  };

  let balanceAmount = activeVault?.usd_balance;
  let balanceText = 'Vault Balance';

  if (isLoadSpendBalance) {
    balanceAmount = activeVault?.spendable_balance?.USD || 0;
    balanceText = 'Spendable Balance';
  }
  if (isLoadCardBalance) {
    balanceAmount = activeVault?.spendable_balance?.USD || 0;
    balanceText = 'Card Balance';
  }
  if (isLoadPendingBalance) {
    balanceAmount = processingBalance;
    balanceText = 'Processing Balance';
  }

  return (
    <View
      style={[
        styles.listContainer,
        isListExpanded && styles.expandedListContainer,
      ]}>
      <View style={{display: isSearchOpen ? 'none' : 'flex'}}>
        <View
          style={[
            styles.listHeaderContainer,
            isListExpanded && {
              borderBottomWidth: 0,
              marginBottom: 40,
              marginHorizontal: -10,
            },
          ]}>
          <TouchableOpacity
            onPress={() => setIsListExpanded(!isListExpanded)}
            style={styles.expandButton}>
            <Image
              resizeMode="cover"
              style={[{height: 20, width: 20}]}
              source={
                isListExpanded
                  ? require('../../assets/collapse-icon.png')
                  : require('../../assets/expand-icon.png')
              }
            />
          </TouchableOpacity>
          <View style={styles.balanceContainer}>
            <Text style={styles.balanceAmount}>
              {usdValueFormatter.format(balanceAmount || 0)}
            </Text>
            <Text style={styles.balanceType}>{balanceText}</Text>
          </View>
          <TouchableOpacity onPress={toggleMenu} style={styles.menuButton}>
            <Image
              resizeMode="cover"
              style={styles.menuImage}
              source={
                isMenuOpened
                  ? require('../../assets/round-close-icon.png')
                  : require('../../assets/round-menu-icon.png')
              }
            />
          </TouchableOpacity>
        </View>
      </View>
      <MenuList
        isListExpanded={isListExpanded}
        setIsAddFundOpen={setIsAddFundOpen}
        isMenuOpened={isMenuOpened}
        setIsLoadSpendBalance={setIsLoadSpendBalance}
        onClose={() => setIsMenuOpened(false)}
        setIsListExpanded={setIsListExpanded}
        activeVault={activeVault}
        setIsSearchOpen={setIsSearchOpen}
        isSearchOpen={isSearchOpen}
        openMakeSpend={() => setIsSpendableOpen(true)}
        setIsLoadPendingBalance={setIsLoadPendingBalance}
        setIsLoadCardBalance={setIsLoadCardBalance}
        processingList={processingList}
        isLoadPendingBalance={isLoadPendingBalance}
      />
      <SpendTransactionList
        isListExpanded={isListExpanded}
        setIsListExpanded={setIsListExpanded}
        setIsSearchOpen={setIsSearchOpen}
        isSearchOpen={isSearchOpen}
        activeVault={activeVault}
        isMenuOpened={isMenuOpened}
      />
    </View>
  );
};

export default CryptoSpendList;

const styles = StyleSheet.create({
  listHeaderContainer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
    borderBottomColor: '#EBEBEB',
    borderBottomWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  balanceContainer: {
    alignItems: 'center',
    flex: 1,
  },
  balanceType: {
    color: '#484848',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
  balanceAmount: {
    color: '#464B4E',
    fontFamily: 'Montserrat',
    fontSize: 30,
  },
  menuButton: {
    position: 'absolute',
    right: 20,
    zIndex: 1,
  },
  menuImage: {
    width: 26,
    height: 26,
  },
  listContainer: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    flex: 1,
    marginHorizontal: 30,
    marginTop: 5,
  },
  expandedListContainer: {
    borderWidth: 0,
    paddingHorizontal: 0,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#484848',
    borderWidth: 1,
    borderRadius: 6,
    paddingHorizontal: 15,
    paddingVertical: 8,
    marginRight: 15,
  },
  buttonFilled: {
    backgroundColor: '#08152D',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 6,
    paddingHorizontal: 15,
    paddingVertical: 8,
    marginRight: 15,
  },
  addIcon: {
    width: 16,
    height: 16,
  },
  buttonFilledText: {
    fontFamily: 'Montserrat-SemiBold',
    marginLeft: 10,
    fontSize: 12,
    color: 'white',
  },
  buttonText: {
    color: '#464B4E',
    fontFamily: 'Montserrat',
    marginLeft: 10,
    fontSize: 12,
  },
  expandButton: {
    position: 'absolute',
    left: 20,
    zIndex: 1,
  },
});
