import React from 'react';
import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';

const EmptySpendCards = () => {
  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.headerContainer}>
          <Text style={styles.header}>Spend Your Crypto Instantly</Text>
          <View style={styles.subHeaderContainer}>
            <Text style={styles.subHeader}>Powered By</Text>
            <Image
              source={require('../assets/crypto-spen-icon.png')}
              style={styles.cryptoSpendIcon}
              resizeMode="center"
            />
          </View>
        </View>
        {list.map((item) => (
          <View style={styles.itemContainer} key={item.title}>
            <Text style={styles.itemHeader}>{item.title}</Text>
            <Image
              style={styles.itemImage}
              source={item.image}
              resizeMode="cover"
            />
          </View>
        ))}
      </ScrollView>
    </View>
  );
};

export default EmptySpendCards;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 30,
  },
  headerContainer: {
    marginBottom: 30,
    marginTop: 30,
  },
  header: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 28,
  },
  subHeaderContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 8,
  },
  subHeader: {
    color: '#484848',
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
    fontSize: 15,
  },
  cryptoSpendIcon: {
    width: 100,
    marginLeft: 10,
  },
  itemContainer: {
    borderColor: '#E5E5E5',
    borderWidth: 1,
    marginBottom: 20,
  },
  itemHeader: {
    color: '#343C48',
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 20,
    paddingVertical: 10,
    fontSize: 16,
  },
  itemImage: {
    height: 200,
    width: '100%',
  },
});

const list = [
  {title: 'Earn It', image: require('../assets/spend-landing/1.jpg')},
  {title: 'Convert It', image: require('../assets/spend-landing/2.jpg')},
  {title: 'Spend It', image: require('../assets/spend-landing/3.jpg')},
];
