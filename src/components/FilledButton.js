import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../configs/ThemeData';

const FilledButton = ({onPress, style, title, color, active}) => {
  return (
    <TouchableOpacity onPress={onPress} style={[styles.outlinedButton, style]}>
      <Text
        style={[
          styles.outlinedBtnText,
          active && styles.active,
          color && {color},
        ]}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

export default FilledButton;

const styles = StyleSheet.create({
  outlinedButton: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    flex: 1,
  },
  outlinedBtnText: {
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 12,
  },
  active: {
    fontFamily: 'Montserrat-Bold',
  },
});
