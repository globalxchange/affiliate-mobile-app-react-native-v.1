import React from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Tooltip from 'react-native-walkthrough-tooltip';
import ThemeData from '../configs/ThemeData';

const {width} = Dimensions.get('window');

const ToolTipContent = ({children, isVisible, onClose, desc}) => {
  return (
    <Tooltip
      isVisible={isVisible}
      content={
        <View style={styles.tooltipContainer}>
          <TouchableOpacity style={styles.tooltipButton}>
            <Text style={styles.buttonText}>Learn More</Text>
          </TouchableOpacity>
          <Text style={styles.tooltipText}>{desc}</Text>
        </View>
      }
      contentStyle={styles.contentStyle}
      tooltipStyle={styles.tooltipStyle}
      arrowStyle={styles.arrowStyle}
      placement="top"
      disableShadow
      allowChildInteraction
      onClose={onClose}>
      {children}
    </Tooltip>
  );
};

export default ToolTipContent;

const styles = StyleSheet.create({
  contentStyle: {
    elevation: 0,
    borderRadius: 0,
    padding: 0,
    borderColor: ThemeData.APP_MAIN_COLOR,
    borderWidth: 0.25,
    backgroundColor: 'white',
  },
  arrowStyle: {
    zIndex: 10,
  },
  tooltipStyle: {},
  backgroundStyle: {},
  tooltipContainer: {},
  tooltipText: {
    textAlign: 'center',
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 11,
    lineHeight: 18,
    paddingHorizontal: 10,
    paddingVertical: 20,
  },
  tooltipButton: {
    backgroundColor: '#08152D',
    height: 30,
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 12,
  },
});
