import React, {useState, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import OTCBotsBottomSheet from './OTCBotsBottomSheet';
import {AppContext} from '../../contexts/AppContextProvider';
import {useNavigation} from '@react-navigation/native';
import VectorIcons from '../../assets/VectorIcons';

const OTCBots = ({selectedApp}) => {
  const {cryptoTableData, isLoggedIn} = useContext(AppContext);

  const {navigate} = useNavigation();

  const [isSheetOpen, setIsSheetOpen] = useState(false);
  const [selectedItem, setSelectedItem] = useState('');

  let fiatAssets = 0;
  let cryptoAssets = 0;

  if (cryptoTableData) {
    // console.log('cryptoTableData', cryptoTableData);

    fiatAssets = cryptoTableData.filter((item) => item.asset_type === 'Fiat')
      .length;

    cryptoAssets = cryptoTableData.length - fiatAssets;
  }

  const lists = [
    {
      icon: require('../../assets/money-icon.png'),
      title: 'Fiat To Crypto',
      numbers: `${fiatAssets * cryptoAssets} Pairs`,
      fromAssets: 'Fiat',
      toAsset: 'Crypto',
    },
    {
      icon: require('../../assets/crypto-fiat-icon.png'),
      title: 'Crypto To Fiat',
      numbers: `${fiatAssets * cryptoAssets} Pairs`,
      fromAssets: 'Crypto',
      toAsset: 'Fiat',
    },
    {
      icon: require('../../assets/crypto-crypto-icon.png'),
      title: 'Crypto To Crypto',
      numbers: `${cryptoAssets * cryptoAssets} Pairs`,
      fromAssets: 'Crypto',
      toAsset: 'Crypto',
    },
    {
      icon: require('../../assets/fiat-fiat-icon.png'),
      title: 'Fiat To Fiat',
      numbers: `${fiatAssets * fiatAssets} Pairs`,
      fromAssets: 'Fiat',
      toAsset: 'Fiat',
    },
  ];

  const onItemClick = (item) => {
    if (isLoggedIn) {
      setSelectedItem(item);
      setIsSheetOpen(true);
    } else {
      navigate('Landing');
    }
  };

  const openChat = () => {
    navigate('Support', {
      openMessage: true,
      messageData: 'Hey Support,\nI want to learn more about my exchange rates',
    });
  };

  const color = selectedApp?.color || '#08152D';

  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      style={styles.scrollContainer}>
      <View style={styles.container}>
        <View style={styles.headerContainer}>
          <VectorIcons.OTCBotsIcon color={color} height={24} width={24} />
          <Text style={[styles.title, {color}]}>Set Your Exchange Rates</Text>
        </View>
        <Text style={styles.desc}>
          The {selectedApp.title} Brokerage License From GX Allows You To
          Control The Exchange Rates For All Pairs Which This Banker Supports.
        </Text>
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.desc}>Learn More </Text>
          <Text style={[styles.hereButton, {color}]} onPress={openChat}>
            Contact Support
          </Text>
        </View>
        <View style={styles.listContainer}>
          {lists.map((item, index) => (
            <TouchableOpacity
              key={index}
              style={styles.listItem}
              onPress={() => onItemClick(item)}>
              <Image
                style={styles.itemIcon}
                source={item.icon}
                resizeMode="contain"
              />
              <Text style={styles.itemTitle}>{item.title}</Text>
              <Text style={styles.itemNumbers}>{item.numbers}</Text>
            </TouchableOpacity>
          ))}
        </View>
      </View>
      <OTCBotsBottomSheet
        setIsSheetOpen={setIsSheetOpen}
        isSheetOpen={isSheetOpen}
        data={selectedItem}
        selectedApp={selectedApp}
        color={color}
      />
    </ScrollView>
  );
};

export default OTCBots;

const styles = StyleSheet.create({
  scrollContainer: {
    flex: 1,
    paddingTop: 10,
    marginTop: 10,
  },
  container: {
    flex: 1,
  },
  title: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    fontSize: 18,
    marginLeft: 10,
  },
  desc: {
    fontFamily: 'Montserrat',
    color: '#9A9A9A',
    fontSize: 11,
    marginBottom: 5,
  },
  hereButton: {
    color: '#08152D',
    fontSize: 11,
    fontFamily: 'Montserrat-SemiBold',
    textDecorationLine: 'underline',
  },
  listContainer: {
    marginTop: 10,
  },
  listItem: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    paddingVertical: 18,
    paddingHorizontal: 20,
    marginTop: 10,
  },
  itemIcon: {
    height: 20,
    width: 20,
  },
  itemTitle: {
    flex: 1,
    paddingHorizontal: 15,
    fontFamily: 'Montserrat-Bold',
    color: '#001D41',
    fontSize: 13,
    opacity: 0.7,
  },
  itemNumbers: {
    color: '#001D41',
    opacity: 0.5,
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  headerIcon: {
    height: 24,
    width: 24,
    marginRight: 10,
  },
});
