import {useNavigation} from '@react-navigation/native';
import React, {useContext, useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../../../configs/ThemeData';
import {AppContext} from '../../../contexts/AppContextProvider';
import AssetsIoController from '../AssetsIoController';

const AssetsIoView = ({color, selectedApp}) => {
  const {isLoggedIn} = useContext(AppContext);

  const {navigate} = useNavigation();

  const [isControllerOpen, setIsControllerOpen] = useState(false);

  const onControllerOpen = () => {
    if (isLoggedIn) {
      setIsControllerOpen(true);
    } else {
      navigate('Landing');
    }
  };

  return (
    <View style={styles.container}>
      <Text style={[styles.title, {color}]}>Control Interest Rates</Text>
      <Text style={styles.desc}>
        The Assets.io Broker License Allows You To Control Liquid And
        Contractual Interest Rates For Your Customers. By Increasing Or
        Decreasing Rates For Your Direct Users, You Unclock Passive Income From
        Each Broker That Have To Suspend This License.
      </Text>
      <View style={styles.actionContainer}>
        <TouchableOpacity style={[styles.outlinedButton, {borderColor: color}]}>
          <Text style={[styles.outlinedButtonText, {color}]}>Update Rates</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={onControllerOpen}
          style={[styles.filledButton, {backgroundColor: color}]}>
          <Text style={styles.filledButtonText}>Full Controller</Text>
        </TouchableOpacity>
      </View>
      <AssetsIoController
        color={color}
        selectedApp={selectedApp}
        isOpen={isControllerOpen}
        setIsOpen={setIsControllerOpen}
      />
    </View>
  );
};

export default AssetsIoView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
    marginTop: 10,
  },
  title: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    fontSize: 16,
  },
  desc: {
    fontFamily: 'Montserrat',
    color: '#9A9A9A',
    fontSize: 11,
    marginBottom: 5,
    marginTop: 10,
  },
  actionContainer: {
    flexDirection: 'row',
    marginTop: 15,
  },
  outlinedButton: {
    flex: 1,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    marginRight: 15,
  },
  outlinedButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 11,
  },
  filledButton: {
    flex: 1,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  filledButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 11,
    color: 'white',
  },
});
