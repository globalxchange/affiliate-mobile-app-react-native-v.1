import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Text,
  Dimensions,
  StatusBar,
} from 'react-native';
import {useContext} from 'react';
import {AppContext} from '../../../contexts/AppContextProvider';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';
import MainList from './MainList';
import FeesView from './FeesView';
import BottomSheetLayout from '../../../layouts/BottomSheetLayout';
import ThemeData from '../../../configs/ThemeData';
import CustomFeeList from './CustomFeeList';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

const {height} = Dimensions.get('window');

const OTCBotsBottomSheet = ({
  isSheetOpen,
  setIsSheetOpen,
  data,
  selectedApp,
  isEditor,
}) => {
  const {top, bottom} = useSafeAreaInsets();

  const {
    cryptoTableData,
    setIsInstaCryptoFeeEditorHide,
    isInstaCryptoFeeEditorHide,
  } = useContext(AppContext);

  const [selectedControllerItem, setSelectedControllerItem] = useState('');
  const [controllerItems, setControllerItems] = useState([]);
  const [listItems, setListItems] = useState([]);
  const [feeData, setFeeData] = useState();
  const [feeViewData, setFeeViewData] = useState();
  const [massFeeEditOpen, setMassFeeEditOpen] = useState(false);
  const [isSearchOpen, setIsSearchOpen] = useState(false);
  const [currentView, setCurrentView] = useState();

  useEffect(() => {
    getAllFees();
  }, []);

  useEffect(() => {
    if (!isSheetOpen) {
      setFeeViewData();
      setMassFeeEditOpen(false);
      setIsSearchOpen(false);
    }
  }, [isSheetOpen]);

  useEffect(() => {
    if (cryptoTableData) {
      const controls = [];

      cryptoTableData.forEach((item) => {
        if (data.fromAssets === item.asset_type) {
          controls.push(item);
        }
      });

      setControllerItems(controls);
      setSelectedControllerItem(controls[0] || '');
    }
  }, [cryptoTableData, data]);

  useEffect(() => {
    if (cryptoTableData) {
      const list = [];

      cryptoTableData.forEach((item) => {
        if (
          data.toAsset === item.asset_type &&
          item.coinSymbol !== selectedControllerItem.coinSymbol
        ) {
          list.push({
            ...item,
            comboSymbol: `${item.coinSymbol}${selectedControllerItem.coinSymbol}`,
            comboText: `${item.coinSymbol}/${selectedControllerItem.coinSymbol}`,
            from: selectedControllerItem.coinSymbol,
            to: item.coinSymbol,
          });
        }
      });
      setListItems(list);
    }
    return () => {};
  }, [cryptoTableData, data, selectedControllerItem]);

  useEffect(() => {
    let activeView = null;

    if (data?.isCustom) {
      activeView = (
        <CustomFeeList
          setIsSearchOpen={setIsSearchOpen}
          isSearchOpen={isSearchOpen}
          onHide={() => setIsInstaCryptoFeeEditorHide(true)}
        />
      );
    } else if (massFeeEditOpen) {
      activeView = (
        <FeesView
          feeViewData={feeViewData}
          setIsSheetOpen={setIsSheetOpen}
          onClose={() => {
            getAllFees();
            setFeeViewData();
            setMassFeeEditOpen(false);
          }}
          getFees={getFees}
          isEditor={isEditor}
          isMassEdit
          openMassEdit={() => setMassFeeEditOpen(true)}
          pairData={listItems}
        />
      );
    } else if (feeViewData) {
      activeView = (
        <FeesView
          feeViewData={feeViewData}
          setIsSheetOpen={setIsSheetOpen}
          onClose={() => {
            getAllFees();
            setFeeViewData();
          }}
          getFees={getFees}
          isEditor={isEditor}
          openMassEdit={() => setMassFeeEditOpen(true)}
          pairData={listItems}
        />
      );
    } else {
      activeView = (
        <MainList
          data={data}
          controllerItems={controllerItems}
          getFees={getFees}
          listItems={listItems}
          selectedControllerItem={selectedControllerItem}
          setSelectedControllerItem={setSelectedControllerItem}
          setIsSheetOpen={setIsSheetOpen}
          setFeeViewData={setFeeViewData}
          openMassEdit={() => {
            setFeeViewData({from: selectedControllerItem.coinSymbol});
            setMassFeeEditOpen(true);
          }}
        />
      );
    }
    setCurrentView(activeView);
  }, [
    isSearchOpen,
    data,
    massFeeEditOpen,
    feeViewData,
    isEditor,
    listItems,
    controllerItems,
    selectedControllerItem,
  ]);

  const getAllFees = async () => {
    const email = await AsyncStorageHelper.getLoginEmail();

    Axios.get(`${GX_API_ENDPOINT}/coin/trade/user/fees/get`, {
      params: {email},
    })
      .then((resp) => {
        const feeDataResp = resp.data;

        setFeeData(feeDataResp.status ? feeDataResp.data[0] : {});
      })
      .catch((error) => console.log('Error on getting fees', error));
  };

  const getFees = (comboSymbol) => {
    return feeData ? feeData[comboSymbol] || 1 : 0;
  };

  return (
    <BottomSheetLayout
      isOpen={isSheetOpen && !isInstaCryptoFeeEditorHide}
      onClose={() => setIsSheetOpen(false)}
      reactToKeyboard={isInstaCryptoFeeEditorHide}>
      <View
        style={
          isSearchOpen
            ? {
                height: height,
                paddingTop: top + (StatusBar.currentHeight || 30),
                paddingBottom: bottom,
              }
            : {}
        }>
        {isSearchOpen || (
          <View style={styles.headerContainer}>
            <Image
              style={styles.headerImage}
              source={selectedApp.icon}
              resizeMode="contain"
            />
          </View>
        )}
        <View style={[styles.fragmentContainer, isSearchOpen && {flex: 1}]}>
          {currentView}
        </View>
      </View>
    </BottomSheetLayout>
  );
};

export default OTCBotsBottomSheet;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    // flex: 1,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    overflow: 'hidden',
  },
  fragmentContainer: {backgroundColor: 'white'},
  headerContainer: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 30,
  },
  headerImage: {
    height: 50,
    width: 200,
  },
  customActions: {
    flexDirection: 'row',
    paddingHorizontal: 30,
    marginTop: 20,
  },
  filledButton: {
    flex: 1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#186AB4',
  },
  filledText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: 'white',
  },
  outlineButton: {
    flex: 1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  outlineText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: '#186AB4',
  },
});
