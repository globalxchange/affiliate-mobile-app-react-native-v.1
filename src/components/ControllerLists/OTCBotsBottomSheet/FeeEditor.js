import React, {Component} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {percentageFormatter, formatterHelper} from '../../../utils';
import {WToast} from 'react-native-smart-tip';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';
import LoadingAnimation from '../../LoadingAnimation';
import CustomNumpadView from '../../CustomNumpadView';

class FeeEditor extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isPercentageFocus: true,
      percentageInput: '',
      currencyInput: '',
      isLoading: false,
    };
    this.setPercentageInput = this.setPercentageInput.bind(this);
    this.setCurrencyInput = this.setCurrencyInput.bind(this);
    this.onPercentFocus = this.onPercentFocus.bind(this);
  }

  onPercentFocus = (isPercentageFocus) => {
    this.setState({isPercentageFocus});
  };

  setPercentageInput = (percentageInput) => {
    this.setState({percentageInput});
  };

  setCurrencyInput = (currencyInput) => {
    this.setState({currencyInput});
  };

  requestFeeChange = async () => {
    const {percentageInput} = this.state;
    const {feeViewData, onClose, isMassEdit, pairData} = this.props;

    const percentage = parseFloat(percentageInput);

    if (isNaN(percentage)) {
      return WToast.show({
        data: 'Please Input A Valid Percentage Value',
        position: WToast.position.TOP,
      });
    }

    this.setState({isLoading: true});

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    if (!isMassEdit) {
      const postData = {
        email,
        token,
        buy: feeViewData.to,
        sell: feeViewData?.from,
        percentage,
        default: false,
      };

      Axios.post(`${GX_API_ENDPOINT}/coin/trade/user/fees/set`, postData)
        .then((resp) => {
          const {data} = resp;
          // console.log('Data', data);

          if (data.status) {
            WToast.show({
              data: `Fees updated for ${feeViewData.comboSymbol}`,
              position: WToast.position.TOP,
            });
            onClose();
          } else {
            WToast.show({
              data: data.message,
              position: WToast.position.TOP,
            });
          }
          this.setState({isLoading: false});
        })
        .catch((error) => {
          this.setState({isLoading: false});
          console.log('Error on updating fees', error);
        });
    } else {
      const parsedPair = pairData?.map((item) => ({
        buy: item.to,
        sell: item.from,
      }));

      const postData = {
        email,
        token,
        common_percentage: percentage,
        pairs_data: parsedPair || [],
      };
      // console.log('PostData', postData);

      Axios.post(
        `${GX_API_ENDPOINT}/coin/trade/user/fees/multiple/set`,
        postData,
      )
        .then((resp) => {
          const {data} = resp;
          // console.log('Data', data);

          if (data.status) {
            WToast.show({
              data: `Fees updated for ${feeViewData.from}`,
              position: WToast.position.TOP,
            });
            onClose();
          } else {
            WToast.show({
              data: data.message,
              position: WToast.position.TOP,
            });
          }
          this.setState({isLoading: false});
        })
        .catch((error) => {
          this.setState({isLoading: false});
          console.log('Error on updating fees', error);
        });
    }
  };

  render() {
    const {feeViewData, isMassEdit} = this.props;

    const {
      isPercentageFocus,
      percentageInput,
      currencyInput,
      isLoading,
    } = this.state;

    return (
      <View style={[styles.container]}>
        <View style={styles.viewContainer}>
          <Text style={styles.desc}>
            {isMassEdit
              ? `Setting Fees For All Trades Where The User Is Selling ${feeViewData?.from}`
              : `Set Your New Fees For ${feeViewData.comboText}`}
          </Text>
          <View style={styles.inputForm}>
            <View style={styles.inputContainer}>
              <TouchableOpacity
                style={[styles.input]}
                onPress={() => this.onPercentFocus(true)}>
                <Text
                  style={[styles.text, isPercentageFocus && styles.focused]}>
                  {percentageInput
                    ? isNaN(parseFloat(percentageInput))
                      ? percentageInput
                      : percentageFormatter.format(parseFloat(percentageInput))
                    : percentageFormatter.format('0')}
                </Text>
              </TouchableOpacity>
              <Text
                style={[
                  styles.cryptoName,
                  isPercentageFocus && styles.focusedText,
                ]}>
                Percentage
              </Text>
            </View>
            <View style={styles.divider} />
            <View style={styles.inputContainer}>
              <TouchableOpacity
                style={[styles.input]}
                onPress={() => this.onPercentFocus(false)}>
                <Text
                  style={[styles.text, !isPercentageFocus && styles.focused]}>
                  {currencyInput
                    ? isNaN(parseFloat(currencyInput))
                      ? currencyInput
                      : formatterHelper(
                          parseFloat(currencyInput),
                          feeViewData?.from,
                        )
                    : formatterHelper('0', feeViewData?.from)}
                </Text>
              </TouchableOpacity>
              <Text
                style={[
                  styles.cryptoName,
                  !isPercentageFocus && styles.focusedText,
                ]}>
                {feeViewData?.from}
              </Text>
            </View>
          </View>
        </View>
        <CustomNumpadView
          currentText={isPercentageFocus ? percentageInput : currencyInput}
          updatedCallback={
            isPercentageFocus ? this.setPercentageInput : this.setCurrencyInput
          }
        />
        <TouchableOpacity
          style={styles.continueButton}
          onPress={this.requestFeeChange}>
          <Text style={styles.buttonText}>Confirm Change Of Fees</Text>
        </TouchableOpacity>
        {isLoading && (
          <View style={[styles.loadingContainer, StyleSheet.absoluteFill]}>
            <LoadingAnimation />
          </View>
        )}
      </View>
    );
  }
}

export default FeeEditor;

const styles = StyleSheet.create({
  container: {},
  viewContainer: {
    padding: 35,
    paddingTop: 0,
  },
  headerImage: {
    height: 40,
    width: 200,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  desc: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat-SemiBold',
    marginTop: 20,
    marginBottom: 10,
    fontSize: 13,
    textAlign: 'center',
  },
  inputForm: {
    marginTop: 20,
  },
  inputContainer: {flexDirection: 'row', alignItems: 'center'},
  input: {
    flexGrow: 1,
    width: 0,
    color: '#041939',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    paddingHorizontal: 10,
  },
  text: {
    color: '#041939',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  focused: {
    color: '#08152D',
  },
  cryptoName: {
    marginLeft: 10,
    color: '#041939',
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
  },
  focusedText: {
    color: '#08152D',
  },
  divider: {
    backgroundColor: '#08152D',
    height: 1,
    marginVertical: 5,
  },
  continueButton: {
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
  },
  loadingContainer: {
    zIndex: 5,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
});
