import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../../../configs/ThemeData';
import ProfileAvatar from '../../ProfileAvatar';

const UserItem = ({avatar, name, email, onPress}) => {
  return (
    <TouchableOpacity style={styles.userItem} onPress={onPress}>
      <ProfileAvatar name={name} avatar={avatar} size={35} />
      <View style={{flex: 1, marginLeft: 10}}>
        <Text style={styles.userName}>{name}</Text>
        <Text style={styles.userEmail}>{email}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default UserItem;

const styles = StyleSheet.create({
  userItem: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 15,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginTop: 15,
    alignItems: 'center',
  },
  userName: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 16,
  },
  userEmail: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 10,
    color: '#9A9A9A',
  },
});
