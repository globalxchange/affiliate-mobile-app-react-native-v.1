import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import ThemeData from '../../../configs/ThemeData';

const {height} = Dimensions.get('window');

const MainList = ({
  data,
  controllerItems,
  setSelectedControllerItem,
  selectedControllerItem,
  listItems,
  getFees,
  setIsSheetOpen,
  setFeeViewData,
  openMassEdit,
}) => {
  const {bottom} = useSafeAreaInsets();

  return (
    <View style={[styles.container]}>
      <View style={styles.viewContainer}>
        {/* <Text style={styles.desc}>
          All {data.title} Trading Pairs. Click On Them To Change Your Spread.
        </Text>
        <Text style={styles.note}>
          Your Default Spread For All Pairs Is 1.00%
        </Text> */}
        <View style={styles.listContainer}>
          <Text style={[styles.title]}>Exchange Fee on {data.title}</Text>
          <ScrollView
            horizontal
            showsHorizontalScrollIndicator={false}
            style={styles.controller}>
            {controllerItems.map((item, index) => (
              <TouchableOpacity
                key={index}
                onPress={() => setSelectedControllerItem(item)}
                style={[
                  styles.controlItem,
                  selectedControllerItem.coinSymbol === item.coinSymbol &&
                    styles.controlItemActive,
                ]}>
                <Image
                  source={{uri: item.image}}
                  style={styles.controlIcon}
                  resizeMode="contain"
                />
                <Text style={styles.controlText}>{item.coinSymbol}</Text>
              </TouchableOpacity>
            ))}
          </ScrollView>
          <ScrollView
            showsVerticalScrollIndicator={false}
            style={styles.itemsContainer}>
            {listItems.map((item, index) => (
              <TouchableOpacity
                key={index}
                style={[styles.item]}
                onPress={() => setFeeViewData(item)}>
                <Image
                  style={styles.itemIcon}
                  source={{uri: item.image}}
                  resizeMode="contain"
                />
                <Text style={styles.itemText}>{item.comboText}</Text>
                <Text style={styles.itemDiscount}>
                  {getFees(item.comboSymbol)}%
                </Text>
              </TouchableOpacity>
            ))}
          </ScrollView>
        </View>
      </View>
      <TouchableOpacity
        style={[
          styles.goToButton,
          {height: 55 + bottom, marginBottom: -bottom, paddingBottom: bottom},
        ]}
        onPress={openMassEdit}>
        <Text style={styles.gotoButtonText}>
          Update Fees For All {selectedControllerItem.coinSymbol} Pairs
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default MainList;

const styles = StyleSheet.create({
  container: {},
  viewContainer: {
    paddingHorizontal: 35,
    paddingBottom: 20,
  },
  headerImage: {
    height: 40,
    width: 200,
  },
  desc: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat',
    marginTop: 20,
    marginBottom: 5,
    fontSize: 13,
    textAlign: 'center',
  },
  note: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 13,
    marginBottom: 10,
    textAlign: 'center',
  },
  listContainer: {
    marginTop: 20,
  },
  title: {
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
    fontSize: 16,
    marginBottom: 25,
  },
  controller: {
    marginTop: 15,
  },
  controlItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 10,
    paddingVertical: 5,
    paddingHorizontal: 10,
    opacity: 0.5,
  },
  controlItemActive: {
    borderColor: '#9A9A9A',
    borderWidth: 1,
    opacity: 1,
  },
  controlIcon: {
    height: 18,
    width: 18,
  },
  controlText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#001D41',
    fontSize: 12,
    marginLeft: 6,
  },
  itemsContainer: {
    marginTop: 20,
    height: height * 0.35,
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    paddingVertical: 12,
    paddingHorizontal: 20,
    marginTop: 10,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  itemIcon: {
    height: 25,
    width: 25,
  },
  itemText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#001D41',
    paddingHorizontal: 10,
    flex: 1,
    fontSize: 13,
  },
  itemDiscount: {
    fontFamily: 'Montserrat',
    color: '#001D41',
    opacity: 0.5,
  },
  goToButton: {
    backgroundColor: '#08152D',
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
  },
  gotoButtonText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
    fontSize: 15,
  },
});
