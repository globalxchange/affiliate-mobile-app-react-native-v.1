import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import FeeEditor from './FeeEditor';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';
import {formatterHelper} from '../../../utils';
import ThemeData from '../../../configs/ThemeData';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

const FeesView = ({
  setIsSheetOpen,
  feeViewData,
  onClose,
  getFees,
  isEditor,
  isMassEdit,
  openMassEdit,
  pairData,
}) => {
  const {bottom} = useSafeAreaInsets();

  const [isFeeEditOpen, setIsFeeEditOpen] = useState(false);
  const [marketPrice, setMarketPrice] = useState();
  const [feeAmount, setFeeAmount] = useState();
  const [clientPrice, setClientPrice] = useState();

  useEffect(() => {
    if (isMassEdit) {
      setIsFeeEditOpen(true);
    } else {
      setIsFeeEditOpen(false);
    }
  }, [isMassEdit]);

  useEffect(() => {
    if (!isMassEdit) {
      Axios.get(`${GX_API_ENDPOINT}/forex/convert`, {
        params: {buy: feeViewData?.to, from: feeViewData?.from},
      })
        .then((resp) => {
          const {data} = resp;
          const price =
            data[
              `${feeViewData?.to.toLowerCase()}_${feeViewData?.from.toLowerCase()}`
            ] || 0;
          setMarketPrice(price);

          const feePercentage = getFees(feeViewData?.comboSymbol);
          const fee = (feePercentage * price) / 100;
          setFeeAmount(fee);
          setClientPrice(price + fee);
        })
        .catch((error) => {
          console.log('Error on getting conversation rate', error);
        });
    }
  }, [feeViewData, getFees, isMassEdit]);

  if (isFeeEditOpen) {
    return (
      <FeeEditor
        onClose={onClose}
        feeViewData={feeViewData}
        isMassEdit={isMassEdit}
        pairData={pairData}
      />
    );
  }

  return (
    <View style={[styles.container]}>
      <View style={styles.viewContainer}>
        {/* <Text style={styles.desc}>
          Right Now All Your Direct Customers Will Experiencing The Following
          Pricing Model When Converting USD Into BTC.
        </Text> */}
        <View style={styles.viewGroup}>
          <Text style={styles.groupTitle}>Market Price With Broker Fees</Text>
          <View style={styles.groupView}>
            <Image
              source={{uri: feeViewData?.image}}
              style={styles.icon}
              resizeMode="contain"
            />
            <Text style={styles.name}>{feeViewData?.comboText}</Text>
            <Text style={styles.value}>
              {marketPrice
                ? `${formatterHelper(marketPrice, feeViewData?.from)} ${
                    feeViewData?.from
                  }`
                : ''}
            </Text>
          </View>
        </View>
        <View style={styles.viewGroup}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <Text style={styles.groupTitle}>Your Fees</Text>
            {isEditor && (
              <TouchableOpacity onPress={() => setIsFeeEditOpen(true)}>
                <Text style={styles.changeFeeButtonText}>Change Your Fees</Text>
              </TouchableOpacity>
            )}
          </View>
          <View style={styles.groupView}>
            <Text style={styles.name}>
              {getFees(feeViewData?.comboSymbol)}%
            </Text>
            <Text style={styles.value}>
              {feeAmount
                ? `${formatterHelper(feeAmount, feeViewData?.from)} ${
                    feeViewData?.from
                  }`
                : ''}
            </Text>
          </View>
        </View>
        <View style={styles.viewGroup}>
          <Text style={styles.groupTitle}>What Your Clients See</Text>
          <View style={styles.groupView}>
            <Image
              source={{uri: feeViewData?.image}}
              style={styles.icon}
              resizeMode="contain"
            />
            <Text style={styles.name}>{feeViewData?.comboText}</Text>
            <Text style={styles.value}>
              {clientPrice
                ? `${formatterHelper(clientPrice, feeViewData?.from)} ${
                    feeViewData?.from
                  }`
                : ''}
            </Text>
          </View>
        </View>
      </View>
      <TouchableOpacity
        style={[
          styles.goToButton,
          {height: 55 + bottom, marginBottom: -bottom, paddingBottom: bottom},
        ]}
        onPress={openMassEdit}>
        <Text style={styles.gotoButtonText}>
          Update Fees For All {feeViewData?.from} Pairs
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default FeesView;

const styles = StyleSheet.create({
  container: {},
  viewContainer: {
    paddingHorizontal: 35,
    paddingBottom: 20,
  },
  desc: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat',
    marginTop: 20,
    marginBottom: 10,
    fontSize: 13,
    textAlign: 'center',
  },
  goToButton: {
    backgroundColor: '#08152D',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  gotoButtonText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
    fontSize: 15,
  },
  viewGroup: {
    marginTop: 30,
  },
  groupTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat',
  },
  groupView: {
    flexDirection: 'row',
    marginTop: 10,
    backgroundColor: 'white',
    paddingVertical: 15,
    paddingHorizontal: 20,
    borderRadius: 5,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    alignItems: 'center',
  },
  icon: {
    width: 22,
    height: 22,
  },
  name: {
    flex: 1,
    paddingHorizontal: 10,
    fontFamily: 'Montserrat-SemiBold',
    color: '#001D41',
  },
  value: {
    color: '#001D41',
    fontFamily: 'Montserrat-SemiBold',
    opacity: 0.5,
  },
  changeFeeButtonText: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    textDecorationLine: 'underline',
  },
});
