import axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {GX_API_ENDPOINT} from '../../../configs';
import ThemeData from '../../../configs/ThemeData';
import PopupLayout from '../../../layouts/PopupLayout';
import SearchLayout from '../../../layouts/SearchLayout';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import FilledButton from '../../FilledButton';
import LoadingAnimation from '../../LoadingAnimation';
import OutlinedButton from '../../OutlinedButton';
import ProfileAvatar from '../../ProfileAvatar';
import UserItem from './UserItem';
import {useNavigation} from '@react-navigation/native';
import {getAssetData, percentageFormatter} from '../../../utils';
import {AppContext} from '../../../contexts/AppContextProvider';

const {height} = Dimensions.get('window');

const CustomFeeList = ({isSearchOpen, setIsSearchOpen, onClose, onHide}) => {
  const {navigate} = useNavigation();

  const {bottom} = useSafeAreaInsets();

  const {cryptoTableData, isInstaCryptoFeeEditorHide} = useContext(AppContext);

  const [feeEditedUsers, setFeeEditedUsers] = useState();
  const [isShowAllUsers, setIsShowAllUsers] = useState(false);
  const [allDirects, setAllDirects] = useState();
  const [searchInput, setSearchInput] = useState('');
  const [isUserPopupOpen, setIsUserPopupOpen] = useState(false);
  const [selectedUser, setSelectedUser] = useState();

  useEffect(() => {
    (async () => {
      const email = await AsyncStorageHelper.getLoginEmail();

      axios
        .get(
          `${GX_API_ENDPOINT}/coin/vault/service/get/fees/user/per/customer`,
          {params: {email, customer_data: true}},
        )
        .then(({data}) => {
          console.log('Data', data);

          const feeData = data?.feesData || [];

          const filteredList = feeData.map((item) => ({
            ...item,
            ...item.customer_data,
          }));

          // console.log('filteredList', filteredList);

          setFeeEditedUsers(filteredList);
        })
        .catch((error) => {
          console.log('Error getting custom fees', error);
        });

      axios
        .get(`${GX_API_ENDPOINT}/brokerage/get/broker/users/by/levels`, {
          params: {email},
        })
        .then(({data}) => {
          // console.log('Direct Users', data);

          const directLevel = data.levels ? data.levels[0] : '';
          setAllDirects(directLevel?.users || []);
        })
        .catch((error) => {
          console.log('Error getting directs', error);
        });
    })();
  }, []);

  const onUserSelected = (email, userObj) => {
    setSelectedUser(userObj);
    setIsSearchOpen(false);
    setIsUserPopupOpen(true);
  };

  if (isSearchOpen) {
    return (
      <SearchLayout
        value={searchInput}
        setValue={setSearchInput}
        onBack={() => setIsSearchOpen(false)}
        list={isShowAllUsers ? allDirects : feeEditedUsers}
        disableFilter
        placeholder={
          isShowAllUsers ? 'Search Direct User' : 'Search Users With Custom Fee'
        }
        showUserList
        onSubmit={onUserSelected}
      />
    );
  }

  return (
    <View style={styles.container}>
      <Text style={[styles.title]}>
        Users For Whom You Have Already Set Custom Fees
      </Text>
      <TouchableOpacity
        style={[styles.searchContainer]}
        onPress={() => setIsSearchOpen(true)}>
        <Image
          source={require('../../../assets/search-icon.png')}
          resizeMode="contain"
          style={[styles.searchIcon]}
        />
        <Text style={styles.searchText}>Search Your Direct Users</Text>
      </TouchableOpacity>
      <View style={[styles.listContainer]}>
        {(isShowAllUsers ? allDirects : feeEditedUsers) ? (
          <FlatList
            data={isShowAllUsers ? allDirects : feeEditedUsers}
            keyExtractor={(item) => item._id || item.email}
            showsVerticalScrollIndicator={false}
            renderItem={({item}) => (
              <UserItem
                avatar={item?.profile_img}
                email={item?.email}
                name={item?.name}
                onPress={() => onUserSelected(null, item)}
              />
            )}
          />
        ) : (
          <View style={{flex: 1, justifyContent: 'center'}}>
            <LoadingAnimation />
          </View>
        )}
      </View>
      <TouchableOpacity
        style={[
          styles.toggleButton,
          {height: 50 + bottom, marginBottom: -bottom, paddingBottom: bottom},
        ]}
        onPress={() => setIsShowAllUsers(!isShowAllUsers)}>
        <Text style={styles.toggleText}>
          {isShowAllUsers ? 'See Custom Fee Directs' : 'See All Directs'}
        </Text>
      </TouchableOpacity>
      <PopupLayout
        noScrollView
        isOpen={isUserPopupOpen}
        noHeader
        onClose={() => setIsUserPopupOpen(false)}>
        <View style={styles.userHeader}>
          <ProfileAvatar
            name={selectedUser?.name}
            avatar={selectedUser?.profile_img}
            size={50}
          />
          <View style={{flex: 1, marginLeft: 10}}>
            <Text style={styles.userName}>{selectedUser?.name}</Text>
            <Text
              numberOfLines={1}
              adjustsFontSizeToFit
              style={styles?.userEmail}>
              {selectedUser?.email}
            </Text>
          </View>
        </View>
        <View style={styles.defaultFeeContainer}>
          <Text style={styles.defaultFeeLabel}>Default Fee</Text>
          <Text style={styles.defaultFee}>
            {percentageFormatter.format(selectedUser?.default_fee || 0)}%
          </Text>
        </View>
        <View style={styles.pairsContainer}>
          {selectedUser?.fees && selectedUser?.fees.length > 0 ? (
            <FlatList
              data={selectedUser.fees}
              keyExtractor={(item) => item.field_key}
              showsVerticalScrollIndicator={false}
              renderItem={({item}) => (
                <TouchableOpacity style={styles.pairItem}>
                  <Image
                    source={{
                      uri: getAssetData(item.to, cryptoTableData)?.coinImage,
                    }}
                    style={styles.tickerImage}
                    resizeMode="contain"
                  />
                  <Text style={styles.tickerName}>
                    {item.to}/{item.from}
                  </Text>
                  <Text style={styles.pairFee}>
                    {percentageFormatter.format(item.fee)}%
                  </Text>
                </TouchableOpacity>
              )}
            />
          ) : (
            <Text style={styles.emptyText}>
              You Have Not Added Any Custom Fee Pairs For {selectedUser?.name}
            </Text>
          )}
        </View>
        <View style={styles.popupActions}>
          <OutlinedButton
            title="Update All Pairs"
            color={ThemeData.APP_MAIN_COLOR}
            style={{borderRadius: 0, height: 50}}
            disabled={!selectedUser?.fees || !selectedUser?.fees.length}
            onPress={() => {
              onHide();
              navigate('InstaCryptoCustomPairFees', {
                user: selectedUser,
                isDefault: true,
              });
            }}
          />
          <FilledButton
            title="Add New Pair"
            style={{height: 50}}
            onPress={() => {
              onHide();
              navigate('InstaCryptoCustomPairFees', {user: selectedUser});
            }}
          />
        </View>
      </PopupLayout>
    </View>
  );
};

export default CustomFeeList;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 35,
  },
  title: {
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
    fontSize: 16,
    marginBottom: 25,
    marginTop: 20,
  },
  searchContainer: {
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 15,
    alignItems: 'center',
  },
  searchIcon: {
    width: 20,
    height: 20,
  },
  searchText: {
    flex: 1,
    marginLeft: 10,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    color: '#9A9A9A',
  },
  listContainer: {
    height: height * 0.45,
  },
  toggleButton: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    marginHorizontal: -35,
  },
  toggleText: {
    color: 'white',
    textAlign: 'center',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  userHeader: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  userName: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 18,
    color: ThemeData.APP_MAIN_COLOR,
    marginBottom: 3,
  },
  userEmail: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 13,
    color: '#9A9A9A',
  },
  pairsContainer: {
    flex: 1,
    justifyContent: 'center',
    marginTop: 20,
  },
  popupActions: {
    flexDirection: 'row',
    marginHorizontal: -30,
    marginBottom: -30,
    alignItems: 'center',
  },
  emptyText: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  pairItem: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 15,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
  },
  tickerImage: {
    width: 20,
    height: 20,
    borderRadius: 10,
    marginRight: 8,
  },
  tickerName: {
    flex: 1,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 13,
  },
  pairFee: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 13,
  },
  defaultFeeContainer: {
    flexDirection: 'row',
    borderRadius: 10,
    overflow: 'hidden',
    marginTop: 25,
    marginBottom: 10,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  defaultFeeLabel: {
    flex: 1,
    color: 'white',
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    paddingVertical: 10,
    fontSize: 13,
  },
  defaultFee: {
    flex: 1,
    textAlign: 'right',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    paddingVertical: 10,
    paddingRight: 20,
    fontSize: 13,
  },
});
