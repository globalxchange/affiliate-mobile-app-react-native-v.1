import Axios from 'axios';
import React from 'react';
import {FlatList, Image, StyleSheet, TouchableOpacity} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import BrandItem from './BrandItem';

const BrokerAppList = ({onItemSelected}) => {
  return (
    <FlatList
      showsVerticalScrollIndicator={false}
      data={BROKER_LIST}
      keyExtractor={(item) => item.title}
      renderItem={({item}) => (
        <BrandItem
          disabled={item.disabled}
          icon={item.icon}
          onPress={() => onItemSelected(item)}
        />
      )}
    />
  );
};

export default BrokerAppList;

const styles = StyleSheet.create({
  itemContainer: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    padding: 20,
    marginBottom: 15,
  },
  itemImage: {
    width: 110,
    height: 50,
  },
});

const BROKER_LIST = [
  {
    title: 'AffiliateApp',
    icon: require('../../assets/affliate-app-logo-dark.png'),
    color: '#08152D',
    // disabled: true,
  },
];
