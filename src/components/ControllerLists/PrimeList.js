/* eslint-disable react-native/no-inline-styles */
import {useNavigation} from '@react-navigation/native';
import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import {GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import BrandItem from './BrandItem';
import BrandItemSkeleton from './BrandItemSkeleton';

const PrimeList = ({onItemSelected}) => {
  const {navigate} = useNavigation();

  const [isPrimeMember, setIsPrimeMember] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isNavigationLoading, setIsNavigationLoading] = useState(false);

  useEffect(() => {
    (async () => {
      const email = await AsyncStorageHelper.getLoginEmail();

      if (email) {
        setIsLoading(true);

        Axios.get(`${GX_API_ENDPOINT}/coin/vault/user/license/get`, {
          params: {email},
        })
          .then(({data}) => {
            const licenses = data?.licenses || [];

            let primeMember = false;
            licenses.forEach((item) => {
              if (
                item.product_code === 'GXPA12' ||
                item.product_code === 'GXPA12-U'
              ) {
                if (item.license_status === 'active') {
                  primeMember = true;
                }
              }
            });
            setIsPrimeMember(primeMember);
          })
          .catch((error) => {
            setIsPrimeMember(false);
          })
          .finally(() => {
            setIsLoading(false);
          });
      }
    })();
  }, []);

  const onUpgradeClick = () => {
    setIsNavigationLoading(true);
    Axios.get('https://teller2.apimachine.com/admin/allBankers')
      .then((resp) => {
        const {data} = resp;

        const brands = data.data || [];

        const gxBrand = brands.find((x) => (x.bankerTag = 'Global X Change'));

        setIsNavigationLoading(false);
        if (gxBrand) {
          navigate('Brands', {
            screen: 'Brands',
            params: {selectedItem: gxBrand},
          });
        }
      })
      .catch((error) => {
        setIsNavigationLoading(false);
      });
  };

  if (isLoading || isNavigationLoading) {
    return (
      <View style={styles.loadingContainer}>
        {/* <LoadingAnimation />
        {isNavigationLoading || (
          <Text style={styles.loadingText}>
            One Moment While We Check Your Prime Eligibility
          </Text>
        )} */}
        {Array(5)
          .fill(0)
          .map((_, index) => (
            <BrandItemSkeleton key={index} />
          ))}
      </View>
    );
  }

  if (!isPrimeMember) {
    return (
      <View style={styles.container}>
        <Text style={styles.loadingText}>
          It Looks Like You Are Not A Prime Broker. Please Upgrade Your License
        </Text>
        <Text onPress={onUpgradeClick} style={styles.hearButton}>
          Here
        </Text>
      </View>
    );
  }

  return (
    <FlatList
      showsVerticalScrollIndicator={false}
      data={PRIME_LIST}
      keyExtractor={(item) => item.title}
      renderItem={({item}) => (
        <BrandItem
          disabled={item.disabled}
          icon={item.icon}
          onPress={() => onItemSelected(item)}
        />
      )}
    />
  );
};

export default PrimeList;

const styles = StyleSheet.create({
  itemContainer: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    padding: 20,
    marginBottom: 15,
  },
  itemImage: {
    width: 110,
    height: 50,
  },
  loadingContainer: {
    // paddingHorizontal: 20,
  },
  loadingText: {
    color: '#08152D',
    fontFamily: ThemeData.FONT_MEDIUM,
    textAlign: 'center',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  hearButton: {
    color: '#08152D',
    fontFamily: ThemeData.FONT_BOLD,
    textAlign: 'center',
    textDecorationLine: 'underline',
  },
});

const PRIME_LIST = [
  {
    title: 'Vault',
    icon: require('../../assets/vault-logo.png'),
    color: '#186AB4',
    mtToolName: 'Exchange Fees',
  },
  {
    title: 'MoneyMarket',
    icon: require('../../assets/money-markets-full-icon.png'),
    color: '#464B4E',
    whiteLogo: require('../../assets/assets-io-white-icon.png'),
    mtToolName: 'Interest Fees',
  },
  {
    title: 'Agency',
    icon: require('../../assets/agency-icon.png'),
    color: '#182542',
    disabled: true,
  },
  {
    title: 'SpendCrypto',
    icon: require('../../assets/spend-icon-full.png'),
    color: '#E87D6D',
    disabled: true,
  },
  {
    title: 'BlockCheck',
    icon: require('../../assets/block-check-icon-blue.png'),
    color: '#186AB4',
    disabled: true,
  },
  {
    title: 'CryptoAssistance',
    icon: require('../../assets/crypto-assistance-icon.png'),
    color: '#186AB4',
    disabled: true,
  },
];
