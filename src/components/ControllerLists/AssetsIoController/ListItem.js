import React, {useContext} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import ThemeData from '../../../configs/ThemeData';
import {AppContext} from '../../../contexts/AppContextProvider';
import {getAssetData, percentageFormatter} from '../../../utils';

const ListItem = ({item, isLiquid, onPress}) => {
  const {walletCoinData} = useContext(AppContext);

  const coin = getAssetData(item.coin, walletCoinData) || '';

  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.listItem}>
        <Image
          style={styles.actionIcon}
          resizeMode="contain"
          source={{uri: coin.coinImage}}
        />
        <Text style={styles.itemName}>{item.coin}</Text>
        <Text style={styles.itemFee}>
          {percentageFormatter.format(
            (isLiquid ? item.liquid : item.iced) || 0,
          )}
          %
        </Text>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default ListItem;

const styles = StyleSheet.create({
  listItem: {
    flexDirection: 'row',
    paddingHorizontal: 25,
    paddingVertical: 20,
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginBottom: 15,
  },
  actionIcon: {
    width: 22,
    height: 22,
  },
  itemName: {
    flex: 1,
    marginLeft: 10,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: '#001D41',
  },
  itemFee: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: '#001D41',
    opacity: 0.5,
  },
});
