import React from 'react';
import {Dimensions, FlatList, StyleSheet, Text, View} from 'react-native';
import ThemeData from '../../../configs/ThemeData';
import ListItem from './ListItem';

const {height} = Dimensions.get('window');

const FeeList = ({listData, isEditor, isLiquid, openOverView}) => {
  return (
    <View>
      <Text style={[styles.headerText]}>
        Interest Fees On {isLiquid ? 'Liquid' : 'Bond'} Earnings
      </Text>
      <FlatList
        style={styles.list}
        data={listData}
        keyExtractor={(item) => item.coin}
        showsVerticalScrollIndicator={false}
        renderItem={({item}) => (
          <ListItem
            onPress={isEditor ? () => openOverView(item) : null}
            item={item}
            isLiquid={isLiquid}
          />
        )}
      />
    </View>
  );
};

export default FeeList;

const styles = StyleSheet.create({
  list: {
    height: height * 0.45,
    marginTop: 10,
    // paddingTop: 10,
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 16,
    marginVertical: 30,
    color: '#08152D',
  },
});
