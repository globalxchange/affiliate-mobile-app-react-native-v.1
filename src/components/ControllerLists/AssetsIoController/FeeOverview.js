/* eslint-disable react-native/no-inline-styles */
import Axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {GX_API_ENDPOINT} from '../../../configs';
import ThemeData from '../../../configs/ThemeData';
import {AppContext} from '../../../contexts/AppContextProvider';
import PopupLayout from '../../../layouts/PopupLayout';
import {getAssetData, percentageFormatter} from '../../../utils';
import LoadingAnimation from '../../LoadingAnimation';

const FeeOverview = ({openEditor, selectedFee, isLiquid}) => {
  const {walletCoinData} = useContext(AppContext);

  const [selectedFrequency, setSelectedFrequency] = useState(FREQUENCIES[0]);
  const [showFrequencyPopup, setShowFrequencyPopup] = useState(false);
  const [grossInterestFee, setGrossInterestFee] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    Axios.get(`${GX_API_ENDPOINT}/coin/iced/get/liquid/interest`, {
      params: {coin: selectedFee.coin},
    })
      .then(({data}) => {
        // console.log('Data', data);

        setGrossInterestFee(data?.interest_rates[0]?.interest_rate || 0);
      })
      .catch((error) => {
        setGrossInterestFee(0);
        console.log('Error getting feee', error);
      })
      .finally(() => setIsLoading(false));

    return () => {};
  }, [selectedFee]);

  const coin = getAssetData(selectedFee.coin, walletCoinData) || '';

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Image
          style={styles.coinImage}
          resizeMode="contain"
          source={{uri: coin?.coinImage}}
        />
        <Text style={styles.headerText}>
          {coin?.coinName} {isLiquid ? 'Liquid' : 'Bond'} Earnings
        </Text>
      </View>
      {isLoading ? (
        <View style={{paddingVertical: 90}}>
          <LoadingAnimation />
        </View>
      ) : (
        <>
          <View style={styles.pickersContainer}>
            <TouchableOpacity style={styles.picker}>
              <Text style={styles.pickerText}>Liquid Earnings</Text>
              <Image
                style={styles.pickerIcon}
                resizeMode="contain"
                source={require('../../../assets/back-solid-icon.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setShowFrequencyPopup(true)}
              style={[styles.picker, {marginLeft: 15}]}>
              <Text style={styles.pickerText}>{selectedFrequency.title}</Text>
              <Image
                style={styles.pickerIcon}
                resizeMode="contain"
                source={require('../../../assets/back-solid-icon.png')}
              />
            </TouchableOpacity>
            <PopupLayout
              autoHeight
              isOpen={showFrequencyPopup}
              headerTitle="Select Frequency"
              onClose={() => setShowFrequencyPopup(false)}>
              <View style={styles.frequencyList}>
                {FREQUENCIES.map((item, index) => (
                  <TouchableOpacity
                    key={item.title}
                    style={[
                      styles.frequencyItem,
                      index === FREQUENCIES.length - 1 && {
                        borderBottomWidth: 0,
                      },
                    ]}
                    onPress={() => {
                      setSelectedFrequency(item);
                      setShowFrequencyPopup(false);
                    }}>
                    <Text style={styles.frequencyItemText}>{item.title}</Text>
                  </TouchableOpacity>
                ))}
              </View>
            </PopupLayout>
          </View>
          <View style={styles.reviewForm}>
            <View style={styles.reviewItem}>
              <Text style={styles.reviewItemTitle}>Gross Interest Rate</Text>
              <Text style={styles.reviewItemValue}>
                {parseFloat(grossInterestFee * selectedFrequency.value).toFixed(
                  4,
                )}{' '}
                %
              </Text>
            </View>
            <View style={styles.reviewItem}>
              <Text style={styles.reviewItemTitle}>Your Fees</Text>
              <Text style={[styles.reviewItemValue, {color: '#D80027'}]}>
                {percentageFormatter.format(
                  isLiquid ? -selectedFee.liquid : -selectedFee?.iced,
                )}{' '}
                %
              </Text>
            </View>
            <View style={styles.reviewItem}>
              <Text style={styles.reviewItemTitle}>Net Interest Rate</Text>
              <Text style={styles.reviewItemValue}>
                {percentageFormatter.format(
                  (grossInterestFee -
                    (grossInterestFee * isLiquid
                      ? selectedFee.liquid
                      : selectedFee?.iced)) *
                    selectedFrequency.value,
                )}{' '}
                %
              </Text>
            </View>
          </View>
          <View style={styles.actionContainer}>
            <View style={[styles.actionOutlined, {opacity: 0.5}]}>
              <Text style={styles.actionOutlinedText}>Simulate</Text>
            </View>
            <TouchableOpacity onPress={openEditor} style={styles.actionFilled}>
              <Text style={styles.actionFilledText}>Change Fee</Text>
            </TouchableOpacity>
          </View>
        </>
      )}
    </View>
  );
};

export default FeeOverview;

const styles = StyleSheet.create({
  container: {},
  headerContainer: {
    marginVertical: 30,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  coinImage: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 18,
    color: '#464B4E',
  },
  pickersContainer: {
    flexDirection: 'row',
  },
  picker: {
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
  pickerText: {
    fontFamily: ThemeData.FONT_MEDIUM,
    color: '#464B4E',
    marginRight: 20,
    fontSize: 11,
  },
  pickerIcon: {
    width: 10,
    height: 10,
    transform: [{rotate: '270deg'}],
  },
  frequencyList: {
    marginVertical: -20,
  },
  frequencyItem: {
    paddingVertical: 20,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
  },
  frequencyItemText: {
    fontFamily: ThemeData.FONT_NORMAL,
  },
  reviewForm: {
    marginTop: 30,
  },
  reviewItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 25,
    paddingVertical: 25,
    marginBottom: 15,
  },
  reviewItemTitle: {
    color: '#464B4E',
    fontFamily: ThemeData.FONT_MEDIUM,
    fontSize: 12,
  },
  reviewItemValue: {
    color: '#464B4E',
    fontFamily: ThemeData.FONT_MEDIUM,
    fontSize: 12,
  },
  actionContainer: {
    flexDirection: 'row',
    marginTop: 20,
  },
  actionOutlined: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  actionOutlinedText: {
    color: '#464B4E',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 13,
  },
  actionFilled: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    backgroundColor: '#464B4E',
    marginLeft: 15,
  },
  actionFilledText: {
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 13,
  },
});

const FREQUENCIES = [
  {title: 'Daily', value: 1},
  {title: 'Weekly', value: 7},
  {title: 'Monthly', value: 30},
  {title: 'Quarterly', value: 90},
  {title: 'Annually', value: 365},
];
