import Axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {GX_API_ENDPOINT} from '../../../configs';
import ThemeData from '../../../configs/ThemeData';
import {AppContext} from '../../../contexts/AppContextProvider';
import BottomSheetLayout from '../../../layouts/BottomSheetLayout';
import {getAssetData} from '../../../utils';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import FeeEditor from './FeeEditor';
import FeeList from './FeeList';
import FeeOverview from './FeeOverview';

const AssetsIoController = ({
  isOpen,
  setIsOpen,
  selectedApp,
  isLiquid,
  isEditor,
}) => {
  const {walletCoinData} = useContext(AppContext);

  const [listData, setListData] = useState();
  const [activeView, setActiveView] = useState();
  const [selectedFee, setSelectedFee] = useState('');

  useEffect(() => {
    getListData();
  }, []);

  useEffect(() => {
    if (listData && selectedFee) {
      const updatedFee = listData.find((x) => x.coin === selectedFee.coin);

      if (updatedFee) {
        setSelectedFee(updatedFee);
      }
    }
  }, [listData]);

  useEffect(() => {
    if (!isOpen) {
      setActiveView();
      setSelectedFee('');
    }
  }, [isOpen]);

  const getListData = async () => {
    const email = await AsyncStorageHelper.getLoginEmail();

    Axios.get(`${GX_API_ENDPOINT}/coin/iced/user/get/interest/fees`, {
      params: {email},
    })
      .then(({data}) => {
        // console.log('FeeList', data);
        setListData(data?.feesData?.fees || []);
      })
      .catch((error) => {});
  };

  const renderActiveView = () => {
    const coin = getAssetData(selectedFee.coin, walletCoinData) || '';

    switch (activeView) {
      case 'Overview':
        return (
          <FeeOverview
            selectedFee={selectedFee}
            openEditor={() => setActiveView('Editor')}
            isLiquid={isLiquid}
          />
        );

      case 'Editor':
        return (
          <FeeEditor
            coin={coin}
            selectedFee={selectedFee}
            isLiquid={isLiquid}
            onDone={() => {
              getListData();
              setActiveView('Overview');
            }}
          />
        );

      default:
        return (
          <FeeList
            isEditor={isEditor}
            isLiquid={isLiquid}
            listData={listData}
            openOverView={(feeItem) => {
              setSelectedFee(feeItem);
              setActiveView('Overview');
            }}
          />
        );
    }
  };

  return (
    <BottomSheetLayout isOpen={isOpen} onClose={() => setIsOpen(false)}>
      <View style={[styles.headerContainer]}>
        <Image
          style={styles.headerImage}
          source={selectedApp.icon}
          resizeMode="contain"
        />
      </View>
      <View style={styles.fragmentContainer}>{renderActiveView()}</View>
    </BottomSheetLayout>
  );
};

export default AssetsIoController;

const styles = StyleSheet.create({
  fragmentContainer: {backgroundColor: 'white', paddingHorizontal: 30},
  headerContainer: {
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 20,
    marginTop: 10,
  },
  headerImage: {
    height: 30,
    width: 180,
  },
  desc: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat',
    marginTop: 20,
    marginBottom: 5,
    fontSize: 13,
    textAlign: 'center',
  },
  actionContainer: {
    flexDirection: 'row',
  },
  actionItem: {
    flexDirection: 'row',
    borderColor: '#9A9A9A',
    borderWidth: 1,
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  actionIcon: {
    width: 20,
    height: 20,
  },
  actionText: {
    marginLeft: 10,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 11,
  },
  button: {
    marginHorizontal: -30,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 15,
  },
});
