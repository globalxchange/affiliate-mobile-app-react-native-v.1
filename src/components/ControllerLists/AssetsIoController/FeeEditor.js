import React, {Component} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {percentageFormatter, formatterHelper} from '../../../utils';
import {WToast} from 'react-native-smart-tip';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';
import LoadingAnimation from '../../LoadingAnimation';
import CustomNumpadView from '../../CustomNumpadView';
import ThemeData from '../../../configs/ThemeData';

class FeeEditor extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isPercentageFocus: true,
      percentageInput: '',
      currencyInput: '',
      isLoading: false,
    };
    this.setPercentageInput = this.setPercentageInput.bind(this);
    this.setCurrencyInput = this.setCurrencyInput.bind(this);
    this.onPercentFocus = this.onPercentFocus.bind(this);
  }

  onPercentFocus = (isPercentageFocus) => {
    this.setState({isPercentageFocus});
  };

  setPercentageInput = (percentageInput) => {
    this.setState({percentageInput});
  };

  setCurrencyInput = (currencyInput) => {
    this.setState({currencyInput});
  };

  requestFeeChange = async () => {
    const {percentageInput} = this.state;
    const {selectedFee, onDone, isLiquid} = this.props;

    const percentage = parseFloat(percentageInput);

    if (isNaN(percentage)) {
      return WToast.show({
        data: 'Please Input A Valid Percentage Value',
        position: WToast.position.TOP,
      });
    }

    this.setState({isLoading: true});

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    let postData = {
      email,
      token,
    };

    if (isLiquid) {
      postData = {
        ...postData,
        interest_fee_data: [
          {
            coin: selectedFee.coin,
            liquid: percentage,
            iced: selectedFee.iced,
          },
        ],
      };
    } else {
      postData = {
        ...postData,
        interest_fee_data: [
          {
            coin: selectedFee.coin,
            iced: percentage,
            liquid: selectedFee.liquid,
          },
        ],
      };
    }

    // console.log('PostData', postData);

    Axios.post(`${GX_API_ENDPOINT}/coin/iced/user/set/interest/fees`, postData)
      .then((resp) => {
        const {data} = resp;
        // console.log('Data', data);

        if (data.status) {
          WToast.show({
            data: `Fees updated for ${selectedFee.coin}`,
            position: WToast.position.TOP,
          });
          onDone();
        } else {
          WToast.show({
            data: data.message,
            position: WToast.position.TOP,
          });
        }
        this.setState({isLoading: false});
      })
      .catch((error) => {
        this.setState({isLoading: false});
        console.log('Error on updating fees', error);
      });
  };

  render() {
    const {coin, selectedFee, isLiquid, onDone} = this.props;

    const {
      isPercentageFocus,
      percentageInput,
      currencyInput,
      isLoading,
    } = this.state;

    return (
      <View style={[styles.container]}>
        <View style={styles.viewContainer}>
          <View style={styles.headerContainer}>
            <Image
              style={styles.coinImage}
              resizeMode="contain"
              source={{uri: coin?.coinImage}}
            />
            <Text style={styles.headerText}>
              {coin?.coinName} {isLiquid ? 'Liquid' : 'Bond'} Earnings
            </Text>
          </View>
          <View style={styles.inputForm}>
            <View style={styles.inputContainer}>
              <Text
                style={[
                  styles.cryptoName,
                  isPercentageFocus && styles.focusedText,
                ]}>
                New Fees
              </Text>
              <TouchableOpacity
                style={[styles.input]}
                onPress={() => this.onPercentFocus(true)}>
                <Text
                  style={[styles.text, isPercentageFocus && styles.focused]}>
                  {percentageInput
                    ? isNaN(parseFloat(percentageInput))
                      ? percentageInput
                      : percentageFormatter.format(parseFloat(percentageInput))
                    : percentageFormatter.format('0')}{' '}
                  %
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.divider} />
          </View>
        </View>
        {
          <View style={styles.keypadContainer}>
            <CustomNumpadView
              currentText={isPercentageFocus ? percentageInput : currencyInput}
              updatedCallback={
                isPercentageFocus
                  ? this.setPercentageInput
                  : this.setCurrencyInput
              }
            />
          </View>
        }
        <TouchableOpacity
          style={styles.continueButton}
          onPress={this.requestFeeChange}>
          <Text style={styles.buttonText}>Confirm Change Of Fees</Text>
        </TouchableOpacity>
        {isLoading && (
          <View style={[styles.loadingContainer]}>
            <LoadingAnimation />
          </View>
        )}
      </View>
    );
  }
}

export default FeeEditor;

const styles = StyleSheet.create({
  container: {},
  viewContainer: {},
  headerContainer: {
    marginVertical: 30,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  coinImage: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 18,
    color: '#464B4E',
  },
  inputForm: {
    marginTop: 10,
    marginBottom: 30,
  },
  inputContainer: {flexDirection: 'row', alignItems: 'center'},
  input: {
    color: '#041939',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    paddingHorizontal: 10,
  },
  text: {
    color: '#041939',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  focused: {
    color: '#464B4E',
  },
  cryptoName: {
    marginLeft: 10,
    color: '#041939',
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
    flex: 1,
  },
  focusedText: {
    color: '#464B4E',
  },
  divider: {
    backgroundColor: '#464B4E',
    height: 1,
    marginVertical: 5,
  },
  continueButton: {
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: '#464B4E',
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
  },
  loadingContainer: {
    zIndex: 5,
    position: 'absolute',
    backgroundColor: 'white',
    justifyContent: 'center',
    left: -30,
    right: -30,
    top: 0,
    bottom: 0,
  },
  keypadContainer: {
    marginHorizontal: -30,
  },
});
