/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';

const BrandItem = ({onPress, disabled, icon}) => {
  return (
    <TouchableOpacity
      disabled={disabled}
      onPress={onPress}
      style={styles.itemContainer}>
      <Image source={icon} resizeMode="contain" style={styles.itemImage} />
      <View
        style={[
          styles.sideRibbon,
          {backgroundColor: disabled ? '#FF2D55' : '#08152D'},
        ]}
      />
    </TouchableOpacity>
  );
};

export default BrandItem;

const styles = StyleSheet.create({
  itemContainer: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    padding: 20,
    marginBottom: 15,
  },
  itemImage: {
    width: 110,
    height: 50,
  },
  sideRibbon: {
    width: 12,
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
  },
});
