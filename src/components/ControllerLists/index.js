import {useNavigation} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import BrokerAppList from './BrokerAppList';
import PrimeList from './PrimeList';
import Exchange_Logo from '../../assets/selectBrandSvgicons/exchange-fee-icon.svg';
import MoneyMarket_Logo from '../../assets/selectBrandSvgicons/money-market-icon.svg';
import Bond_Logo from '../../assets/selectBrandSvgicons/bonds.svg';
const ControllerLists = ({selectedMenu, isGxExpanded, setIsGxExpanded}) => {
  const {navigate} = useNavigation();

  const [activeList, setActiveList] = useState(null);

  const listCard = ({item}) => (
    <TouchableOpacity
      style={styles.brandItem}
      onPress={() => onItemSelected(item)}>
      <Image source={item.halfIcon} style={styles.brandLogo} />
      <Text style={styles.brandTitle}>{item.title}</Text>
    </TouchableOpacity>
  );

  // useEffect(() => {
  //   switch (selectedMenu) {
  //     case 'Prime':
  //       setActiveList(<PrimeList onItemSelected={onItemSelected} />);
  //       break;
  //     case 'Standard':
  //       setActiveList(<BrokerAppList onItemSelected={onItemSelected} />);
  //       break;
  //     case 'Brands':
  //       setActiveList(
  //         <View style={styles.bandsContainer}>
  //           <Text style={styles.bandEmptyText}>
  //             You Are Currently Not Following Any Brands
  //           </Text>
  //         </View>,
  //       );
  //   }
  //   return () => {};
  // }, [selectedMenu]);

  const onItemSelected = (item) => {
    // setSelectedItem(item);
    // setIsGxExpanded(true);
    navigate('BrandController', {selectedBrand: item});
  };

  return (
    <View style={styles.container}>
      {/* <Text style={styles.header}>All CompPlans</Text> */}
      <View style={styles.bandsContainer}>
        <FlatList
          // numColumns={2}
          data={ITEMS}
          keyExtractor={(item) => item.title}
          renderItem={listCard}
          showsVerticalScrollIndicator={false}
        />
      </View>
    </View>
  );
};

export default ControllerLists;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 30,
    marginTop: 11,
    marginBottom: -35,
  },
  header: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.TEXT_COLOR,
    fontSize: 15,
    marginVertical: 20,
  },
  bandsContainer: {
    flex: 1,
    marginHorizontal: -10,
  },
  bandEmptyText: {
    textAlign: 'center',
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 15,
  },
  brandItem: {
    flex: 1,
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 10,
    marginHorizontal: 10,
    marginVertical: 12.5,
    height: 95.04,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: 23,
  },
  brandLogo: {
    width: 31,
    height: 30.93,
    resizeMode: 'contain',
  },
  brandTitle: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 18,
    lineHeight: 22,
    fontStyle: 'normal',
    color: '#383C41',
    marginLeft: 13.13,
  },
});

const ITEMS = [
  {
    title: 'ExchangeFees',
    halfIcon: require('../../assets/exchange-fee-icon_half.png'),
    icon: require('../../assets/exchange-fee-icon.png'),
    color: '#186AB4',
    mtToolName: 'Exchange Fees',
    svgicon: <Exchange_Logo width={50} height={50} />,
  },
  {
    title: 'MoneyMarkets',
    halfIcon: require('../../assets/money-markets-full-icon_half.png'),
    icon: require('../../assets/money-markets-full-icon.png'),
    color: '#186AB4',
    mtToolName: 'Money Market Fees',
    svgicon: <MoneyMarket_Logo width={50} height={50} />,
  },
  {
    title: 'Bonds',
    halfIcon: require('../../assets/bonds-full-icon_half.png'),
    icon: require('../../assets/bonds-full-icon.png'),
    color: '#186AB4',
    mtToolName: 'Bond Fees',
    svgicon: <Bond_Logo width={50} height={50} />,
  },
  // {
  //   title: 'Shares',
  //   icon: require('../../assets/shares-full-icon.png'),
  //   color: '#186AB4',
  //   mtToolName: 'Shares',
  // },
  // {
  //   title: 'Funds',
  //   icon: require('../../assets/funds-full-icon.png'),
  //   color: '#186AB4',
  //   mtToolName: 'Funds',
  // },
  // {
  //   title: 'Nfts',
  //   icon: require('../../assets/nfts-full-icon.png'),
  //   color: '#186AB4',
  //   mtToolName: 'Nfts',
  // },
];
