import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import SkeltonItem from '../SkeltonItem';

const BrandItemSkeleton = () => {
  return (
    <View style={styles.itemContainer}>
      <SkeltonItem itemHeight={50} itemWidth={110} />
      <SkeltonItem itemHeight={150} itemWidth={12} style={styles.sideRibbon} />
    </View>
  );
};

export default BrandItemSkeleton;

const styles = StyleSheet.create({
  itemContainer: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    padding: 20,
    marginBottom: 15,
    overflow: 'hidden',
  },
  itemImage: {
    width: 110,
    height: 50,
  },
  sideRibbon: {
    width: 12,
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
  },
});
