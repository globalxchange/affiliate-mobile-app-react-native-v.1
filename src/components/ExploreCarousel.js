import React, {useEffect, useRef} from 'react';
import {Dimensions, FlatList, StyleSheet, Text, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import Carousel from 'react-native-snap-carousel';
import ThemeData from '../configs/ThemeData';

const {width} = Dimensions.get('window');

const ITEM_WIDTH = Math.round(width * 0.6);
const SLIDER_WIDTH = width;
const ITEM_HEIGHT = Math.round((ITEM_WIDTH * 3) / 6);

const ExploreCarousel = ({setActiveCarousel}) => {
  const listRef = useRef();

  useEffect(() => {
    setActiveCarousel(CAROUSEL_DATA[0]);
  }, []);

  const onSnapToItem = (index) => {
    // setCurrentPos(index);
    setActiveCarousel(CAROUSEL_DATA[index]);
  };

  return (
    <View style={styles.container}>
      <Carousel
        centerContent={false}
        ref={listRef}
        data={CAROUSEL_DATA}
        sliderWidth={SLIDER_WIDTH}
        itemWidth={ITEM_WIDTH}
        onSnapToItem={onSnapToItem}
        renderItem={({item}) => (
          <View style={styles.itemContainer}>
            <Text style={styles.itemLabel}>{item.title}</Text>
            <View
              style={[
                styles.imageContainer,
                {backgroundColor: `#${item.colorCode}`},
              ]}>
              <FastImage
                style={styles.carouselImage}
                source={item.image}
                resizeMode="contain"
              />
            </View>
          </View>
        )}
      />
    </View>
  );
};

export default ExploreCarousel;

const styles = StyleSheet.create({
  container: {
    marginTop: 30,
    marginBottom: 15,
  },
  itemContainer: {
    marginRight: 15,
    marginLeft: -ITEM_WIDTH / 4,
  },
  itemLabel: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 18,
  },
  carouselImage: {
    width: 220,
    height: 140,
    marginTop: 20,
  },
});

const CAROUSEL_DATA = [
  {title: 'Influencers', image: require('../assets/explore-carousel-1.jpg')},
  {title: 'Brokers', image: require('../assets/explore-carousel-2.jpg')},
  {title: 'Assistants', image: require('../assets/explore-carousel-3.jpg')},
];
