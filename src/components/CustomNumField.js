import React, {useContext, useEffect} from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {AppContext} from '../contexts/AppContextProvider';

const CustomNumField = ({
  style,
  value,
  focused,
  onFocus,
  placeholder,
  onChange,
}) => {
  const {setIsNumPadOpen, setCustomKeyBoardCallBack} = useContext(AppContext);

  const onFocusInput = () => {
    onFocus();
    setIsNumPadOpen(true);
    setCustomKeyBoardCallBack(onChange);
  };

  return (
    <TouchableOpacity style={[styles.container, style]} onPress={onFocusInput}>
      <Text style={[styles.text, focused && styles.focused]}>
        {value || placeholder}
      </Text>
    </TouchableOpacity>
  );
};

export default CustomNumField;

const styles = StyleSheet.create({
  container: {
    zIndex: 9,
    position: 'relative',
  },
  text: {
    color: '#041939',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  focused: {
    color: '#08152D',
  },
});
