import React, {useRef, useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {WModalShowToastView, WToast} from 'react-native-smart-tip';
import {formatterHelper} from '../../../utils';
import QuoteInput from '../../Connect/QuoteInput';

const QuoteFrom = ({currency, gettingValue, setGettingValue, onNext}) => {
  const toastRef = useRef();

  const onClickNext = () => {
    const parsedValue = parseFloat(gettingValue);
    if (!parsedValue) {
      return toastRef.current({
        data: 'Enter A Valid Value',
        position: WToast.position.TOP,
      });
    }

    onNext();
  };

  return (
    <View style={styles.container}>
      <WModalShowToastView
        toastInstance={(toastInstance) => (toastRef.current = toastInstance)}
      />
      <Text style={styles.header}>Amount</Text>
      <View style={styles.quoteContainer}>
        <QuoteInput
          image={{
            uri: currency.coinImage,
          }}
          enabled
          unit={currency.coinSymbol}
          // title={`How Much ${paymentCurrency.coinSymbol} Do You Want To Sell?`}
          title="You Are Sending Sending"
          placeholder={formatterHelper('0', currency.coinSymbol)}
          value={gettingValue}
          setValue={(text) => setGettingValue(text)}
          // isLoading={isSendLoading}
        />
      </View>
      <TouchableOpacity style={[styles.buyBtn]} onPress={onClickNext}>
        <Image
          resizeMode="contain"
          source={require('../../../assets/block-check-full-logo-white.png')}
          style={styles.blockIcon}
        />
      </TouchableOpacity>
    </View>
  );
};

export default QuoteFrom;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 10,
    fontSize: 35,
    fontFamily: 'Montserrat-Bold',
  },
  quoteContainer: {
    marginTop: 20,
    marginBottom: 30,
  },
  buyBtn: {
    backgroundColor: '#08152D',
    height: 45,
    borderRadius: 6,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  disabled: {
    opacity: 0.5,
  },
  errorText: {
    color: '#FF2D55',
    textAlign: 'center',
    marginBottom: 20,
    marginTop: -10,
  },
  buyBtnText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
  },
  feeButton: {
    zIndex: 1,
    position: 'absolute',
    top: 0,
    right: 0,
  },
  feeText: {
    color: '#08152D',
    fontFamily: 'Montserrat',
  },
  blockIcon: {
    height: 17,
  },
  loadingContainer: {
    paddingVertical: 60,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
});
