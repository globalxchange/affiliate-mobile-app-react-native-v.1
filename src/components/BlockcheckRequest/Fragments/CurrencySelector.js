import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import {AppContext} from '../../../contexts/AppContextProvider';
import {formatterHelper} from '../../../utils';
import ActionButton from '../../Connect/ActionButton';
import LoadingAnimation from '../../LoadingAnimation';
import VaultSelector from '../../VaultSelector';

const CurrencySelector = ({onNext, currency, setCurrency, isReceive}) => {
  const {cryptoTableData, walletBalances} = useContext(AppContext);

  const [availableCurrencies, setAvailableCurrencies] = useState();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (cryptoTableData && walletBalances) {
      const filteredCurrencies = [];

      cryptoTableData.forEach((item) => {
        if (item.type === 'crypto') {
          if (walletBalances[`${item.coinSymbol.toLowerCase()}_balance`] > 0) {
            filteredCurrencies.push({
              ...item,
              balance: formatterHelper(
                walletBalances[`${item.coinSymbol.toLowerCase()}_balance`],
                item.coinSymbol,
              ),
            });
          }
        }
      });

      setAvailableCurrencies(filteredCurrencies);
    }
  }, [cryptoTableData, walletBalances]);

  const onNextClick = () => {
    if (!currency) {
      return WToast.show({
        data: 'Please select a sending currency',
        position: WToast.position.TOP,
      });
    }
    onNext();
  };

  if (!availableCurrencies || isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <LoadingAnimation />
      </View>
    );
  }

  if (availableCurrencies.length <= 0) {
    return (
      <View style={styles.loadingContainer}>
        <Text style={styles.notAvailableText}>No Balance In Any Currency</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.controlContainer}>
        <View>
          <Text style={styles.header}>Currency</Text>
          <Text style={styles.subHeader}>
            {isReceive
              ? 'Which Currency Are You Looking To Receive?'
              : 'Select The Currency Which You Would Like Your Friend To Receive'}
          </Text>
        </View>
        <View style={styles.dropDownContainer}>
          <VaultSelector
            placeHolder="Select Any Asset"
            onItemSelect={setCurrency}
            selectedItem={currency}
            onNext={onNext}
          />
        </View>
      </View>
      <ActionButton text="Proceed" onPress={onNextClick} />
    </View>
  );
};

export default CurrencySelector;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 40,
    fontSize: 28,
    fontFamily: 'Montserrat-Bold',
    marginTop: 20,
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  controlContainer: {
    justifyContent: 'center',
  },
  dropDownContainer: {
    justifyContent: 'center',
    marginVertical: 30,
  },
  loadingContainer: {
    justifyContent: 'center',
    paddingVertical: 50,
  },
  notAvailableText: {
    color: '#08152D',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
  },
});
