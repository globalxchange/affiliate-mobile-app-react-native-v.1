import React, {useRef, useState} from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import {WModalShowToastView, WToast} from 'react-native-smart-tip';
import ActionButton from '../../Connect/ActionButton';

const SecurityAnswer = ({securityAnswer, setSecurityAnswer, onNext}) => {
  const [answerInput, setAnswerInput] = useState('');
  const [confirmAnswer, setConfirmAnswer] = useState('');
  const [isShowConfirm, setIsShowConfirm] = useState(false);

  const toastRef = useRef();

  const onNextClick = () => {
    if (!answerInput) {
      return toastRef.current({
        data: 'Please Input An Answer',
        position: WToast.position.TOP,
      });
    }

    if (!isShowConfirm) {
      return setIsShowConfirm(true);
    }

    if (answerInput !== confirmAnswer) {
      return toastRef.current({
        data: 'Answers does not matching',
        position: WToast.position.TOP,
      });
    }

    setSecurityAnswer(answerInput);
    onNext();
  };

  return (
    <View style={styles.container}>
      <View style={styles.controlContainer}>
        <View>
          <Text style={styles.header}>
            {isShowConfirm ? 'Confirm Answer' : 'Answer'}
          </Text>
          <Text style={styles.subHeader}>
            What Is The Answer To Your Security Question?
          </Text>
        </View>
      </View>
      <WModalShowToastView
        toastInstance={(toastInstance) => (toastRef.current = toastInstance)}
      />
      <View style={styles.addressTextContainer}>
        <View style={styles.textContainer}>
          <TextInput
            style={styles.addressInput}
            placeholder={'Ex. Feb'}
            value={isShowConfirm ? confirmAnswer : answerInput}
            onChangeText={(text) =>
              isShowConfirm ? setConfirmAnswer(text) : setAnswerInput(text)
            }
            placeholderTextColor="#878788"
          />
        </View>
      </View>
      <ActionButton text="Proceed" onPress={onNextClick} />
    </View>
  );
};

export default SecurityAnswer;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 20,
    fontSize: 30,
    fontFamily: 'Montserrat-Bold',
    marginTop: 20,
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    marginBottom: 30,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  addressTextContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 45,
    marginTop: 30,
  },
  textContainer: {
    borderColor: '#EDEDED',
    borderWidth: 2,
    borderRadius: 6,
    flexGrow: 1,
    width: 0,
    justifyContent: 'center',
    height: 40,
  },
  addressInput: {
    fontFamily: 'Montserrat',
    paddingHorizontal: 15,
    color: 'black',
  },
});
