import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import ActionButton from '../../Connect/ActionButton';
import PhoneInput from './PhoneInput';

const InitiatorPhone = ({initiatorPhone, setInitiatorPhone, onNext}) => {
  const [isCountryOpen, setIsCountryOpen] = useState(false);

  const onNextClick = () => {
    if (!initiatorPhone) {
      return WToast.show({
        data: 'Please Input An Address',
        position: WToast.position.TOP,
      });
    }
    onNext();
  };

  return (
    <View style={styles.container}>
      {isCountryOpen || (
        <View style={styles.controlContainer}>
          <View>
            <Text style={styles.header}>Notify</Text>
            <Text style={styles.subHeader}>
              Enter Your Phone Number To Get SMS Updates On The Transaction
            </Text>
          </View>
        </View>
      )}
      <View style={{marginVertical: 40}}>
        <PhoneInput
          onChangeText={setInitiatorPhone}
          onCountryOpen={(isOpen) => setIsCountryOpen(isOpen)}
        />
      </View>
      <ActionButton text="Proceed" onPress={onNextClick} />
    </View>
  );
};

export default InitiatorPhone;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 20,
    fontSize: 30,
    fontFamily: 'Montserrat-Bold',
    // marginTop: 20,
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    marginBottom: 20,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
});
