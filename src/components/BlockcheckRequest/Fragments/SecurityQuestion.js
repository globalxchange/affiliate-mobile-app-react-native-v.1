import React from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import ActionButton from '../../Connect/ActionButton';

const SecurityQuestion = ({securityQuestion, setSecurityQuestion, onNext}) => {
  const onNextClick = () => {
    if (!securityQuestion) {
      return WToast.show({
        data: 'Please Input An Address',
        position: WToast.position.TOP,
      });
    }
    onNext();
  };

  return (
    <View style={styles.container}>
      <View style={styles.controlContainer}>
        <View>
          <Text style={styles.header}>Security Question</Text>
          <Text style={styles.subHeader}>
            Create A Security Question To Unlock Your FUnds
          </Text>
        </View>
      </View>
      <View style={styles.addressTextContainer}>
        <View style={styles.textContainer}>
          <TextInput
            style={styles.addressInput}
            placeholder={'Ex. When Is My Birthday'}
            value={securityQuestion}
            onChangeText={(text) => setSecurityQuestion(text)}
            placeholderTextColor="#878788"
          />
        </View>
      </View>
      <ActionButton text="Proceed" onPress={onNextClick} />
    </View>
  );
};

export default SecurityQuestion;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 20,
    fontSize: 30,
    fontFamily: 'Montserrat-Bold',
    marginTop: 20,
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    marginBottom: 30,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  addressTextContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 45,
    marginTop: 30,
  },
  textContainer: {
    borderColor: '#EDEDED',
    borderWidth: 2,
    borderRadius: 6,
    flexGrow: 1,
    width: 0,
    justifyContent: 'center',
    height: 40,
  },
  addressInput: {
    fontFamily: 'Montserrat',
    paddingHorizontal: 15,
    color: 'black',
  },
});
