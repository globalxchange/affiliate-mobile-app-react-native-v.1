import React from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import ActionButton from '../../Connect/ActionButton';

const MessageInput = ({message, setMessage, onNext}) => {
  const onNextClick = () => {
    if (!message) {
      return WToast.show({
        data: 'Please Input A Message',
        position: WToast.position.TOP,
      });
    }

    onNext();
  };

  return (
    <View style={styles.container}>
      <View style={styles.controlContainer}>
        <View>
          <Text style={styles.header}>Custom Message</Text>
          <Text style={styles.subHeader}>
            Let Your Recipient Know Why They Are Getting Crypto
          </Text>
        </View>
      </View>
      <View style={styles.addressTextContainer}>
        <View style={styles.textContainer}>
          <TextInput
            style={styles.addressInput}
            placeholder={'Ex. Hey, I Thought You Deserved Some Crypto'}
            value={message}
            onChangeText={(text) => setMessage(text)}
            placeholderTextColor="#878788"
            returnKeyType="done"
          />
        </View>
      </View>
      <ActionButton text="Proceed" onPress={onNextClick} />
    </View>
  );
};

export default MessageInput;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 20,
    fontSize: 30,
    fontFamily: 'Montserrat-Bold',
    marginTop: 20,
  },
  subHeader: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 30,
    fontSize: 13,
    fontFamily: 'Montserrat-SemiBold',
  },
  addressTextContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 45,
    marginTop: 30,
  },
  textContainer: {
    borderColor: '#EDEDED',
    borderWidth: 2,
    borderRadius: 6,
    flexGrow: 1,
    width: 0,
    justifyContent: 'center',
    height: 80,
  },
  addressInput: {
    fontFamily: 'Montserrat',
    paddingHorizontal: 15,
    color: 'black',
  },
});
