import React, {useRef, useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {WModalShowToastView, WToast} from 'react-native-smart-tip';
import {GX_API_ENDPOINT} from '../../../configs';
import {formatterHelper} from '../../../utils';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import QuoteInput from '../../Connect/QuoteInput';
import LoadingAnimation from '../../LoadingAnimation';
import Axios from 'axios';

const ReceiveQuote = ({currency, name, address, recipient, isMail, onNext}) => {
  const [gettingValue, setGettingValue] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const toastRef = useRef();

  const buyClickHandler = async () => {
    if (isNaN(parseFloat(gettingValue)) || parseFloat(gettingValue) <= 0) {
      setGettingValue('');
      return toastRef.current({
        data: 'Please Input A Valid value',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();
    const profileId = await AsyncStorageHelper.getProfileId();
    const userName = await AsyncStorageHelper.getUserName();

    let postData = {
      bcr_type: 'receiver', // sender / receiver
      initiator_name: userName, // name of the initiator
      initiator_profileId: profileId, // optional, need only for type receiver
      initiator_email: email, // email of the initiator
      coin: currency.coinSymbol,
      amount: parseFloat(gettingValue),
      crypto_address: address,
      endUser_name: name, // the end sender or receiver name
    };

    if (isMail) {
      postData = {...postData, endUser_email: recipient};
    } else {
      postData = {...postData, endUser_phone: recipient};
    }

    // console.log('PostData', postData);

    Axios.post(
      `${GX_API_ENDPOINT}/coin/vault/service/blockcheck/request/initiate`,
      postData,
    )
      .then((resp) => {
        const {data} = resp;

        // console.log('Resp', data);

        setIsLoading(false);
        if (data.status) {
          // WToast.show({data: data.message, position: WToast.position.TOP});
          onNext();
        } else {
          WToast.show({
            data: data.message || 'Failed',
            position: WToast.position.TOP,
          });
        }
      })
      .catch((error) => {
        setIsLoading(false);
      });
  };

  return (
    <View style={styles.container}>
      <WModalShowToastView
        toastInstance={(toastInstance) => (toastRef.current = toastInstance)}
      />

      {isLoading ? (
        <View style={[styles.loadingContainer]}>
          <LoadingAnimation />
        </View>
      ) : (
        <>
          <Text style={styles.header}>Amount</Text>
          <Text style={styles.subHeader}>
            How Many {currency?.coinName} Are You Looking To Receive?
          </Text>
          <View style={styles.quoteContainer}>
            <QuoteInput
              image={{
                uri: currency.coinImage,
              }}
              unit={currency.coinSymbol}
              // title="Your Recipient Will Receive"
              enabled
              value={gettingValue}
              setValue={(text) => setGettingValue(text)}
              placeholder={formatterHelper('0', currency.coinSymbol)}
              // isLoading={isGettingLoading}
            />
          </View>
          <TouchableOpacity style={[styles.buyBtn]} onPress={buyClickHandler}>
            <Image
              resizeMode="contain"
              source={require('../../../assets/block-check-full-logo-white.png')}
              style={styles.blockIcon}
            />
          </TouchableOpacity>
        </>
      )}
    </View>
  );
};

export default ReceiveQuote;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 10,
    fontSize: 35,
    fontFamily: 'Montserrat-Bold',
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    marginBottom: 30,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  quoteContainer: {
    marginTop: 20,
    marginBottom: 70,
  },
  buyBtn: {
    backgroundColor: '#08152D',
    height: 45,
    borderRadius: 6,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  disabled: {
    opacity: 0.5,
  },
  errorText: {
    color: '#FF2D55',
    textAlign: 'center',
    marginBottom: 20,
    marginTop: -10,
  },
  buyBtnText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
  },
  feeButton: {
    zIndex: 1,
    position: 'absolute',
    top: 0,
    right: 0,
  },
  feeText: {
    color: '#08152D',
    fontFamily: 'Montserrat',
  },
  blockIcon: {
    height: 17,
  },
  loadingContainer: {
    paddingVertical: 60,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
});
