import React, {useContext, useEffect, useState} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import {AppContext} from '../../../contexts/AppContextProvider';
import ActionButton from '../../Connect/ActionButton';
import Clipboard from '@react-native-community/clipboard';

const MyAddressForm = ({
  currency,
  setAddress,
  onNext,
  isReceive,
  setIsScannerOpen,
  openScanner,
  address,
}) => {
  const {walletBalances} = useContext(AppContext);

  const [addressInput, setAddressInput] = useState('');
  const [isGxAddress, setIsGxAddress] = useState(true);

  useEffect(() => {
    if (walletBalances) {
      // console.log('walletBalances', walletBalances);
      if (isGxAddress) {
        const walletAddress =
          walletBalances.coinAddress[currency.coinSymbol]?.address;

        setAddressInput(walletAddress || '');
      } else {
        setAddressInput('');
      }
    }
  }, [walletBalances, isGxAddress, currency]);

  useEffect(() => {
    if (address) {
      setAddressInput(address);
    }
  }, [address]);

  const pasteAddressHandler = async () => {
    const pastedText = await Clipboard.getString();

    setAddressInput(pastedText);
  };

  const onScannerOpen = () => {
    setIsScannerOpen(true);
    openScanner();
  };

  const onNextClick = () => {
    if (!addressInput) {
      return WToast.show({
        data: 'Please Input An Address',
        position: WToast.position.TOP,
      });
    }
    setAddress(addressInput);
    onNext();
  };

  return (
    <View style={styles.container}>
      <View style={styles.controlContainer}>
        <View>
          <Text style={styles.header}>Your Address</Text>
          <Text style={styles.subHeader}>
            {isGxAddress
              ? `Here Is The ${currency?.coinName} Address For Your ${currency?.coinName} Vault.`
              : `Please Enter The Alternative ${currency?.coinName} Address For This Transaction`}
          </Text>
        </View>
        <View style={styles.addressTextContainer}>
          <View style={styles.textContainer}>
            <TextInput
              style={styles.addressInput}
              placeholder={`Enter ${currency.coinSymbol} Address`}
              value={addressInput}
              onChangeText={(text) => setAddressInput(text)}
              placeholderTextColor="#878788"
            />
          </View>
          <TouchableOpacity style={styles.qrContainer} onPress={onScannerOpen}>
            <Image
              style={styles.qrIcon}
              source={require('../../../assets/qr-code-icon.png')}
              resizeMode="contain"
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.qrContainer}
            onPress={pasteAddressHandler}>
            <Image
              style={styles.qrIcon}
              source={require('../../../assets/address-copy-icon.png')}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
        <Text
          style={styles.switchText}
          onPress={() => setIsGxAddress(!isGxAddress)}>
          {isGxAddress
            ? `Use Another ${currency?.coinName} Address`
            : `Use AffiliateApp ${currency?.coinName} Address`}
        </Text>
      </View>
      <ActionButton text="Proceed" onPress={onNextClick} />
    </View>
  );
};

export default MyAddressForm;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 20,
    fontSize: 30,
    fontFamily: 'Montserrat-Bold',
    marginTop: 20,
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    marginBottom: 30,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  addressTextContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textContainer: {
    borderColor: '#EDEDED',
    borderWidth: 2,
    borderRadius: 6,
    flexGrow: 1,
    width: 0,
    justifyContent: 'center',
    height: 40,
  },
  addressInput: {
    fontFamily: 'Montserrat',
    paddingHorizontal: 15,
    color: 'black',
  },
  qrContainer: {
    borderColor: '#EDEDED',
    borderWidth: 2,
    borderRadius: 6,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    width: 40,
    marginLeft: 5,
  },
  qrIcon: {width: 20, height: 24},
  switchText: {
    marginBottom: 45,
    marginTop: 15,
    textAlign: 'right',
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
    textDecorationLine: 'underline',
  },
});
