import Clipboard from '@react-native-community/clipboard';
import {useNavigation} from '@react-navigation/native';
import React, {useContext, useEffect, useState} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import ActionButton from '../../Connect/ActionButton';

const AddressForm = ({
  currency,
  setAddress,
  onNext,
  setIsScannerOpen,
  openScanner,
  address,
}) => {
  const [addressInput, setAddressInput] = useState('');

  useEffect(() => {
    if (address) {
      setAddressInput(address);
    }
  }, [address]);

  const pasteAddressHandler = async () => {
    const copiedText = await Clipboard.getString();

    setAddressInput(copiedText);
  };

  const onNextClick = () => {
    if (!addressInput) {
      return WToast.show({
        data: 'Please Input An Address',
        position: WToast.position.TOP,
      });
    }
    setAddress(addressInput);
    onNext();
  };

  const onScannerOpen = () => {
    setIsScannerOpen(true);
    openScanner();
  };

  return (
    <View style={styles.container}>
      <View style={styles.controlContainer}>
        <View>
          <Text style={styles.header}>Recipient</Text>
          <Text style={styles.subHeader}>
            Enter The Cryptocurrency Address Of The Person To Whom You Are
            Trying To Send {currency?.coinName}
          </Text>
        </View>
        <View style={styles.addressTextContainer}>
          <View style={styles.textContainer}>
            <TextInput
              style={styles.addressInput}
              placeholder={`Enter ${currency.coinSymbol} Address`}
              value={addressInput}
              onChangeText={(text) => setAddressInput(text)}
              placeholderTextColor="#878788"
            />
          </View>
          <TouchableOpacity style={styles.qrContainer} onPress={onScannerOpen}>
            <Image
              style={styles.qrIcon}
              source={require('../../../assets/qr-code-icon.png')}
              resizeMode="contain"
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.qrContainer}
            onPress={pasteAddressHandler}>
            <Image
              style={styles.qrIcon}
              source={require('../../../assets/address-copy-icon.png')}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
      </View>
      <ActionButton text="Proceed" onPress={onNextClick} />
    </View>
  );
};

export default AddressForm;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 20,
    fontSize: 30,
    fontFamily: 'Montserrat-Bold',
    marginTop: 20,
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    marginBottom: 30,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  addressTextContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 45,
    marginTop: 30,
  },
  textContainer: {
    borderColor: '#EDEDED',
    borderWidth: 2,
    borderRadius: 6,
    flexGrow: 1,
    width: 0,
    justifyContent: 'center',
    height: 40,
  },
  addressInput: {
    fontFamily: 'Montserrat',
    paddingHorizontal: 15,
    color: 'black',
  },
  qrContainer: {
    borderColor: '#EDEDED',
    borderWidth: 2,
    borderRadius: 6,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    width: 40,
    marginLeft: 5,
  },
  qrIcon: {width: 20, height: 24},
});
