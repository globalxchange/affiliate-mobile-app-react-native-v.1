import React, {useState} from 'react';
import {Image, StyleSheet, Text, TextInput, View} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import ActionButton from '../../Connect/ActionButton';
import PhoneInput from './PhoneInput';

const RecipientForm = ({
  onNext,
  isMail,
  setName,
  setEmail,
  setPhone,
  currency,
  isReceive,
}) => {
  const [userName, setUserName] = useState('');
  const [contact, setContact] = useState('');
  const [showContact, setShowContact] = useState(false);

  const onNextClick = () => {
    if (!contact && !contact) {
      if (!userName) {
        return WToast.show({
          data: 'Please Enter The Name',
          position: WToast.position.TOP,
        });
      }
      setShowContact(true);
      setName(userName);
    } else {
      if (!contact) {
        return WToast.show({
          data: `Please Enter The ${isMail ? 'Mail' : 'Phone Number'}`,
          position: WToast.position.TOP,
        });
      }

      if (isMail) {
        setEmail(contact.toLowerCase().trim());
      } else {
        setPhone(contact);
      }
      onNext();
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.controlContainer}>
        <View>
          <Text style={styles.header}>
            {isReceive ? 'Sender' : 'Recipient'}
          </Text>
          {!showContact ? (
            <Text style={styles.subHeader}>
              Enter The Name Or Nickname Of The Person{' '}
              {isReceive ? 'Who Needs To' : 'To Whom You Are Trying To'}{' '}
              {isReceive ? 'Receive' : 'Send'} {currency?.coinName}
            </Text>
          ) : isMail ? (
            <Text style={styles.subHeader}>
              Enter The Email
              {isReceive ? 'Who Needs To' : 'To Whom You Are Trying To'}{' '}
              {isReceive ? 'Receive' : 'Send'} {currency?.coinName}
            </Text>
          ) : (
            <Text style={styles.subHeader}>
              You Have To Enter The Entire Phone Number Including The Country
              Code.
            </Text>
          )}
        </View>
        {!showContact ? (
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.input}
              placeholderTextColor="#9A9A9A"
              placeholder={'Enter Name'}
              value={userName}
              keyboardType={'default'}
              onChangeText={(text) => setUserName(text)}
              returnKeyType="done"
            />
            <View style={styles.iconContainer}>
              <Image
                style={styles.icon}
                resizeMode="contain"
                source={require('../../../assets/default-breadcumb-icon/recipient.png')}
              />
            </View>
          </View>
        ) : isMail ? (
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.input}
              placeholderTextColor="#9A9A9A"
              placeholder={'Enter Email'}
              value={contact}
              keyboardType={'default'}
              onChangeText={(text) => setContact(text)}
              returnKeyType="done"
            />
            <View style={styles.iconContainer}>
              <Image
                style={styles.icon}
                resizeMode="contain"
                source={require('../../../assets/gmail-icon.png')}
              />
            </View>
          </View>
        ) : (
          <PhoneInput onChangeText={setContact} />
        )}
      </View>
      <ActionButton text="Proceed" onPress={onNextClick} />
    </View>
  );
};

export default RecipientForm;

const styles = StyleSheet.create({
  container: {},
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 20,
    fontSize: 30,
    fontFamily: 'Montserrat-Bold',
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    marginBottom: 30,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  controlContainer: {
    justifyContent: 'center',
    marginBottom: 40,
  },
  inputContainer: {
    flexDirection: 'row',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    paddingLeft: 15,
    alignItems: 'center',
    height: 50,
    marginBottom: 30,
    overflow: 'hidden',
  },
  label: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  input: {
    flexGrow: 1,
    width: 0,
    paddingHorizontal: 15,
    fontFamily: 'Montserrat',
    color: 'black',
  },
  iconContainer: {
    borderLeftColor: '#EBEBEB',
    borderLeftWidth: 1,
    width: 50,
    paddingHorizontal: 15,
  },
  icon: {
    flex: 1,
    width: null,
  },
  buttonContainer: {
    flexDirection: 'row',
  },
  loadingContainer: {
    paddingVertical: 50,
    justifyContent: 'center',
  },
});
