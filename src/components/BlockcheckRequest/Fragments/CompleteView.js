import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const CompleteView = ({name, isMail, isSend, onClose}) => {
  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Image
          source={require('../../../assets/block-check-full-logo.png')}
          style={styles.headerImage}
          resizeMode="contain"
        />
        <Text style={styles.header}>Successfully Created</Text>
      </View>
      <View style={styles.textContainer}>
        <Text style={styles.successText}>
          {name} Would Have Received A {isMail ? 'Email' : 'SMS'} Containing
          Your Blockcheck.
        </Text>
        <Text style={styles.successText}>
          Once He Signs The BlockCheck. We Will Notify You.
        </Text>
        <Text style={styles.successText}>
          Remember You Can Always Check The Status Of A Pending Check In Your
          BlockCheck Helper.
        </Text>
      </View>
      <TouchableOpacity onPress={onClose} style={styles.button}>
        <Text style={styles.buttonText}>Close</Text>
      </TouchableOpacity>
    </View>
  );
};

export default CompleteView;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    padding: 40,
  },
  headerContainer: {
    alignItems: 'flex-start',
  },
  headerImage: {
    height: 40,
    width: 190,
  },
  header: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 18,
  },
  textContainer: {
    paddingVertical: 30,
  },
  successText: {
    paddingVertical: 10,
    color: '#9A9A9A',
    fontFamily: 'Montserrat',
  },
  button: {
    backgroundColor: '#08152D',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 6,
    height: 50,
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
  },
});
