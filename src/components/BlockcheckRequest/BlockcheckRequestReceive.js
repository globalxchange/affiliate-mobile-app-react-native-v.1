/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import QRScanFragment from '../QRScanFragment';
import BreadcrumbsReceive from './BreadcrumbsReceive';
import CompleteView from './Fragments/CompleteView';
import CurrencySelector from './Fragments/CurrencySelector';
import MyAddressForm from './Fragments/MyAddressForm';
import ReceiveQuote from './Fragments/ReceiveQuote';
import RecipientForm from './Fragments/RecipientForm';
import TypeSelector from './TypeSelector';

const BlockcheckRequestReceive = ({
  setIsOpen,
  isScannerOpen,
  setIsScannerOpen,
}) => {
  const [selectedView, setSelectedView] = useState();

  const [type, setType] = useState();
  const [recipient, setRecipient] = useState('');
  const [currency, setCurrency] = useState('');
  const [name, setName] = useState('');
  const [address, setAddress] = useState('');

  let activeViewContent;

  switch (selectedView) {
    case 'Type':
      activeViewContent = (
        <TypeSelector
          setType={setType}
          onClose={() => setIsOpen(false)}
          onNext={() => setSelectedView('Currency')}
          isReceive
        />
      );
      break;

    case 'Currency':
      activeViewContent = (
        <CurrencySelector
          currency={currency}
          setCurrency={setCurrency}
          onNext={() => setSelectedView('Recipient')}
          isReceive
        />
      );
      break;

    case 'Recipient':
      activeViewContent = (
        <RecipientForm
          currency={currency}
          onNext={() => setSelectedView('Address')}
          setEmail={setRecipient}
          setPhone={setRecipient}
          setName={setName}
          isMail={type.name === 'Email'}
          isReceive
        />
      );
      break;

    case 'Address':
      activeViewContent = (
        <MyAddressForm
          currency={currency}
          address={address}
          setAddress={setAddress}
          onNext={() => setSelectedView('Quote')}
          setIsScannerOpen={setIsScannerOpen}
          openScanner={() => setSelectedView('QRScan')}
          isReceive
        />
      );
      break;

    case 'QRScan':
      activeViewContent = (
        <QRScanFragment
          onScanned={setAddress}
          onClose={() => setSelectedView('Address')}
          setIsScannerOpen={setIsScannerOpen}
        />
      );
      break;

    case 'Quote':
      activeViewContent = (
        <ReceiveQuote
          name={name}
          address={address}
          currency={currency}
          recipient={recipient}
          isMail={type.name === 'Email'}
          onNext={() => setSelectedView('Completed')}
        />
      );
      break;

    case 'Completed':
      activeViewContent = (
        <CompleteView
          name={name}
          isMail={type.name === 'Email'}
          onClose={() => setIsOpen(false)}
          isSend={false}
        />
      );
      break;

    default:
      activeViewContent = (
        <TypeSelector
          setType={setType}
          onClose={() => setIsOpen(false)}
          onNext={() => setSelectedView('Currency')}
          isReceive
        />
      );
  }

  return (
    <View style={[styles.fragmentContainer, isScannerOpen && {flex: 1}]}>
      {!selectedView ||
      selectedView === 'Type' ||
      selectedView === 'Completed' ? (
        activeViewContent
      ) : (
        <>
          {isScannerOpen || (
            <>
              <View style={styles.headerContainer}>
                <Image
                  style={styles.headerLogo}
                  source={require('../../assets/block-check-full-logo-white.png')}
                  resizeMode="contain"
                />
              </View>
              <BreadcrumbsReceive
                type={type}
                recipient={recipient}
                currency={currency}
              />
            </>
          )}
          <View style={[{padding: 30}, isScannerOpen && {flex: 1, padding: 0}]}>
            {activeViewContent}
          </View>
        </>
      )}
    </View>
  );
};

export default BlockcheckRequestReceive;

const styles = StyleSheet.create({
  fragmentContainer: {
    // flex: 1,
    backgroundColor: 'white',
  },
  headerContainer: {
    backgroundColor: '#08152D',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15,
  },
  headerLogo: {
    height: 22,
  },
});
