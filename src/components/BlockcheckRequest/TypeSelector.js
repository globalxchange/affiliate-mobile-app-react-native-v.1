import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const TypeSelector = ({setType, onClose, onNext, isReceive}) => {
  const onTypeClicked = (item) => {
    setType(item);
    onNext();
  };

  return (
    <View style={styles.container}>
      <View style={styles.viewContainer}>
        <Image
          style={styles.headerImage}
          source={require('../../assets/block-check-full-logo.png')}
          resizeMode="contain"
        />
        <Text style={styles.subHeader}>
          The Safest Way To {isReceive ? 'Receive' : 'Send'} Crypto To An
          External Wallet
        </Text>
        <View style={styles.tutorialContainer}>
          <View style={styles.preview} />
          <View style={styles.detailsContainer}>
            <Text style={styles.tutorialTitle}>
              First Time Using Blockcheck?{' '}
            </Text>
            <Text style={styles.tutorialSubHeader}>
              Watch This Quick Video To See How Blockcheck Works
            </Text>
          </View>
        </View>
        <Text style={styles.introText}>
          Select On Of The Following Methods To Create A Blockcheck™
        </Text>
        <View style={styles.typesContainer}>
          {types.map((item) => (
            <TouchableOpacity
              disabled={item.disabled}
              onPress={() => onTypeClicked(item)}
              key={item.name}
              style={styles.typeItem}>
              <View
                style={[
                  styles.typeIconContainer,
                  item.disabled && {opacity: 0.4},
                ]}>
                <Image
                  resizeMode="contain"
                  style={styles.typeIcon}
                  source={item.image}
                />
              </View>
              <Text style={[styles.typeName, item.disabled && {opacity: 0.4}]}>
                {item.name}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
      </View>
      <TouchableOpacity style={styles.closeButton} onPress={onClose}>
        <Text style={styles.closeButtonText}>Close Blockcheck</Text>
      </TouchableOpacity>
    </View>
  );
};

export default TypeSelector;

const styles = StyleSheet.create({
  container: {},
  viewContainer: {
    padding: 40,
  },
  headerImage: {
    height: 40,
    width: 260,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  subHeader: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat',
    marginTop: 20,
  },
  tutorialContainer: {
    flexDirection: 'row',
    marginTop: 25,
  },
  preview: {
    backgroundColor: 'rgba(24, 106, 180, 0.06);',
    width: 100,
  },
  detailsContainer: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 12,
  },
  tutorialTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
  tutorialSubHeader: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 9,
    marginTop: 8,
  },
  introText: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    marginTop: 25,
  },
  typesContainer: {
    flexDirection: 'row',
    marginTop: 30,
  },
  typeItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  typeIconContainer: {
    borderColor: '#F1F4F6',
    borderWidth: 1,
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    height: 60,
    width: 60,
  },
  typeIcon: {
    flex: 1,
  },
  typeName: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    marginTop: 8,
  },
  closeButton: {
    backgroundColor: '#08152D',
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
  },
  closeButtonText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
  },
});

const types = [
  {name: 'Email', image: require('../../assets/gmail-icon.png')},
  {name: 'Text', image: require('../../assets/phone-text-icon.png')},
  {
    name: 'Friends',
    image: require('../../assets/gx-logo.png'),
    disabled: true,
  },
];
