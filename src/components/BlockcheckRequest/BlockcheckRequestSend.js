import Axios from 'axios';
import React, {useState} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import QRScanFragment from '../QRScanFragment';
import {WToast} from 'react-native-smart-tip';
import {APP_CODE, GX_API_ENDPOINT} from '../../configs';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import BreadcrumbsSend from './BreadcrumbsSend';
import CompleteView from './Fragments/CompleteView';
import CurrencySelector from './Fragments/CurrencySelector';
import InitiatorPhone from './Fragments/InitiatorPhone';
import LoadingView from './Fragments/LoadingView';
import MessageInput from './Fragments/MessageInput';
import QuoteFrom from './Fragments/QuoteFrom';
import RecipientForm from './Fragments/RecipientForm';
import SecurityAnswer from './Fragments/SecurityAnswer';
import SecurityQuestion from './Fragments/SecurityQuestion';
import TypeSelector from './TypeSelector';

const BlockcheckRequestSend = ({
  setIsOpen,
  isScannerOpen,
  setIsScannerOpen,
}) => {
  const [selectedView, setSelectedView] = useState();
  const [isLoading, setIsLoading] = useState(false);

  const [type, setType] = useState();
  const [recipient, setRecipient] = useState('');
  const [currency, setCurrency] = useState('');
  const [name, setName] = useState('');
  const [address, setAddress] = useState('');
  const [initiatorPhone, setInitiatorPhone] = useState('');
  const [coinAmount, setCoinAmount] = useState('');
  const [securityQuestion, setSecurityQuestion] = useState('');
  const [securityAnswer, setSecurityAnswer] = useState('');
  const [messageInput, setMessageInput] = useState(
    'Hey, I Thought You Deserved Some Crypto',
  );

  const buyClickHandler = async () => {
    setIsLoading(true);

    const isMail = type.name === 'Email';

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();
    const profileId = await AsyncStorageHelper.getProfileId();
    const userName = await AsyncStorageHelper.getUserName();

    let postData = {
      token,
      bcr_type: 'sender_escrow',
      initiator_name: userName,
      initiator_profileId: profileId,
      initiator_email: email,
      coin: currency.coinSymbol,
      amount: parseFloat(coinAmount),
      crypto_address: '',
      endUser_name: name,
      initiator_appCode: APP_CODE,
      security_question: securityQuestion,
      security_answer: securityAnswer,
      initiator_phone: initiatorPhone,
      custom_message: messageInput || '',
    };

    if (isMail) {
      postData = {...postData, endUser_email: recipient};
    } else {
      postData = {...postData, endUser_phone: recipient};
    }

    // console.log('PostData', postData);

    Axios.post(
      `${GX_API_ENDPOINT}/coin/vault/service/blockcheck/request/initiate`,
      postData,
    )
      .then((resp) => {
        const {data} = resp;

        // console.log('Resp', data);

        setIsLoading(false);
        if (data.status) {
          // WToast.show({data: data.message, position: WToast.position.TOP});
          setSelectedView('Completed');
        } else {
          WToast.show({
            data: data.message || 'Failed',
            position: WToast.position.TOP,
          });
        }
      })
      .catch((error) => {
        console.log('Error on crreating bloickcheck', error);
        setIsLoading(false);
      });
  };

  let activeViewContent;

  switch (selectedView) {
    case 'Type':
      activeViewContent = (
        <TypeSelector
          setType={setType}
          onClose={() => setIsOpen(false)}
          onNext={() => setSelectedView('InitiatorPhone')}
        />
      );
      break;

    case 'InitiatorPhone':
      activeViewContent = (
        <InitiatorPhone
          initiatorPhone={initiatorPhone}
          setInitiatorPhone={setInitiatorPhone}
          onNext={() => setSelectedView('Recipient')}
        />
      );
      break;

    case 'Recipient':
      activeViewContent = (
        <RecipientForm
          currency={currency}
          onNext={() => setSelectedView('Currency')}
          setEmail={setRecipient}
          setPhone={setRecipient}
          setName={setName}
          isMail={type.name === 'Email'}
        />
      );
      break;

    case 'Currency':
      activeViewContent = (
        <CurrencySelector
          currency={currency}
          setCurrency={setCurrency}
          onNext={() => setSelectedView('Quote')}
        />
      );
      break;

    case 'QRScan':
      activeViewContent = (
        <QRScanFragment
          onScanned={setAddress}
          onClose={() => setSelectedView('Address')}
          setIsScannerOpen={setIsScannerOpen}
        />
      );
      break;

    case 'Quote':
      activeViewContent = (
        <QuoteFrom
          currency={currency}
          gettingValue={coinAmount}
          setGettingValue={setCoinAmount}
          onNext={() => setSelectedView('SecurityQuestion')}
        />
      );
      break;

    case 'SecurityQuestion':
      activeViewContent = (
        <SecurityQuestion
          setSecurityQuestion={setSecurityQuestion}
          securityQuestion={securityQuestion}
          onNext={() => setSelectedView('SecurityAnswer')}
        />
      );
      break;

    case 'SecurityAnswer':
      activeViewContent = (
        <SecurityAnswer
          securityAnswer={securityAnswer}
          setSecurityAnswer={setSecurityAnswer}
          onNext={() => setSelectedView('Message')}
        />
      );
      break;

    case 'Message':
      activeViewContent = (
        <MessageInput
          message={messageInput}
          setMessage={setMessageInput}
          onNext={() => buyClickHandler()}
        />
      );
      break;

    case 'Completed':
      activeViewContent = (
        <CompleteView
          name={name}
          isMail={type.name === 'Email'}
          onClose={() => setIsOpen(false)}
          isSend
        />
      );
      break;

    default:
      activeViewContent = (
        <TypeSelector
          setType={setType}
          onClose={() => setIsOpen(false)}
          onNext={() => setSelectedView('InitiatorPhone')}
        />
      );
  }

  return (
    <View style={[styles.fragmentContainer, isScannerOpen && {flex: 1}]}>
      {!selectedView ||
      selectedView === 'Type' ||
      selectedView === 'Completed' ? (
        activeViewContent
      ) : (
        <>
          {isScannerOpen || (
            <>
              <View style={styles.headerContainer}>
                <Image
                  style={styles.headerLogo}
                  source={require('../../assets/block-check-full-logo-white.png')}
                  resizeMode="contain"
                />
              </View>
              <BreadcrumbsSend
                type={type}
                recipient={recipient}
                currency={currency}
              />
            </>
          )}
          <View style={[{padding: 30}, isScannerOpen && {flex: 1, padding: 0}]}>
            {isLoading ? <LoadingView /> : activeViewContent}
          </View>
        </>
      )}
    </View>
  );
};

export default BlockcheckRequestSend;

const styles = StyleSheet.create({
  fragmentContainer: {
    // flex: 1,
    backgroundColor: 'white',
  },
  headerContainer: {
    backgroundColor: '#08152D',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15,
  },
  headerLogo: {
    height: 22,
  },
});
