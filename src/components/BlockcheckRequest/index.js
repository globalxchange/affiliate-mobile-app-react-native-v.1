/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {StyleSheet} from 'react-native';
import BottomSheetLayout from '../../layouts/BottomSheetLayout';
import BlockcheckRequestReceive from './BlockcheckRequestReceive';
import BlockcheckRequestSend from './BlockcheckRequestSend';

const BlockcheckRequest = ({isOpen, setIsOpen, isSend}) => {
  const [isScannerOpen, setIsScannerOpen] = useState(false);

  useEffect(() => {
    if (!isOpen) {
      setIsScannerOpen(false);
    }
  }, [isOpen]);

  return (
    <BottomSheetLayout
      isOpen={isOpen}
      onClose={() => setIsOpen(false)}
      reactToKeyboard>
      {isSend ? (
        <BlockcheckRequestSend
          isScannerOpen={isScannerOpen}
          setIsScannerOpen={setIsScannerOpen}
          setIsOpen={setIsOpen}
        />
      ) : (
        <BlockcheckRequestReceive
          isScannerOpen={isScannerOpen}
          setIsScannerOpen={setIsScannerOpen}
          setIsOpen={setIsOpen}
        />
      )}
    </BottomSheetLayout>
  );
};

export default BlockcheckRequest;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    // flex: 1,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    overflow: 'hidden',
    backgroundColor: '#08152D',
  },
});
