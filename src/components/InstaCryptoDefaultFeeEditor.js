import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Keyboard, StyleSheet, Text, TextInput, View} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import {GX_API_ENDPOINT} from '../configs';
import ThemeData from '../configs/ThemeData';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';
import FilledButton from './FilledButton';
import ProfileAvatar from './ProfileAvatar';

const InstaCryptoDefaultFeeEditor = ({user, setIsLoading}) => {
  const navigation = useNavigation();

  const [defaultFee, setDefaultFee] = useState('0');
  const [customFee, setCustomFee] = useState('');

  useEffect(() => {
    setDefaultFee((user?.default_fee || 0).toString());
  }, [user]);

  const onSubmitHandler = async () => {
    const fee = parseFloat(customFee);

    if (!fee) {
      return WToast.show({
        data: 'Please Enter A Valid Fee',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);
    Keyboard.dismiss();

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    const postData = {
      email,
      token,
      customer_email: user?.email,
      fee_percentage: fee,
      default_override: true,
    };

    axios
      .post(
        `${GX_API_ENDPOINT}/coin/vault/service/set/fees/user/per/customer`,
        postData,
      )
      .then(({data}) => {
        console.log('updating default Fee', data);
        if (data.status) {
          navigation.goBack();
        } else {
          WToast.show({data: data.message, position: WToast.position.TOP});
        }
      })
      .catch((error) => {})
      .finally(() => setIsLoading(false));
  };

  return (
    <View style={styles.container}>
      <View style={[styles.userHeader]}>
        <ProfileAvatar name={user?.name} avatar={user?.profile_img} size={50} />
        <View style={{flex: 1, marginLeft: 10}}>
          <Text style={styles.userName}>{user?.name}</Text>
          <Text
            numberOfLines={1}
            adjustsFontSizeToFit
            style={styles?.userEmail}>
            {user?.email}
          </Text>
        </View>
      </View>
      <View style={[styles.currencyPickerContainer]}>
        <Text style={styles.currencyPickerTitle}>
          Your Default Fees For {user?.name} Is
        </Text>
        <View style={styles.inputContainer}>
          <TextInput
            placeholder="You Default Fee"
            placeholderTextColor="#9A9A9A"
            style={styles.input}
            value={defaultFee}
            editable={false}
            onChangeText={(text) => setDefaultFee(text)}
          />
          <Text style={styles.inputCurrency}>%</Text>
        </View>
      </View>
      <View style={styles.currencyPickerContainer}>
        <Text style={styles.currencyPickerTitle}>
          Your New Default Fee Will Be
        </Text>
        <View style={styles.inputContainer}>
          <TextInput
            placeholder="0.00"
            placeholderTextColor="#9A9A9A"
            style={styles.input}
            value={customFee}
            onChangeText={(text) => setCustomFee(text)}
            autoFocus
          />
          <Text style={styles.inputCurrency}>%</Text>
        </View>
      </View>
      <FilledButton
        onPress={onSubmitHandler}
        title="Submit"
        style={{
          flex: 0,
          width: 150,
          marginTop: 20,
        }}
      />
    </View>
  );
};

export default InstaCryptoDefaultFeeEditor;

const styles = StyleSheet.create({
  container: {flex: 1, paddingHorizontal: 30, paddingVertical: 20},
  userHeader: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  userName: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 18,
    color: ThemeData.APP_MAIN_COLOR,
    marginBottom: 3,
  },
  userEmail: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 13,
    color: '#9A9A9A',
  },
  currencyPickerContainer: {
    marginTop: 40,
  },
  currencyPickerTitle: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    marginBottom: 20,
  },
  currencyItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginRight: 10,
  },
  coinImage: {
    width: 18,
    height: 18,
    marginRight: 5,
    borderRadius: 9,
  },
  coinName: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 25,
  },
  inputCurrency: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
  },
  input: {
    flex: 1,
    height: 40,
    borderRightColor: ThemeData.BORDER_COLOR,
    borderRightWidth: 1,
    marginRight: 25,
    fontFamily: ThemeData.FONT_NORMAL,
  },
});
