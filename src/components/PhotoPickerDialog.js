import React from 'react';
import {
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';

const PhotoPickerDialog = ({isOpen, setIsOpen, callBack}) => {
  const onButtonClick = (bool) => {
    callBack(bool);
  };

  return (
    <Modal
      animationType="fade"
      transparent
      hardwareAccelerated
      statusBarTranslucent
      visible={isOpen}
      onDismiss={() => setIsOpen(false)}
      onRequestClose={() => setIsOpen(false)}>
      <TouchableOpacity
        activeOpacity={1}
        style={styles.overlay}
        onPress={() => setIsOpen(false)}
        onPressOut={() => {}}>
        <TouchableWithoutFeedback style={{flex: 1}}>
          <View style={[styles.modalContainer]}>
            <Text style={styles.modalTitle}>Select A Photo</Text>
            <TouchableOpacity
              onPress={() => onButtonClick(true)}
              style={[
                styles.modalButton,
                styles.borderTop,
                styles.borderBottom,
              ]}>
              <Text style={styles.modalButtonText}>Take Photo</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => onButtonClick(false)}
              style={[styles.modalButton, styles.borderBottom]}>
              <Text style={styles.modalButtonText}>Choose From Library</Text>
            </TouchableOpacity>
          </View>
        </TouchableWithoutFeedback>
      </TouchableOpacity>
    </Modal>
  );
};

export default PhotoPickerDialog;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'center',
  },
  modalContainer: {
    // flex: 1,
    backgroundColor: 'white',
    overflow: 'hidden',
    marginHorizontal: 20,
    borderRadius: 16,
    paddingVertical: 30,
  },
  modalTitle: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    textAlign: 'center',
    marginBottom: 20,
    fontSize: 20,
  },
  modalButton: {
    paddingVertical: 20,
  },
  modalButtonText: {
    color: '#08152D',
    textAlign: 'center',
    fontFamily: 'Montserrat-SemiBold',
  },
  borderBottom: {
    borderBottomColor: '#EBEBEB',
    borderBottomWidth: 1,
  },
  borderTop: {
    borderTopColor: '#EBEBEB',
    borderTopWidth: 1,
  },
});
