/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import ThemeData from '../configs/ThemeData';
import {getPlaceholderText, getUriImage} from '../utils';

const ProfileAvatar = ({size = 40, name, avatar, bgColor}) => {
  return (
    <>
      {avatar ? (
        <Image
          style={{width: size, height: size, borderRadius: size / 2}}
          source={{uri: avatar}}
          resizeMode="cover"
        />
      ) : (
        <View
          style={{
            width: size,
            height: size,
            borderRadius: size / 2,
            backgroundColor: bgColor || ThemeData.APP_MAIN_COLOR,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            numberOfLines={1}
            adjustsFontSizeToFit
            style={{
              fontFamily: ThemeData.FONT_SEMI_BOLD,
              fontSize: size / 2,
              color: 'white',
            }}>
            {getPlaceholderText(name)}
          </Text>
        </View>
      )}
    </>
  );
};

export default ProfileAvatar;

const styles = StyleSheet.create({});
