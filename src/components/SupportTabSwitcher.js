import React, {useContext, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  TouchableOpacity,
} from 'react-native';
import {supportTabs} from '../configs';
import {SupportContext} from '../contexts/SupportContext';
import {AppContext} from '../contexts/AppContextProvider';
import {useNavigation} from '@react-navigation/core';

const SupportTabSwitcher = () => {
  const {navigate} = useNavigation();

  const {isLoggedIn, isVideoFullScreen} = useContext(AppContext);

  const {activeTab, setActiveTab} = useContext(SupportContext);

  useEffect(() => {
    if (!isLoggedIn) {
      setActiveTab(supportTabs[0]);
    }
  }, []);

  const openMenu = () => {};

  return (
    <View
      style={[
        styles.container,
        {display: isVideoFullScreen ? 'none' : 'flex'},
      ]}>
      {supportTabs.map((item, index) => (
        <React.Fragment key={item.title}>
          <TouchableWithoutFeedback
            style={styles.itemContainer}
            onPress={() => {
              if (item.loginRequired) {
                if (isLoggedIn) {
                  setActiveTab({...item, key: Date.now()});
                } else {
                  navigate('Landing');
                }
              } else {
                setActiveTab({...item, key: Date.now()});
              }
            }}>
            <View style={styles.item}>
              <View
                style={[
                  styles.iconContainer,
                  item.title === activeTab.title && styles.itemActive,
                ]}>
                <Image
                  style={styles.icon}
                  source={item.icon}
                  resizeMode="contain"
                />
              </View>
              <Text
                style={[
                  styles.title,
                  item.title === activeTab.title && styles.titleActive,
                ]}>
                {item.title}
              </Text>
            </View>
          </TouchableWithoutFeedback>
          {index === 1 && (
            <View style={styles.brokerButtonContainer} key="blockButton">
              <TouchableOpacity
                onPress={() => setActiveTab({title: 'Menu', key: Date.now()})}
                style={styles.brokerButton}>
                <Image
                  source={require('../assets/support-category-icons/chats-io-white.png')}
                  resizeMode="contain"
                  style={styles.brokerIcon}
                />
              </TouchableOpacity>
            </View>
          )}
        </React.Fragment>
      ))}
    </View>
  );
};

export default SupportTabSwitcher;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderTopColor: '#EBEBEB',
    borderTopWidth: 1,
    paddingHorizontal: 10,
    alignItems: 'center',
    backgroundColor: 'white',
    height: 70,
  },
  itemContainer: {
    flex: 1,
    paddingHorizontal: 5,
    backgroundColor: 'red',
  },
  item: {
    flex: 1,
    alignItems: 'center',
  },
  iconContainer: {
    height: 30,
    width: 30,
    borderRadius: 3,
    opacity: 0.5,
  },
  itemActive: {
    opacity: 1,
    height: 35,
    width: 35,
  },
  icon: {flex: 1, width: null, height: null},
  title: {
    textAlign: 'center',
    color: '#08152D',
    marginTop: 5,
    fontSize: 12,
    display: 'none',
    fontFamily: 'Montserrat-SemiBold',
  },
  titleActive: {
    display: 'flex',
  },
  brokerButtonContainer: {
    marginBottom: -19,
    marginTop: -35,
    marginVertical: 10,
  },
  brokerButton: {
    backgroundColor: '#08152D',
    width: 75,
    height: 75,
    borderRadius: 37.5,
    padding: 19,
    borderWidth: 0.5,
    borderColor: 'white',
  },
  brokerIcon: {
    flex: 1,
    height: null,
    width: null,
  },
});
