import React, {useEffect, useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import LoadingAnimation from './LoadingAnimation';
import * as WebBrowser from 'expo-web-browser';
import Clipboard from '@react-native-community/clipboard';
import Axios from 'axios';
import BottomSheetLayout from '../layouts/BottomSheetLayout';

const AssetsIoInstaller = ({isOpen, setIsOpen}) => {
  const [links, setLinks] = useState();
  const [isCopied, setIsCopied] = useState(false);

  useEffect(() => {
    Axios.get(
      'https://storeapi.apimachine.com/dynamic/Globalxchangetoken/InviteCarousalforms',
      {
        params: {key: '4c69ba17-af5c-4a5c-a495-9a762aba1142'},
      },
    )
      .then((resp) => {
        const {data} = resp;

        // console.log('Data', data);

        if (data.success) {
          const assetIoData = data.data?.find(
            (item) => (item.Key = 'assets.io'),
          );

          if (assetIoData) {
            setLinks({
              link: assetIoData.formData.Link,
              opportunity: assetIoData.formData.Oppurtunity,
            });
          }
        }
      })
      .catch((error) => {});
  }, []);

  const onGoClick = () => {
    WebBrowser.openBrowserAsync(links?.link);
  };

  const onCopyClick = () => {
    Clipboard.setString(links?.link);
    setIsCopied(true);
    setTimeout(() => {
      setIsCopied(false);
    }, 10000);
  };

  const onOpportunity = () => {
    WebBrowser.openBrowserAsync(links?.opportunity);
  };

  return (
    <BottomSheetLayout isOpen={isOpen} onClose={() => setIsOpen(false)}>
      {links ? (
        <View style={styles.fragmentContainer}>
          <Image
            style={styles.headerLogo}
            source={require('../assets/assets-io-icon.png')}
            resizeMode="contain"
          />
          <Text style={styles.headerText}>
            As A AffiliateApp member, you can login to any of the GXLive App
          </Text>
          <TouchableOpacity onPress={onOpportunity} style={styles.actionButton}>
            <Image
              source={require('../assets/direct-earning-icon.png')}
              resizeMode="contain"
              style={styles.actionIcon}
            />
            <Text style={styles.actionText}>Opportunity</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={onGoClick} style={styles.actionButton}>
            <Image
              source={require('../assets/desktop-icon.png')}
              resizeMode="contain"
              style={styles.actionIcon}
            />
            <Text style={[styles.actionText]}>Go To Site</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={onCopyClick} style={styles.actionButton}>
            <Image
              source={require('../assets/copy-icon.png')}
              resizeMode="contain"
              style={styles.actionIcon}
            />
            <Text style={[styles.actionText]}>
              {isCopied ? 'Copied' : 'Copy Link'}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setIsOpen(false)}
            style={styles.closeButton}>
            <Text style={styles.closeText}>Close</Text>
          </TouchableOpacity>
        </View>
      ) : (
        <View style={styles.loadingAnimation}>
          <LoadingAnimation />
        </View>
      )}
    </BottomSheetLayout>
  );
};

export default AssetsIoInstaller;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    backgroundColor: '#08152D',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    overflow: 'hidden',
  },
  fragmentContainer: {
    backgroundColor: 'white',
    paddingTop: 30,
    alignItems: 'center',
  },
  headerLogo: {
    height: 60,
    width: 200,
  },
  headerText: {
    textAlign: 'center',
    fontFamily: 'Montserrat',
    color: '#08152D',
    marginTop: 20,
    paddingHorizontal: 40,
  },
  actionButton: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#EDEDED',
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 15,
    width: 200,
    justifyContent: 'center',
    marginTop: 25,
  },
  actionIcon: {
    height: 25,
    width: 25,
  },
  actionText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#555555',
    fontSize: 16,
    marginLeft: 15,
  },
  closeButton: {
    marginTop: 40,
    backgroundColor: '#464B4E',
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  closeText: {
    fontFamily: 'Montserrat-Bold',
    color: 'white',
  },
  loadingAnimation: {
    height: 350,
    justifyContent: 'center',
    backgroundColor: 'white',
  },
});
