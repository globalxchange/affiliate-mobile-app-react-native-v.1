import React, {useState, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Dimensions,
  ScrollView,
  SectionList,
} from 'react-native';
import Swipeable from 'react-native-swipeable';
import TransactionDetails from './TransactionDetails';
import Moment from 'moment-timezone';
import {formatterHelper} from '../../utils';
import {useNavigation} from '@react-navigation/native';
import {AppContext} from '../../contexts/AppContextProvider';
import FastImage from 'react-native-fast-image';

const {width, height} = Dimensions.get('window');

const TransactionList = ({selectedCategory, setSelectedCategory}) => {
  const [selectedTxn, setSelectedTxn] = useState();

  const onBackPress = () => {
    setSelectedCategory();
  };

  if (selectedTxn) {
    return (
      <TransactionDetails
        selectedTxn={selectedTxn}
        setSelectedTxn={setSelectedTxn}
      />
    );
  }

  const {type, status, list} = selectedCategory;

  let tempList = list || [];

  const addedList = [];

  const sortedList = [];

  tempList.forEach((tmpItem) => {
    if (!addedList.includes(tmpItem)) {
      const date = Moment(tmpItem.timestamp);
      const subList = [];
      addedList.push(tmpItem);
      subList.push(tmpItem);

      tempList.forEach((item) => {
        if (!addedList.includes(item)) {
          const itemDate = Moment(item.timestamp);
          if (date.isSame(itemDate, 'day')) {
            addedList.push(item);
            subList.push(item);
          }
        }
      });

      sortedList.push({title: date.format('dddd MMMM Do YYYY'), data: subList});
    }
  });

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={onBackPress} style={styles.actionButton}>
          <Image
            source={require('../../assets/back-icon-white.png')}
            style={styles.actionIcon}
            resizeMode="contain"
          />
        </TouchableOpacity>
        <Text style={styles.headerText}>{type}</Text>
      </View>
      {selectedCategory.list && selectedCategory.list.length === 0 ? (
        <View style={[styles.emptyContainer]}>
          <Text style={styles.emptyText}>Not Transactions Found</Text>
        </View>
      ) : (
        <SectionList
          showsVerticalScrollIndicator={false}
          style={styles.transactionList}
          sections={sortedList}
          keyExtractor={(item, index) => item + index}
          renderItem={({item}) => (
            <TransactionItem item={item} setSelectedTxn={setSelectedTxn} />
          )}
          renderSectionHeader={({section: {title}}) => (
            <View style={styles.transactionGroup}>
              <Text style={styles.date}>{title}</Text>
            </View>
          )}
        />
      )}
    </View>
  );
};

const TransactionItem = ({item, setSelectedTxn}) => {
  const navigation = useNavigation();
  const {cryptoTableData} = useContext(AppContext);

  let icon = require('../../assets/bitcoin-icon.png');

  if (cryptoTableData) {
    const coin = cryptoTableData.find((x) => x.coinSymbol === item.coin);
    if (coin) {
      icon = {uri: coin.coinImage};
    }
  }

  const openTimeline = () => {
    navigation.navigate('Timeline', {screen: 'List', params: {data: item}});
  };

  const rightButtons = [
    <TouchableOpacity onPress={openTimeline} style={styles.swipeButton}>
      <Text style={styles.slideText}>{item.status}</Text>
    </TouchableOpacity>,
  ];

  return (
    <Swipeable rightButtons={rightButtons}>
      <View style={styles.transactionItem}>
        <Image source={icon} style={styles.txnIcon} resizeMode="contain" />
        <Text style={styles.txnAsset}>{item.coin}</Text>
        <Text style={styles.txnValue}>
          {formatterHelper(
            item.deposit ? item.amt_to_credit : item.final_buy_amount,
            item.coin,
          )}
        </Text>
      </View>
    </Swipeable>
  );
};
export default TransactionList;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    width: width * 0.85,
    height: height * 0.65,
  },
  header: {
    backgroundColor: '#08152D',
    flexDirection: 'row',
    paddingVertical: 12,
    paddingHorizontal: 20,
    justifyContent: 'space-between',
  },
  headerText: {
    fontFamily: 'Montserrat-Bold',
    color: 'white',
    textAlign: 'center',
    flex: 1,
  },
  actionButton: {
    width: 25,
    padding: 3,
    position: 'absolute',
    zIndex: 6,
    left: 20,
    top: 0,
    bottom: 0,
  },
  actionIcon: {
    flex: 1,
    width: null,
    height: null,
  },
  transactionList: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
  },
  transactionGroup: {
    padding: 15,
  },
  date: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    fontSize: 12,
  },
  transactionItem: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    alignItems: 'center',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    marginHorizontal: 20,
    paddingVertical: 10,
    marginBottom: 10,
  },
  txnIcon: {
    width: 22,
    height: 22,
  },
  txnAsset: {
    flex: 1,
    marginHorizontal: 10,
    fontFamily: 'Montserrat-SemiBold',
    color: '#001D41',
  },
  txnValue: {
    color: '#001D41',
    fontFamily: 'Roboto',
    fontSize: 12,
    opacity: 0.7,
  },
  swipeButton: {
    justifyContent: 'center',
    flex: 1,
  },
  slideText: {
    color: '#001D41',
    marginBottom: 10,
    paddingRight: 50,
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
  emptyContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    flex: 1,
  },
  emptyText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    fontSize: 18,
  },
});
