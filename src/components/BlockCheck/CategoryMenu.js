import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import TransactionsMenu from './TransactionsMenu';

const {width} = Dimensions.get('window');

const CategoryMenu = ({menus, isOpen, isSend, setIsSend}) => {
  const [activeMenu, setActiveMenu] = useState();

  useEffect(() => {
    if (!isOpen) {
      setActiveMenu();
    }
  }, [isOpen]);

  if (activeMenu) {
    return (
      <TransactionsMenu
        isSend={isSend}
        setIsSend={setIsSend}
        activeMenu={activeMenu}
        setActiveMenu={setActiveMenu}
      />
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Image
          style={styles.blockIcon}
          source={require('../../assets/block-check-icon.png')}
          resizeMode="contain"
        />
        <Text style={styles.headerText}>LOCKCHECK</Text>
      </View>
      <View style={styles.menuList}>
        {menus.map((item) => (
          <TouchableOpacity
            key={item}
            onPress={() => setActiveMenu(item)}
            style={styles.menuItem}>
            <Text style={styles.itemText}>{item}</Text>
          </TouchableOpacity>
        ))}
      </View>
    </View>
  );
};

export default CategoryMenu;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    width: width * 0.75,
  },

  header: {
    backgroundColor: '#08152D',
    flexDirection: 'row',
    paddingVertical: 12,
    paddingHorizontal: 50,
    justifyContent: 'center',
  },
  blockIcon: {
    width: 16,
    height: 16,
    marginRight: 2,
  },
  headerText: {
    fontFamily: 'Montserrat-Bold',
    color: 'white',
  },
  menuList: {},
  menuItem: {
    paddingVertical: 50,
    borderBottomColor: '#08152D',
    borderBottomWidth: 1,
    borderLeftColor: '#08152D',
    borderLeftWidth: 1,
    borderRightColor: '#08152D',
    borderRightWidth: 1,
    backgroundColor: 'white',
  },
  itemText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
    fontSize: 16,
  },
});
