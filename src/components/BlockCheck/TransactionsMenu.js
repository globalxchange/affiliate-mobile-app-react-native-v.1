import React, {useState, useEffect, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import TransactionList from './TransactionList';
import {AppContext} from '../../contexts/AppContextProvider';

const {width} = Dimensions.get('window');

const TransactionsMenu = ({activeMenu, setActiveMenu, setIsSend}) => {
  const {blockCheckData, setIsBlockSheetOpen} = useContext(AppContext);

  // console.log('blockCheckData', blockCheckData);

  const {
    processingCryptoDeposit,
    processingCryptoWithdraw,
    processingFaitDeposit,
    processingFaitWithdraw,
  } = blockCheckData;

  const [selectedCategory, setSelectedCategory] = useState();

  useEffect(() => {
    if (activeMenu === 'Crypto') {
    }
  }, [activeMenu]);

  const onBackPress = () => {
    setActiveMenu();
  };

  const onItemPress = (type, status, list) => {
    setSelectedCategory({type, status, list});
  };

  if (selectedCategory) {
    return (
      <TransactionList
        selectedCategory={selectedCategory}
        setSelectedCategory={setSelectedCategory}
      />
    );
  }

  const miniList = [
    {
      title: 'Processing',
      status: [
        {
          title: 'Deposit',
          list:
            activeMenu === 'Crypto'
              ? [...processingCryptoDeposit]
              : [...processingFaitDeposit],
        },
        {
          title: 'Withdrawals',
          list:
            activeMenu === 'Crypto'
              ? [...processingCryptoWithdraw]
              : [...processingFaitWithdraw],
        },
      ],
    },
  ];

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={onBackPress} style={styles.actionButton}>
          <Image
            source={require('../../assets/back-icon-white.png')}
            style={styles.actionIcon}
            resizeMode="contain"
          />
        </TouchableOpacity>
        <Text style={styles.headerText}>{activeMenu}</Text>
      </View>
      <View style={styles.menuList}>
        {miniList.map((item) => (
          <View key={item.title} style={styles.listContainer}>
            <Text style={styles.listHeader}>{item.title}</Text>
            {item.status.map((statusItem) => (
              <TouchableOpacity
                key={statusItem.title}
                style={styles.listItem}
                onPress={() =>
                  onItemPress(statusItem.title, item.title, statusItem.list)
                }>
                <Text style={styles.listText}>{statusItem.title}</Text>
                <Text style={styles.listValue}>{statusItem.list.length}</Text>
              </TouchableOpacity>
            ))}
            {activeMenu === 'Crypto' && (
              <View style={styles.listContainer}>
                <View style={styles.headerContainer}>
                  <Text style={styles.listHeader}>Create A </Text>
                  <Image
                    style={styles.blockIcon}
                    source={require('../../assets/block-check-colored.png')}
                    resizeMode="contain"
                  />
                  <Text style={styles.listHeader}>LOCKCHECK</Text>
                </View>
                <TouchableOpacity
                  onPress={() => {
                    setIsSend(true);
                    setIsBlockSheetOpen(true);
                  }}
                  style={[styles.button, {marginTop: 0}]}>
                  <Image
                    style={styles.buttonIcon}
                    source={require('../../assets/block-check-send.png')}
                    resizeMode="contain"
                  />
                  <Text style={styles.buttonText}>Send</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    setIsSend(false);
                    setIsBlockSheetOpen(true);
                  }}
                  style={styles.button}>
                  <Image
                    style={styles.buttonIcon}
                    source={require('../../assets/block-check-recieve.png')}
                    resizeMode="contain"
                  />
                  <Text style={styles.buttonText}>Receive</Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        ))}
      </View>
    </View>
  );
};

export default TransactionsMenu;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    width: width * 0.75,
  },
  header: {
    backgroundColor: '#08152D',
    flexDirection: 'row',
    paddingVertical: 12,
    paddingHorizontal: 20,
  },
  headerText: {
    fontFamily: 'Montserrat-Bold',
    color: 'white',
    textAlign: 'center',
    flex: 1,
  },
  actionButton: {
    width: 25,
    padding: 3,
    position: 'absolute',
    zIndex: 6,
    left: 20,
    top: 0,
    bottom: 0,
  },
  actionIcon: {
    flex: 1,
    width: null,
    height: null,
  },
  menuList: {
    borderColor: '#08152D',
    borderWidth: 1,
  },
  listContainer: {
    padding: 12,
  },
  listHeader: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
    marginBottom: 15,
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
    paddingVertical: 10,
  },
  listText: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
  listValue: {
    color: '#08152D',
    fontFamily: 'Roboto',
    fontSize: 12,
  },
  separator: {
    backgroundColor: '#08152D',
    height: 1,
    marginHorizontal: -12,
  },
  headerContainer: {
    flexDirection: 'row',
  },
  blockIcon: {
    width: 15,
    height: 19,
    marginRight: 2,
  },
  button: {
    flexDirection: 'row',
    marginTop: 5,
    alignItems: 'center',
    borderColor: '#F1F4F6',
    borderWidth: 1,
    padding: 15,
  },
  buttonIcon: {
    width: 25,
    height: 30,
  },
  buttonText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    marginLeft: 15,
  },
});
