import React, {useState, useEffect, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';
import {AppContext} from '../../contexts/AppContextProvider';
import * as WebBrowser from 'expo-web-browser';
import Axios from 'axios';

const {width, height} = Dimensions.get('window');

const TransactionDetails = ({selectedTxn, setSelectedTxn}) => {
  const {pathData} = useContext(AppContext);

  const [isExpanded, setIsExpanded] = useState(false);
  const [currentStep, setCurrentStep] = useState('');
  const [currentInstitution, setCurrentInstitution] = useState('');

  useEffect(() => {
    const pathId = selectedTxn.path_id;

    const currentPath = pathData.find((item) => item.path_id === pathId);

    if (currentPath) {
      const steps = currentPath.total_steps;
      if (steps) {
        const entries = Object.entries(steps);

        for (const [key, stepData] of entries) {
          if (stepData.status === selectedTxn.status) {
            setCurrentStep(stepData);
            break;
          }
        }
      }
      const instituteId = currentPath.institution_id;
      Axios.get('https://accountingtool.apimachine.com/getlist-of-institutes')
        .then((resp) => {
          const {data} = resp;
          if (data.status) {
            data.data.forEach((item) => {
              if (item._id === instituteId) {
                setCurrentInstitution(item);
                return;
              }
            });
          }
        })
        .catch((error) => {
          console.log('Error on getting institute details', error);
        });
    }
  }, [selectedTxn, pathData]);

  const onBackPress = () => {
    setSelectedTxn();
  };

  console.log('currentStep', currentStep);

  return (
    <View style={styles.container}>
      {currentStep ? (
        <>
          <View style={styles.detailsContainer}>
            <View style={styles.bankerIconContainer}>
              <Image style={styles.bankerIcon} />
            </View>
            <Text style={styles.bankerName}>{currentStep.name}</Text>
            <View style={styles.actionContainer}>
              <TouchableOpacity style={styles.actionButton}>
                <Image
                  source={require('../../assets/mail-icon-colored.png')}
                  style={styles.actionIcon}
                  resizeMode="contain"
                />
              </TouchableOpacity>
              <TouchableOpacity style={styles.actionButton}>
                <Image
                  source={require('../../assets/chats-io-icon-colored.png')}
                  style={styles.actionIcon}
                  resizeMode="contain"
                />
              </TouchableOpacity>
              <TouchableOpacity style={styles.actionButton}>
                <Image
                  source={require('../../assets/onhold-icon-colored.png')}
                  style={styles.actionIcon}
                  resizeMode="contain"
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  WebBrowser.openBrowserAsync(currentStep.howtovideolink || '')
                }
                style={styles.actionButton}>
                <Image
                  source={require('../../assets/youtube-icon-colored.png')}
                  style={styles.actionIcon}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            </View>
            <Text style={styles.bankerDetails}>
              {currentStep.description || ''}
            </Text>
          </View>
          {currentInstitution ? (
            <View style={styles.relatedContainer}>
              <Text style={styles.relatedHeader}>Related Institution</Text>
              <View style={styles.relatedItem}>
                <View style={styles.imageContainer}>
                  <Image
                    source={{uri: currentInstitution.profile_image}}
                    style={styles.images}
                  />
                </View>
                <View style={styles.relatedDetailsContainer}>
                  <Text style={styles.relatedBankerHeader}>
                    {currentInstitution.institute_name}
                  </Text>
                  <Text style={styles.relatedBankerName}>Canada</Text>
                  <Text style={styles.relatedLeanMore}>
                    Learn More About The Involvement Of This Institution
                  </Text>
                </View>
              </View>
            </View>
          ) : null}
          <View style={styles.screenshotContainer}>
            <Text style={styles.relatedHeader}>Screenshots</Text>
            <FlatList
              showsHorizontalScrollIndicator={false}
              horizontal
              data={currentStep.screenshots || []}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({item, index}) => (
                <View style={styles.screenshotItem}>
                  <Image
                    source={{uri: item}}
                    style={styles.screenshotText}
                    resizeMode="cover"
                  />
                </View>
              )}
              ListEmptyComponent={
                <View style={styles.emptyContainer}>
                  <Text style={styles.emptyText}>No Screenshots Available</Text>
                </View>
              }
            />
          </View>
        </>
      ) : (
        <View style={styles.loadingContainer}>
          <Text style={styles.fetchingText}>Fetching Details</Text>
        </View>
      )}
      <TouchableOpacity style={styles.button}>
        <Text style={styles.buttonText}>Current Step</Text>
      </TouchableOpacity>
    </View>
  );
};

export default TransactionDetails;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    width: width * 0.85,
    maxHeight: height * 0.85,
    borderColor: '#EBEBEB',
    borderWidth: 1,
  },
  detailsContainer: {
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 40,
  },
  bankerIconContainer: {
    height: 60,
    width: 60,
    backgroundColor: '#C4C4C4',
    borderRadius: 30,
    overflow: 'hidden',
    marginBottom: 10,
  },
  bankerIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  bankerName: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 22,
    marginBottom: 10,
  },
  actionContainer: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  actionButton: {
    marginHorizontal: 10,
    height: 30,
    width: 30,
    padding: 5,
  },
  actionIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  bankerDetails: {
    textAlign: 'center',
    fontFamily: 'Montserrat',
    fontSize: 11,
    color: '#9A9A9A',
  },
  button: {
    backgroundColor: '#08152D',
  },
  buttonText: {
    textAlign: 'center',
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
    paddingVertical: 10,
    fontSize: 13,
  },
  relatedContainer: {
    paddingHorizontal: 20,
    marginBottom: 15,
  },
  relatedHeader: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    marginBottom: 5,
  },
  relatedItem: {
    flexDirection: 'row',
    borderColor: '#EBEBEB',
    borderWidth: 1,
  },
  imageContainer: {
    height: '100%',
    width: 90,
    borderRightColor: '#EBEBEB',
    borderRightWidth: 1,
  },
  images: {
    flex: 1,
    height: null,
    width: null,
  },
  relatedDetailsContainer: {
    flex: 1,
    padding: 10,
  },
  relatedBankerHeader: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
  },
  relatedBankerName: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 10,
    marginBottom: 10,
  },
  relatedLeanMore: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 8,
    textDecorationLine: 'underline',
  },
  screenshotContainer: {
    paddingHorizontal: 20,
    marginBottom: 15,
  },
  screenshotItem: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    height: 100,
    width: 100,
    marginRight: 5,
  },
  screenshotText: {
    flex: 1,
    height: null,
    width: null,
  },
  loadingContainer: {},
  fetchingText: {},
  emptyContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 30,
  },
  emptyText: {
    fontSize: 10,
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    textAlign: 'center',
  },
});
