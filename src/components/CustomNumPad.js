import React, {useRef, useContext, useEffect} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {Transitioning, Transition} from 'react-native-reanimated';
import {AppContext} from '../contexts/AppContextProvider';
import {WToast} from 'react-native-smart-tip';
import Clipboard from '@react-native-community/clipboard';

const numPadButtons = [
  {
    row: [
      {value: '1', isControl: false},
      {value: '2', isControl: false},
      {value: '3', isControl: false},
    ],
  },
  {
    row: [
      {value: '4', isControl: false},
      {value: '5', isControl: false},
      {value: '6', isControl: false},
    ],
  },
  {
    row: [
      {value: '7', isControl: false},
      {value: '8', isControl: false},
      {value: '9', isControl: false},
    ],
  },
  {
    row: [
      {value: '.', isControl: false},
      {value: '0', isControl: false},
      {
        value: require('../assets/numpad-back-icon.png'),
        isControl: true,
      },
    ],
  },
];

const CustomNumPad = () => {
  const {
    isCustomNumPadOpen,
    setIsNumPadOpen,
    setCustomKeyBoardCallBack,
    customKeyboardCallback,
  } = useContext(AppContext);

  const transitionViewRef = useRef();

  const transition = (
    <Transition.Together>
      <Transition.In
        type="slide-bottom"
        durationMs={500}
        interpolation="easeInOut"
      />
      <Transition.Out
        type="slide-bottom"
        durationMs={500}
        interpolation="easeInOut"
      />
      <Transition.Change durationMs={500} interpolation="linear" />
    </Transition.Together>
  );

  const keyPressHandler = (pressedKey) => {
    if (customKeyboardCallback) {
      customKeyboardCallback(pressedKey.toString());
    }
  };

  const copyClickHandler = () => {
    Clipboard.setString('');

    WToast.show({data: 'Copied to clipboard'});
  };

  const pasteClickHandler = async () => {
    const pastedString = await Clipboard.getString();

    const parsedValue = parseFloat(pastedString);

    if (parsedValue) {
      customKeyboardCallback(parsedValue.toString());
    }
  };

  useEffect(() => {
    if (!isCustomNumPadOpen) {
      setCustomKeyBoardCallBack(null);
    }
  }, [isCustomNumPadOpen]);

  if (transitionViewRef.current) {
    transitionViewRef.current.animateNextTransition();
  }

  return (
    <Transitioning.View
      style={[
        isCustomNumPadOpen ? {zIndex: 1} : {zIndex: -1},
        StyleSheet.absoluteFill,
      ]}
      ref={transitionViewRef}
      transition={transition}>
      {isCustomNumPadOpen && (
        <View style={{flex: 1}}>
          <TouchableOpacity
            style={styles.overlay}
            onPress={() => setIsNumPadOpen(false)}
          />
          <View style={{flexDirection: 'row'}}>
            <View style={styles.keyBoardContainer}>
              {numPadButtons.map((item, index) => (
                <View key={index} style={styles.numpadRow}>
                  {item.row.map((numKey, numKeyIndex) => (
                    <View key={numKeyIndex} style={styles.numKeyContainer}>
                      <TouchableOpacity
                        style={styles.numKey}
                        onPress={() =>
                          keyPressHandler(
                            numKey.isControl ? 'BACK' : numKey.value,
                          )
                        }>
                        {numKey.isControl ? (
                          <Image
                            style={styles.backIcon}
                            source={require('../assets/numpad-back-icon.png')}
                            resizeMode="contain"
                          />
                        ) : (
                          <Text style={styles.numText}>{numKey.value}</Text>
                        )}
                      </TouchableOpacity>
                    </View>
                  ))}
                </View>
              ))}
            </View>
            <View style={styles.numpadColum}>
              <View style={styles.columnContainer}>
                <TouchableOpacity
                  style={styles.numKey}
                  onPress={copyClickHandler}>
                  <Image
                    style={styles.backIcon}
                    source={require('../assets/copy-icon-white.png')}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
              </View>
              <View style={styles.columnContainer}>
                <TouchableOpacity
                  style={styles.numKey}
                  onPress={pasteClickHandler}>
                  <Image
                    style={styles.backIcon}
                    source={require('../assets/paste-icon.png')}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
              </View>
              <View style={styles.columnContainer}>
                <TouchableOpacity
                  style={styles.numKey}
                  onPress={() => keyPressHandler('CLEAR')}>
                  <Image
                    style={styles.backIcon}
                    source={require('../assets/clear-icon.png')}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
              </View>
              <View style={styles.columnContainer}>
                <TouchableOpacity
                  style={styles.numKey}
                  onPress={() => setIsNumPadOpen(false)}>
                  <Image
                    style={styles.backIcon}
                    source={require('../assets/hide-keyboard-icon.png')}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      )}
    </Transitioning.View>
  );
};

export default CustomNumPad;

const styles = StyleSheet.create({
  overlay: {
    flexGrow: 1,
    height: 0,
    zIndex: 1,
  },
  keyBoardContainer: {
    backgroundColor: '#08152D',
    paddingBottom: 15,
    flexGrow: 1,
    width: 0,
  },
  numpadRow: {flexDirection: 'row'},
  numpadColum: {
    backgroundColor: '#08152D',
    width: 70,
  },
  numKeyContainer: {
    width: '33.333%',
    height: 60,
  },
  columnContainer: {
    height: 60,
  },
  numKey: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    backgroundColor: '#08152D',
  },
  numText: {color: 'white', fontFamily: 'Montserrat-SemiBold', fontSize: 18},
  backIcon: {height: 20},
});
