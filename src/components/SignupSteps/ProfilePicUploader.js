import React, {useState, useEffect, useContext} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import {Permissions, FileSystem} from 'react-native-unimodules';
import Constants from 'expo-constants';
import {decode} from 'base64-arraybuffer';
import {S3} from 'aws-sdk';
import {WToast} from 'react-native-smart-tip';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import {GX_API_ENDPOINT, S3_CONFIG} from '../../configs';
import Axios from 'axios';
import {AppContext} from '../../contexts/AppContextProvider';
import {useNavigation} from '@react-navigation/native';
import PhotoPickerDialog from '../PhotoPickerDialog';
import ThemeData from '../../configs/ThemeData';

const ProfilePicUploader = ({setIsLoading, username}) => {
  const navigation = useNavigation();

  const {setLoginData} = useContext(AppContext);

  const [imageUri, setImageUri] = useState();
  const [isShowPickerDialog, setIsShowPickerDialog] = useState(false);

  useEffect(() => {
    (async () => {
      if (Constants.platform.ios) {
        const {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
          // alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

  const pickFileHandler = async (useCamera) => {
    try {
      let result;
      const options = {
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 0.8,
      };

      if (useCamera) {
        result = await ImagePicker.launchCameraAsync(options);
      } else {
        result = await ImagePicker.launchImageLibraryAsync(options);
      }
      // console.log('Result', result);
      if (!result.cancelled) {
        setIsShowPickerDialog(false);
        setImageUri(result.uri);
      }
    } catch (E) {
      console.log(E);
    }
  };

  const uploadHandler = () => {
    if (!imageUri) {
      return WToast.show({
        data: 'Please Select An Image First',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    const BUCKET_NAME = 'gxnitrousdata';

    const S3Client = new S3({
      ...S3_CONFIG,
      Bucket: BUCKET_NAME,
    });

    const fPath = imageUri;

    let options = {encoding: FileSystem.EncodingType.Base64};

    FileSystem.readAsStringAsync(fPath, options)
      .then((data) => {
        const arrayBuffer = decode(data);

        const params = {
          Bucket: BUCKET_NAME,
          Key: `brandlogos/${username}${Date.now()}.jpg`,
          Body: arrayBuffer,
          ContentType: 'image/jpeg',
          ACL: 'public-read',
        };

        S3Client.upload(params, async (err, s3Data) => {
          if (err) {
            console.log('Uploading Profile Pic Error', err);
          }

          if (s3Data.Location) {
            const email = await AsyncStorageHelper.getLoginEmail();
            const token = await AsyncStorageHelper.getAppToken();
            Axios.post(`${GX_API_ENDPOINT}/user/details/edit`, {
              email,
              field: 'profile_img',
              value: s3Data.Location,
              accessToken: token,
            })
              .then((resp) => {
                // console.log('Profile Update Success', resp.data);
                getUserDetails(email);
                navigation.navigate('Drawer', {screen: 'Earn'});
              })
              .catch((error) => {
                console.log('Profile Update Failed', error);
              })
              .finally(() => setIsLoading(false));
          }
          // console.log('Upload Success', s3Data);
        });
      })
      .catch((err) => {
        console.log('​getFile -> err', err);
        setIsLoading(false);
      });
  };

  const getUserDetails = (email) => {
    Axios.get(`${GX_API_ENDPOINT}/user/details/get`, {
      params: {email},
    })
      .then((res) => {
        const {data} = res;

        // console.log('User Details', data);

        AsyncStorageHelper.setUserName(data.user.name);
        AsyncStorageHelper.setAffId(data.user.affiliate_id);
        AsyncStorageHelper.setUserAvatarUrl(data.user.profile_img);
        setLoginData(data.user.name, true, data.user.profile_img);
      })
      .catch((error) => {
        console.log('getUserDetails Error', error);
      });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Congrats</Text>
      <Text style={styles.subHeader}>
        Your Almost Done, Just Upload A Profile Picture To Seal The Deal
      </Text>
      <TouchableOpacity
        style={styles.avatarButton}
        onPress={() => setIsShowPickerDialog(true)}>
        <Image
          style={styles.avatar}
          source={
            imageUri
              ? {uri: imageUri}
              : require('../../assets/user-icon-filled.png')
          }
          resizeMode="contain"
        />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.outlinedButton}
        onPress={() => setIsShowPickerDialog(true)}>
        <Text style={styles.outlinedText}>Upload Again</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.filledButton} onPress={uploadHandler}>
        <Text style={styles.filledText}>I'm Good To Go</Text>
      </TouchableOpacity>
      <PhotoPickerDialog
        isOpen={isShowPickerDialog}
        setIsOpen={setIsShowPickerDialog}
        callBack={(isCamera) => pickFileHandler(isCamera)}
      />
    </View>
  );
};

export default ProfilePicUploader;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold',
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 40,
    marginBottom: 5,
  },
  subHeader: {
    textAlign: 'center',
    fontFamily: 'Montserrat',
    color: '#999C9A',
    fontSize: 16,
  },
  avatarButton: {
    borderColor: ThemeData.APP_MAIN_COLOR,
    borderWidth: 1,
    marginVertical: 60,
    borderRadius: 70,
    width: 140,
    height: 140,
    overflow: 'hidden',
  },
  avatar: {
    flex: 1,
    width: null,
    height: null,
  },
  outlinedButton: {
    borderColor: ThemeData.APP_MAIN_COLOR,
    borderWidth: 1,
    paddingVertical: 10,
    width: 200,
    marginBottom: 10,
  },
  outlinedText: {
    fontFamily: 'Montserrat-SemiBold',
    color: ThemeData.APP_MAIN_COLOR,
    textAlign: 'center',
  },
  filledButton: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    paddingVertical: 10,
    width: 200,
  },
  filledText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
  },
});
