import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';

const BrokerDetails = ({brokerDetails, onYesClick, onNoCLick}) => {
  return (
    <View style={styles.container}>
      <View style={styles.viewContainer}>
        <Text style={styles.loginTitle}>AffiliateSync</Text>
        <Text style={styles.loginSubTitle}>
          Is This Your Sponsoring Broker?
        </Text>
        <View style={styles.brokerDetailsContainer}>
          <Image
            style={styles.brokerImage}
            resizeMode="contain"
            source={{uri: brokerDetails?.profile_img}}
          />
          <View>
            <Text style={styles.brokerName}>{brokerDetails?.name}</Text>
            <Text style={styles.brokerEmail}>{brokerDetails?.email}</Text>
          </View>
        </View>
      </View>
      <View style={styles.actionContainer}>
        <TouchableOpacity style={styles.buttonOutlined} onPress={onNoCLick}>
          <Text style={styles.buttonOutlineText}>No</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.loginButton} onPress={onYesClick}>
          <Text style={styles.loginButtonText}>Yes</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default BrokerDetails;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  viewContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  actionContainer: {
    flexDirection: 'row',
    marginBottom: 40,
  },
  loginButton: {
    marginTop: 15,
    backgroundColor: '#08152D',
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    flex: 1,
  },
  loginButtonText: {
    color: 'white',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  buttonOutlined: {
    marginTop: 15,
    borderColor: '#08152D',
    borderWidth: 1,
    flex: 1,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginRight: 15,
  },
  buttonOutlineText: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  loginTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 50,
    marginTop: 40,
    marginBottom: 10,
  },
  loginSubTitle: {
    color: '#999C9A',
    marginBottom: 40,
    fontFamily: 'Montserrat',
  },
  brokerDetailsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  brokerImage: {
    width: 50,
    height: 50,
    borderRadius: 25,
  },
  brokerName: {
    fontFamily: ThemeData.FONT_BOLD,
    marginLeft: 15,
    fontSize: 18,
  },
  brokerEmail: {
    fontFamily: ThemeData.FONT_NORMAL,
    marginLeft: 15,
    fontSize: 12,
  },
});
