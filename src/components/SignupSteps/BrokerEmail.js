import Clipboard from '@react-native-community/clipboard';
import Axios from 'axios';
import React, {useState} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import {GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import NextIconButton from './NextIconButton';

const BrokerEmail = ({onNext, setIsLoading}) => {
  const [emailInput, setEmailInput] = useState('');

  const onCopyClick = async () => {
    const text = await Clipboard.getString();
    setEmailInput(text);

    // Clipboard.setString('WhyIsShorupanSingle');
  };

  const onNextClick = () => {
    setIsLoading(true);

    const email = emailInput.toLowerCase().trim();

    Axios.get(`${GX_API_ENDPOINT}/user/details/get`, {
      params: {email},
    })
      .then(({data}) => {
        if (data.status) {
          onNext(data.user);
        } else {
          WToast.show({
            data: 'No User Found Associated With This Email',
            position: WToast.position.TOP,
          });
        }
      })
      .catch((error) => {
        console.log('Error on getting broker data', error);
        WToast.show({
          data: 'Please Check Your Internet Connection',
          position: WToast.position.TOP,
        });
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return (
    <View style={styles.container}>
      <View style={styles.viewContainer}>
        <Text style={styles.loginTitle}>AffiliateSync</Text>
        <Text style={styles.loginSubTitle}>
          Please Enter The Email From Your Broker
        </Text>
        <View style={styles.inputContainer}>
          <TextInput
            value={emailInput}
            onChangeText={(text) => setEmailInput(text)}
            style={styles.input}
            placeholderTextColor={'#878788'}
            placeholder="Enter Email.."
            textContentType="emailAddress"
          />

          <TouchableOpacity onPress={onCopyClick}>
            <Image
              source={require('../../assets/copy-icon.png')}
              style={styles.inputIcon}
              resizeMode="contain"
            />
          </TouchableOpacity>
          <NextIconButton style={{marginLeft: 15}} onNext={onNextClick} />
        </View>
      </View>
      {/* <TouchableOpacity style={styles.loginButton} onPress={onNextClick}>
        <Text style={styles.loginButtonText}>Next</Text>
      </TouchableOpacity> */}
    </View>
  );
};

export default BrokerEmail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  viewContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  loginButton: {
    marginTop: 15,
    marginBottom: 30,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    width: 150,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  loginButtonText: {
    color: 'white',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  loginTitle: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-Bold',
    fontSize: 50,
    marginTop: 40,
    marginBottom: 10,
  },
  loginSubTitle: {
    color: '#999C9A',
    marginBottom: 40,
    fontFamily: 'Montserrat',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#F1F4F6',
    borderWidth: 1,
    backgroundColor: 'white',
    borderRadius: 4,
    paddingHorizontal: 15,
    paddingVertical: 2,
    marginBottom: 20,
  },
  inputIcon: {
    height: 25,
    width: 25,
  },
  input: {
    flex: 1,
    height: 45,
    fontFamily: 'Montserrat',
    color: 'black',
    paddingHorizontal: 15,
  },
  subText: {
    fontFamily: 'Montserrat',
    color: ThemeData.APP_MAIN_COLOR,
    marginTop: 30,
  },
  subControlText: {
    fontFamily: 'Montserrat-Bold',
    color: ThemeData.APP_MAIN_COLOR,
    textDecorationLine: 'underline',
  },
});
