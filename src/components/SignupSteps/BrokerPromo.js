import Axios from 'axios';
import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import {GX_API_ENDPOINT} from '../../configs';
import LoadingAnimation from '../LoadingAnimation';
import BrokerDetails from './BrokerDetails';
import BrokerEmail from './BrokerEmail';
import BrokerSyncOption from './BrokerSyncOption';
import NoBrokerView from './NoBrokerView';
import PromoInput from './PromoInput';

const BrokerPromo = ({setAffID, gotoEmail}) => {
  const [activeView, setActiveView] = useState();
  const [brokerDetails, setBrokerDetails] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [promoText, setPromoText] = useState('');

  const findBroker = () => {
    setIsLoading(true);
    Axios.get(`${GX_API_ENDPOINT}/user/details/get`, {
      params: {username: promoText},
    })
      .then(({data}) => {
        if (data.status) {
          setActiveView('BrokerDetails');
          setBrokerDetails(data.user);
          setAffID(promoText);
        } else {
          setActiveView('NoBroker');
        }
      })
      .catch((error) => {
        console.log('Error on getting broker data', error);
        WToast.show({
          data: 'Please Check Your Internet Connection',
          position: WToast.position.TOP,
        });
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  if (isLoading) {
    return (
      <View style={[StyleSheet.absoluteFill, {justifyContent: 'center'}]}>
        <LoadingAnimation />
      </View>
    );
  }

  switch (activeView) {
    case 'BrokerDetails':
      return (
        <BrokerDetails
          brokerDetails={brokerDetails}
          onYesClick={() => gotoEmail()}
          onNoCLick={() => {
            setAffID('');
            setActiveView();
            setBrokerDetails('');
            setPromoText('');
          }}
        />
      );
    case 'NoBroker':
      return (
        <NoBrokerView
          promoText={promoText}
          onNext={() => setActiveView('AlternativeOption')}
        />
      );

    case 'AlternativeOption':
      return (
        <BrokerSyncOption
          goBack={() => {
            setAffID('');
            setActiveView();
            setBrokerDetails('');
            setPromoText('');
          }}
          gotoCheckMail={() => setActiveView('BrokerEmail')}
        />
      );

    case 'BrokerEmail':
      return (
        <BrokerEmail
          setIsLoading={setIsLoading}
          onNext={(data) => {
            setBrokerDetails(data);
            setAffID(data.username);
            setActiveView('BrokerDetails');
          }}
        />
      );

    default:
      return (
        <PromoInput
          onNext={findBroker}
          setPromoText={setPromoText}
          promoText={promoText}
        />
      );
  }
};

export default BrokerPromo;
