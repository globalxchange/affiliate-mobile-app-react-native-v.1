import React, {useState, useRef, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  TouchableOpacity,
  Keyboard,
  Image,
  BackHandler,
} from 'react-native';
import Animated, {
  Transitioning,
  Transition,
  interpolate,
  useCode,
  set,
  Clock,
} from 'react-native-reanimated';
import Axios from 'axios';
import CryptoJS from 'crypto-js';
import {
  GX_API_ENDPOINT,
  GX_AUTH_URL,
  APP_CODE,
  MAILSURP_KEY,
} from '../../configs';
import {emailValidator, usernameRegex} from '../../utils';
import {WToast} from 'react-native-smart-tip';
import UsernameEmailForm from './UsernameEmailForm';
import PasswordForm from './PasswordForm';
import ConfirmPassword from './ConfirmPassword';
import OTPVerify from './OTPVerify';
import ProfilePicUploader from './ProfilePicUploader';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import LandingView from './LandingView';
import BrokerPromo from './BrokerPromo';
import ReanimatedTimingHelper from '../../utils/ReanimatedTimingHelper';
import NameForm from './NameForm';
import BlockcheckAddress from './BlockcheckAddress';
import {MailSlurp} from 'mailslurp-client';
import {useNavigation} from '@react-navigation/native';
import ThemeData from '../../configs/ThemeData';

const SignupSteps = ({isLoading, setIsLoading}) => {
  const navigation = useNavigation();

  const [activeRegStep, setActiveRegStep] = useState('');
  const [usersList, setUsersList] = useState([]);
  const [emailInput, setEmailInput] = useState('');
  const [usernameInput, setUsernameInput] = useState('');
  const [emailValidity, setEmailValidity] = useState(false);
  const [usernameValidity, setUsernameValidity] = useState(false);
  const [passwordInput, setPasswordInput] = useState('');
  const [passwordValidity, setPasswordValidity] = useState(false);
  const [confirmPasword, setConfirmPasword] = useState('');
  const [confirmPasswordValidity, setConfirmPasswordValidity] = useState(false);
  const [otpInput, setOtpInput] = useState('');
  const [isOTPVerified, setIsOTPVerified] = useState(false);
  const [affID, setAffID] = useState('1');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [blockcheckId, setBlockcheckId] = useState('');

  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);

  const chatKeyboardAnimation = useRef(new Animated.Value(0));

  useEffect(() => {
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
      onKeyBoardShow,
    );
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
      onKeyBoardHide,
    );
    BackHandler.addEventListener('hardwareBackPress', onBackPress);
    return () => {
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
        onKeyBoardShow,
      );
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
        onKeyBoardHide,
      );
      BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    };
  }, []);

  useCode(
    () =>
      isKeyboardOpen
        ? [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ]
        : [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ],
    [isKeyboardOpen],
  );

  useEffect(() => {
    getAllUserDetails();
  }, []);

  useEffect(() => {
    if (usersList) {
      const email = emailInput.toLowerCase().trim();
      const validEmail = emailValidator(email);

      if (!validEmail) {
        return setEmailValidity(false);
      }

      setEmailValidity(!usersList.emails.includes(email));
    }
  }, [usersList, emailInput]);

  useEffect(() => {
    if (usersList) {
      const username = usernameInput.toLowerCase().trim();

      if (username.length < 6 || !usernameRegex.test(username)) {
        return setUsernameValidity(false);
      }

      setUsernameValidity(!usersList.usernames.includes(username));
    }
  }, [usersList, usernameInput]);

  const onKeyBoardShow = (e) => {
    setKeyboardHeight(e.endCoordinates.height - 30);
    setIsKeyboardOpen(true);
  };

  const onKeyBoardHide = () => {
    setIsKeyboardOpen(false);
  };

  const getAllUserDetails = () => {
    Axios.get(`${GX_API_ENDPOINT}/listUsernames`).then((res) => {
      const {data} = res;
      if (data.status) {
        const bytes = CryptoJS.Rabbit.decrypt(data.payload, 'gmBuuQ6Er8XqYBd');
        const jsonString = bytes.toString(CryptoJS.enc.Utf8);
        const resultObj = JSON.parse(jsonString);
        setUsersList(resultObj);
      }
    });
  };

  const transitionViewRef = useRef();

  const executeTransform = () => {
    transitionViewRef.current.animateNextTransition();
  };

  const openPasswordForm = () => {
    if (!emailValidity) {
      const email = emailInput.toLowerCase().trim();

      if (!emailValidator(email)) {
        return WToast.show({
          data: 'Please Enter A Valid Email Id',
          position: WToast.position.TOP,
        });
      }
      if (usersList.emails.includes(email)) {
        return WToast.show({
          data: 'This Email Id Is Already Registered',
          position: WToast.position.TOP,
        });
      }
      return WToast.show({
        data: 'Please Enter A Valid Email Id',
        position: WToast.position.TOP,
      });
    }

    if (!usernameValidity) {
      const username = usernameInput.toLowerCase().trim();

      if (!usernameRegex.test(username)) {
        return WToast.show({
          data: 'Please Enter A Valid Username',
          position: WToast.position.TOP,
        });
      }
      if (usersList.usernames.includes(username)) {
        return WToast.show({
          data: 'This Username Is Already In Use',
          position: WToast.position.TOP,
        });
      }
      return WToast.show({
        data: 'Please Enter A Valid Username',
        position: WToast.position.TOP,
      });
    }

    executeTransform();
    setActiveRegStep('PASSWORD');
  };

  const openConfirmPassword = () => {
    if (!passwordValidity) {
      return WToast.show({
        data: 'Please Follow The Password Checklist',
        position: WToast.position.TOP,
      });
    }

    executeTransform();
    setActiveRegStep('CONFIRM');
  };

  const sendVerificationCode = () => {
    if (!confirmPasswordValidity) {
      return WToast.show({
        data: "Passwords Doesn't  Match",
        position: WToast.position.TOP,
      });
    }

    const postData = {
      username:
        usernameInput.trim().toLowerCase() || blockcheckId.trim().toLowerCase(),
      email: emailInput.toLowerCase().trim(),
      password: passwordInput.trim(),
      ref_affiliate: affID.trim() || '1',
      account_type: 'Personal',
      signedup_app: APP_CODE,
    };

    // console.log('postData', postData);

    // setActiveRegStep('VERIFY');

    setIsLoading(true);

    Axios.post(`${GX_AUTH_URL}/gx/user/signup`, postData)
      .then((resp) => {
        const {data} = resp;

        // console.log('Response', data);

        if (data.status) {
          setActiveRegStep('VERIFY');
        } else {
          WToast.show({data: data.message, position: WToast.position.TOP});
        }
      })
      .catch((error) =>
        console.log('Error on sending Verification Code', error),
      )
      .finally(() => setIsLoading(false));
  };

  const verifyOTP = () => {
    if (otpInput.length < 6) {
      return WToast.show({
        data: 'Please Enter The Six Digit Verification Code',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    const postData = {
      email: emailInput.toLowerCase().trim(),
      code: otpInput.toString().trim(),
    };
    Axios.post(`${GX_AUTH_URL}/gx/user/confirm`, postData)
      .then((resp) => {
        const {data} = resp;

        console.log('VerifyResp', data);

        if (data.status) {
          loginUser();
        } else {
          WToast.show({data: data.message, position: WToast.position.TOP});
        }
      })
      .catch((error) => console.log('Error on verifying OTP', error))
      .finally(() => setIsLoading(false));
  };

  const loginUser = () => {
    Axios.post(`${GX_AUTH_URL}/gx/user/login`, {
      email: emailInput.toLowerCase().trim(),
      password: passwordInput,
    })
      .then((resp) => {
        const {data} = resp;

        // console.log('Login Resp', data);

        if (data.status) {
          Axios.post(`${GX_API_ENDPOINT}/gxb/apps/register/user`, {
            email: emailInput.toLowerCase().trim(),
            app_code: APP_CODE,
          }).then((profileResp) => {
            console.log('profileResp', profileResp.data);

            if (profileResp.data.profile_id) {
              AsyncStorageHelper.setProfileId(profileResp.data.profile_id);
            }
          });
          AsyncStorageHelper.setIsLoggedIn(true);
          AsyncStorageHelper.setLoginEmail(emailInput.toLowerCase().trim());
          AsyncStorageHelper.setAppToken(data.idToken);
          AsyncStorageHelper.setRefreshToken(data.refreshToken);
          AsyncStorageHelper.setDeviceKey(data.device_key);
          setActiveRegStep('AVATAR');
        } else {
          WToast.show({
            data: data.message,
            position: WToast.position.TOP,
          });
        }
      })
      .catch((error) => {
        console.log('Login Error', error);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const resendVerificationCode = () => {
    const postData = {
      email: emailInput.toLowerCase().trim(),
    };

    Axios.post(`${GX_AUTH_URL}/gx/user/confirm/resend`, postData)
      .then((resp) => {
        const {data} = resp;
        // console.log('Resend Code', data);
        WToast.show({data: data.message, position: WToast.position.TOP});
      })
      .catch((error) =>
        console.log('Error on resending verification code', error),
      );
  };

  const openLastName = () => {
    if (!firstName.trim()) {
      return WToast.show({
        data: 'Please Enter Your First Name',
        position: WToast.position.TOP,
      });
    }

    setActiveRegStep('LAST_NAME');
  };

  const openBlockCheckAddress = () => {
    if (!lastName.trim()) {
      return WToast.show({
        data: 'Please Enter Your Last Name',
        position: WToast.position.TOP,
      });
    }

    setActiveRegStep('BLOCKCHECK_ADDRESS');
  };

  const createBlockcheckId = async () => {
    const id = blockcheckId.trim().toLowerCase();

    try {
      if (!id) {
        return WToast.show({
          data: 'Please Create A Blockcheck Id',
          position: WToast.position.TOP,
        });
      }
      setIsLoading(true);

      const mailslurp = new MailSlurp({
        apiKey: MAILSURP_KEY,
      });

      const inbox = await mailslurp.inboxController.createInbox(
        '',
        `${id}@blockcheck.io`,
        new Date(),
        false,
        `${firstName} ${lastName}`,
        [APP_CODE],
      );
      setActiveRegStep('PASSWORD');

      setIsLoading(false);

      setEmailInput(inbox.emailAddress);

      const email = await mailslurp.waitController.waitForLatestEmail(
        inbox.id,
        120000,
        true,
      );

      const pattern = 'use this code <strong>([0-9]{6})&nbsp;</strong>';
      const result = await mailslurp.emailController.getEmailContentMatch(
        {pattern},
        email.id,
      );

      console.log('result');

      setOtpInput(result?.matches[1] || '');
    } catch (error) {
      console.log('Error on blockcheck', error);
      setIsLoading(false);
      WToast.show({
        data: 'Error on creating Blockcheck Address',
        position: WToast.position.TOP,
      });
    }
  };

  const onNextClick = () => {
    if (activeRegStep === '' || activeRegStep === 'EMAIL') {
      return openPasswordForm();
    }

    if (activeRegStep === 'PASSWORD') {
      return openConfirmPassword();
    }

    if (activeRegStep === 'CONFIRM') {
      return sendVerificationCode();
    }

    if (activeRegStep === 'VERIFY') {
      return verifyOTP();
    }

    if (activeRegStep === 'FIRST_NAME') {
      return openLastName();
    }

    if (activeRegStep === 'LAST_NAME') {
      return openBlockCheckAddress();
    }

    if (activeRegStep === 'BLOCKCHECK_ADDRESS') {
      return createBlockcheckId();
    }
  };

  const transition = (
    <Transition.Together>
      <Transition.In
        type="slide-right"
        durationMs={200}
        interpolation="easeInOut"
      />
    </Transition.Together>
  );

  const renderActiveComponent = () => {
    switch (activeRegStep) {
      case 'LANDING':
        return (
          <LandingView
            gotoEmail={() => setActiveRegStep('FIRST_NAME')}
            gotoPromo={() => setActiveRegStep('PROMO')}
          />
        );

      case 'PROMO':
        return (
          <BrokerPromo
            setAffID={setAffID}
            gotoEmail={() => setActiveRegStep('FIRST_NAME')}
          />
        );

      case 'FIRST_NAME':
        return (
          <NameForm
            name={firstName}
            setName={setFirstName}
            onNextClick={onNextClick}
          />
        );

      case 'LAST_NAME':
        return (
          <NameForm
            name={lastName}
            setName={setLastName}
            title="Step 3: Enter Last Name"
            placeholder="Ex. Smith"
            onNextClick={onNextClick}
          />
        );

      case 'BLOCKCHECK_ADDRESS':
        return (
          <BlockcheckAddress
            blockcheckId={blockcheckId}
            setBlockcheckId={setBlockcheckId}
            onLegacySignup={() => setActiveRegStep('EMAIL')}
            onNextClick={onNextClick}
          />
        );

      case 'EMAIL':
        return (
          <UsernameEmailForm
            emailInput={emailInput}
            emailValidity={emailValidity}
            isLoading={isLoading}
            setEmailInput={setEmailInput}
            setUsernameInput={setUsernameInput}
            usernameInput={usernameInput}
            usernameValidity={usernameValidity}
            onNextClick={onNextClick}
          />
        );

      case 'PASSWORD':
        return (
          <PasswordForm
            isLoading={isLoading}
            passwordInput={passwordInput}
            setPasswordInput={setPasswordInput}
            passwordValidity={passwordValidity}
            setPasswordValidity={setPasswordValidity}
            onNextClick={onNextClick}
          />
        );

      case 'CONFIRM':
        return (
          <ConfirmPassword
            confirmPassword={confirmPasword}
            confirmPasswordValidity={confirmPasswordValidity}
            isLoading={isLoading}
            setConfirmPassword={setConfirmPasword}
            setPassword={setPasswordInput}
            goBack={() => setActiveRegStep('PASSWORD')}
            password={passwordInput}
            setConfirmPasswordValidity={setConfirmPasswordValidity}
            onNextClick={onNextClick}
          />
        );
      case 'VERIFY':
        return (
          <OTPVerify
            setOtpInput={setOtpInput}
            otpInput={otpInput}
            resendVerificationCode={resendVerificationCode}
            isBlockcheck={usernameInput.trim() === ''}
            onNextClick={onNextClick}
          />
        );
      case 'AVATAR':
        return (
          <ProfilePicUploader
            setIsLoading={setIsLoading}
            username={usernameInput}
          />
        );
      default:
        return (
          <LandingView
            gotoEmail={() => setActiveRegStep('FIRST_NAME')}
            gotoPromo={() => setActiveRegStep('PROMO')}
          />
        );
    }
  };

  const onBackPress = () => {
    switch (activeRegStep) {
      case 'LANDING':
        navigation.goBack();
        break;

      case 'PROMO':
        setActiveRegStep('LANDING');
        break;

      case 'FIRST_NAME':
        setActiveRegStep('PROMO');
        break;

      case 'LAST_NAME':
        setActiveRegStep('FIRST_NAME');
        break;

      case 'BLOCKCHECK_ADDRESS':
        setActiveRegStep('LAST_NAME');
        break;

      case 'EMAIL':
        setActiveRegStep('LAST_NAME');
        break;

      case 'PASSWORD':
        setActiveRegStep(blockcheckId ? 'BLOCKCHECK_ADDRESS' : 'EMAIL');
        break;

      case 'CONFIRM':
        setActiveRegStep('PASSWORD');
        break;

      case 'VERIFY':
        setActiveRegStep('VERIFY');
        break;

      case 'AVATAR':
        break;
      default:
        navigation.goBack();
        break;
    }
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.backButton} onPress={onBackPress}>
        <Image
          style={styles.backIcon}
          source={require('../../assets/back-arrow.png')}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <Transitioning.View
        ref={transitionViewRef}
        transition={transition}
        style={styles.loginFrom}>
        {renderActiveComponent()}
        {/* {activeRegStep !== '' &&
          activeRegStep !== 'LANDING' &&
          activeRegStep !== 'PROMO' &&
          activeRegStep !== 'AVATAR' && (
            <TouchableOpacity style={styles.loginButton} onPress={onNextClick}>
              <Text style={styles.loginButtonText}>Next</Text>
            </TouchableOpacity>
          )} */}
      </Transitioning.View>
      <Animated.View
        style={[
          {
            height: interpolate(chatKeyboardAnimation.current, {
              inputRange: [0, 1],
              outputRange: [0, keyboardHeight],
            }),
          },
        ]}
      />
    </View>
  );
};

export default SignupSteps;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loginFrom: {
    flex: 1,
    paddingHorizontal: 30,
    justifyContent: 'center',
  },
  loginButton: {
    marginTop: 15,
    marginBottom: 30,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    width: 150,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  loginButtonText: {
    color: 'white',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  backButton: {
    marginLeft: 20,
    width: 40,
    height: 40,
  },
  backIcon: {
    flex: 1,
    width: null,
    height: null,
  },
});
