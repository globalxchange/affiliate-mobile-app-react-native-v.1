import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import LoginInputField from '../LoginInputField';

const UsernameEmailForm = ({
  emailInput,
  isLoading,
  setEmailInput,
  emailValidity,
  usernameInput,
  setUsernameInput,
  usernameValidity,
  onNextClick,
}) => {
  const [passwordFocus, setPasswordFocus] = useState(false);
  const [emailFocus, setEmailFocus] = useState(false);

  const onNext = () => {
    if (!emailInput) {
      setPasswordFocus(false);
      setEmailFocus(true);
      return;
    }
    onNextClick && onNextClick();
  };

  return (
    <View style={styles.loginFromContainer}>
      <Text style={styles.loginTitle}>Register</Text>
      <Text style={styles.loginSubTitle}>Step 1: Email & Username</Text>
      <LoginInputField
        focus={emailFocus}
        icon={require('../../assets/email-icon.png')}
        placeholder="EMAIL"
        type="emailAddress"
        value={emailInput}
        editable={!isLoading}
        onChangeText={(text) => setEmailInput(text)}
        validatorStatus={emailValidity}
        showNext
        onNext={() => setPasswordFocus(true)}
      />
      <LoginInputField
        focus={passwordFocus}
        icon={require('../../assets/user-icon.png')}
        placeholder="USERNAME"
        value={usernameInput}
        editable={!isLoading}
        onChangeText={(text) => setUsernameInput(text)}
        validatorStatus={usernameValidity}
        showNext
        onNext={onNext}
      />
    </View>
  );
};

export default UsernameEmailForm;

const styles = StyleSheet.create({
  loginFromContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  loginTitle: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-Bold',
    fontSize: 50,
  },
  loginSubTitle: {
    color: '#999C9A',
    marginBottom: 40,
    fontFamily: 'Montserrat',
  },
});
