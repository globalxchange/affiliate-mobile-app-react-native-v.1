import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';

const BrokerSyncOption = ({goBack, gotoCheckMail}) => {
  return (
    <View style={styles.container}>
      <View style={styles.viewContainer}>
        <Text style={styles.loginTitle}>AffiliateSync</Text>
        <Text style={styles.loginSubTitle}>What You Can Do About It?</Text>
        <TouchableOpacity
          onPress={gotoCheckMail}
          style={styles.optionContainer}>
          <Text style={styles.optionText}>User Email To Identify Broker</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={goBack} style={styles.optionContainer}>
          <Text style={styles.optionText}>Try Another AffiliateSync Code</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default BrokerSyncOption;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  viewContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  loginTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 50,
    marginTop: 40,
    marginBottom: 10,
  },
  loginSubTitle: {
    color: '#08152D',
    marginBottom: 40,
    fontFamily: 'Montserrat-SemiBold',
    marginTop: 40,
  },
  optionContainer: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 20,
    marginBottom: 15,
  },
  optionText: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: '#08152D',
  },
});
