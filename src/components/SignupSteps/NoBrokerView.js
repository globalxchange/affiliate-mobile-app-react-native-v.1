import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';

const NoBrokerView = ({promoText, onNext}) => {
  return (
    <View style={styles.container}>
      <View style={styles.viewContainer}>
        <Text style={styles.loginTitle}>AffiliateSync</Text>
        <Text style={styles.loginSubTitle}>
          The AffiliateSync Code @{promoText || ''} Is Not Matching Anyone In
          Our Records
        </Text>
        <Text style={styles.subHeader}>Why This Could Be Happening?</Text>
        <Text style={[styles.loginSubTitle, {marginBottom: 10}]}>
          - You are entering incorrect capitalizations within the code
        </Text>
        <Text style={[styles.loginSubTitle, {marginBottom: 10}]}>
          - Your Broker Has Changed Their AffiliateSync Code After They Gave It
          To You
        </Text>
      </View>
      <View style={styles.actionContainer}>
        <Text style={styles.actionText}>What You Can Do About It?</Text>
        <Text onPress={onNext} style={styles.actionButton}>
          Click Here
        </Text>
      </View>
    </View>
  );
};

export default NoBrokerView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  viewContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  loginTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 50,
    marginTop: 40,
    marginBottom: 10,
  },
  loginSubTitle: {
    color: '#999C9A',
    marginBottom: 40,
    fontFamily: 'Montserrat',
  },
  subHeader: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    marginBottom: 20,
  },
  actionContainer: {
    flexDirection: 'row',
    marginBottom: 40,
  },
  actionText: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
  },
  actionButton: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    textDecorationLine: 'underline',
    marginLeft: 5,
  },
});
