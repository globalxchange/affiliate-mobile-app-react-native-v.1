import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableWithoutFeedback,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {SharedElement} from 'react-navigation-shared-element';

const {height} = Dimensions.get('window');

const StepItem = ({data, isCurrent}) => {
  const {navigate} = useNavigation();

  const onItemClick = () => {
    navigate('Details', {item: data});
  };

  return (
    <TouchableWithoutFeedback onPress={() => onItemClick()}>
      <View style={styles.container}>
        <View style={styles.thumbContainer}>
          <SharedElement id={`item.${data.key}.photo`}>
            <Image
              source={
                data.thumbnail
                  ? {uri: data.thumbnail}
                  : require('../../assets/step-placeholder.png')
              }
              resizeMode="cover"
              resizeMethod="scale"
              style={styles.thumbImage}
            />
          </SharedElement>
          <Text style={styles.statusName}>{data.status}</Text>
        </View>
        <View style={styles.stepDetailsContainer}>
          <View style={styles.stepImageContainer}>
            <SharedElement id={`item.${data.key}.icon`}>
              <Image
                source={
                  data.icon
                    ? {uri: data.icon}
                    : require('../../assets/step-icon-placeholder.png')
                }
                style={styles.stepImage}
                resizeMode="cover"
              />
            </SharedElement>
          </View>
          <View style={styles.stepDetails}>
            <Text style={styles.stepName}>{data.name}</Text>
            {isCurrent && <Text style={styles.currentStep}>Current Step</Text>}
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default StepItem;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 20,
    marginBottom: 30,
    borderWidth: 1,
    borderColor: '#EBEBEB',
    borderRadius: 6,
  },
  thumbContainer: {
    height: height * 0.5,
    borderBottomColor: '#EBEBEB',
    borderBottomWidth: 1,
    borderRadius: 6,
    width: '100%',
  },
  statusName: {
    fontFamily: 'Montserrat-Bold',
    color: 'white',
    fontSize: 28,
    position: 'absolute',
    bottom: 20,
    left: 20,
    right: 20,
  },
  thumbImage: {
    height: height * 0.5,
    width: '100%',
  },
  stepDetailsContainer: {
    flexDirection: 'row',
    padding: 15,
    alignItems: 'center',

    borderRadius: 6,
  },
  stepImageContainer: {
    width: 90,
    height: 90,
    borderWidth: 1,
    borderColor: '#EBEBEB',
  },
  stepImage: {
    width: 90,
    height: 90,
  },
  stepDetails: {
    marginLeft: 15,
    justifyContent: 'center',
    flex: 1,
  },
  stepName: {
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
    fontSize: 16,
  },
  currentStep: {
    marginTop: 8,
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    fontSize: 12,
  },
});
