/* eslint-disable react-native/no-inline-styles */
import React, {useContext} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {DepositContext} from '../../contexts/DepositContext';
import {getFundingMethodIcon} from '../../utils';

const MethodSelector = () => {
  const {setStep, activeWallet} = useContext(DepositContext);

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Select Deposit Method</Text>
      <View style={styles.methodList}>
        {activeWallet.asset_type === 'Fiat' ? null : (
          <TouchableOpacity
            onPress={() => setStep('WalletTransfer')}
            style={styles.menuItem}>
            <Image
              style={styles.menuItemImage}
              source={getFundingMethodIcon(activeWallet.coinSymbol)}
              resizeMode="contain"
            />
          </TouchableOpacity>
        )}
        <TouchableOpacity
          onPress={() => setStep('VaultDeposit')}
          style={styles.menuItem}>
          <Image
            style={styles.menuItemImage}
            source={require('../../assets/vault-logo.png')}
            resizeMode="contain"
          />
        </TouchableOpacity>
        <TouchableOpacity
          disabled
          style={[styles.menuItem, {borderBottomWidth: 0}]}>
          <Image
            style={styles.menuItemImage}
            source={require('../../assets/snappy-logo.png')}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default MethodSelector;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  methodList: {flex: 1},
  menuItem: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    borderBottomColor: '#EBEBEB',
    borderBottomWidth: 1,
  },
  menuItemImage: {
    width: 100,
  },
  smallMenuIcon: {
    width: 20,
    height: 20,
  },
  methodName: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#001D41',
    marginLeft: 20,
  },
});
