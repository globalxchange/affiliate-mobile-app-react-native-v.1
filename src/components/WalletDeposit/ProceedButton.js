/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {StyleSheet, Text, TouchableOpacity, Image} from 'react-native';

const ProceedButton = ({title, onPress, onBack, disabled}) => {
  return (
    <TouchableOpacity
      disabled={disabled}
      style={[styles.container, disabled && styles.disabled]}
      onPress={onPress}>
      {onBack && (
        <TouchableOpacity style={styles.backButton} onPress={onBack}>
          <Image
            style={styles.backIcon}
            source={require('../../assets/back-icon-colored.png')}
            resizeMode="contain"
          />
        </TouchableOpacity>
      )}
      <Text
        style={[
          styles.buttonText,
          {
            fontSize: title.length > 15 ? 12 : 18,
          },
        ]}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

export default ProceedButton;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#08152D',
    height: 55,
    flexDirection: 'row',
    alignItems: 'center',
  },
  disabled: {
    backgroundColor: 'rgba(8, 21, 45, 0.7);',
  },
  buttonText: {
    position: 'absolute',
    left: 0,
    right: 0,
    zIndex: -1,
    color: 'white',
    fontFamily: 'Montserrat-Bold',
    textAlign: 'center',
    flex: 1,
  },
  backButton: {
    backgroundColor: 'white',
    paddingVertical: 18,
    paddingHorizontal: 20,
  },
  backIcon: {
    height: 12,
    width: 12,
  },
});
