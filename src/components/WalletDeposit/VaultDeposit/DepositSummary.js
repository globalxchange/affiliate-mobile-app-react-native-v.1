import React, {useContext, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {DepositContext} from '../../../contexts/DepositContext';
import {useNavigation} from '@react-navigation/native';
import {AppContext} from '../../../contexts/AppContextProvider';
import * as WebBrowser from 'expo-web-browser';
import {formatterHelper} from '../../../utils';

const DepositSummary = () => {
  const navigation = useNavigation();

  const {activeWallet, cryptoInput} = useContext(DepositContext);

  const {walletBalances} = useContext(AppContext);

  const walletRef = useRef();

  useEffect(() => {
    walletRef.current = activeWallet;
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Deposit Complete</Text>
      <View style={styles.mainContainer}>
        <Text style={styles.message}>
          Congratulations Your Have Deposited{' '}
          {formatterHelper(cryptoInput, activeWallet.coinSymbol)}{' '}
          {activeWallet.coinSymbol} Into Your AffiliateApp Vault
        </Text>
        <View style={styles.confirmation}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image
              style={styles.icon}
              source={{uri: walletRef.current?.coinImage}}
              resizeMode="contain"
            />
            <Text style={styles.walletName}>
              {walletRef.current && walletRef.current.coinSymbol} Wallet Balance
            </Text>
          </View>
          <Text style={styles.walletName}>
            {walletRef.current &&
              formatterHelper(
                walletBalances[
                  `${walletRef.current.coinSymbol.toLowerCase()}_balance`
                ],
                activeWallet.coinSymbol,
              )}
          </Text>
        </View>
        <View style={styles.actionContainer}>
          <ScrollView
            style={styles.scrollView}
            horizontal
            showsHorizontalScrollIndicator={false}>
            <TouchableOpacity
              style={[styles.action, {marginLeft: 10}]}
              onPress={() => navigation.navigate('Earn')}>
              <Image
                style={styles.actionIcon}
                source={require('../../../assets/app-logo.png')}
                resizeMode="contain"
              />
              <Text style={styles.actionText}>Back To Feed</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.action, {marginLeft: 25}]}
              onPress={() =>
                WebBrowser.openBrowserAsync('https://iceprotocol.com/')
              }>
              <Image
                style={styles.actionIcon}
                source={require('../../../assets/iced-icon.png')}
                resizeMode="contain"
              />
              <Text style={[styles.actionText, styles.actionTextBlack]}>
                Invest Bitcoin
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.action, {marginLeft: 25, marginRight: 10}]}
              onPress={() =>
                WebBrowser.openBrowserAsync('https://cryptolottery.com/')
              }>
              <Image
                style={styles.actionIcon}
                source={require('../../../assets/lottery-icon.png')}
                resizeMode="contain"
              />
              <Text style={[styles.actionText, styles.actionTextBlack]}>
                Play Lottery
              </Text>
            </TouchableOpacity>
          </ScrollView>
        </View>
      </View>
    </View>
  );
};

export default DepositSummary;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  mainContainer: {
    flex: 1,
    paddingHorizontal: 40,
    paddingVertical: 40,
    justifyContent: 'space-between',
  },
  message: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat',
  },
  confirmation: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 8,
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  icon: {
    width: 24,
    height: 24,
    marginRight: 15,
  },
  walletName: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
  },
  actionContainer: {
    flexDirection: 'row',
  },
  scrollView: {
    marginHorizontal: -10,
  },
  action: {
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    backgroundColor: 'white',
    borderRadius: 8,
    paddingVertical: 20,
    paddingHorizontal: 30,
    flex: 1,
    marginVertical: 10,
  },
  actionIcon: {
    width: 35,
    height: 35,
  },
  actionText: {
    marginTop: 10,
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    textAlign: 'center',
    fontSize: 12,
  },
  actionTextBlack: {
    color: '#464B4E',
  },
});
