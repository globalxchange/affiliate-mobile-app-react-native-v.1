import React, {useContext, useEffect} from 'react';
import {StyleSheet, View, BackHandler} from 'react-native';
import {DepositContext} from '../../../contexts/DepositContext';
import VaultSelector from './VaultSelector';
import DepositForm from './DepositForm';
import FundingConfirmation from './FundingConfirmation';
import DepositLoading from './DepositLoading';
import DepositSummary from './DepositSummary';
import {useFocusEffect} from '@react-navigation/native';
import {AppContext} from '../../../contexts/AppContextProvider';

const VaultDeposit = ({isKeyBoardOpen}) => {
  const {vaultStep, setVaultStep, activeWallet, clearDepositState} = useContext(
    DepositContext,
  );

  const {walletBalances} = useContext(AppContext);

  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => {
        if (vaultStep === 4) {
          return false;
        }

        if (vaultStep > 0) {
          setVaultStep(vaultStep - 1);
          return true;
        }
        return false;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () => {
        BackHandler.removeEventListener('hardwareBackPress', onBackPress);
      };
    }, [vaultStep]),
  );

  useEffect(() => {
    return () => clearDepositState();
  }, []);

  const renderActiveComponent = () => {
    switch (vaultStep) {
      case 0:
        return <VaultSelector />;
      case 1:
        return (
          <DepositForm
            activeWallet={activeWallet}
            isKeyBoardOpen={isKeyBoardOpen}
            walletBalances={walletBalances}
          />
        );
      case 2:
        return <FundingConfirmation />;
      case 3:
        return <DepositLoading />;
      case 4:
        return <DepositSummary />;
      default:
        return <VaultSelector />;
    }
  };

  return <View style={styles.container}>{renderActiveComponent()}</View>;
};

export default VaultDeposit;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
