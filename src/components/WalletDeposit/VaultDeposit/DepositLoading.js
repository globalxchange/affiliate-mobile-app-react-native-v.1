import React, {useEffect, useContext} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import LoadingAnimation from '../../LoadingAnimation';
import {DepositContext} from '../../../contexts/DepositContext';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import {APP_CODE, GX_API_ENDPOINT} from '../../../configs';
import 'react-native-get-random-values';
import {v4 as uuidv4} from 'uuid';
import Axios from 'axios';
import CryHelper from '../../../utils/CryHelper';
import {WToast} from 'react-native-smart-tip';
import {AppContext} from '../../../contexts/AppContextProvider';
const DepositLoading = () => {
  const {
    setVaultStep,
    cryptoInput,
    activeWallet,
    selectedGxVault,
    clearDepositState,
  } = useContext(DepositContext);
  const {updateWalletBalances} = useContext(AppContext);

  useEffect(() => {
    submitDepositRequest();
  }, []);

  const submitDepositRequest = async () => {
    const profileId = await AsyncStorageHelper.getProfileId();
    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    const postData = {
      email,
      token,
      amount: parseFloat(cryptoInput),
      from_coin: selectedGxVault.coinSymbol,
      to_coin: activeWallet.coinSymbol,
      identifier: uuidv4(),
      app_code: APP_CODE,
      profile_id: profileId,
      vault_id: '',
    };

    // console.log('PostData', postData);

    Axios.post(`${GX_API_ENDPOINT}/coin/vault/service/fund/gx`, postData)
      .then((resp) => {
        const {data} = resp;

        // console.log('submitDepositRequest', data);

        if (data.status) {
          updateWalletBalances();
          setVaultStep(4);
        } else {
          WToast.show({data: `${data.message}`});
          clearDepositState();
        }
      })
      .catch((error) => console.log('error on  submitDepositRequest', error));
  };

  return (
    <View style={styles.container}>
      <LoadingAnimation />
      <Text style={styles.message}>
        Depositing To Your AffiliateApp Wallet{' '}
      </Text>
    </View>
  );
};

export default DepositLoading;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -20,
  },
  message: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 16,
    paddingHorizontal: 40,
  },
});
