import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {formatterHelper, roundHelper} from '../../../utils';
import {DepositContext} from '../../../contexts/DepositContext';
import ProceedButton from '../ProceedButton';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';
import {WToast} from 'react-native-smart-tip';
import CustomNumField from '../../CustomNumField';

class DepositForm extends Component {
  static contextType = DepositContext;

  constructor(props) {
    super(props);

    this.state = {conversionRate: 0};
  }

  componentDidMount() {
    this.getConversionRate();
  }

  componentDidUpdate(prevProps, prevState, snapShot) {
    // Getting conversion  rate when wallet changing
    if (this.props.activeWallet !== prevProps.activeWallet) {
      this.getConversionRate();
      this.clearForms();
    }

    if (this.state.conversionRate !== prevState.conversionRate) {
      this.onFiatInputChange(this.state.fiatInput);
    }
  }

  clearForms = () => {};

  getConversionRate = () => {
    const {activeWallet} = this.props;
    const {selectedGxVault} = this.context;

    if (!activeWallet) {
      return;
    }

    Axios.get(`${GX_API_ENDPOINT}/forex/convert`, {
      params: {buy: selectedGxVault.coinSymbol, from: activeWallet.coinSymbol},
    })
      .then((resp) => {
        const {data} = resp;

        // console.log('Convert Rate', data);

        if (data.status) {
          const conversionRate =
            data[
              `${activeWallet.coinSymbol.toLowerCase()}_${selectedGxVault.coinSymbol.toLowerCase()}`
            ] || 0;

          this.setState({conversionRate});
        } else {
          this.setState({conversionRate: 0});
        }
      })
      .catch((error) => console.log('Conversion Rate error', error));
  };

  onCryptoInputChange = (input) => {
    const {
      cryptoInput,
      setCryptoInput,
      setFiatInput,
      selectedGxVault,
    } = this.context;
    const {conversionRate} = this.state;

    if (input) {
      const parseInputValue = parseFloat(input || 0);

      const updatedFiatValue = parseInputValue * conversionRate;

      setCryptoInput(input);
      setFiatInput(
        roundHelper(
          isNaN(parseFloat(updatedFiatValue)) ? '' : updatedFiatValue,
          selectedGxVault.coinSymbol,
        ),
      );
    } else {
      setFiatInput('');
      setCryptoInput('');
    }
  };

  onFiatInputChange = (input) => {
    const {
      fiatInput,
      setCryptoInput,
      setFiatInput,
      activeWallet,
    } = this.context;
    const {conversionRate} = this.state;

    if (input) {
      setFiatInput(input);

      if (conversionRate) {
        const parseInputValue = parseFloat(input || 0);
        const updatedCryptoValue = parseInputValue / conversionRate;

        setCryptoInput(
          roundHelper(
            isNaN(parseFloat(updatedCryptoValue)) ? '' : updatedCryptoValue,
            activeWallet.coinSymbol,
          ),
        );
      }
    } else {
      setFiatInput('');
      setCryptoInput('');
    }
  };

  onProceed = () => {
    const {
      setVaultStep,
      cryptoInput,
      fiatInput,
      selectedGxVault,
    } = this.context;

    if (!cryptoInput) {
      return WToast.show({data: 'Please input the amount'});
    }

    if (isNaN(parseFloat(cryptoInput))) {
      return WToast.show({
        data: 'Please input a valid Value',
      });
    }

    if (fiatInput <= 0 || fiatInput > selectedGxVault.coinValue) {
      return WToast.show({
        data: `Not Enough Balance in ${selectedGxVault.coinSymbol} GX Vault`,
      });
    }

    setVaultStep(2);
  };

  onHalfClicked = () => {
    const {activeWallet, walletBalances} = this.props;

    if (walletBalances) {
      const balance =
        walletBalances[`${activeWallet.coinSymbol.toLowerCase()}_balance`] || 0;

      const halfBalance = balance / 2;
      this.onCryptoInputChange(halfBalance);
    }
  };

  onFullClicked = () => {
    const {activeWallet, walletBalances} = this.props;

    if (walletBalances) {
      const balance =
        walletBalances[`${activeWallet.coinSymbol.toLowerCase()}_balance`] || 0;

      this.onCryptoInputChange(balance);
    }
  };

  render() {
    const {
      activeWallet,
      isCryptoFocused,
      setIsCryptoFocused,
      cryptoInput,
      fiatInput,
      selectedGxVault,
      setVaultStep,
      setCryptoInput,
      setFiatInput,
    } = this.context;

    const {isKeyBoardOpen} = this.props;

    return (
      <View style={styles.container}>
        <Text style={styles.header}>Enter Deposit Amount</Text>
        <View style={styles.formContainer}>
          <View style={styles.inputForm}>
            <View style={styles.inputContainer}>
              <TextInput
                style={[styles.input, isCryptoFocused && styles.focusedText]}
                placeholder={formatterHelper('0.00', activeWallet.coinSymbol)}
                onChangeText={(text) => this.onCryptoInputChange(text)}
                keyboardType="numeric"
                returnKeyType="done"
                value={cryptoInput}
                onFocus={() => setIsCryptoFocused(true)}
                placeholderTextColor="#878788"
              />
              <TouchableOpacity
                onPress={this.onHalfClicked}
                style={styles.inputButton}>
                <Text style={styles.inputButtonText}>HALF</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={this.onFullClicked}
                style={styles.inputButton}>
                <Text style={styles.inputButtonText}>ALL</Text>
              </TouchableOpacity>
              <Text
                style={[
                  styles.cryptoName,
                  isCryptoFocused && styles.focusedText,
                ]}>
                {activeWallet.coinSymbol}
              </Text>
            </View>
            <View style={styles.divider} />
            <View style={styles.inputContainer}>
              <TextInput
                style={[styles.input, isCryptoFocused || styles.focusedText]}
                placeholder={formatterHelper(
                  '0.00',
                  selectedGxVault.coinSymbol,
                )}
                onChangeText={(text) => this.onFiatInputChange(text)}
                keyboardType="numeric"
                returnKeyType="done"
                value={fiatInput}
                onFocus={() => setIsCryptoFocused(false)}
                placeholderTextColor="#878788"
              />
              <Text
                style={[
                  styles.cryptoName,
                  isCryptoFocused || styles.focusedText,
                ]}>
                {selectedGxVault.coinSymbol}
              </Text>
            </View>
          </View>
        </View>
        {!isKeyBoardOpen && (
          <ProceedButton
            onPress={this.onProceed}
            title="Proceed"
            onBack={() => {
              setVaultStep(0);
              setCryptoInput('');
              setFiatInput('');
            }}
          />
        )}
      </View>
    );
  }
}

export default DepositForm;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    marginBottom: 15,
  },
  formContainer: {
    flex: 1,
    paddingHorizontal: 40,
  },
  inputForm: {marginTop: 20},
  inputContainer: {flexDirection: 'row', alignItems: 'center'},
  input: {
    flexGrow: 1,
    width: 0,
    color: '#041939',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    paddingHorizontal: 10,
  },
  focusedInput: {
    color: '#08152D',
  },
  inputButton: {
    borderWidth: 1,
    borderColor: '#EDEDED',
    justifyContent: 'center',
    paddingHorizontal: 8,
    paddingVertical: 2,
    marginLeft: 5,
  },
  inputButtonText: {
    color: '#B4BBC4',
    fontFamily: 'Montserrat',
    fontSize: 10,
  },
  cryptoName: {
    marginLeft: 10,
    color: '#041939',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  focusedText: {
    color: '#08152D',
  },
  divider: {
    backgroundColor: '#08152D',
    height: 1,
    marginVertical: 5,
  },
});
