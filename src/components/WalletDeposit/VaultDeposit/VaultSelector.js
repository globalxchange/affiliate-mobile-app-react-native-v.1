/* eslint-disable react-native/no-inline-styles */
import React, {useState, useContext, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import ProceedButton from '../ProceedButton';
import {GX_API_ENDPOINT} from '../../../configs';
import {DepositContext} from '../../../contexts/DepositContext';
import Animated from 'react-native-reanimated';
import Axios from 'axios';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import LoadingAnimation from '../../LoadingAnimation';
import {formatterHelper} from '../../../utils';

const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView);

const VaultSelector = () => {
  const {
    setVaultStep,
    setActiveGxVault,
    setStep,
    isGXVaultSelectorExpanded,
    setIsGXVaultSelectorExpanded,
    selectedGxVault,
    gxVaults,
    setGxVaults,
  } = useContext(DepositContext);

  const [activeType, setActiveType] = useState('Crypto');

  const onVaultSelectHandler = (selectedVault) => {
    setActiveGxVault(selectedVault);
    // setVaultStep(1);
    setIsGXVaultSelectorExpanded(false);
  };

  const getGXVaultDetails = async () => {
    const email = await AsyncStorageHelper.getLoginEmail();

    Axios.get(`${GX_API_ENDPOINT}/coin/vault/coins_data`, {
      params: {email},
    })
      .then((resp) => {
        const {data} = resp;
        if (data.status) {
          setGxVaults(data.coins);
        }
      })
      .catch((error) => console.log('Error getting Vault Balances', error));
  };

  useEffect(() => {
    getGXVaultDetails();
  }, []);

  const activeDataSet = gxVaults
    ? activeType === 'Crypto'
      ? gxVaults.filter((item) => item.asset_type === 'Crypto')
      : gxVaults.filter((item) => item.asset_type === 'Fiat')
    : [];

  return (
    <View style={styles.container}>
      {gxVaults ? (
        <>
          <Text style={styles.header}>
            Select The GX Vault You Are Withdrawing From
          </Text>
          <TouchableOpacity
            style={styles.expandIconContainer}
            onPress={() =>
              setIsGXVaultSelectorExpanded(!isGXVaultSelectorExpanded)
            }>
            <Image
              style={styles.expandIcon}
              source={
                isGXVaultSelectorExpanded
                  ? require('../../../assets/collapse-icon.png')
                  : require('../../../assets/expand-icon.png')
              }
              resizeMode="contain"
            />
          </TouchableOpacity>
          <View style={styles.vaultContainer}>
            <View style={styles.switchContainer}>
              <TouchableOpacity
                style={[
                  styles.switchItem,
                  {borderRightColor: '#EBEBEB', borderRightWidth: 1},
                ]}
                onPress={() => setActiveType('Crypto')}>
                <Text
                  style={[
                    styles.switchText,
                    activeType === 'Crypto' && {
                      fontFamily: 'Montserrat-Bold',
                    },
                  ]}>
                  Crypto
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.switchItem}
                onPress={() => setActiveType('Fiat')}>
                <Text
                  style={[
                    styles.switchText,
                    activeType === 'Fiat' && {
                      fontFamily: 'Montserrat-Bold',
                    },
                  ]}>
                  Fiat
                </Text>
              </TouchableOpacity>
            </View>
            <AnimatedScrollView
              scrollEventThrottle={16}
              showsVerticalScrollIndicator={false}>
              {activeDataSet.map((item) => (
                <TouchableOpacity
                  key={item.coinName}
                  style={styles.vaultList}
                  onPress={() => onVaultSelectHandler(item)}>
                  <View style={styles.vaultItem}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      <Image
                        style={styles.coinIcon}
                        source={{uri: item.coinImage}}
                        resizeMode="cover"
                      />
                      <Text
                        style={[
                          styles.vaultName,
                          selectedGxVault &&
                            selectedGxVault.coinName === item.coinName && {
                              fontFamily: 'Montserrat-Bold',
                            },
                        ]}>
                        {item.coinName}
                      </Text>
                    </View>
                    <Text
                      style={[
                        styles.vaultBalance,
                        selectedGxVault &&
                          selectedGxVault.coinName === item.coinName && {
                            fontFamily: 'Montserrat-SemiBold',
                          },
                      ]}>
                      {formatterHelper(item.coinValue, item.coinSymbol)}
                    </Text>
                  </View>
                </TouchableOpacity>
              ))}
            </AnimatedScrollView>
          </View>
          <View
            style={{
              marginBottom: isGXVaultSelectorExpanded ? 40 : 0,
            }}>
            <ProceedButton
              disabled={selectedGxVault === null ? true : false}
              style-={styles.actionButton}
              title={
                selectedGxVault
                  ? `Withdraw From ${selectedGxVault.coinSymbol} GX Vault`
                  : 'Proceed'
              }
              onPress={() => {
                setIsGXVaultSelectorExpanded(false);
                setVaultStep(1);
              }}
              onBack={() => setStep(0)}
            />
          </View>
        </>
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
          <Text style={styles.loadingText}>Fetching GX Vault Data...</Text>
        </View>
      )}
    </View>
  );
};

export default VaultSelector;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
    paddingHorizontal: 40,
  },
  vaultContainer: {
    flex: 1,
  },
  switchContainer: {
    flexDirection: 'row',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 10,
    marginHorizontal: 50,
    marginTop: 25,
  },
  switchItem: {
    flex: 1,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
  },
  switchText: {
    color: '#08152D',
    fontFamily: 'Montserrat',
  },
  vaultList: {
    marginTop: 20,
  },
  vaultItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 50,
    paddingVertical: 30,
    borderBottomColor: '#EBEBEB',
    borderBottomWidth: 1,
  },
  coinIcon: {
    width: 25,
    height: 25,
    marginRight: 15,
    borderRadius: 15,
  },
  vaultName: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 16,
  },
  vaultBalance: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 16,
  },
  expandIconContainer: {
    position: 'absolute',
    top: 0,
    right: 10,
    width: 18,
    height: 18,
  },
  expandIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  loadingText: {marginTop: 10, fontFamily: 'Montserrat'},
});
