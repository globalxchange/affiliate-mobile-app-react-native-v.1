/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useContext, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  TouchableOpacity,
  Keyboard,
  Platform,
} from 'react-native';
import Axios from 'axios';
import {GX_API_ENDPOINT, APP_CODE} from '../../../../configs';
import AsyncStorageHelper from '../../../../utils/AsyncStorageHelper';
import {WToast} from 'react-native-smart-tip';
import {v4 as uuidv4} from 'uuid';
import CryHelper from '../../../../utils/CryHelper';
import {formatterHelper} from '../../../../utils';
import {DepositContext} from '../../../../contexts/DepositContext';
import Animated, {
  useCode,
  set,
  Clock,
  interpolate,
} from 'react-native-reanimated';
import ReanimatedTimingHelper from '../../../../utils/ReanimatedTimingHelper';
import QuoteInput from '../../../QuoteInput';

let axiosToken = null;

const PaymentQuote = ({pathData, onClose, selectedCrypto}) => {
  const {instaDepositData} = useContext(DepositContext);

  const {
    paymentType,
    paymentCurrency,
    selectedCountry,
    selectedBanker,
  } = instaDepositData;

  const [pathId, setPathId] = useState();
  const [spendValue, setSpendValue] = useState('0');
  const [gettingValue, setGettingValue] = useState('0');
  const [errorText, setErrorText] = useState('');
  const [purchaseLoading, setPurchaseLoading] = useState(false);
  const [isSpendLoading, setIsSpendLoading] = useState(false);
  const [isGettingLoading, setIsGettingLoading] = useState(false);
  const [spendEdited, setSpendEdited] = useState();
  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);
  const [spendFocused, setSpendFocused] = useState(false);

  const spendLoaded = useRef(false);
  const getLoaded = useRef(false);
  const chatKeyboardAnimation = useRef(new Animated.Value(1));

  useEffect(() => {
    if (pathData) {
      pathData.forEach((pathItem) => {
        if (
          pathItem.country === selectedCountry.value &&
          pathItem.to_currency === selectedCrypto.coinSymbol &&
          pathItem.from_currency === paymentCurrency.coinSymbol &&
          pathItem.depositMethod === paymentType.code &&
          pathItem.banker === selectedBanker.formData.exchangeID
        ) {
          setPathId(pathItem.path_id);
          return;
        }
      });
    }
  }, [
    pathData,
    selectedCountry,
    selectedCrypto,
    paymentCurrency,
    paymentType,
    selectedBanker,
  ]);

  const onKeyBoardShow = (e) => {
    setKeyboardHeight(e.endCoordinates.height);
    setIsKeyboardOpen(true);
  };

  const onKeyBoardHide = () => {
    setIsKeyboardOpen(false);
  };

  useEffect(() => {
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
      onKeyBoardShow,
    );
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
      onKeyBoardHide,
    );
    return () => {
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
        onKeyBoardShow,
      );
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
        onKeyBoardHide,
      );
    };
  }, []);

  const getApproxQuote = async (value, isSpending, showQuote) => {
    if (isNaN(parseFloat(value))) {
      setGettingValue('');
      return WToast.show({
        data: 'Please Input A Valid value',
        position: WToast.position.TOP,
      });
    }
    if (!pathId || parseFloat(value) <= 0) {
      return;
    }
    if (axiosToken) {
      axiosToken.cancel();
      // setIsLoading(false);
    }

    showQuote
      ? isSpending
        ? setIsGettingLoading(true)
        : setIsSpendLoading(true)
      : setPurchaseLoading(true);

    axiosToken = Axios.CancelToken.source();
    const profileId = await AsyncStorageHelper.getProfileId();
    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    let postData = {
      email,
      token,
      app_code: APP_CODE,
      profile_id: profileId,
      stats: showQuote,
      identifier: uuidv4(), // unique txn identifier
      path_id: pathId, // optional
    };

    if (isSpending) {
      postData = {
        ...postData,
        purchased_from: paymentCurrency.coinSymbol,
        from_amount: parseFloat(value),
      };
    } else {
      postData = {
        ...postData,
        coin_purchased: selectedCrypto.coinSymbol,
        purchased_amount: parseFloat(value),
      };
    }

    // console.log('Post Data', postData);

    const encryptedData = CryHelper.encryptPostData(postData);

    Axios.post(
      `${GX_API_ENDPOINT}/coin/vault/service/trade/execute`,
      {data: encryptedData},
      {
        cancelToken: axiosToken.token,
      },
    )
      .then((resp) => {
        const {data} = resp;

        // console.log('Quote Resp', data);

        if (data.status) {
          if (!showQuote) {
            WToast.show({data: data.message, position: WToast.position.TOP});
            onClose();
            return;
          }

          const cryptoAmount = isSpending
            ? data.finalToAmount
            : data.finalFromAmount;

          isSpending
            ? (getLoaded.current = true)
            : (spendLoaded.current = true);

          isSpending
            ? setGettingValue(
                formatterHelper(cryptoAmount, selectedCrypto.coinSymbol),
              )
            : setSpendValue(
                formatterHelper(cryptoAmount, paymentCurrency.coinSymbol),
              );
          setErrorText('');
        } else {
          if (data.message !== 'System Error') {
            WToast.show({data: data.message, position: WToast.position.TOP});
          }
          if (showQuote) {
            setSpendValue(formatterHelper(0, paymentCurrency.coinSymbol));
          }
        }
      })
      .catch((error) => {
        console.log('Error getting quote', error);
      })
      .finally(() => {
        showQuote
          ? isSpending
            ? setIsGettingLoading(false)
            : setIsSpendLoading(false)
          : setPurchaseLoading(false);

        resetFlags();
      });
  };

  const buyClickHandler = async () => {
    if (purchaseLoading) {
      return;
    }

    // send the last input to trade api
    getApproxQuote(spendEdited ? spendValue : gettingValue, spendEdited, false);
  };

  const resetFlags = () => {
    setTimeout(() => {
      getLoaded.current = false;
      spendLoaded.current = false;
    }, 200);
  };

  const onSpendEdit = (value) => {
    setSpendValue(value);
    setSpendEdited(true);
  };

  const onGetEdit = (value) => {
    setGettingValue(value);
    setSpendEdited(false);
  };

  useEffect(() => {
    if (!getLoaded.current) {
      getApproxQuote(gettingValue, false, true);
      setErrorText('');
    }
  }, [gettingValue]);

  useEffect(() => {
    if (!spendLoaded.current) {
      getApproxQuote(spendValue, true, true);
      setErrorText('');
    }
  }, [spendValue]);

  useCode(
    () =>
      isKeyboardOpen
        ? [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ]
        : [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ],
    [isKeyboardOpen],
  );

  return (
    <View style={styles.container}>
      <Animated.View
        style={{
          overflow: 'hidden',
          maxHeight: interpolate(chatKeyboardAnimation.current, {
            inputRange: [0, 1],
            outputRange: [0, 200],
          }),
        }}>
        <Text style={styles.header}>Get Quote</Text>
      </Animated.View>
      <Animated.View
        style={{
          overflow: 'hidden',
          maxHeight: !spendFocused
            ? interpolate(chatKeyboardAnimation.current, {
                inputRange: [0, 1],
                outputRange: [0, 200],
              })
            : 200,
        }}>
        <QuoteInput
          image={{
            uri: paymentCurrency.coinImage,
          }}
          unit={paymentCurrency.coinSymbol}
          enabled
          title="How Much Do You Want To Spend?"
          placeholder="0.00"
          value={spendValue}
          setValue={onSpendEdit}
          isLoading={isSpendLoading}
          onBlur={() => setSpendFocused(false)}
          onFocus={() => setSpendFocused(true)}
        />
      </Animated.View>
      <Animated.View
        style={{
          overflow: 'hidden',
          maxHeight: spendFocused
            ? interpolate(chatKeyboardAnimation.current, {
                inputRange: [0, 1],
                outputRange: [0, 200],
              })
            : 200,
        }}>
        <QuoteInput
          image={{
            uri: selectedCrypto.coinImage,
          }}
          unit={selectedCrypto.coinSymbol}
          title="You Will Be Getting Apx."
          enabled
          value={gettingValue.toString()}
          setValue={onGetEdit}
          placeholder="0.0000"
          isLoading={isGettingLoading}
          onBlur={() => setSpendFocused(true)}
          onFocus={() => setSpendFocused(false)}
        />
      </Animated.View>
      {errorText !== '' && (
        <View>
          <Text style={styles.errorText}>{errorText}</Text>
        </View>
      )}

      <TouchableOpacity
        style={[
          styles.buyBtn,
          (isSpendLoading || isGettingLoading) && styles.disabled,
        ]}
        onPress={buyClickHandler}
        disabled={isSpendLoading || isGettingLoading}>
        {purchaseLoading ? (
          <>
            <Text style={styles.buyBtnText}>Purchasing</Text>
            <ActivityIndicator
              style={{marginLeft: 20}}
              size="small"
              color="white"
            />
          </>
        ) : (
          <Text style={styles.buyBtnText}>
            Purchase {gettingValue || 0} {selectedCrypto.coinName}s
          </Text>
        )}
      </TouchableOpacity>
    </View>
  );
};

export default PaymentQuote;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  header: {
    color: '#08152D',
    textAlign: 'center',
    marginBottom: 30,
    fontSize: 25,
    fontFamily: 'Montserrat-SemiBold',
  },
  buyBtn: {
    backgroundColor: '#08152D',
    height: 45,
    borderRadius: 6,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  disabled: {
    opacity: 0.5,
  },
  errorText: {
    color: '#FF2D55',
    textAlign: 'center',
    marginBottom: 20,
    marginTop: -10,
  },
  buyBtnText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
  },
});
