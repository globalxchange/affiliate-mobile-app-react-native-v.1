import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {AppContext} from '../../../../contexts/AppContextProvider';
import LoadingAnimation from '../../../LoadingAnimation';
import CustomDropDown from '../../../CustomDropDown';
import {WToast} from 'react-native-smart-tip';
import {DepositContext} from '../../../../contexts/DepositContext';
import ActionButton from '../../../ActionButton';

const FundingMethodChooser = ({onNext, activeCrypto, pathData}) => {
  const {totalPaymentMethods} = useContext(AppContext);
  const {setInstaDepositPaymentType, instaDepositData} = useContext(
    DepositContext,
  );
  const {selectedCountry, paymentType, paymentCurrency} = instaDepositData;

  const [availableMethods, setAvailableMethods] = useState();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => setIsLoading(false), 200);
  }, []);

  useEffect(() => {
    if (
      totalPaymentMethods &&
      pathData &&
      activeCrypto &&
      selectedCountry &&
      paymentCurrency
    ) {
      const filterCountryList = [];

      totalPaymentMethods.forEach((methodItem) => {
        pathData.forEach((pathItem) => {
          if (
            pathItem.country === selectedCountry.value &&
            pathItem.to_currency === activeCrypto.coinSymbol &&
            pathItem.from_currency === paymentCurrency.coinSymbol &&
            pathItem.depositMethod === methodItem.code
          ) {
            if (!filterCountryList.includes(methodItem)) {
              filterCountryList.push(methodItem);
              return;
            }
          }
        });
      });
      setAvailableMethods(filterCountryList);
    }
  }, [
    pathData,
    totalPaymentMethods,
    activeCrypto,
    selectedCountry,
    paymentCurrency,
  ]);

  const onNextClick = () => {
    if (!paymentType) {
      return WToast.show({
        data: 'Please Select A Payment Type',
        position: WToast.position.TOP,
      });
    }
    onNext();
  };

  if (!availableMethods || isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <LoadingAnimation />
      </View>
    );
  }

  if (availableMethods.length <= 0) {
    return (
      <View style={styles.loadingContainer}>
        <Text style={styles.notAvailableText}>
          No Payment Method Supported to Buy {activeCrypto.coinName} in{' '}
          {selectedCountry.value} with {paymentCurrency.coinSymbol} as Payment
          Currency
        </Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Select Funding Method</Text>
      <Text style={styles.subHeader}>
        You Can Buy {activeCrypto.coinName} In {selectedCountry.Key} Using The
        Following Funding Methods
      </Text>
      <View style={styles.controlContainer}>
        <CustomDropDown
          label="Available Payment Method"
          placeHolder="Select Payment Method"
          items={availableMethods}
          selectedItem={paymentType}
          onDropDownSelect={setInstaDepositPaymentType}
        />
      </View>
      <ActionButton text="Proceed To Bankers" onPress={onNextClick} />
    </View>
  );
};

export default FundingMethodChooser;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    color: '#08152D',
    textAlign: 'center',
    marginBottom: 30,
    fontSize: 20,
    fontFamily: 'Montserrat-SemiBold',
  },
  controlContainer: {
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  subHeader: {
    color: '#08152D',
    textAlign: 'center',
    marginBottom: 30,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  loadingContainer: {
    justifyContent: 'center',
    flex: 1,
  },
  notAvailableText: {
    color: '#08152D',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
  },
});
