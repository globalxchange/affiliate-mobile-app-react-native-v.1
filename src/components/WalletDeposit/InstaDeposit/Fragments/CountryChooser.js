import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {AppContext} from '../../../../contexts/AppContextProvider';
import CustomDropDown from '../../../CustomDropDown';
import LoadingAnimation from '../../../LoadingAnimation';
import {WToast} from 'react-native-smart-tip';
import ActionButton from '../../../ActionButton';
import {DepositContext} from '../../../../contexts/DepositContext';

const CountryChooser = ({onNext, activeCrypto, pathData}) => {
  const {countryList} = useContext(AppContext);

  const {setInstaDepositCountry, instaDepositData} = useContext(DepositContext);

  const {selectedCountry} = instaDepositData;

  const [filteredCountries, setFilteredCountries] = useState();
  const [isLoading, setIsLoading] = useState(true);

  const onNextClick = () => {
    if (!selectedCountry) {
      return WToast.show({
        data: 'Please Select A Country First',
        position: WToast.position.TOP,
      });
    }

    onNext();
  };

  useEffect(() => {
    setTimeout(() => setIsLoading(false), 200);
  }, []);

  useEffect(() => {
    if (countryList && activeCrypto && pathData) {
      const filterCountryList = [];

      let currentCountryFound = false;

      countryList.forEach((countryItem) => {
        pathData.forEach((pathItem) => {
          if (
            pathItem.country === countryItem.value &&
            activeCrypto.coinSymbol === pathItem.to_currency
          ) {
            if (selectedCountry.value === pathItem.country) {
              currentCountryFound = true;
            }
            if (!filterCountryList.includes(countryItem)) {
              filterCountryList.push(countryItem);
              return;
            }
          }
        });
      });

      if (!currentCountryFound) {
        setInstaDepositCountry('');
      }

      setFilteredCountries(filterCountryList);
    }
  }, [countryList, activeCrypto, pathData]);

  if (!filteredCountries || isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <LoadingAnimation />
      </View>
    );
  }

  if (filteredCountries.length <= 0) {
    return (
      <View style={styles.loadingContainer}>
        <Text style={styles.notAvailableText}>
          No Countries Supported For {activeCrypto.coinName}
        </Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Confirm Country</Text>
      <Text style={styles.subHeader}>
        Your Default Country Is Set As {selectedCountry.name}. Is This The
        Country You Wish To Make This Transaction From?
      </Text>
      <View style={styles.controlContainer}>
        <CustomDropDown
          label="Select A Country"
          onDropDownSelect={setInstaDepositCountry}
          placeHolder="Please Select A Country"
          items={filteredCountries}
          selectedItem={selectedCountry}
        />
      </View>
      <ActionButton text="Proceed To Payment Method" onPress={onNextClick} />
    </View>
  );
};

export default CountryChooser;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    color: '#08152D',
    textAlign: 'center',
    marginBottom: 30,
    fontSize: 20,
    fontFamily: 'Montserrat-SemiBold',
  },
  controlContainer: {
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  subHeader: {
    color: '#08152D',
    textAlign: 'center',
    marginBottom: 30,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  loadingContainer: {
    justifyContent: 'center',
    flex: 1,
  },
  notAvailableText: {
    color: '#08152D',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
  },
});
