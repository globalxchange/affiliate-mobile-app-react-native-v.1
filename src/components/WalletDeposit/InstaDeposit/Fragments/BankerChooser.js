/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';
import {formatterHelper, usdValueFormatter} from '../../../../utils';
import Axios from 'axios';
import {TELLER_API_ENDPOINT} from '../../../../configs';
import LoadingAnimation from '../../../LoadingAnimation';
import {AppContext} from '../../../../contexts/AppContextProvider';
import {DepositContext} from '../../../../contexts/DepositContext';

const BankerChooser = ({
  onNext,
  activeCrypto,
  toggleExpansion,
  isExpanded,
  pathData,
}) => {
  const {totalPaymentMethods} = useContext(AppContext);

  const {setInstaDepositSelectedBanker, instaDepositData} = useContext(
    DepositContext,
  );
  const {selectedCountry, paymentType, paymentCurrency} = instaDepositData;

  const [activeType, setActiveType] = useState('Bankers');
  const [bankersList, setBankersList] = useState();
  const [tellersList, setTellersList] = useState();

  useEffect(() => {
    Axios.get(`${TELLER_API_ENDPOINT}/matchOrder`, {
      // params: {
      //   paymentMethod: 'tellerTransfer',
      //   preferredCurrency: 'any',
      //   transactionAsset: 'GXT',
      // },
    })
      .then((resp) => {
        const {data} = resp;

        if (data.status) {
          setTellersList(data.data);
        }
      })
      .catch((error) => console.log('error Getting Live Order', error));
  }, []);

  useEffect(() => {
    if (
      totalPaymentMethods &&
      pathData &&
      activeCrypto &&
      selectedCountry &&
      paymentCurrency
    ) {
      Axios.get(
        'https://storeapi.apimachine.com/dynamic/iceprotocol/exchangeList?key=62816d7e-e570-46a2-8c9d-0c1cc97f7bf8',
      )
        .then((resp) => {
          const {data} = resp;

          const bankers = data.data || [];

          const filterBankerList = [];

          bankers.forEach((bankerItem) => {
            pathData.forEach((pathItem) => {
              if (
                pathItem.country === selectedCountry.value &&
                pathItem.to_currency === activeCrypto.coinSymbol &&
                pathItem.from_currency === paymentCurrency.coinSymbol &&
                pathItem.depositMethod === paymentType.code &&
                pathItem.banker === bankerItem.formData.exchangeID
              ) {
                if (!filterBankerList.includes(bankerItem)) {
                  filterBankerList.push(bankerItem);
                  return;
                }
              }
            });
          });
          setBankersList(filterBankerList);
        })
        .catch((error) => console.log('error on getting Bankers List', error));
    }
  }, [
    pathData,
    totalPaymentMethods,
    activeCrypto,
    selectedCountry,
    paymentCurrency,
    paymentType,
  ]);

  const onBankerSelect = (selectedBanker) => {
    setInstaDepositSelectedBanker(selectedBanker);
    onNext();
  };

  if (!bankersList) {
    return (
      <View style={styles.loadingContainer}>
        <LoadingAnimation />
      </View>
    );
  }

  if (bankersList.length <= 0) {
    return (
      <View style={styles.loadingContainer}>
        <Text style={styles.notAvailableText}>
          No Banker Supported to Buy {activeCrypto.coinName} in{' '}
          {selectedCountry.value} with {paymentCurrency.coinSymbol} as Payment
          Currency with {paymentType.name} Method
        </Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Select One of The Vendors</Text>
      <TouchableOpacity
        style={styles.expandIconContainer}
        onPress={toggleExpansion}>
        <Image
          style={styles.expandIcon}
          source={
            isExpanded
              ? require('../../../../assets/collapse-icon.png')
              : require('../../../../assets/expand-icon.png')
          }
          resizeMode="contain"
        />
      </TouchableOpacity>
      <View style={styles.switchContainer}>
        <TouchableOpacity
          style={[
            styles.switchItem,
            {borderRightColor: '#EBEBEB', borderRightWidth: 1},
          ]}
          onPress={() => setActiveType('Bankers')}>
          <Text
            style={[
              styles.switchText,
              activeType === 'Bankers' && {
                fontFamily: 'Montserrat-Bold',
              },
            ]}>
            Bankers
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          disabled
          style={styles.switchItem}
          onPress={() => setActiveType('Tellers')}>
          <Text
            style={[
              styles.switchText,
              activeType === 'Tellers' && {
                fontFamily: 'Montserrat-Bold',
              },
            ]}>
            Tellers
          </Text>
        </TouchableOpacity>
      </View>

      {activeType === 'Bankers' ? (
        <>
          {bankersList ? (
            <FlatList
              style={styles.scrollView}
              data={bankersList}
              showsVerticalScrollIndicator={false}
              keyExtractor={(item) => item.formData.exchangeID}
              renderItem={({item}) => (
                <TouchableOpacity
                  style={styles.item}
                  onPress={() => onBankerSelect(item)}>
                  <Image
                    style={styles.cryptoIcon}
                    resizeMode="contain"
                    source={{
                      uri: item.formData.image,
                    }}
                  />
                  <View style={styles.nameContainer}>
                    <Text style={styles.cryptoName}>
                      {item.formData.exchangeID}
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
              ListEmptyComponent={
                <View style={styles.emptyContainer}>
                  <Text style={styles.emptyText}>
                    No Supported Exchanges Found
                  </Text>
                </View>
              }
            />
          ) : (
            <View style={styles.loadingContainer}>
              <LoadingAnimation />
              <Text style={styles.loadingText}>Fetching Data...</Text>
            </View>
          )}
        </>
      ) : (
        <>
          {tellersList ? (
            <FlatList
              style={styles.scrollView}
              data={tellersList}
              showsVerticalScrollIndicator={false}
              keyExtractor={(item) => `${item._id}_${Math.random()}`}
              renderItem={({item}) => (
                <TouchableOpacity
                  style={styles.item}
                  onPress={() => onBankerSelect(item)}>
                  <Image
                    style={styles.cryptoIcon}
                    resizeMode="contain"
                    source={{
                      uri: 'https://i.pravatar.cc/110',
                    }}
                  />
                  <View style={styles.nameContainer}>
                    <Text style={styles.cryptoName}>
                      {item.profile.cognitoName}
                    </Text>
                  </View>
                  <View style={styles.priceContainer}>
                    <Text style={styles.orderVolume}>
                      {formatterHelper(
                        item.volumeLimitMax,
                        item.transactionAsset,
                      )}{' '}
                      {item.transactionAsset}
                    </Text>
                    <Text style={styles.cryptoPrice}>
                      {usdValueFormatter.format(
                        item.volumeLimitMax * item.price,
                      )}
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
              ListEmptyComponent={
                <View style={styles.emptyContainer}>
                  <Text style={styles.emptyText}>
                    No Supported Crypto Found
                  </Text>
                </View>
              }
            />
          ) : (
            <View style={styles.loadingContainer}>
              <LoadingAnimation />
              <Text style={styles.loadingText}>Fetching Data...</Text>
            </View>
          )}
        </>
      )}

      {/* <ActionButton text="Proceed To Bankers" onPress={onNext} /> */}
    </View>
  );
};

export default BankerChooser;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    color: '#08152D',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 35,
  },
  switchContainer: {
    flexDirection: 'row',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 10,
    marginTop: 25,
  },
  switchItem: {
    flex: 1,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
  },
  switchText: {
    color: '#08152D',
    fontFamily: 'Montserrat',
  },
  methodList: {},
  menuItem: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomColor: '#EBEBEB',
    borderBottomWidth: 1,
  },
  menuItemImage: {
    width: 100,
  },
  smallMenuIcon: {
    width: 20,
    height: 20,
  },
  methodName: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#001D41',
    marginLeft: 20,
    paddingVertical: 30,
  },
  scrollView: {flex: 1, paddingHorizontal: 10, paddingTop: 10},
  item: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 25,
    backgroundColor: 'white',
    marginBottom: 10,
    alignItems: 'center',
    borderBottomColor: '#EBEBEB',
    borderBottomWidth: 1,
  },
  cryptoIcon: {
    width: 25,
    height: 25,
    borderRadius: 15,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  nameContainer: {
    flexGrow: 1,
    width: 0,
    marginLeft: 10,
  },
  cryptoName: {
    color: '#001D41',
    fontSize: 16,
    marginBottom: 2,
    fontFamily: 'Montserrat-Bold',
  },
  noOfVendors: {
    color: '#001D41',
    opacity: 0.5,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  priceContainer: {
    alignItems: 'center',
  },
  noOfPaymentMethod: {
    color: '#001D41',
    opacity: 0.5,
    marginBottom: 2,
    textAlign: 'right',
    fontFamily: 'Roboto-Bold',
  },
  cryptoPrice: {
    color: '#001D41',
    marginBottom: 2,
    textAlign: 'right',
    fontFamily: 'Roboto-Bold',
  },
  orderVolume: {
    textAlign: 'right',
    color: '#001D41',
    fontFamily: 'Roboto',
    fontSize: 10,
  },
  loadingText: {marginTop: 10, fontFamily: 'Montserrat'},
  emptyContainer: {
    marginTop: 20,
  },
  emptyText: {
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
    textAlign: 'center',
    fontSize: 16,
  },
  expandIconContainer: {
    position: 'absolute',
    top: 0,
    right: 10,
    width: 18,
    height: 18,
  },
  expandIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  loadingContainer: {
    justifyContent: 'center',
    flex: 1,
  },
  notAvailableText: {
    color: '#08152D',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
  },
});
