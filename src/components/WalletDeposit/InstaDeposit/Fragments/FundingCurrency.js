import React, {useState, useEffect, useContext} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import CustomDropDown from '../../../CustomDropDown';
import {AppContext} from '../../../../contexts/AppContextProvider';
import LoadingAnimation from '../../../LoadingAnimation';
import {WToast} from 'react-native-smart-tip';
import {DepositContext} from '../../../../contexts/DepositContext';
import ActionButton from '../../../ActionButton';

const FundingCurrency = ({onNext, pathData, activeCrypto}) => {
  const {cryptoTableData} = useContext(AppContext);
  const {instaDepositData, setCheckOutPaymentCurrency} = useContext(
    DepositContext,
  );
  const {selectedCountry, paymentCurrency} = instaDepositData;

  const [availableCurrencies, setAvailableCurrencies] = useState();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => setIsLoading(false), 200);
  }, []);

  useEffect(() => {
    if (cryptoTableData && pathData && activeCrypto && selectedCountry) {
      const filterCountryList = [];

      cryptoTableData.forEach((cryptoItem) => {
        pathData.forEach((pathItem) => {
          if (
            pathItem.country === selectedCountry.value &&
            pathItem.to_currency === activeCrypto.coinSymbol &&
            pathItem.from_currency === cryptoItem.coinSymbol
          ) {
            if (!filterCountryList.includes(cryptoItem)) {
              filterCountryList.push(cryptoItem);
              return;
            }
          }
        });
      });
      setAvailableCurrencies(filterCountryList);
    }
  }, [pathData, cryptoTableData, activeCrypto, selectedCountry]);

  const onNextClick = () => {
    if (!paymentCurrency) {
      return WToast.show({
        data: 'Please select a funding currency',
        position: WToast.position.TOP,
      });
    }
    onNext();
  };

  if (!availableCurrencies || isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <LoadingAnimation />
      </View>
    );
  }

  if (availableCurrencies.length <= 0) {
    return (
      <View style={styles.loadingContainer}>
        <Text style={styles.notAvailableText}>
          No Currencies Supported to Buy {activeCrypto.coinName} in{' '}
          {selectedCountry.value}
        </Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Select Payment Currency</Text>
      <View style={styles.controlContainer}>
        <View style={styles.dropDownContainer}>
          <CustomDropDown
            label="Select Payment Currency"
            items={availableCurrencies}
            placeHolder="Available Currencies"
            onDropDownSelect={setCheckOutPaymentCurrency}
            selectedItem={paymentCurrency}
          />
        </View>
      </View>
      <ActionButton text="Proceed To Methods" onPress={onNextClick} />
    </View>
  );
};

export default FundingCurrency;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    color: '#08152D',
    textAlign: 'center',
    marginBottom: 30,
    fontSize: 20,
    fontFamily: 'Montserrat-SemiBold',
  },
  controlContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  radioContainer: {
    flexDirection: 'row',
  },
  radioButton: {
    flex: 1,
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 4,
    paddingVertical: 10,
  },
  radioButtonActive: {
    borderColor: '#08152D',
  },
  radioText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#788995',
    textAlign: 'center',
  },
  radioTextActive: {
    color: '#08152D',
  },
  dropDownContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  loadingContainer: {
    justifyContent: 'center',
    flex: 1,
  },
  notAvailableText: {
    color: '#08152D',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
  },
});
