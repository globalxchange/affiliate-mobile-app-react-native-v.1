import React, {useState, useContext, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import QRCode from 'react-native-qrcode-svg';
import {DepositContext} from '../../contexts/DepositContext';
import {AppContext} from '../../contexts/AppContextProvider';
import Clipboard from '@react-native-community/clipboard';
import Axios from 'axios';
import {GX_API_ENDPOINT, APP_CODE} from '../../configs';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';

const {width} = Dimensions.get('window');

const WalletTransfer = () => {
  const {activeWallet} = useContext(DepositContext);

  const {walletCoinData, updateWalletBalances} = useContext(AppContext);

  const [isCopied, setIsCopied] = useState(false);
  const [qrSize, setQrSize] = useState(0);
  const [address, setAddress] = useState('');

  useEffect(() => {
    if (walletCoinData) {
      const coin = walletCoinData.find(
        (item) => item.coinSymbol === activeWallet.coinSymbol,
      );

      // console.log('walletCoinData', walletCoinData);

      const addr = coin?.coin_address || coin?.erc20Address || '';
      setAddress(addr);
      if (
        !addr &&
        (activeWallet.coinSymbol === 'BTC' ||
          activeWallet.coinSymbol === 'TRX' ||
          activeWallet.coinSymbol === 'XRP')
      ) {
        requestForAddress(activeWallet.coinSymbol);
      }
    }
  }, [activeWallet, walletCoinData]);

  const copyToClipboardHandler = () => {
    Clipboard.setString(address);
    setIsCopied(true);
  };

  const qrContainerSizeHandler = (event) => {
    const {height} = event.nativeEvent.layout;

    if (height > width * 0.75) {
      setQrSize(width * 0.75);
    } else {
      setQrSize(height);
    }
  };

  const requestForAddress = async (coin) => {
    const email = await AsyncStorageHelper.getLoginEmail();

    let postData = {email, app_code: APP_CODE};

    if (coin !== 'BTC') {
      postData = {...postData, coin};
    }

    let API =
      coin === 'BTC'
        ? `${GX_API_ENDPOINT}/coin/vault/service/coin/request/address`
        : `${GX_API_ENDPOINT}/coin/vault/service/crypto/address/request`;

    Axios.post(API, postData)
      .then((resp) => {
        const {data} = resp;
        // console.log('Data', data);

        setAddress(data.address || '');

        updateWalletBalances();
      })
      .catch((error) => {
        console.log('Error on requesting address', error);
      });
  };

  // console.log('Wallet', walletBalances);

  return (
    <View style={styles.container}>
      <Text style={styles.header}>
        {activeWallet
          ? activeWallet.coinName.length < 10
            ? `Deposit ${activeWallet.coinName}`
            : `Deposit ${activeWallet.coinSymbol}`
          : 'Loading'}
      </Text>
      <View style={styles.addressContainer}>
        <View style={styles.qrContainer} onLayout={qrContainerSizeHandler}>
          <QRCode
            value={address || 'Empty'}
            size={qrSize}
            color={address ? '#08152D' : '#C5DAEC'}
          />
        </View>
        <View style={styles.addressTextContainer}>
          <TouchableOpacity
            style={styles.textContainer}
            onPress={() => setIsCopied(false)}>
            <Text style={[styles.address, isCopied && styles.copiedText]}>
              {isCopied ? 'Copied To Clipboard' : address || 'Empty'}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.copyContainer}
            onPress={copyToClipboardHandler}>
            <Image
              style={styles.copyIcon}
              source={require('../../assets/copy-icon.png')}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>

        {/* {activeWallet.coinSymbol === 'BTC' && !address && (
          <TouchableOpacity
            style={styles.generateAddressButton}
            onPress={requestForAddress}>
            <Text style={styles.generateAddressButtonText}>Click</Text>
            <Text style={styles.generateAddressButtonTextBold}>{' Here '}</Text>
            <Text style={styles.generateAddressButtonText}>
              If You Don’t See Your Address
            </Text>
          </TouchableOpacity>
        )} */}
      </View>
    </View>
  );
};

export default WalletTransfer;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
  },
  header: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  addressContainer: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: 20,
    paddingHorizontal: 50,
  },
  qrContainer: {
    flexGrow: 1,
    height: 0,
    justifyContent: 'center',
    marginBottom: 20,
  },
  addressTextContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textContainer: {
    borderColor: '#EDEDED',
    borderWidth: 2,
    borderRadius: 6,
    flexGrow: 1,
    width: 0,
    justifyContent: 'center',
    height: 40,
  },
  address: {
    color: '#8391A3',
    textAlign: 'center',
    fontSize: 10,
  },
  copiedText: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
  },
  copyContainer: {
    borderColor: '#EDEDED',
    borderWidth: 2,
    borderRadius: 6,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 5,
    marginLeft: 5,
  },
  copyIcon: {width: 20, height: 24},
  notSupportedView: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  notSupportedText: {
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
    fontSize: 18,
  },
  generateAddressButton: {
    marginTop: 15,
    paddingVertical: 5,
    flexDirection: 'row',
    backgroundColor: '#08152D',
    paddingHorizontal: 20,
    borderRadius: 4,
  },
  generateAddressButtonText: {
    color: 'white',
    fontFamily: 'Montserrat',
    fontSize: 13,
  },
  generateAddressButtonTextBold: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 13,
  },
});
