import React, {useContext} from 'react';
import {StyleSheet, View} from 'react-native';
import MethodSelector from './MethodSelector';
import {DepositContext} from '../../contexts/DepositContext';
import VaultDeposit from './VaultDeposit';
import InstaDeposit from './InstaDeposit';
import WalletTransfer from './WalletTransfer';

const WalletDeposit = ({activeWallet, openTxnAudit, isKeyBoardOpen}) => {
  const {step, setStep} = useContext(DepositContext);

  const renderActiveComponent = () => {
    switch (step) {
      case 'InstaDeposit':
        return (
          <InstaDeposit
            activeCrypto={activeWallet}
            onClose={() => setStep(0)}
          />
        );
      case 'WalletTransfer':
        return <WalletTransfer />;
      case 'VaultDeposit':
        return (
          <VaultDeposit
            openTxnAudit={openTxnAudit}
            isKeyBoardOpen={isKeyBoardOpen}
          />
        );
      default:
        return <MethodSelector />;
    }
  };

  return <View style={styles.container}>{renderActiveComponent()}</View>;
};

export default WalletDeposit;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
  },
});
