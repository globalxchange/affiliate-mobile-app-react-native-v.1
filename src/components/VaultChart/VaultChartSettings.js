import React, {useState, useMemo} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Switch,
} from 'react-native';
import PopupLayout from '../../layouts/PopupLayout';
import ThemeData from '../../configs/ThemeData';

const VaultChartSettings = ({
  chartSettings,
  setChartSettings,
  clearChartConfig,
}) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const menuList = useMemo(
    () => [
      {
        title: 'Time Interval',
        icon: require('../../assets/chartSettings/time-interval.png'),
        key: 'timeInterval',
      },
      {
        title: 'Time Horizon',
        icon: require('../../assets/chartSettings/time-horizon.png'),
        key: 'timeHorizon',
      },
      {
        title: 'Prices',
        icon: require('../../assets/chartSettings/prices.png'),
        isRadio: true,
        key: 'prices',
      },
      {
        title: 'Chart Type',
        icon: require('../../assets/chartSettings/chart-type.png'),
        key: 'chartType',
      },
      {
        title: 'Horizontal Scroll',
        icon: require('../../assets/chartSettings/horizontal-scroll.png'),
        isRadio: true,
        key: 'horizotalScrolling',
      },
    ],
    [],
  );

  const onToggle = (key) => {
    const newState = {...chartSettings};
    newState[key] = !newState[key];
    setChartSettings(newState);
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={() => setIsModalOpen(true)}>
        <Image
          style={styles.btnIcon}
          source={require('../../assets/market-charts-icon.png')}
        />
      </TouchableOpacity>
      <PopupLayout
        autoHeight
        noHeader
        noScrollView
        containerStyles={styles.modalStyle}
        isOpen={isModalOpen}
        onClose={() => setIsModalOpen(false)}>
        <View style={styles.modalHeader}>
          <Image
            style={styles.modalHeaderIcon}
            source={require('../../assets/market-charts-logo.png')}
          />
        </View>
        <View style={styles.menuList}>
          {menuList.map((item) => (
            <TouchableOpacity
              onPress={() => {
                if (item.isRadio) {
                  onToggle(item.key);
                } else {
                }
              }}>
              <View style={styles.menuItem}>
                <Image style={styles.menuItemIcon} source={item.icon} />
                <Text style={styles.menuItemText}>{item.title}</Text>
                {item.isRadio ? (
                  <>
                    <Switch
                      value={chartSettings[item.key]}
                      style={styles.switch}
                      onValueChange={(value) => onToggle(item.key)}
                      thumbColor={ThemeData.APP_MAIN_COLOR}
                      trackColor={{true: ThemeData.BORDER_COLOR, false: 'grey'}}
                    />
                    <Text style={styles.menuItemValue}>
                      {chartSettings[item.key] ? 'On' : 'Off'}
                    </Text>
                  </>
                ) : (
                  <Text style={styles.menuItemValue}>
                    {chartSettings[item.key]}
                  </Text>
                )}
              </View>
            </TouchableOpacity>
          ))}
        </View>
        <View style={styles.actionContainer}>
          <TouchableOpacity
            style={styles.actionBtnOultine}
            onPress={() => setIsModalOpen(false)}>
            <Text style={styles.actionBtnOultineText}>Close</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.actionBtnFilled}
            onPress={() => {
              clearChartConfig();
              setIsModalOpen(false);
            }}>
            <Text style={styles.actionBtnFilledText}>Clear Config</Text>
          </TouchableOpacity>
        </View>
      </PopupLayout>
    </View>
  );
};

export default VaultChartSettings;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 15,
    marginBottom: 20,
  },
  btnIcon: {
    height: 20,
    width: 150,
    resizeMode: 'contain',
  },
  modalStyle: {
    padding: 0,
  },
  modalHeader: {
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    marginHorizontal: -30,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -15,
  },
  modalHeaderIcon: {
    height: 30,
    resizeMode: 'contain',
    marginBottom: 15,
  },
  menuList: {},
  menuItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 30,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
  },
  menuItemIcon: {
    resizeMode: 'contain',
    width: 20,
    height: 20,
  },
  menuItemText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.TEXT_COLOR,
    marginLeft: 10,
    flex: 1,
  },
  menuItemValue: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.TEXT_COLOR,
    fontSize: 12,
  },
  actionContainer: {
    flexDirection: 'row',
    marginBottom: -30,
    marginHorizontal: -30,
    borderTopColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
  },
  actionBtnOultine: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
  },
  actionBtnOultineText: {
    color: ThemeData.TEXT_COLOR,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  actionBtnFilled: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
  },
  actionBtnFilledText: {
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  switch: {
    transform: [{scaleX: 0.5}, {scaleY: 0.5}],
  },
  swicthText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.TEXT_COLOR,
    fontSize: 11,
  },
});
