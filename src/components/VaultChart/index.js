/* eslint-disable react-native/no-inline-styles */
import React, {useState, useMemo} from 'react';
import {StyleSheet, Text, View, Dimensions} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import {LineChart} from 'react-native-chart-kit';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import VaultChartSettings from './VaultChartSettings';
import {useQuery} from 'react-query';
import axios from 'axios';
import {GX_API_ENDPOINT} from '../../configs';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import moment from 'moment';
import LoadingAnimation from '../LoadingAnimation';

const {width: sWidth} = Dimensions.get('window');
const X_LABEL_WIDTH = 60;
const X_LABEL_COUNT = Math.round((sWidth * 0.9) / X_LABEL_WIDTH);

const INITIAL_CHART_CONFIG = {
  timeInterval: 'Day',
  timeHorizon: 'All Time',
  prices: true,
  chartType: 'Line',
  horizotalScrolling: true,
};

const getChartData = async ({queryKey}) => {
  const [_key, postData] = queryKey;

  const email = await AsyncStorageHelper.getLoginEmail();
  // const email = 'shorupan@brokerapp.io';

  const {
    data,
  } = await axios.get(
    `${GX_API_ENDPOINT}/coin/vault/service/user/vault/historic/holdings/data`,
    {params: {email, ...postData}},
  );

  return data?.balances_data || [];
};

const VaultChart = ({activeWallet, selectedApp}) => {
  const {bottom} = useSafeAreaInsets();

  const [chartSize, setChartSize] = useState({width: 0, height: 0});
  const [chartSettings, setChartSettings] = useState(INITIAL_CHART_CONFIG);

  const {data, status} = useQuery(
    [
      'chartData',
      {
        app_code: selectedApp?.app_code,
        coin: activeWallet?.coinSymbol,
      },
    ],
    getChartData,
  );

  const dataSet = useMemo(
    () => ({
      labels: data?.map((item) => item.timestamp) || [],
      datasets: [
        {
          data: data?.map((item) => item.price) || [],
        },
      ],
    }),
    [data],
  );

  const yValues = useMemo(() => {
    const values = [];

    if (data?.length <= X_LABEL_COUNT) {
      return data?.map((item) => item.timestamp) || [];
    }

    const interval = Math.round(data?.length / X_LABEL_COUNT);

    for (let i = 0; i < data?.length; i += interval) {
      if (data[i]) {
        values.push(data[i].timestamp);
      }
    }

    return values;
  }, [data]);

  const clearChartConfig = () => {
    setChartSettings(INITIAL_CHART_CONFIG);
  };

  return (
    <>
      {status === 'loading' ? (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      ) : data?.length <= 0 ? (
        <View style={styles.loadingContainer}>
          <Text style={styles.errorText}>Chart Data Not Available</Text>
        </View>
      ) : (
        <>
          <View
            style={[
              styles.chartContainer,
              chartSettings.prices || {marginLeft: -50},
            ]}
            onLayout={(event) => {
              const {width, height} = event.nativeEvent.layout;
              setChartSize({width, height});
            }}>
            <LineChart
              data={dataSet}
              width={chartSize.width}
              height={chartSize.height}
              withInnerLines={false}
              withHorizontalLines={false}
              withVerticalLines={false}
              withShadow={false}
              withHorizontalLabels={chartSettings.prices}
              withVerticalLabels={false}
              yAxisLabel="$"
              yAxisInterval={1}
              chartConfig={{
                backgroundGradientFrom: '#FFFF',
                backgroundGradientTo: '#FFFF',
                backgroundColor: '#FFFF',
                decimalPlaces: 2,
                color: (opacity = 1) => '#0E9347',
                labelColor: (opacity = 1) => '#5F6163',
                propsForLabels: {fontSize: 10},
                propsForDots: {
                  r: '0',
                },
                style: {
                  padding: 0,
                  margin: 0,
                  backgroundColor: 'red',
                },
                propsForVerticalLabels: {
                  disabled: true,
                },
              }}
              style={styles.chartStyle}
            />
          </View>
          <VaultChartSettings
            chartSettings={chartSettings}
            setChartSettings={setChartSettings}
            clearChartConfig={clearChartConfig}
          />
          {chartSettings.horizotalScrolling && (
            <View
              style={[
                styles.chartXValuesContainer,
                {marginBottom: -bottom, paddingBottom: bottom},
                chartSettings.prices && {paddingLeft: 50},
              ]}>
              {yValues?.map((item) => (
                <Text key={item} style={styles.chartXvalue}>
                  {moment(item).format('MMM DD/YY')}
                </Text>
              ))}
            </View>
          )}
        </>
      )}
    </>
  );
};

export default VaultChart;

const styles = StyleSheet.create({
  chartContainer: {
    flex: 1,
    // marginHorizontal: -15,
  },
  chartXValuesContainer: {
    flexDirection: 'row',
    backgroundColor: '#F1F4F6',
    paddingVertical: 10,
  },
  chartXvalue: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.TEXT_COLOR,
    fontSize: 10,
    width: X_LABEL_WIDTH,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  errorText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.TEXT_COLOR,
    textAlign: 'center',
  },
});
