import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {usdValueFormatter, percentageFormatter} from '../utils';
import CandleChart from './CandleChart';

const CryptoDetailsCard = ({
  activeCrypto,
  isBottomSheetOpen,
  buyClickHandler,
  sellClickHandler,
  depositClickHandler,
  withdrawClickHandler,
}) => {
  const parseTheValue = (value = 0) => {
    let parsedValue;
    let symbol;

    if (value > 1000000000000) {
      parsedValue = value / 1000000000000;
      symbol = 'T';
    } else if (value > 1000000000) {
      parsedValue = value / 1000000000;
      symbol = 'B';
    } else {
      parsedValue = value / 1000000;
      symbol = 'M';
    }

    return `${parsedValue.toFixed(2)} ${symbol}`;
  };

  return (
    <View style={styles.container}>
      <LinearGradient
        colors={['#f9f9f9', '#ffffff']}
        style={styles.gradientBackground}>
        {activeCrypto && (
          <>
            <View style={styles.headerContainer}>
              <View
                style={[
                  styles.imageContainer,
                  !isBottomSheetOpen ? {elevation: 1} : {},
                ]}>
                <Image
                  style={styles.image}
                  source={{
                    uri: activeCrypto.coinImage,
                  }}
                  resizeMode="cover"
                />
              </View>
              <View style={styles.namesContainer}>
                <Text style={styles.name}>{activeCrypto.coinName}</Text>
                <Text style={styles.vendors}>
                  {`${activeCrypto.noOfVendor || 0} Vendors`}
                </Text>
              </View>
              <View style={styles.pricesContainer}>
                <Text style={styles.price}>
                  {/* Price */}
                  {usdValueFormatter.format(activeCrypto.price.USD)}
                </Text>
                {/* Change */}
                <Text
                  style={
                    activeCrypto._24hrchange || 0
                      ? styles.upChange
                      : styles.downChange
                  }>
                  {`${percentageFormatter.format(
                    activeCrypto._24hrchange || 0,
                  )}%`}
                </Text>
              </View>
            </View>
            <View style={styles.candleCartContainer}>
              <CandleChart activeCrypto={activeCrypto} />
            </View>
            <View style={styles.changesContainer}>
              <View
                style={[
                  styles.changeItem,
                  !isBottomSheetOpen ? {elevation: 2} : {},
                ]}>
                {/* Change Hr */}
                <Text
                  style={
                    activeCrypto.market_cap > 0
                      ? styles.changeValueUp
                      : styles.changeValueDown
                  }>
                  {parseTheValue(activeCrypto.market_cap)}
                </Text>
                <Text style={styles.changeDuration}>Market Cap</Text>
              </View>
              <View
                style={[
                  styles.changeItem,
                  !isBottomSheetOpen ? {elevation: 2} : {},
                ]}>
                <Text
                  style={
                    activeCrypto.volume_24hr > 0
                      ? styles.changeValueUp
                      : styles.changeValueDown
                  }>
                  {parseTheValue(activeCrypto.volume_24hr)}
                </Text>
                <Text style={styles.changeDuration}>24hr Volume</Text>
              </View>
              <View
                style={[
                  {...styles.changeItem, marginRight: 0},
                  !isBottomSheetOpen ? {elevation: 2} : {},
                ]}>
                <Text
                  style={
                    activeCrypto._24hrchange > 0
                      ? styles.changeValueUp
                      : styles.changeValueDown
                  }>
                  {percentageFormatter.format(
                    Math.abs(activeCrypto._24hrchange),
                  )}
                  %
                </Text>
                <Text style={styles.changeDuration}>24 Hrs Change</Text>
              </View>
            </View>
            <View style={styles.column}>
              <View style={styles.actionBtnContainer}>
                <View style={[styles.buttonContainer, styles.buyBtnContainer]}>
                  <TouchableOpacity
                    style={[styles.button, styles.buyBtn]}
                    onPress={buyClickHandler}>
                    <Text style={styles.btnText}>Buy</Text>
                  </TouchableOpacity>
                </View>
                <View style={[styles.buttonContainer, styles.sellBtnContainer]}>
                  <TouchableOpacity
                    style={[styles.button, styles.sellBtn]}
                    onPress={sellClickHandler}>
                    <Text style={styles.btnText}>Sell</Text>
                  </TouchableOpacity>
                </View>
              </View>
              {activeCrypto.asset_type === 'Fiat' && (
                <View style={styles.actionBtnContainer}>
                  <View
                    style={[
                      styles.buttonContainer,
                      styles.depositButtonContainer,
                    ]}>
                    <TouchableOpacity
                      style={[styles.button, styles.depositButton]}
                      onPress={depositClickHandler}>
                      <Text style={styles.btnText}>Deposit</Text>
                    </TouchableOpacity>
                  </View>
                  <View
                    style={[
                      styles.buttonContainer,
                      styles.withdrawButtonContainer,
                    ]}>
                    <TouchableOpacity
                      onPress={withdrawClickHandler}
                      style={[styles.button, styles.withdrawButton]}>
                      <Text style={styles.btnText}>Withdraw</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              )}
            </View>
          </>
        )}
      </LinearGradient>
    </View>
  );
};

export default CryptoDetailsCard;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  gradientBackground: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageContainer: {
    width: 40,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 4,
  },
  image: {
    height: '100%',
    width: '100%',
    borderRadius: 20,
  },
  namesContainer: {
    flexGrow: 1,
    width: 0,
    paddingHorizontal: 15,
    justifyContent: 'space-between',
  },
  name: {
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
  },
  vendors: {fontSize: 10, color: '#08152D', fontFamily: 'Montserrat-Bold'},
  pricesContainer: {},
  price: {
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'right',
  },
  upChange: {
    textAlign: 'right',
    fontSize: 10,
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
  },
  downChange: {
    textAlign: 'right',
    fontSize: 10,
    fontFamily: 'Montserrat-Bold',
    color: '#FF2D55',
  },
  candleCartContainer: {
    marginTop: 30,
    flexGrow: 1,
    height: 0,
  },
  changesContainer: {
    flexDirection: 'row',
  },
  changeItem: {
    marginTop: 30,
    padding: 12,
    flexGrow: 1,
    width: 0,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    borderRadius: 4,
    marginRight: 10,
  },
  changeValueUp: {
    fontSize: 13,
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
  },
  changeValueDown: {
    fontSize: 13,
    color: '#FF2D55',
    fontFamily: 'Montserrat-SemiBold',
  },
  changeDuration: {
    color: '#000000',
    opacity: 0.4,
    fontSize: 8,
    fontFamily: 'Montserrat-SemiBold',
  },
  column: {
    marginTop: 30,
  },
  actionBtnContainer: {
    flexDirection: 'row',
  },
  buttonContainer: {
    flexGrow: 1,
    width: 0,
    height: 45,
  },
  buyBtnContainer: {
    marginRight: 10,
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    height: '100%',
  },
  buyBtn: {
    backgroundColor: '#30BC96',
  },
  btnText: {
    color: 'white',
    fontSize: 14,
    fontFamily: 'Montserrat-Bold',
  },
  sellBtnContainer: {},
  sellBtn: {
    backgroundColor: '#D81238',
  },
  depositButtonContainer: {
    marginRight: 10,
    marginTop: 10,
  },
  depositButton: {
    backgroundColor: '#08152D',
  },
  withdrawButtonContainer: {
    marginTop: 10,
  },
  withdrawButton: {
    backgroundColor: '#000000',
  },
});
