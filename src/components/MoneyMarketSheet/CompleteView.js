import React, {useContext} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {formatterHelper} from '../../utils';
import * as WebBrowser from 'expo-web-browser';
import {AppContext} from '../../contexts/AppContextProvider';

const CompleteView = ({selectedAsset, selectedApp, cryptoInput}) => {
  const {walletBalances} = useContext(AppContext);

  const updatedBuyBalance =
    walletBalances[`${selectedAsset.coinSymbol.toLowerCase()}_balance`] || 0;

  return (
    <View style={styles.container}>
      <Image
        source={require('../../assets/money-markets-full-icon.png')}
        resizeMode="contain"
        style={styles.moneyMarketIcon}
      />
      <Text style={styles.successText}>
        Your Have Successfully Withdrew{' '}
        {formatterHelper(cryptoInput || 0, selectedAsset.coinSymbol)}{' '}
        {selectedAsset.coinName} From Your MoneyMoney Market Earnings Vault Into
        Your {selectedApp.app_name} {selectedAsset.coinName} Vault
      </Text>
      <View style={styles.valuesContainer}>
        <View style={[styles.valueItem, {marginBottom: 20}]}>
          <Text style={styles.title}>
            New {selectedApp.app_name} {selectedAsset.coinName} Balance
          </Text>
          <View style={styles.inputContainer}>
            <Text style={styles.textInput}>
              {formatterHelper(updatedBuyBalance, selectedAsset.coinSymbol)}
            </Text>
            <View style={styles.unitContainer}>
              <Text style={styles.unitValue}>{selectedAsset.coinSymbol}</Text>
              <Image
                source={{uri: selectedAsset.coinImage}}
                style={styles.unitImage}
                resizeMode="cover"
              />
            </View>
          </View>
        </View>
        <View style={styles.valueItem}>
          <Text style={styles.title}>
            {selectedApp.app_name} MoneyMarket {selectedAsset.coinName} Balance
          </Text>
          <View style={styles.inputContainer}>
            <Text style={styles.textInput}>
              {formatterHelper(0, selectedAsset.coinSymbol)}
            </Text>
            <View style={styles.unitContainer}>
              <Text style={styles.unitValue}>{selectedAsset.coinSymbol}</Text>
              <Image
                source={{uri: selectedAsset.coinImage}}
                style={styles.unitImage}
                resizeMode="cover"
              />
            </View>
          </View>
        </View>
      </View>
      <View style={styles.actionContainer}>
        <ScrollView
          style={styles.scrollView}
          horizontal
          showsHorizontalScrollIndicator={false}>
          <TouchableOpacity style={[styles.action, {marginLeft: 10}]}>
            <Image
              style={styles.actionIcon}
              source={require('../../assets/app-logo.png')}
              resizeMode="contain"
            />
            <Text style={styles.actionText}>Transaction Audit</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.action, {marginLeft: 25}]}
            onPress={() =>
              WebBrowser.openBrowserAsync('https://iceprotocol.com/')
            }>
            <Image
              style={styles.actionIcon}
              source={require('../../assets/iced-icon.png')}
              resizeMode="contain"
            />
            <Text style={[styles.actionText, styles.actionTextBlack]}>
              Invest Bitcoin
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.action, {marginLeft: 25, marginRight: 50}]}
            onPress={() =>
              WebBrowser.openBrowserAsync('https://cryptolottery.com/')
            }>
            <Image
              style={styles.actionIcon}
              source={require('../../assets/lottery-icon.png')}
              resizeMode="contain"
            />
            <Text style={[styles.actionText, styles.actionTextBlack]}>
              Play Lottery
            </Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    </View>
  );
};

export default CompleteView;

const styles = StyleSheet.create({
  container: {
    padding: 30,
  },
  moneyMarketIcon: {
    marginLeft: 'auto',
    marginRight: 'auto',
    height: 50,
  },
  successText: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat',
    fontSize: 12,
    marginTop: 30,
  },
  valuesContainer: {
    marginVertical: 30,
  },
  actionContainer: {
    flexDirection: 'row',
    marginRight: -40,
  },
  scrollView: {
    marginHorizontal: -10,
  },
  action: {
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    backgroundColor: 'white',
    borderRadius: 8,
    paddingVertical: 20,
    paddingHorizontal: 30,
    flex: 1,
    marginVertical: 10,
  },
  actionIcon: {
    width: 35,
    height: 35,
  },
  actionText: {
    marginTop: 10,
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    textAlign: 'center',
    fontSize: 12,
  },
  actionTextBlack: {
    color: '#464B4E',
  },
  valueItem: {},
  title: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  inputContainer: {
    flexDirection: 'row',
    borderWidth: 2,
    borderRadius: 8,
    borderColor: '#EBEBEB',
    height: 50,
    alignItems: 'center',
  },
  textInput: {
    flexGrow: 1,
    width: 0,
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    paddingHorizontal: 20,
    color: '#9A9A9A',
  },
  unitContainer: {
    width: '45%',
    flexDirection: 'row',
    alignItems: 'center',
    borderLeftColor: '#EBEBEB',
    borderLeftWidth: 2,
    justifyContent: 'center',
  },
  unitValue: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
  },
  unitImage: {
    height: 24,
    width: 24,
    borderRadius: 12,
    marginLeft: 15,
  },
});
