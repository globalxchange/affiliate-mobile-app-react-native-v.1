import Axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import {GX_API_ENDPOINT} from '../../configs';
import {AppContext} from '../../contexts/AppContextProvider';
import BottomSheetLayout from '../../layouts/BottomSheetLayout';
import {formatterHelper, roundHelper} from '../../utils';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import LoadingAnimation from '../LoadingAnimation';
import CompleteView from './CompleteView';

const MoneyMarketSheet = ({isOpen, setIsOpen, selectedAsset, selectedApp}) => {
  const {updateWalletBalances} = useContext(AppContext);

  const [isCryptoFocused, setIsCryptoFocused] = useState(true);
  const [cryptoInput, setCryptoInput] = useState('');
  const [fiatInput, setFiatInput] = useState('');
  const [convertValue, setConvertValue] = useState(1);
  const [isCompleted, setIsCompleted] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    Axios.get(`${GX_API_ENDPOINT}/forex/convert`, {
      params: {buy: selectedAsset.coinSymbol, from: 'USD'},
    })
      .then((resp) => {
        const {data} = resp;

        const conversionRate =
          data[`usd_${selectedAsset.coinSymbol.toLowerCase()}`] || 1;

        // console.log('conversionRate', conversionRate);

        setConvertValue(conversionRate);
      })
      .catch((error) => {
        setConvertValue(1);
        console.log('Conversion Rate error', error);
      });
  }, [selectedAsset]);

  useEffect(() => {
    if (!isOpen) {
      setCryptoInput('');
      setFiatInput('');
      setIsCryptoFocused(false);
    }
  }, [isOpen]);

  const onCryptoEditing = (value) => {
    setCryptoInput(value);

    const parsedValue = parseFloat(value);

    if (parsedValue) {
      const convertedValue = roundHelper(parsedValue / convertValue, 'USD');

      setFiatInput(`${convertedValue}`);
    }
  };

  const onFiatEditing = (value) => {
    setFiatInput(value);

    const parsedValue = parseFloat(value);

    if (parsedValue) {
      const convertedValue = roundHelper(
        parsedValue * convertValue,
        selectedAsset.coinSymbol,
      );

      setCryptoInput(`${convertedValue}`);
    }
  };

  const withdrawFund = async () => {
    const parsedAmount = parseFloat(cryptoInput);

    if (!parsedAmount) {
      return WToast.show({
        data: 'Please Input A Valid Value',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    const postData = {
      email,
      token,
      app_code: selectedApp.app_code,
      profile_id: selectedApp.profile_id,
      coin: selectedAsset.coinSymbol,
      amount: parsedAmount,
    };

    // console.log('postData', postData);

    Axios.post(`${GX_API_ENDPOINT}/coin/iced/interest/withdraw`, postData)
      .then((resp) => {
        const {data} = resp;

        // console.log('Withdraw Resp', data);

        if (data.status) {
          updateWalletBalances();
          setIsCompleted(true);
        } else {
          WToast.show({
            data: data.message || 'Error on withdrawing',
            position: WToast.position.TOP,
          });
        }
      })
      .catch((error) => {
        WToast.show({
          data: 'Error on withdrawing',
          position: WToast.position.TOP,
        });
        console.log('Error on withdrawing', error);
      })
      .finally(() => setIsLoading(false));
  };

  return (
    <BottomSheetLayout
      isOpen={isOpen}
      onClose={() => setIsOpen(false)}
      reactToKeyboard>
      {isCompleted ? (
        <CompleteView
          selectedApp={selectedApp}
          selectedAsset={selectedAsset}
          cryptoInput={cryptoInput}
        />
      ) : (
        <>
          <Image
            source={require('../../assets/money-markets-full-icon.png')}
            resizeMode="contain"
            style={styles.moneyMarketIcon}
          />
          {isLoading ? (
            <View style={{paddingVertical: 70}}>
              <LoadingAnimation />
            </View>
          ) : (
            <View style={styles.fragmentContainer}>
              <Text style={styles.balanceHeader}>Withdrawable Balance Is</Text>
              <Text style={styles.balance}>0.0015 BTC</Text>
              <View style={styles.inputForm}>
                <View style={styles.inputContainer}>
                  <TextInput
                    style={[
                      styles.input,
                      isCryptoFocused && styles.focusedText,
                    ]}
                    placeholder={formatterHelper(
                      '0.00',
                      selectedAsset.coinSymbol,
                    )}
                    onChangeText={(text) => onCryptoEditing(text)}
                    keyboardType="numeric"
                    returnKeyType="done"
                    value={cryptoInput}
                    onFocus={() => setIsCryptoFocused(true)}
                    placeholderTextColor="#878788"
                  />
                  <Text
                    style={[
                      styles.cryptoName,
                      isCryptoFocused && styles.focusedText,
                    ]}>
                    {selectedAsset.coinSymbol}
                  </Text>
                </View>
                <View style={styles.divider} />
                <View style={styles.inputContainer}>
                  <TextInput
                    style={[
                      styles.input,
                      isCryptoFocused || styles.focusedText,
                    ]}
                    placeholder={formatterHelper('0.00', 'USD')}
                    onChangeText={(text) => onFiatEditing(text)}
                    value={fiatInput}
                    onFocus={() => setIsCryptoFocused(false)}
                    keyboardType="numeric"
                    returnKeyType="done"
                    placeholderTextColor="#878788"
                  />
                  <Text
                    style={[
                      styles.cryptoName,
                      isCryptoFocused || styles.focusedText,
                    ]}>
                    USD
                  </Text>
                </View>
              </View>
            </View>
          )}
        </>
      )}
      {isLoading || (
        <TouchableOpacity
          onPress={() => (isCompleted ? setIsOpen(false) : withdrawFund())}
          style={styles.closeButton}>
          <Text style={styles.closeButtonText}>
            {isCompleted ? 'Close' : 'Withdraw'}
          </Text>
        </TouchableOpacity>
      )}
    </BottomSheetLayout>
  );
};

export default MoneyMarketSheet;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    // flex: 1,
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    overflow: 'hidden',
    paddingTop: 30,
  },
  moneyMarketIcon: {
    marginLeft: 'auto',
    marginRight: 'auto',
    height: 50,
  },
  fragmentContainer: {
    // flex: 1,
    paddingHorizontal: 30,
    marginBottom: 30,
    marginTop: 30,
  },
  headerImage: {
    height: 35,
    width: 170,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginVertical: 20,
  },
  viewContainer: {
    borderTopColor: '#EBEBEB',
    borderTopWidth: 1,
  },
  loadingContainer: {
    paddingVertical: 70,
  },
  balanceHeader: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat',
  },
  balance: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
  },
  closeButton: {
    backgroundColor: '#08152D',
    alignItems: 'center',
    justifyContent: 'center',
    height: 55,
  },
  closeButtonText: {
    color: 'white',
    justifyContent: 'center',
    fontFamily: 'Montserrat-Bold',
  },
  inputForm: {marginTop: 20},
  inputContainer: {flexDirection: 'row', alignItems: 'center'},
  input: {
    flexGrow: 1,
    width: 0,
    color: '#041939',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    paddingHorizontal: 10,
    height: 45,
  },
  focusedInput: {
    color: '#08152D',
  },
  inputButton: {
    borderWidth: 1,
    borderColor: '#EDEDED',
    justifyContent: 'center',
    paddingHorizontal: 8,
    paddingVertical: 2,
    marginLeft: 5,
  },
  inputButtonText: {
    color: '#B4BBC4',
    fontFamily: 'Montserrat',
    fontSize: 10,
  },
  cryptoName: {
    marginLeft: 10,
    color: '#041939',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  focusedText: {
    color: '#08152D',
  },
  divider: {
    backgroundColor: '#08152D',
    height: 1,
    marginVertical: 5,
  },
});
