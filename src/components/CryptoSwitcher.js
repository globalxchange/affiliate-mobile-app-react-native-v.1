/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useMemo} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  ScrollView,
} from 'react-native';
import {AppContext} from '../contexts/AppContextProvider';
import FastImage from 'react-native-fast-image';
import {formatterHelper, getUriImage} from '../utils';
import ThemeData from '../configs/ThemeData';

const ITEM_WIDTH = 80;

const CryptoSwitcher = ({
  activeCrypto,
  setActiveCrypto,
  isCoinSearchOpen,
  setIsCoinSearchOpen,
  walletBalances,
  isActionBlurEnabled,
}) => {
  const {cryptoTableData} = useContext(AppContext);

  const onItemClicked = (item) => {
    setActiveCrypto(item);
  };

  const filteredList = useMemo(
    () =>
      cryptoTableData?.filter(
        (item) =>
          activeCrypto?.coinName !== item.coinName &&
          activeCrypto?.type === item.type,
      ) || [],
    [cryptoTableData, activeCrypto],
  );

  const activeBalance = useMemo(() => {
    if (!walletBalances) {
      return '';
    }

    const balanceObj = walletBalances.find(
      (x) => x.coinSymbol === activeCrypto?.coinSymbol,
    );

    return `${formatterHelper(
      balanceObj?.coinValue || 0,
      activeCrypto?.coinSymbol,
    )}`;
  }, [walletBalances, activeCrypto]);

  return (
    <View style={[styles.container, {opacity: isActionBlurEnabled ? 0.3 : 1}]}>
      <View style={styles.itemContainer}>
        <View style={[styles.item, {marginRight: 0}]}>
          <View style={[styles.iconContainer]}>
            <FastImage
              style={[styles.icon, {width: 40, height: 40}]}
              source={{
                uri: getUriImage(activeCrypto?.coinImage),
              }}
              resizeMode="contain"
            />
          </View>
          <Text style={[styles.titleActive]}>
            {activeCrypto?.coinName.length < 8
              ? activeCrypto?.coinName
              : activeCrypto?.coinSymbol}
          </Text>
          <Text style={styles.coinBalance}>{activeBalance}</Text>
        </View>
      </View>
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        <TouchableWithoutFeedback
          style={[styles.itemContainer]}
          onPress={() => setIsCoinSearchOpen(true)}
          disabled={isActionBlurEnabled}>
          <View style={[styles.item, {marginRight: 0}]}>
            <View
              style={[
                styles.iconContainer,
                {
                  opacity: 1,
                  borderColor: ThemeData.BORDER_COLOR,
                  borderWidth: 1,
                  borderRadius: 90,
                },
              ]}>
              <FastImage
                style={{height: 18, width: 18}}
                source={require('../assets/search-modern.png')}
                resizeMode="contain"
              />
            </View>
            <Text style={[styles.title]}>Search</Text>
          </View>
        </TouchableWithoutFeedback>
        {filteredList?.map((item) => (
          <TouchableWithoutFeedback
            key={item.coinName}
            disabled={isActionBlurEnabled}
            style={styles.itemContainer}
            onPress={() => onItemClicked(item)}>
            <View style={styles.item}>
              <View style={[styles.iconContainer]}>
                <FastImage
                  style={styles.icon}
                  source={{
                    uri: getUriImage(item.coinImage),
                  }}
                  resizeMode="cover"
                />
              </View>
              <Text style={styles.title}>
                {item.coinName.length < 8 ? item.coinName : item.coinSymbol}
              </Text>
            </View>
          </TouchableWithoutFeedback>
        ))}
      </ScrollView>
    </View>
  );
};

export default CryptoSwitcher;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    alignItems: 'flex-end',
  },
  itemContainer: {paddingHorizontal: 5},
  item: {
    paddingVertical: 15,
    marginRight: 10,
    paddingHorizontal: 10,
    width: ITEM_WIDTH,
  },
  iconContainer: {
    height: 50,
    width: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 'auto',
    marginRight: 'auto',
    borderRadius: 2,
  },
  itemActive: {
    height: 50,
    width: 50,
    opacity: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 'auto',
    marginRight: 'auto',
    borderRadius: 2,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.15,
    shadowRadius: 2,
    backgroundColor: 'white',
    elevation: 2,
  },
  icon: {height: 30, width: 30, borderRadius: 15},
  title: {
    textAlign: 'center',
    color: ThemeData.TEXT_COLOR,
    marginTop: 5,
    fontSize: 10,
    fontFamily: 'Montserrat',
  },
  titleActive: {
    textAlign: 'center',
    color: ThemeData.TEXT_COLOR,
    marginTop: 5,
    fontSize: 11,
    fontFamily: ThemeData.FONT_BOLD,
  },
  coinBalance: {
    textAlign: 'center',
    color: ThemeData.TEXT_COLOR,
    fontSize: 9,
  },
});
