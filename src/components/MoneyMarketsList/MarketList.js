import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {GX_API_ENDPOINT} from '../../configs';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import Axios from 'axios';
import FastImage from 'react-native-fast-image';
import LoadingAnimation from '../LoadingAnimation';
import {formatterHelper, getUriImage} from '../../utils';
import AssetLogs from './AssetLogs';

const MarketList = ({selectedApp, selectedAsset, setSelectedAsset}) => {
  const [moneyMarketData, setMoneyMarketData] = useState();

  useEffect(() => {
    (async () => {
      const email = await AsyncStorageHelper.getLoginEmail();

      Axios.get(
        `${GX_API_ENDPOINT}/coin/vault/service/user/app/interest/balances/get`,
        {params: {email, app_code: selectedApp.app_code}},
      )
        .then((resp) => {
          const {data} = resp;

          // console.log('Money Market Data', data);

          if (data.status) {
            const result =
              data?.result[0]?.balances[0]?.liquid_balances || null;

            setMoneyMarketData(result);
          }
        })
        .catch((error) => {
          console.log('Error on getting money market data', error);
        });
    })();
  }, [selectedApp]);

  return (
    <>
      {selectedAsset ? (
        <AssetLogs selectedAsset={selectedAsset} selectedApp={selectedApp} />
      ) : moneyMarketData ? (
        <FlatList
          numColumns={2}
          style={styles.list}
          showsVerticalScrollIndicator={false}
          data={moneyMarketData}
          keyExtractor={(item, index) => `${item._id || index}`}
          renderItem={({item, index}) => (
            <TouchableOpacity
              onPress={() => setSelectedAsset(item)}
              style={[
                styles.itemContainer,
                index % 2 === 1 && {marginLeft: 20},
              ]}>
              <FastImage
                resizeMode="contain"
                style={styles.coinImage}
                source={{uri: getUriImage(item.coinImage)}}
              />
              <Text style={styles.coinValue}>
                {formatterHelper(item.coinValue, item.coinSymbol)}{' '}
                {item.coinSymbol}
              </Text>
            </TouchableOpacity>
          )}
        />
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      )}
    </>
  );
};

export default MarketList;

const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  list: {
    paddingHorizontal: 50,
    marginTop: 30,
  },
  itemContainer: {
    flex: 1,
    borderColor: '#EBEBEB',
    borderWidth: 1,
    alignItems: 'center',
    paddingVertical: 20,
    borderRadius: 6,
    marginBottom: 20,
  },
  coinValue: {
    marginTop: 10,
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
  },
  coinImage: {
    width: 40,
    height: 40,
  },
});
