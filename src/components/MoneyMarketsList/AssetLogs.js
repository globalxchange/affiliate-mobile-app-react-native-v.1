import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {GX_API_ENDPOINT} from '../../configs';
import {formatterHelper, usdValueFormatter} from '../../utils';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import LoadingAnimation from '../LoadingAnimation';
import MoneyMarketSheet from '../MoneyMarketSheet';
import Moment from 'moment-timezone';

const AssetLogs = ({selectedAsset, selectedApp}) => {
  const [isSheetOpen, setIsSheetOpen] = useState(false);
  const [logs, setLogs] = useState();

  useEffect(() => {
    (async () => {
      const email = await AsyncStorageHelper.getLoginEmail();
      Axios.get(
        `${GX_API_ENDPOINT}/coin/vault/service/user/app/interest/logs/get`,
        {
          params: {
            email,
            app_code: selectedApp.app_code,
            coin: selectedAsset.coinSymbol,
          },
        },
      )
        .then((resp) => {
          const {data} = resp;

          // console.log('Logs Data', data);

          setLogs(data?.logs[0]?.logs);
        })
        .catch((error) => {
          console.log('Error on getting asset logs', error);
        });
    })();
  }, [selectedAsset, selectedApp]);

  return (
    <>
      {logs ? (
        <FlatList
          style={styles.list}
          showsVerticalScrollIndicator={false}
          data={logs}
          keyExtractor={(item, index) => `${item._id || index}`}
          renderItem={({item, index}) => (
            <View style={[styles.itemContainer]}>
              <View style={styles.detailsContainer}>
                <Text style={styles.txnName}>Interest Payment</Text>
                <Text style={styles.coinValue}>
                  {formatterHelper(
                    item.total_interest || item.credited_interest,
                    item.coin,
                  )}
                </Text>
              </View>
              <View style={styles.detailsContainer}>
                <Text style={styles.txnDate}>
                  {Moment.unix(item.timestamp / 1000).format('MMMM Do YYYY')}
                </Text>
                <Text style={styles.txnUsdValue}>
                  {usdValueFormatter.format(
                    item.total_interest_usd || item.credited_usd_value,
                  )}{' '}
                  USD
                </Text>
              </View>
            </View>
          )}
        />
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      )}
      <TouchableOpacity
        onPress={() => setIsSheetOpen(true)}
        style={styles.withdrawButton}>
        <Text style={styles.withdrawText}>Withdraw Earnings</Text>
      </TouchableOpacity>
      <MoneyMarketSheet
        selectedAsset={selectedAsset}
        selectedApp={selectedApp}
        isOpen={isSheetOpen}
        setIsOpen={setIsSheetOpen}
      />
    </>
  );
};

export default AssetLogs;

const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  list: {
    paddingHorizontal: 30,
    marginTop: 30,
    flex: 1,
  },
  itemContainer: {
    flex: 1,
    borderColor: '#EBEBEB',
    borderWidth: 1,
    paddingVertical: 20,
    borderRadius: 6,
    marginBottom: 20,
    paddingHorizontal: 25,
  },
  detailsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  txnName: {
    color: '#788995',
    fontFamily: 'Montserrat-SemiBold',
    marginBottom: 5,
  },
  coinValue: {
    color: '#30BC96',
    fontFamily: 'Montserrat-Bold',
    fontSize: 16,
    marginBottom: 5,
  },
  txnDate: {
    color: '#788995',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 11,
  },
  txnUsdValue: {
    color: '#788995',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 11,
  },
  withdrawButton: {
    backgroundColor: '#08152D',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  withdrawText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
  },
});
