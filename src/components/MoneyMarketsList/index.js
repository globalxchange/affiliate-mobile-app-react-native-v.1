import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {GX_API_ENDPOINT} from '../../configs';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import LoadingAnimation from '../LoadingAnimation';
import MarketList from './MarketList';

const MoneyMarketsList = () => {
  const [appLists, setAppLists] = useState();
  const [searchAppList, setSearchAppList] = useState();
  const [searchText, setSearchText] = useState('');
  const [selectedApp, setSelectedApp] = useState();
  const [selectedAsset, setSelectedAsset] = useState();

  useEffect(() => {
    (async () => {
      const email = await AsyncStorageHelper.getLoginEmail();

      Axios.get(`${GX_API_ENDPOINT}/gxb/apps/registered/user`, {
        params: {email},
      })
        .then((resp) => {
          const {data} = resp;

          // console.log('AppsLis', data);
          setAppLists(data?.userApps || []);
        })
        .catch((error) => {
          console.log('Error on getting Apps', error);
        });
    })();
  }, []);

  useEffect(() => {
    if (appLists) {
      const searchQuery = searchText.trim().toLowerCase();

      setSearchAppList(
        appLists.filter(
          (item) =>
            item?.app_name?.toLowerCase().includes(searchQuery) ||
            item?.app_code?.toLowerCase().includes(searchQuery),
        ),
      );
    }
  }, [appLists, searchText]);

  const onAppSelected = (app) => {
    setSearchText('');
    setSelectedApp(app);
    setSelectedAsset();
  };

  return (
    <View style={styles.container}>
      {selectedApp ? (
        <View style={styles.selectedContainer}>
          <TouchableOpacity
            onPress={() => {
              setSelectedApp();
              setSelectedAsset();
            }}
            style={styles.selectedApp}>
            <Image
              resizeMode="contain"
              style={styles.selectedAppImage}
              source={{uri: selectedApp.app_icon}}
            />
            <Text style={styles.selectedAppName}>{selectedApp.app_name}</Text>
          </TouchableOpacity>
          {selectedAsset && (
            <TouchableOpacity
              onPress={() => setSelectedAsset()}
              style={styles.selectedCoin}>
              <Image
                resizeMode="contain"
                style={styles.selectedCoinImage}
                source={{uri: selectedAsset.coinImage}}
              />
              <Text style={styles.selectedCoinName}>
                {selectedAsset.coinName.length > 10
                  ? selectedAsset.coinSymbol
                  : selectedAsset.coinName}
              </Text>
            </TouchableOpacity>
          )}
        </View>
      ) : (
        <TextInput
          style={styles.searchInput}
          placeholder="Enter Name Of App"
          returnKeyType="done"
          placeholderTextColor="#CACACA"
          value={searchText}
          onChangeText={(text) => setSearchText(text)}
        />
      )}
      {selectedApp ? (
        <MarketList
          selectedApp={selectedApp}
          selectedAsset={selectedAsset}
          setSelectedAsset={setSelectedAsset}
        />
      ) : appLists ? (
        <FlatList
          numColumns={2}
          style={styles.list}
          showsVerticalScrollIndicator={false}
          data={searchAppList}
          keyExtractor={(item, index) => `${item._id || index}`}
          renderItem={({item, index}) => (
            <TouchableOpacity
              onPress={() => onAppSelected(item)}
              style={[
                styles.itemContainer,
                index % 2 === 1 && {marginLeft: 20},
              ]}>
              <Image
                resizeMode="contain"
                style={styles.coinImage}
                source={{uri: item.app_icon}}
              />
              <Text style={styles.coinValue}>{item.app_name}</Text>
            </TouchableOpacity>
          )}
          ListEmptyComponent={
            <View style={styles.emptyContainer}>
              <Text style={styles.emptyText}>No Apps Found</Text>
            </View>
          }
        />
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default MoneyMarketsList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  searchInput: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    marginHorizontal: 30,
    marginTop: 30,
    paddingHorizontal: 20,
    height: 50,
    color: 'black',
    fontFamily: 'Montserrat',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  list: {
    paddingHorizontal: 50,
    marginTop: 30,
  },
  itemContainer: {
    flex: 1,
    borderColor: '#EBEBEB',
    borderWidth: 1,
    alignItems: 'center',
    paddingVertical: 20,
    borderRadius: 6,
    marginBottom: 20,
  },
  coinValue: {
    marginTop: 10,
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
  },
  coinImage: {
    width: 50,
    height: 50,
  },
  emptyContainer: {
    marginTop: 20,
  },
  emptyText: {
    textAlign: 'center',
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    fontSize: 20,
  },
  selectedApp: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 30,
  },
  selectedAppName: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    textAlign: 'center',
    fontSize: 13,
  },
  selectedAppImage: {
    width: 23,
    height: 23,
    marginRight: 10,
  },
  selectedContainer: {
    flexDirection: 'row',
    marginTop: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  selectedCoinImage: {
    width: 20,
    height: 20,
    marginRight: 10,
  },
  selectedCoinName: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    textAlign: 'center',
    fontSize: 12,
  },
  selectedCoin: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 25,
    marginLeft: 10,
  },
});
