import React, {useContext, useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';
import {AppContext} from '../contexts/AppContextProvider';
import {
  usdValueFormatter,
  usdValueFormatterWithoutSign,
  cryptoFormatter,
} from '../utils';
import Axios from 'axios';
import {GX_API_ENDPOINT, APP_CODE} from '../configs';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';
import Dropdown from './Dropdown';
import SkeltonItem from './SkeltonItem';

const TransactionList = ({activeWallet, openTxnAudit}) => {
  const {walletShowInUsd} = useContext(AppContext);

  const [transactionsList, setTransactionsList] = useState();
  const [filteredTxnList, setFilteredTxnList] = useState();
  const [isLoading, setIsLoading] = useState(true);
  const [isTypeFilterOpen, setIsTypeFilterOpen] = useState(false);
  const [typeFilter, setTypeFilter] = useState(typeFilterItems[0]);
  const [isStatusFilterOpen, setIsStatusFilterOpen] = useState(false);
  const [statusFilter, setStatusFilter] = useState(statusFilterItems[0]);

  const getTransactions = async () => {
    setIsLoading(true);

    const profileId = await AsyncStorageHelper.getProfileId();

    const postData = {
      app_code: APP_CODE,
      profile_id: profileId,
      coin: activeWallet.coinSymbol,
    };

    Axios.post(`${GX_API_ENDPOINT}/coin/vault/service/txns/get`, postData)
      .then((resp) => {
        const {data} = resp;
        if (data.status) {
          setTransactionsList(data.txns);
        }
        setIsLoading(false);
        // console.log('TXN List', data.txns);
      })
      .catch((error) => console.log('Error getting txn list', error));
  };

  useEffect(() => {
    getTransactions();
  }, [activeWallet]);

  useEffect(() => {
    if (typeFilter === 'All') {
      setStatusFilter(statusFilterItems[0]);
    }
  }, [typeFilter]);

  useEffect(() => {
    setTypeFilter(typeFilterItems[0]);
    setStatusFilter(statusFilterItems[0]);
  }, [activeWallet]);

  useEffect(() => {
    if (transactionsList) {
      if (typeFilter === 'All') {
        setFilteredTxnList(transactionsList);
      } else {
        const newList = [];

        let type = statusFilter.toLowerCase();

        transactionsList.forEach((item) => {
          const isDeposit = typeFilter === 'Deposits';

          const currentStatus =
            type === 'all' ? item.status.toLowerCase() : type;

          if (
            item.deposit === isDeposit &&
            item.status.toLowerCase() === currentStatus
          ) {
            newList.push(item);
          }
        });

        setFilteredTxnList(newList);
      }
    }
  }, [typeFilter, statusFilter, transactionsList]);

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.header}>Transactions</Text>
        <View style={styles.filterContainer}>
          <Dropdown
            items={typeFilterItems}
            activeItem={typeFilter}
            setActiveItem={setTypeFilter}
            onChange={setIsTypeFilterOpen}
            closeOnChangeValue={isStatusFilterOpen}
          />
          <Dropdown
            isDisabled={typeFilter === 'All'}
            items={statusFilterItems}
            activeItem={statusFilter}
            setActiveItem={setStatusFilter}
            onChange={setIsStatusFilterOpen}
            closeOnChangeValue={isTypeFilterOpen}
            style={{width: 100, marginLeft: 10}}
          />
        </View>
      </View>
      <View style={styles.mask}>
        {!isLoading ? (
          <FlatList
            style={styles.list}
            showsVerticalScrollIndicator={false}
            data={filteredTxnList}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item}) => (
              <TouchableOpacity
                style={[styles.txnItem]}
                onPress={() => openTxnAudit(item)}>
                <Image
                  style={styles.txnIcons}
                  source={
                    item.deposit
                      ? require('../assets/deposit-txn-icon.png')
                      : require('../assets/withdraw-txn-icon.png')
                  }
                  resizeMode="contain"
                />
                <View style={styles.nameContainer}>
                  <Text numberOfLines={1} style={styles.name}>
                    {item._id}
                  </Text>
                  <Text style={styles.value}>
                    {walletShowInUsd
                      ? usdValueFormatter.format(
                          item.amount_usd || item.usd_value,
                        )
                      : activeWallet.coinSymbol === 'USDT'
                      ? usdValueFormatterWithoutSign.format(item.amount)
                      : cryptoFormatter(item.amount)}
                    {!walletShowInUsd &&
                      ` ${activeWallet && activeWallet.coinSymbol}`}
                  </Text>
                </View>
                <View style={styles.metaContainer}>
                  <Text style={styles.date}>{item.date}</Text>
                  <Text
                    style={[
                      styles.type,
                      item.deposit ? {color: '#30BC96'} : {color: '#D80027'},
                    ]}>
                    {item.deposit ? 'Deposit' : 'Withdrawal'}
                  </Text>
                </View>
              </TouchableOpacity>
            )}
            ListEmptyComponent={
              <View style={styles.emptyContainer}>
                <Text style={styles.emptyText}>
                  {`No${statusFilter !== 'All' ? ` ${statusFilter}` : ''}${
                    typeFilter !== 'All' ? ` ${typeFilter}` : ''
                  } ${activeWallet.coinSymbol} Transactions Found`}
                </Text>
              </View>
            }
          />
        ) : (
          <View style={styles.loadingContainer}>
            <SkeltonAnimation />
            <SkeltonAnimation />
            <SkeltonAnimation />
            <SkeltonAnimation />
            <SkeltonAnimation />
          </View>
        )}
      </View>
    </View>
  );
};

const SkeltonAnimation = () => {
  return (
    <View style={styles.txnItem}>
      <SkeltonItem itemWidth={30} style={{borderRadius: 15}} />
      <View style={styles.nameContainer}>
        <SkeltonItem itemWidth={100} itemHeight={10} style={styles.name} />
        <SkeltonItem itemWidth={70} itemHeight={8} style={styles.value} />
      </View>
      <View style={styles.metaContainer}>
        <SkeltonItem itemWidth={80} itemHeight={8} style={styles.date} />
        <SkeltonItem
          itemWidth={40}
          itemHeight={8}
          style={{marginLeft: 'auto', marginTop: 5}}
        />
      </View>
    </View>
  );
};

export default TransactionList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 20,
  },
  headerContainer: {
    paddingHorizontal: 25,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 15,
  },
  header: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
  },
  filterContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  mask: {backgroundColor: '#f7f7f7', flex: 1},
  list: {flex: 1},
  txnItem: {
    flexDirection: 'row',
    paddingHorizontal: 35,
    paddingVertical: 25,
    backgroundColor: 'white',
    marginBottom: 10,
    alignItems: 'center',
  },
  txnIcons: {
    width: 30,
    height: 30,
  },
  nameContainer: {flexGrow: 1, width: 0, paddingHorizontal: 15},
  name: {fontFamily: 'Montserrat-Bold', color: '#001D41'},
  value: {
    fontFamily: 'Montserrat-Bold',
    color: '#001D41',
    opacity: 0.5,
    fontSize: 12,
    marginTop: 5,
  },
  metaContainer: {},
  date: {textAlign: 'right', fontFamily: 'Montserrat', fontSize: 10},
  type: {
    textAlign: 'right',
    fontFamily: 'Montserrat-Bold',
    fontSize: 12,
    marginTop: 5,
  },
  loadingContainer: {
    flex: 1,
    // backgroundColor: '#fff',
  },
  loadingText: {marginTop: 10, fontFamily: 'Montserrat'},
  emptyContainer: {
    flex: 1,
    paddingHorizontal: 35,
    marginTop: 40,
  },
  emptyText: {
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
    textAlign: 'center',
    fontSize: 16,
  },
});

const typeFilterItems = ['All', 'Deposits', 'Withdrawals'];

const statusFilterItems = [
  'All',
  'Pending',
  'Processing',
  'Processed',
  'Terminated',
];
