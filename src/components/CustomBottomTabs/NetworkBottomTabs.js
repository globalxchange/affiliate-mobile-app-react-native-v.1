/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useEffect} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {AppContext} from '../../contexts/AppContextProvider';
import ExploreIcon from '../../assets/bottom-bar-icon/ExploreIcon';
import TeamsIcon from '../../assets/bottom-bar-icon/TeamsIcon';
import NetworkIcon from '../../assets/bottom-bar-icon/network-icon';
import ProfileIcon from '../../assets/bottom-bar-icon/profile-icon';

const NetworkBottomTabs = ({state, descriptors, navigation}) => {
  const {isVideoFullScreen, setActiveRoute} = useContext(AppContext);

  const {bottom} = useSafeAreaInsets();

  useEffect(() => {
    navigation.addListener('focus', onScreenFocus);

    return () => {
      navigation.removeListener('focus', onScreenFocus);
    };
  }, [navigation]);

  const onScreenFocus = () => {
    setActiveRoute('Network');
  };

  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  const bottomMenus = [
    {
      onPress: () => navigation.navigate('Explore'),
      tabBarLabel: 'Explore',
      tabBarIcon: ({color, size, opacity}) => (
        <ExploreIcon
          color={color}
          width={size}
          height={size}
          opacity={opacity}
        />
      ),
    },
    {
      onPress: () => navigation.navigate('Teams'),
      tabBarLabel: 'Teams',
      tabBarIcon: ({color, size, opacity}) => (
        <TeamsIcon color={color} width={size} height={size} opacity={opacity} />
      ),
    },
    {
      onPress: () =>
        navigation.navigate('Network', {
          screen: 'Chain',
          params: {screen: 'root', params: {screen: 'Levels'}},
        }),
      tabBarLabel: 'Chain',
      tabBarIcon: ({color, size, opacity}) => (
        <NetworkIcon
          color={color}
          width={size}
          height={size}
          opacity={opacity}
        />
      ),
    },
    {
      onPress: () => navigation.navigate('Profile'),
      tabBarLabel: 'Profile',
      tabBarIcon: ({color, size, opacity}) => (
        <ProfileIcon
          color={color}
          width={size}
          height={size}
          opacity={opacity}
        />
      ),
    },
  ];

  return (
    <View
      style={[
        styles.container,
        {
          paddingBottom: bottom || 15,
          display: isVideoFullScreen ? 'none' : 'flex',
        },
      ]}>
      {bottomMenus.map((route, index) => {
        const label = route.tabBarLabel;

        if (label === 'CreateBrokerage') {
          return;
        }

        const Icon = route.tabBarIcon;

        const isFocused = state.index === index;

        const size = 25;

        const color = isFocused ? '#08152D' : '#424141';

        const opacity = isFocused ? 1 : 0.3;

        const fontSize = isFocused ? 10 : 9;

        return [
          <TouchableOpacity
            key={index}
            onPress={route.onPress}
            style={styles.button}>
            {Icon && <Icon color={color} size={size} opacity={opacity} />}
            <Text
              style={[
                styles.label,
                {
                  color,
                  opacity,
                  fontSize,
                  display: isFocused ? 'flex' : 'none',
                },
              ]}>
              {label}
            </Text>
          </TouchableOpacity>,
          index === 1 && (
            <View style={styles.brokerButtonContainer} key="blockButton">
              <TouchableOpacity
                style={styles.brokerButton}
                onPress={() => navigation.navigate('NetworkCenter')}>
                <Image
                  source={require('../../assets/network-controller-icon.png')}
                  resizeMode="contain"
                  style={styles.brokerIcon}
                />
              </TouchableOpacity>
            </View>
          ),
        ];
      })}
    </View>
  );
};

export default NetworkBottomTabs;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderTopColor: '#EBEBEB',
    borderTopWidth: 1,
    paddingVertical: 12,
    paddingHorizontal: 10,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  button: {
    flex: 1,
    alignItems: 'center',
  },
  icon: {
    height: 28,
    width: 28,
  },
  label: {
    fontFamily: 'Montserrat-SemiBold',
    marginTop: 5,
    textAlign: 'center',
  },
  brokerButtonContainer: {
    marginBottom: -19,
    marginTop: -20,
    marginVertical: 10,
  },
  brokerButton: {
    backgroundColor: '#08152D',
    width: 75,
    height: 75,
    borderRadius: 37.5,
    padding: 19,
  },
  brokerIcon: {
    flex: 1,
    height: null,
    width: null,
  },
});
