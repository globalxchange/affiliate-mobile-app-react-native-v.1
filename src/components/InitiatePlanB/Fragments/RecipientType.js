import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import ActionButton from '../ActionButton';

const RecipientType = ({setIsMailSelected, onNext}) => {
  const onProceed = () => {
    WToast.show({
      data: 'Please Click On Any Type',
      position: WToast.position.TOP,
    });
  };

  const onItemSelected = (item) => {
    item.name === 'Email' ? setIsMailSelected(true) : setIsMailSelected(false);
    onNext();
  };

  return (
    <View style={styles.container}>
      <View style={styles.controlContainer}>
        <Text style={styles.header}>Recipient Type</Text>
        <Text style={styles.subHeader}>
          How Do You Want To Notify Your Recipient?
        </Text>
        <View style={styles.typesContainer}>
          {types.map((item) => (
            <TouchableOpacity
              disabled={item.disabled}
              onPress={() => onItemSelected(item)}
              key={item.name}
              style={styles.typeItem}>
              <View
                style={[
                  styles.typeIconContainer,
                  item.disabled && {opacity: 0.4},
                ]}>
                <Image
                  resizeMode="contain"
                  style={styles.typeIcon}
                  source={item.image}
                />
              </View>
              <Text style={[styles.typeName, item.disabled && {opacity: 0.4}]}>
                {item.name}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
      </View>
      <ActionButton text="Proceed" onPress={onProceed} />
    </View>
  );
};

export default RecipientType;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 40,
    paddingVertical: 20,
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 20,
    fontSize: 30,
    fontFamily: 'Montserrat-Bold',
  },
  controlContainer: {
    justifyContent: 'center',
    marginBottom: 40,
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  typesContainer: {
    flexDirection: 'row',
    marginTop: 30,
  },
  typeItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  typeIconContainer: {
    borderColor: '#F1F4F6',
    borderWidth: 1,
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    height: 60,
    width: 60,
  },
  typeIcon: {
    flex: 1,
  },
  typeName: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    marginTop: 8,
  },
});

const types = [
  {name: 'Email', image: require('../../../assets/gmail-icon.png')},
  {name: 'Text', image: require('../../../assets/phone-text-icon.png')},
];
