import Axios from 'axios';
import React, {useContext, useEffect, useRef, useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {WModalShowToastView, WToast} from 'react-native-smart-tip';
import {GX_API_ENDPOINT} from '../../../configs';
import {AppContext} from '../../../contexts/AppContextProvider';
import {formatterHelper, roundHelper} from '../../../utils';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import QuoteInput from '../../Connect/QuoteInput';
import LoadingAnimation from '../../LoadingAnimation';

const QuoteFrom = ({
  sendingCurrency,
  recipientCurrency,
  name,
  address,
  recipient,
  recipientPhone,
  isMail,
  onNext,
  senderAppCode,
  senderProfileId,
  message,
  expiryDays,
}) => {
  const {updateWalletBalances} = useContext(AppContext);

  const [sendValue, setSpendValue] = useState('');
  const [gettingValue, setGettingValue] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [bonusAmount, setBonusAmount] = useState('');
  const [conversionRate, setConversionRate] = useState(0);

  const toastRef = useRef();
  const isSpendEdited = useRef(true);

  useEffect(() => {
    Axios.get(`${GX_API_ENDPOINT}/forex/convert`, {
      params: {
        buy: recipientCurrency.coinSymbol,
        from: sendingCurrency.coinSymbol,
      },
    })
      .then((resp) => {
        const {data} = resp;

        if (data.status) {
          const rate =
            data[
              `${sendingCurrency.coinSymbol.toLowerCase()}_${recipientCurrency.coinSymbol.toLowerCase()}`
            ];

          setConversionRate(rate);
        } else {
          setConversionRate(0);
        }
      })
      .catch((error) => console.log('Conversion Rate error', error));

    return () => {};
  }, [recipientCurrency, sendingCurrency]);

  useEffect(() => {
    if (sendValue && conversionRate && isSpendEdited.current) {
      setGettingValue(
        roundHelper(sendValue * conversionRate, recipientCurrency.coinSymbol),
      );
    }
  }, [sendValue, conversionRate]);

  useEffect(() => {
    if (gettingValue && conversionRate && !isSpendEdited.current) {
      setSpendValue(
        roundHelper(gettingValue / conversionRate, sendingCurrency.coinSymbol),
      );
    }
  }, [gettingValue, conversionRate]);

  const buyClickHandler = async () => {
    if (
      (isNaN(parseFloat(sendValue)) || parseFloat(sendValue) <= 0) &&
      (isNaN(parseFloat(gettingValue)) || parseFloat(gettingValue) <= 0)
    ) {
      setGettingValue('');
      return toastRef.current({
        data: 'Please Input A Valid value',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    let postData = {
      email,
      token,
      app_code: senderAppCode,
      profile_id: senderProfileId,
      sending_currency: sendingCurrency.coinSymbol,
      receiving_currency: sendingCurrency.coinSymbol,
      user_name: name, // INVITED USER NAME
      custom_message: message, // custom message by User
      stats: false, // for trade, set true will give you the trade stats
      expire_in_days: parseInt(expiryDays) || 5,
      user_email: recipient,
    };

    if (gettingValue) {
      postData = {...postData, receiving_amount: parseFloat(gettingValue)};
    } else {
      postData = {...postData, sending_amount: parseFloat(sendValue)};
    }

    if (!isMail) {
      postData = {...postData, user_phone: recipientPhone};
    }

    console.log('PostData', postData);

    Axios.post(
      `${GX_API_ENDPOINT}/coin/vault/service/invite/user/by/transfer`,
      postData,
    )
      .then((resp) => {
        const {data} = resp;

        console.log('Resp', data);

        setIsLoading(false);
        if (data.status) {
          //  toastRef.current({data: data.message, position: WToast.position.TOP});
          updateWalletBalances();
          onNext();
        } else {
          toastRef.current({
            data: data.message || 'Failed',
            position: WToast.position.TOP,
          });
        }
      })
      .catch((error) => {
        console.log('Error', error);
        setIsLoading(false);
      });
  };

  return (
    <View style={styles.container}>
      <WModalShowToastView
        toastInstance={(toastInstance) => (toastRef.current = toastInstance)}
      />

      {isLoading ? (
        <View style={[styles.loadingContainer]}>
          <LoadingAnimation />
        </View>
      ) : (
        <>
          <Text style={styles.header}>Amount</Text>
          <View style={styles.quoteContainer}>
            <QuoteInput
              image={{
                uri: sendingCurrency.coinImage,
              }}
              enabled
              unit={sendingCurrency.coinSymbol}
              // title={`How Much ${paymentCurrency.coinSymbol} Do You Want To Sell?`}
              title="You Will Be Sending"
              placeholder={formatterHelper('0', sendingCurrency.coinSymbol)}
              value={sendValue}
              setValue={(text) => {
                setSpendValue(text);
                isSpendEdited.current = true;
              }}
              // isLoading={isSendLoading}
            />

            <QuoteInput
              image={{
                uri: recipientCurrency.coinImage,
              }}
              unit={recipientCurrency.coinSymbol}
              title={`${name} Should Be Receiving`}
              enabled
              value={gettingValue.toString()}
              setValue={(text) => {
                setGettingValue(text);
                isSpendEdited.current = false;
              }}
              placeholder={formatterHelper('0', recipientCurrency.coinSymbol)}
              // isLoading={isGettingLoading}
            />

            <QuoteInput
              image={{
                uri: sendingCurrency.coinImage,
              }}
              unit={sendingCurrency.coinSymbol}
              title={'#InitiatePlanB Bonus'}
              enabled={false}
              value={bonusAmount.toString()}
              setValue={(text) => setBonusAmount(text)}
              placeholder={formatterHelper('0', sendingCurrency.coinSymbol)}
              // isLoading={isGettingLoading}
            />
          </View>

          <TouchableOpacity style={[styles.buyBtn]} onPress={buyClickHandler}>
            <Image
              resizeMode="contain"
              source={require('../../../assets/block-check-full-logo-white.png')}
              style={styles.blockIcon}
            />
          </TouchableOpacity>
        </>
      )}
    </View>
  );
};

export default QuoteFrom;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    paddingHorizontal: 40,
    paddingVertical: 20,
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 10,
    fontSize: 35,
    fontFamily: 'Montserrat-Bold',
  },
  quoteContainer: {
    marginTop: 20,
    marginBottom: 30,
  },
  buyBtn: {
    backgroundColor: '#08152D',
    height: 45,
    borderRadius: 6,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  disabled: {
    opacity: 0.5,
  },
  errorText: {
    color: '#FF2D55',
    textAlign: 'center',
    marginBottom: 20,
    marginTop: -10,
  },
  buyBtnText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
  },
  feeButton: {
    zIndex: 1,
    position: 'absolute',
    top: 0,
    right: 0,
  },
  feeText: {
    color: '#08152D',
    fontFamily: 'Montserrat',
  },
  blockIcon: {
    height: 17,
  },
  loadingContainer: {
    paddingVertical: 60,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
});
