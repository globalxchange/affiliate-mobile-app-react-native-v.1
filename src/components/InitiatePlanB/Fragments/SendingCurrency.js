import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ActionButton from '../ActionButton';
import LoadingAnimation from '../../LoadingAnimation';
import {WToast} from 'react-native-smart-tip';
import {formatterHelper} from '../../../utils';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../../configs';
import VaultSelector from '../../VaultSelector';

const SendingCurrency = ({
  onNext,
  setSendingCurrency,
  sendingCurrency,
  senderAppCode,
  senderProfileId,
}) => {
  const [availableCurrencies, setAvailableCurrencies] = useState();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    Axios.post(`${GX_API_ENDPOINT}/coin/vault/service/coins/get`, {
      app_code: senderAppCode,
      profile_id: senderProfileId,
    })
      .then((resp) => {
        const {data} = resp;

        // console.log('Sending Balance', data);

        if (data.status) {
          const balances = data.coins_data || [];

          const filteredCurrencies = [];

          balances.forEach((item) => {
            if (item.coinValue > 0) {
              filteredCurrencies.push({
                ...item,
                balance: formatterHelper(item.coinValue, item.coinSymbol),
                image: item.coinImage,
                name: item.coinName,
              });
            }
          });

          setAvailableCurrencies(filteredCurrencies);
        }
      })
      .catch((error) => {})
      .finally(() => setIsLoading(false));
  }, [senderAppCode, senderProfileId]);

  const onNextClick = () => {
    if (!sendingCurrency) {
      return WToast.show({
        data: 'Please select a sending currency',
        position: WToast.position.TOP,
      });
    }
    onNext();
  };

  if (!availableCurrencies || isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <LoadingAnimation />
      </View>
    );
  }

  if (availableCurrencies.length <= 0) {
    return (
      <View style={styles.loadingContainer}>
        <Text style={styles.notAvailableText}>No Balance In Any Currency</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.controlContainer}>
        <View>
          <Text style={styles.header}>Sending Currency</Text>
          <Text style={styles.subHeader}>
            Select The Currency That You Would Like To Send Your Friend
          </Text>
        </View>
        <View style={styles.dropDownContainer}>
          <VaultSelector
            placeHolder="Select One Of Your Assets"
            onItemSelect={setSendingCurrency}
            selectedItem={sendingCurrency}
            onNext={onNext}
          />
        </View>
      </View>
      <ActionButton text="Proceed To Select Currency" onPress={onNextClick} />
    </View>
  );
};

export default SendingCurrency;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    paddingHorizontal: 40,
    paddingVertical: 20,
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 40,
    fontSize: 28,
    fontFamily: 'Montserrat-Bold',
    marginTop: 20,
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  controlContainer: {
    justifyContent: 'center',
  },
  dropDownContainer: {
    justifyContent: 'center',
    marginVertical: 30,
  },
  loadingContainer: {
    justifyContent: 'center',
    paddingVertical: 50,
  },
  notAvailableText: {
    color: '#08152D',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
  },
});
