import React, {useContext} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {AppContext} from '../../../contexts/AppContextProvider';
import {formatterHelper} from '../../../utils';

const CompleteView = ({name, isMail, isSend, onClose, sendingCurrency}) => {
  const {walletBalances} = useContext(AppContext);

  const updatedSendBalance =
    walletBalances[`${sendingCurrency.coinSymbol.toLowerCase()}_balance`] || 0;

  const openTransactionAudit = async () => {};

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.header}>Completed</Text>
        <Text style={styles.subHeader}>Great Job. You Have Sent To {name}</Text>
      </View>
      <View style={styles.valuesContainer}>
        <View style={styles.valueItem}>
          <Text style={styles.title}>
            New {sendingCurrency.coinName} Balance
          </Text>
          <View style={styles.inputContainer}>
            <Text style={styles.textInput}>
              {formatterHelper(updatedSendBalance, sendingCurrency.coinSymbol)}
            </Text>
            <View style={styles.unitContainer}>
              <Text style={styles.unitValue}>{sendingCurrency.coinSymbol}</Text>
              <Image
                source={{uri: sendingCurrency.coinImage}}
                style={styles.unitImage}
                resizeMode="cover"
              />
            </View>
          </View>
        </View>
      </View>
      <TouchableOpacity onPress={onClose} style={styles.button}>
        <Text style={styles.buttonText}>Close</Text>
      </TouchableOpacity>
    </View>
  );
};

export default CompleteView;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    paddingHorizontal: 40,
    paddingBottom: 30,
    paddingTop: 10,
  },
  headerContainer: {
    alignItems: 'flex-start',
  },
  headerImage: {
    height: 40,
    width: 190,
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 10,
    fontSize: 30,
    fontFamily: 'Montserrat-Bold',
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    marginBottom: 20,
    fontSize: 12,
    fontFamily: 'Montserrat',
    marginTop: 10,
  },
  textContainer: {
    paddingVertical: 30,
  },
  successText: {
    paddingVertical: 10,
    color: '#9A9A9A',
    fontFamily: 'Montserrat',
  },
  button: {
    backgroundColor: '#08152D',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 6,
    height: 50,
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
  },
  valuesContainer: {
    marginVertical: 50,
  },
  actionContainer: {
    flexDirection: 'row',
  },
  scrollView: {
    marginHorizontal: -10,
    // flex: 1,
  },
  action: {
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    backgroundColor: 'white',
    borderRadius: 8,
    paddingVertical: 10,
    paddingHorizontal: 20,
    flex: 1,
    marginVertical: 10,
    minWidth: 130,
  },
  actionIcon: {
    width: 35,
    height: 35,
  },
  actionText: {
    marginTop: 10,
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    textAlign: 'center',
    fontSize: 12,
  },
  actionTextBlack: {
    color: '#464B4E',
  },
  valueItem: {marginBottom: 10},
  title: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  inputContainer: {
    flexDirection: 'row',
    borderWidth: 2,
    borderRadius: 8,
    borderColor: '#EBEBEB',
    height: 50,
    alignItems: 'center',
  },
  textInput: {
    flexGrow: 1,
    width: 0,
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    paddingHorizontal: 20,
    color: 'black',
  },
  unitContainer: {
    width: '45%',
    flexDirection: 'row',
    alignItems: 'center',
    borderLeftColor: '#EBEBEB',
    borderLeftWidth: 2,
    justifyContent: 'center',
  },
  unitValue: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
  },
  unitImage: {
    height: 24,
    width: 24,
    borderRadius: 12,
    marginLeft: 15,
  },
});
