import React, {useState, useEffect, useContext} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ActionButton from '../ActionButton';
import {AppContext} from '../../../contexts/AppContextProvider';
import LoadingAnimation from '../../LoadingAnimation';
import {WToast} from 'react-native-smart-tip';
import VaultSelector from '../../VaultSelector';

const RecipientCurrency = ({
  onNext,
  setRecipientCurrency,
  recipientCurrency,
}) => {
  const {cryptoTableData} = useContext(AppContext);

  const [availableCurrencies, setAvailableCurrencies] = useState();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (cryptoTableData) {
      setAvailableCurrencies(cryptoTableData);
    }
  }, [cryptoTableData]);

  const onNextClick = () => {
    if (!recipientCurrency) {
      return WToast.show({
        data: 'Please select a sending currency',
        position: WToast.position.TOP,
      });
    }
    onNext();
  };

  if (!availableCurrencies || isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <LoadingAnimation />
      </View>
    );
  }

  if (availableCurrencies.length <= 0) {
    return (
      <View style={styles.loadingContainer}>
        <Text style={styles.notAvailableText}>No Currencies Supported</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Recipient Currency</Text>
      <Text style={styles.subHeader}>
        Select The Currency Which You Would Like Your Friend To Receive
      </Text>
      <View style={styles.controlContainer}>
        <View style={styles.dropDownContainer}>
          <VaultSelector
            placeHolder="Select One Of Your Assets"
            onItemSelect={setRecipientCurrency}
            selectedItem={recipientCurrency}
            onNext={onNext}
          />
        </View>
      </View>
      <ActionButton text="Proceed To Select Currency" onPress={onNextClick} />
    </View>
  );
};

export default RecipientCurrency;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    paddingHorizontal: 40,
    paddingVertical: 20,
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 40,
    fontSize: 30,
    fontFamily: 'Montserrat-Bold',
    marginTop: 20,
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  controlContainer: {
    justifyContent: 'center',
  },
  radioContainer: {
    flexDirection: 'row',
  },
  radioButton: {
    flex: 1,
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 4,
    paddingVertical: 10,
  },
  radioButtonActive: {
    borderColor: '#08152D',
  },
  radioText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#788995',
    textAlign: 'center',
  },
  radioTextActive: {
    color: '#08152D',
  },
  dropDownContainer: {
    justifyContent: 'center',
    marginVertical: 30,
  },
  loadingContainer: {
    justifyContent: 'center',
    paddingVertical: 50,
  },
  notAvailableText: {
    color: '#08152D',
    textAlign: 'center',
    fontSize: 18,
    fontFamily: 'Montserrat-SemiBold',
  },
});
