import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {WToast} from 'react-native-smart-tip';

const MessageForm = ({setMessage, onNext}) => {
  const [isEditorOpen, setIsEditorOpen] = useState(false);
  const [messageInput, setMessageInput] = useState(
    'Hey, I Thought You Deserved Some Crypto',
  );

  const onProceed = () => {
    if (!messageInput) {
      setIsEditorOpen(true);
      return WToast.show({
        data: 'Please Input Your Message',
        position: WToast.position.TOP,
      });
    }
    setMessage(messageInput);
    onNext();
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Create Message</Text>
      <Text style={styles.subHeader}>
        Let Your Recipient Know Why They Are Getting Crypto
      </Text>
      <View style={styles.messageContainer}>
        {isEditorOpen ? (
          <TextInput
            placeholderTextColor={'#878788'}
            placeholder="Your Message"
            value={messageInput}
            style={styles.input}
            onChangeText={(text) => setMessageInput(text)}
            multiline
          />
        ) : (
          <Text style={styles.message}>
            Hey, I Thought You Deserved Some Crypto
          </Text>
        )}
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity
          onPress={() => setIsEditorOpen(!isEditorOpen)}
          style={[styles.button, {marginRight: 10}]}>
          <Text style={styles.buttonText}>
            {isEditorOpen ? 'Close' : 'Edit'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={onProceed} style={styles.button}>
          <Text style={styles.buttonText}>Proceed</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default MessageForm;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 40,
    paddingVertical: 20,
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 20,
    fontSize: 30,
    fontFamily: 'Montserrat-Bold',
  },
  subHeader: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    textAlign: 'left',
  },
  messageContainer: {
    height: 180,
    justifyContent: 'center',
    paddingVertical: 15,
  },
  message: {
    color: '#231F20',
    textAlign: 'left',
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  buttonContainer: {
    flexDirection: 'row',
  },
  button: {
    flex: 1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#08152D',
    borderRadius: 6,
  },
  buttonText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
  },
  input: {
    fontFamily: 'Montserrat',
    color: 'black',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 20,
    minHeight: 50,
  },
});
