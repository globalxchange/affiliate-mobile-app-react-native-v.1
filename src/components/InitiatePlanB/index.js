/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {Image, StyleSheet, View} from 'react-native';
import BottomSheetLayout from '../../layouts/BottomSheetLayout';
import Breadcrumb from './Breadcrumb';
import CompleteView from './Fragments/CompleteView';
import ExpirySelector from './Fragments/ExpirySelector';
import FromAppSelector from './Fragments/FromAppSelector';
import MessageForm from './Fragments/MessageForm';
import QuoteFrom from './Fragments/QuoteFrom';
import RecipientCurrency from './Fragments/RecipientCurrency';
import RecipientForm from './Fragments/RecipientForm';
import SendingCurrency from './Fragments/SendingCurrency';

const InitiatePlanB = ({isOpen, setIsOpen}) => {
  const [currentStep, setCurrentStep] = useState();

  const [message, setMessage] = useState('');
  const [senderAppCode, setSenderAppCode] = useState('');
  const [senderProfileId, setSetSenderProfileId] = useState('');
  const [sendingCurrency, setSendingCurrency] = useState('');
  const [recipientCurrency, setRecipientCurrency] = useState('');
  const [recipientName, setRecipientName] = useState('');
  const [isMail, setIsMail] = useState(true);
  const [recipient, setRecipient] = useState('');
  const [recipientPhone, setRecipientPhone] = useState('');
  const [expiryDays, setExpiryDays] = useState('');

  useEffect(() => {
    if (!isOpen) {
      resetState();
    }
  }, [isOpen]);

  const resetState = () => {
    setCurrentStep();
    setMessage('');
    setSenderAppCode('');
    setSetSenderProfileId('');
    setSendingCurrency('');
    setRecipientCurrency('');
    setRecipientName('');
    setIsMail(true);
    setRecipient('');
  };

  let activeViewContent;

  switch (currentStep) {
    case 'MessageForm':
      activeViewContent = (
        <MessageForm
          setMessage={setMessage}
          onNext={() => setCurrentStep('AppSelector')}
        />
      );
      break;
    case 'AppSelector':
      activeViewContent = (
        <FromAppSelector
          setAppCode={setSenderAppCode}
          setSenderProfileId={setSetSenderProfileId}
          onNext={() => setCurrentStep('SendingCurrency')}
        />
      );
      break;

    case 'SendingCurrency':
      activeViewContent = (
        <SendingCurrency
          senderAppCode={senderAppCode}
          senderProfileId={senderProfileId}
          sendingCurrency={sendingCurrency}
          setSendingCurrency={setSendingCurrency}
          onNext={() => setCurrentStep('RecipientCurrency')}
        />
      );
      break;

    case 'RecipientCurrency':
      activeViewContent = (
        <RecipientCurrency
          onNext={() => setCurrentStep('DestinationAddress')}
          recipientCurrency={recipientCurrency}
          setRecipientCurrency={setRecipientCurrency}
        />
      );
      break;

    case 'DestinationAddress':
      activeViewContent = (
        <RecipientForm
          currency={SendingCurrency}
          onNext={() => setCurrentStep('Expiry')}
          setEmail={setRecipient}
          setPhone={setRecipientPhone}
          setName={setRecipientName}
          setIsMail={setIsMail}
        />
      );
      break;

    case 'Expiry':
      activeViewContent = (
        <ExpirySelector
          onNext={() => setCurrentStep('Quote')}
          expiryDays={expiryDays}
          setExpiryDays={setExpiryDays}
        />
      );
      break;

    case 'Quote':
      activeViewContent = (
        <QuoteFrom
          senderAppCode={senderAppCode}
          senderProfileId={senderProfileId}
          name={recipientName}
          sendingCurrency={sendingCurrency}
          recipientCurrency={recipientCurrency}
          recipient={recipient}
          recipientPhone={recipientPhone}
          isMail={isMail}
          message={message}
          onNext={() => setCurrentStep('Completed')}
          expiryDays={expiryDays}
        />
      );
      break;

    case 'Completed':
      activeViewContent = (
        <CompleteView
          sendingCurrency={sendingCurrency}
          name={recipientName}
          isMail={isMail}
          onClose={() => setIsOpen(false)}
          isSend
        />
      );
      break;

    default:
      activeViewContent = (
        <MessageForm
          setMessage={setMessage}
          onNext={() => setCurrentStep('AppSelector')}
        />
      );
  }

  return (
    <BottomSheetLayout
      isOpen={isOpen}
      onClose={() => setIsOpen(false)}
      reactToKeyboard>
      <View style={styles.fragmentContainer}>
        <Image
          source={require('../../assets/plan-b-icon-fill.png')}
          style={styles.headerImage}
          resizeMode="contain"
        />
        <View style={styles.viewContainer}>
          {currentStep !== 'Completed' && <Breadcrumb />}
          {activeViewContent}
        </View>
      </View>
    </BottomSheetLayout>
  );
};

export default InitiatePlanB;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    // flex: 1,
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    overflow: 'hidden',
  },
  fragmentContainer: {
    // flex: 1,
  },
  headerImage: {
    height: 30,
    width: 170,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginVertical: 20,
  },
  viewContainer: {
    borderTopColor: '#EBEBEB',
    borderTopWidth: 1,
  },
});
