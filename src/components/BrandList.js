import React, {useEffect, useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import Axios from 'axios';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {GX_API_ENDPOINT} from '../configs';
import LoadingAnimation from './LoadingAnimation';
import SkeltonItem from './SkeltonItem';

const BrandList = () => {
  const {navigate} = useNavigation();

  const [brandList, setBrandList] = useState();

  useEffect(() => {
    Axios.get('https://teller2.apimachine.com/admin/allBankers')
      .then((resp) => {
        const {data} = resp;
        if (data.status) {
          // console.log('data', data);
          setBrandList(data.data);
        } else {
          setBrandList([]);
        }
      })
      .catch((error) => {
        setBrandList([]);
      });
  }, []);

  const onItemClick = (item) => {
    navigate('Brands', {selectedItem: item});
  };

  return (
    <View style={styles.container}>
      {brandList ? (
        <FlatList
          showsVerticalScrollIndicator={false}
          data={brandList}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item}) => (
            <BrandItem onItemClick={onItemClick} item={item} />
          )}
        />
      ) : (
        <View style={styles.loadingContainer}>
          <SkeltonAnimation />
          <SkeltonAnimation />
          <SkeltonAnimation />
          <SkeltonAnimation />
        </View>
      )}
    </View>
  );
};

const BrandItem = ({onItemClick, item}) => {
  const [noOfLicence, setNoOfLicence] = useState('');

  useEffect(() => {
    if (item) {
      Axios.get(`${GX_API_ENDPOINT}/gxb/product/get`, {
        params: {product_created_by: item.email},
      })
        .then((resp) => {
          const {data} = resp;

          // console.log('getLicenceDetails', data);

          const products = data.products || [];

          setNoOfLicence(products.length || 0);
        })
        .catch((error) => {
          console.log('Error on getting licence details', error);
        });
    }
  }, [item]);

  return (
    <TouchableOpacity
      onPress={() => onItemClick(item)}
      style={[styles.itemContainer]}>
      <View
        style={[
          styles.imageContainer,
          {backgroundColor: `#${item.colorCode}`},
        ]}>
        <Image
          source={{uri: item.profilePicWhitePNG}}
          style={styles.image}
          resizeMode="contain"
        />
      </View>
      <View style={styles.itemDetails}>
        <Text style={styles.itemName}>{item.displayName}</Text>
        <Text style={styles.numberOfLicense}>{noOfLicence || 0} Licenses</Text>
      </View>
    </TouchableOpacity>
  );
};

const SkeltonAnimation = () => (
  <View style={[styles.itemContainer]}>
    <SkeltonItem itemWidth={80} itemHeight={60} />
    <View style={styles.itemDetails}>
      <SkeltonItem itemWidth={180} itemHeight={20} style={styles.itemName} />
      <SkeltonItem itemWidth={80} itemHeight={10} style={{marginTop: 5}} />
    </View>
  </View>
);

export default BrandList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    marginBottom: 5,
  },
  loadingContainer: {
    flex: 1,
  },
  itemContainer: {
    flexDirection: 'row',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    overflow: 'hidden',
    marginTop: 20,
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 80,
  },
  image: {
    height: 30,
    width: 30,
  },
  itemDetails: {
    justifyContent: 'center',
    paddingLeft: 20,
    paddingVertical: 10,
  },
  itemName: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  numberOfLicense: {
    color: '#08152D',
    fontFamily: 'Montserrat',
  },
});
