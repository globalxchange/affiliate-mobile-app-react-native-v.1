import React, {useEffect, useState} from 'react';
import {Dimensions, Image, StyleSheet, Text, View} from 'react-native';
import {BarCodeScanner} from 'expo-barcode-scanner';

const {width} = Dimensions.get('window');
const qrSize = width * 0.7;

const QRScanFragment = ({onScanned, onClose, setIsScannerOpen}) => {
  const [scanned, setScanned] = useState(false);
  const [hasPermission, setHasPermission] = useState(null);

  useEffect(() => {
    (async () => {
      await getPermision();
    })();
  }, []);

  const getPermision = async () => {
    const {status} = await BarCodeScanner.requestPermissionsAsync();
    setHasPermission(status === 'granted');
  };

  const handleBarCodeScanned = ({type, data}) => {
    setScanned(true);
    if (onScanned) {
      onScanned(data);
    }
    closeScanner();
  };

  const closeScanner = () => {
    setIsScannerOpen(false);
    onClose();
  };

  if (hasPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  return (
    <View style={styles.container}>
      <BarCodeScanner
        barCodeTypes={[BarCodeScanner.Constants.BarCodeType.qr]}
        onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
        style={[styles.qrContainer]}>
        <Text style={styles.description}>Scan your QR code</Text>
        <Image style={styles.qr} source={require('../assets/qr-frame.png')} />
        <Text onPress={closeScanner} style={styles.cancel}>
          Cancel
        </Text>
      </BarCodeScanner>
    </View>
  );
};

export default QRScanFragment;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
  },
  qrContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'black',
  },
  qr: {
    marginTop: '20%',
    marginBottom: '20%',
    width: qrSize,
    height: qrSize,
  },
  description: {
    fontSize: width * 0.06,
    marginTop: '12%',
    textAlign: 'center',
    width: '70%',
    fontFamily: 'Montserrat-Bold',
    color: 'white',
  },
  cancel: {
    fontSize: width * 0.05,
    textAlign: 'center',
    width: '70%',
    marginBottom: 40,
    fontFamily: 'Montserrat-Bold',
    color: 'white',
    paddingVertical: 15,
  },
});
