import React, {useEffect, useRef, useState} from 'react';
import {Keyboard, Platform, StyleSheet, Text, View} from 'react-native';
import Animated, {
  Clock,
  interpolate,
  set,
  useCode,
} from 'react-native-reanimated';
import ReanimatedTimingHelper from '../utils/ReanimatedTimingHelper';

const KeyboardOffset = ({offset = 0, onlyIos}) => {
  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);

  const chatKeyboardAnimation = useRef(new Animated.Value(0));

  useEffect(() => {
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
      onKeyBoardShow,
    );
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
      onKeyBoardHide,
    );
    return () => {
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
        onKeyBoardShow,
      );
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
        onKeyBoardHide,
      );
    };
  }, []);

  useCode(
    () =>
      isKeyboardOpen
        ? [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ]
        : [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ],
    [isKeyboardOpen],
  );

  const onKeyBoardShow = (e) => {
    if (onlyIos) {
      if (Platform.OS === 'ios') {
        setKeyboardHeight(e.endCoordinates.height - 30);
        setIsKeyboardOpen(true);
      }
    } else {
      setKeyboardHeight(e.endCoordinates.height - 30);
      setIsKeyboardOpen(true);
    }
  };

  const onKeyBoardHide = () => {
    setIsKeyboardOpen(false);
  };

  return (
    <Animated.View
      style={[
        {
          height: interpolate(chatKeyboardAnimation.current, {
            inputRange: [0, 1],
            outputRange: [0, keyboardHeight + offset],
          }),
        },
      ]}
    />
  );
};

export default KeyboardOffset;
