import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  Dimensions,
} from 'react-native';

const {width} = Dimensions.get('window');

const ViewSwitcher = ({activeSwitch, setActiveSwitch, menuItems}) => {
  return (
    <View style={styles.listSwitcher}>
      {menuItems.map((item) => (
        <TouchableWithoutFeedback
          key={item.title}
          style={styles.itemContainer}
          onPress={item.disabled ? null : () => setActiveSwitch(item.title)}>
          <View style={styles.item}>
            <View
              style={
                activeSwitch === item.title
                  ? styles.itemActive
                  : styles.iconContainer
              }>
              <Image
                style={styles.icon}
                source={item.image}
                resizeMode="contain"
              />
            </View>
            <Text
              style={
                activeSwitch === item.title ? styles.titleActive : styles.title
              }>
              {item.title}
            </Text>
            <View
              style={[
                styles.selector,
                activeSwitch === item.title && styles.selectorActive,
              ]}
            />
          </View>
        </TouchableWithoutFeedback>
      ))}
    </View>
  );
};

export default ViewSwitcher;

const styles = StyleSheet.create({
  listSwitcher: {
    flexDirection: 'row',
    marginHorizontal: 20,
    marginTop: 20,
  },
  itemContainer: {paddingHorizontal: 5},
  item: {
    width: (width - 40) / 4,
  },
  iconContainer: {
    height: 40,
    opacity: 0.5,
    width: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 'auto',
    marginRight: 'auto',
    borderRadius: 2,
  },
  itemActive: {
    height: 40,
    width: 50,
    opacity: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 'auto',
    marginRight: 'auto',
    borderRadius: 2,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.15,
    shadowRadius: 2,
    backgroundColor: 'white',
    elevation: 2,
  },
  icon: {height: 25, width: 25},
  title: {
    textAlign: 'center',
    color: '#788995',
    marginTop: 5,
    opacity: 0.5,
    fontSize: 10,
    fontFamily: 'Montserrat',
  },
  titleActive: {
    textAlign: 'center',
    color: '#08152D',
    marginTop: 5,
    fontSize: 10,
    fontFamily: 'Montserrat-SemiBold',
  },
  selector: {
    width: 0,
    height: 0,
    borderLeftWidth: 8,
    borderRightWidth: 8,
    borderBottomWidth: 8,
    borderStyle: 'solid',
    backgroundColor: 'transparent',
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: 'white',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 5,
  },
  selectorActive: {
    borderBottomColor: '#F9F9F9',
  },
});
