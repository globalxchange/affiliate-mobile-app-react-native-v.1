import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  FlatList,
  Dimensions,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {getUriImage, urlValidatorRegex} from '../../utils';

const {width, height} = Dimensions.get('window');

const ListPopup = ({
  isDropDownOpen,
  setIsDropDownOpen,
  items,
  onItemSelected,
}) => {
  return (
    <Modal
      animationType="fade"
      transparent
      visible={isDropDownOpen}
      hardwareAccelerated
      statusBarTranslucent
      onDismiss={() => setIsDropDownOpen(false)}
      onRequestClose={() => setIsDropDownOpen(false)}>
      <TouchableOpacity
        activeOpacity={1}
        style={styles.overlay}
        onPress={() => setIsDropDownOpen(false)}
        onPressOut={() => {}}>
        <TouchableWithoutFeedback style={{flex: 1}}>
          <View style={styles.modalContainer}>
            <FlatList
              showsVerticalScrollIndicator={false}
              style={styles.dropDownList}
              data={items}
              keyExtractor={(item) => item.name}
              renderItem={({item}) => (
                <TouchableOpacity
                  disabled={item.disabled}
                  onPress={onItemSelected ? () => onItemSelected(item) : null}
                  style={[
                    styles.dropDownItem,
                    item.disabled && {opacity: 0.5},
                  ]}>
                  <FastImage
                    style={styles.countryImage}
                    source={
                      urlValidatorRegex.test(item.image)
                        ? {uri: getUriImage(item.image)}
                        : item.image
                    }
                    resizeMode="contain"
                  />
                  <Text style={styles.itemText}>{item.name}</Text>
                </TouchableOpacity>
              )}
              ListEmptyComponent={
                <Text style={styles.emptyText}>No Option Found...</Text>
              }
            />
          </View>
        </TouchableWithoutFeedback>
      </TouchableOpacity>
    </Modal>
  );
};

export default ListPopup;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.2)',
    flex: 1,
    justifyContent: 'center',
    paddingVertical: height * 0.25,
  },
  modalContainer: {
    paddingVertical: 20,
    backgroundColor: 'white',
    borderRadius: 10,
    width: width * 0.75,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  dropDownList: {
    paddingHorizontal: 30,
  },
  dropDownItem: {flexDirection: 'row', paddingVertical: 10},
  countryImage: {height: 24, width: 24},
  itemText: {
    flex: 1,
    marginLeft: 15,
    fontFamily: 'Montserrat',
    color: '#9A9A9A',
  },
  itemSubText: {
    fontFamily: 'Roboto',
    color: '#788995',
    fontSize: 12,
  },
  emptyText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    textAlign: 'center',
    paddingVertical: 20,
    fontSize: 18,
  },
});
