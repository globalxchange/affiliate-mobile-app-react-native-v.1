import React, {useState, useEffect, useContext} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ViewSwitcher from './ViewSwitcher';
import BrokerFragment from './Fragments/BrokerFragment';
import OTCBots from '../ControllerLists/OTCBots';
import AffiliateBank from './Fragments/AffiliateBank';
import Engagement from './Fragments/Engagement';
import {AppContext} from '../../contexts/AppContextProvider';

const BrokerViews = ({activeBanker}) => {
  const [activeLicense, setActiveLicense] = useState('');

  const switchItems = [
    {
      title: 'Broker',
      image: require('../../assets/broker-app-icon-dark.png'),
    },
    {
      title: 'OTCBots',
      image: require('../../assets/otc-bots-icon.png'),
      disabled: !activeLicense,
    },
    {
      title: 'Engagement',
      image: require('../../assets/engagement-icon.png'),
      disabled: !activeLicense,
    },
    {
      title: 'AffiliateBank',
      image: require('../../assets/affliate-bank-icon.png'),
      disabled: !activeLicense,
    },
  ];

  const [activeSwitch, setActiveSwitch] = useState(switchItems[0].title);

  let activeFragment = null;

  switch (activeSwitch) {
    case 'Broker':
      activeFragment = (
        <BrokerFragment
          activeBanker={activeBanker}
          setActiveLicense={setActiveLicense}
        />
      );
      break;

    case 'OTCBots':
      activeFragment = <OTCBots />;
      break;

    case 'AffiliateBank':
      activeFragment = <AffiliateBank />;
      break;

    case 'Engagement':
      activeFragment = <Engagement activeBanker={activeBanker} />;
      break;

    default:
      activeFragment = <BrokerFragment setActiveLicense={setActiveLicense} />;
  }

  return (
    <View style={styles.container}>
      <ViewSwitcher
        menuItems={switchItems}
        activeSwitch={activeSwitch}
        setActiveSwitch={setActiveSwitch}
      />
      {activeFragment}
    </View>
  );
};

export default BrokerViews;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
