import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';

const FilterMenu = ({
  selectedFilter,
  setSelectedFilter,
  onClose,
  isTxnType,
  setTxnType,
}) => {
  let menus = isTxnType ? [...TYPE_MENU_ITEMS] : [...LIST_MENU_ITEMS];

  const selectedIndex = menus.findIndex((x) => x.key === selectedFilter);

  // if (selectedIndex > -1) {
  //   menus.splice(selectedIndex, 1);
  // }

  const onItemPress = (item) => {
    if (isTxnType) {
      setTxnType(item);
    } else {
      setSelectedFilter(item);
    }
    onClose();
  };

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        {menus.map((item) => (
          <TouchableOpacity
            onPress={() => onItemPress(item)}
            key={item.key}
            style={styles.filterItem}>
            <Text style={styles.filerItemText}>{item.title}</Text>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  );
};

export default FilterMenu;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: -20,
    marginBottom: -20,
  },
  filterItem: {
    borderBottomColor: '#EBEBEB',
    borderBottomWidth: 1,
    paddingHorizontal: 20,
    flex: 1,
    justifyContent: 'center',
    paddingVertical: 30,
  },
  filerItemText: {
    color: '#001D41',
    fontFamily: 'Montserrat',
    fontSize: 14,
  },
});

const LIST_MENU_ITEMS = [
  {title: 'All', key: 'All'},
  {title: 'Direct Broker', key: 'DB'},
  {title: 'Broker Chain', key: 'BC'},
  {title: 'Direct Broker-Dealer', key: 'DBD'},
  {title: 'Broker-Dealer Chain', key: 'BDC'},
];

const TYPE_MENU_ITEMS = [
  {title: 'Trading Revenue', key: 'TR'},
  {title: 'Token Sales', key: 'TS'},
  {title: 'License Subscriptions', key: 'LS'},
  {title: 'Interest', key: 'IN'},
  // {title: 'Bonds', key: 'BO'},
  {title: 'Legacy Money Markets', key: 'MM'},
];
