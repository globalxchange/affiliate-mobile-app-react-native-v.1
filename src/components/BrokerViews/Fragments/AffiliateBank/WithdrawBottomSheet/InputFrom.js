import React, {useRef} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import CustomNumpadView from '../../../../CustomNumpadView';
import {
  usdValueFormatterWithoutSign,
  usdValueFormatter,
} from '../../../../../utils';
import {WToast, WModalShowToastView} from 'react-native-smart-tip';

const InputFrom = ({
  withdrawAmount,
  setWithdrawAmount,
  withdrawableBalance,
  onWithdraw,
  setWhereSend,
}) => {
  const toastRef = useRef();

  const withdrawClick = () => {
    if (isNaN(parseFloat(withdrawAmount)) || !parseFloat(withdrawAmount)) {
      return WToast.show({
        data: 'Please Input A Valid Value',
        position: WToast.position.TOP,
      });
    }

    // onWithdraw();
    setWhereSend(true);
  };

  return (
    <View>
      <WModalShowToastView
        toastInstance={(toastInstance) => (toastRef.current = toastInstance)}
      />
      <View style={styles.viewContainer}>
        <Image
          source={require('../../../../../assets/affliate-bank-icon-full_blue.png')}
          resizeMode="contain"
          style={styles.headerIcon}
        />
        <View style={styles.inputContainer}>
          <TouchableOpacity style={[styles.input]}>
            <Text style={[styles.text, styles.focused]}>
              {withdrawAmount
                ? isNaN(parseFloat(withdrawAmount))
                  ? '$' + withdrawAmount
                  : '$' +
                    usdValueFormatterWithoutSign.format(
                      parseFloat(withdrawAmount),
                    )
                : '$' + usdValueFormatterWithoutSign.format('0')}
            </Text>
          </TouchableOpacity>
          <Text style={[styles.cryptoName, styles.focusedText]}>USD</Text>
        </View>
        <View style={styles.divider} />
        <View style={styles.balanceContainer}>
          <Text style={styles.balanceLabel}>Balance</Text>
          <Text style={styles.balanceAmount}>
            {withdrawableBalance === ''
              ? 'Calculating'
              : usdValueFormatter.format(withdrawableBalance)}
          </Text>
        </View>
      </View>
      <CustomNumpadView
        currentText={withdrawAmount}
        updatedCallback={setWithdrawAmount}
      />
      <TouchableOpacity
        onPress={() => {
          withdrawClick();
        }}
        style={styles.continueButton}>
        <Text style={styles.buttonText}>Confirm Withdraw</Text>
      </TouchableOpacity>
    </View>
  );
};

export default InputFrom;

const styles = StyleSheet.create({
  container: {flex: 1},

  viewContainer: {
    paddingVertical: 30,
    paddingHorizontal: 30,
  },
  headerIcon: {
    height: 52.45,
    width: 253.45,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  inputContainer: {flexDirection: 'row', alignItems: 'center', marginTop: 40},
  input: {
    flexGrow: 1,
    width: 0,
    color: '#041939',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    paddingHorizontal: 10,
  },
  text: {
    color: '#001D41',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 20,
  },
  focused: {
    color: '#001D41',
  },
  divider: {
    backgroundColor: '#08152D',
    height: 1,
    marginVertical: 5,
  },
  cryptoName: {
    marginLeft: 10,
    color: '#001D41',
    fontFamily: 'Montserrat-Bold',
    fontSize: 20,
  },
  focusedText: {
    color: '#001D41',
  },
  continueButton: {
    height: 55,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: '#001D41',
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
    // fontWeight: '700',
  },
  balanceContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  balanceLabel: {
    color: '#001D41',
    fontFamily: 'Montserrat',
    marginLeft: 10,
    fontWeight: '400',
    fontSize: 15,
  },
  balanceAmount: {
    color: '#001D41',
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
  },
});
