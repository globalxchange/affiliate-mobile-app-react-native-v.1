import React, {useEffect, useRef} from 'react';
import {StyleSheet, Text, View, Image, Animated} from 'react-native';

const Loader = ({apptitle}) => {
  const spread = useRef(new Animated.Value(1)).current;

  useEffect(() => {
    Animated.loop(
      Animated.sequence([
        Animated.timing(spread, {
          toValue: 1,
          duration: 0,
          useNativeDriver: true,
          types: 'linear',
        }),
        Animated.timing(spread, {
          toValue: 1.2,
          duration: 1000,
          useNativeDriver: true,
          types: 'linear',
        }),
        Animated.timing(spread, {
          toValue: 1,
          duration: 1000,
          useNativeDriver: true,
          types: 'linear',
        }),
      ]),
    ).start();
  }, []);
  return (
    <View style={styles.container}>
      <Animated.Image
        style={[
          styles.icon,
          {
            transform: [{scale: spread}],
          },
        ]}
        source={require('../../../../../assets/affliate-bank-icon-full_blue.png')}
        resizeMode="contain"
      />
      <Text style={styles.loadingText}>
        Withdrawing Commissions To {apptitle ? apptitle : 'null'}. This Can Take
        Several Minutes
      </Text>
    </View>
  );
};

export default Loader;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
    position: 'relative',
  },
  icon: {
    width: '80%',
    height: 62.71,
  },
  loadingText: {
    textAlign: 'center',
    color: '#001D41',
    fontSize: 13,
    fontFamily: 'Montserrat',
    position: 'absolute',
    bottom: 36,
    width: '100%',
  },
});
