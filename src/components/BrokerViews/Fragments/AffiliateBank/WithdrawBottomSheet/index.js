import React, {useState, useEffect, useContext} from 'react';
import {Button, StyleSheet, View} from 'react-native';
import InputFrom from './InputFrom';
import Loader from './Loader';
import CompleteView from './CompleteView';
import AsyncStorageHelper from '../../../../../utils/AsyncStorageHelper';
import Axios from 'axios';
import {GX_API_ENDPOINT, APP_CODE} from '../../../../../configs';
import {WToast} from 'react-native-smart-tip';
import {AppContext} from '../../../../../contexts/AppContextProvider';
import BottomSheetLayout from '../../../../../layouts/BottomSheetLayout';
import WithdrawSelection from './WithdrawSelection';
import axios from 'axios';

const WithdrawBottomSheet = ({
  isBottomSheetOpen,
  setIsBottomSheetOpen,
  withdrawableBalance,
  updateBrokerBalances,
}) => {
  const {updateWalletBalances} = useContext(AppContext);

  const [withdrawAmount, setWithdrawAmount] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isCompleted, setIsCompleted] = useState(false);
  const [wheresend, setWhereSend] = useState(false);
  const [selecteduserapp, setSelectedUserApp] = useState([]);

  useEffect(() => {
    if (!isBottomSheetOpen) {
      setWithdrawAmount('');
      setIsLoading(false);
      setIsCompleted(false);
      setWhereSend(false);
      setSelectedUserApp([]);

      console.log('sheet closed...');
    }
  }, [isBottomSheetOpen]);

  useEffect(() => {
    if (selecteduserapp?.profile_id) {
      // console.log("data in usestate",selecteduserapp);
      onWithdraw();
      // setIsLoading(true);
      // setTimeout(() => {
      //   setIsLoading(false);
      // }, 5000);
      // setIsCompleted(true);
    } else {
      console.log('empty');
    }
  }, [selecteduserapp]);

  // useEffect(() => {
  //   if (gxbalance && coinvalue) {
  //     setIsCompleted(true);
  //   }
  // }, [gxbalance, coinvalue]);

  const onWithdraw = async () => {
    setIsLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();
    // const profileId = await AsyncStorageHelper.getProfileId();

    const postData = {
      email,
      token,
      app_code: selecteduserapp?.app_code,
      profile_id: selecteduserapp?.profile_id,
      amount: parseFloat(withdrawAmount),
    };
    // console.log('Post data 1:\n', postData);
    // Axios.post(`${GX_API_ENDPOINT}/coin/vault/gxb/balance/withdraw`, postData)
    Axios.post(
      `https://comms.globalxchange.io/coin/vault/gxb/balance/withdraw`,
      postData,
    )
      .then((resp) => {
        const {data} = resp;

        console.log('onWithdraw', data);

        // setIsLoading(false);

        if (data.status) {
          updateWalletBalances();
          updateBrokerBalances();
          // handleBrokerBalance();

          setIsCompleted(true);
          // setIsLoading(false);

          // setIsCompleted(true);
        } else {
          setIsLoading(false);
          setIsBottomSheetOpen(false);
          WToast.show({
            data: data.message || 'Error on Withdrawing',
            position: WToast.position.TOP,
          });
        }
      })
      .catch((error) => {
        setIsLoading(true);
        WToast.show({
          data: 'Error on Withdrawing',
          position: WToast.position.TOP,
        });
        console.log('Error on withdraw', error);
      });
  };

  return (
    <BottomSheetLayout
      isOpen={isBottomSheetOpen}
      onClose={() => {
        setIsBottomSheetOpen(false);
      }}>
      {isCompleted ? (
        <CompleteView
          // apptitle={selecteduserapp?.app_name}
          onClose={() => setIsBottomSheetOpen(false)}
          withdrawAmount={withdrawAmount}
          withdrawableBalance={withdrawableBalance}
          // gxbalance={gxbalance}
          setIsLoading={setIsLoading}
          selecteduserapp={selecteduserapp}
          isLoading={isLoading}
          isBottomSheetOpen={isBottomSheetOpen}
          setIsBottomSheetOpen={setIsBottomSheetOpen}
        />
      ) : wheresend ? (
        <WithdrawSelection
          setIsLoading={setIsLoading}
          setSelectedUserApp={setSelectedUserApp}
        />
      ) : (
        <InputFrom
          setWithdrawAmount={setWithdrawAmount}
          withdrawAmount={withdrawAmount}
          withdrawableBalance={withdrawableBalance}
          onWithdraw={onWithdraw}
          setWhereSend={setWhereSend}
        />
      )}
      {isLoading && (
        <View style={[styles.loadingContainer, StyleSheet.absoluteFill]}>
          <Loader apptitle={selecteduserapp?.app_name} />
        </View>
      )}
    </BottomSheetLayout>
  );
};

export default WithdrawBottomSheet;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    overflow: 'hidden',
    // height: height * 0.65,
  },
  loadingContainer: {
    zIndex: 5,
    backgroundColor: 'white',
    justifyContent: 'center',
  },
});
