import {
  Dimensions,
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import axios from 'axios';
import AsyncStorageHelper from '../../../../../utils/AsyncStorageHelper';

export default function WithdrawSelection({setIsLoading, setSelectedUserApp}) {
  const [appsdata, setAppsData] = useState([]);

  const getApps = async () => {
    try {
      // setIsLoading(true);
      const email = await AsyncStorageHelper.getLoginEmail();

      const res = await axios.get(
        `https://comms.globalxchange.io/gxb/apps/registered/user?email=${email}`,
      );

      setAppsData(res.data.userApps);

      // setIsLoading(false);
    } catch (error) {
      console.error(error);
    }
  };
  useEffect(() => {
    getApps();
  }, []);
  const appCard = ({item}) => {
    // console.log(item.app_name);
    return (
      <TouchableOpacity
        onPress={() => {
          setSelectedUserApp(item);
          // console.log(item);
        }}>
        <View style={styles.cardBox}>
          <Image
            source={{
              uri: item.app_icon,
            }}
            resizeMode="contain"
            style={styles.cardIcon}
          />
          <Text style={styles.cardContent}>{item.app_name}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View
      style={{
        height: 0.7 * Dimensions.get('screen').height,
      }}>
      <View style={styles.viewContainer}>
        <Image
          source={require('../../../../../assets/affliate-bank-icon-full_blue.png')}
          resizeMode="contain"
          style={styles.headerIcon}
        />
        <Text style={styles.subHeader}>
          Where Do You Want To Send The Withdrawal?
        </Text>

        <FlatList
          showsVerticalScrollIndicator={false}
          data={appsdata}
          keyExtractor={(item) => item.app_code}
          renderItem={appCard}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  headerIcon: {
    height: 52.45,
    width: 253.45,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  viewContainer: {
    paddingHorizontal: 32,
    paddingTop: 32,
    paddingBottom: 75,
  },
  subHeader: {
    color: '#001D41',
    marginTop: 61,
    marginBottom: 34,
    fontSize: 12,
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'left',
    fontWeight: '600',
  },
  cardBox: {
    width: '100%',
    height: 74,
    backgroundColor: 'white',
    borderColor: '#E5E5E5',
    borderWidth: 0.5,
    flexDirection: 'row',
    // justifyContent:"flex-start"
    alignItems: 'center',
    paddingLeft: 19,
    borderRadius: 15,
    marginBottom: 18,
  },
  cardIcon: {
    width: 24,
    height: 24,
    marginRight: 6,
  },
  cardContent: {
    fontFamily: 'Montserrat-Bold',
    // fontWeight: '700',
    fontSize: 15,
  },
});
