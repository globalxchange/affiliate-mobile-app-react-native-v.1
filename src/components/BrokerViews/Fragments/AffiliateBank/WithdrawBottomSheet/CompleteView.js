import React, {useContext, useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import {usdValueFormatter} from '../../../../../utils';
import {AppContext} from '../../../../../contexts/AppContextProvider';
import * as WebBrowser from 'expo-web-browser';
import AsyncStorageHelper from '../../../../../utils/AsyncStorageHelper';
import axios from 'axios';

const CompleteView = ({
  withdrawAmount,
  withdrawableBalance,
  onClose,

  isLoading,
  setIsLoading,
  selecteduserapp,
  isBottomSheetOpen,
}) => {
  const {walletBalances, cryptoTableData} = useContext(AppContext);
  const [coinvalue, setCoinValue] = useState('');
  const [change, setChange] = useState(false);
  const [gxbalance, setGxBalance] = useState('');

  let usdBalance = 0;
  let usdCoin = '';

  useEffect(() => {
    if (!isBottomSheetOpen) {
      setCoinValue('');
      setGxBalance('');
    }
  }, [isBottomSheetOpen]);

  if (walletBalances) {
    usdBalance = walletBalances.usd_balance || 0;
  }

  useEffect(() => {
    setIsLoading(true);
    handleBrokerBalance();
    handleCoinValue();
  }, []);
  useEffect(() => {
    if (gxbalance !== '' && coinvalue !== '') {
      setIsLoading(false);
      setChange(true);
    } else {
      setIsLoading(true);
      console.log('working.... in loading false');
    }
  }, [gxbalance, coinvalue]);

  if (cryptoTableData) {
    usdCoin = cryptoTableData.find((item) => item.coinSymbol === 'USD') || '';
  }

  const handleCoinValue = async () => {
    try {
      const email = await AsyncStorageHelper.getLoginEmail();

      const postData = {
        app_code: selecteduserapp?.app_code,
        email,
        include_coins: ['USD'],
      };
      const res = await axios.post(
        'https://comms.globalxchange.io/coin/vault/service/coins/get',
        postData,
      );
      console.log('coind data:\n', res?.data?.coins_data[0]?.coinValue);
      setCoinValue(res?.data?.coins_data[0]?.coinValue);
    } catch (error) {
      onClose();
      WToast.show({
        data: 'Error on GettingCoinValue',
        position: WToast.position.TOP,
      });
      console.log('Error on GettingCoinValue', error);
    }
  };

  const handleBrokerBalance = async () => {
    try {
      const email = await AsyncStorageHelper.getLoginEmail();

      const res = await axios.get(
        `https://comms.globalxchange.io/coin/vault/gxb/balance?email=${email}`,
      );
      console.log('balance:\t', res?.data?.gxbroker_balance);
      setGxBalance(res?.data?.gxbroker_balance);
    } catch (error) {
      onClose();
      WToast.show({
        data: 'Error on GettingGXbalance',
        position: WToast.position.TOP,
      });
      console.log('Error on GettingGXbalance', error);
    }
  };
  return (
    <View
      style={{
        height: !change ? 0.7 * Dimensions.get('screen').height : 'auto',
      }}>
      <View style={styles.container}>
        <View
          style={{
            display: 'flex',
            alignItems: 'center',
          }}>
          <Image
            style={styles.headerImage}
            resizeMode="contain"
            source={require('../../../../../assets/affliate-bank-icon-full_blue.png')}
          />
        </View>
        <Text
          style={{
            color: '#001D41',
            fontSize: 22,
            fontFamily: 'Montserrat-Bold',
            marginBottom: 11,
          }}>
          Congratulations
        </Text>
        <Text style={styles.subHeader}>
          Your Have Successfully Withdrew{' '}
          {usdValueFormatter.format(withdrawAmount)} USD From Your AffiliateBank
          Balance To {selecteduserapp?.app_name}.
        </Text>
        <View style={styles.valuesContainer}>
          <View style={styles.valueItem}>
            <Text style={styles.title}>Update AffiliateBank Balance</Text>
            <View style={styles.inputContainer}>
              <Text style={styles.textInput}>
                {/* {usdValueFormatter.format(withdrawableBalance)} */}
                {gxbalance}
              </Text>
              <View style={styles.unitContainer}>
                {/* <Image
                source={{uri: usdCoin.coinImage}}
                style={styles.unitImage}
                resizeMode="cover"
              /> */}
                <Text style={[styles.textInput, {textAlign: 'right'}]}>
                  {/* {usdValueFormatter.format(withdrawableBalance)} */}
                  USD
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.valueItem}>
            <Text style={styles.title}>
              Updated {selecteduserapp?.app_name}
            </Text>
            <View style={styles.inputContainer}>
              <Text style={styles.textInput}>
                {/* {usdValueFormatter.format(usdBalance)} */}
                {coinvalue}
              </Text>
              <View style={styles.unitContainer}>
                <Text style={[styles.textInput, {textAlign: 'right'}]}>
                  {/* {usdValueFormatter.format(withdrawableBalance)} */}
                  USD
                </Text>
              </View>
            </View>
          </View>
        </View>
        {/* <View style={styles.actionContainer}>
        <ScrollView
          style={styles.scrollView}
          horizontal
          showsHorizontalScrollIndicator={false}>
          <TouchableOpacity style={[styles.action, {marginLeft: 10}]}>
            <Image
              style={styles.actionIcon}
              source={require('../../../../../assets/app-logo.png')}
              resizeMode="contain"
            />
            <Text style={styles.actionText}>Transaction Audit</Text>
          </TouchableOpacity>
          <TouchableOpacity style={[styles.action, {marginLeft: 25}]}>
            <Image
              style={styles.actionIcon}
              source={require('../../../../../assets/iced-icon.png')}
              resizeMode="contain"
            />
            <Text style={[styles.actionText, styles.actionTextBlack]}>
              Earn More On USD
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.action, {marginLeft: 25, marginRight: 50}]}
            onPress={() =>
              WebBrowser.openBrowserAsync('https://cryptolottery.com/')
            }>
            <Image
              style={styles.actionIcon}
              source={require('../../../../../assets/lottery-icon.png')}
              resizeMode="contain"
            />
            <Text style={[styles.actionText, styles.actionTextBlack]}>
              Play Lottery
            </Text>
          </TouchableOpacity>
        </ScrollView>
      </View> */}
        <TouchableOpacity onPress={onClose || null} style={styles.closeButton}>
          <Text style={styles.closeText}>Close</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default CompleteView;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    backgroundColor: 'white',
    paddingHorizontal: 30,
    marginTop: 35,
    marginBottom: 28,
  },
  headerImage: {
    height: 50,
    width: 253.45,
    marginBottom: 52.45,
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 10,
    fontSize: 26,
    fontFamily: 'Montserrat-Bold',
  },
  subHeader: {
    color: '#001D41',
    textAlign: 'left',
    marginBottom: 48,
    fontSize: 12,
    fontFamily: 'Montserrat',
    lineHeight: 25,
    fontWeight: '400',
  },
  valuesContainer: {
    marginVertical: 0,
  },
  actionContainer: {
    flexDirection: 'row',
    marginRight: -40,
  },
  scrollView: {
    marginHorizontal: -10,
  },
  action: {
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    backgroundColor: 'white',
    borderRadius: 8,
    paddingVertical: 20,
    paddingHorizontal: 30,
    flex: 1,
    marginVertical: 10,
  },
  actionIcon: {
    width: 35,
    height: 35,
  },
  actionText: {
    marginTop: 10,
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    textAlign: 'center',
    fontSize: 12,
  },
  actionTextBlack: {
    color: '#464B4E',
  },
  valueItem: {marginBottom: 37},
  title: {
    color: '#001D41',
    marginBottom: 15,
    fontSize: 13,
    fontFamily: 'Montserrat-SemiBold',
    // lineHeight:16
    // fontWeight: '600',
  },
  inputContainer: {
    flexDirection: 'row',
    borderWidth: 0.5,
    borderRadius: 15,
    borderColor: '#E5E5E5',
    height: 74,
    alignItems: 'center',
    paddingHorizontal: 25,
  },
  textInput: {
    flexGrow: 1,
    width: 0,
    textAlign: 'left',
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
    // paddingHorizontal: 20,

    color: '#001D41',
  },
  unitContainer: {
    width: '45%',
    flexDirection: 'row',
    alignItems: 'center',
    // borderLeftColor: '#EBEBEB',
    // borderLeftWidth: 2,
    justifyContent: 'flex-end',
  },
  unitValue: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
  },
  unitImage: {
    height: 24,
    width: 24,
    borderRadius: 12,
    marginLeft: 15,
  },
  closeButton: {
    backgroundColor: '#001D41',
    justifyContent: 'center',
    alignItems: 'center',
    height: 68,
    // marginHorizontal: -30,
    // marginBottom: -35,
    marginTop: 0,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: '#001D41',
  },
  closeText: {
    color: 'white',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    // lineHeight: 22,
  },
});
