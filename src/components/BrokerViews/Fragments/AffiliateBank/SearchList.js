/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import LoadingAnimation from '../../../LoadingAnimation';
import InterestEarningItem from './InterestEarningItem';
import TransactionItem from './TransactionItem';

const SearchList = ({
  transactionList,
  setIsSearchFocused,
  selectedFilter,
  isFullScreen,
  setIsFullScreen,
  txnType,
}) => {
  const [searchInput, setSearchInput] = useState('');
  const [filteredList, setFilteredList] = useState();

  useEffect(() => {
    if (transactionList) {
      const searchQuery = searchInput.trim().toLowerCase();

      const filtered = transactionList.filter(
        (x) =>
          x.txn?.name?.toLowerCase()?.includes(searchQuery) ||
          x.txn?.txn_name?.toLowerCase()?.includes(searchQuery) ||
          x.txn?.email?.toLowerCase()?.includes(searchQuery),
      );
      setFilteredList(filtered);
    }
  }, [searchInput, transactionList]);

  return (
    <View style={styles.container}>
      {isFullScreen ? (
        <View style={styles.fullScreenHeader}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={styles.fullScreenTitle}>
              {txnType?.title || 'Trading Revenue'}
            </Text>
            <TouchableOpacity
              onPress={() => {
                setIsSearchFocused(true);
                setIsFullScreen(false);
              }}>
              <Image
                style={[styles.searchIcon, {padding: 10}]}
                source={require('../../../../assets/search-icon.png')}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
          <Text
            onPress={() => {
              setIsFullScreen(false);
              setIsSearchFocused(false);
            }}
            style={styles.backText}>
            Back
          </Text>
        </View>
      ) : (
        <View style={styles.searchContainer}>
          <TouchableOpacity onPress={() => setIsSearchFocused(false)}>
            <Image
              style={styles.countryIcon}
              source={require('../../../../assets/back-arrow.png')}
              resizeMode="contain"
            />
          </TouchableOpacity>
          <View style={styles.separator} />
          <TextInput
            style={styles.searchInput}
            onChangeText={(text) => setSearchInput(text)}
            value={searchInput}
            autoFocus
            placeholder={'Search Transaction'}
            placeholderTextColor={'#878788'}
          />
          <Image
            style={styles.searchIcon}
            source={require('../../../../assets/search-icon.png')}
            resizeMode="contain"
          />
        </View>
      )}
      {transactionList ? (
        <View style={styles.listContainer}>
          {filteredList?.length > 0 ? (
            <FlatList
              showsVerticalScrollIndicator={false}
              data={filteredList}
              keyExtractor={(item, index) => item._id || index}
              renderItem={({item}) => (
                <>
                  {txnType.key === 'IN' ? (
                    <InterestEarningItem
                      item={item}
                      selectedFilter={selectedFilter.key}
                    />
                  ) : (
                    <TransactionItem
                      selectedFilter={selectedFilter.key}
                      item={item}
                    />
                  )}
                </>
              )}
            />
          ) : (
            <View
              style={[
                {
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                },
              ]}>
              <Text style={styles.emptyText}>No Transactions Found</Text>
            </View>
          )}
        </View>
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default SearchList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: -20,
  },
  searchContainer: {
    flexDirection: 'row',
    marginTop: 40,
    height: 45,
    paddingHorizontal: 15,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.15,
    shadowRadius: 2,
    backgroundColor: 'white',
    borderRadius: 2,
    elevation: 12,
    alignItems: 'center',
  },
  countryIcon: {
    height: 22,
    width: 22,
    marginTop: 'auto',
    marginBottom: 'auto',
    marginRight: 15,
  },
  separator: {
    backgroundColor: '#CACACA',
    width: 1,
    marginVertical: 8,
  },
  searchInput: {
    flexGrow: 1,
    width: 0,
    marginTop: 'auto',
    marginBottom: 'auto',
    paddingHorizontal: 15,
    fontFamily: 'Montserrat',
    fontWeight: 'normal',
    color: 'black',
  },
  searchIcon: {
    height: 15,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  emptyText: {
    color: '#001D41',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
  },
  listContainer: {
    flex: 1,
    marginBottom: 20,
  },
  fullScreenHeader: {
    marginTop: 40,
  },
  fullScreenTitle: {
    flex: 1,
    marginRight: 15,
    color: '#001D41',
    fontFamily: 'Montserrat-Bold',
    fontSize: 17,
  },
  backText: {
    paddingVertical: 8,
    color: '#001D41',
    fontFamily: 'Montserrat',
    textDecorationLine: 'underline',
  },
});
