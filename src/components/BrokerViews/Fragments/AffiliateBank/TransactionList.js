/* eslint-disable react-native/no-inline-styles */
import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {GX_API_ENDPOINT} from '../../../../configs';
import AsyncStorageHelper from '../../../../utils/AsyncStorageHelper';
import LoadingAnimation from '../../../LoadingAnimation';
import SkeltonItem from '../../../SkeltonItem';
import FilterMenu from './FilterMenu';
import InterestEarningItem from './InterestEarningItem';
import SearchList from './SearchList';
import TransactionItem from './TransactionItem';

const TransactionList = ({
  isSearchFocused,
  setIsSearchFocused,
  isFullScreen,
  setIsFullScreen,
}) => {
  const [isMenuOpened, setIsMenuOpened] = useState(false);
  const [selectedFilter, setSelectedFilter] = useState({
    title: 'All',
    key: 'All',
  });
  const [isTxnTypeFilterOpen, setIsTxnTypeFilterOpen] = useState(false);
  const [txnType, setTxnType] = useState('');
  const [otcTxns, setOtcTxns] = useState();
  const [tokenTxns, setTokenTxns] = useState();
  const [digitalTxns, setDigitalTxns] = useState();
  const [licenseTxns, setLicenseTxns] = useState();
  const [activeList, setActiveList] = useState();
  const [interestList, setInterestList] = useState();

  useEffect(() => {
    (async () => {
      const email = await AsyncStorageHelper.getLoginEmail();
      // const email = 'manichandra.teja@gmail.com';

      Axios.get(`${GX_API_ENDPOINT}/brokerage/otc/txn/stats/get`, {
        params: {email},
      })
        .then((resp) => {
          const {data} = resp;

          // console.log('Transaction List', data);

          if (data.status) {
            setOtcTxns(data.logs || []);
          } else {
            setOtcTxns([]);
          }
        })
        .catch((error) => {
          console.log('Error getting Transaction', error);
        });

      Axios.get(`${GX_API_ENDPOINT}/brokerage/stats/token_txns`, {
        params: {email},
      })
        .then((resp) => {
          const {data} = resp;

          // console.log('Transaction List', data);

          setTokenTxns(data.all || []);
        })
        .catch((error) => {
          console.log('Error getting Transaction', error);
        });

      Axios.get(`${GX_API_ENDPOINT}/brokerage/stats/digital_txns`, {
        params: {email},
      })
        .then((resp) => {
          const {data} = resp;

          // console.log('Transaction List', data);

          setDigitalTxns(data.all || []);
        })
        .catch((error) => {
          console.log('Error getting Transaction', error);
        });

      Axios.get(`${GX_API_ENDPOINT}/brokerage/interest/comm/txns/get`, {
        params: {email},
      })
        .then((resp) => {
          const {data} = resp;

          // console.log('Transaction List', data);

          setInterestList(data?.logsData?.logs || []);
        })
        .catch((error) => {
          console.log('Error getting Transaction', error);
        });

      Axios.get(`${GX_API_ENDPOINT}/brokerage/comp4/txn/stats/get`, {
        params: {email},
      })
        .then((resp) => {
          const {data} = resp;

          // console.log('Transaction List', data);

          setLicenseTxns(data.logs || []);
        })
        .catch((error) => {
          console.log('Error getting Transaction', error);
        });
    })();
  }, []);

  useEffect(() => {
    switch (txnType.key) {
      case 'TR':
        setActiveList(otcTxns);
        break;
      case 'TS':
        setActiveList(tokenTxns);
        break;
      case 'LS':
        setActiveList(licenseTxns);
        break;
      case 'MM':
        setActiveList(digitalTxns);
        break;
      case 'IN':
        setActiveList(interestList);
        break;
      default:
        setActiveList(otcTxns);
        break;
    }
  }, [txnType, otcTxns, tokenTxns, digitalTxns, licenseTxns, interestList]);

  if (isSearchFocused || isFullScreen) {
    return (
      <SearchList
        transactionList={activeList}
        setIsSearchFocused={setIsSearchFocused}
        selectedFilter={selectedFilter}
        isFullScreen={isFullScreen}
        setIsFullScreen={setIsFullScreen}
        txnType={txnType}
      />
    );
  }

  return (
    <View style={[styles.container]}>
      <View style={[styles.headerContainer]}>
        <Text style={styles.header}>Latest Transactions</Text>
        <Text
          onPress={() => setIsFullScreen(true)}
          style={[styles.header, {opacity: 0.3}]}>
          FullScreen
        </Text>
      </View>
      {activeList ? (
        <View style={styles.listContainer}>
          <View style={styles.searchContainer}>
            <Text
              style={styles.searchInput}
              onPress={() => setIsSearchFocused(true)}>
              Search Transactions...
            </Text>
            <TouchableOpacity
              onPress={() => {
                setIsTxnTypeFilterOpen(true);
                setIsMenuOpened(!isMenuOpened);
              }}
              style={styles.filterItem}>
              <Text style={styles.filterText}>{txnType?.key || 'TR'}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setIsTxnTypeFilterOpen(false);
                setIsMenuOpened(!isMenuOpened);
              }}
              style={styles.filterItem}>
              <Text style={styles.filterText}>
                {selectedFilter.key || selectedFilter.tittle || 'All'}
              </Text>
            </TouchableOpacity>
          </View>
          {isMenuOpened ? (
            <FilterMenu
              selectedFilter={selectedFilter}
              setSelectedFilter={setSelectedFilter}
              isTxnType={isTxnTypeFilterOpen}
              onClose={() => setIsMenuOpened(false)}
              setTxnType={setTxnType}
            />
          ) : activeList.length > 0 ? (
            <FlatList
              showsVerticalScrollIndicator={false}
              data={activeList}
              keyExtractor={(item, index) => item._id || index.toString()}
              renderItem={({item}) => (
                <>
                  {txnType.key === 'IN' ? (
                    <InterestEarningItem
                      item={item}
                      selectedFilter={selectedFilter.key}
                    />
                  ) : (
                    <TransactionItem
                      txnType={txnType}
                      selectedFilter={selectedFilter.key}
                      item={item}
                    />
                  )}
                </>
              )}
            />
          ) : (
            <View
              style={[
                {
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                },
              ]}>
              <Text style={styles.emptyText}>No Transactions Found</Text>
            </View>
          )}
        </View>
      ) : (
        <SkeltonAnimation />
      )}
    </View>
  );
};

const SkeltonAnimation = () => (
  <View style={styles.listContainer}>
    <View style={styles.searchContainer}>
      <SkeltonItem
        style={[styles.searchInput, {marginHorizotal: 10}]}
        itemHeight={10}
        itemWidth={20}
      />
      <View style={styles.filterItem}>
        <SkeltonItem style={styles.filterText} itemHeight={10} itemWidth={20} />
      </View>
      <View style={styles.filterItem}>
        <SkeltonItem style={styles.filterText} itemHeight={10} itemWidth={20} />
      </View>
    </View>
    <>
      {Array(5)
        .fill(1)
        .map((_, index) => (
          <View style={styles.itemContainer}>
            <View style={styles.itemImageContainer}>
              <SkeltonItem itemHeight={35} itemWidth={35} />
            </View>
            <View style={styles.itemDetailsContainer}>
              <View style={styles.itemDetails}>
                <SkeltonItem itemHeight={20} itemWidth={100} />
                <SkeltonItem itemHeight={10} itemWidth={80} />
              </View>
              <SkeltonItem itemHeight={10} itemWidth={50} />
            </View>
          </View>
        ))}
    </>
  </View>
);

export default TransactionList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 40,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  header: {
    color: '#001D41',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  listContainer: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingBottom: 20,
    marginTop: 20,
    flex: 1,
  },
  emptyText: {
    color: '#001D41',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
  },
  searchContainer: {
    flexDirection: 'row',
    borderBottomColor: '#EBEBEB',
    borderBottomWidth: 1,
    marginHorizontal: -20,
    height: 40,
    alignItems: 'center',
  },
  searchInput: {
    flex: 1,
    paddingHorizontal: 20,
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
  filterItem: {
    justifyContent: 'center',
    borderLeftColor: '#EBEBEB',
    borderLeftWidth: 1,
    width: 60,
    alignItems: 'center',
    height: '100%',
  },
  filterText: {
    color: '#001D41',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 25,
  },
  itemDetailsContainer: {
    flex: 1,
    marginLeft: 10,
  },
  itemDetails: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
