import React, {useContext} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {AppContext} from '../../../../contexts/AppContextProvider';
import Moment from 'moment-timezone';
import {usdValueFormatter} from '../../../../utils';

const TransactionItem = ({item, selectedFilter, txnType}) => {
  const {cryptoTableData} = useContext(AppContext);

  let coinData;

  if (cryptoTableData) {
    coinData = cryptoTableData.find(
      (x) =>
        x.coinSymbol ===
        (item?.txn?.amount_coin ||
          item?.txn?.purchased ||
          item?.txn?.debit_data?.coin ||
          item?.txn?.coin_to_debit ||
          item?.txn?.coin ||
          item?.txn?.paymentOption ||
          'USD'),
    );
  }

  const date = `${Moment.unix(item?.txn?.timestamp / 1000)
    .tz('America/New_York')
    .format('MMM Do YYYY h:mm A')} EST`;

  let value = 0;

  switch (selectedFilter) {
    case 'All':
      value =
        (item?.commissions?.it_commission || 0) +
        (item?.commissions?.dt_commission || 0);
      break;
    case 'DB':
      value = item?.commissions?.dt_commission || 0;
      break;
    case 'BC':
      value = item?.commissions?.it_commission || 0;
      break;
    case 'DBD':
      value = item?.commissions?.bd_commission || 0;
      break;
    case 'BDC':
      value = item?.commissions?.bd_v || 0;
      break;
    default:
      value =
        (item?.commissions?.it_commission || 0) +
        (item?.commissions?.dt_commission || 0);
  }

  if (txnType === 'LS') {
    value = item?.commissions?.com_credited || 0;
  }

  return (
    <View style={styles.itemContainer}>
      <View style={styles.itemImageContainer}>
        <Image
          source={{uri: coinData?.coinImage}}
          resizeMode="cover"
          style={styles.itemImage}
        />
      </View>
      <View style={styles.itemDetailsContainer}>
        <View style={styles.itemDetails}>
          <Text style={styles.txnName}>
            {item?.txn?.name?.split(' ')[0] || item?.txn?.txn_name || ''}
          </Text>
          <Text style={styles.txnValue}>{`${usdValueFormatter.format(
            value,
          )}`}</Text>
        </View>
        <Text style={styles.txnAppName}>{date}</Text>
      </View>
    </View>
  );
};

export default TransactionItem;

const styles = StyleSheet.create({
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 25,
  },
  itemImageContainer: {},
  itemImage: {
    width: 35,
    height: 35,
  },
  itemDetailsContainer: {
    flex: 1,
    marginLeft: 10,
  },
  itemDetails: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  txnName: {
    color: '#464B4E',
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
    marginBottom: 5,
  },
  txnValue: {
    color: '#464B4E',
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
  },
  txnAppName: {
    color: '#9EA1AD',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 11,
  },
});
