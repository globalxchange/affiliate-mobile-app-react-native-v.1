/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useContext, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Dimensions,
  ScrollView,
} from 'react-native';
import WithdrawBottomSheet from './WithdrawBottomSheet';
import {usdValueFormatter} from '../../../../utils';
import {useNavigation} from '@react-navigation/native';
import {AppContext} from '../../../../contexts/AppContextProvider';
import TransactionList from './TransactionList';

const {width} = Dimensions.get('window');

const AffiliateBank = () => {
  const {
    affiliateBalance,
    withdrawableAffiliateBalance,
    updateBrokerBalances,
  } = useContext(AppContext);

  const {navigate} = useNavigation();

  const [isWithdrawOpen, setIsWithdrawOpen] = useState(false);
  const [isShowTotalBalance, setIsShowTotalBalance] = useState(false);
  const [isSearchFocused, setIsSearchFocused] = useState(false);
  const [isFullScreen, setIsFullScreen] = useState(false);

  useEffect(() => {
    if (affiliateBalance === '' || withdrawableAffiliateBalance === '') {
      updateBrokerBalances();
    }
  }, []);

  return (
    <View style={styles.scrollContainer}>
      <View style={styles.container}>
        <View
          style={{display: isSearchFocused || isFullScreen ? 'none' : 'flex'}}>
          <View style={styles.headerContainer}>
            <Text
              onPress={() => setIsShowTotalBalance(false)}
              style={[styles.headerText, isShowTotalBalance && {opacity: 0.5}]}>
              Affiliate Balance
            </Text>
            <Text
              onPress={() => setIsShowTotalBalance(true)}
              style={[styles.headerText, isShowTotalBalance || {opacity: 0.5}]}>
              Total Balance
            </Text>
          </View>
          <View style={styles.withdrawContainer}>
            <Text style={styles.withdrawBalance}>
              {isShowTotalBalance
                ? affiliateBalance === ''
                  ? 'Calculating'
                  : usdValueFormatter.format(affiliateBalance)
                : withdrawableAffiliateBalance === ''
                ? 'Calculating'
                : usdValueFormatter.format(withdrawableAffiliateBalance)}
            </Text>
            <TouchableOpacity
              style={styles.withdrawButton}
              onPress={() => setIsWithdrawOpen(true)}>
              <Text style={styles.withdrawButtonText}>Withdraw</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.analyticsContainer}>
            <Text style={[styles.headerText]}>Advanced Analysis</Text>
          </View>
          <View>
            <ScrollView
              horizontal
              showsHorizontalScrollIndicator={false}
              style={styles.actionsContainer}>
              <TouchableOpacity
                onPress={() => navigate('BrokerEarning', {type: 'Direct'})}
                style={styles.controlContainer}>
                <Image
                  source={require('../../../../assets/direct-earning-icon.png')}
                  style={styles.controlIcon}
                  resizeMode="contain"
                />
                <Text style={styles.controlName}>Direct</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => navigate('BrokerEarning', {type: 'Indirect'})}
                style={styles.controlContainer}>
                <Image
                  source={require('../../../../assets/indirect-earning-icon.png')}
                  style={styles.controlIcon}
                  resizeMode="contain"
                />
                <Text style={styles.controlName}>Indirect</Text>
              </TouchableOpacity>
              <View style={[styles.controlContainer, {opacity: 0.4}]}>
                <Image
                  source={require('../../../../assets/direct-earning-icon.png')}
                  style={styles.controlIcon}
                  resizeMode="contain"
                />
                <Text style={styles.controlName}>Custom</Text>
              </View>
              <TouchableOpacity
                style={[styles.controlContainer]}
                onPress={() => navigate('CouncilOverrides')}>
                <Image
                  source={require('../../../../assets/direct-earning-icon.png')}
                  style={styles.controlIcon}
                  resizeMode="contain"
                />
                <Text style={styles.controlName}>Council Overrides</Text>
              </TouchableOpacity>
            </ScrollView>
          </View>
        </View>
        <TransactionList
          isSearchFocused={isSearchFocused}
          setIsSearchFocused={setIsSearchFocused}
          isFullScreen={isFullScreen}
          setIsFullScreen={setIsFullScreen}
        />
      </View>
      <WithdrawBottomSheet
        isBottomSheetOpen={isWithdrawOpen}
        setIsBottomSheetOpen={setIsWithdrawOpen}
        updateBrokerBalances={updateBrokerBalances}
        withdrawableBalance={withdrawableAffiliateBalance}
      />
    </View>
  );
};

export default AffiliateBank;

const styles = StyleSheet.create({
  scrollContainer: {
    flex: 1,
    backgroundColor: 'white',
  },
  container: {
    flex: 1,
    paddingTop: 20,
    paddingHorizontal: 30,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 14,
  },
  withdrawContainer: {
    flexDirection: 'row',
    marginTop: 15,
    backgroundColor: 'white',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 20,
    borderColor: '#EBEBEB',
    borderWidth: 1,
  },
  withdrawBalance: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 20,
  },
  withdrawButton: {
    borderColor: '#08152D',
    borderWidth: 1,
    paddingVertical: 5,
    paddingHorizontal: 15,
  },
  withdrawButtonText: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 11,
  },
  controlContainer: {
    flexDirection: 'row',
    marginTop: 10,
    backgroundColor: 'white',
    alignItems: 'center',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    width: (width - 40 - 35) / 2,
    marginRight: 15,
    justifyContent: 'center',
    height: 40,
  },
  controlIcon: {
    width: 18,
    height: 18,
  },
  controlName: {
    marginLeft: 10,
    fontFamily: 'Montserrat-SemiBold',
    color: '#001D41',
    fontSize: 12,
  },
  buttonOutlined: {
    borderColor: '#001D41',
    borderWidth: 1,
    paddingVertical: 3,
    paddingHorizontal: 12,
  },
  buttonOutlinedText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#001D41',
    opacity: 0.5,
    fontSize: 11,
  },
  buttonFilled: {
    backgroundColor: '#08152D',
    paddingVertical: 3,
    paddingHorizontal: 12,
    marginLeft: 10,
  },
  buttonFilledText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
    fontSize: 11,
  },
  analyticsContainer: {
    marginTop: 35,
  },
  actionsContainer: {
    flexDirection: 'row',
  },
});
