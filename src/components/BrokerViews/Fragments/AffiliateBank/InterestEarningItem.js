import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {usdValueFormatter} from '../../../../utils';

const InterestEarningItem = ({item, selectedFilter}) => {
  let value = 0;

  switch (selectedFilter) {
    case 'All':
      value =
        (item?.commissions?.it_commission || 0) +
        (item?.commissions?.dt_commission || 0) +
        (item?.commissions?.mot_commission || 0);
      break;
    case 'DB':
      value = item?.commissions?.dt_commission || 0;
      break;
    case 'BC':
      value = item?.commissions?.it_commission || 0;
      break;
    case 'DBD':
      value = item?.commissions?.bd_commission || 0;
      break;
    case 'BDC':
      value = item?.commissions?.bd_v || 0;
      break;
    default:
      value =
        (item?.commissions?.it_commission || 0) +
        (item?.commissions?.dt_commission || 0) +
        (item?.commissions?.mot_commission || 0);
  }

  return (
    <View style={styles.itemContainer}>
      <View style={styles.itemImageContainer}>
        <Image
          source={
            item?.txn?.liquid_interest
              ? require('../../../../assets/liquid-earnings-icon.png')
              : require('../../../../assets/bonds-icon.png')
          }
          resizeMode="contain"
          style={styles.itemImage}
        />
      </View>
      <View style={styles.itemDetailsContainer}>
        <View style={styles.itemDetails}>
          <Text numberOfLines={1} style={styles.txnName}>
            {item?.txn?.email}
          </Text>
          <Text style={styles.txnValue}>{`${usdValueFormatter.format(
            value,
          )}`}</Text>
        </View>
        <Text numberOfLines={1} style={styles.txnAppName}>
          {item?.txn?.liquid_interest
            ? `Interest On ${item?.txn?.coin} Holding`
            : `${item?.txn?.coin} Bond`}
        </Text>
      </View>
    </View>
  );
};

export default InterestEarningItem;

const styles = StyleSheet.create({
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 25,
  },
  itemImageContainer: {},
  itemImage: {
    width: 30,
    height: 30,
  },
  itemDetailsContainer: {
    flex: 1,
    marginLeft: 15,
  },
  itemDetails: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  txnName: {
    color: '#464B4E',
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
    marginBottom: 5,
    flex: 1,
    marginRight: 20,
  },
  txnValue: {
    color: '#464B4E',
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
  },
  txnAppName: {
    color: '#9EA1AD',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 11,
  },
});
