/* eslint-disable react-native/no-inline-styles */
import React, {useEffect} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

const LicenseTypes = ({activeType, setActiveType}) => {
  useEffect(() => {
    setActiveType(TYPES[0]);
  }, []);

  return (
    <View style={styles.licenseTypeContainer}>
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        {TYPES.map((item) => (
          <TouchableOpacity
            key={item}
            onPress={() => setActiveType(item)}
            style={[
              styles.itemContainer,
              activeType === item && styles.itemContainerActive,
            ]}>
            <Text
              style={[styles.itemName, activeType !== item && {opacity: 0.25}]}>
              {item}
            </Text>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  );
};

export default LicenseTypes;

const styles = StyleSheet.create({
  licenseTypeContainer: {
    borderBottomColor: '#EBEBEB',
    borderBottomWidth: 1,
    marginRight: -30,
    marginTop: 40,
    marginBottom: 30,
  },
  itemContainer: {
    marginRight: 20,
    paddingBottom: 10,
  },
  itemContainerActive: {
    borderBottomColor: '#08152D',
    borderBottomWidth: 1,
  },
  itemName: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 25,
  },
});

const TYPES = ['Monthly', 'One Time', 'Staking'];
