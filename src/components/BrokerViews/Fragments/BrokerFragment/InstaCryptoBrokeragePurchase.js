import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import LicenseBuyBottomSheet from './LicenseBuyBottomSheet';
import {formatterHelper, getUriImage} from '../../../../utils';

const InstaCryptoBrokeragePurchase = ({data, onClose}) => {
  const [isPurchaseOpen, setIsPurchaseOpen] = useState(false);
  const [selectedBilling, setSelectedBilling] = useState('');

  // console.log('data', data);

  const billingCycleObj = data?.billing_cycle || {};

  let billingCycles = [];

  Object.entries(billingCycleObj).forEach(([key, value]) => {
    billingCycles.push({...value, key});
  });

  const onItemClick = (item) => {
    setSelectedBilling(item.key);
    setIsPurchaseOpen(true);
  };

  return (
    <>
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={styles.scrollContainer}>
        <View style={styles.container}>
          <TouchableOpacity onPress={onClose} style={styles.backButton}>
            <Image
              source={require('../../../../assets/back-solid-icon.png')}
              style={styles.backIcon}
              resizeMode="contain"
            />
            <Text style={styles.backButtonText}>All Products</Text>
          </TouchableOpacity>
          <View style={styles.headerContainer}>
            <View style={styles.headerImageContainer}>
              <FastImage
                source={{uri: getUriImage(data.product_icon)}}
                style={styles.headerImage}
                resizeMode="contain"
              />
            </View>
            <View style={styles.headerTitleContainer}>
              <Text style={styles.headerTitle}>{data.product_name || ''}</Text>
              <Text style={styles.headerSubtitle}>{data.sub_text || ''}</Text>
            </View>
          </View>
          <Text style={styles.desc}>{data.full_description || ''}</Text>
          <View style={styles.licenseContainer}>
            <Text style={styles.licenseHeader}>Licenses</Text>
            {billingCycles.map((item, index) => (
              <TouchableOpacity
                key={index}
                style={styles.cycleItem}
                onPress={() => onItemClick(item)}>
                <Text style={styles.cycleName}>{item.key}</Text>
                <Text style={styles.cyclePrice}>
                  {formatterHelper(item.price || 0, item.coin)} {item.coin}
                  {item.key !== 'lifetime' ? ` Per ${item.key}` : ''}
                </Text>
              </TouchableOpacity>
            ))}
          </View>
        </View>
        <LicenseBuyBottomSheet
          isBottomSheetOpen={isPurchaseOpen}
          setIsBottomSheetOpen={setIsPurchaseOpen}
          purchaseData={data}
          selectedBilling={selectedBilling}
        />
      </ScrollView>
    </>
  );
};

export default InstaCryptoBrokeragePurchase;

const styles = StyleSheet.create({
  scrollContainer: {
    flex: 1,
    backgroundColor: '#F9F9F9',
  },
  container: {
    flex: 1,
    paddingTop: 20,
    paddingBottom: 25,
    paddingHorizontal: 30,
  },
  backButton: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  backButtonText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 11,
    marginLeft: 5,
  },
  backIcon: {
    width: 10,
    height: 10,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 30,
  },
  headerImageContainer: {
    width: 55,
    height: 55,
    padding: 8,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.15,
    shadowRadius: 2,
    backgroundColor: 'white',
    elevation: 2,
  },
  headerImage: {
    flex: 1,
    width: null,
    height: null,
  },
  headerTitleContainer: {
    marginLeft: 20,
    flex: 1,
  },
  headerTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 20,
    marginBottom: 3,
  },
  headerSubtitle: {
    fontFamily: 'Montserrat',
    color: '#9A9A9A',
    fontSize: 12,
  },
  desc: {
    fontFamily: 'Montserrat',
    color: '#9A9A9A',
    fontSize: 14,
    marginBottom: 30,
  },
  licenseContainer: {},
  licenseHeader: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 20,
  },
  cycleItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 8,
    paddingVertical: 10,
    paddingHorizontal: 20,
    marginTop: 20,
  },
  cycleName: {
    flex: 1,
    fontFamily: 'Montserrat',
    color: '#08152D',
    textTransform: 'capitalize',
    fontSize: 15,
  },
  cyclePrice: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    fontSize: 12,
  },
  buttonContainer: {
    flexDirection: 'row',
  },
  buttonFilled: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    backgroundColor: '#08152D',
  },
  buttonFilledText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    backgroundColor: 'white',
  },
  buttonText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
  },
});
