import React from 'react';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import LicenseItem from './LicenseItem';

const AllLicenses = ({list, onClose, openUpgrade}) => {
  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <TouchableOpacity onPress={onClose}>
          <FastImage
            source={require('../../../../assets/back-arrow.png')}
            resizeMode="contain"
            style={styles.backArrow}
          />
        </TouchableOpacity>
        <View>
          <Text style={styles.header}>All Licenses</Text>
          <View style={styles.divider} />
        </View>
      </View>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={list}
        keyExtractor={(item, index) => `${index}`}
        renderItem={({item}) => (
          <LicenseItem item={item} openUpgrade={() => openUpgrade(item)} />
        )}
      />
    </View>
  );
};

export default AllLicenses;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 35,
    paddingTop: 30,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  backArrow: {
    width: 25,
    height: 20,
    marginRight: 20,
  },
  header: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 24,
  },
  divider: {
    backgroundColor: '#08152D',
    height: 1,
    marginRight: 20,
    marginTop: 5,
  },
});
