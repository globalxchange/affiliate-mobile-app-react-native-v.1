/* eslint-disable react-native/no-inline-styles */
import Axios from 'axios';
import React, {useState, useEffect} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {APP_CODE, GX_API_ENDPOINT} from '../../../../configs';
import ThemeData from '../../../../configs/ThemeData';
import {formatterHelper} from '../../../../utils';
import LoadingAnimation from '../../../LoadingAnimation';

const ProductItem = ({itemData, onItemPurchase, disableButton}) => {
  const [isDetailsOpen, setIsDetailsOpen] = useState(false);
  const [productDetails, setProductDetails] = useState();

  useEffect(() => {
    if (itemData) {
      const postData = {
        app_code: APP_CODE,
        product_id: itemData.product_id,
      };

      Axios.post(`${GX_API_ENDPOINT}/gxb/product/price/with/fees/get`, postData)
        .then((resp) => {
          const {data} = resp;

          // console.log('data', data);
          setProductDetails(data.product || '');
        })
        .catch((error) => {});
    }
  }, [itemData]);

  return (
    <View
      style={[
        styles.container,
        isDetailsOpen && {paddingBottom: 0},
        disableButton && {paddingBottom: 20},
      ]}>
      <TouchableOpacity
        onPress={() => setIsDetailsOpen(!isDetailsOpen)}
        style={[styles.headerContainer, disableButton || {flex: 1}]}>
        <Image
          source={{uri: itemData?.product_icon || null}}
          style={styles.itemIcon}
          resizeMode="contain"
        />
        <Text style={[styles.itemName]}>
          {itemData?.product_name || itemData.name}
        </Text>
        <Image
          source={require('../../../../assets/back-solid-icon.png')}
          style={[
            styles.dropdownIcon,
            {transform: [{rotate: isDetailsOpen ? '90deg' : '270deg'}]},
          ]}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <Text style={styles.desc}>{itemData?.sub_text}</Text>
      {isDetailsOpen && (
        <View style={styles.detailsContainer}>
          {productDetails ? (
            <>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <Text style={styles.moreTitle}>More About The License</Text>
                <Text style={[styles.moreTitle, {textDecorationLine: 'none'}]}>
                  {productDetails?.pricesWithFees
                    ? productDetails?.pricesWithFees[0]?.coin
                    : 'USD'}
                </Text>
              </View>
              <Text style={styles.fullDesc}>{itemData?.full_description}</Text>
              <View style={styles.valuesContainer}>
                <Text style={styles.valueLabel}>Issuance Fee</Text>
                <Text style={styles.valueItem}>
                  {formatterHelper(
                    productDetails?.pricesWithFees
                      ? productDetails?.pricesWithFees[0]?.price
                      : productDetails?.first_purchase?.price || 0,
                    productDetails?.pricesWithFees
                      ? productDetails?.pricesWithFees[0]?.coin
                      : productDetails?.first_purchase?.coin || 'USD',
                  )}{' '}
                  {productDetails?.pricesWithFees
                    ? productDetails?.pricesWithFees[0]?.coin
                    : productDetails?.first_purchase?.coin || 'USD'}
                </Text>
              </View>
              <View style={styles.valuesContainer}>
                <Text style={styles.valueLabel}>Renewal Fee</Text>
                <Text style={styles.valueItem}>
                  {formatterHelper(
                    productDetails?.pricesWithFees
                      ? productDetails?.pricesWithFees[1]?.price
                      : productDetails.billing_cycle?.monthly?.price || 0,
                    productDetails?.pricesWithFees
                      ? productDetails?.pricesWithFees[1]?.coin
                      : productDetails?.billing_cycle?.monthly?.coin || 'USD',
                  )}{' '}
                  {productDetails?.pricesWithFees
                    ? productDetails?.pricesWithFees[1]?.coin
                    : productDetails?.billing_cycle?.monthly?.coin || 'USD'}
                </Text>
              </View>
              <View style={styles.valuesContainer}>
                <Text style={styles.valueLabel}>Prerequisites</Text>
                <Text style={styles.valueItem}>None</Text>
              </View>
            </>
          ) : (
            <View style={styles.loadingContainer}>
              <LoadingAnimation height={60} width={60} />
            </View>
          )}
          {!disableButton && (
            <View style={styles.actionsContainer}>
              <TouchableOpacity style={styles.filledButton}>
                <Text style={styles.filledButtonText}>Earning Power</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={onItemPurchase}
                style={styles.outlinedButton}>
                <Text style={styles.outlinedButtonText}>Purchase</Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      )}
    </View>
  );
};

export default ProductItem;

const styles = StyleSheet.create({
  container: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    padding: 20,
    marginBottom: 20,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    // flex: 1,
    marginBottom: 10,
  },
  itemIcon: {
    width: 25,
    height: 25,
  },
  itemName: {
    flex: 1,
    paddingHorizontal: 15,
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 15,
  },
  dropdownIcon: {
    width: 10,
    height: 10,
    transform: [{rotate: '270deg'}],
  },
  desc: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
  },
  detailsContainer: {
    marginTop: 25,
  },
  loadingContainer: {
    marginBottom: 10,
  },
  moreTitle: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    textDecorationLine: 'underline',
    fontSize: 13,
  },
  fullDesc: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 11,
    marginTop: 15,
    marginBottom: 10,
  },
  valuesContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  valueLabel: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
  },
  valueItem: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
  },
  actionsContainer: {
    flexDirection: 'row',
    marginHorizontal: -20,
    marginTop: 20,
  },
  filledButton: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    flex: 1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  filledButtonText: {
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  outlinedButton: {
    flex: 1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopColor: '#EBEBEB',
    borderTopWidth: 1,
  },
  outlinedButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
});
