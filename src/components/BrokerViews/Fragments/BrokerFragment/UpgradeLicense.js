/* eslint-disable react-native/no-inline-styles */
import {useNavigation} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import AutoResizeText from '../../../AutoResizeText';
import LoadingAnimation from '../../../LoadingAnimation';
import LicenseTypes from './LicenseTypes';

const UpgradeLicense = ({
  onClose,
  allLicenses,
  brokerLicenceDetails,
  showUpgrade,
}) => {
  const {navigate} = useNavigation();

  const [activeType, setActiveType] = useState();
  const [parsedList, setParsedList] = useState();

  useEffect(() => {
    if (allLicenses) {
      const all = [...allLicenses];

      // console.log('all', all);
      // console.log('brokerLicenceDetails', brokerLicenceDetails);

      const activeIndex = allLicenses.findIndex(
        (x) => x.product_code === brokerLicenceDetails.product_code,
      );

      if (activeIndex > -1) {
        all.splice(activeIndex, 1);
      }

      const monthlyProducts = all.filter((item) => item.monthly === true);

      const lifeTimeProducts = all.filter((item) => item.lifetime === true);

      const stakingProducts = all.filter(
        (item) => item.staking_allowed === true,
      );

      const productsObj = {
        Monthly: monthlyProducts,
        'One Time': lifeTimeProducts,
        Staking: stakingProducts,
      };

      // console.log('productsObj', productsObj);

      setParsedList(productsObj);
    }
  }, [allLicenses, brokerLicenceDetails]);

  const itemClickHandler = (item) => {
    navigate('Support', {
      openMessage: true,
      messageData: `Hey Support,\nI want to upgrade my license ${brokerLicenceDetails.license_id} to ${item.product_name}`,
    });
  };

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <TouchableOpacity onPress={onClose}>
          <FastImage
            source={require('../../../../assets/back-arrow.png')}
            resizeMode="contain"
            style={styles.backArrow}
          />
        </TouchableOpacity>
        <View>
          <Text style={styles.header}>All Licenses</Text>
          <View style={styles.divider} />
        </View>
      </View>
      <View style={styles.currentLicenseContainer}>
        <Text style={styles.currentLicenseTitle}>Upgrading</Text>
        <View style={styles.activeLicense}>
          <Text numberOfLines={1} style={styles.licenseName}>
            {showUpgrade.license_id === 'GXBTL'
              ? 'Legacy GXBroker License'
              : showUpgrade?.name || 'InstaCrypto Brokerage'}
          </Text>
          <Image
            style={styles.checkIcon}
            resizeMode="contain"
            source={require('../../../../assets/green-check.png')}
          />
        </View>
      </View>
      <View style={styles.listContainer}>
        <Text style={styles.upgradeTitle}>
          Which One Of The Products Are You Upgrading To?
        </Text>
        <LicenseTypes activeType={activeType} setActiveType={setActiveType} />
        {parsedList ? (
          <View style={styles.productList}>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              data={parsedList[activeType || 'Monthly'] || []}
              keyExtractor={(item, index) => `${index}`}
              renderItem={({item, index}) => (
                <TouchableOpacity
                  onPress={() => itemClickHandler(item)}
                  style={[styles.productItem]}>
                  <View
                    style={[
                      styles.productTitleContainer,
                      {
                        backgroundColor:
                          index % 2 === 0 ? '#186AB4' : '#08152D',
                      },
                    ]}>
                    {item?.product_name?.split(' ').map((text, i) => (
                      <AutoResizeText key={`${text}${i}`} text={text} />
                    ))}
                  </View>
                </TouchableOpacity>
              )}
              ListEmptyComponent={
                <Text style={styles.emptyText}>
                  No Products Available For Now
                </Text>
              }
            />
          </View>
        ) : (
          <View style={styles.loadingContainer}>
            <LoadingAnimation />
          </View>
        )}
      </View>
    </View>
  );
};

export default UpgradeLicense;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 35,
    paddingTop: 30,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  backArrow: {
    width: 25,
    height: 20,
    marginRight: 20,
  },
  header: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 24,
  },
  divider: {
    backgroundColor: '#08152D',
    height: 1,
    marginRight: 20,
    marginTop: 5,
  },
  currentLicenseContainer: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 25,
  },
  currentLicenseTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 30,
  },
  activeLicense: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
  },
  checkIcon: {
    width: 20,
    height: 20,
  },
  licenseName: {
    flex: 1,
    marginRight: 10,
    color: '#08152D',
    fontFamily: 'Montserrat',
  },
  listContainer: {
    paddingHorizontal: 20,
    paddingVertical: 25,
  },
  upgradeTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat',
  },
  productList: {
    marginTop: 20,
    marginRight: -30,
  },
  productItem: {
    marginRight: 15,
  },
  productTitleContainer: {
    height: 150,
    marginBottom: 10,
    borderRadius: 8,
    justifyContent: 'center',
    paddingHorizontal: 15,
    width: 130,
  },
  productTitle: {
    color: 'white',
    fontFamily: 'Montserrat-Bold',
  },
  productPice: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 12,
    textAlign: 'center',
  },
  emptyText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
    fontSize: 18,
  },
});
