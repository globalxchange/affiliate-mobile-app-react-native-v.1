/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const PurchaseOptions = ({setActiveView, onClose, purchaseData}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.header}>Which Option Best Explains You?</Text>
      <View style={styles.optionsList}>
        {(purchaseData?.product_id === 'GXPA12-U61c3a509327fT1605139014026' ||
          purchaseData?.product_id === 'GXPA12f317fdb6b684T1602658279878') && (
          <TouchableOpacity
            onPress={() => setActiveView('LegacyReview')}
            style={styles.optionItem}>
            <Image
              style={styles.optionIcon}
              resizeMode="contain"
              source={require('../../../../../assets/gx-logo.png')}
            />
            <Text style={styles.optionText}>I Am A Legacy GXBroker</Text>
          </TouchableOpacity>
        )}
        <TouchableOpacity
          onPress={() => setActiveView('SelectCoin')}
          style={[styles.optionItem]}>
          <Image
            style={styles.optionIcon}
            resizeMode="contain"
            source={require('../../../../../assets/first-time-buy.png')}
          />
          <Text style={styles.optionText}>I Am A First Time License Buyer</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setActiveView('PurchaseUpgrade')}>
          <View style={[styles.optionItem, {opacity: 1, marginBottom: 0}]}>
            <Image
              style={styles.optionIcon}
              resizeMode="contain"
              source={require('../../../../../assets/money-padlock.png')}
            />
            <Text style={styles.optionText}>I Am Upgrading A License</Text>
          </View>
        </TouchableOpacity>
      </View>
      <TouchableOpacity style={[styles.checkoutButton]} onPress={onClose}>
        <Text style={styles.buttonText}>Close</Text>
      </TouchableOpacity>
    </View>
  );
};

export default PurchaseOptions;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 40,
    paddingHorizontal: 30,
    flex: 1,
  },
  header: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 20,
    marginBottom: 10,
  },
  optionsList: {
    flex: 1,
    justifyContent: 'center',
  },
  optionItem: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    height: 50,
    alignItems: 'center',
    marginBottom: 15,
    flexDirection: 'row',
    paddingHorizontal: 20,
  },
  optionIcon: {
    width: 25,
    height: 25,
  },
  optionText: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    flex: 1,
    marginLeft: 20,
  },
  checkoutButton: {
    backgroundColor: '#08152D',
    height: 45,
    borderRadius: 6,
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 16,
    fontFamily: 'Montserrat-SemiBold',
  },
});
