import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import VaultSelector from '../../../../VaultSelector';

const CoinPicker = ({
  coinsWithBalance,
  selectedAsset,
  onAssetSelected,
  title,
  subTitle,
  onNext,
}) => {
  const onNextClick = () => {
    if (selectedAsset) {
      onNext();
    }
  };

  return (
    <View style={styles.viewContainer}>
      <Text style={styles.headerText}>{title || 'Select Vault To Pay'}</Text>
      <Text style={styles.subText}>
        {subTitle ||
          'Select The Vault That You Would Like Debit For This Purchase'}
      </Text>
      <View style={styles.controlContainer}>
        <VaultSelector
          placeHolder="Select From Vaults"
          onItemSelect={onAssetSelected}
          selectedItem={selectedAsset}
          onNext={onNext}
        />
      </View>
      <TouchableOpacity style={[styles.checkoutButton]} onPress={onNextClick}>
        <Text style={styles.buttonText}>Proceed</Text>
      </TouchableOpacity>
    </View>
  );
};

export default CoinPicker;

const styles = StyleSheet.create({
  viewContainer: {
    paddingVertical: 40,
    paddingHorizontal: 30,
    flex: 1,
  },
  headerText: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 22,
    marginBottom: 10,
  },
  subText: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat',
    fontSize: 13,
    marginBottom: 20,
  },
  controlContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  checkoutButton: {
    marginTop: 40,
    backgroundColor: '#08152D',
    height: 45,
    borderRadius: 6,
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 16,
    fontFamily: 'Montserrat-SemiBold',
  },
});
