/* eslint-disable react-native/no-inline-styles */
import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {useRef} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {WModalShowToastView, WToast} from 'react-native-smart-tip';
import {APP_CODE, GX_API_ENDPOINT} from '../../../../../configs';
import ThemeData from '../../../../../configs/ThemeData';
import PopupLayout from '../../../../../layouts/PopupLayout';
import {formatterHelper} from '../../../../../utils';
import AsyncStorageHelper from '../../../../../utils/AsyncStorageHelper';
import LoadingAnimation from '../../../../LoadingAnimation';

const LegacyUpgrade = ({
  onClose,
  setIsLoading,
  setActiveView,
  getUserLicenses,
  onNext,
}) => {
  const [isDetailsOpen, setIsDetailsOpen] = useState(false);
  const [productDetails, setProductDetails] = useState();
  const [showDescMore, setShowDescMore] = useState(false);

  const toastRef = useRef();

  const itemData = {
    product_id: 'ICBGXT937538117da0T1600185913470',
    product_name: 'InstaCrypto Brokerage Premium',
    product_icon: '',
    sub_text: "Get paid to sell crypto's supported by GX on InstaCrypto App",
    full_description:
      "Get paid to sell crypto's supported by GX on InstaCrypto App",
    name: 'InstaCrypto Brokerage Premium',
  };

  useEffect(() => {
    if (itemData) {
      const postData = {
        app_code: APP_CODE,
        product_id: itemData.product_id,
      };

      Axios.post(`${GX_API_ENDPOINT}/gxb/product/price/with/fees/get`, postData)
        .then((resp) => {
          const {data} = resp;

          // console.log('data', data);
          setProductDetails(data.product || '');
        })
        .catch((error) => {});
    }
  }, [itemData]);

  const makeGrandFathered = async () => {
    setIsLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    Axios.post(`${GX_API_ENDPOINT}/gxb/product/upgrade/user/license`, {
      email,
      token,
      product_id: 'ICBGXT937538117da0T1600185913470',
    })
      .then((resp) => {
        const {data} = resp;

        // console.log('makeGrandFathered', data);

        if (data.status) {
          setActiveView('Completed');
          getUserLicenses();
        } else {
          WToast.show({
            data: data.message || 'Error On License Upgrade',
            position: WToast.position.TOP,
          });
        }
      })
      .catch((error) => {
        console.log('Error on legacy upgrade', error);
      })
      .finally(() => setIsLoading(false));
  };

  const descText =
    'You Will Be Granted This License Without Requirement To Pay The First Month Fee Or The Reoccurring Monthly Subscription. Your Grandfathered License Will Remain Active As Long As There Is No Interference WIth the Tokens From Your Original Staking Contract.';

  return (
    <View style={styles.container}>
      <WModalShowToastView
        toastInstance={(toastInstance) => (toastRef.current = toastInstance)}
      />
      <Text style={styles.header}>How It Works</Text>
      <Text
        style={styles.subText}
        onPress={() => setShowDescMore(!showDescMore)}>
        {descText.substring(0, 90)}
        <Text style={{fontFamily: ThemeData.FONT_BOLD}}> (Read More...)</Text>
      </Text>
      <PopupLayout
        isOpen={showDescMore}
        onClose={() => setShowDescMore(false)}
        headerImage={require('../../../../../assets/broker-checkout-icon.png')}>
        <Text style={styles.header}>How It Works</Text>
        <Text style={styles.subText}>{descText}</Text>
      </PopupLayout>
      <View style={{flex: 1, justifyContent: 'center'}}>
        <View style={[styles.productContainer]}>
          <TouchableOpacity
            onPress={() => setIsDetailsOpen(!isDetailsOpen)}
            style={[styles.product]}>
            <View style={styles.headerContainer}>
              <Image
                source={{uri: itemData?.product_icon || null}}
                style={styles.itemIcon}
                resizeMode="contain"
              />
              <Text style={styles.itemName}>InstaCrypto Brokerage Premium</Text>
            </View>
            <Text style={styles.desc}>
              {itemData?.sub_text}.{' '}
              <Text style={{fontFamily: ThemeData.FONT_BOLD}}>
                Click To Expand
              </Text>
            </Text>
          </TouchableOpacity>
          <PopupLayout
            isOpen={isDetailsOpen}
            onClose={() => setIsDetailsOpen(false)}
            headerImage={require('../../../../../assets/broker-checkout-icon.png')}>
            <View style={styles.headerContainer}>
              <Image
                source={{uri: itemData?.product_icon || null}}
                style={styles.itemIcon}
                resizeMode="contain"
              />
              <Text style={styles.itemName}>InstaCrypto Brokerage Premium</Text>
            </View>
            <Text style={styles.desc}>{itemData?.sub_text}</Text>
            <View style={styles.detailsContainer}>
              {productDetails ? (
                <>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <Text style={styles.moreTitle}>More About The License</Text>
                    <Text
                      style={[styles.moreTitle, {textDecorationLine: 'none'}]}>
                      {productDetails?.pricesWithFees
                        ? productDetails?.pricesWithFees[0]?.coin
                        : 'USD'}
                    </Text>
                  </View>
                  <Text style={styles.fullDesc}>
                    {itemData?.full_description}
                  </Text>
                  <View style={styles.valuesContainer}>
                    <Text style={styles.valueLabel}>Issuance Fee</Text>
                    <Text style={styles.valueItem}>
                      {formatterHelper(
                        productDetails?.pricesWithFees
                          ? productDetails?.pricesWithFees[0]?.price
                          : productDetails?.first_purchase?.price || 0,
                        productDetails?.pricesWithFees
                          ? productDetails?.pricesWithFees[0]?.coin
                          : productDetails?.first_purchase?.coin || 'USD',
                      )}{' '}
                      {productDetails?.pricesWithFees
                        ? productDetails?.pricesWithFees[0]?.coin
                        : productDetails?.first_purchase?.coin || 'USD'}
                    </Text>
                  </View>
                  <View style={styles.valuesContainer}>
                    <Text style={styles.valueLabel}>Renewal Fee</Text>
                    <Text style={styles.valueItem}>
                      {formatterHelper(
                        productDetails?.pricesWithFees
                          ? productDetails?.pricesWithFees[1]?.price
                          : productDetails.billing_cycle?.monthly?.price || 0,
                        productDetails?.pricesWithFees
                          ? productDetails?.pricesWithFees[1]?.coin
                          : productDetails?.billing_cycle?.monthly?.coin ||
                              'USD',
                      )}{' '}
                      {productDetails?.pricesWithFees
                        ? productDetails?.pricesWithFees[1]?.coin
                        : productDetails?.billing_cycle?.monthly?.coin || 'USD'}
                    </Text>
                  </View>
                  <View style={styles.valuesContainer}>
                    <Text style={styles.valueLabel}>Prerequisites</Text>
                    <Text style={styles.valueItem}>None</Text>
                  </View>
                </>
              ) : (
                <View style={styles.loadingContainer}>
                  <LoadingAnimation height={60} width={60} />
                </View>
              )}
            </View>
          </PopupLayout>
        </View>
      </View>
      <TouchableOpacity style={[styles.checkoutButton]} onPress={onNext}>
        <Text style={styles.buttonText}>Proceed</Text>
      </TouchableOpacity>
    </View>
  );
};

export default LegacyUpgrade;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 40,
    paddingHorizontal: 30,
    flex: 1,
  },
  header: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 23,
    marginBottom: 20,
  },
  subText: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 12,
    marginBottom: 20,
    lineHeight: 20,
  },
  productContainer: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: 20,
  },
  product: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    padding: 20,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  itemIcon: {
    width: 25,
    height: 25,
  },
  itemName: {
    flex: 1,
    paddingHorizontal: 15,
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 15,
  },
  dropdownIcon: {
    width: 10,
    height: 10,
    transform: [{rotate: '270deg'}],
  },
  desc: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
  },
  detailsContainer: {
    marginTop: 25,
  },
  loadingContainer: {
    marginBottom: 10,
  },
  moreTitle: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    textDecorationLine: 'underline',
    fontSize: 13,
  },
  fullDesc: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 11,
    marginTop: 15,
    marginBottom: 10,
  },
  valuesContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  valueLabel: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
  },
  valueItem: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
  },
  actionsContainer: {
    flexDirection: 'row',
    marginTop: 20,
  },
  filledButton: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    flex: 1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  filledButtonText: {
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 13,
  },
  outlinedButton: {
    flex: 1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#EBEBEB',
    borderWidth: 1,
  },
  outlinedButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 13,
  },
  checkoutButton: {
    backgroundColor: '#08152D',
    height: 45,
    borderRadius: 6,
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 16,
    fontFamily: 'Montserrat-SemiBold',
  },
});
