import React from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {formatterHelper, getUriImage} from '../../../../../utils';
import ProductItem from '../ProductItem';

const PurchaseOverview = ({
  onCheckOut,
  selectedAsset,
  purchaseData,
  statsData,
}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.header}>Final Overview</Text>
      <Text style={styles.subText}>You Will Be Purchasing A License For</Text>
      <ScrollView showsVerticalScrollIndicator={false}>
        <TouchableWithoutFeedback>
          <View>
            <ProductItem itemData={purchaseData} disableButton />
            <Text style={styles.insuranceText}>
              You Will Be Paying For The{' '}
              {formatterHelper(
                purchaseData?.pricesWithFees
                  ? purchaseData?.pricesWithFees[0]?.price
                  : purchaseData?.first_purchase?.price || 0,
                purchaseData?.pricesWithFees
                  ? purchaseData?.pricesWithFees[0]?.coin
                  : purchaseData?.first_purchase?.coin || 'USD',
              )}{' '}
              {purchaseData?.pricesWithFees
                ? purchaseData?.pricesWithFees[0]?.coin
                : purchaseData?.first_purchase?.coin || 'USD'}{' '}
              Issuance Fee WIth Your {selectedAsset?.coinName} Vault
            </Text>
            <View style={styles.coinContainer}>
              <View style={styles.coinDetails}>
                <FastImage
                  source={{uri: getUriImage(selectedAsset.coinImage)}}
                  style={styles.coinImage}
                  resizeMode="contain"
                />
                <Text numberOfLines={1} style={styles.coinName}>
                  {selectedAsset?.coinName} Balance
                </Text>
              </View>
              <View style={styles.coinValue}>
                <Text style={styles.valueText}>
                  {selectedAsset?.balance} {selectedAsset?.coinSymbol}
                </Text>
              </View>
            </View>
            <Text style={styles.insuranceText}>
              Cost Of License In {selectedAsset?.coinName}
            </Text>
            <View style={styles.coinContainer}>
              <View style={styles.coinDetails}>
                <Image
                  source={{uri: purchaseData?.product_icon || null}}
                  style={styles.coinImage}
                  resizeMode="contain"
                />
                <Text numberOfLines={1} style={styles.coinName}>
                  {purchaseData?.product_name}
                </Text>
              </View>
              <View style={styles.coinValue}>
                <Text style={styles.valueText}>
                  {formatterHelper(
                    statsData?.finalFromAmount || 1,
                    selectedAsset?.coinSymbol,
                  )}{' '}
                  {selectedAsset?.coinSymbol}
                </Text>
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </ScrollView>
      <TouchableOpacity style={[styles.checkoutButton]} onPress={onCheckOut}>
        <Text style={styles.buttonText}>Proceed</Text>
      </TouchableOpacity>
    </View>
  );
};

export default PurchaseOverview;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 40,
    paddingHorizontal: 30,
    flex: 1,
  },
  header: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 23,
    marginBottom: 15,
  },
  subText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
    marginBottom: 10,
  },
  insuranceText: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 13,
    marginBottom: 10,
  },
  coinContainer: {
    flexDirection: 'row',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    height: 60,
    alignItems: 'center',
    paddingHorizontal: 20,
    marginBottom: 20,
  },
  coinDetails: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  coinImage: {
    width: 22,
    height: 22,
  },
  coinName: {
    flex: 1,
    marginHorizontal: 10,
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 13,
  },
  coinValue: {
    flex: 1,
    alignItems: 'flex-end',
  },
  valueText: {
    textAlign: 'right',
    color: '#08152D',
    fontFamily: 'Montserrat',
  },
  checkoutButton: {
    marginTop: 40,
    backgroundColor: '#08152D',
    height: 45,
    borderRadius: 6,
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 16,
    fontFamily: 'Montserrat-SemiBold',
  },
});
