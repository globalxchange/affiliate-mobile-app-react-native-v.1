/* eslint-disable react-native/no-inline-styles */
import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {APP_CODE, GX_API_ENDPOINT} from '../../../../../../configs';
import ThemeData from '../../../../../../configs/ThemeData';
import {formatterHelper} from '../../../../../../utils';
import LoadingAnimation from '../../../../../LoadingAnimation';

const UpgradeReview = ({purchaseData, onNext}) => {
  const [isDetailsOpen, setIsDetailsOpen] = useState(true);
  const [productDetails, setProductDetails] = useState();

  // console.log('purchaseData', purchaseData);

  useEffect(() => {
    if (purchaseData) {
      const postData = {
        app_code: APP_CODE,
        product_id: purchaseData.product_id,
      };

      Axios.post(`${GX_API_ENDPOINT}/gxb/product/price/with/fees/get`, postData)
        .then((resp) => {
          const {data} = resp;

          // console.log('data', data);
          setProductDetails(data.product || '');
        })
        .catch((error) => {});
    }
  }, [purchaseData]);

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Review Upgrade</Text>
      <Text style={styles.subText}>
        You Will Be Charged The Difference Between The Issuance Fee You Paid In
        Your Previous License And This One Immediately. You Will Be Also Charged
        The Prorated Amount For The Difference In The Monthly Subscription Fee
        For The Remaining Days Of This Month
      </Text>
      <ScrollView showsVerticalScrollIndicator={false} bounces={false}>
        <TouchableWithoutFeedback>
          <View style={[styles.productContainer, {paddingBottom: 0}]}>
            <View style={[styles.product, isDetailsOpen && {paddingBottom: 0}]}>
              <TouchableOpacity
                onPress={() => setIsDetailsOpen(!isDetailsOpen)}
                style={styles.headerContainer}>
                <Image
                  source={{uri: purchaseData?.product_icon || null}}
                  style={styles.itemIcon}
                  resizeMode="contain"
                />
                <Text style={styles.itemName}>
                  {purchaseData?.product_name}
                </Text>
                <Image
                  source={require('../../../../../../assets/back-solid-icon.png')}
                  style={[
                    styles.dropdownIcon,
                    {transform: [{rotate: isDetailsOpen ? '90deg' : '270deg'}]},
                  ]}
                  resizeMode="contain"
                />
              </TouchableOpacity>
              <Text style={styles.desc}>{purchaseData?.sub_text}</Text>
              {isDetailsOpen && (
                <View style={styles.detailsContainer}>
                  {productDetails ? (
                    <>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}>
                        <Text style={styles.moreTitle}>
                          More About The License
                        </Text>
                        <Text
                          style={[
                            styles.moreTitle,
                            {textDecorationLine: 'none'},
                          ]}>
                          {productDetails?.pricesWithFees
                            ? productDetails?.pricesWithFees[0]?.coin
                            : 'USD'}
                        </Text>
                      </View>
                      <Text style={styles.fullDesc}>
                        {purchaseData?.full_description}
                      </Text>
                      <View style={styles.valuesContainer}>
                        <Text style={styles.valueLabel}>Issuance Fee</Text>
                        <Text style={styles.valueItem}>
                          {formatterHelper(
                            productDetails?.pricesWithFees
                              ? productDetails?.pricesWithFees[0]?.price
                              : productDetails?.first_purchase?.price || 0,
                            productDetails?.pricesWithFees
                              ? productDetails?.pricesWithFees[0]?.coin
                              : productDetails?.first_purchase?.coin || 'USD',
                          )}{' '}
                          {productDetails?.pricesWithFees
                            ? productDetails?.pricesWithFees[0]?.coin
                            : productDetails?.first_purchase?.coin || 'USD'}
                        </Text>
                      </View>
                      <View style={styles.valuesContainer}>
                        <Text style={styles.valueLabel}>Renewal Fee</Text>
                        <Text style={styles.valueItem}>
                          {formatterHelper(
                            productDetails?.pricesWithFees
                              ? productDetails?.pricesWithFees[1]?.price
                              : productDetails.billing_cycle?.monthly?.price ||
                                  0,
                            productDetails?.pricesWithFees
                              ? productDetails?.pricesWithFees[1]?.coin
                              : productDetails?.billing_cycle?.monthly?.coin ||
                                  'USD',
                          )}{' '}
                          {productDetails?.pricesWithFees
                            ? productDetails?.pricesWithFees[1]?.coin
                            : productDetails?.billing_cycle?.monthly?.coin ||
                              'USD'}
                        </Text>
                      </View>
                      <View style={styles.valuesContainer}>
                        <Text style={styles.valueLabel}>Prerequisites</Text>
                        <Text style={styles.valueItem}>None</Text>
                      </View>
                    </>
                  ) : (
                    <View style={styles.loadingContainer}>
                      <LoadingAnimation height={60} width={60} />
                    </View>
                  )}
                  <View style={styles.actionsContainer}>
                    <TouchableOpacity style={styles.filledButton}>
                      <Text style={styles.filledButtonText}>Earning Power</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={onNext}
                      style={styles.outlinedButton}>
                      <Text style={styles.outlinedButtonText}>Upgrade</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              )}
            </View>
          </View>
        </TouchableWithoutFeedback>
      </ScrollView>
      {/* <TouchableOpacity style={[styles.checkoutButton]} onPress={onClose}>
        <Text style={styles.buttonText}>Close</Text>
      </TouchableOpacity> */}
    </View>
  );
};

export default UpgradeReview;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 40,
    paddingHorizontal: 30,
    flex: 1,
  },
  header: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 23,
    marginBottom: 20,
  },
  subText: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 12,
    marginBottom: 20,
    lineHeight: 20,
  },
  productContainer: {
    flex: 1,
    justifyContent: 'center',
    marginBottom: 20,
  },
  product: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    padding: 20,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  itemIcon: {
    width: 25,
    height: 25,
  },
  itemName: {
    flex: 1,
    paddingHorizontal: 15,
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 15,
  },
  dropdownIcon: {
    width: 10,
    height: 10,
    transform: [{rotate: '270deg'}],
  },
  desc: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
  },
  detailsContainer: {
    marginTop: 25,
  },
  loadingContainer: {
    marginBottom: 10,
  },
  moreTitle: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    textDecorationLine: 'underline',
    fontSize: 13,
  },
  fullDesc: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 11,
    marginTop: 15,
    marginBottom: 10,
  },
  valuesContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  valueLabel: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
  },
  valueItem: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
  },
  actionsContainer: {
    flexDirection: 'row',
    marginHorizontal: -20,
    marginTop: 20,
  },
  filledButton: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    flex: 1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  filledButtonText: {
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  outlinedButton: {
    flex: 1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopColor: '#EBEBEB',
    borderTopWidth: 1,
  },
  outlinedButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  checkoutButton: {
    backgroundColor: '#08152D',
    height: 45,
    borderRadius: 6,
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 16,
    fontFamily: 'Montserrat-SemiBold',
  },
});
