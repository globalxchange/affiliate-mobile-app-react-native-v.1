import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import VaultSelector from '../../../../../VaultSelector';

const SelectCoin = ({
  coinsWithBalance,
  selectedAsset,
  setSelectedAsset,
  onNext,
}) => {
  const onNextClick = () => {
    if (selectedAsset) {
      onNext(selectedAsset);
    }
  };

  return (
    <View style={styles.viewContainer}>
      <Text style={styles.headerText}>
        Which Of Your Vaults Do You Want To Use To Pay The Difference?
      </Text>
      <View style={styles.controlContainer}>
        <VaultSelector
          placeHolder="Select From Vaults"
          onItemSelect={setSelectedAsset}
          selectedItem={selectedAsset}
          onNext={onNext}
        />
      </View>
      <TouchableOpacity style={[styles.checkoutButton]} onPress={onNextClick}>
        <Text style={styles.buttonText}>Proceed</Text>
      </TouchableOpacity>
    </View>
  );
};

export default SelectCoin;

const styles = StyleSheet.create({
  viewContainer: {
    paddingVertical: 40,
    paddingHorizontal: 30,
    flex: 1,
  },
  headerText: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 21,
    marginBottom: 10,
  },
  subText: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat',
    fontSize: 13,
    marginBottom: 20,
  },
  controlContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  checkoutButton: {
    marginTop: 40,
    backgroundColor: '#08152D',
    height: 45,
    borderRadius: 6,
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 16,
    fontFamily: 'Montserrat-SemiBold',
  },
});
