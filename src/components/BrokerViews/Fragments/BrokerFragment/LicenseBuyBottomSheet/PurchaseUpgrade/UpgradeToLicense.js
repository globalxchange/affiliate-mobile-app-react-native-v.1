import React from 'react';
import {
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import LoadingAnimation from '../../../../../LoadingAnimation';
import ProductItem from '../../ProductItem';

const UpgradeToLicense = ({setUpgradeToLicense, onNext, allLicenses}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.header}>
        Select The License You Are Upgrading To?
      </Text>
      <ScrollView showsVerticalScrollIndicator={false}>
        <TouchableWithoutFeedback>
          <View>
            {allLicenses ? (
              allLicenses?.map((item) => (
                <ProductItem
                  key={item._id}
                  itemData={item}
                  onItemPurchase={() => {
                    setUpgradeToLicense(item);
                    onNext();
                  }}
                />
              ))
            ) : (
              <LoadingAnimation />
            )}
          </View>
        </TouchableWithoutFeedback>
      </ScrollView>
    </View>
  );
};

export default UpgradeToLicense;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
  },
  header: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 20,
    marginBottom: 10,
  },
});
