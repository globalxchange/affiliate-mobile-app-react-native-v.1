import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import {APP_CODE, GX_API_ENDPOINT} from '../../../../../../configs';
import AsyncStorageHelper from '../../../../../../utils/AsyncStorageHelper';
import LoadingAnimation from '../../../../../LoadingAnimation';
import CompleteView from '../CompleView';
import Overview from './Overview';
import SelectCoin from './SelectCoin';
import UpgradeReview from './UpgradeReview';
import UpgradeToLicense from './UpgradeToLicense';
import UpgradingLicense from './UpgradingLicense';

const PurchaseUpgrade = ({
  purchaseData,
  onClose,
  userAllLicense,
  coinsWithBalance,
  getUserLicenses,
  isUpgrading,
  allLicenses,
}) => {
  const [currentView, setCurrentView] = useState();
  const [selectedAsset, setSelectedAsset] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [upgradingLicense, setUpgradingLicense] = useState();
  const [statsData, setStatsData] = useState();
  const [upgradeToLicense, setUpgradeToLicense] = useState('');

  useEffect(() => {
    if (isUpgrading) {
      // console.log('purchaseData', purchaseData);

      setUpgradingLicense(purchaseData);
    }
  }, [purchaseData, isUpgrading]);

  const makePurchase = async (stats, coin) => {
    // console.log('upgradingLicense', upgradingLicense);
    // console.log('purchaseData', purchaseData);
    // console.log('upgradeToLicense', upgradeToLicense);

    setIsLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();
    const profileId = await AsyncStorageHelper.getProfileId();

    const postData = {
      email,
      token,
      current_license_code: upgradingLicense.license_code,
      upgrade_to_product_id: isUpgrading
        ? upgradeToLicense.product_id
        : purchaseData.product_id,
      user_pay_coin: coin || selectedAsset.coinSymbol,
      stats: stats || false,
      app_code: APP_CODE,
      profile_id: profileId,
    };

    console.log('PostData', postData);

    Axios.post(`${GX_API_ENDPOINT}/gxb/product/license/upgrade`, postData)
      .then((resp) => {
        const {data} = resp;

        console.log('makePurchase', data);

        if (data.status) {
          if (stats) {
            setStatsData(data);
          } else {
            setCurrentView('Completed');
            getUserLicenses();
          }
        } else {
          WToast.show({
            data: data.message || 'Error on license upgrade',
            position: WToast.position.TOP,
          });
        }
      })
      .catch((error) => {
        console.log('Error on upgrading', error);
      })
      .finally(() => setIsLoading(false));
  };

  const renderActiveView = () => {
    switch (currentView) {
      case 'Review':
        return (
          <UpgradeReview
            purchaseData={isUpgrading ? upgradeToLicense : purchaseData}
            onNext={() => setCurrentView('SelectCoin')}
          />
        );
      case 'SelectCoin':
        return (
          <SelectCoin
            coinsWithBalance={coinsWithBalance}
            setSelectedAsset={setSelectedAsset}
            selectedAsset={selectedAsset}
            onNext={(coin) => {
              setIsLoading(true);
              setCurrentView('Overview');
              makePurchase(true, coin.coinSymbol);
            }}
          />
        );

      case 'Overview':
        return (
          <Overview
            purchaseData={isUpgrading ? upgradeToLicense : purchaseData}
            statsData={statsData}
            onNext={() => {
              makePurchase(false);
            }}
            selectedAsset={selectedAsset}
          />
        );

      case 'Completed':
        return (
          <CompleteView
            onClose={onClose}
            purchaseData={isUpgrading ? upgradeToLicense : purchaseData}
            selectedAsset={selectedAsset}
          />
        );

      default:
        return isUpgrading ? (
          <UpgradeToLicense
            setUpgradeToLicense={setUpgradeToLicense}
            onNext={() => setCurrentView('Review')}
            allLicenses={allLicenses}
          />
        ) : (
          <UpgradingLicense
            userAllLicense={userAllLicense}
            setUpgradingLicense={setUpgradingLicense}
            onNext={() => setCurrentView('Review')}
          />
        );
    }
  };

  return (
    <View style={styles.container}>
      {isLoading ? (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      ) : (
        renderActiveView()
      )}
    </View>
  );
};

export default PurchaseUpgrade;

const styles = StyleSheet.create({
  container: {flex: 1},
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
});
