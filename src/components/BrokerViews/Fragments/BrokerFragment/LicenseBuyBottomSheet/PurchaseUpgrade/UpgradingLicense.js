import React from 'react';
import {
  FlatList,
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
} from 'react-native';
import LicenseItem from '../../LicenseItem';

const UpgradingLicense = ({userAllLicense, onNext, setUpgradingLicense}) => {
  return (
    <TouchableWithoutFeedback>
      <View style={styles.container}>
        <Text style={styles.header}>Select The License You Are Upgrading?</Text>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={userAllLicense}
          keyExtractor={(item, index) => `${index}`}
          renderItem={({item}) => (
            <LicenseItem
              item={item}
              openUpgrade={() => {
                setUpgradingLicense(item);
                onNext();
              }}
            />
          )}
        />
      </View>
    </TouchableWithoutFeedback>
  );
};

export default UpgradingLicense;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
  },
  header: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 20,
    marginBottom: 10,
  },
});
