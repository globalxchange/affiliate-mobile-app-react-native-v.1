import React, {useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import PopupLayout from '../../../../../layouts/PopupLayout';

const LegacyUpgradeReview = ({setActiveView}) => {
  const [showQOne, setShowQOne] = useState(false);
  const [showQTwo, setShowQTwo] = useState(false);

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Lets See If You Qualify</Text>
      <View style={styles.qualificationsContainer}>
        <View style={styles.viewGroup}>
          <View style={styles.groupHeader}>
            <Text style={styles.itemTitle}>Qualification 1</Text>
            <Text
              style={styles.learnMore}
              onPress={() => setShowQOne(!showQOne)}>
              Click Here
            </Text>
          </View>
          <PopupLayout
            isOpen={showQOne}
            onClose={() => setShowQOne(false)}
            headerImage={require('../../../../../assets/broker-checkout-icon.png')}>
            <Text style={styles.itemHeader}>Qualification 1</Text>
            <Text style={styles.itemDecs}>
              The Email Which You Are Logged In Right Now Has To Be The Same
              Email Which Staked A Minimum Of 500 GXTokens Between April 1st
              2019 - December 31st 202 or Stated A Minimum Of 1000 GXTokens At
              Any Point Between January 1st 2020 To September 18th 2020.
            </Text>
          </PopupLayout>
        </View>
        <View style={styles.viewGroup}>
          <View style={styles.groupHeader}>
            <Text style={styles.itemTitle}>Qualification 2</Text>
            <Text
              style={styles.learnMore}
              onPress={() => setShowQTwo(!showQTwo)}>
              Click Here
            </Text>
          </View>
          <PopupLayout
            isOpen={showQTwo}
            onClose={() => setShowQTwo(false)}
            headerImage={require('../../../../../assets/broker-checkout-icon.png')}>
            <Text style={styles.itemHeader}>Qualification 2</Text>
            <Text style={styles.itemDecs}>
              The GXTokens That You Used In That Original Staking Contract Were
              Never Unstaked And Never Used For Another Grandfathering Procedure
            </Text>
          </PopupLayout>
        </View>
      </View>
      <TouchableOpacity
        style={[styles.checkoutButton]}
        onPress={() => setActiveView('LegacyUpgrade')}>
        <Text style={styles.buttonText}>Proceed</Text>
      </TouchableOpacity>
    </View>
  );
};

export default LegacyUpgradeReview;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 30,
    paddingHorizontal: 30,
    flex: 1,
  },
  header: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 23,
    marginBottom: 10,
  },
  qualificationsContainer: {
    flex: 1,
    justifyContent: 'space-evenly',
  },
  viewGroup: {},
  groupHeader: {
    flexDirection: 'row',
    marginBottom: 10,
    justifyContent: 'space-between',
    alignItems: 'baseline',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    paddingVertical: 25,
    paddingHorizontal: 25,
  },
  learnMore: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 15,
    textDecorationLine: 'underline',
  },
  itemTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 14,
  },
  itemHeader: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 20,
    marginBottom: 20,
  },
  itemDecs: {
    color: '#08152D',
    fontSize: 12,
    fontFamily: 'Montserrat',
    lineHeight: 28,
  },
  checkoutButton: {
    backgroundColor: '#08152D',
    height: 45,
    borderRadius: 6,
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 16,
    fontFamily: 'Montserrat-SemiBold',
  },
});
