import React, {useState, useEffect} from 'react';
import {StyleSheet, View, Image, Dimensions} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import Axios from 'axios';
import {GX_API_ENDPOINT, APP_CODE} from '../../../../../configs';
import AsyncStorageHelper from '../../../../../utils/AsyncStorageHelper';
import LoadingAnimation from '../../../../LoadingAnimation';
import {formatterHelper} from '../../../../../utils';
import CompleteView from './CompleView';
import PurchaseOptions from './PurchaseOptions';
import CoinPicker from './CoinPicker';
import LegacyUpgrade from './LegacyUpgrade';
import LegacyUpgradeReview from './LegacyUpgradeReview';
import PurchaseOverview from './PurchaseOverview';
import PurchaseUpgrade from './PurchaseUpgrade';
import LegacyQuote from './LegacyQuote';
import BottomSheetLayout from '../../../../../layouts/BottomSheetLayout';

const {height} = Dimensions.get('window');

const LicenseBuyBottomSheet = ({
  isBottomSheetOpen,
  setIsBottomSheetOpen,
  purchaseData,
  allLicenses,
  userAllLicense,
  getUserLicenses,
  isUpgrading,
}) => {
  // console.log('purchaseData', purchaseData);

  const [activeView, setActiveView] = useState();

  const [selectedAsset, setSelectedAsset] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [coinsWithBalance, setCoinsWithBalance] = useState();
  const [statsData, setStatsData] = useState();

  useEffect(() => {
    if (isUpgrading && isBottomSheetOpen) {
      setActiveView('PurchaseUpgrade');
    }
  }, [isUpgrading, isBottomSheetOpen]);

  useEffect(() => {
    if (!isBottomSheetOpen) {
      setSelectedAsset('');
      setActiveView();
      setIsLoading(false);
    }
  }, [isBottomSheetOpen]);

  useEffect(() => {
    (async () => {
      setIsLoading(true);
      const profileId = await AsyncStorageHelper.getProfileId();

      Axios.post(`${GX_API_ENDPOINT}/coin/vault/service/coins/get`, {
        app_code: APP_CODE,
        profile_id: profileId,
      })
        .then((resp) => {
          const {data} = resp;

          // console.log('Sending Balance', data);

          if (data.status) {
            const balances = data.coins_data || [];

            const filteredCurrencies = [];

            balances.forEach((item) => {
              filteredCurrencies.push({
                ...item,
                balance: formatterHelper(item.coinValue || 0, item.coinSymbol),
                image: item.coinImage,
                name: item.coinName,
              });
            });

            setCoinsWithBalance(filteredCurrencies);
          }
        })
        .catch((error) => {})
        .finally(() => setIsLoading(false));
    })();
  }, []);

  const onAssetSelected = (asset) => {
    setSelectedAsset(asset);
    setIsLoading(true);
    onCheckOut(true, asset?.coinSymbol);
  };

  const onCheckOut = async (stats, coin) => {
    if (!coin && !selectedAsset) {
      return WToast.show({
        data: 'Please Select An Asset First',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();
    const profileId = await AsyncStorageHelper.getProfileId();

    let billingMethod = '';

    if (purchaseData?.monthly) {
      billingMethod = 'monthly';
    } else if (purchaseData?.annual) {
      billingMethod = 'annual';
    } else if (purchaseData?.lifetime) {
      billingMethod = 'lifetime';
    } else if (purchaseData?.staking_allowed) {
      billingMethod = 'staking';
    } else {
      billingMethod = 'monthly';
    }

    const postData = {
      email,
      token,
      app_code: APP_CODE,
      profile_id: profileId,
      product_id: purchaseData.product_id,
      billing_method: billingMethod,
      pay_with_coin: coin || selectedAsset.coinSymbol,
      client_app: APP_CODE,
      stats: stats || false,
    };

    // console.log('purchaseData', purchaseData);

    // console.log('Post Data', postData);

    Axios.post(`${GX_API_ENDPOINT}/gxb/product/buy`, postData)
      .then((resp) => {
        const {data} = resp;

        // console.log('Purchase Data', data);

        if (data.status) {
          if (stats) {
            setStatsData(data.quoteData);
          } else {
            setActiveView('Completed');
            getUserLicenses();
            WToast.show({
              data: `${'Purchase Successful\n'}${data.license_code}`,
              position: WToast.position.TOP,
            });
          }
        } else {
          WToast.show({data: data.message, position: WToast.position.TOP});
        }
      })
      .catch((error) => {
        console.log('Error on purchasing', error);
      })
      .finally(() => setIsLoading(false));
  };

  const renderActiveComponent = () => {
    switch (activeView) {
      case 'Options':
        return (
          <PurchaseOptions
            setActiveView={setActiveView}
            onClose={() => setIsBottomSheetOpen(false)}
            purchaseData={purchaseData}
          />
        );

      case 'LegacyReview':
        return <LegacyUpgradeReview setActiveView={setActiveView} />;

      case 'LegacyUpgrade':
        return (
          <LegacyUpgrade
            setIsLoading={setIsLoading}
            allLicenses={allLicenses}
            setActiveView={setActiveView}
            onClose={() => setIsBottomSheetOpen(false)}
            getUserLicenses={getUserLicenses}
            onNext={() => setActiveView('LegacyCoinPicker')}
          />
        );

      case 'LegacyCoinPicker':
        return (
          <CoinPicker
            coinsWithBalance={coinsWithBalance}
            onAssetSelected={setSelectedAsset}
            selectedAsset={selectedAsset}
            title="Select Vault"
            subTitle="To Pay For The Difference"
            onNext={() => setActiveView('LegacyQuote')}
          />
        );

      case 'LegacyQuote':
        return (
          <LegacyQuote
            purchaseData={purchaseData}
            selectedAsset={selectedAsset}
            onClose={() => setIsBottomSheetOpen(false)}
            onBack={() => setActiveView('LegacyCoinPicker')}
            onSuccess={() => setActiveView('Completed')}
          />
        );

      case 'SelectCoin':
        return (
          <CoinPicker
            coinsWithBalance={coinsWithBalance}
            onAssetSelected={onAssetSelected}
            selectedAsset={selectedAsset}
            title="Select Vault To Pay"
            subTitle="Select The Vault That You Would Like Debit For This Purchase"
            onNext={() => setActiveView('PurchaseOverview')}
          />
        );

      case 'PurchaseOverview':
        return (
          <PurchaseOverview
            onCheckOut={() => onCheckOut(false)}
            selectedAsset={selectedAsset}
            purchaseData={purchaseData}
            statsData={statsData}
          />
        );

      case 'Completed':
        return (
          <CompleteView
            onClose={() => setIsBottomSheetOpen(false)}
            selectedAsset={selectedAsset}
            purchaseData={purchaseData}
          />
        );

      case 'PurchaseUpgrade':
        return (
          <PurchaseUpgrade
            purchaseData={purchaseData}
            onClose={() => setIsBottomSheetOpen(false)}
            userAllLicense={userAllLicense}
            coinsWithBalance={coinsWithBalance}
            getUserLicenses={getUserLicenses}
            isUpgrading={isUpgrading}
            allLicenses={allLicenses}
          />
        );

      default:
        return (
          <PurchaseOptions
            setActiveView={setActiveView}
            onClose={() => setIsBottomSheetOpen(false)}
            purchaseData={purchaseData}
          />
        );
    }
  };

  return (
    <BottomSheetLayout
      isOpen={isBottomSheetOpen}
      height={height * 0.65}
      onClose={() => setIsBottomSheetOpen(false)}>
      <View style={styles.header}>
        <Image
          style={styles.headerLogo}
          source={require('../../../../../assets/broker-checkout-icon.png')}
          resizeMode="contain"
        />
      </View>
      {isLoading ? (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      ) : (
        <>{renderActiveComponent()}</>
      )}
    </BottomSheetLayout>
  );
};

export default LicenseBuyBottomSheet;

const styles = StyleSheet.create({
  closeButton: {
    width: 24,
    height: 24,
    position: 'absolute',
    right: 15,
    top: 20,
    padding: 4,
  },
  icon: {
    flex: 1,
    width: null,
    height: null,
  },
  header: {
    backgroundColor: '#08152D',
    shadowColor: '#000000',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15,
  },
  headerLogo: {
    height: 22,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
});
