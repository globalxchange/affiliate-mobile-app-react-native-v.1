import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../../../../../configs/ThemeData';
import AsyncStorageHelper from '../../../../../utils/AsyncStorageHelper';

const LegacyFailedMessage = ({statsData, onClose, onViewQuote}) => {
  const [userEmail, setUserEmail] = useState('');

  useEffect(() => {
    (async () => {
      const email = await AsyncStorageHelper.getLoginEmail();
      setUserEmail(email);
    })();
  }, [statsData]);

  let errorMessage = '';

  if (statsData?.message === 'The user has already Upgraded License!') {
    errorMessage = `According To Our Database You Are Not Able To Upgrade Via The Legacy GXToken Grandfather Discount because  ${userEmail} has already utilized their one grandfather discount.`;
  } else if (
    statsData?.message === 'The user does not have a active Broker License'
  ) {
    errorMessage = `According To Our Database You Are Not Able To Upgrade Via The Legacy GXToken Grandfather Discount because  ${userEmail} has not staked the required amount of GXTokens prior to the cut off date.`;
  } else {
    errorMessage = `According To Our Database You Are Not Able To Upgrade Via The Legacy GXToken Grandfather Discount because  ${JSON.stringify(
      statsData,
    )}`;
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Sorry</Text>
      <Text style={styles.errorMessage}>{errorMessage}</Text>
      <View style={styles.actionContainer}>
        <TouchableOpacity style={[styles.backButton]} onPress={onClose}>
          <Text style={styles.backButtonText}>Close</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.checkoutButton]} onPress={onViewQuote}>
          <Text style={styles.buttonText}>View Quote</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default LegacyFailedMessage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 40,
    paddingHorizontal: 30,
  },
  header: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 30,
    marginBottom: 30,
  },
  errorMessage: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    lineHeight: 25,
    fontSize: 13,
  },
  actionContainer: {
    flexDirection: 'row',
    marginTop: 'auto',
  },
  checkoutButton: {
    marginTop: 20,
    backgroundColor: '#08152D',
    height: 45,
    borderRadius: 6,
    justifyContent: 'center',
    flex: 1,
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 13,
    fontFamily: 'Montserrat-SemiBold',
  },
  backButton: {
    marginTop: 20,
    height: 45,
    borderRadius: 6,
    justifyContent: 'center',
    flex: 1,
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
    marginRight: 15,
  },
  backButtonText: {
    color: '#08152D',
    textAlign: 'center',
    fontSize: 13,
    fontFamily: 'Montserrat-SemiBold',
  },
});
