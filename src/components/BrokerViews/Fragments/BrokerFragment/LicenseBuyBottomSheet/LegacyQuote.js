import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import {APP_CODE, GX_API_ENDPOINT} from '../../../../../configs';
import ThemeData from '../../../../../configs/ThemeData';
import {formatterHelper} from '../../../../../utils';
import AsyncStorageHelper from '../../../../../utils/AsyncStorageHelper';
import LoadingAnimation from '../../../../LoadingAnimation';
import {WToast} from 'react-native-smart-tip';
import LegacyFailedMessage from './LegacyFailedMessage';

const LegacyQuote = ({
  purchaseData,
  selectedAsset,
  onClose,
  onBack,
  onSuccess,
}) => {
  const [isLoading, setIsLoading] = useState(true);
  const [requiredCurrency, setRequiredCurrency] = useState('');
  const [debitValue, setDebitValue] = useState('');
  const [firstPurchaseData, setFirstPurchaseData] = useState('');
  const [monthlyPurchaseData, setMonthlyPurchaseData] = useState('');
  const [statsData, setStatsData] = useState('');
  const [isPurchaseLoading, setIsPurchaseLoading] = useState(false);
  const [showFailedMessage, setShowFailedMessage] = useState(false);
  const [redoQuoteFlag, setRedoQuoteFlag] = useState(false);

  useEffect(() => {
    setIsLoading(true);

    console.log('purchaseData', purchaseData);
    console.log('selectedAsset', selectedAsset);

    (async () => {
      if (purchaseData && selectedAsset) {
        try {
          const {
            data,
          } = await Axios.post(
            `${GX_API_ENDPOINT}/gxb/product/price/with/fees/get`,
            {app_code: APP_CODE, product_id: purchaseData.product_id},
          );

          console.log('Product Details', data);

          const firstPurchase = data?.product?.pricesWithFees[0];
          const monthlyPurchase = data?.product?.pricesWithFees[1];
          setFirstPurchaseData(firstPurchase);
          setMonthlyPurchaseData(monthlyPurchase);

          const reqCurrency = firstPurchase?.coin;
          setRequiredCurrency(reqCurrency);

          let offerPrice = 0;

          if (reqCurrency === 'GXT') {
            offerPrice = firstPurchase?.price - 250;
          } else if (reqCurrency === 'USD') {
            offerPrice = firstPurchase?.price - 99;
          }
          setDebitValue(offerPrice);

          let email;
          if (redoQuoteFlag) {
            email = 'perry@perrywong.ca';
          } else {
            email = await AsyncStorageHelper.getLoginEmail();
          }
          const token = await AsyncStorageHelper.getAppToken();

          const statsPostData = {
            email,
            token,
            product_id: purchaseData?.product_id,
            app_code: APP_CODE,
            requiredCurrency: reqCurrency,
            desiredCurrency: selectedAsset.coinSymbol,
            debitAmount: offerPrice,
            stats: true,
          };

          console.log('statsPostData', statsPostData);

          const quoteResp = await Axios.post(
            `${GX_API_ENDPOINT}/gxb/product/upgrade/user/license`,
            statsPostData,
          );

          console.log('quoteResp', quoteResp.data);
          setIsLoading(false);
          setStatsData(quoteResp.data?.quoteData || quoteResp.data);
          if (!quoteResp.data?.status) {
            setShowFailedMessage(true);
          }
        } catch (error) {
          console.log('Error on Quote', error);
        }
      }
    })();
  }, [purchaseData, selectedAsset, redoQuoteFlag]);

  const onCheckOut = async () => {
    setIsLoading(true);
    setIsPurchaseLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    const postData = {
      email,
      token,
      product_id: purchaseData?.product_id,
      app_code: APP_CODE,
      requiredCurrency: requiredCurrency,
      desiredCurrency: selectedAsset.coinSymbol,
      debitAmount: debitValue,
      stats: false,
    };

    Axios.post(`${GX_API_ENDPOINT}/gxb/product/upgrade/user/license`, postData)
      .then(({data}) => {
        console.log('Purchase Resp', data);
        setIsLoading(false);
        setIsPurchaseLoading(false);

        WToast.show({data: data.message || '', position: WToast.position.TOP});

        if (data.status) {
          onSuccess();
        }
      })
      .catch((error) => {
        console.log('Error on license upgrade', error);
        setIsLoading(false);
        setIsPurchaseLoading(false);
      });
  };

  if (isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <LoadingAnimation />
        {isPurchaseLoading || (
          <Text style={styles.loadingText}>
            Generating Quote After Grandfather Benefit
          </Text>
        )}
      </View>
    );
  }

  if (showFailedMessage) {
    return (
      <LegacyFailedMessage
        statsData={statsData}
        onClose={onClose}
        onViewQuote={() => {
          setIsLoading(true);
          setShowFailedMessage(false);
          setRedoQuoteFlag(true);
        }}
      />
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Final Overview</Text>
      <ScrollView showsVerticalScrollIndicator={false}>
        <TouchableWithoutFeedback>
          <View>
            <View style={styles.viewGroup}>
              <Text style={styles.insuranceText}>Pre Discount Costs</Text>
              <View style={styles.viewContainer}>
                <View style={styles.coinContainer}>
                  <View style={styles.coinDetails}>
                    <Text numberOfLines={1} style={styles.coinName}>
                      Insurance Fee
                    </Text>
                  </View>
                  <View style={styles.coinValue}>
                    <Text style={styles.valueText}>
                      {formatterHelper(
                        firstPurchaseData?.price || 0,
                        requiredCurrency,
                      )}{' '}
                      {requiredCurrency}
                    </Text>
                  </View>
                </View>
                <View style={styles.divider} />
                <View style={styles.coinContainer}>
                  <View style={styles.coinDetails}>
                    <Text numberOfLines={1} style={styles.coinName}>
                      Monthly Fee
                    </Text>
                  </View>
                  <View style={styles.coinValue}>
                    <Text style={styles.valueText}>
                      {formatterHelper(
                        monthlyPurchaseData?.price || 0,
                        requiredCurrency,
                      )}{' '}
                      {requiredCurrency}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
            <View style={styles.viewGroup}>
              <Text style={styles.insuranceText}>
                Legacy {requiredCurrency} Staking Discount
              </Text>
              <View style={styles.viewContainer}>
                <View style={styles.coinContainer}>
                  <View style={styles.coinDetails}>
                    <Text numberOfLines={1} style={styles.coinName}>
                      Issuance Fee Savings
                    </Text>
                  </View>
                  <View style={styles.coinValue}>
                    <Text style={styles.valueText}>
                      {formatterHelper(
                        requiredCurrency === 'GXT' ? 250 : 99,
                        requiredCurrency,
                      )}{' '}
                      {requiredCurrency}
                    </Text>
                  </View>
                </View>
                <View style={styles.divider} />
                <View style={styles.coinContainer}>
                  <View style={styles.coinDetails}>
                    <Text numberOfLines={1} style={styles.coinName}>
                      Monthly Fee Savings
                    </Text>
                  </View>
                  <View style={styles.coinValue}>
                    <Text style={styles.valueText}>
                      {formatterHelper(
                        requiredCurrency === 'GXT' ? 250 : 99,
                        requiredCurrency,
                      )}{' '}
                      {requiredCurrency}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
            <View style={styles.viewGroup}>
              <Text style={styles.insuranceText}>Post Discount Costs</Text>
              <View style={styles.viewContainer}>
                <View style={styles.coinContainer}>
                  <View style={styles.coinDetails}>
                    <Text numberOfLines={1} style={styles.coinName}>
                      Issuance Fee
                    </Text>
                  </View>
                  <View style={styles.coinValue}>
                    <Text style={styles.valueText}>
                      {formatterHelper(debitValue || 0, requiredCurrency)}{' '}
                      {requiredCurrency}
                    </Text>
                  </View>
                </View>
                <View style={styles.divider} />
                <View style={styles.coinContainer}>
                  <View style={styles.coinDetails}>
                    <Text numberOfLines={1} style={styles.coinName}>
                      Monthly Fee
                    </Text>
                  </View>
                  <View style={styles.coinValue}>
                    <Text style={styles.valueText}>
                      {formatterHelper(0, requiredCurrency)} {requiredCurrency}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
            <View style={styles.viewGroup}>
              <Text style={styles.insuranceText}>
                You Selected To Pay With Your
              </Text>
              <View style={styles.viewContainer}>
                <View style={styles.coinContainer}>
                  <View style={styles.coinDetails}>
                    <Text numberOfLines={1} style={styles.coinName}>
                      {selectedAsset?.coinSymbol} Vault
                    </Text>
                  </View>
                  <View style={styles.coinValue}>
                    <Text style={styles.valueText}>
                      {formatterHelper(
                        selectedAsset?.coinValue || 0,
                        selectedAsset?.coinSymbol,
                      )}{' '}
                      {selectedAsset?.coinSymbol}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
            <View style={styles.viewGroup}>
              <Text style={styles.insuranceText}>
                Post Discount Costs In {selectedAsset?.coinSymbol}
              </Text>
              <View style={styles.viewContainer}>
                <View style={styles.coinContainer}>
                  <View style={styles.coinDetails}>
                    <Text numberOfLines={1} style={styles.coinName}>
                      Issuance Fee
                    </Text>
                  </View>
                  <View style={styles.coinValue}>
                    <Text style={styles.valueText}>
                      {formatterHelper(
                        statsData?.finalFromAmount || 0,
                        selectedAsset?.coinSymbol,
                      )}{' '}
                      {selectedAsset?.coinSymbol}
                    </Text>
                  </View>
                </View>
                <View style={styles.divider} />
                <View style={styles.coinContainer}>
                  <View style={styles.coinDetails}>
                    <Text numberOfLines={1} style={styles.coinName}>
                      Monthly Fee
                    </Text>
                  </View>
                  <View style={styles.coinValue}>
                    <Text style={styles.valueText}>
                      {formatterHelper(0, selectedAsset?.coinSymbol)}{' '}
                      {selectedAsset?.coinSymbol}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
            <View style={styles.actionContainer}>
              <TouchableOpacity style={[styles.backButton]} onPress={onBack}>
                <Text style={styles.backButtonText}>Back</Text>
              </TouchableOpacity>
              {redoQuoteFlag || selectedAsset.coinValue <= 0 || (
                <TouchableOpacity
                  style={[styles.checkoutButton]}
                  onPress={onCheckOut}>
                  <Text style={styles.buttonText}>Proceed</Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </TouchableWithoutFeedback>
      </ScrollView>
    </View>
  );
};

export default LegacyQuote;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 40,
    paddingHorizontal: 30,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingVertical: 40,
    paddingHorizontal: 30,
  },
  loadingText: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_MEDIUM,
    textAlign: 'center',
    fontSize: 16,
  },
  header: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 23,
    marginBottom: 30,
  },
  viewGroup: {
    marginBottom: 30,
  },
  viewContainer: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    paddingHorizontal: 20,
  },
  divider: {
    height: 1,
    backgroundColor: '#EBEBEB',
  },
  insuranceText: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 13,
    marginBottom: 10,
  },
  coinContainer: {
    flexDirection: 'row',
    height: 60,
    alignItems: 'center',
  },
  coinDetails: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  coinImage: {
    width: 22,
    height: 22,
  },
  coinName: {
    flex: 1,
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 13,
  },
  coinValue: {
    alignItems: 'flex-end',
  },
  valueText: {
    textAlign: 'right',
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 13,
  },
  actionContainer: {
    flexDirection: 'row',
  },
  checkoutButton: {
    marginTop: 20,
    backgroundColor: '#08152D',
    height: 45,
    borderRadius: 6,
    justifyContent: 'center',
    flex: 1,
    marginLeft: 15,
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 13,
    fontFamily: 'Montserrat-SemiBold',
  },
  backButton: {
    marginTop: 20,
    height: 45,
    borderRadius: 6,
    justifyContent: 'center',
    flex: 1,
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
  },
  backButtonText: {
    color: '#08152D',
    textAlign: 'center',
    fontSize: 13,
    fontFamily: 'Montserrat-SemiBold',
  },
});
