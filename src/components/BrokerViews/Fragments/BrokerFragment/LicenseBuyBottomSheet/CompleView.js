/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useEffect} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {AppContext} from '../../../../../contexts/AppContextProvider';
import {formatterHelper, getUriImage} from '../../../../../utils';

const CompleteView = ({onClose, selectedAsset, purchaseData}) => {
  const {walletBalances, updateWalletBalances} = useContext(AppContext);

  useEffect(() => {
    updateWalletBalances();
  }, []);

  const updatedSendBalance =
    walletBalances[`${selectedAsset?.coinSymbol?.toLowerCase()}_balance`] || 0;

  // console.log('purchaseData', purchaseData);

  return (
    <View style={styles.container}>
      <Text style={styles.headerText}>Success</Text>
      <View style={styles.viewContainer}>
        <View>
          <Text style={styles.itemTitle}>Here Are Your License Details</Text>
          <View style={styles.itemContainer}>
            <Text style={styles.itemLabel}>Brand</Text>
            <View style={styles.divider} />
            <Text style={styles.itemValue}>GlobalXChange</Text>
          </View>
          <View style={styles.itemContainer}>
            <Text style={styles.itemLabel}>License</Text>
            <View style={styles.divider} />
            <Text style={styles.itemValue}>{purchaseData?.product_code}</Text>
          </View>
        </View>
        {selectedAsset ? (
          <View>
            <Text style={styles.itemTitle}>
              New {selectedAsset?.coinName} Balance
            </Text>
            <View style={styles.itemContainer}>
              <Text style={styles.itemLabel}>
                {formatterHelper(updatedSendBalance, selectedAsset.coinSymbol)}
              </Text>
              <View style={styles.divider} />
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  flex: 1,
                  height: '100%',
                  flexDirection: 'row',
                }}>
                <FastImage
                  source={{uri: getUriImage(selectedAsset.coinImage)}}
                  resizeMode="contain"
                  style={{width: 20, height: 20, marginRight: 10}}
                />
                <Text style={[styles.itemValue, {flex: 0}]}>
                  {selectedAsset.coinSymbol}
                </Text>
              </View>
            </View>
          </View>
        ) : null}
      </View>
      <View style={styles.actionsContainer}>
        <TouchableOpacity onPress={onClose} style={styles.buttonOutlined}>
          <Text style={styles.buttonOutlinedText}>See License</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={onClose} style={styles.closeButton}>
          <Text style={styles.closeText}>Close</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default CompleteView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 35,
  },
  headerText: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 34,

    paddingHorizontal: 30,
  },
  viewContainer: {
    flex: 1,
    paddingHorizontal: 30,
    justifyContent: 'space-evenly',
  },
  itemTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
  },
  itemContainer: {
    flexDirection: 'row',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    height: 50,
    alignItems: 'center',
    marginTop: 10,
  },
  itemLabel: {
    flex: 1,
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
  },
  itemValue: {
    flex: 1,
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
  },
  divider: {
    width: 1,
    backgroundColor: '#EBEBEB',
    height: '100%',
  },
  closeButton: {
    backgroundColor: '#08152D',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  closeText: {
    color: 'white',
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
  },
  actionsContainer: {
    flexDirection: 'row',
    paddingHorizontal: 30,
  },
  buttonOutlined: {
    flex: 1,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    marginRight: 10,
  },
  buttonOutlinedText: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
  },
});
