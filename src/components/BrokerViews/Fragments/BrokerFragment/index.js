/* eslint-disable react-native/no-inline-styles */
import React, {useState, useContext, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import {AppContext} from '../../../../contexts/AppContextProvider';
import Axios from 'axios';
import {APP_CODE, GX_API_ENDPOINT} from '../../../../configs';
import AsyncStorageHelper from '../../../../utils/AsyncStorageHelper';
import ListPopup from '../../ListPopup';
import LoadingAnimation from '../../../LoadingAnimation';
import LicenseTypes from './LicenseTypes';
import AllLicenses from './AllLicenses';
import UpgradeLicense from './UpgradeLicense';
import ProductItem from './ProductItem';
import LicenseBuyBottomSheet from './LicenseBuyBottomSheet';
import {useNavigation} from '@react-navigation/native';

const BrokerFragment = ({setActiveLicense, activeBanker}) => {
  const {cryptoTableData, isLoggedIn} = useContext(AppContext);

  const {navigate} = useNavigation();

  const [isTotalAssetsOpen, setIsTotalAssetsOpen] = useState(false);
  const [brokerLicenceDetails, setBrokerLicenceDetails] = useState('');
  const [allLicenceDetails, setAllLicenceDetails] = useState();
  const [selectedLicence, setSelectedLicence] = useState('');
  const [activeType, setActiveType] = useState();
  const [allLicenses, setAllLicenses] = useState();
  const [showAllList, setShowAllList] = useState(false);
  const [userAllLicense, setUserAllLicense] = useState();
  const [showUpgrade, setShowUpgrade] = useState();
  const [isPurchaseOpen, setIsPurchaseOpen] = useState(false);

  useEffect(() => {
    (async () => {
      Axios.get(`${GX_API_ENDPOINT}/gxb/product/get`, {
        params: {product_created_by: activeBanker.email},
      })
        .then((resp) => {
          const {data} = resp;

          // console.log('getLicenceDetails', data);

          const products = data.products || [];
          setAllLicenses(products);

          const monthlyProducts = products.filter(
            (item) => item.monthly === true,
          );

          const lifeTimeProducts = products.filter(
            (item) => item.lifetime === true,
          );

          const stakingProducts = products.filter(
            (item) => item.staking_allowed === true,
          );

          const productsObj = {
            Monthly: monthlyProducts,
            'One Time': lifeTimeProducts,
            Staking: stakingProducts,
          };

          // console.log('productsObj', productsObj);

          setAllLicenceDetails(productsObj);
        })
        .catch((error) => {
          console.log('Error on getting licence details', error);
        });
    })();
  }, [activeBanker]);

  const getUserLicenses = async () => {
    const email = await AsyncStorageHelper.getLoginEmail();

    if (email) {
      Axios.get(`${GX_API_ENDPOINT}/coin/vault/user/license/get`, {
        params: {email},
      })
        .then((resp) => {
          const {data} = resp;

          // console.log('All User Licenses', data);

          if (data.status) {
            const licenceList = data.licenses || [];

            let brokerDetails = '';
            const parsedList = [];

            licenceList.forEach((item) => {
              const license = allLicenses.find(
                (x) => x.product_id === item.product_id,
              );

              if (license) {
                const parsedItem = {
                  ...item,
                  name: license?.product_name,
                  icon: license?.product_icon,
                };

                parsedList.push(parsedItem);

                if (
                  !brokerDetails &&
                  item.app_code === APP_CODE &&
                  (item.license_status === 'active' ||
                    item.license_status === 'grandfathered')
                ) {
                  brokerDetails = parsedItem;
                }
              }
            });

            setUserAllLicense(parsedList);
            // console.log('brokerDetails', brokerDetails);
            setActiveLicense(brokerDetails);
            setBrokerLicenceDetails(brokerDetails);
          }
        })
        .catch((error) => {
          console.log('Error on getting broker data', error);
        });
    }
  };

  useEffect(() => {
    if (allLicenses) {
      getUserLicenses();
    }
  }, [allLicenses]);

  useEffect(() => {
    if (allLicenceDetails) {
      setSelectedLicence(allLicenceDetails[activeType || 'Monthly'][0] || '');
    }
  }, [activeType, allLicenceDetails]);

  const onItemPurchase = (item) => {
    if (!isLoggedIn) {
      navigate('Landing');
    } else {
      setIsPurchaseOpen(true);
      setSelectedLicence(item);
    }
  };

  if (!allLicenses && !brokerLicenceDetails) {
    return (
      <View style={styles.loadingContainer}>
        <LoadingAnimation />
      </View>
    );
  }

  if (showUpgrade) {
    return (
      <UpgradeLicense
        allLicenses={allLicenses}
        onClose={() => setShowUpgrade()}
        brokerLicenceDetails={brokerLicenceDetails}
        showUpgrade={showUpgrade}
      />
    );
  }

  if (showAllList) {
    return (
      <AllLicenses
        openUpgrade={setShowUpgrade}
        list={userAllLicense}
        onClose={() => setShowAllList(false)}
      />
    );
  }

  const products = allLicenceDetails
    ? allLicenceDetails[activeType || 'Monthly'] || []
    : [];

  return (
    <View style={styles.scrollContainer}>
      <View style={styles.container}>
        <TouchableOpacity style={styles.licenceHeaderContainer}>
          <Text style={[styles.header, {marginBottom: 0}]}>
            Active Licenses
          </Text>
          <Text
            onPress={() => navigate('Support', {openMyLicenses: true})}
            style={[styles.seeAllButton, {marginBottom: 0}]}>
            See All License
          </Text>
        </TouchableOpacity>
        <View style={styles.licenceItem}>
          <Image
            source={
              brokerLicenceDetails.license_id === 'GXBTL'
                ? require('../../../../assets/gx-logo.png')
                : {
                    uri: brokerLicenceDetails
                      ? brokerLicenceDetails?.icon || null
                      : null,
                  }
            }
            style={styles.licenceItemIcon}
            resizeMode="contain"
          />
          <Text style={styles.licenceItemName}>
            {brokerLicenceDetails.license_id === 'GXBTL'
              ? 'Legacy GXBroker License'
              : brokerLicenceDetails?.name || 'InstaCrypto Brokerage'}
          </Text>
          {brokerLicenceDetails ? (
            <Image
              source={require('../../../../assets/green-check.png')}
              style={styles.licenceItemStatusIcon}
              resizeMode="contain"
            />
          ) : (
            <View style={styles.redDot} />
          )}
        </View>
        <LicenseTypes activeType={activeType} setActiveType={setActiveType} />
        <View style={styles.listContainer}>
          <ScrollView showsVerticalScrollIndicator={false}>
            {allLicenceDetails ? (
              <View style={styles.productList}>
                {products?.length > 0 ? (
                  <>
                    {products.map((item, index) => (
                      <ProductItem
                        key={index}
                        itemData={item}
                        onItemPurchase={() => onItemPurchase(item)}
                      />
                    ))}
                  </>
                ) : (
                  <Text style={styles.emptyText}>
                    No Products Available For Now
                  </Text>
                )}
              </View>
            ) : (
              <View style={styles.loadingContainer}>
                <LoadingAnimation />
              </View>
            )}
          </ScrollView>
        </View>
      </View>
      <ListPopup
        isDropDownOpen={isTotalAssetsOpen}
        setIsDropDownOpen={setIsTotalAssetsOpen}
        items={cryptoTableData || []}
      />
      <LicenseBuyBottomSheet
        isBottomSheetOpen={isPurchaseOpen}
        setIsBottomSheetOpen={setIsPurchaseOpen}
        purchaseData={selectedLicence}
        allLicenses={allLicenses}
        userAllLicense={userAllLicense}
        getUserLicenses={getUserLicenses}
      />
    </View>
  );
};

export default BrokerFragment;

const styles = StyleSheet.create({
  scrollContainer: {
    flex: 1,
    backgroundColor: 'white',
  },
  container: {
    flex: 1,
    paddingTop: 30,
    paddingHorizontal: 30,
    backgroundColor: 'white',
  },
  header: {
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
    fontSize: 16,
    marginBottom: 15,
  },
  seeAllButton: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 12,
    textDecorationLine: 'underline',
  },
  listContainer: {
    flex: 1,
    marginBottom: -20,
  },
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },
  itemText: {
    color: '#9A9A9A',
    flex: 1,
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
  itemAction: {
    backgroundColor: '#08152D',
    minWidth: 70,
    minHeight: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  itemActionText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 10,
  },
  licenceHeaderContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 10,
  },
  changeButtonText: {
    fontFamily: 'Montserrat',
    color: '#788995',
    fontSize: 13,
  },
  licenceItem: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    paddingHorizontal: 18,
    paddingVertical: 10,
    marginTop: 10,
  },
  licenceItemIcon: {
    width: 25,
    height: 25,
  },
  licenceItemName: {
    flex: 1,
    paddingHorizontal: 10,
    color: '#788995',
    fontFamily: 'Montserrat',
    fontSize: 13,
  },
  licenceItemStatusIcon: {width: 18, height: 18},
  redDot: {
    backgroundColor: '#D80027',
    width: 18,
    height: 18,
    borderRadius: 10,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  productList: {
    // marginTop: 10,
  },
  productItem: {
    marginRight: 15,
  },
  productTitleContainer: {
    height: 150,
    marginBottom: 10,
    borderRadius: 8,
    justifyContent: 'center',
    paddingHorizontal: 15,
    width: 130,
  },
  productTitle: {
    color: 'white',
    fontFamily: 'Montserrat-Bold',
  },
  productPice: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 12,
    textAlign: 'center',
  },
  emptyText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
    fontSize: 18,
  },
});
