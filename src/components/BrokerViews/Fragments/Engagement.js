/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
} from 'react-native';
import {AppContext} from '../../../contexts/AppContextProvider';
import {useNavigation} from '@react-navigation/native';
import * as WebBrowser from 'expo-web-browser';
import Clipboard from '@react-native-community/clipboard';

const Engagement = ({activeBanker}) => {
  const {userName} = useContext(AppContext);

  const {navigate} = useNavigation();

  const [isCopied, setIsCopied] = useState(false);

  // console.log('activeBanker', activeBanker);

  const linkCopyHandler = () => {
    const link = `https://globalxchange.com/register/${(
      userName || ''
    ).toLowerCase()}`;

    Clipboard.setString(link);
    setIsCopied(true);

    setTimeout(() => setIsCopied(false), 10000);
  };

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.headerText}>
          {activeBanker ? `${activeBanker.bankerTag}'s ` : ''}Referral Link
        </Text>
      </View>
      <View style={styles.controlContainer}>
        <TouchableOpacity style={styles.affLinkContainer}>
          <Text style={styles.link}>globalxchange.com/register/</Text>
          <Text style={styles.username}>{(userName || '').toLowerCase()}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={linkCopyHandler}
          style={styles.buttonOutlined}>
          <Text style={styles.buttonOutlinedText}>
            {isCopied ? 'Copied' : 'Copy'}
          </Text>
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        onPress={() => WebBrowser.openBrowserAsync('https://instacrypto.tv/')}
        style={styles.controlContainer}>
        <Image
          style={styles.tvIcon}
          source={require('../../../assets/broker-tv-icon.png')}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <View style={styles.scrollView}>
        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
          <TouchableOpacity
            onPress={() => navigate('Tools')}
            style={[styles.action, {marginLeft: 10}]}>
            <Image
              style={styles.actionIcon}
              source={require('../../../assets/tools-icon.png')}
              resizeMode="contain"
            />
            <Text style={styles.actionText}>Tools</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.action, {marginLeft: 25}]}
            onPress={() => navigate('Lean')}>
            <Image
              style={[styles.actionIcon]}
              source={require('../../../assets/training-icon.png')}
              resizeMode="contain"
            />
            <Text style={[styles.actionText]}>Training</Text>
          </TouchableOpacity>
          <TouchableOpacity
            disabled
            style={[styles.action, {marginLeft: 25, marginRight: 10}]}>
            <Image
              style={[styles.actionIcon, {opacity: 0.5}]}
              source={require('../../../assets/run-ad-icon.png')}
              resizeMode="contain"
            />
            <Text style={[styles.actionText, {opacity: 0.5}]}>Run Ads</Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    </View>
  );
};

export default Engagement;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 20,
    paddingHorizontal: 30,
    backgroundColor: '#F9F9F9',
    // justifyContent: 'space-between',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerText: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 14,
  },
  headerTextLink: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 11,
  },
  controlContainer: {
    flexDirection: 'row',
    marginTop: 10,
    backgroundColor: 'white',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 15,
    justifyContent: 'center',
    borderRadius: 6,
    marginVertical: 20,
  },
  affLinkContainer: {
    flexDirection: 'row',
    flex: 1,
    overflow: 'hidden',
    marginRight: 10,
  },
  link: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
  username: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 12,
  },
  buttonOutlined: {
    borderColor: 'rgba(0, 29, 65, 0.5)',
    borderWidth: 1,
    paddingVertical: 3,
    paddingHorizontal: 12,
  },
  buttonOutlinedText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    fontSize: 11,
  },
  tvIcon: {
    height: 38,
    width: 100,
  },
  moreIcon: {
    height: 12,
    width: 12,
  },
  moreButton: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  moreText: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    marginRight: 10,
  },
  scrollView: {
    marginHorizontal: -10,
  },
  action: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    borderRadius: 8,
    paddingVertical: 15,
    paddingHorizontal: 40,
    // flex: 1,
    marginBottom: 10,
    marginTop: 'auto',
    borderColor: '#EBEBEB',
    borderWidth: 1,
  },
  actionIcon: {
    width: 30,
    height: 30,
  },
  actionText: {
    marginTop: 10,
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    textAlign: 'center',
    fontSize: 12,
  },
  actionTextBlack: {
    color: '#464B4E',
  },
});
