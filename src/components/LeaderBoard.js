import React from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {usdValueFormatter} from '../utils';
import LoadingAnimation from './LoadingAnimation';
import NumberToWord from 'number-to-words';
import Swipeable from 'react-native-swipeable';

const LeaderBoard = ({isOtc, tokensLeads, otcLeads, activeBrand}) => {
  const rightButtons = [
    <TouchableOpacity style={styles.swipeButton}>
      <Text style={styles.slideText}>Learn More</Text>
    </TouchableOpacity>,
  ];

  return (
    <View style={styles.container}>
      {tokensLeads && otcLeads ? (
        <FlatList
          showsVerticalScrollIndicator={false}
          data={
            (activeBrand && activeBrand.formData.Name) === 'Cryptocurrency'
              ? isOtc
                ? otcLeads.slice(0, 10)
                : tokensLeads.slice(0, 10)
              : []
          }
          keyExtractor={(item, index) => item._id}
          renderItem={({item, index}) => (
            <Swipeable rightButtons={rightButtons} rightButtonWidth={100}>
              <View style={styles.itemContainer}>
                <Image
                  source={
                    item.profile_img
                      ? {uri: item.profile_img}
                      : require('../assets/empty-profile.png')
                  }
                  style={styles.icon}
                  resizeMode="contain"
                />
                <Text style={styles.name}>{`${NumberToWord.toOrdinal(
                  index + 1,
                )} Place`}</Text>
                <Text style={styles.amount}>
                  {usdValueFormatter.format(item.total_commissions || 0)}
                </Text>
              </View>
            </Swipeable>
          )}
          ListEmptyComponent={
            <Text style={styles.emptyText}>
              No Leaderboard Data is Not Available
            </Text>
          }
        />
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default LeaderBoard;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    marginBottom: 5,
  },
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 8,
    padding: 20,
    marginTop: 20,
  },
  icon: {
    width: 25,
    height: 25,
  },
  name: {
    flex: 1,
    paddingHorizontal: 15,
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
  },
  amount: {
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  emptyText: {
    textAlign: 'center',
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    marginTop: 60,
    fontSize: 15,
  },
  swipeButton: {
    marginTop: 20,
    backgroundColor: '#08152D',
    flex: 1,
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8,
    width: 100,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  slideText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
    fontSize: 12,
    textAlign: 'center',
  },
});
