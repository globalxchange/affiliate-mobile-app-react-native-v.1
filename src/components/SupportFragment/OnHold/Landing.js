import React, {useState, useContext} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import PasswordForm from './PasswordForm';
import {SupportContext} from '../../../contexts/SupportContext';

const Landing = ({setDevLoggedIn}) => {
  const [showPassForm, setShowPassForm] = useState(false);
  const {setActiveTab} = useContext(SupportContext);

  if (showPassForm) {
    return <PasswordForm setDevLoggedIn={setDevLoggedIn} />;
  }

  return (
    <View style={styles.container}>
      <Text style={styles.header}>
        We Are Currently Adding Some More Tender Love & Care To This Page.
        OnHold Will Be Ready Soon.
      </Text>
      <View style={styles.buttonContainer}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => setActiveTab({title: 'Chats'})}>
          <Text style={styles.buttonText}>I Still Need Support</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => setShowPassForm(true)}>
          <Text style={styles.buttonText}>I'm A Developer</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Landing;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-evenly',
    paddingHorizontal: 35,
  },
  header: {
    textAlign: 'center',
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
    fontSize: 18,
  },
  buttonContainer: {
    alignItems: 'center',
  },
  button: {
    backgroundColor: 'white',
    paddingVertical: 10,
    borderRadius: 4,
    width: 200,
    marginBottom: 20,
  },
  buttonText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
  },
});
