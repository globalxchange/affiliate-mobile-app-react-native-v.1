import React, {Component} from 'react';
import {Text, View, StyleSheet} from 'react-native';

class Counter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      minutes: 3,
      seconds: 0,
      isCompleted: false,
    };
  }

  componentDidMount() {
    this.startTimer();
  }

  componentDidUpdate(prevProps) {
    const {time} = this.props;

    if (time && time !== 0 && prevProps.time !== time) {
      console.log('Time Changed', time);
      this.state.minutes = parseInt(time / 60);
      this.state.seconds = time % 60;
      clearInterval(this.myInterval);
      this.startTimer();
    }
  }

  componentWillUnmount() {
    clearInterval(this.myInterval);
  }

  startTimer = () => {
    this.myInterval = setInterval(() => {
      const {seconds, minutes} = this.state;

      if (seconds > 0) {
        this.setState(({seconds}) => ({
          seconds: seconds - 1,
        }));
      }
      if (seconds === 0) {
        if (minutes === 0) {
          clearInterval(this.myInterval);
          this.setState({isCompleted: true});
        } else {
          this.setState(({minutes}) => ({
            minutes: minutes - 1,
            seconds: 59,
          }));
        }
      }
    }, 1000);
  };

  render() {
    const {minutes, seconds, isCompleted} = this.state;

    return (
      <View style={styles.getOnHoldContainer}>
        {isCompleted ? (
          <Text style={styles.completedMessage}>
            You Will Receive Our Call Shortly
          </Text>
        ) : (
          <View style={styles.countDownContainer}>
            <View style={styles.timeContainer}>
              <Text style={styles.time}>
                {minutes.toString().padStart(2, '0')}
              </Text>
              <Text style={styles.timeLabel}>Minutes</Text>
            </View>
            <View style={styles.separator}>
              <Text style={styles.time}>:</Text>
            </View>
            <View style={styles.timeContainer}>
              <Text style={styles.time}>
                {seconds.toString().padStart(2, '0')}
              </Text>
              <Text style={styles.timeLabel}>Seconds</Text>
            </View>
          </View>
        )}
      </View>
    );
  }
}

export default Counter;

const styles = StyleSheet.create({
  getOnHoldContainer: {
    borderWidth: 2,
    borderColor: '#08152D',
    height: 150,
    width: 150,
    borderRadius: 75,
    justifyContent: 'center',
    alignItems: 'center',
  },
  countDownContainer: {
    flexDirection: 'row',
  },
  timeContainer: {
    alignItems: 'center',
  },
  time: {
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
    fontSize: 28,
  },
  timeLabel: {
    fontFamily: 'Montserrat-Bold',
    color: '#788995',
    fontSize: 8,
  },
  separator: {paddingHorizontal: 5},
  completedMessage: {
    textAlign: 'center',
    paddingHorizontal: 15,
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
  },
});
