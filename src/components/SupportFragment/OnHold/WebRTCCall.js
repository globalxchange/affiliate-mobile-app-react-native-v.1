import React, {useState, useRef, useContext, useEffect} from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import {supportSocketUrl} from '../../../configs';
import {
  RTCPeerConnection,
  mediaDevices,
  RTCSessionDescription,
  RTCIceCandidate,
} from 'react-native-webrtc';
import {SupportContext} from '../../../contexts/SupportContext';
import io from 'socket.io-client';
import InCallManager from 'react-native-incall-manager';
import {useNavigation} from '@react-navigation/native';

const peerConfig = {iceServers: [{url: 'stun:stun.l.google.com:19302'}]};

const WebRTCCall = ({setWaitingTime}) => {
  const navigation = useNavigation();

  const {
    selectedIssueType,
    selectedNature,
    selectedTxnType,
    setStream,
    setIsCallConnected,
    shouldHangup,
  } = useContext(SupportContext);

  const [reRender, setReRender] = useState(false);

  const socketID = useRef();
  const socket = useRef();
  const pcRef = useRef();
  const candidateList = useRef([]);
  const callerSocket = useRef();

  const onMediaStartRecording = (stream) => {
    // setLocalStream(stream);
    pcRef.current.addStream(stream);
  };

  const onMediaRecordingFailure = (stream) => {
    console.log('Media Recording Failed', stream);
  };

  const requestCall = () => {
    const payload = {
      selectedIssueType: selectedIssueType.title,
      selectedNature: selectedNature.title,
      selectedTxnType: selectedTxnType.title,
    };
    sendToServer('requestCall', payload);
  };

  const onAnswerHandler = () => {
    console.log('Answering Offer');
    pcRef.current.createAnswer({offerToReceiveVideo: 1}).then((sdp) => {
      // console.log('Answer SDP', JSON.stringify(sdp));
      sendToServer('acceptCall', {to: callerSocket.current, sdp});
      pcRef.current.setLocalDescription(sdp);
    });
    setIsCallConnected(true);
  };

  const sendToServer = (messageType, payload) => {
    socket.current.emit(messageType, {socketID: socketID.current, payload});
  };

  useEffect(() => {
    socket.current = io(supportSocketUrl, {query: 'admin=false'});

    socket.current.on('connectionSuccess', (data) => {
      console.log('Socket Connected', data);
      socketID.current = data.success;
    });

    socket.current.on('receiveCall', (data) => {
      const {sdp, from} = data;
      console.log('Remote SDP', sdp);
      pcRef.current.setRemoteDescription(new RTCSessionDescription(sdp));
      callerSocket.current = from;
      setReRender(!reRender);
    });

    socket.current.on('onCandidate', (candidate) => {
      candidateList.current = [...candidateList.current, candidate];
    });

    socket.current.on('adminCallConnected', () => {
      console.log('Call COnnectted');
      try {
        pcRef.current.addIceCandidate(
          new RTCIceCandidate(candidateList.current[0]),
        );
      } catch (error) {
        console.log('Error on adding candidate', error);
      }
    });

    socket.current.on('waitingTimeUpdated', (time) => {
      setWaitingTime(parseFloat(time || 0));
    });

    socket.current.on('hangupCall', () => {
      setIsCallConnected(false);
      pcRef.current.close();
      socket.current.disconnect();
      navigation.goBack();
    });

    pcRef.current = new RTCPeerConnection(peerConfig);

    pcRef.current.onicecandidate = (e) => {
      // if (e.candidate) {
      //   sendToServer('onCandidate', {
      //     to: callerSocket.current,
      //     candidate: e.candidate,
      //   });
      //   // console.log('OnIceCandidate', JSON.stringify(e.candidate));
      // }
    };

    pcRef.current.oniceconnectionstatechange = (e) => {
      // console.log('oniceconnectionstatechange', e);
    };

    pcRef.current.onaddstream = (e) => {
      console.log('onaddstreamevent', e);

      setStream(e.stream);
      InCallManager.start({media: 'audio'});
    };

    mediaDevices.enumerateDevices().then((sourceInfos) => {
      console.log(sourceInfos);

      const constraints = {
        audio: true,
        video: false,
      };

      mediaDevices
        .getUserMedia(constraints)
        .then(onMediaStartRecording)
        .catch(onMediaRecordingFailure);
    });

    setTimeout(() => requestCall(), 5000);

    return () => {
      sendToServer('hangupCall', {target: callerSocket.current});
      pcRef.current.close();
      socket.current.disconnect();
    };
  }, []);

  useEffect(() => {
    if (shouldHangup) {
      sendToServer('hangupCall', {target: callerSocket.current});
      setIsCallConnected(false);
      pcRef.current.close();
      socket.current.disconnect();
      navigation.goBack();
    }
  }, [shouldHangup]);

  return (
    <>
      {callerSocket.current && (
        <View style={styles.container}>
          <Text style={styles.callingText}>
            Our Support Engineer Is Calling You
          </Text>
          <TouchableOpacity
            style={styles.answerButton}
            onPress={onAnswerHandler}>
            <Text style={styles.buttonText}>Answer</Text>
          </TouchableOpacity>
        </View>
      )}
    </>
  );
};

export default WebRTCCall;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    borderTopEndRadius: 10,
    borderTopStartRadius: 10,
    alignItems: 'center',
  },
  callingText: {
    color: '#08152D',
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold',
    marginTop: 20,
  },
  answerButton: {
    backgroundColor: '#08152D',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 'auto',
    marginBottom: 'auto',
    width: 100,
    height: 100,
    borderRadius: 50,
  },
  buttonText: {
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold',
    color: 'white',
  },
});
