import React, {useContext} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, FlatList} from 'react-native';
import {SupportContext} from '../../../contexts/SupportContext';
import {issueType} from './onHoldData';
import GetOnHoldAction from './GetOnHoldAction';

const OnHoldView = () => {
  const {
    selectedIssueType,
    selectedNature,
    selectedTxnType,
    setSelectedIssueType,
    setSelectedNature,
    setSelectedTxnType,
  } = useContext(SupportContext);

  return (
    <>
      <View style={styles.container}>
        <Text style={styles.header}>What Best Describe Your Issue?</Text>
        <FlatList
          style={styles.issueList}
          horizontal
          showsHorizontalScrollIndicator={false}
          data={selectedIssueType || issueType}
          keyExtractor={(item) => item.title}
          renderItem={({item, index}) => (
            <IssueItem
              onSelect={() => setSelectedIssueType([item])}
              text={item.title}
            />
          )}
        />
        {selectedIssueType && (
          <>
            <Text style={styles.header}>Nature Of Transaction?</Text>
            <FlatList
              style={styles.issueList}
              horizontal
              showsHorizontalScrollIndicator={false}
              data={selectedNature || selectedIssueType[0].natures}
              keyExtractor={(item) => item.title}
              renderItem={({item, index}) => (
                <IssueItem
                  onSelect={() => setSelectedNature([item])}
                  text={item.title}
                />
              )}
            />
          </>
        )}
        {selectedNature && (
          <>
            <Text style={styles.header}>
              What Type Of Transaction Were You Trying Make?
            </Text>
            <FlatList
              style={styles.issueList}
              horizontal
              showsHorizontalScrollIndicator={false}
              data={selectedTxnType || selectedNature[0].txnTypes}
              keyExtractor={(item) => item}
              renderItem={({item, index}) => (
                <IssueItem
                  onSelect={() => setSelectedTxnType([item])}
                  text={item}
                />
              )}
            />
          </>
        )}
      </View>
      <GetOnHoldAction />
    </>
  );
};
const IssueItem = ({text, onSelect}) => (
  <TouchableOpacity style={styles.issueItem} onPress={onSelect}>
    <Text style={styles.issueText}>{text}</Text>
  </TouchableOpacity>
);

export default OnHoldView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingBottom: 20,
  },
  header: {
    fontFamily: 'Montserrat-Bold',
    color: 'white',
    fontSize: 11,
    marginTop: 25,
  },
  issueList: {
    marginTop: 10,
    flexGrow: 0,
  },
  issueItem: {
    backgroundColor: 'white',
    justifyContent: 'center',
    paddingHorizontal: 20,
    paddingVertical: 5,
    borderRadius: 4,
    minHeight: 32,
    marginRight: 10,
  },
  issueText: {
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
    fontSize: 10,
    maxWidth: 130,
    textAlign: 'center',
  },
});
