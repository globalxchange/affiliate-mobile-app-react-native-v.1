import React, {useContext, useState, useEffect} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {SupportContext} from '../../../contexts/SupportContext';
import Counter from './Counter';
import WebRTCCall from './WebRTCCall';

const GetOnHoldAction = () => {
  const {
    selectedIssueType,
    selectedNature,
    selectedTxnType,
    requestForCall,
    setRequestForCall,
  } = useContext(SupportContext);

  const [isGetOnHoldEnable, setIsGetOnHoldEnable] = useState(false);
  const [step, setStep] = useState(0);
  const [waitingTime, setWaitingTime] = useState(3 * 60);

  const renderComponent = () => {
    switch (step) {
      case 0:
        return (
          <TouchableOpacity
            disabled={!isGetOnHoldEnable}
            onPress={() => setStep(1)}>
            <View
              style={[
                styles.getOnHoldContainer,
                isGetOnHoldEnable && styles.buttonEnabled,
              ]}>
              <Text style={styles.getOnHoldText}>Get OnHold</Text>
            </View>
          </TouchableOpacity>
        );
      case 1:
        return (
          <View style={styles.phonConfirmContainer}>
            <Text style={styles.header}>
              Do You Want Us Call Right Now Or Do You Want A Scheduled Callback?
            </Text>
            <View style={styles.actionColumnContainer}>
              <TouchableOpacity
                style={[styles.columnButton, {marginBottom: 15}]}>
                <Text style={styles.buttonText}>Schedule</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.columnButton}
                onPress={() => {
                  setStep(2);
                  setRequestForCall(true);
                }}>
                <Text style={styles.buttonText}>CALL ME NOW!!</Text>
              </TouchableOpacity>
            </View>
          </View>
        );
      case 2:
        return <Counter time={waitingTime} />;
    }
  };

  useEffect(() => {
    setIsGetOnHoldEnable(
      selectedIssueType && selectedNature && selectedTxnType ? true : false,
    );
  }, [selectedIssueType, selectedNature, selectedTxnType]);

  return (
    <View style={styles.actionContainer}>
      {renderComponent()}
      {requestForCall && <WebRTCCall setWaitingTime={setWaitingTime} />}
    </View>
  );
};

export default GetOnHoldAction;

const styles = StyleSheet.create({
  actionContainer: {
    backgroundColor: 'white',
    borderTopEndRadius: 10,
    borderTopStartRadius: 10,
    padding: 25,
    alignItems: 'center',
    height: 230,
    justifyContent: 'center',
  },
  getOnHoldContainer: {
    borderWidth: 2,
    borderColor: '#08152D',
    height: 150,
    width: 150,
    borderRadius: 75,
    justifyContent: 'center',
    alignItems: 'center',
    opacity: 0.5,
  },
  buttonEnabled: {opacity: 1},
  getOnHoldText: {
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
    fontSize: 15,
  },
  phonConfirmContainer: {
    flex: 1,
    padding: 15,
    justifyContent: 'space-between',
  },
  header: {
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
    textAlign: 'center',
    fontSize: 16,
  },
  phoneNumber: {
    fontFamily: 'Montserrat-Bold',
    color: '#788995',
    textAlign: 'center',
    fontSize: 24,
  },
  actionRowContainer: {flexDirection: 'row'},
  actionColumnContainer: {alignItems: 'center'},
  rowButton: {
    backgroundColor: '#08152D',
    flex: 1,
    borderRadius: 6,
    height: 35,
    justifyContent: 'center',
  },
  columnButton: {
    backgroundColor: '#08152D',
    flex: 1,
    borderRadius: 6,
    maxHeight: 35,
    justifyContent: 'center',
    maxWidth: 150,
    width: 150,
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Montserrat-Bold',
    fontSize: 14,
    textAlign: 'center',
  },
});
