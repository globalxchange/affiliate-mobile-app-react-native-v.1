import React, {useContext} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {SupportContext} from '../../../contexts/SupportContext';
import AppStatusBar from '../../AppStatusBar';

const OnCallScreen = () => {
  const {stream, isCallConnected, setShouldHangUp} = useContext(SupportContext);

  return (
    <>
      {isCallConnected && (
        <View style={styles.container}>
          <AppStatusBar backgroundColor="#08152D" barStyle="light-content" />
          <View style={styles.callingContainer}>
            {stream ? (
              <View style={styles.callView}>
                <Text style={styles.callerName}>John Doe</Text>
                <Text style={styles.callingActivity}>Calling</Text>
                <View style={styles.userImageContainer}>
                  <Image
                    style={styles.image}
                    resizeMode="cover"
                    source={require('../../../assets/call-images.png')}
                  />
                </View>
                <View style={styles.callActionContainer}>
                  <TouchableOpacity onPress={() => setShouldHangUp(true)}>
                    <Image
                      style={styles.hangUp}
                      resizeMode="contain"
                      source={require('../../../assets/hang-up-icon.png')}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            ) : (
              <View style={{padding: 15}}>
                <Text style={styles.connectingText}>Connecting...</Text>
              </View>
            )}
          </View>
        </View>
      )}
    </>
  );
};

export default OnCallScreen;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    backgroundColor: '#08152D',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    zIndex: 1,
  },
  callingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  connectingText: {
    fontSize: 22,
    textAlign: 'center',
    color: 'white',
    fontFamily: 'Montserrat-Bold',
  },
  callView: {},
  callerName: {
    fontFamily: 'Montserrat-Bold',
    textAlign: 'center',
    color: 'white',
    fontSize: 22,
    marginBottom: 5,
  },
  callingActivity: {
    fontFamily: 'Montserrat',
    textAlign: 'center',
    color: 'white',
    marginBottom: 20,
  },
  userImageContainer: {},
  image: {height: 150, width: 150, borderRadius: 75},
  callActionContainer: {
    marginTop: 20,
    alignItems: 'center',
  },
  hangUp: {
    width: 70,
    height: 70,
  },
});
