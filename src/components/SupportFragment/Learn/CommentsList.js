import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';

const CommentsList = () => {
  return (
    <View style={styles.container}>
      {/* <View style={styles.commentInputContainer}>
        <TextInput
          style={styles.commentInput}
          placeholder="Write Comment...!"
          placeholderTextColor="#878788"
        />
        <TouchableOpacity style={styles.sendButton}>
          <Image
            style={styles.sendIcon}
            resizeMode="contain"
            source={require('../../../assets/send-icon.png')}
          />
        </TouchableOpacity>
      </View>
      <FlatList
        data={Array(8).fill(1)}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item, index) => index.toString()}
        renderItem={() => (
          <View style={styles.commentItem}>
            <Image style={styles.commentAvatar} />
            <Text style={styles.commentText}>
              In This Episode We Are Thrilled To Award Larry Bird With 1 Billion
              Dollars In Bitcoin
            </Text>
          </View>
        )}
      /> */}
      <Text style={styles.comingSoonText}>
        Soon You Will Be Able To Comment And Like
      </Text>
    </View>
  );
};

export default CommentsList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  commentInputContainer: {
    flexDirection: 'row',
    paddingHorizontal: 25,
    alignItems: 'center',
    borderColor: '#E9E8E8',
    borderWidth: 1,
  },
  commentInput: {
    flex: 1,
    fontFamily: 'Montserrat-SemiBold',
    color: 'black',
  },
  sendButton: {
    width: 30,
    height: 30,
    padding: 4,
  },
  sendIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  commentItem: {
    paddingHorizontal: 25,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 8,
    borderBottomColor: '#E9E8E8',
    borderBottomWidth: 1,
  },
  commentAvatar: {
    width: 24,
    height: 24,
    backgroundColor: '#C4C4C4',
    borderRadius: 12,
  },
  commentText: {
    flex: 1,
    marginLeft: 15,
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
  comingSoonText: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 50,
  },
});
