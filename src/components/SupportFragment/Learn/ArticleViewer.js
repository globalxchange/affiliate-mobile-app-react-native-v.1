import React from 'react';
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import HTMLView from 'react-native-htmlview';
import * as WebBrowser from 'expo-web-browser';
import ThemeData from '../../../configs/ThemeData';

const ArticleViewer = ({selectedArticle, onClose}) => {
  // console.log('selectedArticle', selectedArticle);

  const onLinkPress = (url) => {
    WebBrowser.openBrowserAsync(url);
  };

  const messageViewTextStyles = {
    style: {color: '#9A9A9A', fontFamily: 'Montserrat'},
  };

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <TouchableOpacity style={styles.backButton} onPress={onClose}>
          <Image
            style={styles.backIcon}
            resizeMode="contain"
            source={require('../../../assets/back-arrow.png')}
          />
        </TouchableOpacity>
        <Text style={styles.title}>{selectedArticle.title}</Text>
      </View>
      <ScrollView
        style={styles.scrollContainer}
        showsVerticalScrollIndicator={false}>
        <HTMLView
          value={selectedArticle?.desc || DEMO_TEXT}
          stylesheet={sentMessageView}
          onLinkPress={onLinkPress}
          textComponentProps={messageViewTextStyles}
        />
      </ScrollView>
    </View>
  );
};

export default ArticleViewer;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-Bold',
    paddingHorizontal: 60,
    textAlign: 'center',
    flex: 1,
  },
  backButton: {
    position: 'absolute',
    left: 0,
    zIndex: 1,
  },
  backIcon: {
    width: 40,
    height: 30,
  },
  scrollContainer: {
    marginTop: 20,
  },
});

const sentMessageView = StyleSheet.create({
  a: {
    fontWeight: 'bold',
    textDecorationLine: 'underline',
  },
});

const DEMO_TEXT = `Lorem ipsum, dolor sit amet consectetur adipisicing elit. Hic quibusdam sit atque at commodi rerum quidem molestiae, fugit doloribus, omnis nemo assumenda quisquam qui, aspernatur neque aperiam consequatur reiciendis ab!`;
