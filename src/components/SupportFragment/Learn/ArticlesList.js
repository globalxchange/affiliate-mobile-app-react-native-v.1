import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import LoadingAnimation from '../../LoadingAnimation';
import ArticleViewer from './ArticleViewer';

const ArticlesList = ({articleList, setHideHeader, articleData}) => {
  const [selectedArticle, setSelectedArticle] = useState();

  useEffect(() => {
    if (articleData) {
      setSelectedArticle(articleData);
    }
  }, [articleData]);

  const onCloseHandler = () => {
    setSelectedArticle();
    setHideHeader(false);
  };

  const onItemSelected = (item) => {
    setHideHeader(true);
    setSelectedArticle(item);
  };

  if (selectedArticle) {
    return (
      <ArticleViewer
        selectedArticle={selectedArticle}
        onClose={onCloseHandler}
      />
    );
  }

  return (
    <View style={styles.container}>
      {articleList ? (
        <FlatList
          showsVerticalScrollIndicator={false}
          data={articleList}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => (
            <TouchableOpacity
              onPress={() => onItemSelected(item)}
              style={styles.videoItem}>
              <View style={styles.iconContainer}>
                <Image
                  style={styles.icon}
                  resizeMode="contain"
                  source={{uri: item.icon}}
                />
              </View>
              <View style={styles.itemDetails}>
                <Text style={styles.detailsTitle}>{item.title}</Text>
                <Text style={styles.detailsDesc}>{item.desc}</Text>
              </View>
            </TouchableOpacity>
          )}
          ListEmptyComponent={
            <Text style={styles.emptyText}>No Articles Found</Text>
          }
        />
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default ArticlesList;

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    flex: 1,
  },
  videoItem: {
    flexDirection: 'row',
    paddingLeft: 25,
    marginBottom: 20,
  },
  iconContainer: {
    backgroundColor: '#08152D',
    width: 75,
    height: 75,
    padding: 12,
  },
  icon: {
    flex: 1,
    height: null,
    width: null,
  },
  itemDetails: {
    flex: 1,
    paddingHorizontal: 15,
    borderTopColor: '#E9E8E8',
    borderTopWidth: 1,
    borderBottomColor: '#E9E8E8',
    borderBottomWidth: 1,
    justifyContent: 'center',
  },
  detailsTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
  },
  detailsDesc: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 10,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  emptyText: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    marginTop: 40,
    fontSize: 18,
  },
});
