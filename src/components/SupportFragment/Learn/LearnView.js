import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import PublicationsLists from './PublicationsLists';
import LearnFragment from './LearnFragment';
import CategoriesList from './CategoriesList';

const LearnView = ({videoData, articleData}) => {
  const [selectedComponent, setSelectedComponent] = useState('List');
  const [selectedPublication, setSelectedPublication] = useState('');
  const [selectedLearnCategory, setSelectedLearnCategory] = useState('');

  useEffect(() => {
    // console.log('videoData', videoData);
    // console.log('articleData', articleData);

    if (videoData || articleData) {
      setSelectedComponent('Learn');
      if (videoData) {
        setSelectedLearnCategory(videoData.selectedCategory);
      } else {
        setSelectedLearnCategory(articleData.selectedCategory);
      }
    }
  }, [videoData, articleData]);

  const renderComponent = () => {
    switch (selectedComponent) {
      case 'List':
        return (
          <PublicationsLists
            setSelectedPublication={setSelectedPublication}
            openCategory={() => setSelectedComponent('Categories')}
          />
        );

      case 'Categories':
        return (
          <CategoriesList
            selectedPublication={selectedPublication}
            setSelectedLearnCategory={setSelectedLearnCategory}
            onNext={() => setSelectedComponent('Learn')}
          />
        );

      case 'Learn':
        return (
          <LearnFragment
            articleData={articleData}
            videoData={videoData}
            selectedLearnCategory={selectedLearnCategory}
          />
        );

      default:
        return (
          <PublicationsLists
            setSelectedPublication={setSelectedPublication}
            openCategory={() => setSelectedComponent('Categories')}
          />
        );
    }
  };

  return <View style={styles.container}>{renderComponent()}</View>;
};

export default LearnView;

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: 'white'},
});
