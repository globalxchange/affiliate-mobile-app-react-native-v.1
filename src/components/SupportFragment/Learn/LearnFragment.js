import Axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ThemeData from '../../../configs/ThemeData';
import {AppContext} from '../../../contexts/AppContextProvider';
import ArticlesList from './ArticlesList';
import VideosList from './VideosList';

const LearnFragment = ({selectedLearnCategory, articleData, videoData}) => {
  const {isVideoFullScreen} = useContext(AppContext);

  const [selectedType, setSelectedType] = useState('Article');
  const [videoList, setVideoList] = useState();
  const [articleList, setArticleList] = useState();
  const [hideHeader, setHideHeader] = useState(false);

  useEffect(() => {
    if (articleData) {
      setSelectedType('Article');
    } else if (videoData) {
      setSelectedType('Videos');
    }
  }, [articleData, videoData]);

  useEffect(() => {
    Axios.get('https://fxagency.apimachine.com/video/category', {
      params: {category: selectedLearnCategory?._id || ''},
    })
      .then((resp) => {
        const {data} = resp;

        if (data.status) {
          setVideoList(data.data || []);
        } else {
          setVideoList([]);
        }
      })
      .catch((error) => {
        console.log('Error on getting Video List');
      });

    Axios.get('https://fxagency.apimachine.com/article/category', {
      params: {category: selectedLearnCategory?._id || ''},
    })
      .then((resp) => {
        const {data} = resp;

        if (data.status) {
          setArticleList(data.data || []);
        } else {
          setArticleList([]);
        }
      })
      .catch((error) => {
        console.log('Error on getting Article List');
      });

    // console.log('selectedLearnCategory', selectedLearnCategory);
  }, [selectedLearnCategory]);

  return (
    <View style={styles.container}>
      <View style={{display: isVideoFullScreen ? 'none' : 'flex'}}>
        <View style={styles.headerContainer}>
          <Text style={styles.header}>{selectedLearnCategory?.title}</Text>
          <Text style={styles.noOfPosts}>{videoList?.length || 0} Posts</Text>
        </View>
      </View>
      <Text
        numberOfLines={4}
        style={[styles.desc, {display: isVideoFullScreen ? 'none' : 'flex'}]}>
        {selectedLearnCategory?.cv}
      </Text>
      <View style={styles.mainContainer}>
        <View
          style={[
            styles.fragHeaderContainer,
            {display: isVideoFullScreen || hideHeader ? 'none' : 'flex'},
          ]}>
          <Text
            style={[
              styles.fragHeader,
              selectedType === 'Article' && {fontFamily: 'Montserrat-Bold'},
            ]}
            onPress={() => setSelectedType('Article')}>
            Article
          </Text>
          <Text style={styles.separator}>|</Text>
          <Text
            style={[
              styles.fragHeader,
              selectedType === 'Videos' && {fontFamily: 'Montserrat-Bold'},
            ]}
            onPress={() => setSelectedType('Videos')}>
            Videos
          </Text>
        </View>
        {selectedType === 'Article' ? (
          <ArticlesList
            setHideHeader={setHideHeader}
            articleList={articleList}
            articleData={articleData?.articleData}
          />
        ) : (
          <VideosList videoList={videoList} videoData={videoData?.videoData} />
        )}
      </View>
    </View>
  );
};

export default LearnFragment;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    paddingHorizontal: 25,
    paddingTop: 25,
  },
  header: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 18,
  },
  noOfPosts: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
  desc: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: 'Montserrat',
    fontSize: 11,
    marginTop: 10,
    paddingHorizontal: 25,
    paddingBottom: 25,
  },
  mainContainer: {
    backgroundColor: 'white',
    flex: 1,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  fragHeaderContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 25,
    paddingTop: 25,
  },
  fragHeader: {
    fontFamily: 'Montserrat',
    fontSize: 16,
    color: '#08152D',
  },
  separator: {
    fontFamily: 'Montserrat-Bold',
    fontSize: 16,
    color: '#08152D',
    paddingHorizontal: 8,
  },
});
