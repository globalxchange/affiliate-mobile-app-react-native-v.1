import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {timeParserExpanded} from '../../../utils';
import LoadingAnimation from '../../LoadingAnimation';
import VideoPlayView from './VideoPlayView';

const VideosList = ({videoList, videoData}) => {
  const [showPlayer, setShowPlayer] = useState(false);
  const [selectedVideo, seSelectedVideo] = useState();

  useEffect(() => {
    if (videoData) {
      setShowPlayer(true);
      seSelectedVideo(videoData);
    }
  }, [videoData]);

  const onVideSelected = (item) => {
    seSelectedVideo(item);
    setShowPlayer(true);
  };

  if (showPlayer) {
    return (
      <VideoPlayView
        videoList={videoList}
        selectedVideo={selectedVideo}
        seSelectedVideo={seSelectedVideo}
        onBack={() => {
          setShowPlayer(false);
        }}
      />
    );
  }

  // console.log('VideoList', videoList);

  return (
    <View style={styles.container}>
      {videoList ? (
        <>
          <Text style={styles.header}>Most Popular</Text>
          <FlatList
            style={styles.videoList}
            showsHorizontalScrollIndicator={false}
            horizontal
            data={videoList}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item}) => (
              <VideoItem item={item} onVideSelected={onVideSelected} />
            )}
          />
          <Text style={styles.header}>All Videos</Text>
          <FlatList
            style={styles.videoList}
            showsHorizontalScrollIndicator={false}
            horizontal
            data={videoList}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item}) => (
              <VideoItem item={item} onVideSelected={onVideSelected} />
            )}
          />
        </>
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

const VideoItem = ({item, onVideSelected}) => {
  return (
    <TouchableOpacity
      style={styles.videoItem}
      onPress={() => onVideSelected(item)}>
      <View style={styles.coverContainer}>
        <Image
          style={styles.coverImage}
          source={{uri: item.image}}
          resizeMode="cover"
        />
        <Image
          style={styles.playIcon}
          source={require('../../../assets/play-icon.png')}
          resizeMode="contain"
        />
      </View>
      <View style={styles.itemDetails}>
        <Text numberOfLines={1} style={styles.detailsTitle}>
          {item.title}
        </Text>
        <Text style={styles.detailsDesc}>
          {timeParserExpanded(Date.parse(item.updatedAt || ''))}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default VideosList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    paddingLeft: 25,
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
    marginTop: 20,
  },
  videoList: {
    marginTop: 10,
    paddingHorizontal: 25,
  },
  videoItem: {
    width: 180,
    marginRight: 10,
  },
  coverContainer: {
    backgroundColor: '#08152D',
    justifyContent: 'center',
    alignItems: 'center',
    height: 90,
    width: 180,
  },
  coverImage: {...StyleSheet.absoluteFill, zIndex: 0, height: 90, width: 180},
  playIcon: {
    width: 30,
    height: 30,
    zIndex: 1,
  },
  itemDetails: {
    paddingHorizontal: 15,
    paddingVertical: 8,
    borderColor: '#E9E8E8',
    borderWidth: 1,
    justifyContent: 'center',
  },
  detailsTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
  },
  detailsDesc: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 8,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
});
