import Axios from 'axios';
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {AGENCY_API_URL, APP_CODE} from '../../../configs';
import ThemeData from '../../../configs/ThemeData';
import LoadingAnimation from '../../LoadingAnimation';

const PublicationsLists = ({openCategory, setSelectedPublication}) => {
  const [searchInput, setSearchInput] = useState('');
  const [publicationsList, setPublicationsList] = useState();
  const [filteredList, setFilteredList] = useState();

  useEffect(() => {
    Axios.get(`${AGENCY_API_URL}/publication/appcode`, {
      params: {app_code: APP_CODE},
    })
      .then((resp) => {
        const {data} = resp;

        setPublicationsList(data.data || []);
      })
      .catch((error) => {});
  }, []);

  useEffect(() => {
    if (publicationsList) {
      setFilteredList(
        publicationsList.filter((item) =>
          item.name?.toLowerCase().includes(searchInput.toLowerCase()),
        ),
      );
    } else {
      setFilteredList([]);
    }
  }, [searchInput, publicationsList]);

  const onCategorySelected = (item) => {
    setSelectedPublication(item);
    openCategory();
  };

  if (!publicationsList) {
    return (
      <View style={styles.loadingContainer}>
        <LoadingAnimation />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.searchContainer}>
        <TextInput
          style={styles.searchInput}
          placeholder="Search Publications"
          value={searchInput}
          onChangeText={(text) => setSearchInput(text)}
          placeholderTextColor="#878788"
        />
        <Image
          style={styles.searchIcon}
          resizeMode="contain"
          source={require('../../../assets/search-icon-colored.png')}
        />
      </View>
      <FlatList
        showsVerticalScrollIndicator={false}
        style={styles.categoryList}
        data={filteredList}
        numColumns={2}
        keyExtractor={(item) => item.title}
        renderItem={({item, index}) => (
          <TouchableOpacity
            onPress={() => onCategorySelected(item)}
            style={[
              styles.categoryItem,
              index % 2 === 0 && {marginRight: 20},
              index === filteredList.length - 1 && {marginRight: 0},
              index > filteredList.length - 3 && {marginBottom: 120},
            ]}>
            <View style={styles.iconContainer}>
              <Image
                style={styles.categoryIcon}
                source={{uri: item.profile_pic}}
                resizeMode="contain"
              />
            </View>
            <Text style={styles.categoryTitle}>{item.name}</Text>
            <Text style={styles.categoryTagline}>{item.description}</Text>
          </TouchableOpacity>
        )}
        ListEmptyComponent={
          <Text style={styles.emptyText}>No Publications Found</Text>
        }
      />
    </View>
  );
};

export default PublicationsLists;

const styles = StyleSheet.create({
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  emptyText: {
    color: 'white',
    textAlign: 'center',
    fontFamily: 'Montserrat-SemiBold',
    marginTop: 50,
    fontSize: 16,
  },
  container: {
    paddingHorizontal: 25,
    paddingTop: 30,
  },
  searchContainer: {
    flexDirection: 'row',
    backgroundColor: 'white',
    paddingHorizontal: 20,
    borderRadius: 10,
    alignItems: 'center',
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
  },
  searchInput: {
    color: '#08152D',
    flex: 1,
    fontFamily: 'Montserrat-SemiBold',
    height: 40,
  },
  searchIcon: {
    width: 16,
  },
  categoryList: {
    marginTop: 25,
  },
  categoryItem: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    padding: 10,
    borderRadius: 10,
    marginBottom: 20,
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
  },
  iconContainer: {
    width: 45,
    height: 45,
    borderRadius: 22.5,
    borderColor: '#08152D',
    borderWidth: 1,
    overflow: 'hidden',
    padding: 8,
  },
  categoryIcon: {
    flex: 1,
    width: null,
    height: null,
  },
  categoryTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    marginVertical: 3,
  },
  categoryTagline: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 10,
  },
});
