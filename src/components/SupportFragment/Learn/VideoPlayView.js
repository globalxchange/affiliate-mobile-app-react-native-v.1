/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  BackHandler,
} from 'react-native';
import VideoPlayer from '../../VideoPlayer';
import CommentsList from './CommentsList';
import RelatedVideos from './RelatedVideos';

const VideoPlayView = ({
  selectedVideo,
  onBack,
  descHeight,
  videoList,
  seSelectedVideo,
}) => {
  const [selectedTab, setSelectedTab] = useState(tabs[0]);
  const [isVideoPlay, setIsVideoPlay] = useState(false);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', onBackHandler);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', onBackHandler);
    };
  }, []);

  const onBackHandler = () => {
    if (onBack) {
      onBack();
    }
    return true;
  };

  const renderTabComponent = () => {
    const payingIndex = videoList?.findIndex(
      (x) => x._id === selectedVideo._id,
    );

    const nextVideos = [...(videoList || [])];

    if (payingIndex > -1) {
      nextVideos.splice(payingIndex, 1);
    }

    switch (selectedTab) {
      case 'Comments':
        return <CommentsList />;
      case 'Category Videos':
        return (
          <RelatedVideos
            seSelectedVideo={seSelectedVideo}
            nextVideos={nextVideos}
          />
        );
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <TouchableOpacity onPress={onBack} style={styles.backButton}>
          <Image
            style={styles.backIcon}
            resizeMode="contain"
            source={require('../../../assets/back-arrow.png')}
          />
        </TouchableOpacity>
        <Text style={styles.header}>{selectedVideo?.title}</Text>
      </View>
      {isVideoPlay ? (
        <VideoPlayer
          offsetHeight={15}
          videoId={selectedVideo?.video || ''}
          isBrainVideo
          play
        />
      ) : (
        <TouchableOpacity
          onPress={() => setIsVideoPlay(true)}
          style={styles.videoContainer}>
          <Image
            style={styles.playIcon}
            resizeMode="contain"
            source={require('../../../assets/play-icon.png')}
          />
        </TouchableOpacity>
      )}
      <View style={styles.tabContainer}>
        {tabs.map((item) => (
          <Text
            key={item}
            style={[
              styles.tabItem,
              item === selectedTab && {
                fontFamily: 'Montserrat-Bold',
              },
            ]}
            onPress={() => setSelectedTab(item)}>
            {item}
          </Text>
        ))}
      </View>
      {renderTabComponent()}
    </View>
  );
};

export default VideoPlayView;

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: 'white', marginTop: -30},
  headerContainer: {
    justifyContent: 'center',
    marginBottom: 15,
    height: 30,
  },
  backButton: {
    position: 'absolute',
    left: 15,
    zIndex: 1,
  },
  backIcon: {
    width: 30,
  },
  header: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    paddingHorizontal: 50,
  },
  videoContainer: {
    backgroundColor: '#08152D',
    justifyContent: 'center',
    alignItems: 'center',
    height: 170,
  },
  playIcon: {
    width: 30,
  },
  tabContainer: {flexDirection: 'row', paddingVertical: 10},
  tabItem: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat',
    flex: 1,
    fontSize: 12,
  },
});

const tabs = ['Comments', 'Category Videos', 'Share'];
