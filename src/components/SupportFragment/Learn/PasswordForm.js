import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Keyboard,
  Platform,
} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import Animated, {
  useCode,
  set,
  Clock,
  interpolate,
} from 'react-native-reanimated';
import ReanimatedTimingHelper from '../../../utils/ReanimatedTimingHelper';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

const PasswordForm = ({setDevLoggedIn}) => {
  const [passwordInput, setPasswordInput] = useState('');
  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);

  const indices = useSafeAreaInsets();

  const chatKeyboardAnimation = useRef(new Animated.Value(0));

  const onKeyBoardShow = (e) => {
    if (Platform.OS === 'ios') {
      setKeyboardHeight(e.endCoordinates.height - indices.bottom);
    }
    setIsKeyboardOpen(true);
  };

  const onKeyBoardHide = () => {
    setIsKeyboardOpen(false);
  };

  useEffect(() => {
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
      onKeyBoardShow,
    );
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
      onKeyBoardHide,
    );
    return () => {
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
        onKeyBoardShow,
      );
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
        onKeyBoardHide,
      );
    };
  }, []);

  const onGandAccess = () => {
    if (!passwordInput) {
      return WToast.show({
        data: 'Please Input The Secret Password',
        position: WToast.position.TOP,
      });
    }

    if (passwordInput !== 'PleaseLemmeIn') {
      return WToast.show({
        data: "Nah.. You' Not A Dev",
        position: WToast.position.TOP,
      });
    }

    setDevLoggedIn(true);
  };

  useCode(
    () =>
      isKeyboardOpen
        ? [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ]
        : [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ],
    [isKeyboardOpen],
  );

  return (
    <View style={styles.container}>
      <Animated.View
        style={[
          styles.headerContainer,
          {
            opacity: interpolate(chatKeyboardAnimation.current, {
              inputRange: [0, 1],
              outputRange: [1, 0],
            }),
          },
        ]}>
        <Image
          style={styles.headerImage}
          source={require('../../../assets/emoji-looking.png')}
          resizeMode="contain"
        />
        <Text style={styles.header}>So Your A Developer Eh?</Text>
      </Animated.View>

      <Animated.View
        style={[
          styles.inputForm,
          {
            transform: [
              {
                translateY: interpolate(chatKeyboardAnimation.current, {
                  inputRange: [0, 1],
                  outputRange: [0, -keyboardHeight],
                }),
              },
            ],
          },
        ]}>
        <Text style={styles.inputHeader}>What Is The Secret Password?</Text>
        <TextInput
          style={styles.passwordInput}
          placeholder="*********"
          placeholderTextColor="#788995"
          secureTextEntry
          value={passwordInput}
          onChangeText={(text) => setPasswordInput(text)}
          returnKeyType="done"
        />
        <TouchableOpacity style={styles.button} onPress={onGandAccess}>
          <Text style={styles.buttonText}>Gain Access</Text>
        </TouchableOpacity>
      </Animated.View>
    </View>
  );
};

export default PasswordForm;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    alignItems: 'center',
    paddingHorizontal: 35,
    flex: 1,
    justifyContent: 'center',
    overflow: 'hidden',
  },
  header: {
    textAlign: 'center',
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
    fontSize: 18,
    marginTop: 30,
  },
  headerImage: {
    width: 100,
    height: 100,
  },
  inputForm: {
    backgroundColor: 'white',
    borderTopEndRadius: 10,
    borderTopStartRadius: 10,
    padding: 30,
    alignItems: 'center',
  },
  inputHeader: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 18,
  },
  passwordInput: {
    textAlign: 'center',
    height: 50,
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    marginVertical: 30,
    letterSpacing: 3,
    width: '100%',
    fontSize: 20,
  },
  button: {
    backgroundColor: '#08152D',
    paddingVertical: 10,
    borderRadius: 4,
    width: 200,
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
  },
});
