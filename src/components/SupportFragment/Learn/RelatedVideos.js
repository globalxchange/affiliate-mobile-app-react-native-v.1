import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';
import {timeParserExpanded} from '../../../utils';

const RelatedVideos = ({nextVideos, seSelectedVideo}) => {
  return (
    <View style={styles.container}>
      <FlatList
        style={styles.videoList}
        showsVerticalScrollIndicator={false}
        data={nextVideos}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <TouchableOpacity
            onPress={() => seSelectedVideo(item)}
            style={styles.videoItem}>
            <View style={styles.iconContainer}>
              <Image
                style={styles.icon}
                resizeMode="contain"
                source={{uri: item.image}}
              />
            </View>
            <View style={styles.itemDetails}>
              <Text style={styles.detailsTitle}>{item.title}</Text>
              <Text style={styles.detailsDesc}>
                {timeParserExpanded(Date.parse(item.updatedAt || ''))}
              </Text>
            </View>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

export default RelatedVideos;

const styles = StyleSheet.create({
  container: {flex: 1},
  videoList: {
    paddingVertical: 15,
  },
  videoItem: {
    flexDirection: 'row',
    paddingLeft: 25,
    marginBottom: 20,
  },
  iconContainer: {
    backgroundColor: '#08152D',
    width: 100,
    height: 75,
    padding: 30,
  },
  icon: {
    flex: 1,
    height: null,
    width: null,
  },
  itemDetails: {
    flex: 1,
    paddingHorizontal: 15,
    borderTopColor: '#E9E8E8',
    borderTopWidth: 1,
    borderBottomColor: '#E9E8E8',
    borderBottomWidth: 1,
    justifyContent: 'center',
  },
  detailsTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
  },
  detailsDesc: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 10,
  },
});
