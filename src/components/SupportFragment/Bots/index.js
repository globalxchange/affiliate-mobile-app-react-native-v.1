/* eslint-disable react-native/no-inline-styles */
import {useRoute} from '@react-navigation/native';
import Axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {AGENCY_API_URL, APP_CODE} from '../../../configs';
import {SupportContext} from '../../../contexts/SupportContext';
import Fragments from './Fragments';
import LandingPage from './LandingPage';
import RedirectModal from './RedirectModal';
import SubCategory from './SubCategory';
import TopCategory from './TopCategory';

const Bots = () => {
  const {params} = useRoute();

  const {isFullScreen} = useContext(SupportContext);

  const [showLanding, setShowLanding] = useState(true);
  const [activeCategory, setActiveCategory] = useState('');
  const [activeSubCategory, setActiveSubCategory] = useState({title: 'Add'});
  const [hideSubCategory, setHideSubCategory] = useState(false);
  const [learnSubMenu, setLearnSubMenu] = useState();

  useEffect(() => {
    if (params?.openMyLicenses) {
      setShowLanding(false);
      setActiveCategory({title: 'My Licenses'});
    }
  }, [params]);

  useEffect(() => {
    (async () => {
      Axios.get(`${AGENCY_API_URL}/publication/appcode`, {
        params: {app_code: APP_CODE},
      })
        .then((resp) => {
          const {data} = resp;
          // console.log('Resp On ', data);

          const list = data.data || [];

          const subMenu = list.map((x) => ({
            title: x.name,
            icon: {uri: x.profile_pic},
            id: x._id,
          }));

          setLearnSubMenu(subMenu);
        })
        .catch((error) => {
          console.log('Error On', error);
        });
    })();
  }, []);

  useEffect(() => {
    setHideSubCategory(false);
  }, [activeCategory]);

  if (showLanding) {
    return (
      <LandingPage
        onItemPress={(item) => {
          setActiveCategory(item);
          setShowLanding(false);
        }}
        learnSubMenu={learnSubMenu}
      />
    );
  }

  return (
    <View
      style={[
        styles.container,
        isFullScreen && {paddingTop: 0, paddingHorizontal: 0},
      ]}>
      <TopCategory
        activeCategory={activeCategory}
        setActiveCategory={setActiveCategory}
        setActiveSubCategory={setActiveSubCategory}
        learnSubMenu={learnSubMenu}
      />
      {isFullScreen || hideSubCategory || (
        <SubCategory
          activeCategory={activeCategory}
          activeSubCategory={activeSubCategory}
          setActiveSubCategory={setActiveSubCategory}
        />
      )}
      <Fragments
        activeCategory={activeCategory}
        activeSubCategory={activeSubCategory}
        setHideSubCategory={setHideSubCategory}
      />
      <RedirectModal />
    </View>
  );
};

export default Bots;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 30,
    paddingHorizontal: 20,
  },
});
