import React, {useContext} from 'react';
import {
  SectionList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeData from '../../../configs/ThemeData';
import {AppContext} from '../../../contexts/AppContextProvider';

const LandingPage = ({onItemPress, learnSubMenu}) => {
  const {isLoggedIn} = useContext(AppContext);

  const SECTIONS = [
    {
      title: 'Looking For Info?',
      data: [
        {
          title: 'My Network',
          disabled: !isLoggedIn,
        },
        {
          title: 'My Licenses',
          disabled: !isLoggedIn,
        },
        {
          title: 'Transactions',
          header: 'What Is The Type Of Transaction',
          subMenu: [
            {
              title: 'Add',
              icon: require('../../../assets/support-category-icons/add.png'),
            },
            {
              title: 'Send',
              icon: require('../../../assets/support-category-icons/send.png'),
            },
            {
              title: 'Trade',
              icon: require('../../../assets/support-category-icons/trade.png'),
            },
            {
              title: 'Invest',
              icon: require('../../../assets/support-category-icons/invest.png'),
            },
          ],
          disabled: !isLoggedIn,
        },
      ],
    },
    {
      title: 'Looking For Guidance?',
      data: [
        {
          title: 'Actions',
          subMenu: [
            {
              title: 'Add',
              icon: require('../../../assets/support-category-icons/add.png'),
            },
            {
              title: 'Send',
              icon: require('../../../assets/support-category-icons/send.png'),
            },
            {
              title: 'Trade',
              icon: require('../../../assets/support-category-icons/trade.png'),
            },
            {
              title: 'Invest',
              icon: require('../../../assets/support-category-icons/invest.png'),
            },
          ],
          disabled: !isLoggedIn,
        },
        {
          title: 'Learn',
          header: 'What Do You Want To Learn About?',
          subMenu: learnSubMenu,
        },
      ],
    },
  ];

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Hey I’m Eric</Text>
      <Text style={styles.subHeader}>The Affiliate Bot</Text>
      <Text style={styles.desc}>
        I was created to help you making lightning fast decisions as you grow
        your network. Please select one of the categories so I can best assist
        you.
      </Text>
      <SectionList
        showsVerticalScrollIndicator={false}
        stickySectionHeadersEnabled={false}
        sections={SECTIONS}
        keyExtractor={(item, index) => item + index}
        renderSectionHeader={({section: {title}}) => (
          <Text style={styles.sectionHeader}>{title}</Text>
        )}
        renderItem={({item}) => (
          <TouchableOpacity
            onPress={() => onItemPress(item)}
            style={styles.listItem}>
            <Text style={styles.itemText}>{item.title}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

export default LandingPage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 20,
    paddingTop: 30,
    paddingBottom: 20,
  },
  header: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 24,
    color: ThemeData.APP_MAIN_COLOR,
  },
  subHeader: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 14,
    marginTop: 2,
    marginBottom: 10,
  },
  desc: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 10,
    color: ThemeData.APP_MAIN_COLOR,
    lineHeight: 18,
  },
  sectionHeader: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    paddingTop: 40,
    backgroundColor: 'white',
  },
  listItem: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginTop: 20,
    padding: 20,
  },
  itemText: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
  },
});
