import Axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {GX_API_ENDPOINT} from '../../../../configs';
import {AppContext} from '../../../../contexts/AppContextProvider';
import {timeParserExpanded} from '../../../../utils';
import AsyncStorageHelper from '../../../../utils/AsyncStorageHelper';
import LoadingAnimation from '../../../LoadingAnimation';
import TxnView from './TxnView';

const TransactionAdd = () => {
  const {cryptoTableData} = useContext(AppContext);

  const [totalTransactionList, setTotalTransactionList] = useState();
  const [groupedList, setGroupedList] = useState();
  const [showGrouped, setShowGrouped] = useState(false);
  const [selectedTxn, setSelectedTxn] = useState();

  useEffect(() => {
    if (cryptoTableData) {
      (async () => {
        const email = await AsyncStorageHelper.getLoginEmail();
        Axios.get(
          `${GX_API_ENDPOINT}/coin/vault/service/path/deposit/txn/get`,
          {
            params: {email},
          },
        )
          .then((resp) => {
            const {data} = resp;
            // console.log('Transactions', data);

            const txns = data.txns || [];
            setTotalTransactionList(txns);

            const list = [];
            cryptoTableData.forEach((cryptoItem) => {
              const coinTxns = [];
              txns.forEach((txnItem) => {
                if (cryptoItem.coinSymbol === txnItem.coin) {
                  coinTxns.push(txnItem);
                }
              });

              if (coinTxns.length > 0) {
                list.push({...cryptoItem, txnsList: coinTxns});
              }
            });

            setGroupedList(list);
          })
          .catch((error) => {
            console.log('Error on getting transaction list', error);
          });
      })();
    }
  }, [cryptoTableData]);

  const onGroupItemSelected = (item) => {
    setTotalTransactionList([...item.txnsList]);
    setShowGrouped(false);
  };

  const onItemSelected = (item) => {
    setSelectedTxn(item);
  };

  if (selectedTxn) {
    return <TxnView selectedTxn={selectedTxn} />;
  }

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.header}>
          {showGrouped
            ? 'Which Of Your Assets Were Used In The Transaction?'
            : 'Recent Deposits'}
        </Text>
        {!showGrouped && (
          <Text
            onPress={() => setShowGrouped(true)}
            style={styles.headerButton}>
            I Don’t See It
          </Text>
        )}
      </View>
      {totalTransactionList ? (
        showGrouped ? (
          <FlatList
            style={styles.listContainer}
            showsVerticalScrollIndicator={false}
            data={groupedList}
            keyExtractor={(item, index) =>
              `${item.coinSymbol || item._id}${index}`
            }
            renderItem={({item}) => (
              <ListItem item={item} onPress={() => onGroupItemSelected(item)} />
            )}
            ListEmptyComponent={
              <Text style={styles.emptyText}>No Transactions Found</Text>
            }
          />
        ) : (
          <FlatList
            style={styles.listContainer}
            showsVerticalScrollIndicator={false}
            data={totalTransactionList}
            keyExtractor={(item, index) =>
              `${item.coinSymbol || item._id}${index}`
            }
            renderItem={({item}) => (
              <ListItem item={item} onPress={() => onItemSelected(item)} />
            )}
            ListEmptyComponent={
              <Text style={styles.emptyText}>No Transactions Found</Text>
            }
          />
        )
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

const ListItem = ({item, onPress}) => {
  const {cryptoTableData} = useContext(AppContext);

  let icon = require('../../../../assets/bitcoin-icon.png');
  let name = 'BTC';

  if (cryptoTableData) {
    const coin = cryptoTableData.find(
      (x) => x.coinSymbol === (item.coinSymbol || item.coin),
    );
    if (coin) {
      icon = {uri: coin.coinImage};
      name = coin.coinName;
    }
  }

  // console.log('item', item);

  return (
    <TouchableOpacity onPress={onPress} style={styles.listItem}>
      <Image source={icon} resizeMode="contain" style={styles.itemIcon} />
      <View style={styles.textContainer}>
        <Text style={styles.itemTitle}>
          {item.coinSymbol || `${item.coin} Deposit`}
        </Text>
        <Text style={styles.itemSubTitle}>
          {item.txnsList
            ? `${item.txnsList.length} Transactions`
            : timeParserExpanded(item.timestamp)}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default TransactionAdd;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  header: {
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 13,
    textDecorationLine: 'underline',
  },
  headerButton: {
    color: '#464B4E',
    fontFamily: 'Montserrat',
    fontSize: 13,
    textDecorationLine: 'underline',
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  listContainer: {
    marginTop: 20,
  },
  listItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
    borderColor: '#E7E7E7',
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  itemIcon: {
    width: 35,
    height: 35,
  },
  textContainer: {
    flex: 1,
    paddingLeft: 20,
  },
  itemTitle: {
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
  },
  itemSubTitle: {
    color: '#464B4E',
    fontFamily: 'Montserrat',
    fontSize: 11,
  },
  emptyText: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
    textAlign: 'center',
    color: '#464B4E',
  },
});
