/* eslint-disable react-native/no-inline-styles */
import React, {useState} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TouchableWithoutFeedback,
  Dimensions,
} from 'react-native';
import * as WebBrowser from 'expo-web-browser';

const {width} = Dimensions.get('window');

const LicenseItem = ({item, openUpgrade, noActions, style}) => {
  const [showPurchaseId, setShowPurchaseId] = useState(false);
  const [showLastPaymentId, setShowLastPaymentId] = useState(false);

  const onLicenseClick = () => {
    WebBrowser.openBrowserAsync(
      `https://liceneses.brokerapp.io/${item.license_code}`,
    );
  };

  return (
    <TouchableWithoutFeedback>
      <View style={[styles.itemContainer, style]}>
        <View style={styles.itemHeader}>
          <Text style={styles.itemTitle}>{item.name}</Text>
          {(item.license_status === 'active' ||
            item.license_status === 'grandfathered') && (
            <Image
              style={styles.checkIcon}
              resizeMode="contain"
              source={require('../../../../assets/green-check.png')}
            />
          )}
        </View>
        <View style={styles.detailsContainer}>
          <View style={styles.itemDetailsContainer}>
            <Text style={styles.detailsLabel}>Brand:</Text>
            <Text style={styles.detailsValue}>{item.client_app}</Text>
          </View>
          <View style={styles.itemDetailsContainer}>
            <Text style={styles.detailsLabel}>Product:</Text>
            <Text style={styles.detailsValue}>{item?.product_code}</Text>
          </View>
          <View style={styles.itemDetailsContainer}>
            <Text style={styles.detailsLabel}>License:</Text>
            <Text style={styles.detailsValue}>{item?.license_id}</Text>
          </View>
          <View style={styles.itemDetailsContainer}>
            {showPurchaseId || (
              <Text style={styles.detailsLabel}>Last Purchase ID:</Text>
            )}
            <Text
              onPress={() => setShowPurchaseId(!showPurchaseId)}
              style={[
                styles.detailsValue,
                showPurchaseId || {textDecorationLine: 'underline'},
              ]}>
              {showPurchaseId ? item?.last_purchase_id : 'Show'}
            </Text>
          </View>
          <View style={styles.itemDetailsContainer}>
            {showLastPaymentId || (
              <Text style={styles.detailsLabel}>Last Payment ID:</Text>
            )}
            <Text
              onPress={() => setShowLastPaymentId(!showLastPaymentId)}
              style={[
                styles.detailsValue,
                showLastPaymentId || {textDecorationLine: 'underline'},
              ]}>
              {showLastPaymentId ? item?.last_payment_id : 'Show'}
            </Text>
          </View>
          {noActions ? null : (
            <View style={styles.buttonContainer}>
              <TouchableOpacity
                onPress={onLicenseClick}
                style={styles.outlineButton}>
                <Text style={styles.outlineButtonText}>
                  License Certificate
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={openUpgrade}
                style={styles.filledButton}>
                <Text style={styles.filledButtonText}>Upgrade</Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default LicenseItem;

const styles = StyleSheet.create({
  itemContainer: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 16,
    marginRight: 20,
    width: width * 0.7,
  },
  itemHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  itemTitle: {
    flex: 1,
    marginRight: 20,
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 16,
  },
  checkIcon: {
    width: 24,
    height: 24,
  },
  itemDetailsContainer: {
    marginBottom: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  detailsLabel: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 12,
  },
  detailsValue: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    flex: 1,
    textAlign: 'right',
    fontSize: 12,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginTop: 10,
  },
  outlineButton: {
    flex: 1,
    borderColor: '#EBEBEB',
    borderWidth: 1,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  outlineButtonText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 10,
    textAlign: 'center',
  },
  filledButton: {
    flex: 1,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#08152D',
    marginLeft: 20,
  },
  filledButtonText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 10,
    textAlign: 'center',
  },
  detailsContainer: {
    marginTop: 'auto',
  },
});
