/* eslint-disable react-native/no-inline-styles */
import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {FlatList, Image, StyleSheet, View} from 'react-native';
import ThemeData from '../../../../configs/ThemeData';
import AsyncStorageHelper from '../../../../utils/AsyncStorageHelper';
import LoadingAnimation from '../../../LoadingAnimation';
import ViewHeader from '../MyNetwork/Lineage/ViewHeader';
import BrandItem from './BrandItem';
import LicenseList from './LicenseList';

const MyLicenses = ({userEmail, onBack}) => {
  const [brandsList, setBrandsList] = useState();
  const [activeBrand, setActiveBrand] = useState('');
  const [queryEmail, setQueryEmail] = useState('');

  useEffect(() => {
    (async () => {
      setQueryEmail(userEmail || (await AsyncStorageHelper.getLoginEmail()));
    })();
  }, [userEmail]);

  useEffect(() => {
    Axios.get('https://teller2.apimachine.com/admin/allBankers')
      .then(({data}) => {
        if (data.status) {
          const brands = data.data || [];

          setBrandsList([
            {
              bankerTag: 'All Licenses',
              profilePicURL: Image.resolveAssetSource(
                require('../../../../assets/buy-icon.png'),
              ).uri,
            },
            ...brands,
          ]);
          // console.log('data', data);
        } else {
          setBrandsList([]);
        }
      })
      .catch((error) => {
        setBrandsList([]);
      });
  }, []);

  useEffect(() => {
    if (brandsList) {
      setActiveBrand(brandsList[0]);
    }
  }, [brandsList]);

  return (
    <View style={styles.container}>
      <ViewHeader
        headerText="Select A Brand"
        disableBack={userEmail ? false : true}
        onBack={onBack}
      />
      <View style={styles.listContainer}>
        {brandsList ? (
          <View style={styles.brandListContainer}>
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              data={brandsList}
              keyExtractor={(item) => item._id}
              renderItem={({item}) => (
                <BrandItem
                  brandData={item}
                  isSelected={item.bankerTag === activeBrand.bankerTag}
                  onPress={() => setActiveBrand(item)}
                />
              )}
            />
          </View>
        ) : (
          <View style={{flex: 1, justifyContent: 'center'}}>
            <LoadingAnimation />
          </View>
        )}
        {activeBrand ? (
          <LicenseList activeBrand={activeBrand} userEmail={queryEmail} />
        ) : null}
      </View>
    </View>
  );
};

export default MyLicenses;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    color: '#464B4E',
    fontFamily: 'Montserrat-Bold',
    fontSize: 14,
  },
  listContainer: {
    flex: 1,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    padding: 20,
  },
  brandListContainer: {},
});
