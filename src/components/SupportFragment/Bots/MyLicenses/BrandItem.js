/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../../../../configs/ThemeData';

const BrandItem = ({brandData, isSelected, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <View
        style={[styles.brandItem, isSelected && {borderWidth: 2, opacity: 1}]}>
        <Image
          source={{uri: brandData.profilePicURL}}
          resizeMode="contain"
          style={styles.branImage}
        />
        <Text numberOfLines={1} style={styles.brandName}>
          {brandData.bankerTag}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default BrandItem;

const styles = StyleSheet.create({
  brandItem: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 5,
    width: 90,
    height: 90,
    marginRight: 15,
    opacity: 0.6,
  },
  branImage: {
    width: 40,
    height: 40,
  },
  brandName: {
    color: '#464B4E',
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 8,
    marginTop: 15,
    textTransform: 'capitalize',
  },
});
