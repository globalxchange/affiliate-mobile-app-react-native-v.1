/* eslint-disable react-native/no-inline-styles */
import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {WToast} from 'react-native-smart-tip';
import {GX_API_ENDPOINT} from '../../../../configs';
import ThemeData from '../../../../configs/ThemeData';
import {getUriImage} from '../../../../utils';
import LoadingAnimation from '../../../LoadingAnimation';
import PurchasedLicenses from './PurchasedLicenses';

const {width} = Dimensions.get('window');

const LicenseList = ({activeBrand, userEmail}) => {
  const [currentUserDetails, setCurrentUserDetails] = useState();
  const [activeFilter, setActiveFilter] = useState(FILTERS[0]);
  const [productLicenses, setProductLicenses] = useState();
  const [errorMessage, setErrorMessage] = useState('');
  const [totalPurchaseLicenses, setTotalPurchaseLicenses] = useState(0);
  const [selectedLicense, setSelectedLicense] = useState(false);

  useEffect(() => {
    if (userEmail) {
      Axios.get(`${GX_API_ENDPOINT}/brokerage/stats/get/uplines`, {
        params: {email: userEmail},
      })
        .then(({data}) => {
          if (data.status) {
            setCurrentUserDetails(data.user || '');
          } else {
            WToast.show({data: data.message, position: WToast.position.TOP});
          }
        })
        .catch((error) => console.log('Error on locating user', error));
    }
  }, [userEmail]);

  useEffect(() => {
    setProductLicenses();
    setErrorMessage('');
    setSelectedLicense();

    const merchant = activeBrand?.bankerTag
      ? activeBrand?.bankerTag === 'All Licenses'
        ? undefined
        : activeBrand?.bankerTag
      : undefined;

    let params = {
      email: userEmail,
      merchant,
    };

    if (activeFilter?.status === true) {
      params = {...params, getActive: true};
    } else if (activeFilter.status === false) {
      params = {...params, getInactive: true};
    }

    Axios.get(`${GX_API_ENDPOINT}/coin/vault/user/license/get`, {
      params,
    })
      .then((resp) => {
        const {data} = resp;

        // console.log('Data', data);

        if (data.status) {
          setTotalPurchaseLicenses(data.total_licenses_purchased);
          const licenceList = data.productLicenses || [];

          setProductLicenses(licenceList);
        } else {
          setErrorMessage(data.message);
          setProductLicenses([]);
        }
      })
      .catch((error) => {
        setProductLicenses([]);
        console.log('Error on getting broker data', error);
      });
  }, [userEmail, activeBrand, activeFilter]);

  // console.log('productLicenses', productLicenses);

  return (
    <View style={styles.container}>
      <View style={styles.filterContainer}>
        {FILTERS.map((item) => (
          <TouchableOpacity
            key={item.title}
            onPress={() => setActiveFilter(item)}>
            <View
              style={[
                styles.filterItem,
                item.title !== activeFilter?.title && {opacity: 0.35},
              ]}>
              <Text style={styles.filterText}>{item.title}</Text>
            </View>
          </TouchableOpacity>
        ))}
      </View>
      {productLicenses ? (
        errorMessage ? (
          <View style={styles.errorContainer}>
            <Text style={styles.errorMessage}>{errorMessage}</Text>
          </View>
        ) : (
          <View style={styles.viewContainer}>
            <View style={styles.userDetails}>
              <View style={styles.userContainer}>
                <FastImage
                  source={{uri: getUriImage(currentUserDetails?.profile_img)}}
                  style={styles.userImage}
                  resizeMode="contain"
                />
                <Text numberOfLines={1} style={styles.userName}>
                  {currentUserDetails?.name}
                </Text>
                {selectedLicense ? (
                  <TouchableOpacity
                    onPress={() => setSelectedLicense()}
                    style={styles.backButton}>
                    <Image
                      source={require('../../../../assets/back-bend-arrow.png')}
                      resizeMode="contain"
                      style={styles.backIcon}
                    />
                  </TouchableOpacity>
                ) : null}
              </View>
              <Text style={styles.licenseDetails}>
                {selectedLicense
                  ? `Has Obtained The Following Licenses For ${selectedLicense.product_name}`
                  : `Has${
                      activeFilter.decsText ? '' : ' purchased'
                    } ${totalPurchaseLicenses} ${
                      activeFilter.decsText
                    } licenses across 5 products offered by ${
                      activeBrand?.bankerTag || 'All Brands'
                    }.`}
              </Text>
            </View>
            <View style={styles.listContainer}>
              {selectedLicense ? (
                <PurchasedLicenses selectedLicense={selectedLicense} />
              ) : (
                <FlatList
                  data={productLicenses}
                  keyExtractor={(item) => item.product_id}
                  showsVerticalScrollIndicator={false}
                  renderItem={({item}) => (
                    <TouchableOpacity
                      onPress={() => setSelectedLicense(item)}
                      style={styles.itemContainer}>
                      <Image
                        source={{uri: item.product_icon}}
                        resizeMode="cover"
                        style={styles.productIcon}
                      />
                      <Text style={[styles.productName, {flex: 1}]}>
                        {item.product_name}
                      </Text>
                      <Text style={styles.productName}>
                        {item.license_count}
                      </Text>
                    </TouchableOpacity>
                  )}
                />
              )}
            </View>
          </View>
        )
      ) : (
        <View style={{flex: 1, justifyContent: 'center'}}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default LicenseList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
  viewContainer: {
    flex: 1,
  },
  header: {
    color: '#464B4E',
    fontFamily: 'Montserrat-Bold',
    fontSize: 14,
    textTransform: 'capitalize',
  },
  listContainer: {
    flex: 1,
  },
  emptyContainer: {
    width: width - 60,
    marginTop: 30,
    paddingHorizontal: 30,
  },
  emptyText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: '#464B4E',
    textAlign: 'center',
    fontSize: 16,
    textTransform: 'capitalize',
  },
  filterContainer: {
    flexDirection: 'row',
    borderTopColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginHorizontal: -20,
  },
  filterItem: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    width: 80,
    height: 28,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
  },
  filterText: {
    fontFamily: ThemeData.FONT_MEDIUM,
    fontSize: 12,
  },
  userDetails: {
    marginVertical: 20,
  },
  userContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  userImage: {
    width: 35,
    height: 35,
    borderRadius: 20,
    backgroundColor: '#C4C4C4',
  },
  userName: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 18,
    color: ThemeData.APP_MAIN_COLOR,
    textTransform: 'capitalize',
    paddingLeft: 10,
    flex: 1,
  },
  backButton: {
    width: 30,
    height: 30,
    padding: 5,
  },
  backIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  licenseDetails: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 10,
  },
  errorContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  errorMessage: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 18,
  },
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    padding: 25,
    marginBottom: 10,
  },
  productIcon: {
    width: 30,
    height: 30,
    borderRadius: 15,
    marginRight: 10,
  },
  productName: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 14,
  },
});

const FILTERS = [
  {status: undefined, title: 'All', decsText: ''},
  {status: true, title: 'Active', decsText: 'active'},
  {status: false, title: 'Inactive', decsText: 'inactive'},
];
