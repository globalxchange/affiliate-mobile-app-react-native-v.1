import React, {useContext, useEffect, useState} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import ThemeData from '../../../../configs/ThemeData';
import {OneToOneChatContext} from '../../../../contexts/OneToOneChatContext';
import {timeAgoFormatter} from '../../../../utils';
import LoadingAnimation from '../../../LoadingAnimation';
import ChatView from './ChatView';
import FriendItem from './FriendItem';
import RecentItem from './RecentItem';
import SearchBar from './SearchBar';

const OneToOneChat = ({paramUser}) => {
  const {
    isSocketConnected,
    allUsers,
    setSelectedUser,
    selectedUser,
    recentList,
    setCurrentTreadId,
  } = useContext(OneToOneChatContext);

  useEffect(() => {
    if (paramUser && paramUser !== true) {
      setSelectedUser(paramUser);
    }
  }, [paramUser]);

  const [searchInput, setSearchInput] = useState('');

  if (selectedUser) {
    return <ChatView />;
  }

  return (
    <View style={styles.container}>
      <SearchBar
        placeHolder="Search Contacts"
        searchText={searchInput}
        setSearchText={setSearchInput}
      />
      {isSocketConnected ? (
        <>
          <Text style={styles.header}>Recents</Text>
          <View style={{marginHorizontal: -20}}>
            {recentList ? (
              <FlatList
                contentContainerStyle={{paddingLeft: 20, paddingRight: 10}}
                horizontal
                showsHorizontalScrollIndicator={false}
                data={recentList}
                keyExtractor={(_, index) => index.toString()}
                renderItem={({item}) => (
                  <RecentItem
                    name={`${item.first_name?.trim()}`}
                    time={timeAgoFormatter(item.msg_timestamp)}
                    onPress={() => {
                      setCurrentTreadId(item.thread_id);
                      setSelectedUser(item);
                    }}
                    avatar={item.avatar}
                  />
                )}
              />
            ) : (
              <LoadingAnimation />
            )}
          </View>
          <Text style={styles.header}>Friends</Text>
          <View style={{flex: 1}}>
            {allUsers ? (
              <FlatList
                showsVerticalScrollIndicator={false}
                data={allUsers}
                keyExtractor={(_, index) => index.toString()}
                renderItem={({item}) => (
                  <FriendItem
                    name={`${item.first_name?.trim()} ${
                      (item.last_name || '')[0]
                    }`}
                    avatar={item.avatar}
                    bio={item.bio}
                    onPress={() => setSelectedUser(item)}
                  />
                )}
              />
            ) : (
              <LoadingAnimation />
            )}
          </View>
        </>
      ) : (
        <View style={{flex: 1, justifyContent: 'center'}}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default OneToOneChat;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: 20,
    paddingBottom: 0,
  },
  header: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    marginTop: 40,
    fontSize: 18,
    marginBottom: 10,
  },
});
