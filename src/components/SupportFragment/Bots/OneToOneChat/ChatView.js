import React, {useContext, useEffect, useRef, useState} from 'react';
import {
  BackHandler,
  FlatList,
  Image,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import {TypingAnimation} from 'react-native-typing-animation';
import ThemeData from '../../../../configs/ThemeData';
import {OneToOneChatContext} from '../../../../contexts/OneToOneChatContext';
import KeyboardOffset from '../../../KeyboardOffset';
import LoadingAnimation from '../../../LoadingAnimation';
import ChatItem from '../../Chats/ChatItem';
import ChatHeader from './ChatHeader';

const ChatView = () => {
  const {
    messageArray,
    isUserTyping,
    userObject,
    selectedUser,
    sendMessage,
    notifyTyping,
    setSelectedUser,
  } = useContext(OneToOneChatContext);

  const [chatInput, setChatInput] = useState('');

  const flatListRef = useRef();

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backHandler);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backHandler);
    };
  }, []);

  const backHandler = () => {
    setSelectedUser();
    return true;
  };

  const handleSubmitMessage = () => {
    const message = chatInput.trim();

    if (!message) {
      return WToast.show({
        data: 'Please Input A Message',
        position: WToast.position.TOP,
      });
    }

    sendMessage(message);
    setChatInput('');
  };

  const onChatInputChange = (text) => {
    setChatInput(text);
    // socketRef.current.emit('typing', userObject.current.username, threadId);
    notifyTyping();
  };

  return (
    <View style={styles.container}>
      <ChatHeader />
      {messageArray ? (
        <View style={[styles.chatContainer]}>
          <View style={styles.container}>
            {messageArray?.length > 0 ? (
              <FlatList
                ref={flatListRef}
                onContentSizeChange={() =>
                  flatListRef.current.scrollToEnd({animated: true})
                }
                onLayout={() =>
                  flatListRef.current.scrollToEnd({animated: true})
                }
                showsVerticalScrollIndicator={false}
                style={styles.chatView}
                data={messageArray}
                keyExtractor={(item) => item.timestamp.toString()}
                renderItem={({item, index}) => (
                  <ChatItem
                    isSent={item.sender?.username === userObject?.username}
                    data={item}
                  />
                )}
              />
            ) : (
              <View style={styles.emptyContainer}>
                <Text style={styles.emptyMessage}>
                  Start Your Conversation with {selectedUser?.first_name}
                </Text>
              </View>
            )}
          </View>
          {isUserTyping && (
            <View style={styles.typingContainer}>
              <TypingAnimation
                dotColor="#08152D"
                dotMargin={3}
                dotAmplitude={3}
                dotRadius={2.5}
                dotX={12}
                dotY={6}
                style={styles.typingAnimation}
              />
              <Text
                style={
                  styles.typingMessage
                }>{`${selectedUser?.first_name?.trim()} is Typing`}</Text>
            </View>
          )}
          <View style={styles.chatInputContainer}>
            <TextInput
              placeholder="Type your message"
              style={styles.chatInput}
              value={chatInput}
              onChangeText={onChatInputChange}
              placeholderTextColor="#878788"
            />
            <TouchableOpacity
              style={styles.sendIcon}
              onPress={handleSubmitMessage}>
              <Image
                style={styles.icon}
                source={require('../../../../assets/send-icon-white.png')}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
          <KeyboardOffset offset={Platform.OS === 'ios' ? -60 : -150} onlyIos />
        </View>
      ) : (
        <View style={styles.loadingView}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default ChatView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  chatContainer: {
    flex: 1,
    paddingHorizontal: 25,
    paddingTop: 25,
    backgroundColor: 'white',
  },
  chatView: {
    flex: 1,
  },
  chatInputContainer: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: 10,
    paddingTop: 10,
  },
  chatInput: {
    color: 'black',
    flex: 1,
    borderColor: '#08152D',
    borderWidth: 1,
    marginHorizontal: 15,
    paddingHorizontal: 10,
    paddingVertical: 4,
    borderRadius: 8,
    fontFamily: 'Montserrat',
    maxHeight: 100,
    minHeight: 40,
  },
  attachButton: {
    width: 25,
    height: 25,
    padding: 2,
  },
  icon: {
    flex: 1,
    width: null,
    height: null,
  },
  sendIcon: {
    backgroundColor: '#08152D',
    width: 60,
    maxHeight: 40,
    padding: 10,
    borderRadius: 18,
  },
  loadingView: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  typingContainer: {
    height: 20,
  },
  typingAnimation: {},
  typingMessage: {
    marginLeft: 35,
    fontFamily: 'Montserrat',
  },
  emptyContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 40,
  },
  emptyMessage: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
});
