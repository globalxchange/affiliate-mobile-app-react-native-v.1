import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../../../../configs/ThemeData';

const FriendItem = ({name, avatar, bio, onPress}) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <Image
        source={{
          uri: avatar,
        }}
        resizeMode="cover"
        style={styles.userImage}
      />
      <View style={styles.detailsContainer}>
        <Text style={styles.name}>{name}</Text>
        <Text style={styles.lastTime}>{bio}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default FriendItem;

const styles = StyleSheet.create({
  container: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    padding: 20,
    flexDirection: 'row',
    marginBottom: 10,
  },
  userImage: {
    width: 50,
    height: 50,
    borderRadius: 25,
  },
  detailsContainer: {
    flex: 1,
    marginLeft: 20,
  },
  name: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    marginTop: 8,
    fontSize: 16,
    textTransform: 'capitalize',
  },
  lastTime: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 10,
    marginTop: 2,
    color: '#BBBBBB',
  },
});
