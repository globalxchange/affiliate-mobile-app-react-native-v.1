import React from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
} from 'react-native';
import ThemeData from '../../../../configs/ThemeData';

const {width} = Dimensions.get('window');

const RecentItem = ({name, time, onPress, avatar}) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <Image
        source={{
          uri: avatar,
        }}
        resizeMode="cover"
        style={styles.userImage}
      />
      <Text numberOfLines={1} style={styles.name}>
        {name}
      </Text>
      <Text style={styles.lastTime}>{time}</Text>
    </TouchableOpacity>
  );
};

export default RecentItem;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginRight: 10,
    padding: 20,
    width: (width - 40) / 3,
    height: (width - 40) / 3,
    borderRadius: 8,
  },
  userImage: {
    width: 50,
    height: 50,
    borderRadius: 25,
  },
  name: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    marginTop: 8,
  },
  lastTime: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 10,
    marginTop: 2,
    color: '#BBBBBB',
  },
});
