import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {timeParserExpanded} from '../../../../utils';

const VideosList = ({selectedCategory, videoList}) => {
  const {navigate} = useNavigation();

  const onVideSelected = (item) => {
    console.log('item', item);
    navigate('Support', {
      openVideo: true,
      openArticle: false,
      videoData: item,
      category: selectedCategory,
    });
  };

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.header}>
          Total Of {videoList?.length || 0} Videos In This Playlist
        </Text>
        {/* <TouchableOpacity>
          <Image
            source={require('../../../../assets/search-icon.png')}
            style={styles.searchIcon}
            resizeMode="contain"
          />
        </TouchableOpacity> */}
      </View>
      <FlatList
        style={styles.listContainer}
        horizontal={videoList?.length > 0}
        showsHorizontalScrollIndicator={false}
        data={videoList}
        keyExtractor={(item) => item._id}
        renderItem={({item}) => (
          <TouchableOpacity
            style={styles.videoItem}
            onPress={() => onVideSelected(item)}>
            <View style={styles.coverContainer}>
              <Image
                style={styles.coverImage}
                source={{uri: item.image}}
                resizeMode="cover"
              />
              <Image
                style={styles.playIcon}
                source={require('../../../../assets/play-icon.png')}
                resizeMode="contain"
              />
            </View>
            <View style={styles.itemDetails}>
              <Text numberOfLines={1} style={styles.detailsTitle}>
                {item.title}
              </Text>
              <Text style={styles.detailsDesc}>
                {timeParserExpanded(Date.parse(item.updatedAt || ''))}
              </Text>
            </View>
          </TouchableOpacity>
        )}
        ListEmptyComponent={
          <Text style={styles.emptyText}>No Videos Available</Text>
        }
      />
      <TouchableOpacity style={styles.button}>
        <Text style={styles.buttonText}>
          Can’t Find What You Are Looking For?
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default VideosList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  header: {
    flex: 1,
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
    marginRight: 15,
  },
  searchIcon: {
    width: 20,
    height: 20,
  },
  videoItem: {
    width: 180,
    marginRight: 10,
  },
  coverContainer: {
    backgroundColor: '#08152D',
    justifyContent: 'center',
    alignItems: 'center',
    height: 90,
    width: 180,
  },
  coverImage: {...StyleSheet.absoluteFill, zIndex: 0, height: 90, width: 180},
  playIcon: {
    width: 30,
    height: 30,
    zIndex: 1,
  },
  itemDetails: {
    paddingHorizontal: 15,
    paddingVertical: 8,
    borderColor: '#E9E8E8',
    borderWidth: 1,
    justifyContent: 'center',
  },
  detailsTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
  },
  detailsDesc: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 8,
  },
  listContainer: {
    marginVertical: 20,
  },
  button: {
    backgroundColor: '#08152D',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: -30,
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
  emptyText: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
    textAlign: 'center',
    color: '#464B4E',
    paddingVertical: 30,
  },
});
