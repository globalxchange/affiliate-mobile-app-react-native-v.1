import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const ArticleList = ({selectedCategory, articleList}) => {
  const {navigate} = useNavigation();

  const onItemSelected = (item) => {
    navigate('Support', {
      openArticle: true,
      openVideo: false,
      articleData: item,
      selectedCategory: selectedCategory,
    });
  };

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.header}>
          Total Of {articleList?.length || 0} Article In This Playlist
        </Text>
        {/* <TouchableOpacity>
          <Image
            source={require('../../../../assets/search-icon.png')}
            style={styles.searchIcon}
            resizeMode="contain"
          />
        </TouchableOpacity> */}
      </View>
      <View style={styles.listContainer}>
        {articleList?.length ? (
          articleList.map((item, index) => (
            <TouchableOpacity
              onPress={() => onItemSelected(item)}
              style={styles.videoItem}>
              <View style={styles.iconContainer}>
                <Image
                  style={styles.icon}
                  resizeMode="contain"
                  source={{uri: item.icon}}
                />
              </View>
              <View style={styles.itemDetails}>
                <Text style={styles.detailsTitle}>{item.title}</Text>
                <Text style={styles.detailsDesc}>{item.desc}</Text>
              </View>
            </TouchableOpacity>
          ))
        ) : (
          <Text style={styles.emptyText}>No Articles Available</Text>
        )}
      </View>
      <TouchableOpacity style={styles.button}>
        <Text style={styles.buttonText}>
          Can’t Find What You Are Looking For?
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default ArticleList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  header: {
    flex: 1,
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
    marginRight: 15,
  },
  videoItem: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  iconContainer: {
    backgroundColor: '#08152D',
    width: 75,
    height: 75,
    padding: 12,
  },
  icon: {
    flex: 1,
    height: null,
    width: null,
  },
  itemDetails: {
    flex: 1,
    paddingHorizontal: 15,
    borderTopColor: '#E9E8E8',
    borderTopWidth: 1,
    borderBottomColor: '#E9E8E8',
    borderBottomWidth: 1,
    justifyContent: 'center',
  },
  detailsTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
  },
  detailsDesc: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 10,
  },
  listContainer: {
    marginTop: 20,
  },
  button: {
    backgroundColor: '#08152D',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: -30,
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
  emptyText: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
    textAlign: 'center',
    color: '#464B4E',
    paddingVertical: 30,
  },
});
