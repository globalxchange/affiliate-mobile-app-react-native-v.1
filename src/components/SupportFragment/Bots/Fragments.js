/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {SupportContext} from '../../../contexts/SupportContext';
import ActionsAdd from './ActionsAdd';
import LearnBot from './LearnBot';
import MyLicenses from './MyLicenses';
import MyNetwork from './MyNetwork';
import TransactionAdd from './TransactionAdd';
import TransactionSend from './TransactionSend';

const Fragments = ({activeCategory, activeSubCategory, setHideSubCategory}) => {
  const {isFullScreen} = useContext(SupportContext);

  const [activeFragment, setActiveFragment] = useState();

  useEffect(() => {
    let fragment = null;

    switch (activeCategory?.title) {
      case 'My Network':
        fragment = (
          <MyNetwork
            setHideSubCategory={setHideSubCategory}
            activeSubCategory={activeSubCategory}
            key={Date.now().toString()}
          />
        );
        break;

      case 'My Licenses':
        fragment = <MyLicenses />;
        break;

      case 'Transactions':
        switch (activeSubCategory?.title) {
          case 'Send':
            fragment = <TransactionSend />;
            break;
          case 'Add':
            fragment = <TransactionAdd />;
            break;
        }
        break;

      case 'Actions':
        switch (activeSubCategory?.title) {
          case 'Add':
            fragment = <ActionsAdd setHideSubCategory={setHideSubCategory} />;
            break;
        }
        break;

      case 'Learn':
        return <LearnBot publisher={activeSubCategory} />;
    }
    setActiveFragment(fragment);
  }, [activeCategory, activeSubCategory]);

  return (
    <View style={[styles.container, isFullScreen && {marginTop: 0}]}>
      {activeFragment}
    </View>
  );
};

export default Fragments;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 25,
  },
});
