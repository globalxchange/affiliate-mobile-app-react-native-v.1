import React, {useContext} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {AppContext} from '../../../contexts/AppContextProvider';
import {SupportContext} from '../../../contexts/SupportContext';

const TopCategory = ({
  activeCategory,
  setActiveCategory,
  setActiveSubCategory,
  learnSubMenu,
}) => {
  const {isLoggedIn} = useContext(AppContext);
  const {isFullScreen} = useContext(SupportContext);

  const CATEGORIES = [
    {
      title: 'My Network',
      disabled: !isLoggedIn,
    },
    {
      title: 'My Licenses',
      disabled: !isLoggedIn,
    },
    {
      title: 'Transactions',
      header: 'What Is The Type Of Transaction',
      subMenu: [
        {
          title: 'Add',
          icon: require('../../../assets/support-category-icons/add.png'),
        },
        {
          title: 'Send',
          icon: require('../../../assets/support-category-icons/send.png'),
        },
        {
          title: 'Trade',
          icon: require('../../../assets/support-category-icons/trade.png'),
        },
        {
          title: 'Invest',
          icon: require('../../../assets/support-category-icons/invest.png'),
        },
      ],
      disabled: !isLoggedIn,
    },
    {
      title: 'Actions',
      subMenu: [
        {
          title: 'Add',
          icon: require('../../../assets/support-category-icons/add.png'),
        },
        {
          title: 'Send',
          icon: require('../../../assets/support-category-icons/send.png'),
        },
        {
          title: 'Trade',
          icon: require('../../../assets/support-category-icons/trade.png'),
        },
        {
          title: 'Invest',
          icon: require('../../../assets/support-category-icons/invest.png'),
        },
      ],
      disabled: !isLoggedIn,
    },
    {
      title: 'Learn',
      header: 'What Do You Want To Learn About?',
      subMenu: learnSubMenu,
    },
  ];

  return (
    <View style={{display: isFullScreen ? 'none' : 'flex'}}>
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        style={styles.container}>
        {CATEGORIES.map((item) => (
          <TouchableOpacity
            disabled={item.disabled}
            key={item.title}
            onPress={() => {
              setActiveCategory(item);
            }}>
            <View
              style={[
                styles.itemContainer,
                activeCategory.title === item.title && styles.itemActive,
              ]}>
              <Text
                style={[
                  styles.itemText,
                  activeCategory.title === item.title && styles.itemTextActive,
                ]}>
                {item.title}
              </Text>
            </View>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  );
};

export default TopCategory;

const styles = StyleSheet.create({
  container: {
    marginRight: -30,
  },
  itemContainer: {
    paddingHorizontal: 20,
    paddingVertical: 12,
    borderColor: '#E7E7E7',
    borderWidth: 1,
    marginRight: 10,
    minWidth: 120,
    justifyContent: 'center',
    alignItems: 'center',
    opacity: 0.5,
  },
  itemActive: {
    opacity: 1,
  },
  itemText: {
    fontFamily: 'Montserrat',
    color: '#464B4E',
  },
  itemTextActive: {
    fontFamily: 'Montserrat-SemiBold',
  },
});
