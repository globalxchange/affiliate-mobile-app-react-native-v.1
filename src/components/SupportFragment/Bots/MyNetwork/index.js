import React, {useState} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Lineage from './Lineage';

const {width} = Dimensions.get('window');

const MyNetwork = ({activeSubCategory}) => {
  const [selectedTopCategory, setSelectedTopCategory] = useState('');
  const [hideTopCategory, setHideTopCategory] = useState(false);

  const renderFragment = () => {
    switch (selectedTopCategory.title) {
      case 'Lineage':
        return (
          <Lineage
            activeSubCategory={activeSubCategory}
            setHideSubCategory={setHideTopCategory}
          />
        );
      default:
        return null;
    }
  };

  return (
    <View style={styles.container}>
      <Text style={[styles.header, hideTopCategory && {display: 'none'}]}>
        Select A Topic
      </Text>
      <View>
        <FlatList
          style={[styles.scrollView, hideTopCategory && {display: 'none'}]}
          horizontal
          showsHorizontalScrollIndicator={false}
          data={TOP_SUB_MENU}
          keyExtractor={(item) => item.title}
          renderItem={({item}) => (
            <TouchableOpacity
              disabled={item.disabled}
              key={item.title}
              onPress={() => setSelectedTopCategory(item)}>
              <View
                style={[
                  styles.itemContainer,
                  selectedTopCategory.title === item.title && styles.itemActive,
                  item.disabled && {opacity: 0.5},
                ]}>
                <Image
                  resizeMode="contain"
                  source={item.icon}
                  style={styles.itemIcon}
                />
                <Text
                  numberOfLines={1}
                  adjustsFontSizeToFit
                  minimumFontScale={0.8}
                  style={styles.itemText}>
                  {item.title}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        />
      </View>
      {renderFragment()}
    </View>
  );
};

export default MyNetwork;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    color: '#464B4E',
    fontFamily: 'Montserrat-Bold',
    fontSize: 14,
  },
  scrollView: {
    marginTop: 20,
    marginRight: -30,
  },
  itemContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#E7E7E7',
    padding: 10,
    marginRight: 20,
    width: width / 5,
    height: width / 5,
  },
  itemActive: {
    borderWidth: 2,
  },
  itemIcon: {
    width: 35,
    height: 35,
  },
  itemText: {
    textAlign: 'center',
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    marginTop: 10,
    fontSize: 12,
  },
});

const TOP_SUB_MENU = [
  {
    title: 'Lineage',
    icon: require('../../../../assets/crowdfunding.png'),
  },
  {
    title: 'Earnings',
    icon: require('../../../../assets/support-category-icons/send.png'),
    disabled: true,
  },
  {
    title: 'Reports',
    icon: require('../../../../assets/support-category-icons/trade.png'),
    disabled: true,
  },
  {
    title: 'Audit',
    icon: require('../../../../assets/support-category-icons/invest.png'),
    disabled: true,
  },
];
