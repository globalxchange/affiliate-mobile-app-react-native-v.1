import React, {useContext, useEffect} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../../../../../configs/ThemeData';
import {SupportContext} from '../../../../../contexts/SupportContext';

const ViewHeader = ({
  headerText = 'Lineage FAQ',
  onBack,
  disableBack,
  disableExpand,
}) => {
  const {isFullScreen, setIsFullScreen} = useContext(SupportContext);

  useEffect(() => {
    if (disableExpand) {
      setIsFullScreen(false);
    }
  }, [disableExpand]);

  const onClose = () => {
    setIsFullScreen(false);
    onBack && onBack();
  };

  return (
    <View style={styles.headerContainer}>
      <Text style={styles.headerText}>{headerText}</Text>
      {disableExpand || (
        <TouchableOpacity
          onPress={() => setIsFullScreen(!isFullScreen)}
          style={styles.cancelButton}>
          <Image
            source={
              isFullScreen
                ? require('../../../../../assets/collapse-icon.png')
                : require('../../../../../assets/expand-icon.png')
            }
            style={styles.cancelIcon}
            resizeMode="contain"
          />
        </TouchableOpacity>
      )}
      {disableBack || (
        <TouchableOpacity onPress={onClose} style={styles.cancelButton}>
          <Image
            source={require('../../../../../assets/cancel-icon-colored.png')}
            style={styles.cancelIcon}
            resizeMode="contain"
          />
        </TouchableOpacity>
      )}
    </View>
  );
};

export default ViewHeader;

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: '#F1F4F6',
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    paddingRight: 5,
  },
  headerText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    flex: 1,
    paddingHorizontal: 20,
  },
  cancelButton: {
    width: 20,
    height: 20,
    padding: 3,
    marginRight: 10,
  },
  cancelIcon: {
    flex: 1,
    width: null,
    height: null,
  },
});
