/* eslint-disable react-native/no-inline-styles */
import {useNavigation} from '@react-navigation/native';
import React, {useContext, useEffect, useState} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {SupportContext} from '../../../../../contexts/SupportContext';
import FAQ from './FAQ';
import SwapUpline from './SwapUpline';

const {width} = Dimensions.get('window');

const Lineage = ({activeSubCategory, setHideSubCategory}) => {
  const {navigate} = useNavigation();

  const {setIsRedirectModalOpen} = useContext(SupportContext);

  const [selectedMenu, setSelectedMenu] = useState('');
  const [hideSubMenu, setHideSubMenu] = useState(false);
  const [activeView, setActiveView] = useState(null);

  useEffect(() => {
    switch (selectedMenu?.title) {
      case "FAQ's":
        setActiveView(
          <FAQ
            activeSubCategory={activeSubCategory}
            setHideSubCategory={setHideSubCategory}
            setHideSubMenu={setHideSubMenu}
          />,
        );
        break;
      case 'ChainView':
        setActiveView(null);
        setIsRedirectModalOpen(true, {
          name: 'ChainView',
          onNavigate: () =>
            navigate('Network', {
              screen: 'Chain',
              params: {screen: 'ChainView'},
            }),
        });
        break;
      case 'Swap Uplines':
        setHideSubMenu(true);
        setActiveView(
          <SwapUpline
            onBack={() => {
              setHideSubMenu(false);
              setSelectedMenu('');
            }}
            selectedOption={selectedMenu}
          />,
        );
        break;
      default:
        setActiveView(null);
    }
  }, [selectedMenu, activeSubCategory]);

  return (
    <View style={styles.container}>
      <Text style={[styles.header, hideSubMenu && {display: 'none'}]}>
        What Do You Want To Know About Your Lineage?
      </Text>
      <View>
        {hideSubMenu || (
          <ScrollView
            horizontal
            showsHorizontalScrollIndicator={false}
            style={styles.scrollView}>
            {SUB_MENU.map((item) => (
              <TouchableOpacity
                disabled={item.disabled}
                key={item.title}
                onPress={() => {
                  setSelectedMenu({...item, date: Date.now()});
                  setHideSubCategory(true);
                }}>
                <View
                  style={[
                    styles.itemContainer,
                    selectedMenu?.title === item.title && styles.itemActive,
                    item.disabled && {opacity: 0.5},
                  ]}>
                  <Image
                    resizeMode="contain"
                    source={item.icon}
                    style={styles.itemIcon}
                  />
                  <Text
                    numberOfLines={1}
                    adjustsFontSizeToFit
                    minimumFontScale={0.7}
                    style={styles.itemText}>
                    {item.title}
                  </Text>
                </View>
              </TouchableOpacity>
            ))}
          </ScrollView>
        )}
      </View>
      {activeView}
    </View>
  );
};

export default Lineage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    color: '#464B4E',
    fontFamily: 'Montserrat-Bold',
    fontSize: 14,
    marginTop: 20,
  },
  scrollView: {
    marginRight: -30,
    marginTop: 10,
  },
  itemContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#E7E7E7',
    padding: 10,
    marginRight: 20,
    width: width / 5,
    height: width / 5,
  },
  itemActive: {
    borderWidth: 2,
  },
  itemIcon: {
    width: 35,
    height: 35,
  },
  itemText: {
    textAlign: 'center',
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    marginTop: 10,
    fontSize: 12,
  },
});

const SUB_MENU = [
  {
    title: "FAQ's",
    icon: require('../../../../../assets/question-mark.png'),
  },
  {
    title: 'ChainView',
    icon: require('../../../../../assets/network-tree.png'),
  },
  {
    title: 'Swap Uplines',
    icon: require('../../../../../assets/swap-upline-icon.png'),
  },
];
