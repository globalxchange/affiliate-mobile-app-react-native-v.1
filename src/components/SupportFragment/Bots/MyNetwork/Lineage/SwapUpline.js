/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useEffect, useState} from 'react';
import {
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeData from '../../../../../configs/ThemeData';
import AsyncStorageHelper from '../../../../../utils/AsyncStorageHelper';
import LoadingAnimation from '../../../../LoadingAnimation';
import ViewHeader from './ViewHeader';
import axios from 'axios';
import {APP_CODE, GX_API_ENDPOINT} from '../../../../../configs';
import {WToast} from 'react-native-smart-tip';
import {SupportContext} from '../../../../../contexts/SupportContext';
import SearchLayout from '../../../../../layouts/SearchLayout';
import PopupLayout from '../../../../../layouts/PopupLayout';
import {useNavigation} from '@react-navigation/native';

const SwapUpline = ({onBack, selectedOption}) => {
  const {setIsFullScreen} = useContext(SupportContext);

  const {navigate} = useNavigation();

  const [currentUpline, setCurrentUpline] = useState();
  const [selectedInputType, setSelectedInputType] = useState(INPUT_OPTIONS[0]);
  const [inputValue, setInputValue] = useState('');
  const [isEditorOpen, setIsEditorOpen] = useState(false);
  const [desiredUpline, setDesiredUpline] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [showConfirmPopup, setShowConfirmPopup] = useState(false);
  const [showSuccessPopup, setShowSuccessPopup] = useState(false);

  useEffect(() => {
    setIsFullScreen(isEditorOpen);
  }, [isEditorOpen]);

  useEffect(() => {
    (async () => {
      const email = await AsyncStorageHelper.getLoginEmail();

      axios
        .get(`${GX_API_ENDPOINT}/brokerage/stats/get/uplines`, {
          params: {email},
        })
        .then(({data}) => {
          if (data.status) {
            console.log('Data', data);

            const uplines = data?.uplines;

            const directUpline = uplines ? uplines[0] : '';

            setCurrentUpline(directUpline || {name: 'No Upline', email: ''});
          } else {
            WToast.show({data: data.message, position: WToast.position.TOP});
            onBack();
          }
        })
        .catch((error) => console.log('Error on locating user', error));
    })();
  }, []);

  const onSubmitHandler = (submitValue) => {
    const input = submitValue || inputValue;

    if (!input) {
      return WToast.show({
        data: 'Please Input A Value',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    const value = {};

    value[`${selectedInputType.paramKey}`] = input?.toLowerCase().trim();

    axios
      .get(`${GX_API_ENDPOINT}/user/details/get`, {
        params: value,
      })
      .then(({data}) => {
        if (data.status) {
          setDesiredUpline(data.user);
          setIsEditorOpen(false);
        } else {
          WToast.show({
            data: `No User Found Associated With This ${selectedInputType.paramKey}`,
            position: WToast.position.TOP,
          });
        }
      })
      .catch((error) => {
        console.log('Error on getting broker data', error);
        WToast.show({
          data: 'Please Check Your Internet Connection',
          position: WToast.position.TOP,
        });
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  const onUplineChange = async () => {
    setIsLoading(true);
    setShowConfirmPopup(false);

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    const postData = {
      email,
      token,
      app_code: 'gx',
      update_data: {
        ref_affiliate: desiredUpline?.affiliate_id || '1',
      },
    };

    // console.log('desiredUpline', desiredUpline);

    // console.log('PostData', postData);

    axios
      .post(`${GX_API_ENDPOINT}/user/profile/fields/update/values`, postData)
      .then(({data}) => {
        // console.log('Data', data);

        if (data.status) {
          setShowSuccessPopup(true);
        } else {
          WToast.show({data: data.message, position: WToast.position.TOP});
        }
      })
      .catch((error) => {
        console.log('Error on updating uplines', error);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  if (isEditorOpen) {
    return (
      <SearchLayout
        placeholder={`Enter ${selectedInputType.title}`}
        value={inputValue}
        setValue={setInputValue}
        onBack={() => setIsEditorOpen(false)}
        onSubmit={onSubmitHandler}
        keyboardOffset={Platform.OS === 'android' ? 75 : 100}
        filters={INPUT_OPTIONS}
        selectedFilter={selectedInputType}
        setSelectedFilter={setSelectedInputType}
        isLoading={isLoading}
      />
    );
  }

  return (
    <View style={styles.container}>
      <ViewHeader onBack={onBack} headerText="Swap Uplines" disableExpand />
      <View style={styles.selectedOption}>
        <Image
          style={styles.optionIcon}
          resizeMode="contain"
          source={selectedOption.icon}
        />
        <Text style={styles.optionName}>{selectedOption.title}</Text>
      </View>
      <View style={styles.listContainer}>
        {isLoading || !currentUpline ? (
          <View style={{flex: 1, justifyContent: 'center'}}>
            <LoadingAnimation />
          </View>
        ) : (
          <View style={{flex: 1}}>
            <View style={styles.itemContainer}>
              <Text style={styles.itemHeader}>Current Upline</Text>
              <View style={styles.item}>
                <Image
                  resizeMode="cover"
                  style={styles.userImage}
                  source={{uri: currentUpline?.profile_img}}
                />
                <View style={styles.nameContainer}>
                  <View style={{flexDirection: 'row'}}>
                    <Text numberOfLines={1} style={styles.userName}>
                      {currentUpline?.name}
                    </Text>
                  </View>
                  {currentUpline?.email ? (
                    <View style={{flexDirection: 'row'}}>
                      <Text numberOfLines={1} style={styles.userEmail}>
                        {currentUpline?.email}
                      </Text>
                    </View>
                  ) : null}
                </View>
              </View>
            </View>
            {desiredUpline ? (
              <View style={styles.itemContainer}>
                <Text style={styles.itemHeader}>Desired Upline</Text>
                <View style={styles.item}>
                  <Image
                    resizeMode="cover"
                    style={styles.userImage}
                    source={{uri: desiredUpline?.profile_img}}
                  />
                  <View style={styles.nameContainer}>
                    <View style={{flexDirection: 'row'}}>
                      <Text numberOfLines={1} style={styles.userName}>
                        {desiredUpline?.name}
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <Text numberOfLines={1} style={styles.userEmail}>
                        {desiredUpline?.email}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            ) : (
              <View style={styles.inputForm}>
                <Text style={styles.itemHeader}>Desired Upline</Text>
                <View style={styles.inputTypesContainer}>
                  {INPUT_OPTIONS.map((item) => (
                    <TouchableOpacity
                      key={item.title}
                      onPress={() => setSelectedInputType(item)}>
                      <View
                        style={[
                          styles.inputType,
                          selectedInputType.title === item.title && {
                            opacity: 1,
                          },
                        ]}>
                        <Text
                          style={[
                            styles.inputTypeName,
                            selectedInputType.title === item.title && {
                              fontFamily: ThemeData.FONT_SEMI_BOLD,
                            },
                          ]}>
                          {item.title}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  ))}
                </View>
                <TouchableOpacity onPress={() => setIsEditorOpen(true)}>
                  <Text
                    style={[
                      styles.inputField,
                      inputValue ? {} : {color: '#878788'},
                    ]}>
                    {inputValue || `Enter ${selectedInputType.title}`}
                  </Text>
                </TouchableOpacity>
              </View>
            )}
            <View style={styles.actionContainer}>
              <TouchableOpacity
                disabled={!desiredUpline}
                onPress={() => setShowConfirmPopup(true)}>
                <View
                  style={[
                    styles.submitButton,
                    {opacity: desiredUpline ? 1 : 0.35},
                  ]}>
                  <Text style={styles.submitButtonText}>Submit</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </View>
      <PopupLayout
        isOpen={showConfirmPopup}
        onClose={() => setShowConfirmPopup(false)}
        autoHeight
        headerTitle="Upline Swap Confirmation">
        <View style={styles.popupContainer}>
          <Text style={styles.popupText}>
            You Will Be Responsible For Conveying This Upline Swap To The
            Respective Parties
          </Text>
          <View style={styles.popupActionContainer}>
            <TouchableOpacity
              style={styles.filledButton}
              onPress={() => setShowConfirmPopup(false)}>
              <Text style={styles.filledButtonText}>Back</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.outlinedButton}
              onPress={onUplineChange}>
              <Text style={styles.outlinedButtonText}>Proceed</Text>
            </TouchableOpacity>
          </View>
        </View>
      </PopupLayout>
      <PopupLayout
        isOpen={showSuccessPopup}
        onClose={() => {
          setShowConfirmPopup(false);
        }}
        autoHeight
        headerTitle="Upline Swap Confirmation">
        <View style={styles.popupContainer}>
          <Text style={styles.popupText}>
            Your New Upline Is {desiredUpline?.name}. Please Inform Accordingly.
          </Text>
          <View style={styles.popupActionContainer}>
            <TouchableOpacity style={styles.filledButton} onPress={onBack}>
              <Text style={styles.filledButtonText}>Back To Bots</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.outlinedButton}
              onPress={() => navigate('Earn')}>
              <Text style={styles.outlinedButtonText}>Home</Text>
            </TouchableOpacity>
          </View>
        </View>
      </PopupLayout>
    </View>
  );
};

export default SwapUpline;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  selectedOption: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 20,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
  },
  optionIcon: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  optionName: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 15,
  },
  listContainer: {
    flex: 1,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    padding: 20,
    borderTopWidth: 0,
  },
  itemContainer: {
    marginBottom: 20,
  },
  item: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    padding: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemHeader: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
    marginBottom: 15,
  },
  userImage: {
    width: 35,
    height: 35,
    borderRadius: 20,
    backgroundColor: '#C4C4C4',
  },
  nameContainer: {
    flex: 1,
    marginHorizontal: 10,
    justifyContent: 'center',
  },
  userName: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 15,
    color: ThemeData.APP_MAIN_COLOR,
    textTransform: 'capitalize',
  },
  userEmail: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 9,
    color: ThemeData.APP_MAIN_COLOR,
    paddingRight: 20,
  },
  emptyText: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 14,
    color: ThemeData.APP_MAIN_COLOR,
    marginTop: 20,
  },
  inputForm: {
    borderTopWidth: 0,
    marginTop: 20,
    marginBottom: 20,
  },
  inputTypesContainer: {
    flexDirection: 'row',
  },
  inputType: {
    paddingHorizontal: 20,
    paddingVertical: 5,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginRight: 10,
    opacity: 0.4,
    minWidth: 85,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputTypeName: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 11,
    color: ThemeData.APP_MAIN_COLOR,
  },
  inputField: {
    marginTop: 30,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 15,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 14,
  },
  submitButton: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    paddingVertical: 12,
    width: 130,
    alignItems: 'center',
    opacity: 0.5,
  },
  submitButtonText: {
    color: 'white',
    fontSize: 14,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  actionContainer: {
    flexDirection: 'row',
    marginTop: 10,
  },
  popupContainer: {
    padding: 10,
  },
  popupText: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_NORMAL,
    lineHeight: 20,
  },
  popupActionContainer: {
    flexDirection: 'row',
    marginTop: 30,
  },
  filledButton: {
    flex: 1,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    borderRadius: 6,
  },
  filledButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: 'white',
    fontSize: 12,
  },
  outlinedButton: {
    flex: 1,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 6,
    borderColor: ThemeData.APP_MAIN_COLOR,
    borderWidth: 1,
    marginLeft: 10,
  },
  outlinedButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
  },
});

const INPUT_OPTIONS = [
  {title: 'Email', paramKey: 'email'},
  {title: 'Name', paramKey: 'name'},
  {title: 'Username', paramKey: 'username'},
];
