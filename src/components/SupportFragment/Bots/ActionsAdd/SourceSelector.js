import React from 'react';
import {
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

const {width} = Dimensions.get('window');

const SourceSelector = ({activeType, setActiveType}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.header}>
        {'Where Are You Sending The Crypto From?'}
      </Text>
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        style={styles.scrollView}>
        {TYPES.map((item) => (
          <TouchableOpacity
            key={item.title}
            onPress={() => setActiveType(item)}>
            <View
              style={[
                styles.itemContainer,
                activeType?.title === item.title && styles.itemActive,
              ]}>
              <Image
                resizeMode="contain"
                source={item.icon}
                style={styles.itemIcon}
              />
              <Text
                numberOfLines={1}
                adjustsFontSizeToFit
                style={styles.itemText}>
                {item.title}
              </Text>
            </View>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  );
};

export default SourceSelector;

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
  },
  header: {
    color: '#464B4E',
    fontFamily: 'Montserrat-Bold',
    fontSize: 14,
  },
  scrollView: {
    marginTop: 20,
    marginRight: -30,
  },
  itemContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#E7E7E7',
    padding: 10,
    marginRight: 20,
    width: width / 5,
    height: width / 5,
    opacity: 0.5,
  },
  itemActive: {
    borderWidth: 2,
    opacity: 1,
  },
  itemIcon: {
    width: 35,
    height: 35,
  },
  itemText: {
    textAlign: 'center',
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    marginTop: 10,
    fontSize: 12,
  },
});

const TYPES = [
  {
    title: 'External',
    icon: require('../../../../assets/support-category-icons/add.png'),
  },
  {
    title: 'GX App',
    icon: require('../../../../assets/support-category-icons/send.png'),
  },
  {
    title: 'MoneyMarket',
    icon: require('../../../../assets/support-category-icons/trade.png'),
  },
  {
    title: 'Bonds',
    icon: require('../../../../assets/support-category-icons/invest.png'),
  },
];
