import React, {useContext, useEffect, useState} from 'react';
import {
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {AppContext} from '../../../../contexts/AppContextProvider';
import {getUriImage} from '../../../../utils';

const {width} = Dimensions.get('window');

const CoinsSelector = ({selectedCoin, setSelectedCoin}) => {
  const {walletCoinData} = useContext(AppContext);

  const [ethCoins, setEthCoins] = useState();

  useEffect(() => {
    if (walletCoinData) {
      const coins = walletCoinData.filter(
        (x) =>
          x.hasOwnProperty('erc20Address') ||
          x.coinSymbol === 'ETH' ||
          x.coinSymbol === 'TRX',
      );

      // console.log('coins', coins);
      setEthCoins(coins);
    }
  }, [walletCoinData]);

  return (
    <View style={styles.container}>
      <Text style={styles.header}>{'Choose One Of The Following Coins'}</Text>
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={styles.scrollView}>
        {ethCoins ? (
          ethCoins.map((item) => (
            <TouchableOpacity
              key={item.coinSymbol}
              onPress={() => setSelectedCoin(item)}>
              <View
                style={[
                  styles.itemContainer,
                  selectedCoin?.coinSymbol === item.coinSymbol &&
                    styles.itemActive,
                ]}>
                <FastImage
                  resizeMode="contain"
                  source={{uri: getUriImage(item.coinImage)}}
                  style={styles.itemIcon}
                />
                <Text
                  numberOfLines={1}
                  adjustsFontSizeToFit
                  style={styles.itemText}>
                  {item.coinSymbol}
                </Text>
              </View>
            </TouchableOpacity>
          ))
        ) : (
          <View></View>
        )}
      </ScrollView>
    </View>
  );
};

export default CoinsSelector;

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
  },
  header: {
    color: '#464B4E',
    fontFamily: 'Montserrat-Bold',
    fontSize: 14,
  },
  scrollView: {
    marginTop: 20,
    marginRight: -30,
  },
  itemContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#E7E7E7',
    padding: 10,
    marginRight: 20,
    width: width / 5,
    height: width / 5,
    opacity: 0.5,
  },
  itemActive: {
    borderWidth: 2,
    opacity: 1,
  },
  itemIcon: {
    width: 35,
    height: 35,
  },
  itemText: {
    textAlign: 'center',
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    marginTop: 10,
    fontSize: 12,
  },
});
