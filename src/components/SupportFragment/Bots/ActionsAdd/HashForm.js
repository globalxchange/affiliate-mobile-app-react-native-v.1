import React, {useContext, useEffect, useState} from 'react';
import {
  Dimensions,
  Image,
  Modal,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import Clipboard from '@react-native-community/clipboard';
import {AppContext} from '../../../../contexts/AppContextProvider';
import {WToast} from 'react-native-smart-tip';
import AsyncStorageHelper from '../../../../utils/AsyncStorageHelper';
import {APP_CODE, GX_API_ENDPOINT} from '../../../../configs';
import Axios from 'axios';
import LoadingAnimation from '../../../LoadingAnimation';
import QRCode from 'react-native-qrcode-svg';
import {getUriImage} from '../../../../utils';

const {width} = Dimensions.get('window');

const HashForm = ({selectedCoin}) => {
  const {walletCoinData} = useContext(AppContext);

  const [addressInput, setAddressInput] = useState('');
  const [hashInput, setHashInput] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const [addressQrOpen, setAddressQrOpen] = useState(false);

  useEffect(() => {
    if (walletCoinData) {
      const ethCoinData = walletCoinData.find(
        (item) =>
          item.coinSymbol ===
          (selectedCoin.coinSymbol === 'TRX' ? 'TRX' : 'ETH'),
      );
      const ethAddress = ethCoinData?.coin_address;
      setAddressInput(ethAddress || '');
    }
  }, [walletCoinData, selectedCoin]);

  const pasteHandler = async (callback) => {
    const copiedText = await Clipboard.getString();

    if (callback) {
      callback(copiedText.trim());
    }
  };

  const submitHandler = async () => {
    if (!addressInput) {
      return WToast.show({
        data: 'Please Enter The Address',
        position: WToast.position.TOP,
      });
    }
    if (!hashInput) {
      return WToast.show({
        data: 'Please Enter The Hash',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);
    const email = await AsyncStorageHelper.getLoginEmail();

    let postData = {
      app_code: APP_CODE,
      email,
    };

    if (selectedCoin.coinSymbol === 'TRX') {
      postData = {...postData, hash: hashInput.trim()};
    } else {
      postData = {
        ...postData,
        coin: selectedCoin.coinSymbol,
        txn_hash: hashInput.trim(),
      };
    }

    let API =
      selectedCoin.coinSymbol === 'TRX'
        ? `${GX_API_ENDPOINT}/coin/vault/service/deposit/trx/external/request`
        : `${GX_API_ENDPOINT}/coin/vault/service/deposit/eth/coins/request`;

    Axios.post(API, postData)
      .then((resp) => {
        const {data} = resp;
        console.log('Hash Resp', data);

        WToast.show({data: data.message || '', position: WToast.position.TOP});
        setIsLoading(false);
      })
      .catch((error) => {
        setIsLoading(false);
      });
  };

  if (isLoading) {
    return (
      <View style={styles.loadingContainer}>
        <LoadingAnimation />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <FastImage
          source={{uri: getUriImage(selectedCoin?.coinImage)}}
          resizeMode="contain"
          style={styles.coinImage}
        />
        <Text style={styles.coinName}>{selectedCoin.coinName}</Text>
      </View>
      <View style={styles.inputGroup}>
        <Text style={styles.groupTitle}>Step 1</Text>
        <Text style={styles.groupDesc}>
          Copy this Ethereum address and input it as the destination address
          into which ever wallet you are currently using
        </Text>
        <View style={styles.inputContainer}>
          <Text numberOfLines={1} style={styles.input}>
            {addressInput || 'Address Not Found'}
          </Text>
          <TouchableOpacity
            onPress={() => setAddressQrOpen(true)}
            style={styles.inputAction}>
            <FastImage
              source={require('../../../../assets/qr-code-icon.png')}
              resizeMode="contain"
              style={styles.inputActionImage}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.inputGroup}>
        <Text style={styles.groupTitle}>Step 2</Text>
        <Text style={styles.groupDesc}>
          Enter the transaction hash for the transfer for it to reflect into
          your InstaCrypto Vault.
        </Text>
        <View style={styles.inputContainer}>
          <TextInput
            value={hashInput}
            onChangeText={(text) => setHashInput(text)}
            style={styles.input}
            placeholder="Enter Hash Here"
            placeholderTextColor="#B4BBC4"
          />
          <TouchableOpacity
            onPress={() => pasteHandler(setHashInput)}
            style={styles.inputAction}>
            <FastImage
              source={require('../../../../assets/paste-icon-blue.png')}
              resizeMode="contain"
              style={styles.inputActionImage}
            />
          </TouchableOpacity>
        </View>
      </View>
      <TouchableOpacity onPress={submitHandler} style={styles.submitButton}>
        <Text style={styles.submitText}>Submit Hash</Text>
      </TouchableOpacity>
      <Modal
        animationType="fade"
        transparent={true}
        visible={addressQrOpen}
        onRequestClose={() => setAddressQrOpen(false)}>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.overlay}
          onPress={() => setAddressQrOpen(false)}
          onPressOut={() => {}}>
          <TouchableWithoutFeedback style={{flex: 1}}>
            <View style={styles.modalView}>
              <QRCode
                value={addressInput || 'Empty'}
                size={width * 0.7}
                color={addressInput ? '#08152D' : '#C5DAEC'}
              />
            </View>
          </TouchableWithoutFeedback>
        </TouchableOpacity>
      </Modal>
    </View>
  );
};

export default HashForm;

const styles = StyleSheet.create({
  container: {},
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  coinImage: {
    width: 40,
    height: 40,
  },
  coinName: {
    flex: 1,
    paddingHorizontal: 15,
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 18,
  },
  inputGroup: {
    marginTop: 25,
  },
  groupTitle: {
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
  },
  groupDesc: {
    color: '#464B4E',
    fontFamily: 'Montserrat',
    fontSize: 12,
    marginTop: 10,
  },
  inputContainer: {
    marginTop: 15,
    borderColor: '#E7E7E7',
    borderWidth: 1,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    flex: 1,
    paddingHorizontal: 15,
    color: '#464B4E',
    fontFamily: 'Montserrat',
  },
  inputAction: {
    paddingHorizontal: 15,
    borderLeftColor: '#E7E7E7',
    borderLeftWidth: 1,
    height: '100%',
    justifyContent: 'center',
  },
  inputActionImage: {
    width: 23,
    height: 23,
  },
  submitButton: {
    borderColor: '#464B4E',
    borderWidth: 1,
    width: 180,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
  submitText: {
    color: '#464B4E',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 13,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  centeredView: {
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 20,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
});
