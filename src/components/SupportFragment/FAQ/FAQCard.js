import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';

const FAQCard = ({data, pos}) => {
  return (
    <View
      style={[
        styles.mainContainer,
        pos % 2 === 0 ? {paddingLeft: 5} : {paddingRight: 5},
      ]}>
      <View style={styles.container}>
        <View style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={data.image}
            resizeMode="contain"
          />
        </View>
        <Text style={styles.header}>{data.header}</Text>
        <Text style={styles.subHeading}>{data.subHeading}</Text>
      </View>
    </View>
  );
};

export default FAQCard;

const styles = StyleSheet.create({
  mainContainer: {
    width: '50%',
  },
  container: {
    backgroundColor: 'white',
    paddingHorizontal: 25,
    paddingVertical: 15,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 1,
    marginBottom: 10,
    alignItems: 'center',
  },
  imageContainer: {
    width: 45,
    height: 45,
    borderRadius: 25,
    padding: 10,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 1,
  },
  image: {
    flex: 1,
    height: null,
    width: null,
  },
  header: {
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
    color: '#000000',
  },
  subHeading: {
    textAlign: 'center',
    fontFamily: 'Montserrat',
    color: '#858585',
    fontSize: 10,
  },
});
