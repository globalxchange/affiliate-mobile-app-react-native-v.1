import React, {useState} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';

const FAQItem = ({no}) => {
  const [isCollapsed, setIsCollapsed] = useState(false);

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.headerContainer}
        onPress={() => setIsCollapsed(!isCollapsed)}>
        <View style={styles.numberContainer}>
          <Text style={styles.number}>{no}</Text>
        </View>
        <Text style={styles.header}>Lorem Ipsum Doler Sit</Text>
        <Image
          style={styles.icon}
          resizeMode="contain"
          source={
            isCollapsed
              ? require('../../../assets/chevron-up.png')
              : require('../../../assets/chevron-down.png')
          }
        />
      </TouchableOpacity>
      {isCollapsed && (
        <View style={styles.detailsContainer}>
          <Text style={styles.faqDetails}>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni est
            in dicta adipisci illum aut earum aliquam natus ut repellat. Sunt
            quo suscipit temporibus eveniet. Nesciunt neque officiis dolorum
            porro!
          </Text>
        </View>
      )}
    </View>
  );
};

export default FAQItem;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 10,
    marginBottom: 20,
  },
  headerContainer: {flexDirection: 'row', alignItems: 'center'},
  numberContainer: {
    backgroundColor: 'white',
    borderColor: '#F2F2F2',
    borderWidth: 1,
    borderRadius: 12,
    width: 24,
    height: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
  number: {
    color: '#000000',
    fontFamily: 'Montserrat-SemiBold',
  },
  header: {
    flexGrow: 1,
    width: 0,
    color: '#000000',
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 15,
  },
  icon: {
    width: 14,
    height: 14,
  },
  faqDetails: {
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
  detailsContainer: {
    borderTopColor: '#F2F2F2',
    borderTopWidth: 1,
    marginTop: 5,
    paddingTop: 5,
  },
});
