import React, {useRef, useState} from 'react';
import {
  Dimensions,
  Image,
  Modal,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import Animated, {interpolate} from 'react-native-reanimated';
import LoadingAnimation from '../../LoadingAnimation';

const {width, height} = Dimensions.get('window');

const FileModal = ({
  isOpen,
  setIsOpen,
  file,
  sendImage,
  setMessage,
  message,
  isLoading,
}) => {
  const [keyboardHeight, setKeyboardHeight] = useState(0);
  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);

  const chatKeyboardAnimation = useRef(new Animated.Value(0));

  let isImage = false;

  try {
    isImage = ['jpg', 'jpeg', 'png', 'gif'].includes(
      file.name.split('.').pop().toLowerCase(),
    );
  } catch (error) {}

  const fileSize = file?.size || 0;

  let fileSizeText = '';

  if (fileSize % (1024 * 1024) >= 1) {
    fileSizeText = `${(fileSize / (1024 * 1024)).toFixed(2)} MB`;
  } else {
    fileSizeText = `${(fileSize / 1024).toFixed(2)} KB`;
  }

  return (
    <Modal
      animationType="slide"
      visible={isOpen}
      transparent
      hardwareAccelerated
      statusBarTranslucent
      onDismiss={() => setIsOpen(false)}
      onRequestClose={() => setIsOpen(false)}>
      <TouchableOpacity
        activeOpacity={1}
        style={styles.overlay}
        onPress={() => {}}
        onPressOut={() => {}}>
        <TouchableWithoutFeedback style={{flex: 1}}>
          <Animated.View
            style={[
              styles.container,
              {
                transform: [
                  {
                    translateY: interpolate(chatKeyboardAnimation.current, {
                      inputRange: [0, 1],
                      outputRange: [0, -keyboardHeight],
                    }),
                  },
                ],
              },
            ]}>
            <Text style={styles.confirmText}>Confirm Sending Image?</Text>
            <TextInput
              value={message}
              onChangeText={(text) => setMessage(text)}
              placeholder="Type Your Message"
              placeholderTextColor={'#878788'}
              style={styles.messageInput}
              returnKeyLabel="done"
            />
            {isImage ? (
              <Image
                resizeMode="contain"
                style={styles.previewImage}
                source={{uri: file.uri}}
              />
            ) : (
              <View style={styles.fileContainer}>
                <Image
                  source={require('../../../assets/file-icon.png')}
                  resizeMode="contain"
                  style={styles.fileIcon}
                />
                <View style={styles.fileDetailsContainer}>
                  <Text style={styles.fileName}>{file?.name}</Text>
                  {file?.size && (
                    <Text style={styles.fileSize}>{fileSizeText}</Text>
                  )}
                </View>
              </View>
            )}
            {isLoading && (
              <View style={styles.loadingContainer}>
                <LoadingAnimation width={50} height={50} />
              </View>
            )}
            <View style={styles.controlContainer}>
              <TouchableOpacity
                onPress={() => setIsOpen(false)}
                style={styles.buttonOutlined}>
                <Text style={styles.buttonOutlinedText}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={isLoading ? null : sendImage}
                style={styles.buttonFilled}>
                <Text style={styles.buttonFilledText}>
                  {isLoading ? 'Sending' : 'Send'}
                </Text>
              </TouchableOpacity>
            </View>
          </Animated.View>
        </TouchableWithoutFeedback>
      </TouchableOpacity>
    </Modal>
  );
};

export default FileModal;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'center',
  },
  container: {
    backgroundColor: 'white',
    borderRadius: 10,
    overflow: 'hidden',
    marginHorizontal: 20,
    padding: 30,
  },
  previewImage: {
    height: height * 0.5,
    width: '100%',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: 10,
  },
  confirmText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 20,
  },
  controlContainer: {
    flexDirection: 'row',
  },
  buttonOutlined: {
    borderColor: '#08152D',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    flex: 1,
  },
  buttonOutlinedText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
  },
  buttonFilled: {
    backgroundColor: '#08152D',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    flex: 1,
    marginLeft: 20,
  },
  buttonFilledText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
  },
  messageInput: {
    height: 40,
    fontFamily: 'Montserrat',
    borderColor: '#08152D',
    borderWidth: 1,
    marginBottom: 10,
    marginTop: 20,
    paddingHorizontal: 15,
    color: '#08152D',
  },
  loadingContainer: {},
  fileContainer: {
    flexDirection: 'row',
    borderColor: '#08152D',
    borderWidth: 1,
    padding: 20,
    marginBottom: 20,
    alignItems: 'center',
  },
  fileIcon: {
    width: 40,
    height: 40,
  },
  fileDetailsContainer: {
    flex: 1,
    paddingHorizontal: 20,
  },
  fileName: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
  },
  fileSize: {
    fontFamily: 'Montserrat',
    color: '#08152D',
    fontSize: 12,
  },
});
