import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {timestampParser} from '../../../utils';
import HTMLView from 'react-native-htmlview';
import * as WebBrowser from 'expo-web-browser';
import Clipboard from '@react-native-community/clipboard';
import {WToast} from 'react-native-smart-tip';

const ChatItem = ({data, style, isSent}) => {
  const onLinkPress = (url) => {
    // console.log('link', url);
    WebBrowser.openBrowserAsync(url);
  };

  const copyHandler = () => {
    Clipboard.setString(data.message || '');
    WToast.show({data: 'Message Text Coped', position: WToast.position.TOP});
  };

  const onMessageClick = () => {
    const messageText = data.message || '';
    const URL_REGEX = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()!@:%_\+.~#?&\/\/=]*)/gm;
    const link = messageText.match(URL_REGEX);

    if (link) {
      WebBrowser.openBrowserAsync(link[0] || '');
    }
  };

  const messageViewTextStyles = isSent
    ? {style: {color: 'white', fontFamily: 'Montserrat'}}
    : {
        style: {color: '#9A9A9A', fontFamily: 'Montserrat'},
      };

  // console.log('Data', data);

  const isFile = data.type === 'file' || data.type === 'image';

  let isImage = false;

  let fileSizeText = '';

  if (isFile) {
    try {
      isImage = ['jpg', 'jpeg', 'png', 'gif'].includes(
        data.location.split('.').pop().toLowerCase(),
      );

      // console.log('Is Image', isImage);
    } catch (error) {
      console.log('Error: ' + error);
    }
  }

  return (
    <TouchableOpacity
      onLongPress={copyHandler}
      onPress={onMessageClick}
      style={[
        styles.container,
        isSent ? styles.sentMessage : styles.receivedMessage,
        style,
      ]}>
      <View
        style={[
          styles.textContainer,
          isSent ? styles.sentTextContainer : styles.receivedTextContainer,
        ]}>
        {isFile &&
          (isImage ? (
            <View style={styles.imageContainer}>
              <Image
                style={styles.image}
                source={{uri: data.location}}
                resizeMode="contain"
              />
            </View>
          ) : (
            <View style={styles.fileContainer}>
              <Image
                source={require('../../../assets/file-icon.png')}
                resizeMode="contain"
                style={styles.fileIcon}
              />
              <View style={styles.fileDetailsContainer}>
                <Text style={[styles.fileName, messageViewTextStyles.style]}>
                  {data.filename}
                </Text>
              </View>
            </View>
          ))}
        <HTMLView
          value={data.message}
          stylesheet={sentMessageView}
          onLinkPress={onLinkPress}
          textComponentProps={messageViewTextStyles}
        />
        {/* <Text
          style={[
            styles.messageText,
            isSent ? styles.sentMessageText : styles.receivedMessageText,
          ]}>
          {data.message}
        </Text> */}
      </View>
      <Text style={styles.time}>{timestampParser(data.timestamp)}</Text>
    </TouchableOpacity>
  );
};

export default ChatItem;

const styles = StyleSheet.create({
  container: {
    marginBottom: 20,
    maxWidth: '80%',
  },
  sentMessage: {
    marginLeft: 'auto',
    alignItems: 'flex-end',
  },
  receivedMessage: {
    marginRight: 'auto',
    alignItems: 'flex-start',
  },
  textContainer: {
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderRadius: 12,
  },
  sentTextContainer: {
    backgroundColor: '#08152D',
    borderBottomRightRadius: 0,
  },
  receivedTextContainer: {
    backgroundColor: '#F0EFEB',
    color: '#08152D',
  },
  messageText: {
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
  sentMessageText: {
    color: 'white',
  },
  receivedMessageText: {
    borderBottomLeftRadius: 0,
  },
  time: {
    fontFamily: 'Montserrat',
    fontSize: 9,
    paddingHorizontal: 2,
  },
  imageContainer: {},
  image: {
    height: 200,
    width: 200,
  },
  fileContainer: {
    flexDirection: 'row',
    padding: 20,
    alignItems: 'center',
  },
  fileIcon: {
    width: 40,
    height: 40,
  },
  fileDetailsContainer: {
    paddingHorizontal: 20,
  },
  fileName: {
    fontFamily: 'Montserrat-SemiBold',
  },
  fileSize: {
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
});

const sentMessageView = StyleSheet.create({
  p: {
    color: 'white',
  },
  baseFontStyle: {
    color: 'white',
  },
  a: {
    fontWeight: 'bold',
  },
});

const receivedMessageView = StyleSheet.create({});
