import React, {useRef, useEffect, useState, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  TouchableOpacity,
  TextInput,
  Keyboard,
  Platform,
} from 'react-native';
import ChatItem from './ChatItem';
import io from 'socket.io-client';
import {GX_CHAT_SOCKET_ENDPOINT, S3_CONFIG} from '../../../configs';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import {WToast} from 'react-native-smart-tip';
import {AppContext} from '../../../contexts/AppContextProvider';
import LoginReminder from '../LoginReminder';
import LoadingAnimation from '../../LoadingAnimation';
import {TypingAnimation} from 'react-native-typing-animation';
import Animated, {
  set,
  Clock,
  interpolate,
  useCode,
} from 'react-native-reanimated';
import ReanimatedTimingHelper from '../../../utils/ReanimatedTimingHelper';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import Axios from 'axios';
import FileModal from './FileModal';
import {Constants, Permissions} from 'react-native-unimodules';
import DocumentPicker from 'react-native-document-picker';
import {RNS3} from 'react-native-s3-upload';

const Chats = ({inputMessage}) => {
  const indices = useSafeAreaInsets();

  const {isLoggedIn} = useContext(AppContext);

  const [messageArray, setMessageArray] = useState([]);
  const [selectedGroup, setSelectedGroup] = useState('');
  const [typingFlag, setTypingFlag] = useState(false);
  const [typingMessage, setTypingMessage] = useState('');
  const [threadId, setThreadId] = useState('');
  const [chatSection, setChatSection] = useState(false);
  const [groupReadUnreadList, setGroupReadUnreadList] = useState([]);
  const [chatInput, setChatInput] = useState('');
  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);
  const [file, setFile] = useState('');
  const [isFileModalOpen, setIsFileModalOpen] = useState(false);
  const [isFileLoading, setIsFileLoading] = useState(false);

  const socketRef = useRef();
  const userSocketRef = useRef();
  const userObject = useRef();
  const flatListRef = useRef();
  const chatKeyboardAnimation = useRef(new Animated.Value(0));

  const setUpSocket = async () => {
    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    const userResp = await Axios.post(
      `${GX_CHAT_SOCKET_ENDPOINT}/gxchat/get_user`,
      {
        email,
        token,
      },
    );

    // console.log('userResp', userResp.data);

    if (userResp.data.status) {
      const username = userResp.data.payload.username;

      userObject.current = {
        username,
        email,
      };

      socketRef.current = io.connect(GX_CHAT_SOCKET_ENDPOINT, {
        reconnection: false,
        query: {
          email,
          token,
          tokfortest: 'nvestisgx',
          blackoriginnull: 'YdF3UMS6uAPRUZWnP2w6cgYy27z7r4',
        },
      });

      userSocketRef.current = io.connect(`${GX_CHAT_SOCKET_ENDPOINT}/user`, {
        reconnection: false,
        query: {
          email,
          token,
          tokfortest: 'nvestisgx',
          blackoriginnull: 'YdF3UMS6uAPRUZWnP2w6cgYy27z7r4',
        },
      });

      getAdminsAndCheck();
    }
  };

  useEffect(() => {
    (async () => {
      if (Constants.platform.ios) {
        const {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
          alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

  useEffect(() => {
    if (isLoggedIn) {
      setUpSocket();
    }
  }, [isLoggedIn]);

  useEffect(() => {
    if (!isFileModalOpen) {
      setIsFileLoading(false);
    }
  }, [isFileModalOpen]);

  useEffect(() => {
    if (inputMessage) {
      setChatInput(inputMessage);
    }
  }, [inputMessage]);

  const onKeyBoardShow = (e) => {
    if (Platform.OS === 'ios') {
      setKeyboardHeight(e.endCoordinates.height);
      setIsKeyboardOpen(true);
    }
  };

  const onKeyBoardHide = () => {
    if (Platform.OS === 'ios') {
      setIsKeyboardOpen(false);
    }
  };

  useCode(
    () =>
      isKeyboardOpen
        ? [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ]
        : [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ],
    [isKeyboardOpen],
  );

  useEffect(() => {
    Keyboard.addListener('keyboardWillShow', onKeyBoardShow);
    Keyboard.addListener('keyboardWillHide', onKeyBoardHide);
    return () => {
      Keyboard.removeListener('keyboardWillShow', onKeyBoardShow);
      Keyboard.removeListener('keyboardWillHide', onKeyBoardHide);
    };
  }, []);

  useEffect(() => {
    if (userSocketRef.current) {
      userSocketRef.current.on('new_message_notify', (data) => {
        // write code to put dot on user if receiver is cuerrentuser

        if (groupReadUnreadList.length > 1) {
          // console.log(data);
          if (data.group) {
            let tempArr = [...groupReadUnreadList];
            const objIndex = tempArr.findIndex(
              (obj) => obj.group_name === data.receiver,
            );
            tempArr[objIndex].sender = data.sender;

            let newArr = [
              ...tempArr.filter((o) => o.group_name === data.receiver),
            ];
            tempArr = tempArr.filter((obj) => obj.group_name !== data.receiver);
            newArr = [...newArr, ...tempArr];
            // console.log(newArr);
            setGroupReadUnreadList([...newArr]);

            // console.log(tempArr);
          }
        }
      });
    }
    return () => {
      if (userSocketRef.current) {
        userSocketRef.current.off('new_message_notify');
      }
    };
  }, [groupReadUnreadList]);

  // useEffect to  update the message list when the other party types a new message.
  useEffect(() => {
    if (socketRef.current) {
      socketRef.current.on('msg_notify', (data) => {
        // console.log(data);
        if (data.thread_id === threadId) {
          // console.log(messageArray);
          if (messageArray[messageArray.length - 1] !== data) {
            setMessageArray([...messageArray, data]);
          }
        }
      });
    }
    return () => {
      if (socketRef.current) {
        socketRef.current.off('msg_notify');
      }
    };
  }, [messageArray, threadId]);

  // useEffect to notify that other party which the current user is interaction is typing
  useEffect(() => {
    if (socketRef.current) {
      socketRef.current.on('someone_typing', (typingUser, socketThreadId) => {
        if (
          socketThreadId === threadId &&
          typingUser !== userObject.current.username
        ) {
          setTypingMessage(`${typingUser} is typing`);
          setTypingFlag(true);
          setTimeout(() => {
            setTypingFlag(false);
          }, 1800);
        } else {
          setTypingFlag(false);
        }
      });
    }
    return () => {
      if (socketRef.current) {
        socketRef.current.off('someone_typing');
      }
    };
  }, [threadId]);

  const getAdminsAndCheck = () => {
    if (socketRef.current) {
      // console.log('User Object', userObject.current);

      socketRef.current.emit(
        'get_user_support_group_interaction_list',
        userObject.current,
        (response) => {
          // console.log('get_user_support_group_interaction_list', response);
          setSelectedGroup(response[0].group_name);
          setGroupReadUnreadList([...response]);

          socketRef.current.emit(
            'get_support_group_chat_history',
            response[0].group_name,
            response[0].thread_id,
            (socketResp) => {
              // console.log('All messages', socketResp);
              setThreadId(response[0].thread_id);
              setChatSection(true);
              setMessageArray([...socketResp.reverse()]);
            },
          );
        },
      );
    }
  };

  const handleSubmitMessage = () => {
    const currentUser = userObject.current.username;

    const inputText = chatInput.trim();

    if (inputText.length > 0) {
      const tempArr = [...groupReadUnreadList];
      const objIndex = tempArr.findIndex(
        (obj) => obj.group_name === selectedGroup,
      );
      tempArr[objIndex].last_messaged_user = currentUser;
      // console.log(tempArr);
      setGroupReadUnreadList(tempArr);

      const notificationData = {
        sender: currentUser,
        receiver: selectedGroup,
        group: true,
      };

      userSocketRef.current.emit('new_group_message', notificationData);

      setChatInput('');

      const payload = {
        message: inputText,
        thread_id: threadId,
        sender: currentUser,
        timestamp: Date.now(),
      };

      socketRef.current.emit(
        'new_support_group_message',
        JSON.stringify(payload),
        (response) => {
          console.log('Payload ->', payload);

          console.log('Send Message Resp ->', response);

          if (response === 'success') {
            setMessageArray([
              ...messageArray,
              {
                message: inputText,
                threadId,
                sender: currentUser,
                timestamp: Date.now(),
              },
            ]);

            let tempArr1 = [...groupReadUnreadList];

            const objI = tempArr1.findIndex(
              (obj) => obj.group_name === selectedGroup,
            );
            tempArr1[objI].sender = currentUser;

            let newArr = [
              ...tempArr1.filter((o) => o.group_name === selectedGroup),
            ];
            tempArr1 = tempArr1.filter(
              (obj) => obj.group_name !== selectedGroup,
            );
            newArr = [...newArr, ...tempArr1];
            // console.log(newArr);
            setGroupReadUnreadList([...newArr]);
          } else {
            // console.log(response);
            WToast.show({data: 'Error Sending Message...'});
          }
        },
      );
    } else {
      WToast.show({data: 'Please Type Something...'});
    }
  };

  const onSendImage = async () => {
    try {
      const options = {
        type: [
          DocumentPicker.types.images,
          DocumentPicker.types.video,
          DocumentPicker.types.pdf,
        ],
      };

      let result = await DocumentPicker.pick(options);

      if (!result.cancelled) {
        // console.log('result', result);

        // const fPath = result.uri;

        setFile(result);

        // setFileUri(fPath);
        setIsFileModalOpen(true);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const initiateSendImage = async () => {
    if (file) {
      try {
        setIsFileLoading(true);

        const fileName = file.name.replace(' ', '_');

        const fullFileName = `${Date.now().toString()}${fileName}`;

        const fileObj = {
          uri: file.uri,
          name: fullFileName,
          type: file.type,
        };

        const options = {
          keyPrefix: 'uploads/',
          bucket: 'chatsgx',
          region: 'us-east-2',
          accessKey: S3_CONFIG.accessKeyId,
          secretKey: S3_CONFIG.secretAccessKey,
          successActionStatus: 201,
        };

        RNS3.put(fileObj, options).then((resp) => {
          if (resp.status !== 201) {
            throw new Error('Failed to upload image to S3');
          }
          // console.log(resp.body);

          const {body} = resp;

          const inputText = chatInput.trim();

          if (body?.postResponse?.location) {
            let message_data = {
              message: inputText,
              thread_id: threadId,
              sender: userObject.current.username,
              timestamp: Date.now(),
              filename: fileName,
              type: 'file',
              location: body?.postResponse?.location,
            };

            socketRef.current.emit(
              'new_support_group_message',
              JSON.stringify(message_data),
              (response) => {
                console.log('response', response);

                if (response === 'success') {
                  setMessageArray([...messageArray, {...message_data}]);
                } else {
                  console.log('Error in sending message');
                }
                setChatInput('');
                setFile('');
                setIsFileLoading(false);
                setIsFileModalOpen(false);
              },
            );
          }
        });
      } catch (error) {
        console.log('Error on FileUpload', error);
      }
    }
  };

  return (
    <>
      {isLoggedIn ? (
        <>
          <View style={styles.container}>
            <View style={styles.chatHeader}>
              <Image
                style={styles.agentAvatar}
                source={require('../../../assets/avatar.png')}
              />
              <View style={styles.header}>
                <Text style={styles.agentName}>Support Agent</Text>
                <Text style={styles.brandingText}>Powered By Chats.io</Text>
              </View>
            </View>
            {chatSection ? (
              <View style={[styles.chatContainer]}>
                <View style={styles.container}>
                  <FlatList
                    ref={flatListRef}
                    onContentSizeChange={() =>
                      flatListRef.current.scrollToEnd({animated: true})
                    }
                    onLayout={() =>
                      flatListRef.current.scrollToEnd({animated: true})
                    }
                    showsVerticalScrollIndicator={false}
                    style={styles.chatView}
                    data={messageArray}
                    keyExtractor={(item) => item.timestamp.toString()}
                    renderItem={({item, index}) => (
                      <ChatItem
                        isSent={item.sender === userObject.current.username}
                        data={item}
                      />
                    )}
                  />
                </View>
                {typingFlag && (
                  <View style={styles.typingContainer}>
                    <TypingAnimation
                      dotColor="#08152D"
                      dotMargin={3}
                      dotAmplitude={3}
                      dotRadius={2.5}
                      dotX={12}
                      dotY={6}
                      style={styles.typingAnimation}
                    />
                    <Text style={styles.typingMessage}>
                      {typingMessage || 'Hello'}
                    </Text>
                  </View>
                )}
                <View style={styles.chatInputContainer}>
                  <TouchableOpacity
                    onPress={onSendImage}
                    style={styles.attachButton}>
                    <Image
                      style={styles.icon}
                      source={require('../../../assets/attachment-icon.png')}
                    />
                  </TouchableOpacity>
                  <TextInput
                    placeholder="Type your message"
                    style={styles.chatInput}
                    value={chatInput}
                    onChangeText={(text) => {
                      setChatInput(text);
                      socketRef.current.emit(
                        'typing',
                        userObject.current.username,
                        threadId,
                      );
                    }}
                    placeholderTextColor="#878788"
                  />
                  <TouchableOpacity
                    style={styles.sendIcon}
                    onPress={handleSubmitMessage}>
                    <Image
                      style={styles.icon}
                      source={require('../../../assets/send-icon-white.png')}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                </View>
                <Animated.View
                  style={{
                    height: interpolate(chatKeyboardAnimation.current, {
                      inputRange: [0, 1],
                      outputRange: [0, keyboardHeight - indices.bottom],
                    }),
                  }}
                />
              </View>
            ) : (
              <View style={styles.loadingView}>
                <LoadingAnimation />
              </View>
            )}
          </View>
        </>
      ) : (
        <LoginReminder />
      )}
      <FileModal
        file={file}
        isOpen={isFileModalOpen}
        setIsOpen={setIsFileModalOpen}
        sendImage={initiateSendImage}
        setMessage={setChatInput}
        message={chatInput}
        isLoading={isFileLoading}
      />
    </>
  );
};

export default Chats;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  chatHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 15,
  },
  header: {},
  agentAvatar: {
    width: 30,
    height: 30,
    backgroundColor: 'white',
    borderRadius: 15,
    marginRight: 10,
  },
  agentName: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
  },
  brandingText: {
    fontFamily: 'Montserrat',
    color: 'white',
    fontSize: 8,
    opacity: 0.7,
  },
  chatContainer: {
    flex: 1,
    paddingHorizontal: 25,
    paddingTop: 25,
    backgroundColor: 'white',
  },
  chatView: {
    flex: 1,
  },
  chatInputContainer: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: 10,
    paddingTop: 10,
  },
  chatInput: {
    color: 'black',
    flex: 1,
    borderColor: '#08152D',
    borderWidth: 1,
    marginHorizontal: 15,
    paddingHorizontal: 10,
    paddingVertical: 4,
    borderRadius: 8,
    fontFamily: 'Montserrat',
    maxHeight: 100,
    minHeight: 40,
  },
  attachButton: {
    width: 25,
    height: 25,
    padding: 2,
  },
  icon: {
    flex: 1,
    width: null,
    height: null,
  },
  sendIcon: {
    backgroundColor: '#08152D',
    width: 60,
    maxHeight: 40,
    padding: 10,
    borderRadius: 18,
  },
  loadingView: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  typingContainer: {
    height: 20,
  },
  typingAnimation: {},
  typingMessage: {
    marginLeft: 35,
    fontFamily: 'Montserrat',
  },
});
