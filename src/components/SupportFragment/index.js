import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {SupportContext} from '../../contexts/SupportContext';
import Ticket from './Ticket';
import Chats from './Chats';
import Bots from './Bots';
import LearnView from './Learn/LearnView';
import ChatsIoMenu from './ChatIoMenu';
import OneToOneChat from './Bots/OneToOneChat';
import {AppContext} from '../../contexts/AppContextProvider';
import {useNavigation} from '@react-navigation/native';
import {OneToOneChatProvider} from '../../contexts/OneToOneChatContext';

const SupportFragment = ({routeParams}) => {
  const {navigate} = useNavigation();

  const {isLoggedIn} = useContext(AppContext);

  const {activeTab, setActiveTab, setIsFullScreen} = useContext(SupportContext);
  const [activeFragment, setActiveFragment] = useState();

  console.log({routeParams});

  useEffect(() => {
    if (routeParams?.openMessage) {
      setActiveTab({
        title: 'Chats',
        icon: require('../../assets/support-category-icons/chats-icon.png'),
      });
    } else if (
      routeParams?.openLearn ||
      routeParams?.openArticle ||
      routeParams?.openVideo
    ) {
      setActiveTab({
        title: 'Learn',
        icon: require('../../assets/support-category-icons/faq-icon.png'),
      });
    } else if (routeParams?.openUserChat) {
      setActiveTab({
        title: 'Chat',
        icon: require('../../assets/one-to-one-chat-icon.png'),
      });
    } else if (routeParams?.openMyLicenses || routeParams?.openBots) {
      setActiveTab({
        title: 'Bots',
        icon: require('../../assets/support-category-icons/bots-icon.png'),
      });
    }
  }, [routeParams]);

  useEffect(() => {
    switch (activeTab?.title) {
      case 'Menu':
        setActiveFragment(<ChatsIoMenu key={Date.now().toString()} />);
        break;
      case 'Bots':
        setActiveFragment(<Bots key={Date.now().toString()} />);
        break;
      case 'Chat':
        if (isLoggedIn) {
          setActiveFragment(
            <OneToOneChatProvider>
              <OneToOneChat paramUser={routeParams?.openUserChat || null} />
            </OneToOneChatProvider>,
          );
        } else {
          navigate('Landing');
        }
        break;
      case 'Chats':
        setActiveFragment(
          <Chats
            inputMessage={
              routeParams.openMessage ? routeParams.messageData : ''
            }
          />,
        );
        break;
      case 'Ticket':
        setActiveFragment(<Ticket />);
        break;
      case 'Learn':
        setActiveFragment(
          <LearnView
            videoData={
              routeParams.openVideo
                ? {
                    videoData: routeParams.videoData,
                    selectedCategory: routeParams.category,
                  }
                : ''
            }
            articleData={
              routeParams.openArticle
                ? {
                    articleData: routeParams.articleData,
                    selectedCategory: routeParams.category,
                  }
                : ''
            }
          />,
        );
        break;
      default:
        setActiveFragment(<Bots key={Date.now().toString()} />);
    }
    setIsFullScreen(false);
  }, [activeTab, routeParams]);

  return (
    <View style={styles.container}>
      <View style={styles.fragmentContainer}>{activeFragment}</View>
    </View>
  );
};

export default SupportFragment;

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: '#08152D'},
  fragmentContainer: {flex: 1},
});
