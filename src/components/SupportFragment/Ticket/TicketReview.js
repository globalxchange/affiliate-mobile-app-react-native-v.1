import React, {useContext} from 'react';
import {StyleSheet, Text, View, TextInput, ScrollView} from 'react-native';
import CustomPicker from '../../CustomPicker';
import PrimaryButton from '../../PrimaryButton';
import {SupportContext} from '../../../contexts/SupportContext';
import {WToast} from 'react-native-smart-tip';

const TicketReview = ({onNextStep, onEditStep}) => {
  const {
    ticketIssueType,
    ticketIssueSubType,
    ticketMessage,
    selectedImage,
    ticketEmailId,
    ticketPhoneNumber,
  } = useContext(SupportContext);

  const onDoneClick = () => {
    if (!ticketIssueType) {
      return WToast.show({data: 'Please select a issue type'});
    }
    if (!ticketIssueSubType) {
      return WToast.show({data: 'Please select a sub issue type'});
    }

    if (!ticketMessage.trim()) {
      return WToast.show({data: 'Please input your message'});
    }

    if (!ticketPhoneNumber && !ticketEmailId) {
      return WToast.show({data: 'Please provide any contact preference'});
    }

    onNextStep();
  };

  return (
    <ScrollView style={styles.container}>
      <Text style={styles.header}>Review Ticket</Text>
      <CustomPicker
        placeHolder="Type Of Issue"
        icon={require('../../../assets/man-icon.png')}
        value={ticketIssueType ? ticketIssueType.category : ''}
      />
      <CustomPicker
        placeHolder="Sub Issue Type"
        icon={require('../../../assets/envelope-icon.png')}
        value={ticketIssueSubType ? ticketIssueSubType.subcategory : ''}
      />
      {selectedImage ? (
        <View style={styles.names}>
          <Text style={styles.textHeader}>FileName</Text>
          <Text style={styles.textItem}>
            {selectedImage.uri.split('/').pop()}
          </Text>
        </View>
      ) : null}
      {ticketEmailId || ticketPhoneNumber ? (
        <View style={styles.names}>
          <Text style={styles.textHeader}>
            {ticketEmailId ? 'Email' : 'Phone'}
          </Text>
          <Text style={styles.textItem}>
            {ticketEmailId || ticketPhoneNumber}
          </Text>
        </View>
      ) : null}
      <View style={styles.inputContainer}>
        <Text style={styles.textHeader}>Message</Text>
        <TextInput
          editable={false}
          style={styles.input}
          placeholder="Message"
          value={ticketMessage}
          returnKeyType="done"
          clearButtonMode="while-editing"
        />
      </View>
      <View style={styles.row}>
        <PrimaryButton
          style={{marginRight: 10}}
          title="Edit"
          onPress={onEditStep}
        />
        <PrimaryButton
          style={{marginLeft: 10}}
          title="Send Ticket"
          onPress={onDoneClick}
        />
      </View>
    </ScrollView>
  );
};

export default TicketReview;

const styles = StyleSheet.create({
  container: {},
  header: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    marginBottom: 15,
  },
  row: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  inputContainer: {
    borderColor: '#E9E8E8',
    borderWidth: 1,
    borderRadius: 12,
    paddingHorizontal: 15,
    marginBottom: 20,
    paddingVertical: 10,
  },
  input: {
    color: '#000000',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 14,
    paddingRight: 10,
  },
  names: {
    marginBottom: 20,
    borderColor: '#E9E8E8',
    borderWidth: 1,
    borderRadius: 12,
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
  textHeader: {
    color: '#858585',
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
  textItem: {
    color: '#000000',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
});
