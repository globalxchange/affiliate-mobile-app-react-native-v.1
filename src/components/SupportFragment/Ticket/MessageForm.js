import React, {useContext} from 'react';
import {StyleSheet, View, Text, TextInput} from 'react-native';
import {SupportContext} from '../../../contexts/SupportContext';
import {WToast} from 'react-native-smart-tip';

const MessageForm = ({onNextStep}) => {
  const {ticketMessage, setTicketMessage} = useContext(SupportContext);

  const onSubmit = () => {
    if (ticketMessage.trim()) {
      onNextStep();
    } else {
      WToast.show({data: 'Please input your message'});
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Submit Ticket</Text>
      <View style={styles.inputContainer}>
        <TextInput
          style={styles.input}
          placeholder="Message"
          // multiline
          value={ticketMessage}
          onChangeText={(text) => setTicketMessage(text)}
          returnKeyType="done"
          autoFocus={true}
          // selectionColor={colors.orangeColor}
          onSubmitEditing={onSubmit}
          clearButtonMode="while-editing"
          placeholderTextColor={'#878788'}
        />
      </View>
    </View>
  );
};

export default MessageForm;

const styles = StyleSheet.create({
  container: {},
  header: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    marginBottom: 15,
  },
  inputContainer: {
    borderColor: '#E9E8E8',
    borderWidth: 1,
    borderRadius: 12,
    paddingHorizontal: 15,
    marginBottom: 20,
    height: 150,
  },
  input: {
    color: '#000000',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 14,
    paddingRight: 10,
    paddingVertical: 5,
  },
});
