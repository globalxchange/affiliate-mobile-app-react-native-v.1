import React, {useState, useContext, useEffect, useRef} from 'react';
import {StyleSheet, View, Keyboard, Platform} from 'react-native';
import TickerForm from './TickerForm';
import MessageForm from './MessageForm';
import TicketReview from './TicketReview';
import LoadingView from './LoadingView';
import TicketSuccess from './TicketSuccess';
import ContactPreference from './ContactPreference';
import {AppContext} from '../../../contexts/AppContextProvider';
import LoginReminder from '../LoginReminder';
import Axios from 'axios';
import {SUPPORT_TICKET_API} from '../../../configs';
import LoadingAnimation from '../../LoadingAnimation';
import {SupportContext} from '../../../contexts/SupportContext';
import Animated, {
  useCode,
  set,
  Clock,
  interpolate,
} from 'react-native-reanimated';
import ReanimatedTimingHelper from '../../../utils/ReanimatedTimingHelper';

const Ticket = () => {
  const {isLoggedIn} = useContext(AppContext);
  const {clearTicketState} = useContext(SupportContext);

  const [step, setStep] = useState(0);

  const [categories, setCategories] = useState();
  const [subCategories, setSubCategories] = useState();
  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);

  const chatKeyboardAnimation = useRef(new Animated.Value(0));

  const onKeyBoardShow = (e) => {
    if (Platform.OS === 'ios') {
      setKeyboardHeight(e.endCoordinates.height);
      setIsKeyboardOpen(true);
    }
  };

  const onKeyBoardHide = () => {
    if (Platform.OS === 'ios') {
      setIsKeyboardOpen(false);
    }
  };

  useEffect(() => {
    Axios.get(`${SUPPORT_TICKET_API}/sup_ctg`).then((resp) => {
      const {data} = resp;
      if (data.status) {
        setCategories(data.data);
      }
    });

    Axios.get(`${SUPPORT_TICKET_API}/sup_sub_ctg`).then((resp) => {
      const {data} = resp;
      if (data.status) {
        setSubCategories(data.data);
      }
    });
    Keyboard.addListener('keyboardDidShow', onKeyBoardShow);
    Keyboard.addListener('keyboardDidHide', onKeyBoardHide);
    return () => {
      clearTicketState();
      Keyboard.removeListener('keyboardDidShow', onKeyBoardShow);
      Keyboard.removeListener('keyboardDidHide', onKeyBoardHide);
    };
  }, []);

  const renderComponent = () => {
    switch (step) {
      case 0:
        return (
          <TickerForm
            onDone={() => setStep(2)}
            onTypeMore={() => setStep(1)}
            openContactPreference={() => setStep(5)}
            categories={categories}
            subCategories={subCategories}
          />
        );
      case 1:
        return <MessageForm onNextStep={() => setStep(2)} />;
      case 2:
        return (
          <TicketReview
            onNextStep={() => setStep(3)}
            onEditStep={() => setStep(0)}
          />
        );
      case 3:
        return (
          <LoadingView
            onComplete={() => setStep(4)}
            onError={() => setStep(2)}
          />
        );

      case 4:
        return <TicketSuccess />;

      case 5:
        return <ContactPreference goBack={() => setStep(0)} />;
      default:
        return (
          <TickerForm
            onDone={() => setStep(2)}
            onTypeMore={() => setStep(1)}
            openContactPreference={() => setStep(5)}
            categories={categories}
            subCategories={subCategories}
          />
        );
    }
  };

  useCode(
    () =>
      isKeyboardOpen
        ? [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ]
        : [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ],
    [isKeyboardOpen],
  );

  return (
    <>
      {isLoggedIn ? (
        <Animated.View
          style={[
            styles.container,
            {
              transform: [
                {
                  translateY: interpolate(chatKeyboardAnimation.current, {
                    inputRange: [0, 1],
                    outputRange: [0, -keyboardHeight + 45],
                  }),
                },
              ],
            },
          ]}>
          {categories && subCategories ? (
            renderComponent()
          ) : (
            <View style={styles.loginContainer}>
              <LoadingAnimation />
            </View>
          )}
        </Animated.View>
      ) : (
        <LoginReminder />
      )}
    </>
  );
};

export default Ticket;

const styles = StyleSheet.create({
  container: {
    marginTop: 'auto',
    backgroundColor: 'white',
    borderTopEndRadius: 10,
    borderTopStartRadius: 10,
    padding: 25,
  },
  loginContainer: {
    paddingVertical: 50,
  },
});
