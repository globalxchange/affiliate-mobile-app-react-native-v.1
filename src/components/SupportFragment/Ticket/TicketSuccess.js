import React from 'react';
import {StyleSheet, Text, View, FlatList} from 'react-native';
import FAQCard from './FAQCard';

const TicketSuccess = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.header}>Ticket #1 Has Been Submitted</Text>
      <Text style={styles.info}>
        People Who Has Similar Issues Found These Article Helpful
      </Text>
      <FlatList
        style={styles.flatList}
        horizontal
        showsHorizontalScrollIndicator={false}
        data={list}
        keyExtractor={(item) => item.header}
        renderItem={({item, index}) => <FAQCard data={item} />}
      />
    </View>
  );
};

export default TicketSuccess;

const styles = StyleSheet.create({
  container: {},
  header: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    marginBottom: 15,
  },
  info: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
  flatList: {
    marginTop: 40,
    marginBottom: 20,
  },
});

const list = [
  {
    header: 'GET STARTED',
    image: require('../../../assets/project-management-icon.png'),
    subHeading: 'Lorem Ipsum is simply',
  },
  {
    header: 'ACCOUNTS',
    image: require('../../../assets/accounts-icon.png'),
    subHeading: 'Lorem Ipsum is simply',
  },
  {
    header: 'SUBSCRIPTION',
    image: require('../../../assets/subscription-models-icon.png'),
    subHeading: 'Lorem Ipsum is simply',
  },
  {
    header: 'HELP',
    image: require('../../../assets/help-icon.png'),
    subHeading: 'Lorem Ipsum is simply',
  },
];
