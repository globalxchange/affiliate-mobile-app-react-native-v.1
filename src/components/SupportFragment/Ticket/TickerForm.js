/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Image,
  TouchableOpacity,
  TextInput,
  FlatList,
} from 'react-native';
import {Constants} from 'react-native-unimodules';
import * as ImagePicker from 'expo-image-picker';
import PrimaryButton from '../../PrimaryButton';
import OutlinedButton from '../../OutlinedButton';
import {SupportContext} from '../../../contexts/SupportContext';
import {WToast} from 'react-native-smart-tip';

const TickerForm = ({
  onDone,
  onTypeMore,
  openContactPreference,
  categories,
  subCategories,
}) => {
  const {
    ticketIssueType,
    ticketIssueSubType,
    setTicketIssueType,
    setTicketIssueSubType,
    ticketPhoneNumber,
    ticketEmailId,
    ticketMessage,
    setSelectedImage,
    selectedImage,
  } = useContext(SupportContext);

  const [isCategoryOpen, setIsCategoryOpen] = useState(false);
  const [isSubCategoryOpen, setIsSubCategoryOpen] = useState(false);
  const [searchInput, setSearchInput] = useState('');

  useEffect(() => {
    (async () => {
      if (Constants.platform.ios) {
        const {status} = await ImagePicker.requestCameraRollPermissionsAsync();
        if (status !== 'granted') {
          // alert('Sorry, we need camera roll permissions to make this work!');
        }
      }
    })();
  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: false,
      quality: 0.8,
    });

    if (!result.cancelled) {
      setSelectedImage({
        uri: result.uri,
        name: `${Date.now()}.jpg`,
        type: 'image/jpeg',
      });
    }
  };

  const onOptionSelected = (option) => {
    if (isCategoryOpen) {
      setTicketIssueType(option);
      setTicketIssueSubType(null);
    } else {
      setTicketIssueSubType(option);
    }
    setIsCategoryOpen(false);
    setIsSubCategoryOpen(false);
  };

  const onDoneClick = () => {
    if (!ticketIssueType) {
      return WToast.show({data: 'Please select a issue type'});
    }
    if (!ticketIssueSubType) {
      return WToast.show({data: 'Please select a sub issue type'});
    }

    if (!ticketMessage.trim()) {
      return WToast.show({data: 'Please input your message'});
    }

    if (!ticketPhoneNumber && !ticketEmailId) {
      return WToast.show({data: 'Please provide any contact preference'});
    }

    onDone();
  };

  const onTypeMoreClick = () => {
    if (!ticketIssueType) {
      return WToast.show({data: 'Please select a issue type'});
    }
    if (!ticketIssueSubType) {
      return WToast.show({data: 'Please select a sub issue type'});
    }
    onTypeMore();
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Submit Ticket</Text>
      <TouchableWithoutFeedback onPress={() => setIsCategoryOpen(true)}>
        <View style={styles.dropdownContainer}>
          <View style={styles.inputContainer}>
            <Text style={styles.placeHolder}>Type Of Issue</Text>
            <Text style={styles.input}>
              {ticketIssueType ? ticketIssueType.category : 'Select Issue Type'}
            </Text>
          </View>
          <View style={styles.iconContainer}>
            <Image
              style={styles.icon}
              source={require('../../../assets/man-icon.png')}
              resizeMode="contain"
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
      <TouchableWithoutFeedback
        onPress={() => {
          if (ticketIssueType) {
            setIsSubCategoryOpen(true);
          } else {
            WToast.show({data: 'Please select type of issue first'});
          }
        }}>
        <View style={styles.dropdownContainer}>
          <View style={styles.inputContainer}>
            <Text style={styles.placeHolder}>Sub Issue Type</Text>
            <Text style={styles.input}>
              {ticketIssueSubType
                ? ticketIssueSubType.subcategory
                : 'Select Issue Type'}
            </Text>
          </View>
          <View style={styles.iconContainer}>
            <Image
              style={styles.icon}
              source={require('../../../assets/envelope-icon.png')}
              resizeMode="contain"
            />
          </View>
        </View>
      </TouchableWithoutFeedback>

      {isCategoryOpen || isSubCategoryOpen ? (
        <View style={styles.modalView}>
          <Text style={styles.header}>
            {isCategoryOpen ? 'Search Categories' : 'Sub Category'}
          </Text>
          <View style={styles.modalHeader}>
            {isSubCategoryOpen && ticketIssueType && (
              <TouchableOpacity
                style={styles.mainCategoryContainer}
                onPress={() => {
                  setIsSubCategoryOpen(false);
                  setIsCategoryOpen(true);
                }}>
                <Text style={styles.mainCategory}>
                  {ticketIssueType.category}
                </Text>
                <Image
                  style={styles.dropdownIcon}
                  source={require('../../../assets/dropdown-icon.png')}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            )}
            <TextInput
              style={[styles.input, styles.searchInput]}
              placeholder="Ex. Transactional"
              value={searchInput}
              onChangeText={(input) => setSearchInput(input)}
              returnKeyType="search"
              placeholderTextColor={'#878788'}
            />
            <View style={styles.searchIcon}>
              <Image
                source={require('../../../assets/search-icon.png')}
                style={styles.icon}
                resizeMode="contain"
              />
            </View>
          </View>
          <FlatList
            showsVerticalScrollIndicator={false}
            style={styles.optionsList}
            data={
              isCategoryOpen
                ? categories
                : subCategories.filter(
                    (item) => item.categoryID === ticketIssueType._id,
                  )
            }
            keyExtractor={(item) => item._id}
            renderItem={({item}) => (
              <TouchableOpacity
                style={styles.optionItem}
                onPress={() => onOptionSelected(item)}>
                <View style={styles.optionDot} />
                <Text style={styles.optionText}>
                  {isCategoryOpen ? item.category : item.subcategory}
                </Text>
                <Text style={styles.optionTime}>24Hr Replay Time</Text>
              </TouchableOpacity>
            )}
            ListEmptyComponent={
              <View style={styles.emptyContainer}>
                <Text style={styles.emptyText}>No Items Found</Text>
              </View>
            }
          />
        </View>
      ) : null}
      <View style={styles.row}>
        <OutlinedButton
          onPress={pickImage}
          title={selectedImage ? 'Image Selected' : 'Upload Screenshot'}
          style={{marginRight: 10}}
        />
        <OutlinedButton
          title={
            ticketPhoneNumber || ticketEmailId ? 'Phone' : 'Contact Preference'
          }
          style={{marginLeft: 10}}
          onPress={openContactPreference}
          active={ticketPhoneNumber || ticketEmailId ? true : false}
        />
      </View>
      <View style={styles.row}>
        <PrimaryButton
          style={{marginRight: 10}}
          title="I’m Done"
          onPress={onDoneClick}
        />
        <PrimaryButton
          style={{marginLeft: 10}}
          title="Type More"
          onPress={onTypeMoreClick}
        />
      </View>
    </View>
  );
};

export default TickerForm;

const styles = StyleSheet.create({
  container: {},
  header: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    marginBottom: 15,
  },
  row: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  dropdownContainer: {
    borderColor: '#E9E8E8',
    borderWidth: 1,
    borderRadius: 12,
    paddingHorizontal: 15,
    paddingVertical: 6,
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  inputContainer: {
    flex: 1,
  },
  placeHolder: {
    color: '#858585',
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
  input: {
    flex: 1,
    color: '#000000',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 14,
    paddingRight: 10,
    backgroundColor: 'white',
    paddingVertical: 3,
  },
  searchInput: {
    paddingLeft: 10,
  },
  iconContainer: {
    borderColor: '#F6F6F6',
    borderWidth: 1,
    width: 40,
    height: 40,
    padding: 10,
    borderRadius: 20,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  icon: {
    flex: 1,
    height: null,
    width: null,
  },
  modalView: {
    position: 'absolute',
    zIndex: 1,
    top: -25,
    left: -25,
    bottom: -25,
    right: -25,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: 'white',
    paddingVertical: 25,
    paddingHorizontal: 25,
  },
  modalHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  mainCategoryContainer: {
    borderRightColor: '#CACACA',
    borderRightWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingRight: 10,
    paddingLeft: 5,
  },
  mainCategory: {
    fontFamily: 'Montserrat-Bold',
    color: '#788995',
  },
  dropdownIcon: {
    width: 10,
    marginLeft: 10,
  },
  closeButton: {
    width: 28,
    height: 28,
    padding: 8,
    marginRight: 10,
  },
  closeIcon: {
    flex: 1,
    width: null,
    height: null,
  },
  searchIcon: {
    width: 28,
    height: 28,
    padding: 8,
    marginLeft: 10,
  },
  optionsList: {
    marginTop: 20,
  },
  optionItem: {
    paddingVertical: 12,
    borderBottomColor: '#EBEBEB',
    borderBottomWidth: 1,
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  optionDot: {
    width: 14,
    height: 14,
    borderRadius: 7,
    backgroundColor: '#FEDB41',
    marginRight: 15,
  },
  optionText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    flex: 1,
  },
  optionTime: {
    fontFamily: 'Montserrat',
    color: '#08152D',
    fontSize: 12,
    marginLeft: 15,
  },
  emptyContainer: {
    marginTop: 40,
  },
  emptyText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'black',
    textAlign: 'center',
    fontSize: 20,
  },
});
