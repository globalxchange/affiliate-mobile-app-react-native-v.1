import {useNavigation} from '@react-navigation/native';
import React, {useContext, useEffect, useState} from 'react';
import {FlatList, Image, StyleSheet, Text, TextInput, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import ThemeData from '../../../configs/ThemeData';
import {SupportContext} from '../../../contexts/SupportContext';
import {AppContext} from '../../../contexts/AppContextProvider';

const ChatsIoMenu = () => {
  const {navigate} = useNavigation();
  const {setActiveTab} = useContext(SupportContext);
  const {isLoggedIn} = useContext(AppContext);

  const [searchText, setSearchText] = useState('');
  const [searchList, setSearchList] = useState();

  useEffect(() => {
    const searchQuery = searchText.toLowerCase().trim();

    setSearchList(
      menus.filter((x) => x.title?.toLowerCase().includes(searchQuery)),
    );
  }, [searchText]);

  const menus = [
    {
      title: 'Speak To Support',
      icon: require('../../../assets/qr-black.png'),
      onPress: () =>
        isLoggedIn ? setActiveTab({title: 'Chats'}) : navigate('Landing'),
    },
    {
      title: 'Manage Contacts',
      icon: require('../../../assets/wallet-icon-menu-support.png'),
      disabled: true,
    },
    {
      title: 'Schedule A Meeting',
      icon: require('../../../assets/currency-icon-support.png'),
      disabled: true,
    },
    {
      title: 'Chats.io MarketPlace',
      icon: require('../../../assets/exit-support-icon.png'),
      disabled: true,
    },
    {
      title: 'Edit Profile',
      icon: require('../../../assets/exit-support-icon.png'),
      onPress: () => (isLoggedIn ? navigate('Settings') : navigate('Landing')),
    },
  ];

  return (
    <View style={styles.container}>
      <View style={styles.searchContainer}>
        <Image
          style={styles.searchIcon}
          resizeMode="contain"
          source={require('../../../assets/search-icon.png')}
        />
        <TextInput
          style={styles.inputText}
          placeholder="What Do You Want To Do?"
          placeholderTextColor={'#878788'}
          value={searchText}
          onChangeText={(text) => setSearchText(text)}
        />
      </View>
      <FlatList
        data={searchList}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item) => item.title}
        renderItem={({item}) => (
          <TouchableOpacity disabled={item.disabled} onPress={item.onPress}>
            <View
              style={[
                styles.menuItemContainer,
                item.disabled && {opacity: 0.5},
              ]}>
              <Image
                source={item.icon}
                resizeMode="contain"
                style={styles.itemIcon}
              />
              <Text style={styles.menuName}>{item.title}</Text>
            </View>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

export default ChatsIoMenu;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: 20,
  },
  menuItemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 25,
    paddingVertical: 20,
    marginTop: 15,
  },
  itemIcon: {
    width: 30,
    height: 30,
  },
  menuName: {
    flex: 1,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    marginLeft: 10,
  },
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 20,
  },
  searchIcon: {
    width: 20,
    height: 20,
  },
  inputText: {
    flex: 1,
    marginLeft: 10,
    height: 50,
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
  },
});
