import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';

const LoginReminder = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <Text style={styles.loginText}>Please Login To Use This Service</Text>
      <TouchableOpacity
        style={styles.loginButton}
        onPress={() => navigation.navigate('Landing')}>
        <Text style={styles.buttonText}>Login Here</Text>
      </TouchableOpacity>
    </View>
  );
};

export default LoginReminder;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 30,
  },
  loginText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
  },
  loginButton: {
    marginTop: 30,
    backgroundColor: 'white',
    paddingHorizontal: 25,
    paddingVertical: 8,
    borderRadius: 6,
  },
  buttonText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
  },
});
