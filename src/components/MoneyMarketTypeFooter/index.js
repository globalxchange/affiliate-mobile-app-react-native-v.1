import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import React from 'react';
import ThemeData from '../../configs/ThemeData';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

const TYPES = [
  {
    key: 'crypto',
    name: 'Crypto',
    icon: require('../../assets/crypto.png'),
  },
  {
    key: 'fiat',
    name: 'Forex',
    icon: require('../../assets/forex.png'),
  },
];

const MoneyMarketTypeFooter = ({type, setType}) => {
  const {bottom} = useSafeAreaInsets();
  return (
    <View style={[styles.container, {marginBottom: bottom / 2}]}>
      <FlatList
        style={styles.listView}
        contentContainerStyle={styles.listViewIn}
        horizontal={true}
        data={TYPES}
        keyExtractor={(item) => item.key}
        showsHorizontalScrollIndicator={false}
        renderItem={({item}) => (
          <TouchableOpacity
            key={item.key}
            onPress={() => setType(item.key)}
            style={[
              styles.coinItm,
              {
                borderColor:
                  item.key === type
                    ? ThemeData.TEXT_COLOR
                    : ThemeData.BORDER_COLOR,
              },
            ]}>
            <Image source={item.icon} style={styles.icon} />
            <Text style={styles.name}>{item.name}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

export default MoneyMarketTypeFooter;

const styles = StyleSheet.create({
  container: {
    height: 70,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 30,
    borderTopColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
  },
  listView: {
    height: 70,
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  listViewIn: {
    alignItems: 'center',
  },
  coinItm: {
    height: 40,
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 10,
    flexDirection: 'row',
    paddingHorizontal: 15,
  },
  name: {
    fontSize: 16,
  },
  icon: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
    marginRight: 4,
  },
});
