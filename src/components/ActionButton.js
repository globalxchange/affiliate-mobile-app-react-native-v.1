import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';

const ActionButton = ({text, onPress}) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <Text style={styles.text}>{text}</Text>
    </TouchableOpacity>
  );
};

export default ActionButton;

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
    backgroundColor: '#08152D',
    height: 45,
    borderRadius: 6,
    justifyContent: 'center',
  },
  text: {
    color: 'white',
    textAlign: 'center',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
  },
});
