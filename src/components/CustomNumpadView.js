import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import Clipboard from '@react-native-community/clipboard';
import {WToast} from 'react-native-smart-tip';

const CustomNumpadView = ({currentText, updatedCallback}) => {
  const copyClickHandler = () => {
    Clipboard.setString('');

    WToast.show({data: 'Copied to clipboard', position: WToast.position.TOP});
  };

  const pasteClickHandler = async () => {
    const pastedString = await Clipboard.getString();

    const parsedValue = parseFloat(pastedString);

    if (parsedValue) {
      this.keyPressCallBack(parsedValue.toString());
    }
  };

  const keyPressCallBack = (pressedKey) => {
    const input = pressedKey.toString();

    if (input) {
      let oldText = currentText;

      let newText;
      if (input === 'BACK') {
        newText = oldText.toString().slice(0, -1);
      } else if (input === 'CLEAR') {
        newText = '';
      } else if (input === '.') {
        if (!oldText.toString().includes('.')) {
          console.log('Dot comes here');
          newText = oldText.toString() + input;
        } else {
          newText = oldText.toString();
        }
      } else {
        newText = oldText.toString().concat(input);
      }

      updatedCallback(newText);
    }
  };

  return (
    <View style={styles.numpadContainer}>
      <View style={{flexDirection: 'row'}}>
        <View style={styles.keyBoardContainer}>
          {numPadButtons.map((item, index) => (
            <View key={index} style={styles.numpadRow}>
              {item.row.map((numKey, numKeyIndex) => (
                <View key={numKeyIndex} style={styles.numKeyContainer}>
                  <TouchableOpacity
                    style={styles.numKey}
                    onPress={() =>
                      keyPressCallBack(numKey.isControl ? 'BACK' : numKey.value)
                    }>
                    {numKey.isControl ? (
                      <Image
                        style={styles.backIcon}
                        source={require('../assets/numpad-back-icon.png')}
                        resizeMode="contain"
                      />
                    ) : (
                      <Text style={styles.numText}>{numKey.value}</Text>
                    )}
                  </TouchableOpacity>
                </View>
              ))}
            </View>
          ))}
        </View>
        <View style={styles.numpadColum}>
          <View style={styles.columnContainer}>
            <TouchableOpacity style={styles.numKey} onPress={copyClickHandler}>
              <Image
                style={styles.backIcon}
                source={require('../assets/copy-icon-white.png')}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
          <View style={styles.columnContainer}>
            <TouchableOpacity style={styles.numKey} onPress={pasteClickHandler}>
              <Image
                style={styles.backIcon}
                source={require('../assets/paste-icon.png')}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
          <View style={styles.columnContainer}>
            <TouchableOpacity
              style={styles.numKey}
              onPress={() => keyPressCallBack('CLEAR')}>
              <Image
                style={styles.backIcon}
                source={require('../assets/clear-icon.png')}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
          <View style={styles.columnContainer}>
            <TouchableOpacity style={styles.numKey} onPress={() => {}}>
              <Image
                style={styles.backIcon}
                source={require('../assets/hide-keyboard-icon.png')}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

export default CustomNumpadView;

const styles = StyleSheet.create({
  numpadContainer: {},
  keyBoardContainer: {
    backgroundColor: '#001D41',
    paddingBottom: 15,
    flexGrow: 1,
    width: 0,
  },
  numpadRow: {flexDirection: 'row'},
  numpadColum: {
    backgroundColor: '#001D41',
    width: 70,
  },
  numKeyContainer: {
    width: '33.333%',
    height: 70,
  },
  columnContainer: {
    height: 70,
  },
  numKey: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
    backgroundColor: '#001D41',
  },
  numText: {
    color: '#FFFFFF',
    fontFamily: 'Montserrat',
    fontSize: 20,
    fontWeight:"500"
  },
  backIcon: {height: 20},
});

const numPadButtons = [
  {
    row: [
      {value: '7', isControl: false},
      {value: '8', isControl: false},
      {value: '9', isControl: false},
    ],
  },
  {
    row: [
      {value: '4', isControl: false},
      {value: '5', isControl: false},
      {value: '6', isControl: false},
    ],
  },

  {
    row: [
      {value: '1', isControl: false},
      {value: '2', isControl: false},
      {value: '3', isControl: false},
    ],
  },
  {
    row: [
      {value: '.', isControl: false},
      {value: '0', isControl: false},
      {
        value: require('../assets/numpad-back-icon.png'),
        isControl: true,
      },
    ],
  },
];
