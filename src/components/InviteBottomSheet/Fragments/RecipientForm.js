import React, {useState} from 'react';
import {Image, StyleSheet, Text, TextInput, View} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import ActionButton from '../ActionButton';
import PhoneInput from './PhoneInput';
import RecipientType from './RecipientType';

const RecipientForm = ({
  onNext,
  setName,
  setEmail,
  setPhone,
  currency,
  setIsMail,
}) => {
  const [userName, setUserName] = useState('');
  const [contact, setContact] = useState('');
  const [showType, setShowType] = useState(true);
  const [showContact, setShowContact] = useState(false);
  const [isMailSelected, setIsMailSelected] = useState(true);

  const onNextClick = () => {
    if (!contact && !contact) {
      if (!userName) {
        return WToast.show({
          data: 'Please Enter The Name',
          position: WToast.position.TOP,
        });
      }
      setShowType(true);
      setName(userName);
    } else {
      if (!contact) {
        return WToast.show({
          data: `Please Enter The ${isMailSelected ? 'Mail' : 'Phone Number'}`,
          position: WToast.position.TOP,
        });
      }

      if (isMailSelected) {
        setEmail(contact.toLowerCase().trim());
      } else {
        setPhone(contact);
      }
      setIsMail(isMailSelected);
      onNext();
    }
  };

  if (showType) {
    return (
      <RecipientType
        setIsMailSelected={setIsMailSelected}
        onNext={() => {
          setShowType(false);
          setShowContact(true);
        }}
      />
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.controlContainer}>
        <View>
          <Text style={styles.header}>
            {showContact ? 'Destination' : 'Recipient Name'}
          </Text>
        </View>
        {!showContact ? (
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.input}
              placeholderTextColor="#9A9A9A"
              placeholder={'Enter Name'}
              value={userName}
              keyboardType={'default'}
              onChangeText={(text) => setUserName(text)}
              returnKeyType="done"
            />
            <View style={styles.iconContainer}>
              <Image
                style={styles.icon}
                resizeMode="contain"
                source={require('../../../assets/default-breadcumb-icon/recipient.png')}
              />
            </View>
          </View>
        ) : isMailSelected ? (
          <View style={styles.inputContainer}>
            <TextInput
              style={styles.input}
              placeholderTextColor="#9A9A9A"
              placeholder={'Enter Email'}
              value={contact}
              keyboardType={'default'}
              onChangeText={(text) => setContact(text)}
              returnKeyType="done"
            />
            <View style={styles.iconContainer}>
              <Image
                style={styles.icon}
                resizeMode="contain"
                source={require('../../../assets/gmail-icon.png')}
              />
            </View>
          </View>
        ) : (
          <PhoneInput onChangeText={setContact} />
        )}
      </View>
      <ActionButton text="Proceed" onPress={onNextClick} />
    </View>
  );
};

export default RecipientForm;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 40,
    paddingVertical: 20,
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 20,
    fontSize: 30,
    fontFamily: 'Montserrat-Bold',
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    marginBottom: 30,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  controlContainer: {
    justifyContent: 'center',
    marginBottom: 40,
  },
  inputContainer: {
    flexDirection: 'row',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    paddingLeft: 15,
    alignItems: 'center',
    height: 50,
    marginBottom: 30,
    overflow: 'hidden',
  },
  label: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  input: {
    flexGrow: 1,
    width: 0,
    paddingHorizontal: 15,
    fontFamily: 'Montserrat',
    color: 'black',
  },
  iconContainer: {
    borderLeftColor: '#EBEBEB',
    borderLeftWidth: 1,
    width: 50,
    paddingHorizontal: 15,
  },
  icon: {
    flex: 1,
    width: null,
  },
  buttonContainer: {
    flexDirection: 'row',
  },
  loadingContainer: {
    paddingVertical: 50,
    justifyContent: 'center',
  },
});
