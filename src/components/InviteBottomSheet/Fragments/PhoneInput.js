import React, {useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import countries from '../../../utils/countries';

const PhoneInput = ({onChangeText}) => {
  const [activeCountry, setActiveCountry] = useState('');
  const [numberInput, setNumberInput] = useState('');
  const [isCountryOpen, setIsCountryOpen] = useState(false);
  const [searchInput, setSearchInput] = useState('');
  const [searchResult, setSearchResult] = useState();

  useEffect(() => {
    const usa = countries.find((item) => item.alpha2Code === 'US');

    setActiveCountry(usa);
  }, []);

  useEffect(() => {
    if (isCountryOpen) {
      const query = searchInput.trim().toLowerCase();

      const newArray = countries.filter(
        (item) =>
          item.alpha2Code.toLowerCase().includes(query) ||
          item.alpha3Code.toLowerCase().includes(query) ||
          item.name.toLowerCase().includes(query) ||
          item.nativeName.toLowerCase().includes(query),
      );
      setSearchResult(newArray);
    }
  }, [isCountryOpen, searchInput]);

  useEffect(() => {
    if (activeCountry) {
      onChangeText(`+${activeCountry.callingCodes[0]}${numberInput}`);
    }
  }, [activeCountry, numberInput]);

  const onCountryItemClicked = (item) => {
    setActiveCountry(item);
    setIsCountryOpen(false);
    setSearchInput('');
  };

  return (
    <>
      <View style={styles.container}>
        <View style={styles.countryCodeContainer}>
          <Text
            style={styles.countryCode}
            onPress={() => setIsCountryOpen(true)}>
            +{activeCountry ? activeCountry.callingCodes[0] : '1'}
          </Text>
        </View>
        <View style={styles.inputContainer}>
          <TextInput
            placeholderTextColor="#9A9A9A"
            style={styles.input}
            returnKeyType="done"
            keyboardType="numeric"
            value={numberInput}
            onChangeText={(text) => setNumberInput(text)}
            placeholder={'00 000 000'}
          />
        </View>
      </View>

      {isCountryOpen && (
        <View style={styles.countryListContainer}>
          <View style={styles.searchContainer}>
            <TextInput
              style={styles.searchInput}
              onChangeText={(text) => setSearchInput(text)}
              value={searchInput}
              autoFocus={false}
              placeholder={'Type In The Country'}
              placeholderTextColor={'#878788'}
            />
            <Image
              style={styles.searchIcon}
              source={require('../../../assets/search-icon.png')}
              resizeMode="contain"
            />
          </View>
          <FlatList
            style={styles.list}
            data={searchResult}
            keyExtractor={(item) => item.alpha2Code}
            showsVerticalScrollIndicator={false}
            renderItem={({item}) => (
              <TouchableOpacity onPress={() => onCountryItemClicked(item)}>
                <View style={styles.countryItem}>
                  <Text style={styles.itemName}>{item.name}</Text>
                  <Text style={styles.itemCode}>+{item.callingCodes[0]}</Text>
                </View>
              </TouchableOpacity>
            )}
          />
        </View>
      )}
    </>
  );
};

export default PhoneInput;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingLeft: 15,
    alignItems: 'center',
    height: 50,
    marginBottom: 30,
    overflow: 'hidden',
  },
  countryCodeContainer: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    height: '100%',
    justifyContent: 'center',
    paddingHorizontal: 20,
    borderRadius: 6,
  },
  countryCode: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'black',
    fontSize: 18,
  },
  inputContainer: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    height: '100%',
    flex: 1,
    marginLeft: 5,
    borderRadius: 6,
  },
  input: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'black',
    fontSize: 18,
    textAlign: 'center',
    height: '100%',
  },
  countryListContainer: {
    position: 'absolute',
    backgroundColor: 'white',
    zIndex: 2,
    left: -30,
    right: -30,
    top: -100,
    bottom: -50,
    paddingHorizontal: 20,
  },
  searchContainer: {
    flexDirection: 'row',
    marginTop: 40,
    height: 45,
    paddingHorizontal: 15,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.15,
    shadowRadius: 2,
    backgroundColor: 'white',
    borderRadius: 2,
    elevation: 12,
  },
  searchInput: {
    flexGrow: 1,
    width: 0,
    marginTop: 'auto',
    marginBottom: 'auto',
    paddingHorizontal: 15,
    fontFamily: 'Montserrat',
    fontWeight: 'normal',
    color: 'black',
  },
  searchIcon: {
    height: 15,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  list: {
    marginTop: 20,
  },
  countryItem: {
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
    marginHorizontal: 1,
    marginTop: 15,
    flexDirection: 'row',
    paddingHorizontal: 25,
    paddingVertical: 20,
  },
  itemName: {
    flex: 1,
    color: '#001D41',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 15,
  },
  itemCode: {
    color: '#001D41',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 13,
    opacity: 0.5,
  },
});
