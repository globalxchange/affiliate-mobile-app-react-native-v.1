import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const CompleteView = ({onClose}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.header}>Congratulations</Text>
      <Text style={styles.subHeader}>
        Your prospect has just received a link to download the latest version of
        the AffiliateApp. Click Below To Learn More About The App
      </Text>
      <View style={styles.actionContainer}>
        <TouchableOpacity onPress={onClose} style={styles.button}>
          <Text style={styles.buttonText}>Close</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.button, {marginLeft: 20}]}>
          <Text style={styles.buttonText}>Register</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default CompleteView;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
    paddingHorizontal: 40,
    paddingBottom: 30,
    paddingTop: 10,
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    fontSize: 30,
    fontFamily: 'Montserrat-Bold',
    marginTop: 40,
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    fontSize: 12,
    fontFamily: 'Montserrat',
    marginVertical: 50,
  },
  actionContainer: {
    flexDirection: 'row',
  },
  button: {
    backgroundColor: '#08152D',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 6,
    height: 50,
    flex: 1,
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
  },
});
