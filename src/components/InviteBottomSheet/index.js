import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Image, StyleSheet, View} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import {APP_CODE, GX_API_ENDPOINT} from '../../configs';
import BottomSheetLayout from '../../layouts/BottomSheetLayout';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import LoadingAnimation from '../LoadingAnimation';
import Breadcrumb from './Breadcrumb';
import CompleteView from './Fragments/CompleteView';
import MessageForm from './Fragments/MessageForm';
import RecipientForm from './Fragments/RecipientForm';

const InviteBottomSheet = ({isOpen, setIsOpen, isIos}) => {
  const [currentStep, setCurrentStep] = useState();
  const [isEmail, setIsEmail] = useState(false);
  const [emailId, setEmailId] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [messageInput, setMessageInput] = useState(
    'Hey, Check The All New Affiliate App',
  );

  useEffect(() => {
    if (!isOpen) {
      resetState();
    }
  }, [isOpen]);

  const resetState = () => {
    setCurrentStep();
    setEmailId('');
    setPhoneNumber('');
    setMessageInput('Hey, Check The All New Affiliate App');
    setIsEmail(false);
  };

  const sendInvitation = async () => {
    setCurrentStep('Loading');

    const userEmail = await AsyncStorageHelper.getLoginEmail();

    let postData = {
      userEmail,
      app_code: APP_CODE,
      app_type: isIos ? 'ios' : 'android',
      custom_message: messageInput.trim(),
    };

    if (isEmail) {
      postData = {...postData, email: emailId.toLowerCase().trim()};
    } else {
      postData = {...postData, mobile: phoneNumber};
    }

    // console.log('PostData', postData);

    Axios.post(
      `${GX_API_ENDPOINT}/coin/vault/service/send/app/links/invite`,
      postData,
    )
      .then((resp) => {
        const {data} = resp;

        // console.log('Invite Resp', data);

        if (data.status) {
          setCurrentStep('Completed');
        } else {
          WToast.show({
            data: data.message || 'Error On Invite',
            position: WToast.position.TOP,
          });
          setCurrentStep('Message');
        }
      })
      .catch((error) => {
        console.log('Error On Invite', error);
        setCurrentStep('Message');
        WToast.show({data: 'Error On Invite', position: WToast.position.TOP});
      });
  };

  let activeViewContent;

  switch (currentStep) {
    case 'Message':
      activeViewContent = (
        <MessageForm
          onNext={sendInvitation}
          message={messageInput}
          setMessage={setMessageInput}
        />
      );
      break;

    case 'Loading':
      activeViewContent = (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      );
      break;

    case 'Completed':
      activeViewContent = <CompleteView onClose={() => setIsOpen(false)} />;
      break;

    default:
      activeViewContent = (
        <RecipientForm
          setEmail={setEmailId}
          setPhone={setPhoneNumber}
          setIsMail={setIsEmail}
          onNext={() => setCurrentStep('Message')}
        />
      );
  }

  return (
    <BottomSheetLayout
      isOpen={isOpen}
      onClose={() => setIsOpen(false)}
      reactToKeyboard>
      <View style={styles.fragmentContainer}>
        <Image
          source={require('../../assets/affliate-app-logo-dark.png')}
          style={styles.headerImage}
          resizeMode="contain"
        />
        <View style={styles.viewContainer}>
          {currentStep !== 'Completed' && <Breadcrumb />}
          {activeViewContent}
        </View>
      </View>
    </BottomSheetLayout>
  );
};

export default InviteBottomSheet;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    // flex: 1,
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    overflow: 'hidden',
  },
  fragmentContainer: {
    // flex: 1,
  },
  headerImage: {
    height: 35,
    width: 170,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginVertical: 20,
  },
  viewContainer: {
    borderTopColor: '#EBEBEB',
    borderTopWidth: 1,
  },
  loadingContainer: {
    paddingVertical: 70,
  },
});
