import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import FastImage from 'react-native-fast-image';

const Breadcrumb = ({
  selectedType,
  sendCurrency,
  recipientCurrency,
  destinationMail,
  clearForType,
  clearForSendingCurrency,
  clearForToCurrency,
  clearForRecipientAddress,
  clearForTransact,
}) => {
  const onTypeSelect = () => {
    clearForType();
  };

  const onSendCurrencySelect = () => {
    clearForSendingCurrency();
  };

  const onToCurrencySelect = () => {
    clearForToCurrency();
  };

  const onRecipientSelect = () => {
    clearForRecipientAddress();
  };

  const onTransactSelect = () => {
    clearForTransact();
  };

  return (
    <View style={styles.container}>
      <View style={[styles.itemContainer]}>
        <TouchableOpacity
          disabled={!selectedType}
          style={[styles.item]}
          onPress={onSendCurrencySelect}>
          <Image
            style={styles.icon}
            source={
              sendCurrency
                ? {uri: sendCurrency.image}
                : require('../../assets/phone-text-icon.png')
            }
            resizeMode="contain"
          />
          <Text style={styles.name}>Method</Text>
        </TouchableOpacity>
      </View>

      <View
        style={[
          styles.itemContainer,
          !destinationMail ? styles.disabled : null,
        ]}>
        <TouchableOpacity
          disabled={!(selectedType && sendCurrency && recipientCurrency)}
          style={styles.item}
          onPress={onRecipientSelect}>
          <Image
            style={styles.forwardIcon}
            resizeMode="contain"
            source={require('../../assets/forward-icon-grey.png')}
          />
          <FastImage
            style={styles.icon}
            source={require('../../assets/default-breadcumb-icon/recipient.png')}
            resizeMode="contain"
          />
          <Text style={styles.name}>Destination</Text>
        </TouchableOpacity>
      </View>
      <View
        style={[
          styles.itemContainer,
          !destinationMail ? styles.disabled : null,
        ]}>
        <Image
          style={styles.forwardIcon}
          resizeMode="contain"
          source={require('../../assets/forward-icon-grey.png')}
        />
        <TouchableOpacity
          disabled={
            !(
              selectedType &&
              sendCurrency &&
              recipientCurrency &&
              destinationMail
            )
          }
          onPress={onTransactSelect}
          style={styles.item}>
          <Image
            style={styles.icon}
            source={require('../../assets/default-breadcumb-icon/transact.png')}
            resizeMode="contain"
          />
          <Text style={styles.name}>Message</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Breadcrumb;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: 15,
  },

  itemContainer: {
    flex: 1,
    // backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
  },
  item: {
    width: '100%',
    paddingHorizontal: 12,
    alignItems: 'center',
    justifyContent: 'center',
  },
  disabled: {
    opacity: 0.35,
  },
  forwardIcon: {
    position: 'absolute',
    left: -6,
    width: 12,
  },
  icon: {
    height: 24,
    width: 24,
    marginBottom: 3,
  },
  name: {
    color: '#788995',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 8,
    width: '100%',
    overflow: 'hidden',
    height: 10,
    textAlign: 'center',
  },
});
