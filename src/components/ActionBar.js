import React, {useState, useContext} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Keyboard,
  Dimensions,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import AppStatusBar from './AppStatusBar';
import {AppContext} from '../contexts/AppContextProvider';
import ConnectIcon from '../assets/VectorIcons/connectLogo.svg';
import Connect from './Connect';
import ThemeData from '../configs/ThemeData';

const {width} = Dimensions.get('window');

const ActionBar = () => {
  const {isLoggedIn, isVideoFullScreen} = useContext(AppContext);

  const [isConnectOpen, setIsConnectOpen] = useState(false);

  const navigation = useNavigation();

  const openDrawer = () => {
    Keyboard.dismiss();
    navigation.openDrawer();
  };

  return (
    <>
      <AppStatusBar backgroundColor="#FFFFFF" barStyle="light-content" />
      <View
        style={[
          styles.container,
          {display: isVideoFullScreen ? 'none' : 'flex'},
        ]}>
        <TouchableOpacity style={styles.toggleButton} onPress={openDrawer}>
          <Image
            style={styles.menuIcon}
            source={require('../assets/menu-icon.png')}
            resizeMode="contain"
          />
        </TouchableOpacity>
        <View style={styles.appIconContainer}>
          <Image
            style={styles.appIcon}
            resizeMode="contain"
            source={require('../assets/affliate-app-logo-dark.png')}
          />
        </View>
        {/* <TouchableOpacity
          style={styles.sendButton}
          onPress={() =>
            isLoggedIn ? setIsConnectOpen(true) : navigation.navigate('Landing')
          }>
          <ConnectIcon style={styles.sendIcon} color={ThemeData.TEXT_COLOR} />
        </TouchableOpacity> */}
        <Connect
          isOpen={isConnectOpen}
          onClose={() => setIsConnectOpen(false)}
        />
      </View>
    </>
  );
};

export default ActionBar;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: '#FFFFFF',
    height: 70,
    display: 'flex',
    flexDirection: 'row',
    position: 'relative',
    borderBottomColor: '#EBEBEB',
    borderBottomWidth: 1,
  },
  toggleButton: {
    marginTop: 'auto',
    marginBottom: 'auto',
    height: '100%',
    justifyContent: 'center',
  },
  sendButton: {
    marginTop: 'auto',
    marginBottom: 'auto',
    height: '100%',
    justifyContent: 'center',
    marginLeft: 'auto',
    width: 40,
    marginRight: 8,
  },
  menuIcon: {marginHorizontal: 20, width: 30, height: 30},
  appIconContainer: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    zIndex: -1,
  },
  appIcon: {
    height: 36,
    width: width / 2.6,
    marginTop: 'auto',
    marginBottom: 'auto',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
});
