import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Keyboard,
  TouchableOpacity,
  Image,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const BrokerActionBar = () => {
  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);

  const navigation = useNavigation();

  const openDrawer = () => {
    if (!isKeyboardOpen) {
      navigation.openDrawer();
    }
  };

  const onKeyBoardShow = (e) => {
    setIsKeyboardOpen(true);
  };

  const onKeyBoardHide = () => {
    setIsKeyboardOpen(false);
  };

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', onKeyBoardShow);
    Keyboard.addListener('keyboardDidHide', onKeyBoardHide);
    return () => {
      Keyboard.removeListener('keyboardDidShow', onKeyBoardShow);
      Keyboard.removeListener('keyboardDidHide', onKeyBoardHide);
    };
  }, []);

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.toggleButton} onPress={openDrawer}>
        <Image
          style={styles.menuIcon}
          source={require('../assets/menu-icon.png')}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <View style={styles.appIconContainer}>
        <Image
          style={styles.appIcon}
          resizeMode="contain"
          source={require('../assets/affliate-app-logo-dark.png')}
        />
      </View>
    </View>
  );
};

export default BrokerActionBar;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    backgroundColor: '#08152D',
    height: 70,
    display: 'flex',
    flexDirection: 'row',
    position: 'relative',
  },
  toggleButton: {
    marginTop: 'auto',
    marginBottom: 'auto',
    height: '100%',
    justifyContent: 'center',
  },
  sendButton: {
    marginTop: 'auto',
    marginBottom: 'auto',
    height: '100%',
    justifyContent: 'center',
    marginLeft: 'auto',
    width: 40,
  },
  menuIcon: {marginLeft: 20},
  sendIcon: {
    marginRight: 20,
    flex: 1,
    height: null,
    width: null,
  },
  appIconContainer: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    zIndex: -1,
  },
  appIcon: {
    height: 36,
    // width: 22,
    marginTop: 'auto',
    marginBottom: 'auto',
    marginLeft: 'auto',
    marginRight: 'auto',
  },
});
