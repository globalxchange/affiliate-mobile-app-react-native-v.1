/* eslint-disable react-native/no-inline-styles */
import {useNavigation} from '@react-navigation/native';
import React, {useEffect} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';

const MenuController = ({
  selectedMenu,
  setSelectedMenu,
  selectedBrand,
  setScreenToShow,
}) => {
  const {navigate} = useNavigation();

  // console.log('Select Brand', selectedBrand);

  const MENU_ITEMS = [
    // {
    //   title: 'About',
    //   menu: [
    //     {
    //       title: 'Comp Plan',
    //       subTitle: 'Understand How You Get Paid',
    //       icon: require('../../assets/currency-coin-icon.png'),
    //       disabled: true,
    //     },
    //     {
    //       title: 'Legal',
    //       subTitle: 'Legal Margin That We Have To Show',
    //       icon: require('../../assets/legal-icon.png'),
    //       disabled: true,
    //     },
    //   ],
    // },
    // {
    //   title: 'Earnings',
    //   menu: [
    //     {
    //       title: 'Earnings Forecast',
    //       subTitle: 'Estimate Earnings Based On Current Network',
    //       icon: require('../../assets/earning-forcast-icon.png'),
    //       onPress: () => navigate('BrandEarningForecast', {selectedBrand}),
    //       disabled: selectedBrand?.title === 'AffiliateApp',
    //     },
    //     {
    //       title: 'Simulator',
    //       subTitle: 'Find Out How Much You Can Earn',
    //       icon: require('../../assets/simulator-icon.png'),
    //       disabled: true,
    //     },
    //     {
    //       title: 'Historical Stats',
    //       subTitle: 'Analyze Your Performance With This Brand',
    //       icon: require('../../assets/hostorical-status-icon.png'),
    //       disabled: !(selectedBrand?.title === 'AffiliateApp'),
    //     },
    //   ],
    // },
    {
      title: 'Monetize',
      menu: [
        {
          pos: 0,
          title: selectedBrand.mtToolName,
          subTitle: `Explore The ${selectedBrand.mtToolName} You Use To Earn`,
          icon: require('../../assets/fees-icon.png'),
          desc: `${selectedBrand.mtToolName} refer to the amount of money which is deducted from each interest payment made to users on ${selectedBrand.title}`,
          onPress: () => setScreenToShow('BrandSettings'),
        },
      ],
    },
    // {title: 'Training', disabled: true},
  ];

  useEffect(() => {
    setSelectedMenu(MENU_ITEMS[0]);
    setScreenToShow('BrandSettings');
  }, []);

  return (
    <View style={styles.container}>
      <ScrollView showsHorizontalScrollIndicator={false} horizontal>
        {MENU_ITEMS.map((item) => (
          <TouchableOpacity
            // disabled={item.disabled}
            key={item.title}
            onPress={() => {
              setSelectedMenu(item);
              setScreenToShow('');
            }}
            style={[
              styles.menuItem,
              selectedMenu?.title === item.title && {
                borderBottomColor: ThemeData.APP_MAIN_COLOR,
                borderBottomWidth: 1,
              },
            ]}>
            <Text
              style={[
                styles.menuText,
                selectedMenu?.title === item.title && {
                  color: ThemeData.APP_MAIN_COLOR,
                },
              ]}>
              {item.title}
            </Text>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  );
};

export default MenuController;

const styles = StyleSheet.create({
  container: {
    marginHorizontal: -20,
    borderBottomColor: '#E7E7E7',
    borderBottomWidth: 1,
    marginTop: 10,
    display: 'none',
  },
  menuItem: {
    paddingBottom: 10,
    paddingHorizontal: 30,
  },
  menuText: {
    color: '#E7E7E7',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
});
