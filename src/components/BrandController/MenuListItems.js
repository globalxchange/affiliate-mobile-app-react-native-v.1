import React from 'react';
import {useMemo} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import BrandSettings from '../BrandSettings';
import MenuItem from '../BrandSettings/MenuItem';

const MenuListItems = ({
  selectedMenu,
  selectedBrand,
  screenToShow,
  setScreenToShow,
}) => {
  const menuOrDetail = useMemo(() => {
    switch (screenToShow) {
      case 'BrandSettings':
        console.log('bradning...');
        return (
          <BrandSettings
            selectedBrand={selectedBrand}
            onClose={() => setScreenToShow('')}
          />
        );

      default:
        console.log('elkse...');
        return (
          <FlatList
            showsVerticalScrollIndicator={false}
            data={selectedMenu?.menu}
            keyExtractor={(item) => item.title}
            renderItem={({item}) => (
              <MenuItem
                icon={item.icon}
                title={item.title}
                subTitle={item.subTitle}
                onPress={item.onPress}
                disabled={item.disabled}
              />
            )}
          />
        );
    }
  }, [screenToShow, selectedBrand, selectedMenu.menu, setScreenToShow]);

  return <View style={styles.container}>{menuOrDetail}</View>;
};

export default MenuListItems;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
});
