import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import MenuController from './MenuController';
import MenuListItems from './MenuListItems';

const BrandController = ({selectedBrand}) => {
  const [selectedMenu, setSelectedMenu] = useState('');
  const [screenToShow, setScreenToShow] = useState('');
  return (
    <View style={styles.container}>
      <MenuController
        selectedMenu={selectedMenu}
        setSelectedMenu={setSelectedMenu}
        selectedBrand={selectedBrand}
        setScreenToShow={setScreenToShow}
      />
      <MenuListItems
        selectedMenu={selectedMenu}
        selectedBrand={selectedBrand}
        screenToShow={screenToShow}
        setScreenToShow={setScreenToShow}
      />
    </View>
  );
};

export default BrandController;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
