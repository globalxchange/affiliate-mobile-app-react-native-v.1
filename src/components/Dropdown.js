import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  FlatList,
  Modal,
  Dimensions,
} from 'react-native';
import ThemeData from '../configs/ThemeData';

const {width, height} = Dimensions.get('window');

const Dropdown = ({
  isDisabled,
  activeItem,
  setActiveItem,
  items,
  style,
  onChange,
  closeOnChangeValue,
}) => {
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);

  const onItemSelected = (item) => {
    setActiveItem(item);
    setIsDropdownOpen(false);
  };

  useEffect(() => {
    if (closeOnChangeValue) {
      setIsDropdownOpen(false);
    }
  }, [closeOnChangeValue]);

  useEffect(() => {
    if (onChange) {
      onChange(isDropdownOpen);
    }
  }, [isDropdownOpen, onChange]);

  return (
    <View style={[styles.container, style, isDisabled && styles.disabled]}>
      <TouchableOpacity
        disabled={isDisabled}
        style={styles.dropdownButton}
        onPress={() => setIsDropdownOpen(!isDropdownOpen)}>
        <Text style={styles.activeItem}>{activeItem}</Text>
        <Image
          style={styles.dropdownIcon}
          source={
            isDropdownOpen
              ? require('../assets/dropdown-icon-close-colored.png')
              : require('../assets/dropdown-icon-colored.png')
          }
          resizeMode="contain"
        />
      </TouchableOpacity>

      <Modal
        animationType="fade"
        transparent
        visible={isDropdownOpen}
        hardwareAccelerated
        statusBarTranslucent
        onDismiss={() => setIsDropdownOpen(false)}
        onRequestClose={() => setIsDropdownOpen(false)}>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.overlay}
          onPress={() => setIsDropdownOpen(false)}
          onPressOut={() => {}}>
          <TouchableWithoutFeedback style={{flex: 1}}>
            <View style={styles.modalContainer}>
              <FlatList
                style={styles.dropDownList}
                data={items}
                keyExtractor={(item) => item}
                renderItem={({item}) => (
                  <TouchableOpacity
                    disabled={item.disabled}
                    onPress={() => onItemSelected(item)}
                    style={[
                      styles.dropDownItem,
                      item.disabled && {opacity: 0.5},
                    ]}>
                    <Text style={styles.itemText}>{item}</Text>
                  </TouchableOpacity>
                )}
              />
            </View>
          </TouchableWithoutFeedback>
        </TouchableOpacity>
      </Modal>
    </View>
  );
};

export default Dropdown;

const styles = StyleSheet.create({
  container: {
    width: 115,
    backgroundColor: '#FFFFFF',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
    zIndex: 2,
  },
  disabled: {
    opacity: 0.5,
    elevation: 0,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0,
    shadowRadius: 0,
  },
  dropdownButton: {
    paddingHorizontal: 15,
    paddingVertical: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  activeItem: {
    marginRight: 10,
    fontFamily: 'Montserrat-SemiBold',
    color: ThemeData.TEXT_COLOR,
    fontSize: 9,
  },
  dropdownIcon: {
    height: 12,
    width: 12,
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.2)',
    flex: 1,
    justifyContent: 'center',
    paddingVertical: height * 0.25,
  },
  modalContainer: {
    paddingVertical: 20,
    backgroundColor: 'white',
    borderRadius: 10,
    width: width * 0.75,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  dropDownList: {
    paddingHorizontal: 30,
  },
  dropDownItem: {flexDirection: 'row', paddingVertical: 10},
  countryImage: {height: 24, width: 24},
  itemText: {marginLeft: 15, fontFamily: 'Montserrat'},
});
