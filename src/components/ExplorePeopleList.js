import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import {FlatList} from 'react-native-gesture-handler';
import ThemeData from '../configs/ThemeData';

const ExplorePeopleList = ({title}) => {
  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.header}>{title}</Text>
        <TouchableOpacity style={styles.viewAllBtn}>
          <Text style={styles.viewAllBtnText}>View All</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.listContainer}>
        <FlatList
          contentContainerStyle={{paddingHorizontal: 5}}
          horizontal
          showsHorizontalScrollIndicator={false}
          data={Array(10).fill(1)}
          keyExtractor={(item, index) => index}
          renderItem={({item}) => (
            <View style={styles.listItem}>
              <FastImage
                resizeMode="cover"
                style={styles.itemImage}
                source={{uri: 'https://i.pravatar.cc/300'}}
              />
            </View>
          )}
        />
      </View>
    </View>
  );
};

export default ExplorePeopleList;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
    marginTop: 30,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    paddingBottom: 10,
    marginBottom: 10,
  },
  header: {
    fontFamily: ThemeData.FONT_MEDIUM,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 16,
  },
  viewAllBtn: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 4,
  },
  viewAllBtnText: {
    fontFamily: ThemeData.FONT_MEDIUM,
    fontSize: 11,
    color: ThemeData.APP_MAIN_COLOR,
    paddingHorizontal: 20,
    paddingVertical: 5,
  },
  listContainer: {
    marginTop: 10,
    marginHorizontal: -20,
  },
  listItem: {
    marginHorizontal: 15,
    width: 60,
    height: 60,
    borderRadius: 30,
    overflow: 'hidden',
  },
  itemImage: {
    flex: 1,
    height: null,
    width: null,
  },
});
