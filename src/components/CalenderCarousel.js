import React, {useEffect, useRef, useState} from 'react';
import {Dimensions, StyleSheet, Text, View} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import {animatedStyles, scrollInterpolator} from '../utils/CarouselAnimation';
import LoadingAnimation from './LoadingAnimation';
import Moment from 'moment-timezone';

const {width} = Dimensions.get('window');

const SLIDER_WIDTH = width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.45);
const ITEM_HEIGHT = Math.round((ITEM_WIDTH * 3) / 6);

const DATE_LIMIT = 15;

const CalenderCarousel = ({setSelectedDate}) => {
  const [currentPos, setCurrentPos] = useState(0);

  const [carouselData, setCarouselData] = useState();

  const carouselRef = useRef();

  useEffect(() => {
    const currentDate = Moment.now();

    const prevDateSubArray = [];
    const nextDateSunArray = [];

    for (let i = 1; i <= DATE_LIMIT; i++) {
      const newDate = Moment(currentDate).add(i, 'day');

      nextDateSunArray.push(newDate.unix());
    }

    for (let i = DATE_LIMIT; i >= 1; i--) {
      const newDate = Moment(currentDate).subtract(i, 'day');

      prevDateSubArray.push(newDate.unix());
    }

    setSelectedDate(Moment.unix(currentDate / 1000).format('MM/DD/YYYY'));
    const datesArray = [
      ...prevDateSubArray,
      currentDate / 1000,
      ...nextDateSunArray,
    ];

    setCarouselData(datesArray);
  }, []);

  const onSnapToItem = (index) => {
    setCurrentPos(index);
    if (carouselData) {
      setSelectedDate(Moment.unix(carouselData[index]).format('MM/DD/YYYY'));
    }
  };

  return (
    <View>
      {carouselData ? (
        <Carousel
          ref={carouselRef}
          firstItem={15}
          data={carouselData}
          sliderWidth={SLIDER_WIDTH}
          itemWidth={ITEM_WIDTH}
          containerCustomStyle={styles.carouselContainer}
          inactiveSlideShift={0}
          onSnapToItem={onSnapToItem}
          scrollInterpolator={scrollInterpolator}
          slideInterpolatedStyle={animatedStyles}
          useScrollView={true}
          inactiveSlideOpacity={0.5}
          inactiveSlideScale={0.6}
          renderItem={({item}) => (
            <View style={styles.itemContainer}>
              <Text style={styles.day}>{Moment.unix(item).format('DD')}</Text>
              <Text style={styles.month}>
                {Moment.unix(item).format('MMMM')}
              </Text>
            </View>
          )}
        />
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation height={70} width={70} />
        </View>
      )}
    </View>
  );
};

export default CalenderCarousel;

const styles = StyleSheet.create({
  carouselContainer: {
    marginTop: 10,
  },
  itemContainer: {
    zIndex: 3,
  },
  day: {
    color: '#08152D',
    fontSize: 40,
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
  },
  month: {
    color: '#08152D',
    fontSize: 14,
    fontFamily: 'Montserrat',
    textAlign: 'center',
  },
  loadingContainer: {
    height: ITEM_HEIGHT,
    justifyContent: 'center',
  },
});
