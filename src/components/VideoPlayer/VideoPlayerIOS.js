import React, {useEffect, useRef, useState} from 'react';
import {ActivityIndicator, Dimensions, StyleSheet, View} from 'react-native';
import Animated, {Clock, set, useCode, Value} from 'react-native-reanimated';
import Video from 'react-native-video';
import ReanimatedTimingHelper from '../../utils/ReanimatedTimingHelper';

const {width} = Dimensions.get('window');

const TEST_URL =
  'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4';

const VideoPlayerIOS = ({videoLink, play}) => {
  const [isControlOpen, setIsControlOpen] = useState(false);
  const [isPaused, setIsPaused] = useState(false);
  const [isVideoBuffering, setIsVideoBuffering] = useState(false);

  const controlsOpacity = useRef(new Value(0));
  const playerRef = useRef();

  useEffect(() => {
    if (isControlOpen) {
      setTimeout(() => setIsControlOpen(false), 3000);
    }
  }, [isControlOpen]);

  useCode(
    () =>
      isControlOpen
        ? [
            set(
              controlsOpacity.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ]
        : [
            set(
              controlsOpacity.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ],
    [isControlOpen],
  );

  useEffect(() => {
    setIsPaused(!play);
  }, [play]);

  return (
    <View style={styles.container}>
      <Video
        source={{uri: videoLink}}
        ref={playerRef}
        style={styles.video}
        resizeMode="cover"
        paused={isPaused}
        fullscreen
        controls
        fullscreenAutorotate={true}
        fullscreenOrientation={'landscape'}
        onBuffer={({isBuffering}) => setIsVideoBuffering(isBuffering)}
      />
      {isVideoBuffering && (
        <View style={styles.bufferingContainer}>
          <ActivityIndicator color={'#08152D'} size="large" />
        </View>
      )}
    </View>
  );
};

export default VideoPlayerIOS;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
  },
  video: {
    width,
    height: width * (9 / 16),
    backgroundColor: 'black',
  },
  controlsContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 5,
  },
  controls: {
    flex: 1,
    paddingHorizontal: 20,
    paddingTop: 35,
    paddingBottom: 20,
    justifyContent: 'space-between',
  },
  backButton: {
    height: 23,
    width: 20,
  },
  icon: {
    flex: 1,
    height: null,
    width: null,
  },
  playBackControllerContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  seekButton: {height: 30, width: 30},
  playButton: {marginHorizontal: 25, height: 50, width: 50},
  fullScreenControls: {
    width: 20,
    height: 20,
    marginLeft: 'auto',
  },
  loadingContainer: {
    justifyContent: 'center',
  },
  bufferingContainer: {
    ...StyleSheet.absoluteFill,
    justifyContent: 'center',
  },
});
