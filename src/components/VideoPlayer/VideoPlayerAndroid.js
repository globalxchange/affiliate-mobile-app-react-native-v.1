/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect, useContext} from 'react';
import {
  StyleSheet,
  Dimensions,
  View,
  TouchableOpacity,
  StatusBar,
  TouchableWithoutFeedback,
  ActivityIndicator,
} from 'react-native';
import Video from 'react-native-video';
import Orientation from 'react-native-orientation-locker';
import {FullscreenClose, FullscreenOpen} from './assets/icons';
import {PlayerControls} from './PlayerControls';
import {ProgressBar} from './ProgressBar';
import {AppContext} from '../../contexts/AppContextProvider';

const VideoPlayerAndroid = ({videoLink, play, offsetHeight}) => {
  const {setIsVideoFullScreen} = useContext(AppContext);

  const videoRef = React.createRef();
  const [state, setState] = useState({
    fullscreen: false,
    play: false,
    currentTime: 0,
    duration: 0,
    showControls: true,
  });
  const [isVideoBuffering, setIsVideoBuffering] = useState(false);

  useEffect(() => {
    setIsVideoFullScreen(state.fullscreen);
  }, [state.fullscreen]);

  useEffect(() => {
    Orientation.addOrientationListener(handleOrientation);
    return () => {
      handleOrientation();
      Orientation.removeOrientationListener(handleOrientation);
      Orientation.unlockAllOrientations();
      setIsVideoFullScreen(false);
    };
  }, []);

  useEffect(() => {
    if (state.showControls) {
      setTimeout(() => setState((s) => ({...s, showControls: false})), 4000);
    }
  }, [state.showControls]);

  useEffect(() => {
    setState((s) => ({...s, play}));
  }, [play]);

  const handleOrientation = (orientation) => {
    orientation === 'LANDSCAPE-LEFT' || orientation === 'LANDSCAPE-RIGHT'
      ? (setState((s) => ({...s, fullscreen: true})), StatusBar.setHidden(true))
      : (setState((s) => ({...s, fullscreen: false})),
        StatusBar.setHidden(false));
  };

  const handleFullscreen = () => {
    state.fullscreen
      ? Orientation.unlockAllOrientations()
      : Orientation.lockToLandscapeLeft();
  };

  const handlePlayPause = () => {
    // If playing, pause and show controls immediately.
    if (state.play) {
      setState({...state, play: false, showControls: true});
      return;
    }

    setState({...state, play: true});
    setTimeout(() => setState((s) => ({...s, showControls: false})), 2000);
  };

  const skipBackward = () => {
    videoRef.current.seek(state.currentTime - 15);
    setState({...state, currentTime: state.currentTime - 15});
  };

  const skipForward = () => {
    videoRef.current.seek(state.currentTime + 15);
    setState({...state, currentTime: state.currentTime + 15});
  };

  const onSeek = (data) => {
    videoRef.current.seek(data.seekTime);
    setState({...state, currentTime: data.seekTime});
  };

  const onLoadEnd = (data) => {
    setState((s) => ({
      ...s,
      duration: data.duration,
      currentTime: data.currentTime,
    }));
  };

  const onProgress = (data) => {
    setState((s) => ({
      ...s,
      currentTime: data.currentTime,
    }));
  };

  const onEnd = () => {
    setState({...state, play: false});
    videoRef.current.seek(0);
  };

  const showControls = () => {
    state.showControls
      ? setState({...state, showControls: false})
      : setState({...state, showControls: true});
  };

  return (
    <View
      style={[
        styles.container,
        state.fullscreen && {marginTop: -(offsetHeight || 0), marginBottom: -5},
      ]}>
      <TouchableWithoutFeedback onPress={showControls}>
        <View>
          <Video
            ref={videoRef}
            source={{
              uri: videoLink,
            }}
            style={state.fullscreen ? styles.fullscreenVideo : styles.video}
            controls={false}
            resizeMode={'cover'}
            onLoad={onLoadEnd}
            onProgress={onProgress}
            onEnd={onEnd}
            paused={!state.play}
            // onVideoBuffer={() => setIsVideoBuffering(true)}
            // onBuffer={({isBuffering}) => setIsVideoBuffering(isBuffering)}
          />
          {isVideoBuffering && (
            <View style={styles.bufferingContainer}>
              <ActivityIndicator color={'#08152D'} size="large" />
            </View>
          )}
          {state.showControls && (
            <View style={styles.controlOverlay}>
              <TouchableOpacity
                onPress={handleFullscreen}
                hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                style={styles.fullscreenButton}>
                {state.fullscreen ? <FullscreenClose /> : <FullscreenOpen />}
              </TouchableOpacity>
              <PlayerControls
                onPlay={handlePlayPause}
                onPause={handlePlayPause}
                playing={state.play}
                showPreviousAndNext={false}
                showSkip={true}
                skipBackwards={skipBackward}
                skipForwards={skipForward}
              />
              <ProgressBar
                currentTime={state.currentTime}
                duration={state.duration > 0 ? state.duration : 0}
                onSlideStart={handlePlayPause}
                onSlideComplete={handlePlayPause}
                onSlideCapture={onSeek}
              />
            </View>
          )}
        </View>
      </TouchableWithoutFeedback>
    </View>
  );
};

export default VideoPlayerAndroid;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'black',
    zIndex: 10,
  },
  video: {
    height: Dimensions.get('window').width * (9 / 16),
    width: Dimensions.get('window').width,
    backgroundColor: 'black',
  },
  fullscreenVideo: {
    height: Dimensions.get('window').width,
    width: Dimensions.get('window').height,
    backgroundColor: 'black',
  },
  text: {
    marginTop: 30,
    marginHorizontal: 20,
    fontSize: 15,
    textAlign: 'justify',
  },
  fullscreenButton: {
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'flex-end',
    alignItems: 'center',
    paddingRight: 10,
  },
  controlOverlay: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: '#00000066',
    justifyContent: 'space-between',
  },
  bufferingContainer: {
    ...StyleSheet.absoluteFill,
    justifyContent: 'center',
  },
});
