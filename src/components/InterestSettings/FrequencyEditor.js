import axios from 'axios';
import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import {APP_CODE, GX_API_ENDPOINT} from '../../configs';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import LoadingAnimation from '../LoadingAnimation';
import ProfileSettingsItem from '../ProfileSettingsItem';

const FrequencyEditor = ({interestData, isLiquid, onBack, onClose}) => {
  const [isLoading, setIsLoading] = useState(false);

  const changeFrequency = async (frequency) => {
    setIsLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    const updateData = {
      ...(interestData || {
        payoutDestination_data: {app_code: APP_CODE},
      }),
      payoutFrequency: frequency || 'daily',
      payoutDestination: 'vault',
    };

    let postData = {
      token,
      email,
    };

    if (isLiquid) {
      postData = {...postData, liquidData: updateData};
    } else {
      postData = {...postData, icedData: updateData};
    }

    // console.log('Post Data: ', postData);

    axios
      .post(
        `${GX_API_ENDPOINT}/coin/iced/set/user/interest/payout/destination`,
        postData,
      )
      .then(({data}) => {
        // console.log('update data', data);

        if (data.status) {
          onClose();
        } else {
          WToast.show({data: data.message, position: WToast.position.TOP});
        }
      })
      .catch((error) => {
        console.log('Error on updating destination app');
        WToast.show({data: 'Network Error', position: WToast.position.TOP});
      })
      .finally(() => setIsLoading(false));
  };

  if (isLoading) {
    return (
      <View style={{flex: 1, justifyContent: 'center'}}>
        <LoadingAnimation />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <ProfileSettingsItem
        title="Daily"
        subText="Add Earnings Daily To Wallet"
        onPress={() => changeFrequency('daily')}
      />
      <ProfileSettingsItem
        title="Weekly"
        subText="Add Earnings Weekly To Wallet"
        onPress={() => changeFrequency('weekly')}
      />
      <ProfileSettingsItem
        title="Monthly"
        subText="Add Earnings Monthly To Wallet"
        onPress={() => changeFrequency('monthly')}
      />
    </View>
  );
};

export default FrequencyEditor;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
