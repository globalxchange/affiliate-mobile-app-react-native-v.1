import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ProfileSettingsItem from '../ProfileSettingsItem';
import DestinationAppSelector from './DestinationAppSelector';

const EditDestination = ({onBack, interestData, isLiquid, onClose}) => {
  const [selectedOption, setSelectedOption] = useState();

  if (selectedOption === 'GxApp') {
    return (
      <DestinationAppSelector
        interestData={interestData}
        isLiquid={isLiquid}
        onClose={onClose}
      />
    );
  }

  return (
    <View style={styles.container}>
      <ProfileSettingsItem
        title="Another GX App"
        subText="Update First, Last, & Your Nick Name"
        onPress={() => setSelectedOption('GxApp')}
      />
      <ProfileSettingsItem
        title="External Wallet"
        subText="Update First, Last, & Your Nick Name"
        onPress={() => setSelectedOption('External')}
        isDisabled
      />
    </View>
  );
};

export default EditDestination;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
