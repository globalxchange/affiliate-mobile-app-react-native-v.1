import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {GX_API_ENDPOINT} from '../../configs';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import LoadingAnimation from '../LoadingAnimation';

const {height} = Dimensions.get('window');

const AffiliateSelector = ({setAffiliateInput, onNext, userName}) => {
  const [searchInput, setSearchInput] = useState('');
  const [usersList, setUsersList] = useState('');
  const [filteredList, setFilteredList] = useState('');

  const {bottom} = useSafeAreaInsets();

  useEffect(() => {
    (async () => {
      const affId = await AsyncStorageHelper.getAffId();

      Axios.post(`${GX_API_ENDPOINT}/get_broker_names`, {affiliate_id: affId})
        .then((resp) => {
          const {data} = resp;
          // console.log('Users List', data);
          setUsersList(data || []);
        })
        .catch((error) => {
          console.log('Error on getting AppList', error);
        });
    })();
  }, []);

  useEffect(() => {
    const searchQuery = searchInput.trim().toLowerCase();

    if (usersList) {
      const list = usersList.filter(
        (item) =>
          item?.email?.toLowerCase()?.includes(searchQuery) ||
          item?.name?.toLowerCase()?.includes(searchQuery),
      );

      setFilteredList(list);
    }
  }, [searchInput, usersList]);

  const onItemSelected = (item) => {
    // console.log('Item', item);
    setAffiliateInput(item.affiliate_id);
    onNext(item.affiliate_id);
  };

  const onUserClick = async () => {
    const affId = await AsyncStorageHelper.getAffId();

    setAffiliateInput(affId);
    onNext(affId);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>
        Who Do You Want To Place {userName || 'user'} Under?
      </Text>
      <View style={styles.searchContainer}>
        <View style={styles.separator} />
        <TextInput
          style={styles.searchInput}
          onChangeText={(text) => setSearchInput(text)}
          value={searchInput}
          placeholder={'Search User'}
          placeholderTextColor={'#878788'}
        />
        <Image
          style={styles.searchIcon}
          source={require('../../assets/search-icon.png')}
          resizeMode="contain"
        />
      </View>
      {usersList ? (
        <View style={styles.listContainer}>
          <FlatList
            data={filteredList}
            keyExtractor={(item) => item._id}
            showsVerticalScrollIndicator={false}
            renderItem={({item}) => (
              <TouchableOpacity onPress={() => onItemSelected(item)}>
                <View style={styles.item}>
                  <Image
                    style={styles.cryptoIcon}
                    resizeMode="contain"
                    source={require('../../assets/user-icon.png')}
                  />
                  <View style={styles.nameContainer}>
                    <Text style={styles.countryName}>{item.name}</Text>
                    <Text style={styles.userEmail}>{item.email}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />
        </View>
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      )}
      <TouchableOpacity
        onPress={onUserClick}
        style={[styles.directButton, {marginBottom: (-bottom || 0) - 35}]}>
        <Text style={[styles.directText, {paddingBottom: bottom || 20}]}>
          Directly Under Me
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default AffiliateSelector;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    marginTop: 20,
    fontFamily: 'Montserrat',
    color: '#999C9A',
    textTransform: 'capitalize',
  },
  searchContainer: {
    flexDirection: 'row',
    marginTop: 40,
    height: 45,
    paddingHorizontal: 15,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.15,
    shadowRadius: 2,
    backgroundColor: 'white',
    borderRadius: 2,
    elevation: 12,
    alignItems: 'center',
  },
  countryIcon: {
    height: 22,
    width: 22,
    marginTop: 'auto',
    marginBottom: 'auto',
    marginRight: 15,
  },
  separator: {
    backgroundColor: '#CACACA',
    width: 1,
    marginVertical: 8,
  },
  searchInput: {
    flexGrow: 1,
    width: 0,
    marginTop: 'auto',
    marginBottom: 'auto',
    paddingHorizontal: 15,
    fontFamily: 'Montserrat',
    fontWeight: 'normal',
    color: 'black',
  },
  searchIcon: {
    height: 15,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  loadingContainer: {
    marginTop: 80,
    marginBottom: 30,
  },
  listContainer: {
    flex: 1,
    marginTop: 20,
  },
  item: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 25,
    backgroundColor: 'white',
    marginBottom: 10,
    alignItems: 'center',
  },
  cryptoIcon: {
    width: 25,
    height: 25,
    borderRadius: 10,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  nameContainer: {
    flexGrow: 1,
    width: 0,
    marginLeft: 10,
  },
  countryName: {
    color: '#001D41',
    fontSize: 16,
    marginBottom: 2,
    fontFamily: 'Montserrat-Bold',
  },
  userEmail: {
    color: '#001D41',
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  priceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  methodsNo: {
    color: '#001D41',
    opacity: 0.5,
    marginBottom: 2,
    textAlign: 'right',
    fontFamily: 'Roboto-Bold',
    marginRight: 10,
    fontSize: 12,
  },
  directButton: {
    backgroundColor: '#EBEBEB',
    marginHorizontal: -35,
    marginBottom: -30,
  },
  directText: {
    fontFamily: 'Montserrat-Bold',
    color: '#001D41',
    textAlign: 'center',
    paddingTop: 20,
  },
});
