import React from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import ThemeData from '../../configs/ThemeData';
import {usernameRegex} from '../../utils';

const BlockcheckMail = ({
  blockcheckId,
  setBlockcheckId,
  onNext,
  isKeyboardOpen,
}) => {
  const onPressNext = () => {
    const username = blockcheckId.toLowerCase().trim();

    let isValid = !(username.length < 6 || !usernameRegex.test(username));

    if (isValid) {
      onNext();
    } else {
      WToast.show({
        data: 'Please Input A Valid Username',
        position: WToast.position.TOP,
      });
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Create A BlockCheck Address</Text>
      <View style={styles.inputContainer}>
        <TextInput
          placeholder="EX. shorupan"
          placeholderTextColor={'#878788'}
          style={styles.input}
          value={blockcheckId}
          onChangeText={(text) => setBlockcheckId(text)}
        />
        <Text style={styles.inputPostfix}>@blockcheck.io</Text>
      </View>
      {isKeyboardOpen || (
        <TouchableOpacity onPress={onPressNext} style={styles.button}>
          <Text style={styles.buttonText}>Next Step</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

export default BlockcheckMail;

const styles = StyleSheet.create({
  container: {},
  header: {
    color: '#999C9A',
    fontFamily: ThemeData.FONT_MEDIUM,
    fontSize: 15,
    marginBottom: 25,
    marginTop: 60,
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 60,
  },
  input: {
    flex: 1,
    borderBottomColor: ThemeData.APP_MAIN_COLOR,
    borderBottomWidth: 1,
    paddingHorizontal: 15,
    paddingVertical: 10,
    fontFamily: ThemeData.FONT_MEDIUM,
  },
  inputPostfix: {
    marginLeft: 5,
    fontFamily: ThemeData.FONT_MEDIUM,
    color: ThemeData.APP_MAIN_COLOR,
  },
  button: {
    backgroundColor: '#08152D',
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
    height: 45,
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
  },
});
