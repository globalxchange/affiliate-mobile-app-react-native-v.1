import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {GX_API_ENDPOINT} from '../../configs';
import LoadingAnimation from '../LoadingAnimation';

const {height} = Dimensions.get('window');

const AppSelector = ({selectedApp, setSelectedApp, onNext}) => {
  const [searchInput, setSearchInput] = useState('');
  const [appList, setAppList] = useState('');
  const [filteredList, setFilteredList] = useState('');

  useEffect(() => {
    Axios.get(`${GX_API_ENDPOINT}/gxb/apps/get`)
      .then((resp) => {
        const {data} = resp;
        // console.log('AppsList', data);
        setAppList(data.apps || []);
      })
      .catch((error) => {
        console.log('Error on getting AppList', error);
      });
  }, []);

  useEffect(() => {
    const searchQuery = searchInput.trim().toLowerCase();

    if (appList) {
      const list = appList.filter(
        (item) =>
          item?.app_name?.toLowerCase()?.includes(searchQuery) ||
          item?.app_code?.toLowerCase()?.includes(searchQuery),
      );

      setFilteredList(list);
    }
  }, [searchInput, appList]);

  const onItemSelected = (item) => {
    setSelectedApp(item);
    onNext();
  };

  return (
    <View style={styles.container}>
      <View style={styles.searchContainer}>
        <View style={styles.separator} />
        <TextInput
          style={styles.searchInput}
          onChangeText={(text) => setSearchInput(text)}
          value={searchInput}
          placeholder={'Enter The App'}
          placeholderTextColor={'#878788'}
        />
        <Image
          style={styles.searchIcon}
          source={require('../../assets/search-icon.png')}
          resizeMode="contain"
        />
      </View>
      {appList ? (
        <View style={styles.listContainer}>
          <FlatList
            data={filteredList}
            keyExtractor={(item) => item._id}
            showsVerticalScrollIndicator={false}
            renderItem={({item}) => (
              <TouchableOpacity onPress={() => onItemSelected(item)}>
                <View style={styles.item}>
                  <Image
                    style={styles.cryptoIcon}
                    resizeMode="contain"
                    source={{
                      uri: item.app_icon,
                    }}
                  />
                  <View style={styles.nameContainer}>
                    <Text style={styles.countryName}>{item.app_name}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />
        </View>
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default AppSelector;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  searchContainer: {
    flexDirection: 'row',
    marginTop: 40,
    height: 45,
    paddingHorizontal: 15,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.15,
    shadowRadius: 2,
    backgroundColor: 'white',
    borderRadius: 2,
    elevation: 12,
    alignItems: 'center',
  },
  countryIcon: {
    height: 22,
    width: 22,
    marginTop: 'auto',
    marginBottom: 'auto',
    marginRight: 15,
  },
  separator: {
    backgroundColor: '#CACACA',
    width: 1,
    marginVertical: 8,
  },
  searchInput: {
    flexGrow: 1,
    width: 0,
    marginTop: 'auto',
    marginBottom: 'auto',
    paddingHorizontal: 15,
    fontFamily: 'Montserrat',
    fontWeight: 'normal',
    color: 'black',
  },
  searchIcon: {
    height: 15,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  loadingContainer: {
    marginTop: 80,
    marginBottom: 30,
  },
  listContainer: {
    flex: 1,
    marginTop: 20,
  },
  item: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 25,
    backgroundColor: 'white',
    marginBottom: 10,
    alignItems: 'center',
  },
  cryptoIcon: {
    width: 25,
    height: 25,
    borderRadius: 10,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  nameContainer: {
    flexGrow: 1,
    width: 0,
    marginLeft: 10,
  },
  countryName: {
    color: '#001D41',
    fontSize: 14,
    marginBottom: 2,
    fontFamily: 'Montserrat-Bold',
  },
  priceContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  methodsNo: {
    color: '#001D41',
    opacity: 0.5,
    marginBottom: 2,
    textAlign: 'right',
    fontFamily: 'Roboto-Bold',
    marginRight: 10,
    fontSize: 12,
  },
});
