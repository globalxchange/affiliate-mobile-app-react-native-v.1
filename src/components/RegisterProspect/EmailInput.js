/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import {emailValidator} from '../../utils';

const EmailInput = ({isKeyboardOpen, email, setEmail, onNext}) => {
  const [isValid, setIsValid] = useState(false);

  useEffect(() => {
    const emailTrimmed = email.toLowerCase().trim();
    setIsValid(emailValidator(emailTrimmed));
  }, [email]);

  const onPressNext = () => {
    if (isValid) {
      onNext();
    } else {
      WToast.show({
        data: 'Please Input A Valid Email',
        position: WToast.position.TOP,
      });
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.inputView}>
        <Text style={styles.label}>Step 1: Enter The Email</Text>
        <View style={styles.inputContainer}>
          <Image
            source={require('../../assets/email-icon.png')}
            style={styles.img}
            resizeMode="contain"
          />
          <TextInput
            style={styles.input}
            keyboardType="email-address"
            value={email}
            onChangeText={(text) => setEmail(text)}
            placeholderTextColor="#878788"
            placeholder="Email"
            autoFocus
          />
          {isValid ? (
            <TouchableOpacity style={styles.nextButton} onPress={onPressNext}>
              <Image
                style={styles.nextImage}
                source={require('../../assets/next-forward-icon-white.png')}
                resizeMode="contain"
              />
            </TouchableOpacity>
          ) : (
            <View
              style={[
                styles.validatorSign,
                {backgroundColor: isValid ? '#08152D' : '#D80027'},
              ]}
            />
          )}
        </View>
      </View>
      {isKeyboardOpen || (
        <TouchableOpacity onPress={onPressNext} style={styles.button}>
          <Text style={styles.buttonText}>Next Step</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

export default EmailInput;

const styles = StyleSheet.create({
  container: {},
  inputView: {paddingVertical: 60},
  label: {
    color: '#999C9A',
  },
  inputContainer: {
    borderColor: '#EBEBEB',
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
    marginTop: 20,
    height: 60,
  },
  img: {
    height: 15,
    width: 15,
  },
  input: {
    paddingHorizontal: 10,
    flexGrow: 1,
    width: 0,
    fontFamily: 'Montserrat',
    color: 'black',
  },
  validatorSign: {
    width: 10,
    height: 10,
    borderRadius: 5,
  },
  button: {
    backgroundColor: '#08152D',
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
    height: 45,
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
  },
  nextButton: {
    backgroundColor: '#08152D',
    marginRight: -15,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  nextImage: {
    width: 20,
    height: 20,
  },
});
