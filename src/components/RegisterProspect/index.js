import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  BackHandler,
  Image,
  Keyboard,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {WToast} from 'react-native-smart-tip';
import {APP_CODE, GX_AUTH_URL, MAILSURP_KEY} from '../../configs';
import EmailInput from './EmailInput';
import Success from './Success';
import UserNameInput from './UserNameInput';
import LoadingAnimation from '../LoadingAnimation';
import AppSelector from './AppSelector';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import AffiliateSelector from './AffiliateSelector';
import BottomSheetLayout from '../../layouts/BottomSheetLayout';
import EmailType from './EmailType';
import BlockcheckMail from './BlockcheckMail';
import {MailSlurp} from 'mailslurp-client';
import {useNavigation} from '@react-navigation/native';

const RegisterProspect = () => {
  const {bottom} = useSafeAreaInsets();

  const {goBack} = useNavigation();

  const [isOpen, setIsOpen] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);
  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);
  const [activeView, setActiveView] = useState();
  const [email, setEmail] = useState('');
  const [userName, setUserName] = useState('');
  const [selectedApp, setSelectedApp] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [affiliateInput, setAffiliateInput] = useState('');
  const [blockcheckId, setBlockcheckId] = useState('');

  useEffect(() => {
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
      onKeyBoardShow,
    );
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
      onKeyBoardHide,
    );
    BackHandler.addEventListener('hardwareBackPress', backHandler);
    return () => {
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
        onKeyBoardShow,
      );
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
        onKeyBoardHide,
      );
      BackHandler.removeEventListener('hardwareBackPress', backHandler);
    };
  }, []);

  useEffect(() => {
    if (!isOpen) {
      resetState();
    }
  }, [isOpen]);

  const onKeyBoardShow = (e) => {
    setKeyboardHeight(
      e.endCoordinates.height - (Platform.OS === 'ios' ? bottom : 0),
    );
    setIsKeyboardOpen(true);
  };

  const onKeyBoardHide = () => {
    setKeyboardHeight(0);
    setIsKeyboardOpen(false);
  };

  const backHandler = () => {
    switch (activeView) {
      case 'AppSelector':
        goBack();
        break;
      case 'EmailType':
        setActiveView('AppSelector');
        break;
      case 'Blockcheck':
        setActiveView('EmailType');
        break;
      case 'Email':
        setActiveView('EmailType');
        break;
      case 'Username':
        setActiveView('Email');
        break;
      case 'Affiliate':
        setActiveView(userName ? 'Email' : 'Blockcheck');
        break;
      case 'Competed':
        goBack();
        break;
      default:
        goBack();
    }
    return true;
  };

  const resetState = () => {
    setActiveView();
    setEmail('');
    setUserName('');
    setIsLoading(false);
    setBlockcheckId('');
    setAffiliateInput('');
    setSelectedApp('');
  };

  const registerAdmin = async (affIdPassed) => {
    const emailId = blockcheckId
      ? `${blockcheckId.toLowerCase().trim()}@blockcheck.io`
      : email.toLowerCase().trim();
    const username = blockcheckId
      ? blockcheckId.toLowerCase().trim()
      : userName.toLowerCase().trim();
    const affId = await AsyncStorageHelper.getAffId();

    setIsLoading(true);

    // console.log('affIdPassed', affIdPassed);

    const postData = {
      username,
      email: emailId,
      ref_affiliate: affIdPassed || affiliateInput || affId,
      app_code: selectedApp?.app_code || APP_CODE,
      client_app: selectedApp?.app_code || APP_CODE,
    };

    // console.log('PostData', postData);

    Axios.post(`${GX_AUTH_URL}/gx/user/admin/signup`, postData)
      .then((resp) => {
        const {data} = resp;

        // console.log('Resp', data);

        if (data.status) {
          setActiveView('Competed');
        } else {
          WToast.show({
            data: data.message || 'Error registering admins',
            position: WToast.position.TOP,
          });
        }
      })
      .catch((error) => {})
      .finally(() => setIsLoading(false));
  };

  const createMailSlurp = async () => {
    const mailslurp = new MailSlurp({
      apiKey: MAILSURP_KEY,
    });

    const inbox = await mailslurp.inboxController.createInbox(
      '',
      `${blockcheckId}@blockcheck.io`,
      new Date(),
      false,
      `${blockcheckId}`,
      [APP_CODE],
    );
    setEmail(inbox.emailAddress);
  };

  const renderComponent = () => {
    switch (activeView) {
      case 'AppSelector':
        return (
          <AppSelector
            selectedApp={selectedApp}
            setSelectedApp={setSelectedApp}
            onNext={() => setActiveView('EmailType')}
          />
        );

      case 'EmailType':
        return (
          <EmailType
            onBlockCheck={() => setActiveView('Blockcheck')}
            onEmailClick={() => setActiveView('Email')}
          />
        );

      case 'Blockcheck':
        return (
          <BlockcheckMail
            isKeyboardOpen={isKeyboardOpen}
            blockcheckId={blockcheckId}
            setBlockcheckId={setBlockcheckId}
            onNext={() => {
              createMailSlurp();
              setActiveView('Affiliate');
            }}
          />
        );

      case 'Email':
        return (
          <EmailInput
            isKeyboardOpen={isKeyboardOpen}
            email={email}
            setEmail={setEmail}
            onNext={() => setActiveView('Username')}
          />
        );

      case 'Username':
        return (
          <UserNameInput
            isKeyboardOpen={isKeyboardOpen}
            setUserName={setUserName}
            userName={userName}
            onNext={() => setActiveView('Affiliate')}
          />
        );

      case 'Affiliate':
        return (
          <AffiliateSelector
            setAffiliateInput={setAffiliateInput}
            userName={userName}
            onNext={(affIdPassed) => registerAdmin(affIdPassed)}
          />
        );

      case 'Competed':
        return (
          <Success
            username={userName}
            onClose={() => setIsOpen(false)}
            selectedApp={selectedApp}
          />
        );

      default:
        return (
          <AppSelector
            selectedApp={selectedApp}
            setSelectedApp={setSelectedApp}
            onNext={() => setActiveView('EmailType')}
          />
        );
    }
  };

  return (
    <View style={styles.container}>
      {isLoading ? (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      ) : (
        <View style={styles.fragmentContainer}>
          <View style={styles.headerContainer}>
            <TouchableOpacity onPress={backHandler} style={styles.backButton}>
              <Image
                source={require('../../assets/back-arrow.png')}
                resizeMode="contain"
                style={styles.backIcon}
              />
            </TouchableOpacity>
            <Image
              source={require('../../assets/broker-sync.png')}
              style={styles.headerImage}
              resizeMode="contain"
            />
          </View>
          {renderComponent()}
        </View>
      )}
    </View>
  );
};

export default RegisterProspect;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    flexDirection: 'row',
  },
  backButton: {
    position: 'absolute',
    zIndex: 9,
    width: 40,
    height: 30,
  },
  backIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  headerImage: {
    height: 55,
    width: 200,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  fragmentContainer: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 30,
    paddingHorizontal: 35,
    paddingBottom: 35,
  },

  loadingContainer: {
    justifyContent: 'center',
    paddingVertical: 80,
    backgroundColor: 'white',
  },
});
