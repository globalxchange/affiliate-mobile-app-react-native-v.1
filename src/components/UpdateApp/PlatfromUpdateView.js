import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {APP_CODE, GX_API_ENDPOINT} from '../../configs';
import ProfileSettingsItem from '../ProfileSettingsItem';
import * as WebBrowser from 'expo-web-browser';
import {showToastMessage} from '../../utils';
import LoadingAnimation from '../LoadingAnimation';

const PlatfromUpdateView = ({isAndroid}) => {
  const [updateHistory, setUpdateHistory] = useState('');
  const [showHistory, setShowHistory] = useState(false);

  useEffect(() => {
    axios
      .get(`${GX_API_ENDPOINT}/gxb/apps/mobile/app/links/logs/get`, {
        params: {app_code: APP_CODE},
      })
      .then(({data}) => {
        // console.log('App Logs', data);

        const logs = data.logs || [];

        setUpdateHistory(logs);
      })
      .catch((error) => {});
  }, []);

  const onOpenLink = async () => {
    const linkObj = updateHistory[0];
    const link = isAndroid ? linkObj?.android_app_link : linkObj?.ios_app_link;

    if (link) {
      WebBrowser.openBrowserAsync(link);
    } else {
      showToastMessage('No Update Link Found');
    }
  };
  const onViewHistory = () => {
    if (updateHistory.length <= 0) {
      return showToastMessage('No Update Links Found');
    }
    setShowHistory(true);
  };

  return (
    <View style={styles.container}>
      {updateHistory ? (
        showHistory ? (
          <></>
        ) : (
          <>
            <ProfileSettingsItem
              title={'Open Link'}
              subText={'Direct To Download Page'}
              onPress={onOpenLink}
            />
            <ProfileSettingsItem
              title={'Version Hisory'}
              subText={'See All Updates For Android'}
              onPress={onViewHistory}
            />
          </>
        )
      ) : (
        <LoadingAnimation />
      )}
    </View>
  );
};

export default PlatfromUpdateView;

const styles = StyleSheet.create({
  container: {},
});
