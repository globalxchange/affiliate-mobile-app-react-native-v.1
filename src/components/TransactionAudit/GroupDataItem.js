import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';

const GroupDataItem = ({
  header,
  valueKey,
  value,
  disabled,
  style,
  onMoreClick,
}) => {
  return (
    <View style={[styles.container, disabled && {opacity: 0.5}, style]}>
      <TouchableOpacity disabled={!onMoreClick} onPress={onMoreClick}>
        <View style={styles.row}>
          <Text style={styles.headerItem}>{header}</Text>
          <Text style={styles.headerItem}>More Info</Text>
        </View>
        <View style={styles.valueContainer}>
          <Text style={styles.value}>{valueKey}</Text>
          <Text style={styles.value}>{value}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default GroupDataItem;

const styles = StyleSheet.create({
  container: {
    marginBottom: 30,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerItem: {
    fontFamily: 'Montserrat',
    color: '#08152D',
    fontSize: 12,
  },
  valueContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderColor: '#EBEBEB',
    borderWidth: 0.8,
    borderRadius: 6,
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginTop: 10,
  },
  value: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#788995',
    fontSize: 14,
  },
});
