import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Image,
} from 'react-native';
import BasicData from './BasicData';
import FromDetails from './FromDetails';
import BottomSheetLayout from '../../layouts/BottomSheetLayout';

const {height} = Dimensions.get('window');

const TransactionAudit = ({selectedAuditTxn, isAuditOpen, setIsAuditOpen}) => {
  const [isExpanded, setIsExpanded] = useState(false);
  const [activeView, setActiveView] = useState('');
  const [headerText, setHeaderText] = useState('');

  const onBack = () => {
    const viewIndex = viewKeys.findIndex((item) => item === activeView);

    if (viewIndex <= 0) {
      setIsAuditOpen(false);
    } else {
      setActiveView(viewKeys[viewIndex - 1]);
    }
  };

  const renderFragment = () => {
    switch (activeView) {
      case 'BASIC':
        return (
          <BasicData
            setHeaderText={setHeaderText}
            data={selectedAuditTxn}
            setActiveView={setActiveView}
          />
        );
      case 'FROM_DETAILS':
        return (
          <FromDetails setHeaderText={setHeaderText} data={selectedAuditTxn} />
        );
      default:
        return (
          <BasicData
            setHeaderText={setHeaderText}
            data={selectedAuditTxn}
            setActiveView={setActiveView}
          />
        );
    }
  };

  useEffect(() => {
    setIsExpanded(false);
  }, [activeView]);

  return (
    <BottomSheetLayout
      isOpen={isAuditOpen}
      onClose={() => setIsAuditOpen(false)}
      style={{height: isExpanded ? height - 100 : height / 1.4}}>
      <View style={styles.headerContainer}>
        <TouchableOpacity style={styles.headerButton} onPress={onBack}>
          <Image
            style={styles.headerButtonIcon}
            resizeMode="contain"
            source={require('../../assets/back-icon-white.png')}
          />
        </TouchableOpacity>
        <Text style={styles.headerText}>{headerText}</Text>
        <TouchableOpacity
          style={styles.headerButton}
          onPress={() => setIsExpanded(!isExpanded)}>
          <Image
            style={styles.headerButtonIcon}
            resizeMode="contain"
            source={require('../../assets/expand-icon-white.png')}
          />
        </TouchableOpacity>
      </View>
      {renderFragment()}
      <View style={styles.actionContainer}>
        <TouchableOpacity style={styles.actionButton}>
          <Text style={styles.actionButtonText}>Share</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.actionButtonOutlined}>
          <Text style={styles.actionButtonTextOutlined}>Download</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.actionButton}>
          <Text style={styles.actionButtonText}>Dispute</Text>
        </TouchableOpacity>
      </View>
    </BottomSheetLayout>
  );
};

export default TransactionAudit;

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: '#08152D',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 30,
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
  },
  headerText: {
    flex: 1,
    textAlign: 'center',
    color: 'white',
    fontFamily: 'Montserrat-Bold',
    fontSize: 17,
  },
  headerButton: {
    height: 30,
    width: 30,
    padding: 6,
  },
  headerButtonIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  actionContainer: {
    flexDirection: 'row',
    marginBottom: -1,
  },
  actionButton: {
    flex: 1,
    paddingVertical: 12,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#08152D',
  },
  actionButtonText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
  },
  actionButtonOutlined: {
    flex: 1,
    paddingVertical: 12,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderColor: '#08152D',
    borderWidth: 1,
  },
  actionButtonTextOutlined: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
  },
});

const viewKeys = ['BASIC', 'FROM_DETAILS'];
