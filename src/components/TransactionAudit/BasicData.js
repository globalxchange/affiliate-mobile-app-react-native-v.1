import React, {useEffect} from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  TouchableWithoutFeedback,
} from 'react-native';
import DataTextItem from './DataTextItem';
import GroupDataItem from './GroupDataItem';
import {formatterHelper} from '../../utils';

const BasicData = ({data, setActiveView, setHeaderText}) => {
  useEffect(() => {
    setHeaderText('Transaction Summary');
  }, []);

  // console.log('data', data);

  return (
    <View style={styles.container}>
      <ScrollView
        style={styles.scrollView}
        showsVerticalScrollIndicator={false}>
        <TouchableWithoutFeedback>
          <View style={{paddingVertical: 30}}>
            <DataTextItem
              header="Transaction ID"
              value={data.identifier}
              actionIcon={require('../../assets/block-check-colored.png')}
            />
            <DataTextItem
              textStyle={{
                opacity: 0.5,
                fontFamily: 'Montserrat',
              }}
              header="Nick Name"
              value="You Haven't Added This Yet"
              actionIcon={require('../../assets/edit-icon.png')}
            />
            <GroupDataItem
              header="Transaction Type"
              valueKey={`External ${data.deposit ? 'Deposit' : 'Withdraw'}`}
              value={data.status}
            />
            <GroupDataItem
              header="From Currency"
              valueKey={data.from || data.coin}
              value={formatterHelper(data.amount, data.from || data.coin)}
              onMoreClick={() => setActiveView('FROM_DETAILS')}
            />
            <GroupDataItem
              header="To Currency"
              valueKey={data.coin}
              value={formatterHelper(data.amount, data.from || data.coin)}
            />
            <GroupDataItem
              disabled
              header="Earning Meter"
              valueKey={data.coin}
              value={formatterHelper(data.amount, data.from || data.coin)}
            />
            <GroupDataItem
              style={{marginBottom: 0}}
              header="Fee Meter"
              valueKey={data.coin}
              value={formatterHelper(data.amount, data.from || data.coin)}
            />
          </View>
        </TouchableWithoutFeedback>
      </ScrollView>
    </View>
  );
};

export default BasicData;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 30,
    paddingVertical: 10,
  },
  scrollView: {
    flex: 1,
  },
});
