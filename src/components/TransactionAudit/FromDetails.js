import React, {useEffect} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import {formatterHelper} from '../../utils';
import ValueDetails from './ValueDetails';

const FromDetails = ({data, setHeaderText}) => {
  useEffect(() => {
    setHeaderText('From Currency');
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.dataItem}>
        <View style={styles.row}>
          <Text style={styles.label}>From Currency</Text>
          <Text style={styles.label}>Amount</Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.mainValue}>{data.from || data.coin}</Text>
          <Text style={styles.mainValue}>
            {formatterHelper(data.amount, data.from || data.coin)}
          </Text>
        </View>
      </View>
      <View style={styles.dataItem}>
        <View style={styles.row}>
          <Text style={styles.label}>Banker</Text>
        </View>
        <View style={styles.row}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={styles.mainValue}>Global X Change</Text>
            <Image
              style={{width: 16, height: 16, marginLeft: 10}}
              resizeMode="contain"
              source={require('../../assets/link-icon.png')}
            />
          </View>
          <TouchableOpacity style={styles.contactButton}>
            <Text style={styles.contactButtonText}>Contact</Text>
          </TouchableOpacity>
        </View>
      </View>
      <ValueDetails
        header={`Per ${data.coin}`}
        valueNow="$1000"
        valueThen="$1001"
      />
      <ValueDetails
        header="Your Transaction"
        valueNow="$1000"
        valueThen="$1001"
        style={{marginBottom: 0}}
      />
    </View>
  );
};

export default FromDetails;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 30,
    paddingVertical: 40,
    justifyContent: 'center',
  },
  dataItem: {
    marginBottom: 30,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 5,
    alignItems: 'center',
  },
  label: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
  mainValue: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 16,
  },
  contactButton: {
    backgroundColor: '#08152D',
    paddingVertical: 4,
    paddingHorizontal: 20,
    borderRadius: 6,
  },
  contactButtonText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 10,
  },
});
