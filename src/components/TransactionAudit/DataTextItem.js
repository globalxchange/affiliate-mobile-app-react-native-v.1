import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';

const DataTextItem = ({header, value, actionIcon, textStyle}) => {
  return (
    <View style={styles.textItemContainer}>
      <Text style={styles.textItemHeader}>{header}</Text>
      <View style={styles.textItemDataContainer}>
        <Text style={[styles.textItemValue, textStyle]}>{value}</Text>
        <TouchableOpacity style={styles.textItemActionButton}>
          <Image
            style={styles.textItemActionIcon}
            resizeMode="contain"
            source={require('../../assets/copy-icon.png')}
          />
        </TouchableOpacity>
        <TouchableOpacity style={styles.textItemActionButton}>
          <Image
            style={styles.textItemActionIcon}
            resizeMode="contain"
            source={actionIcon}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default DataTextItem;

const styles = StyleSheet.create({
  textItemContainer: {
    marginBottom: 30,
  },
  textItemHeader: {
    fontFamily: 'Montserrat',
    color: '#08152D',
    fontSize: 12,
  },
  textItemDataContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textItemValue: {
    flex: 1,
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
    fontSize: 14,
  },
  textItemActionButton: {
    width: 26,
    height: 26,
    padding: 5,
    marginHorizontal: 3,
  },
  textItemActionIcon: {
    flex: 1,
    height: null,
    width: null,
  },
});
