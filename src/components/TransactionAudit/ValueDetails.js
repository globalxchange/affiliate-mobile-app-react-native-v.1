import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

const ValueDetails = ({header, valueNow, valueThen, style}) => {
  return (
    <View style={[styles.container, style]}>
      <Text style={styles.header}>{header}</Text>
      <View style={styles.valuesContainer}>
        <View style={styles.valueItemContainer}>
          <Text style={styles.value}>{valueThen}</Text>
          <Text style={styles.valueLabel}>Time Of Transaction</Text>
        </View>
        <View style={styles.valueItemContainer}>
          <Text style={[styles.value, {textAlign: 'right'}]}>{valueNow}</Text>
          <Text style={[styles.valueLabel, {textAlign: 'right'}]}>
            Price Right Now
          </Text>
        </View>
      </View>
    </View>
  );
};

export default ValueDetails;

const styles = StyleSheet.create({
  container: {
    marginBottom: 30,
  },
  header: {
    fontFamily: 'Montserrat',
    color: '#08152D',
    fontSize: 12,
  },
  valuesContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderColor: '#EBEBEB',
    borderWidth: 0.8,
    borderRadius: 6,
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginTop: 10,
  },
  valueItemContainer: {},
  value: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#788995',
    fontSize: 14,
  },
  valueLabel: {
    fontFamily: 'Montserrat',
    color: '#788995',
    fontSize: 9,
  },
});
