import React, {useState} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import CountryPicker, {FlagButton} from 'react-native-country-picker-modal';
import {TextInput} from 'react-native-gesture-handler';

const PhoneInputView = ({placeHolder, value, onChange}) => {
  const [countryCode, setCountryCode] = useState('FR');
  const [selectedCountry, setSelectedCountry] = useState(null);
  const [isCountryModalOpen, setIsCountryModalOpen] = useState(false);

  const onSelect = (country) => {
    setCountryCode(country.cca2);
    setSelectedCountry(country);
  };

  return (
    <View style={styles.container}>
      <CountryPicker
        countryCode={countryCode}
        visible={isCountryModalOpen}
        withFlag
        withFilter
        withCountryNameButton
        withAlphaFilter
        withCallingCode
        withEmoji
        onSelect={onSelect}
        renderFlagButton={() => {
          return null;
        }}
        onClose={() => setIsCountryModalOpen(false)}
      />
      <View style={styles.selectorContainer}>
        <FlagButton
          onOpen={() => setIsCountryModalOpen(!isCountryModalOpen)}
          withEmoji={true}
          countryCode={countryCode}
          withCallingCodeButton={true}
          containerButtonStyle={styles.selector}
        />
        <View style={styles.phoneInput}>
          <View style={styles.inputContainer}>
            <Text style={styles.placeHolder}>{placeHolder}</Text>
            <View>
              <TextInput
                style={styles.input}
                value={value}
                onChange={onChange}
                keyboardType="number-pad"
                placeholderTextColor="#878788"
              />
            </View>
          </View>
          <View style={styles.iconContainer}>
            <Image
              style={styles.icon}
              source={require('../assets/phone-numbers-call.png')}
              resizeMode="contain"
            />
          </View>
        </View>
      </View>
    </View>
  );
};

export default PhoneInputView;

const styles = StyleSheet.create({
  container: {
    borderColor: '#E9E8E8',
    borderWidth: 1,
    borderRadius: 12,
    paddingHorizontal: 15,
    marginBottom: 20,
  },
  selectorContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  selector: {
    height: 50,
    minWidth: 60,
    maxWidth: 90,
    justifyContent: 'center',
    fontFamily: 'Montserrat-SemiBold',
    marginRight: 10,
  },
  phoneInput: {
    flexDirection: 'row',
    flex: 1,
    borderLeftColor: '#E9E8E8',
    borderLeftWidth: 1,
    paddingTop: 6,
    paddingLeft: 10,
    alignItems: 'center',
  },
  inputContainer: {
    flex: 1,
  },
  placeHolder: {
    color: '#858585',
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
  input: {
    color: '#000000',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 14,
    paddingRight: 10,
  },
  iconContainer: {
    borderColor: '#F6F6F6',
    borderWidth: 1,
    width: 40,
    height: 40,
    padding: 10,
    borderRadius: 20,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  icon: {
    flex: 1,
    height: null,
    width: null,
  },
});
