import React from 'react';
import {StyleSheet} from 'react-native';
import {Line, Rect} from 'react-native-svg';

const MARGIN = 4;

const Candle = ({index, candle, caliber, scaleY, scaleBody}) => {
  const {high, low, open, close} = candle;

  const x = caliber * index + 0.5 * caliber;

  const color = open > close ? '#FF2D55' : '#08152D';

  return (
    <>
      <Line
        x1={x}
        x2={x}
        y1={scaleY(high)}
        y2={scaleY(low)}
        stroke={color}
        strokeWidth={1}
      />
      <Rect
        x={caliber * index + MARGIN}
        y={scaleY(Math.max(open, close))}
        width={caliber - MARGIN * 2}
        height={scaleBody(Math.max(open, close) - Math.min(open, close))}
        fill={color}
        rx={4}
      />
    </>
  );
};

export default Candle;

const styles = StyleSheet.create({});
