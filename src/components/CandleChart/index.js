import React, {useState, useEffect} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {Svg} from 'react-native-svg';
import {scaleLinear} from 'd3-scale';
import Candle from './Candle';
import FrequencyController from './FrequencyController';
import ScaleView from './ScaleView';
import Axios from 'axios';
import {FIN_HUB_KEY} from '../../configs';
import Moment from 'moment-timezone';
import LoadingAnimation from '../LoadingAnimation';

const nofCandles = 22;

const CandleChart = ({activeCrypto}) => {
  const [selectedFreq, setSelectedFreq] = useState('Minutes');

  const [chartHeight, setChartHeight] = useState(0);
  const [chartWidth, setChartWidth] = useState(0);
  const [candleData, setCandleData] = useState();
  // const [candles, setCandles] = useState([]);

  const caliber = candleData ? chartWidth / (candleData.length || 1) : 1;

  const values = candleData
    ? candleData.map((candle) => [candle.low, candle.high]).flat()
    : [];
  const domain = [Math.min(...values), Math.max(...values)];

  const scaleY = scaleLinear().domain(domain).range([chartHeight, 0]);
  const scaleBody = scaleLinear()
    .domain([0, Math.max(...domain) - Math.min(...domain)])
    .range([0, chartHeight]);

  const getCandleData = () => {
    setCandleData();
    let resolution = '1';
    const currentMoment = Math.trunc(Date.now() / 1000);
    let fromTime = Moment.unix(currentMoment)
      .subtract(nofCandles, 'minute')
      .unix();
    switch (selectedFreq) {
      case 'Minutes':
        resolution = '1';
        fromTime = Moment.unix(currentMoment)
          .subtract(nofCandles, 'minute')
          .unix();
        break;
      case '5 Mins':
        resolution = '5';
        fromTime = Moment.unix(currentMoment)
          .subtract(nofCandles * 5, 'minute')
          .unix();
        break;
      case 'Hours':
        resolution = '60';
        fromTime = Moment.unix(currentMoment)
          .subtract(nofCandles, 'hours')
          .unix();
        break;
      case 'Days':
        resolution = 'D';
        fromTime = Moment.unix(currentMoment)
          .subtract(nofCandles, 'days')
          .unix();
        break;
      case 'Weeks':
        resolution = 'W';
        fromTime = Moment.unix(currentMoment)
          .subtract(nofCandles * 7, 'days')
          .unix();
        break;
    }

    const params = {
      token: FIN_HUB_KEY,
      symbol: `BITFINEX:${activeCrypto.coinSymbol}USD`,
      resolution: resolution,
      from: fromTime,
      to: currentMoment,
    };

    const apiURL = 'https://finnhub.io/api/v1/crypto/candle';

    Axios.get(apiURL, {
      params,
    })
      .then((resp) => {
        const apiData = resp.data;

        if (apiData.s === 'ok') {
          const parsedCandleData = [];

          const range =
            (apiData.t.length || []) < nofCandles
              ? apiData.t.length
              : nofCandles;

          for (let i = 0; i < range; i++) {
            const {t, o, c, h, l} = apiData;

            const candleMoment = Moment.unix(t[i]);
            const date = candleMoment.format('YYYY-MM-DD HH:mm');
            const day = candleMoment.date();

            const parsedCandle = {
              date,
              day,
              open: o[i],
              high: h[i],
              low: l[i],
              close: c[i],
            };
            parsedCandleData.push(parsedCandle);
          }
          setCandleData(parsedCandleData);
        } else {
          setCandleData([]);
        }
      })
      .catch((error) => console.log('Error on getting chart data', error));
  };

  useEffect(() => {
    getCandleData();
  }, [activeCrypto, selectedFreq]);

  useEffect(() => {
    setSelectedFreq('Minutes');
  }, [activeCrypto]);

  return (
    <View style={styles.container}>
      {candleData ? (
        <>
          <FrequencyController {...{selectedFreq, setSelectedFreq}} />
          <View style={styles.chartView}>
            {candleData.length > 0 ? (
              <>
                <View
                  style={styles.canvasContainer}
                  onLayout={({nativeEvent}) => {
                    const {width, height} = nativeEvent.layout;
                    setChartWidth(width);
                    setChartHeight(height);
                  }}>
                  <Svg
                    width={chartWidth}
                    height={chartHeight}
                    style={styles.svgCanvas}>
                    {candleData.map((candle, index) => (
                      <Candle
                        key={index}
                        {...{index, candle, caliber, domain, scaleY, scaleBody}}
                      />
                    ))}
                  </Svg>
                </View>
                <ScaleView domain={domain} />
              </>
            ) : (
              <View style={styles.notChartContainer}>
                <Text style={styles.noChartHeader}>
                  {'Sorry..!!\nChart Data Not Available Now.'}
                </Text>
              </View>
            )}
          </View>
        </>
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default CandleChart;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  chartView: {
    flexDirection: 'row',
    marginTop: 20,
    flexGrow: 1,
    height: 0,
  },
  canvasContainer: {
    height: '100%',
    flexGrow: 1,
    width: 0,
    marginRight: 10,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  notChartContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  noChartHeader: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 15,
  },
});
