import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {usdValueFormatter} from '../../utils';

const ScaleView = ({domain}) => {
  const [low, high] = domain;

  const deference = (high - low) / 6;
  const scales = [];
  for (let i = 1; i <= 6; i++) {
    scales.push(deference * i + low);
  }
  scales.reverse();

  return (
    <View style={styles.container}>
      {scales.map((item) => (
        <Text key={item} style={styles.scaleItem}>
          {usdValueFormatter.format(item)}
        </Text>
      ))}
    </View>
  );
};

export default ScaleView;

const styles = StyleSheet.create({
  container: {
    height: '100%',
    justifyContent: 'space-between',
    marginLeft: 10,
  },
  scaleItem: {
    opacity: 0.4,
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 9,
    color: '#08152D',
  },
});
