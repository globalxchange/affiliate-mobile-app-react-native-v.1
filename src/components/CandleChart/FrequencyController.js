import React from 'react';
import {View, StyleSheet, TouchableOpacity, Text} from 'react-native';

const controller = ['Minutes', '5 Mins', 'Hours', 'Days', 'Weeks'];

const FrequencyController = ({selectedFreq, setSelectedFreq}) => {
  return (
    <View style={styles.container}>
      {controller.map((item) => (
        <TouchableOpacity
          style={styles.freqItem}
          key={item}
          onPress={() => setSelectedFreq(item)}>
          <Text
            style={[
              styles.freqText,
              selectedFreq === item && styles.freqTextActive,
            ]}>
            {item}
          </Text>
        </TouchableOpacity>
      ))}
    </View>
  );
};

export default FrequencyController;

const styles = StyleSheet.create({
  container: {flexDirection: 'row'},
  freqItem: {
    flexGrow: 1,
    width: 0,
  },
  freqText: {
    textAlign: 'center',
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 10,
  },
  freqTextActive: {fontFamily: 'Montserrat-Bold'},
});
