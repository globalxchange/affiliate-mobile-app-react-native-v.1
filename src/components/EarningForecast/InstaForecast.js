import {useRoute} from '@react-navigation/native';
import React, {useContext, useState} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import {AppContext} from '../../contexts/AppContextProvider';
import MenuItem from '../BrandSettings/MenuItem';

const InstaForecast = () => {
  const {params} = useRoute();
  const {selectedBrand} = params;

  const {cryptoTableData} = useContext(AppContext);

  const [isOTCBotsOpen, setIsOTCBotsOpen] = useState(false);
  const [otcData, setOtcData] = useState('');

  let fiatAssets = 0;
  let cryptoAssets = 0;

  if (cryptoTableData) {
    // console.log('cryptoTableData', cryptoTableData);

    fiatAssets = cryptoTableData.filter((item) => item.asset_type === 'Fiat')
      .length;

    cryptoAssets = cryptoTableData.length - fiatAssets;
  }

  const lists = [
    {
      icon: require('../../assets/fiat-crypto-icon.png'),
      title: 'Fiat To Crypto',
      numbers: `${fiatAssets * cryptoAssets} Pairs`,
      fromAssets: 'Fiat',
      toAsset: 'Crypto',
      subTitle:
        'Examine Your Current Exchange Fee Structure For Fiat To Crypto',
    },
    {
      icon: require('../../assets/crypto-fiat-icon.png'),
      title: 'Crypto To Fiat',
      numbers: `${fiatAssets * cryptoAssets} Pairs`,
      fromAssets: 'Crypto',
      toAsset: 'Fiat',
      subTitle:
        'Examine Your Current Exchange Fee Structure For Crypto To Fiat',
    },
    {
      icon: require('../../assets/fiat-fiat-icon.png'),
      title: 'Fiat To Fiat',
      numbers: `${fiatAssets * fiatAssets} Pairs`,
      fromAssets: 'Fiat',
      toAsset: 'Fiat',
      subTitle: 'Examine Your Current Exchange Fee Structure For Fiat To Fiat',
    },
    {
      icon: require('../../assets/crypto-crypto-icon.png'),
      title: 'Crypto To Crypto',
      numbers: `${cryptoAssets * cryptoAssets} Pairs`,
      fromAssets: 'Crypto',
      toAsset: 'Crypto',
      subTitle:
        'Examine Your Current Exchange Fee Structure For Crypto To Crypto',
    },
  ];

  const color = selectedBrand?.color || '#08152D';

  return (
    <View style={styles.container}>
      <FlatList
        data={lists}
        keyExtractor={(item) => item.title}
        renderItem={({item}) => (
          <MenuItem
            icon={item.icon}
            title={item.title}
            subTitle={item.subTitle}
            onPress={() => {
              setOtcData(item);
              setIsOTCBotsOpen(true);
            }}
          />
        )}
      />
    </View>
  );
};

export default InstaForecast;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
