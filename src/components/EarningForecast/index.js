import {useNavigation, useRoute} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import ForecastView from './ForecastView';
import InstaForecast from './InstaForecast';
import TypeSelector from './TypeSelector';

const EarningForecast = ({setShowSwitcher, selectedFrequency}) => {
  const {goBack} = useNavigation();
  const {params} = useRoute();

  const [selectedCategory, setSelectedCategory] = useState();

  useEffect(() => {
    if (selectedCategory) {
      setShowSwitcher(true);
    } else {
      setShowSwitcher(false);
    }
  }, [selectedCategory]);

  const {selectedBrand} = params;

  let activeView;

  switch (selectedCategory) {
    case 'Bonds':
      activeView = (
        <ForecastView selectedFrequency={selectedFrequency} isBond />
      );
      break;
    case 'Liquid':
      activeView = <ForecastView selectedFrequency={selectedFrequency} />;
      break;
    default:
      activeView =
        selectedBrand.title === 'Vault' ? (
          <InstaForecast />
        ) : (
          <TypeSelector
            setSelectedCategory={setSelectedCategory}
            selectedBrand={selectedBrand}
          />
        );
  }

  return (
    <View style={styles.container}>
      <View style={styles.breadCrumbsContainer}>
        <Text onPress={goBack} style={styles.prevBreadCrumb}>
          Earnings
        </Text>
        <Text style={styles.breadCrumbArrow}>{' -> '}</Text>
        <Text
          onPress={() => setSelectedCategory()}
          style={[
            selectedCategory ? styles.prevBreadCrumb : styles.currentBreadCrumb,
          ]}>
          Earnings Forecast
        </Text>
        {selectedCategory ? (
          <>
            <Text style={styles.breadCrumbArrow}>{' -> '}</Text>
            <Text style={styles.currentBreadCrumb}>{selectedCategory}</Text>
          </>
        ) : null}
      </View>
      <View style={styles.fragmentContainer}>{activeView}</View>
    </View>
  );
};

export default EarningForecast;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  breadCrumbsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 30,
  },
  prevBreadCrumb: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    color: '#464B4E',
  },
  breadCrumbArrow: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
  },
  currentBreadCrumb: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    textDecorationLine: 'underline',
    fontSize: 12,
    color: '#464B4E',
  },
  fragmentContainer: {
    marginTop: 30,
    flex: 1,
  },
});
