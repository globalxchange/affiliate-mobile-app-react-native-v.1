import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import LoadingAnimation from '../LoadingAnimation';
import ValueItem from './ValueItem';

const {width} = Dimensions.get('window');

const ForecastView = ({isBond, selectedFrequency}) => {
  const [forecastDataLiquid, setForecastDataLiquid] = useState();
  const [forecastDataBond, setForecastDataBond] = useState();
  const [currentData, setCurrentData] = useState();

  useEffect(() => {
    (async () => {
      const email = __DEV__
        ? 'lanceg1@gmail.com'
        : await AsyncStorageHelper.getLoginEmail();

      Axios.get(
        `${GX_API_ENDPOINT}/coin/iced/get/today/interest/commission/earnings`,
        {params: {email}},
      )
        .then(({data}) => {
          const liquidData = data.liquid;
          if (liquidData) {
            const coinData = [];
            Object.entries(liquidData?.coinTotalCommission).forEach(
              ([key, value]) => {
                coinData.push({
                  coin: key,
                  ...value,
                });
              },
            );
            setForecastDataLiquid({...liquidData, coinData});
          }

          const bondData = data.iced;

          if (bondData) {
            const coinData = [];
            Object.entries(bondData?.coinTotalCommission).forEach(
              ([key, value]) => {
                coinData.push({
                  coin: key,
                  ...value,
                });
              },
            );

            setForecastDataBond({...bondData, coinData});
          }
        })
        .catch((error) => {
          console.log('Error on getting forecast Data', error);
        });
    })();
  }, []);

  useEffect(() => {
    if (forecastDataBond && forecastDataLiquid) {
      if (isBond) {
        setCurrentData(forecastDataBond);
      } else {
        setCurrentData(forecastDataLiquid);
      }
    }
  }, [isBond, forecastDataBond, forecastDataLiquid]);

  return (
    <View style={styles.container}>
      {currentData ? (
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.viewGroup}>
            <Text style={styles.groupTitle}>
              Total Fees From Liquid Earnings In Your Network
            </Text>
            <View style={styles.valueContainer}>
              <ValueItem
                value={
                  (selectedFrequency?.value || 1) *
                  currentData.totalCommissionUSD
                }
                currency="USD"
                usdValue={
                  (selectedFrequency?.value || 1) *
                  currentData.totalCommissionUSD
                }
                style={styles.coinValue}
              />
              <View style={styles.divider} />
              <Text style={styles.coinSymbol}>USD</Text>
            </View>
          </View>
          <View style={styles.viewGroup}>
            <Text style={styles.groupTitle}>Total Fees Organized By Asset</Text>
            <View style={styles.listContainer}>
              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={currentData?.coinData}
                keyExtractor={(item) => item.coin}
                renderItem={({item}) => (
                  <View style={styles.listItem}>
                    <ValueItem
                      value={(selectedFrequency?.value || 1) * item.value}
                      usdValue={
                        (selectedFrequency?.value || 1) * item.value_usd
                      }
                      currency={item.coin}
                      style={styles.listValue}
                    />
                    <Text style={styles.listCoin}>{item.coin}</Text>
                  </View>
                )}
              />
            </View>
          </View>

          <View style={styles.viewGroup}>
            <Text style={styles.groupTitle}>
              Your Revenue Based On Those Fees
            </Text>
            <View style={styles.valueContainer}>
              <ValueItem
                value={
                  (selectedFrequency?.value || 1) *
                  currentData.userTotalCommissionUSD
                }
                currency="USD"
                usdValue={
                  (selectedFrequency?.value || 1) *
                  currentData.userTotalCommissionUSD
                }
                style={styles.coinValue}
              />
              <View style={styles.divider} />
              <Text style={styles.coinSymbol}>USD</Text>
            </View>
          </View>
          <View style={styles.viewGroup}>
            <Text style={styles.groupTitle}>
              Your Revenue Organized By Asset
            </Text>
            <View style={styles.listContainer}>
              <FlatList
                horizontal
                showsHorizontalScrollIndicator={false}
                data={currentData?.coinData}
                keyExtractor={(item) => item.coin}
                renderItem={({item}) => (
                  <View style={styles.listItem}>
                    <ValueItem
                      value={(selectedFrequency?.value || 1) * item.userValue}
                      usdValue={
                        (selectedFrequency?.value || 1) * item.userValue_usd
                      }
                      currency={item.coin}
                      style={styles.listValue}
                    />
                    <Text style={styles.listCoin}>{item.coin}</Text>
                  </View>
                )}
              />
            </View>
          </View>
        </ScrollView>
      ) : (
        <View style={{flex: 1, justifyContent: 'center'}}>
          <LoadingAnimation />
        </View>
      )}
    </View>
  );
};

export default ForecastView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // marginTop: 20,
    marginBottom: -20,
    marginHorizontal: -30,
  },
  viewGroup: {
    marginTop: 10,
    marginBottom: 15,
    marginHorizontal: 30,
  },
  groupTitle: {
    color: '#464B4E',
    fontFamily: ThemeData.FONT_MEDIUM,
    fontSize: 13,
  },
  valueContainer: {
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 20,
    height: 50,
    alignItems: 'center',
    marginTop: 10,
  },
  coinSymbol: {
    color: '#464B4E',
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 11,
  },
  coinValue: {
    color: '#464B4E',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 13,
    flex: 1,
  },
  divider: {
    height: '100%',
    width: 1,
    backgroundColor: ThemeData.BORDER_COLOR,
    marginHorizontal: 20,
  },
  listContainer: {
    marginTop: 10,
    marginRight: -30,
  },
  listItem: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    width: width * 0.35,
    height: width * 0.24,
    marginRight: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  listValue: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 18,
    paddingVertical: 5,
  },
  listCoin: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 10,
  },
});
