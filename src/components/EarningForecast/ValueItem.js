/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import PopupLayout from '../../layouts/PopupLayout';
import {cryptoList, formatterHelper, usdValueFormatter} from '../../utils';

const ValueItem = ({value, currency, usdValue, style}) => {
  const [isValueExpanded, setIsValueExpanded] = useState(false);
  const [isPopupOpen, setIsPopupOpen] = useState(false);

  useEffect(() => {
    const isCrypto = cryptoList.includes(currency);

    if (isCrypto) {
      setIsValueExpanded(value !== 0 && value < 0.000009);
    } else {
      setIsValueExpanded(value !== 0 && value < 0.009);
    }
  }, [value, currency]);

  return (
    <>
      <Text
        onPress={() => setIsPopupOpen(true)}
        style={
          isValueExpanded
            ? {
                fontSize: 13,
                fontFamily: ThemeData.FONT_SEMI_BOLD,
                color: '#464B4E',
                paddingVertical: 5,
              }
            : style
        }>
        {isValueExpanded
          ? 'Click To Expand'
          : currency === 'USD'
          ? usdValueFormatter.format(value)
          : formatterHelper(value, currency)}
      </Text>
      <PopupLayout
        headerImage={require('../../assets/assets-io-white-icon.png')}
        headerBackground="#464B4E"
        isOpen={isPopupOpen}
        noScrollView
        autoHeight
        onClose={() => setIsPopupOpen(false)}>
        <View style={styles.popupContainer}>
          <View style={styles.itemContainer}>
            <Text style={styles.itemTitle}>
              Your Revenue Based On Those Fees
            </Text>
            <View style={styles.valueContainer}>
              <Text numberOfLines={1} style={styles.coinValue}>
                {value}
              </Text>
              <View style={styles.divider} />
              <Text style={styles.coinSymbol}>{currency}</Text>
            </View>
          </View>
          <View style={styles.itemContainer}>
            <Text style={styles.itemTitle}>
              Your Revenue Based On Those Fees In USD
            </Text>
            <View style={styles.valueContainer}>
              <Text numberOfLines={1} style={styles.coinValue}>
                ${usdValue}
              </Text>
              <View style={styles.divider} />
              <Text style={styles.coinSymbol}>USD</Text>
            </View>
          </View>
        </View>
      </PopupLayout>
    </>
  );
};

export default ValueItem;

const styles = StyleSheet.create({
  popupContainer: {},
  itemContainer: {
    marginBottom: 30,
  },
  itemTitle: {
    color: '#464B4E',
    fontFamily: ThemeData.FONT_MEDIUM,
    fontSize: 13,
  },
  valueContainer: {
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 20,
    height: 50,
    alignItems: 'center',
    marginTop: 10,
  },
  coinSymbol: {
    color: '#464B4E',
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 11,
  },
  coinValue: {
    color: '#464B4E',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 13,
    flex: 1,
  },
  divider: {
    height: '100%',
    width: 1,
    backgroundColor: ThemeData.BORDER_COLOR,
    marginHorizontal: 20,
  },
});
