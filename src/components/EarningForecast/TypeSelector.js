import React from 'react';
import {StyleSheet, View} from 'react-native';
import MenuItem from '../BrandSettings/MenuItem';

const TypeSelector = ({setSelectedCategory, selectedBrand}) => {
  return (
    <View style={styles.container}>
      <MenuItem
        onPress={() => setSelectedCategory('Liquid')}
        icon={require('../../assets/liquid-earnings-icon.png')}
        subTitle={`Forecast Your ${
          selectedBrand.title === 'InstaCrypto' ? 'MT' : 'Fees'
        } For Liquid Earnings`}
        title="Liquid Earnings"
      />
      <MenuItem
        onPress={() => setSelectedCategory('Bonds')}
        icon={require('../../assets/bonds-icon.png')}
        subTitle={`Forecast Your ${
          selectedBrand.title === 'InstaCrypto' ? 'MT' : 'Fees'
        } For Bonds`}
        title="Bonds"
      />
    </View>
  );
};

export default TypeSelector;

const styles = StyleSheet.create({
  container: {
    // marginTop: 40,
  },
});
