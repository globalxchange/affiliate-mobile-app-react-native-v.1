import React, {useState, useContext, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';
import {AppContext} from '../contexts/AppContextProvider';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../configs';
import LoadingAnimation from './LoadingAnimation';
import {getUriImage, urlValidatorRegex} from '../utils';
import PaymentMethodDetails from './PaymentMethodDetails';
import FastImage from 'react-native-fast-image';

const MethodsList = () => {
  const {
    homeSearchInput,
    filterActiveCountry,
    setTotalPaymentMethod,
    pathData,
  } = useContext(AppContext);

  const [methodsList, setMethodsList] = useState();
  const [unfilteredList, setUnfilteredList] = useState();
  const [filteredList, setFilteredList] = useState();
  const [searchCountryText, setSearchCountryText] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const [isDetailsOpen, setIsDetailsOpen] = useState(false);
  const [detailsData, setDetailsData] = useState();

  useEffect(() => {
    if (filterActiveCountry) {
      if (filterActiveCountry.value === 'Worldwide') {
        setSearchCountryText('Globally');
      } else {
        setSearchCountryText(`In ${filterActiveCountry.value}`);
      }
    }
  }, [filterActiveCountry]);

  useEffect(() => {
    getMethods();
  }, []);

  useEffect(() => {
    if (pathData && methodsList && filterActiveCountry) {
      if (filterActiveCountry.value === 'Worldwide') {
        setUnfilteredList(methodsList);
      } else {
        let parsedMethods = [];
        methodsList.map((item) => {
          pathData.map((pathItem) => {
            if (
              pathItem.country === filterActiveCountry.value &&
              (item.code === pathItem.depositMethod ||
                item.code === pathItem.paymentMethod) &&
              !parsedMethods.includes(item)
            ) {
              return parsedMethods.push(item);
            }
          });
        });
        setUnfilteredList(parsedMethods);
      }
    }
  }, [filterActiveCountry, pathData, methodsList]);

  useEffect(() => {
    if (unfilteredList) {
      const list = unfilteredList.filter(
        (item) =>
          item.name.toLowerCase().includes(homeSearchInput.toLowerCase()) ||
          item.code.toLowerCase().includes(homeSearchInput.toLowerCase()),
      );
      setFilteredList(list);
    }
  }, [homeSearchInput, unfilteredList]);

  const getMethods = () => {
    setIsLoading(true);

    Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/payment/methods/get`, {
      params: {},
    })
      .then((resp) => {
        const methodsData = resp.data;
        const methods = methodsData.status ? methodsData.methods : [];

        let parsedMethods = [];

        methods.forEach((item) => {
          let noOfBankers = 0;
          pathData.map((pathItem) => {
            if (
              pathItem.depositMethod === item.code ||
              pathItem.paymentMethod === item.code
            ) {
              noOfBankers += 1;
            }
          });
          parsedMethods.push({...item, noOfBankers});
        });

        setMethodsList(parsedMethods);
        setTotalPaymentMethod(parsedMethods);
        setIsLoading(false);
      })
      .catch((error) => console.log('Error getting methods list', error));
  };

  const openMethodDetailsHandler = (item) => {
    setDetailsData(item);
    setIsDetailsOpen(true);
  };

  return (
    <View style={styles.container}>
      {unfilteredList ? (
        <FlatList
          style={styles.scrollView}
          data={filteredList}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item) => item._id}
          refreshControl={
            <RefreshControl refreshing={isLoading} onRefresh={getMethods} />
          }
          renderItem={({item}) => (
            <TouchableOpacity
              style={styles.item}
              onPress={() => openMethodDetailsHandler(item)}>
              <FastImage
                style={styles.cryptoIcon}
                resizeMode="contain"
                source={
                  urlValidatorRegex.test(item.icon)
                    ? {uri: getUriImage(item.icon)}
                    : require('../assets/money-pig-icon.png')
                }
              />
              <View style={styles.nameContainer}>
                <Text style={styles.cryptoName}>{item.name}</Text>
                <Text style={styles.noOfBankers}>
                  {item.noOfBankers} Bankers
                </Text>
              </View>
            </TouchableOpacity>
          )}
          ListEmptyComponent={
            <View style={styles.emptyContainer}>
              <Text style={styles.emptyText}>
                No Supported Methods Found {searchCountryText}
              </Text>
            </View>
          }
        />
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
          <Text style={styles.loadingText}>Fetching Data...</Text>
        </View>
      )}
      <PaymentMethodDetails
        isBottomSheetOpen={isDetailsOpen}
        setIsBottomSheetOpen={setIsDetailsOpen}
        data={detailsData}
      />
    </View>
  );
};

export default MethodsList;

const styles = StyleSheet.create({
  container: {backgroundColor: '#f7f7f7', flex: 1},
  scrollView: {flex: 1, paddingHorizontal: 10, paddingTop: 10},
  item: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    paddingVertical: 25,
    backgroundColor: 'white',
    marginBottom: 10,
    alignItems: 'center',
  },
  cryptoIcon: {
    width: 25,
    height: 25,
    marginTop: 'auto',
    marginBottom: 'auto',
  },
  nameContainer: {
    flexGrow: 1,
    width: 0,
    marginLeft: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  cryptoName: {
    color: '#001D41',
    fontSize: 16,
    marginBottom: 2,
    fontFamily: 'Montserrat-Bold',
  },
  noOfBankers: {
    color: '#001D41',
    opacity: 0.5,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  emptyContainer: {
    marginTop: 20,
  },
  emptyText: {
    fontFamily: 'Montserrat',
    color: '#08152D',
    textAlign: 'center',
    fontSize: 16,
    paddingHorizontal: 30,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  loadingText: {marginTop: 10, fontFamily: 'Montserrat'},
});
