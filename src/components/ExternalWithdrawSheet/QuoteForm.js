import {useNavigation} from '@react-navigation/core';
import React, {useContext, useEffect, useState} from 'react';
import {
  StyleSheet,
  Switch,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import WithdrawalContext from '../../contexts/WithdrawalContext';
import {formatterHelper, roundHelper, usdValueFormatter} from '../../utils';

const QuoteForm = ({onClose}) => {
  const {navigate} = useNavigation();

  const {activeWallet} = useContext(WithdrawalContext);
  const {walletCoinData} = useContext(AppContext);

  const [isCryptoFocused, setIsCryptoFocused] = useState(false);
  const [fiatInput, setFiatInput] = useState('');
  const [cryptoInput, setCryptoInput] = useState('');
  const [activeCoinBalance, setActiveCoinBalance] = useState('');
  const [isPriceLockEnabled, setIsPriceLockEnabled] = useState(false);

  useEffect(() => {
    if (walletCoinData && activeWallet) {
      const coinData = walletCoinData.find(
        (item) => item.coinSymbol === activeWallet.coinSymbol,
      );
      setActiveCoinBalance(coinData);
    }
  }, [activeWallet, walletCoinData]);

  const onFiatInputChange = (input) => {
    if (input) {
      const updatedFiatValue =
        parseFloat(input || 0) / (activeCoinBalance?.price?.USD || 1);
      setFiatInput(input);
      setCryptoInput(
        roundHelper(
          isNaN(parseFloat(updatedFiatValue)) ? '' : updatedFiatValue,
          activeCoinBalance?.coinSymbol,
        ),
      );
    } else {
      setCryptoInput('');
      setFiatInput('');
    }
  };

  const onCryptoInputChange = (input) => {
    console.log({input, activeCoinBalance: activeCoinBalance?.price?.USD});

    if (input) {
      const updatedFiatValue =
        parseFloat(input || 0) * (activeCoinBalance?.price?.USD || 0);

      console.log({updatedFiatValue});
      setCryptoInput(input);
      setFiatInput(
        roundHelper(
          isNaN(parseFloat(updatedFiatValue)) ? '' : updatedFiatValue,
          'USD',
        ),
      );
    } else {
      setCryptoInput('');
      setFiatInput('');
    }
  };

  const onProceedConfirm = () => {
    onClose();
    navigate('ExternalWithdraw');
  };

  return (
    <View style={styles.container}>
      <Text style={styles.balanceText}>
        {formatterHelper(
          activeCoinBalance?.coinValue || 0,
          activeCoinBalance?.coinSymbol,
        )}
        <Text style={styles.balanceTextCoin}>
          {' '}
          {activeCoinBalance.coinSymbol}
        </Text>
      </Text>
      <Text style={styles.usdBalance}>
        {usdValueFormatter.format(activeCoinBalance?.coinValueUSD)}
      </Text>
      <View style={styles.percentageContainer}>
        <TouchableOpacity style={[styles.percentageItem, {marginLeft: 0}]}>
          <Text style={styles.percentageText}>Custom</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.percentageItem}>
          <Text style={styles.percentageText}>100%</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.inputForm}>
        <View style={styles.inputContainer}>
          <TextInput
            style={[styles.input, isCryptoFocused && styles.focusedText]}
            placeholder={formatterHelper('0.00', activeWallet.coinSymbol)}
            onChangeText={(text) => onCryptoInputChange(text)}
            keyboardType="numeric"
            returnKeyType="done"
            value={cryptoInput}
            onFocus={() => setIsCryptoFocused(true)}
            placeholderTextColor="#878788"
          />
          <Text
            style={[styles.cryptoName, isCryptoFocused && styles.focusedText]}>
            {activeWallet.coinSymbol}
          </Text>
        </View>
        <View style={styles.divider} />
        <View style={styles.inputContainer}>
          <TextInput
            style={[styles.input, isCryptoFocused || styles.focusedText]}
            placeholder={formatterHelper('0.00', 'USD')}
            onChangeText={(text) => onFiatInputChange(text)}
            keyboardType="numeric"
            returnKeyType="done"
            value={fiatInput}
            onFocus={() => setIsCryptoFocused(false)}
            placeholderTextColor="#878788"
          />
          <Text
            style={[styles.cryptoName, isCryptoFocused || styles.focusedText]}>
            USD
          </Text>
        </View>
      </View>
      <View style={styles.priceLockContainer}>
        <Text style={styles.priceLock}>Turn On PriceLock Insurance</Text>
        <Switch
          trackColor={{false: '#767577', true: '#81b0ff'}}
          thumbColor={ThemeData.TEXT_COLOR}
          onValueChange={() =>
            setIsPriceLockEnabled((previousState) => !previousState)
          }
          value={isPriceLockEnabled}
        />
      </View>
      <TouchableOpacity style={styles.actionBtn} onPress={onProceedConfirm}>
        <Text style={styles.actionBtnText}>Proceed To Confirmation</Text>
      </TouchableOpacity>
    </View>
  );
};

export default QuoteForm;

const styles = StyleSheet.create({
  container: {},
  balanceText: {
    fontSize: 35,
    fontFamily: ThemeData.FONT_BOLD,
    color: '#1A6BB4',
    textAlign: 'center',
  },
  balanceTextCoin: {
    fontSize: 20,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  usdBalance: {
    fontFamily: ThemeData.FONT_BOLD,
    textAlign: 'center',
    color: '#5F6163',
    fontSize: 16,
  },
  percentageContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 15,
  },
  percentageItem: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    width: 65,
    paddingVertical: 5,
    borderRadius: 5,
    marginLeft: 5,
  },
  percentageText: {
    fontFamily: ThemeData.FONT_MEDIUM,
    fontSize: 10,
    textAlign: 'center',
  },
  inputForm: {marginVertical: 30},
  inputContainer: {flexDirection: 'row', alignItems: 'center'},
  input: {
    flexGrow: 1,
    width: 0,
    color: '#5F6163',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    paddingHorizontal: 10,
  },
  focusedInput: {
    color: ThemeData.TEXT_COLOR,
  },
  inputButton: {
    borderWidth: 1,
    borderColor: '#EDEDED',
    justifyContent: 'center',
    paddingHorizontal: 8,
    paddingVertical: 2,
    marginLeft: 5,
  },
  inputButtonText: {
    color: '#B4BBC4',
    fontFamily: 'Montserrat',
    fontSize: 10,
  },
  cryptoName: {
    marginLeft: 10,
    color: '#5F6163',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  focusedText: {
    color: ThemeData.TEXT_COLOR,
  },
  divider: {
    backgroundColor: '#F2A900',
    height: 1,
    marginVertical: 5,
  },
  priceLockContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  priceLock: {
    color: '#5F6163',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
  actionBtn: {
    backgroundColor: ThemeData.TEXT_COLOR,
    borderRadius: 9,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
  actionBtnText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
});
