import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import Clipboard from '@react-native-community/clipboard';

const AddressForm = ({addressInput, setAddressInput, onProceedAmount}) => {
  const onPasteClick = async () => {
    const copiedText = await Clipboard.getString();
    setAddressInput(copiedText.trim());
  };

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Image
          source={require('../../assets/external-wallet-address.png')}
          style={styles.headerIcon}
        />
        <Text style={styles.headerText}>ADDRESS</Text>
      </View>
      <Text style={styles.descText}>
        Enter The External BTC Address That You Want The Coins Sent To
      </Text>
      <TextInput
        style={styles.addressInput}
        placeholder="Wallet Address"
        value={addressInput}
        onChangeText={(text) => setAddressInput(text)}
      />
      <TouchableOpacity style={styles.pasteBtn} onPress={onPasteClick}>
        <Image
          source={require('../../assets/paste-icon-blue.png')}
          style={styles.pasteIcon}
        />
        <Text style={styles.pasteText}>Click To Paste From Clipboard</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.actionBtn} onPress={onProceedAmount}>
        <Text style={styles.actionBtnText}>Proceed To Amount</Text>
      </TouchableOpacity>
    </View>
  );
};

export default AddressForm;

const styles = StyleSheet.create({
  container: {},
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.TEXT_COLOR,
    fontSize: 28,
  },
  headerIcon: {
    width: 32,
    height: 32,
    marginRight: 10,
    resizeMode: 'contain',
  },
  descText: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 12,
    marginTop: 15,
    color: '#5F6163',
    lineHeight: 22,
  },
  addressInput: {
    height: 55,
    marginVertical: 25,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 20,
    fontFamily: ThemeData.FONT_MEDIUM,
  },
  pasteBtn: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pasteIcon: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
    marginRight: 5,
  },
  pasteText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    fontSize: 11,
    color: '#5F6163',
  },
  actionBtn: {
    backgroundColor: ThemeData.TEXT_COLOR,
    borderRadius: 9,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 40,
  },
  actionBtnText: {
    textAlign: 'center',
    color: 'white',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
  },
});
