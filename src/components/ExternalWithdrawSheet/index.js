import React, {useState} from 'react';
import {Image, StyleSheet, View} from 'react-native';
import BottomSheetLayout from '../../layouts/BottomSheetLayout';
import {showToastMessage} from '../../utils';
import AddressForm from './AddressForm';
import QuoteForm from './QuoteForm';

const ExternalWithdrawSheet = ({isOpen, onClose}) => {
  const [isShowQuote, setIsShowQuote] = useState(false);
  const [addressInput, setAddressInput] = useState('');

  const onProceedAmount = () => {
    if (!addressInput) {
      return showToastMessage('Please Enter External Wallet Address');
    }
    setIsShowQuote(true);
  };

  return (
    <BottomSheetLayout isOpen={isOpen} onClose={onClose} reactToKeyboard>
      <View style={styles.container}>
        <Image
          source={require('../../assets/external-address-icon.png')}
          style={styles.headerIcon}
        />
        <View style={styles.viewContainer}>
          {isShowQuote ? (
            <QuoteForm onClose={onClose} />
          ) : (
            <AddressForm
              addressInput={addressInput}
              setAddressInput={setAddressInput}
              onProceedAmount={onProceedAmount}
            />
          )}
        </View>
      </View>
    </BottomSheetLayout>
  );
};

export default ExternalWithdrawSheet;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 30,
    paddingVertical: 25,
  },
  headerIcon: {
    resizeMode: 'contain',
    height: 40,
    marginLeft: 'auto',
    marginRight: 'auto',
    width: 170,
  },
  viewContainer: {
    marginTop: 40,
  },
});
