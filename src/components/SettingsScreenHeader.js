import {useNavigation} from '@react-navigation/native';
import React, {useContext} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import ThemeData from '../configs/ThemeData';
import {AppContext} from '../contexts/AppContextProvider';
import {getUriImage} from '../utils';

const SettingsScreenHeader = ({header, subHeader, breadCrumbs}) => {
  const {goBack} = useNavigation();

  const {avatar, fullName, userEmail} = useContext(AppContext);

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <View style={{flex: 1}}>
          <Text style={styles.header}>{header || 'Settings'}</Text>
          {breadCrumbs ? (
            <View style={styles.breadCrumbs}>
              {breadCrumbs?.map((item, index) => (
                <React.Fragment key={item.title}>
                  <Text onPress={item.onPress} style={styles.subHeader}>
                    {item.title}
                  </Text>
                  {breadCrumbs?.length === index + 1 || (
                    <Text style={styles.subHeader}>{' -> '}</Text>
                  )}
                </React.Fragment>
              ))}
            </View>
          ) : (
            <Text style={styles.subHeader}>
              {subHeader || 'Select One Of The Following Options'}
            </Text>
          )}
        </View>
        <View style={[styles.avatarContainer]}>
          {avatar ? (
            <FastImage
              style={[styles.avatar]}
              source={{uri: getUriImage(avatar)}}
              resizeMode={'cover'}
            />
          ) : (
            <Image
              style={styles.avatar}
              source={require('../assets/app-logo.png')}
              resizeMode={'contain'}
            />
          )}
        </View>
      </View>
    </View>
  );
};

export default SettingsScreenHeader;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 20,
    marginTop: 5,
  },
  profileContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    borderTopColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    marginHorizontal: -20,
    paddingHorizontal: 20,
    height: 70,
  },
  avatarContainer: {
    overflow: 'hidden',
    width: 45,
    height: 45,
    borderRadius: 40,
  },
  avatar: {
    flex: 1,
    width: null,
    height: null,
  },
  nameContainer: {
    marginLeft: 10,
    flex: 1,
  },
  userName: {
    fontSize: 18,
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_BOLD,
  },
  userEmail: {
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 10,
    fontFamily: ThemeData.FONT_NORMAL,
  },
  backButton: {
    width: 30,
    height: 30,
    padding: 6,
  },
  backButtonIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 30,
    marginTop: 15,
  },
  header: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 26,
  },
  subHeader: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
  },
  divider: {
    width: 1,
    backgroundColor: ThemeData.BORDER_COLOR,
    height: '100%',
    marginHorizontal: 20,
  },
  breadCrumbs: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
