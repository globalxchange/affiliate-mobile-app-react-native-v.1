import React, {useState} from 'react';
import {StyleSheet, View} from 'react-native';
import ViewSwitcher from './ViewSwitcher';
import InstantFunnels from './Fragments/InstantFunnels';

const ToolsViews = ({setFooterOverride}) => {
  const [activeSwitch, setActiveSwitch] = useState(switchItems[0]);

  return (
    <View style={styles.container}>
      <ViewSwitcher
        menuItems={switchItems}
        activeSwitch={activeSwitch}
        setActiveSwitch={setActiveSwitch}
      />
      <InstantFunnels
        setFooterOverride={setFooterOverride}
        activeSwitch={activeSwitch}
      />
    </View>
  );
};

export default ToolsViews;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

const switchItems = [
  {
    title: 'InstaCrypto',
    image: require('../../assets/tools-controls/instacrypto.png'),
    url:
      'https://storeapi.apimachine.com/dynamic/GXFunnels/InstaCryptoFunnelCentre?key=7d9e0009-7fb8-465a-a561-02cd0cbd4cba',
  },
  {
    title: 'AffiliateApp',
    image: require('../../assets/tools-controls/broker-app.png'),
    url:
      'https://storeapi.apimachine.com/dynamic/GXFunnels/BrokerAppFunnelCentre?key=7d9e0009-7fb8-465a-a561-02cd0cbd4cba',
  },
  {
    title: 'Assets.io',
    image: require('../../assets/tools-controls/assets-io.png'),
    url:
      'https://storeapi.apimachine.com/dynamic/GXFunnels/AssetsFunnelCentre?key=7d9e0009-7fb8-465a-a561-02cd0cbd4cba',
  },
  {
    title: 'GX',
    image: require('../../assets/tools-controls/gx.png'),
    url:
      'https://storeapi.apimachine.com/dynamic/GXFunnels/GXFunnelCentre?key=7d9e0009-7fb8-465a-a561-02cd0cbd4cba',
  },
  {
    title: 'GXToken',
    image: require('../../assets/tools-controls/gxtoken.png'),
    url:
      'https://storeapi.apimachine.com/dynamic/GXFunnels/GXTokenFunnelCentre?key=7d9e0009-7fb8-465a-a561-02cd0cbd4cba',
  },
  {
    title: 'Agency',
    image: require('../../assets/tools-controls/agency.png'),
    url:
      'https://storeapi.apimachine.com/dynamic/GXFunnels/AgencyFunnelCentre?key=7d9e0009-7fb8-465a-a561-02cd0cbd4cba',
  },
];
