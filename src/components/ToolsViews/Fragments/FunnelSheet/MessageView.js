import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import Clipboard from '@react-native-community/clipboard';

const MessageView = ({funnel}) => {
  const [message, setMessage] = useState(messageText);
  const [isCopied, setIsCopied] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [messageInput, setMessageInput] = useState(messageText);

  const onCopyClick = () => {
    Clipboard.setString(message);
    setIsCopied(true);
    setTimeout(() => setIsCopied(false), 5000);
  };

  const messageTypeDone = () => {
    if (messageInput) {
      setMessage(messageInput);
    }
    setIsEditing(false);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>
        Send This To Someone Who Has Voiced A Desire To Buy Crypto In The US
      </Text>
      {isEditing ? (
        <TextInput
          style={styles.input}
          value={messageInput}
          onChangeText={(text) => setMessageInput(text)}
          multiline
          placeholderTextColor={'#878788'}
          placeholder="Please Input Your Message"
        />
      ) : (
        <Text style={styles.message}>{message}</Text>
      )}

      <View style={styles.actionContainer}>
        <TouchableOpacity
          style={styles.buttonOutline}
          onPress={() => setIsEditing(!isEditing)}>
          <Text style={styles.buttonOutlineText}>
            {isEditing ? 'Cancel' : 'Make Edits'}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttonFilled}
          onPress={isEditing ? messageTypeDone : onCopyClick}>
          <Text style={styles.buttonFilledText}>
            {isEditing ? 'Done' : isCopied ? 'Copied' : 'Copy Message'}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default MessageView;

const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
    paddingBottom: 30,
    paddingHorizontal: 35,
  },
  header: {
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
    textAlign: 'center',
    fontSize: 14,
    marginBottom: 40,
  },
  message: {
    fontFamily: 'Montserrat',
    color: '#08152D',
    textAlign: 'center',
    fontSize: 13,
  },
  actionContainer: {
    flexDirection: 'row',
    marginTop: 40,
  },
  buttonOutline: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    borderColor: '#08152D',
    borderWidth: 1,
    marginRight: 15,
  },
  buttonOutlineText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    fontSize: 13,
  },
  buttonFilled: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    backgroundColor: '#08152D',
  },
  buttonFilledText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
    fontSize: 13,
  },
  input: {
    fontFamily: 'Montserrat',
    color: '#08152D',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    padding: 10,
  },
});

const messageText =
  'I remember that you were looking to buy crypto. I recently found this app that allows you to buy coins from any vendor in the US. Let me know if you are interested.';
