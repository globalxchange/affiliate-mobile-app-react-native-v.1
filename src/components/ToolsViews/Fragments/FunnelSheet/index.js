import React, {useState, useContext} from 'react';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import MainView from './MainView';
import Clipboard from '@react-native-community/clipboard';
import {AppContext} from '../../../../contexts/AppContextProvider';
import MessageView from './MessageView';
import BottomSheetLayout from '../../../../layouts/BottomSheetLayout';

const FunnelSheet = ({isOpen, setIsOpen, funnel}) => {
  const {userName} = useContext(AppContext);

  const [showProspect, setShowProspect] = useState(false);
  const [isCopied, setIsCopied] = useState(false);

  const copyText = funnel ? `${funnel.formData.link}${userName}` : '';

  const onCopyClick = () => {
    Clipboard.setString(copyText);
    setIsCopied(true);
    setTimeout(() => setIsCopied(false), 5000);
  };

  return (
    <BottomSheetLayout
      isOpen={isOpen}
      onClose={() => setIsOpen(false)}
      reactToKeyboard>
      <View style={styles.headerImageContainer}>
        <Image
          source={require('../../../../assets/funnel-icon.png')}
          style={styles.headerImage}
          resizeMode="contain"
        />
      </View>
      {showProspect ? (
        <MessageView funnel={funnel} />
      ) : (
        <MainView funnel={funnel} showMessage={() => setShowProspect(true)} />
      )}
      <View style={styles.linkView}>
        {isCopied ? (
          <Text style={styles.successText}>
            Your Link Has Been Copied To Your Clipboard
          </Text>
        ) : (
          <>
            <Text numberOfLines={1} style={styles.footerText}>
              {copyText}
            </Text>
            <TouchableOpacity onPress={onCopyClick} style={styles.footerButton}>
              <Image
                source={require('../../../../assets/copy-icon.png')}
                resizeMode="contain"
                style={styles.footerButtonIcon}
              />
            </TouchableOpacity>
          </>
        )}
      </View>
    </BottomSheetLayout>
  );
};

export default FunnelSheet;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    // flex: 1,
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
  },
  headerImageContainer: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
    marginBottom: 25,
  },
  headerImage: {
    height: 40,
  },
  linkView: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    height: 70,
    borderTopColor: '#EBEBEB',
    borderTopWidth: 1,
  },
  footerButton: {
    height: 30,
    width: 30,
    padding: 8,
  },
  footerButtonIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  footerText: {
    flex: 1,
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 10,
    fontSize: 12,
    textDecorationLine: 'underline',
  },
  successText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 10,
    textAlign: 'center',
    fontSize: 12,
  },
});
