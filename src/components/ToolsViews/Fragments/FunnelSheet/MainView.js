import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import HTMLView from 'react-native-htmlview';

const MainView = ({funnel, showMessage}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.header}>{funnel.formData.funnelname}</Text>
      <Image
        source={{uri: funnel.formData.thumbnail}}
        style={styles.thumbnail}
        resizeMode="cover"
      />
      <View style={styles.descContainer}>
        <HTMLView
          value={funnel.formData.idealaudience}
          stylesheet={sentMessageView}
          textComponentProps={{
            style: {
              color: '#08152D',
              fontFamily: 'Montserrat',
              textAlign: 'center',
            },
          }}
        />
        <TouchableOpacity onPress={showMessage} style={styles.button}>
          <Text style={styles.buttonText}>InstaProspects</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default MainView;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
  },
  header: {
    backgroundColor: '#08152D',
    color: 'white',
    fontFamily: 'Montserrat-Bold',
    textAlign: 'center',
    paddingVertical: 15,
  },
  thumbnail: {
    height: 220,
  },
  descContainer: {
    paddingVertical: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    backgroundColor: '#08152D',
    paddingVertical: 10,
    paddingHorizontal: 20,
    marginTop: 10,
  },
  buttonText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
  },
});

const sentMessageView = StyleSheet.create({
  p: {
    color: '#08152D',
    textAlign: 'center',
  },
  a: {
    fontWeight: 'bold',
  },
});
