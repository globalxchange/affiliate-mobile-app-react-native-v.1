import React, {useEffect, useState, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Axios from 'axios';
import LoadingAnimation from '../../LoadingAnimation';
import {AppContext} from '../../../contexts/AppContextProvider';
import * as WebBrowser from 'expo-web-browser';
import FunnelSheet from './FunnelSheet';

const {width} = Dimensions.get('window');

const ITEM_WIDTH = width * 0.5;

const InstantFunnels = ({setFooterOverride, activeSwitch}) => {
  const {userName} = useContext(AppContext);

  const [earnData, setEarnData] = useState();
  const [isDetailsOpen, setIsDetailsOpen] = useState(false);
  const [selectedFunnel, setSelectedFunnel] = useState();

  useEffect(() => {
    if (activeSwitch) {
      setEarnData();
      Axios.get(activeSwitch.url)
        .then((resp) => {
          const {data} = resp;

          if (data.success) {
            setEarnData(data.data || []);
          } else {
            setEarnData([]);
          }
        })
        .catch((error) => {
          console.log('Error on getting earnData', error);
        });
    }
  }, [activeSwitch]);

  useEffect(() => {
    if (!isDetailsOpen) {
      setSelectedFunnel();
    }
  }, [isDetailsOpen]);

  const openWebsite = (item) => {
    const url = `${item.formData.link}${userName}`;
    WebBrowser.openBrowserAsync(url);
  };

  const onItemClick = (item) => {
    setIsDetailsOpen(true);
    setSelectedFunnel(item);
  };

  return (
    <View style={styles.container}>
      <View>
        <Text style={styles.title}>{activeSwitch.title}</Text>
        {earnData && (
          <Text style={styles.desc}>
            There Is {earnData.length} Active Funnel For {activeSwitch.title}
          </Text>
        )}
      </View>
      <View>
        {earnData ? (
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={earnData}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item}) => (
              <View style={styles.itemContainer}>
                <TouchableOpacity onPress={() => onItemClick(item)}>
                  <Image
                    source={{uri: item.formData.thumbnail}}
                    style={styles.itemImage}
                    resizeMode="cover"
                  />
                  <Text style={styles.itemName}>
                    {item.formData.funnelname}
                  </Text>
                  {/* <Text numberOfLines={1} style={styles.itemDesc}>
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet
                  odit accusantium sequi dicta voluptates ut ad.
                </Text> */}
                </TouchableOpacity>
              </View>
            )}
            ListEmptyComponent={
              <Text style={styles.emptyText}>No InstaEarn Campaigns Found</Text>
            }
          />
        ) : (
          <View style={styles.loadingContainer}>
            <LoadingAnimation height={60} />
          </View>
        )}
      </View>
      <FunnelSheet
        isOpen={isDetailsOpen}
        setIsOpen={setIsDetailsOpen}
        funnel={selectedFunnel}
      />
    </View>
  );
};

export default InstantFunnels;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingBottom: 15,
    paddingTop: 20,
    paddingHorizontal: 30,
    justifyContent: 'space-evenly',
    backgroundColor: '#FBFBFB',
  },
  title: {
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
    fontSize: 20,
    marginBottom: 15,
  },
  desc: {
    fontFamily: 'Montserrat',
    color: '#9A9A9A',
    fontSize: 12,
    marginBottom: 5,
  },
  actionContainer: {
    flexDirection: 'row',
    marginTop: 'auto',
  },
  buttonOutlined: {
    borderColor: '#08152D',
    borderWidth: 1,
    paddingVertical: 5,
    paddingHorizontal: 12,
  },
  buttonOutlinedText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    fontSize: 8,
  },
  buttonFilled: {
    backgroundColor: '#08152D',
    paddingVertical: 5,
    paddingHorizontal: 12,
    marginLeft: 5,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonFilledText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
    fontSize: 8,
  },
  itemContainer: {
    width: ITEM_WIDTH,
    marginRight: 20,
    backgroundColor: 'white',
    borderColor: '#EBEBEB',
    borderWidth: 1,
  },
  itemImage: {
    width: ITEM_WIDTH,
    height: ITEM_WIDTH * 0.6,
    backgroundColor: '#C4C4C4',
  },
  itemName: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 13,
    marginTop: 8,
    marginBottom: 10,
    paddingHorizontal: 15,
  },
  itemDesc: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 13,
    marginBottom: 10,
  },
  loadingContainer: {
    justifyContent: 'center',
  },
  emptyText: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    textAlign: 'center',
    width: width - 60,
  },
  linkView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  footerButton: {
    height: 30,
    width: 30,
    padding: 8,
  },
  footerButtonIcon: {
    flex: 1,
    height: null,
    width: null,
  },
  footerText: {
    flex: 1,
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 10,
    fontSize: 12,
  },
  successText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
    paddingHorizontal: 10,
    textAlign: 'center',
    fontSize: 12,
  },
});
