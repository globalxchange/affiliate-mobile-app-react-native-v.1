import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import React from 'react';
import ThemeData from '../../configs/ThemeData';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

const ExchangeBaseFooter = ({baseCoins, coin, setCoin}) => {
  const {bottom} = useSafeAreaInsets();
  return (
    <View style={[styles.container, {marginBottom: bottom / 2}]}>
      <Text style={styles.textBase}>Base</Text>
      <Image
        source={require('../../assets/forward-icon-colored.png')}
        style={styles.arrow}
      />
      <FlatList
        style={styles.listView}
        contentContainerStyle={styles.listViewIn}
        horizontal={true}
        data={baseCoins}
        keyExtractor={(item) => item}
        showsHorizontalScrollIndicator={false}
        renderItem={({item}) => (
          <TouchableOpacity
            onPress={() => setCoin(item)}
            style={[
              styles.coinItm,
              {
                borderColor:
                  item === coin ? ThemeData.TEXT_COLOR : ThemeData.BORDER_COLOR,
              },
            ]}>
            <Text style={styles.appName}>{item}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

export default ExchangeBaseFooter;

const styles = StyleSheet.create({
  container: {
    height: 70,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 30,
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
  },
  textBase: {
    fontSize: 25,
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.TEXT_COLOR,
  },
  arrow: {
    marginLeft: 5,
    height: 25,
    width: 23,
    resizeMode: 'contain',
  },
  listView: {
    marginLeft: 10,
    height: 70,
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  listViewIn: {
    alignItems: 'center',
  },
  coinItm: {
    height: 40,
    width: 70,
    borderWidth: 1,
    borderColor: ThemeData.BORDER_COLOR,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 10,
  },
});
