import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {WToast} from 'react-native-smart-tip';
import {NEW_CHAT_API} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import {getPlaceholderText, getUriImage} from '../../utils';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';

const {width} = Dimensions.get('window');

const SLIDER_WIDTH = width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.5);
const ITEM_HEIGHT = Math.round((ITEM_WIDTH * 3) / 2);

const CarouselItem = ({item, onItemSelected}) => {
  const {navigate} = useNavigation();

  const {chatAppId} = useContext(AppContext);

  const [chatData, setChatData] = useState();

  useEffect(() => {
    if (chatAppId) {
      (async () => {
        const email = await AsyncStorageHelper.getLoginEmail();
        const token = await AsyncStorageHelper.getAppToken();

        const postData = {email: item?.email || '', app_id: chatAppId};
        const headers = {email, token};

        axios
          .post(`${NEW_CHAT_API}/get_user`, postData, {headers})
          .then(({data}) => {
            if (data.status) {
              setChatData(data?.payload || '');
            } else {
              setChatData();
            }
          })
          .catch((error) => {
            console.log('Error on getting user details', error);
          });
      })();
    }
  }, [item, chatAppId]);

  const openChat = () => {
    if (chatData) {
      navigate('Support', {openUserChat: chatData});
    } else {
      WToast.show({
        data: 'Sorry. This User Not available in Chat',
        position: WToast.position.TOP,
      });
    }
  };

  return (
    <View style={styles.itemContainer}>
      <TouchableOpacity
        onPress={() => onItemSelected(item)}
        style={[styles.imageContainer]}>
        {item.profile_img ? (
          <FastImage
            style={styles.carouselImage}
            source={{uri: getUriImage(item.profile_img)}}
            resizeMode="cover"
          />
        ) : (
          <View style={styles.profilePlaceholder}>
            <Text style={styles.placeholderText}>
              {getPlaceholderText(item.name)}
            </Text>
          </View>
        )}
      </TouchableOpacity>
      <TouchableOpacity onPress={openChat} style={styles.detail}>
        <View style={styles.detailsContainer}>
          <Text
            adjustsFontSizeToFit
            numberOfLines={1}
            style={styles.userName}>{`@${item.username}`}</Text>
          <Text adjustsFontSizeToFit numberOfLines={1} style={styles.name}>
            {item.name}
          </Text>
        </View>
        <View
          style={[
            styles.chatActivity,
            {backgroundColor: chatData ? 'green' : 'red'},
          ]}
        />
      </TouchableOpacity>
    </View>
  );
};

export default CarouselItem;

const styles = StyleSheet.create({
  itemContainer: {
    zIndex: 3,
  },
  imageContainer: {
    overflow: 'hidden',
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  carouselImage: {
    width: ITEM_WIDTH,
    height: ITEM_HEIGHT,
  },
  profilePlaceholder: {
    width: ITEM_WIDTH,
    height: ITEM_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 8,
  },
  placeholderText: {
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 30,
    textTransform: 'uppercase',
  },
  counter: {
    marginTop: 25,
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  loadingContainer: {
    height: ITEM_HEIGHT,
    justifyContent: 'center',
  },
  detailsContainer: {
    marginTop: 'auto',
    padding: 10,
    flex: 1,
  },
  userName: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    marginTop: 5,

    fontSize: 16,
  },
  name: {
    color: '#08152D',
    fontSize: 12,
    fontFamily: 'Montserrat',
    textTransform: 'capitalize',
  },
  detail: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  chatActivity: {
    width: 16,
    height: 16,
    borderRadius: 8,
  },
});
