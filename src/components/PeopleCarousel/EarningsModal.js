/* eslint-disable react-native/no-inline-styles */
import React, {useEffect, useState} from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import Modal from 'react-native-modal';
import ThemeData from '../../configs/ThemeData';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import LoadingAnimation from '../LoadingAnimation';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../configs';
import {usdValueFormatter} from '../../utils';

const {width, height} = Dimensions.get('window');

const EarningsModal = ({isOpen, onClose, selectedCustomer, userEmail}) => {
  const [selectedTab, setSelectedTab] = useState();
  const [earningsData, setEarningsData] = useState();
  const [selectedEarnings, setSelectedEarnings] = useState();

  useEffect(() => {
    if (!isOpen) {
      setSelectedTab();
      setEarningsData();
      setSelectedEarnings();
    }
  }, [isOpen]);

  useEffect(() => {
    setEarningsData();

    if (selectedCustomer) {
      (async () => {
        const email = userEmail || (await AsyncStorageHelper.getLoginEmail());

        Axios.get(`${GX_API_ENDPOINT}/brokerage/com/earnings/get`, {
          params: {
            email,
            customer: selectedCustomer || '',
          },
        })
          .then(({data}) => {
            // console.log('Data', data);

            const userData = data?.users ? data?.users[0] : '';

            const earnings = userData?.comData;

            const parsedData = [
              {
                type: 'total',
                revenue: earnings?.total_revenue || 0,
                earnings: earnings?.total_earnings || 0,
                direct: earnings?.direct || 0,
                indirect: earnings?.indirect || 0,
              },
              ...(earnings?.data || []),
            ];

            setEarningsData(parsedData);
            setSelectedTab(parsedData[0]);
          })
          .catch((error) => {
            console.log('Error on getting commission data', error);
          });
      })();
    }
  }, [selectedCustomer, userEmail]);

  useEffect(() => {
    if (earningsData) {
      const earning = earningsData.find((x) => x?.type === selectedTab?.type);
      setSelectedEarnings(earning);
    } else {
      setSelectedEarnings();
    }
  }, [selectedTab, earningsData]);

  return (
    <Modal
      isVisible={isOpen}
      onDismiss={onClose}
      deviceWidth={width}
      deviceHeight={height}
      onBackButtonPress={onClose}
      onBackdropPress={onClose}
      animationIn="zoomIn"
      animationOut="zoomOut"
      backdropColor="#ffffff"
      backdropOpacity={0.9}>
      <TouchableOpacity
        activeOpacity={1}
        style={styles.overlay}
        onPress={onClose}
        onPressOut={onClose}>
        <TouchableWithoutFeedback style={{flex: 1}}>
          <View style={styles.modalContainer}>
            <View style={styles.headerContainer}>
              <TouchableOpacity style={styles.headerTab}>
                <Text style={styles.headerTabText}>Earnings</Text>
              </TouchableOpacity>
              <TouchableOpacity
                disabled
                style={[
                  styles.headerTab,
                  {
                    backgroundColor: 'white',
                    borderBottomColor: ThemeData.APP_MAIN_COLOR,
                    borderBottomWidth: 1,
                  },
                ]}>
                <Text
                  style={[
                    styles.headerTabText,
                    {
                      color: ThemeData.APP_MAIN_COLOR,
                      fontFamily: ThemeData.FONT_NORMAL,
                      opacity: 0.5,
                    },
                  ]}>
                  Equity
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.tabContainer}>
              {earningsData?.map((item) => (
                <TouchableOpacity
                  key={item.type}
                  onPress={() => setSelectedTab(item)}
                  style={[
                    styles.tabItem,
                    selectedTab?.type === item.type && {borderBottomWidth: 1},
                  ]}>
                  <Text
                    style={[
                      styles.tabTitle,
                      selectedTab?.type === item.type && {
                        fontFamily: ThemeData.FONT_SEMI_BOLD,
                      },
                    ]}>
                    {item.type}
                  </Text>
                </TouchableOpacity>
              ))}
            </View>
            <View style={styles.modalContent}>
              {earningsData ? (
                <View style={styles.earnings}>
                  <View style={styles.earningItem}>
                    <Text style={styles.earningItemTitle}>
                      {usdValueFormatter.format(selectedEarnings?.revenue || 0)}
                    </Text>
                    <Text style={styles.earningItemValue}>
                      {selectedTab?.type} Revenue
                    </Text>
                  </View>
                  <View style={styles.earningItem}>
                    <Text style={styles.earningItemTitle}>
                      {usdValueFormatter.format(
                        selectedEarnings?.earnings || 0,
                      )}
                    </Text>
                    <Text style={styles.earningItemValue}>
                      {selectedTab?.type} Earnings
                    </Text>
                  </View>
                  <View style={styles.earningItem}>
                    <Text style={styles.earningItemTitle}>
                      {usdValueFormatter.format(selectedEarnings?.direct || 0)}
                    </Text>
                    <Text style={styles.earningItemValue}>
                      {selectedTab?.type} Earnings
                    </Text>
                  </View>
                  <View style={styles.earningItem}>
                    <Text style={styles.earningItemTitle}>
                      {usdValueFormatter.format(
                        selectedEarnings?.indirect || 0,
                      )}
                    </Text>
                    <Text style={styles.earningItemValue}>
                      {selectedTab?.type} Earnings
                    </Text>
                  </View>
                </View>
              ) : (
                <View style={styles.loadingContainer}>
                  <LoadingAnimation />
                </View>
              )}
            </View>
          </View>
        </TouchableWithoutFeedback>
      </TouchableOpacity>
    </Modal>
  );
};

export default EarningsModal;

const styles = StyleSheet.create({
  modalContainer: {
    backgroundColor: 'white',
    borderRadius: 20,
    width: width * 0.85,
    marginLeft: 'auto',
    marginRight: 'auto',
    overflow: 'hidden',
    borderWidth: 1,
    borderColor: ThemeData.APP_MAIN_COLOR,
    height: height * 0.6,
  },
  headerContainer: {
    flexDirection: 'row',
  },
  headerTab: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 50,
    backgroundColor: ThemeData.APP_MAIN_COLOR,
  },
  headerTabText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: 'white',
  },
  tabContainer: {
    flexDirection: 'row',
    borderBottomColor: ThemeData.APP_MAIN_COLOR,
    borderBottomWidth: 0.5,
  },
  tabItem: {
    flex: 1,
    justifyContent: 'center',
    borderBottomColor: ThemeData.APP_MAIN_COLOR,
    paddingVertical: 12,
  },
  tabTitle: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 13,
    textTransform: 'capitalize',
  },
  modalContent: {
    flex: 1,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  earnings: {
    flex: 1,
    justifyContent: 'space-evenly',
  },
  earningItem: {},
  earningItemTitle: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 30,
  },
  earningItemValue: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 11,
    textTransform: 'capitalize',
  },
});
