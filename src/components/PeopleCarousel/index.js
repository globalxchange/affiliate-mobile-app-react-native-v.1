import React, {useEffect, useRef, useState} from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import Carousel from 'react-native-snap-carousel';
import ThemeData from '../../configs/ThemeData';
import {getPlaceholderText, getUriImage} from '../../utils';
import {
  animatedStyles,
  scrollInterpolator,
} from '../../utils/CarouselAnimation';
import LoadingAnimation from '../LoadingAnimation';
import EarningsModal from './EarningsModal';
import {useRoute} from '@react-navigation/native';
import CarouselItem from './CarouselItem';

const {width} = Dimensions.get('window');

const SLIDER_WIDTH = width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.5);
const ITEM_HEIGHT = Math.round((ITEM_WIDTH * 3) / 2);

const PeopleCarousel = ({
  users,
  selectedUser,
  setSelectedUser,
  isPopupOpen,
  setIsPopupOpen,
  scrollIndex,
}) => {
  const {params} = useRoute();

  const {userEmail} = params;

  const carouselRef = useRef();

  const [currentPos, setCurrentPos] = useState(0);

  useEffect(() => {
    carouselRef.current?.snapToItem(scrollIndex || 0);
  }, [scrollIndex]);

  const onSnapToItem = (index) => {
    setCurrentPos(index);
    // if (carouselData) {
    //   setActiveCarousel(carouselData[index]);
    // }
  };

  // console.log('users', users);

  const onItemSelected = (item) => {
    setSelectedUser(item);
    setIsPopupOpen(true);
  };

  return (
    <View style={styles.container}>
      {users ? (
        users.length > 0 ? (
          <Carousel
            ref={carouselRef}
            data={users}
            sliderWidth={SLIDER_WIDTH}
            itemWidth={ITEM_WIDTH}
            containerCustomStyle={styles.carouselContainer}
            inactiveSlideShift={0}
            onSnapToItem={onSnapToItem}
            scrollInterpolator={scrollInterpolator}
            slideInterpolatedStyle={animatedStyles}
            loop
            onScrollToIndexFailed={() => {}}
            inactiveSlideOpacity={0.5}
            inactiveSlideScale={0.7}
            renderItem={({item, index}) => (
              <CarouselItem item={item} onItemSelected={onItemSelected} />
            )}
          />
        ) : (
          <View style={styles.emptyContainer}>
            <Text style={styles.emptyText}>No Users Found...</Text>
          </View>
        )
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation height={70} width={70} />
        </View>
      )}
      <EarningsModal
        isOpen={isPopupOpen}
        onClose={() => setIsPopupOpen(false)}
        selectedCustomer={selectedUser}
        userEmail={userEmail}
      />
    </View>
  );
};

export default PeopleCarousel;

const styles = StyleSheet.create({
  container: {},
  carouselContainer: {},

  actionContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 30,
  },
  actionButton: {
    width: 50,
    height: 50,
    padding: 15,
    borderRadius: 25,
    marginHorizontal: 15,
  },
  actionIcon: {
    flex: 1,
    width: null,
    height: null,
  },
  emptyContainer: {},
  emptyText: {
    textAlign: 'center',
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    fontSize: 18,
  },
});

const carouselData = [
  require('../../assets/PeopleImagePlaceholders/1.png'),
  require('../../assets/PeopleImagePlaceholders/2.png'),
  require('../../assets/PeopleImagePlaceholders/3.png'),
];
