import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../configs/ThemeData';

const ComingSoon = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.header}>This Page Is Coming Soon</Text>
      <TouchableOpacity style={styles.devButton}>
        <Text style={styles.devBtnText}>Dev Access</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ComingSoon;

const styles = StyleSheet.create({
  container: {flex: 1, justifyContent: 'center', alignItems: 'center'},
  header: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 22,
    marginBottom: 40,
  },
  devButton: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    height: 50,
    width: 190,
    justifyContent: 'center',
    alignItems: 'center',
  },
  devBtnText: {
    color: 'white',
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 15,
  },
});
