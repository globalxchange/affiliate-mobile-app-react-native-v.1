import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Image,
} from 'react-native';
import LoadingAnimation from '../LoadingAnimation';
import Axios from 'axios';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import {GX_API_ENDPOINT} from '../../configs';
import {usdValueFormatter, percentageFormatter} from '../../utils';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import BottomSheetLayout from '../../layouts/BottomSheetLayout';

const UserStatsSheet = ({isBottomSheetOpen, setIsBottomSheetOpen, data}) => {
  const {bottom} = useSafeAreaInsets();

  const [statsData, setStatsData] = useState({});

  useEffect(() => {
    if (data && isBottomSheetOpen) {
      (async () => {
        const email = await AsyncStorageHelper.getLoginEmail();

        Axios.get(`${GX_API_ENDPOINT}/brokerage/otc/txn/stats/get`, {
          params: {email, customer_email: data.email},
        })
          .then((resp) => {
            const stats = resp.data;

            // console.log('Data', stats);

            const totalTransaction = stats.total_transactions;
            const transactionalVolume = stats.total_txn_volume;
            const totalFeePaid = stats.total_txn_fees;
            const averageFeePaid = stats.total_txn_fees_percentage;
            const revenueFromUser = stats.total_txn_revenue;

            if (stats.status) {
              setStatsData({
                totalTransaction,
                transactionalVolume,
                totalFeePaid,
                averageFeePaid,
                revenueFromUser,
              });
            }
          })
          .catch((error) => {
            console.log('Error on getting statsData', error);
          });
      })();
    } else {
      setStatsData();
    }
  }, [data, isBottomSheetOpen]);

  return (
    <BottomSheetLayout
      isOpen={isBottomSheetOpen}
      onClose={() => setIsBottomSheetOpen(false)}>
      {data && (
        <View style={styles.viewContainer}>
          <View style={styles.headerContainer}>
            <Image
              source={{
                uri: data.profile_img || 'https://i.pravatar.cc/110',
              }}
              style={styles.avatar}
              resizeMode="cover"
            />
            <View style={styles.nameContainer}>
              <Text style={styles.name}>{data.name || 'Jon Doe'}</Text>
              <Text style={styles.email}>{data.email}</Text>
            </View>
          </View>
          {statsData ? (
            <View style={styles.listContainer}>
              <View style={styles.listItem}>
                <Text style={styles.listTitle}>Total Transaction</Text>
                <Text style={styles.listValue}>
                  {statsData.totalTransaction || 0}
                </Text>
              </View>
              <View style={styles.listItem}>
                <Text style={styles.listTitle}>Transaction Volume</Text>
                <Text style={styles.listValue}>
                  {usdValueFormatter.format(statsData.transactionalVolume || 0)}
                </Text>
              </View>
              <View style={styles.listItem}>
                <Text style={styles.listTitle}>Total Fee Paid</Text>
                <Text style={styles.listValue}>
                  {usdValueFormatter.format(statsData.totalFeePaid || 0)}
                </Text>
              </View>
              <View style={styles.listItem}>
                <Text style={styles.listTitle}>Average Fee Percentage</Text>
                <Text style={styles.listValue}>
                  {percentageFormatter.format(statsData.averageFeePaid || 0)}%
                </Text>
              </View>
              <View style={styles.listItem}>
                <Text style={styles.listTitle}>
                  You Revenue From {data.name ? data.name.split(' ')[0] : ''}
                </Text>
                <Text style={styles.listValue}>
                  {usdValueFormatter.format(statsData.revenueFromUser || 0)}
                </Text>
              </View>
            </View>
          ) : (
            <View style={styles.loadingContainer}>
              <LoadingAnimation />
            </View>
          )}
        </View>
      )}
      <TouchableOpacity
        onPress={() => setIsBottomSheetOpen(false)}
        style={styles.closeButton}>
        <Text style={styles.closeText}>Close</Text>
      </TouchableOpacity>
    </BottomSheetLayout>
  );
};

export default UserStatsSheet;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    // flex: 1,
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    paddingHorizontal: 30,
    paddingTop: 20,
    paddingBottom: 20,
  },
  viewContainer: {
    paddingHorizontal: 10,
    paddingVertical: 15,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  avatar: {
    height: 45,
    width: 45,
    borderRadius: 25,
  },
  nameContainer: {
    marginLeft: 15,
    flex: 1,
  },
  name: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
  },
  email: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat',
    fontSize: 12,
  },
  listContainer: {
    marginTop: 20,
    marginBottom: 20,
  },
  listItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
    backgroundColor: 'white',
    paddingVertical: 15,
    paddingHorizontal: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
    marginHorizontal: 5,
    marginBottom: 1,
  },
  listTitle: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 12,
  },
  listValue: {
    color: '#001D41',
    fontFamily: 'Montserrat-Bold',
    opacity: 0.5,
    fontSize: 12,
  },
  closeButton: {
    backgroundColor: '#08152D',
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: -30,
    marginBottom: -20,
  },
  closeText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
  },
  loadingContainer: {
    justifyContent: 'center',
    height: 300,
  },
});
