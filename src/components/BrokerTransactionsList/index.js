import React, {useState} from 'react';
import {StyleSheet, Text, View, SectionList} from 'react-native';
import {useEffect} from 'react';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../configs';
import {usdValueFormatter} from '../../utils';
import Moment from 'moment-timezone';
import LoadingAnimation from '../LoadingAnimation';
import UserStatsSheet from './UserStatsSheet';

const BrokerTransactionList = ({hidden, selectedUser, type}) => {
  const [totalTransaction, setTotalTransaction] = useState();
  const [filteredList, setFilteredList] = useState();
  const [parsedList, setParsedList] = useState();
  const [isStatsOpen, setIsStatsOpen] = useState(false);

  useEffect(() => {
    (async () => {
      const email = await AsyncStorageHelper.getLoginEmail();
      // const email = 'shorupan@gmail.com';

      Axios.get(`${GX_API_ENDPOINT}/brokerage/stats/otc_txns`, {
        params: {email},
      })
        .then((resp) => {
          const {data} = resp;

          // console.log('Direct', email, 'Resp', data);

          if (data.status) {
            if (type === 'Direct') {
              setTotalTransaction(data.direct);
              setFilteredList(data.direct);
            } else {
              setTotalTransaction(data.indirect);
              setFilteredList(data.indirect);
            }
          }
        })
        .catch((error) => {
          console.log('Error on getting transactions');
        });
    })();
  }, [type]);

  useEffect(() => {
    if (selectedUser && totalTransaction) {
      if (
        selectedUser.name === 'All Directs' ||
        selectedUser.name === 'All Indirect'
      ) {
        setFilteredList(totalTransaction);
      } else {
        const filtered = totalTransaction.filter(
          (item) => item.txn.email === selectedUser.email,
        );

        setFilteredList(filtered);
      }
    }
  }, [selectedUser, totalTransaction]);

  useEffect(() => {
    const tempList = filteredList || [];

    const addedList = [];

    const parsed = [];

    tempList.forEach((tmpItem) => {
      if (!addedList.includes(tmpItem)) {
        const date = Moment(tmpItem.txn.timestamp);
        const subList = [];
        addedList.push(tmpItem);
        subList.push(tmpItem);

        tempList.forEach((item) => {
          if (!addedList.includes(item)) {
            const itemDate = Moment(item.txn.timestamp);
            if (date.isSame(itemDate, 'day')) {
              addedList.push(item);
              subList.push(item);
            }
          }
        });

        parsed.push({
          title: date.format('dddd MMMM Do YYYY'),
          data: subList,
        });
      }
    });
    setParsedList(parsed);
  }, [filteredList]);

  const onStatsClick = () => {
    setIsStatsOpen(true);
  };

  return (
    <View
      style={[
        styles.container,
        hidden && {display: 'none', overflow: 'hidden'},
      ]}>
      {filteredList ? (
        <>
          <View style={styles.headerContainer}>
            {selectedUser && selectedUser.email ? (
              <Text onPress={onStatsClick} style={styles.stats}>
                {type === 'Direct'
                  ? `${
                      selectedUser.name ? selectedUser.name.split(' ')[0] : ''
                    }'s Stats`
                  : 'Brokerage Stats'}
              </Text>
            ) : (
              <Text style={styles.header}>
                {selectedUser && selectedUser.email
                  ? `${
                      selectedUser.name ? selectedUser.name.split(' ')[0] : ''
                    }'s`
                  : `${type} Customer`}{' '}
                Transactions
              </Text>
            )}
          </View>
          <SectionList
            stickySectionHeadersEnabled={false}
            style={styles.list}
            showsVerticalScrollIndicator={false}
            sections={parsedList}
            keyExtractor={(item, index) => item + index}
            renderSectionHeader={({section: {title}}) => (
              <View style={styles.transactionGroup}>
                <Text style={styles.date}>{title}</Text>
              </View>
            )}
            renderItem={({item}) => (
              <View style={styles.item}>
                <Text numberOfLines={1} style={styles.txnName}>{`${
                  item && item.txn && item.txn.name
                    ? item.txn.name.split(' ')[0]
                    : ''
                } Did A ${item?.txn?.purchased}`}</Text>
                <Text
                  style={
                    styles.txnCommination
                  }>{`You Made ${usdValueFormatter.format(
                  item?.commissions?.dt_commission || 0,
                )} USD`}</Text>
              </View>
            )}
            ListEmptyComponent={
              <View style={styles.emptyContainer}>
                <Text style={styles.emptyText}>No Transaction Found</Text>
              </View>
            }
          />
        </>
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      )}
      <UserStatsSheet
        isBottomSheetOpen={isStatsOpen}
        setIsBottomSheetOpen={setIsStatsOpen}
        data={selectedUser}
      />
    </View>
  );
};

export default BrokerTransactionList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 30,
    paddingBottom: 10,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
    backgroundColor: 'white',
  },
  header: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    fontSize: 12,
  },
  noOfTxns: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    fontSize: 12,
  },
  stats: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    fontSize: 12,
    textDecorationLine: 'underline',
  },
  list: {
    marginHorizontal: -5,
  },
  item: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    backgroundColor: 'white',
    paddingVertical: 15,
    paddingHorizontal: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
    marginHorizontal: 5,
    marginBottom: 1,
  },
  txnName: {
    color: '#001D41',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 11,
    paddingRight: 20,
    flex: 1,
  },
  txnCommination: {
    color: '#001D41',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 11,
  },
  transactionGroup: {
    // padding: 15,
    marginBottom: 5,
    marginTop: 20,
  },
  date: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    fontSize: 10,
  },
  emptyContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyText: {
    textAlign: 'center',
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    fontSize: 15,
    marginTop: 30,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
});
