import React, {useEffect, useRef, useState} from 'react';
import {Dimensions, StyleSheet, Text, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import Carousel from 'react-native-snap-carousel';
import {getUriImage} from '../utils';
import {animatedStyles, scrollInterpolator} from '../utils/CarouselAnimation';
import LoadingAnimation from './LoadingAnimation';

const {width} = Dimensions.get('window');

const SLIDER_WIDTH = width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.65);
const ITEM_HEIGHT = Math.round((ITEM_WIDTH * 3) / 6);

const EventCarousel = ({events, setActiveEvent}) => {
  const [currentPos, setCurrentPos] = useState(0);

  const [carouselData, setCarouselData] = useState();

  const carouselRef = useRef();

  useEffect(() => {
    if (events) {
      setActiveEvent(events[0]);
    }
    setCarouselData(events);
  }, [events]);

  const onSnapToItem = (index) => {
    setCurrentPos(index);
    if (carouselData) {
      setActiveEvent(carouselData[index]);
    }
  };

  return (
    <View>
      {carouselData ? (
        <Carousel
          ref={carouselRef}
          data={carouselData}
          sliderWidth={SLIDER_WIDTH}
          itemWidth={ITEM_WIDTH}
          containerCustomStyle={styles.carouselContainer}
          inactiveSlideShift={0}
          onSnapToItem={onSnapToItem}
          scrollInterpolator={scrollInterpolator}
          slideInterpolatedStyle={animatedStyles}
          useScrollView={true}
          loop
          inactiveSlideOpacity={0.5}
          inactiveSlideScale={0.75}
          renderItem={({item}) => (
            <View style={styles.itemContainer}>
              <View style={[styles.imageContainer]}>
                <FastImage
                  style={styles.carouselImage}
                  source={{uri: getUriImage(item.icon)}}
                  resizeMode="cover"
                />
              </View>
              <Text style={styles.itemLabel}>{item.name}</Text>
            </View>
          )}
        />
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation height={70} width={70} />
        </View>
      )}
    </View>
  );
};

export default EventCarousel;

const styles = StyleSheet.create({
  carouselContainer: {
    marginTop: 10,
  },
  itemContainer: {
    zIndex: 3,
  },
  imageContainer: {
    width: ITEM_WIDTH,
    height: ITEM_HEIGHT,
    overflow: 'hidden',
    borderRadius: 6,
  },
  carouselImage: {
    flex: 1,
    height: null,
    width: null,
  },
  itemLabel: {
    color: '#08152D',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
    marginTop: 15,
  },
  counter: {
    marginTop: 25,
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  loadingContainer: {
    height: ITEM_HEIGHT,
    justifyContent: 'center',
  },
});

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

const EVENTS_DATA = [
  {
    icon: require('../assets/brands-carousel/insta-crypto.png'),
    name: 'Main Event',
  },
  {
    icon: require('../assets/brands-carousel/insta-crypto.png'),
    name: 'Main Event',
  },
  {
    icon: require('../assets/brands-carousel/insta-crypto.png'),
    name: 'Main Event',
  },
  {
    icon: require('../assets/brands-carousel/insta-crypto.png'),
    name: 'Main Event',
  },
  {
    icon: require('../assets/brands-carousel/insta-crypto.png'),
    name: 'Main Event',
  },
  {
    icon: require('../assets/brands-carousel/insta-crypto.png'),
    name: 'Main Event',
  },
];
