import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ThemeData from '../configs/ThemeData';

const ProfileSettingsItem = ({
  onPress,
  isDisabled,
  title,
  subText,
  icon,
  style = {},
}) => {
  return (
    <TouchableOpacity disabled={isDisabled} onPress={onPress}>
      <View style={[styles.optionItem, isDisabled && {opacity: 0.4}]}>
        {icon ? (
          <Image style={styles.icon} source={icon} resizeMode="contain" />
        ) : null}
        <View style={styles.optionDetails}>
          <Text style={styles.optionTitle}>{title}</Text>
          {subText ? <Text style={styles.optionDesc}>{subText}</Text> : null}
        </View>
        <Image
          source={require('../assets/chevron-up.png')}
          resizeMode="contain"
          style={styles.arrowIcon}
        />
      </View>
    </TouchableOpacity>
  );
};

export default ProfileSettingsItem;

const styles = StyleSheet.create({
  optionItem: {
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    padding: 20,
    alignItems: 'center',
    marginBottom: 15,
  },
  icon: {
    width: 30,
    height: 30,
    marginRight: 10,
  },
  optionDetails: {
    flex: 1,
  },
  optionTitle: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
  },
  optionDesc: {
    color: ThemeData.APP_MAIN_COLOR,
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 10,
    opacity: 0.75,
    marginTop: 5,
  },
  arrowIcon: {
    width: 20,
    height: 20,
    transform: [{rotate: '90deg'}],
  },
});
