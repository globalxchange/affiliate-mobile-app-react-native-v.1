import {useNavigation} from '@react-navigation/native';
import React, {useContext, useEffect, useState} from 'react';
import {useMemo} from 'react';
import {FlatList, ScrollView, StyleSheet, View} from 'react-native';
import {AppContext} from '../../contexts/AppContextProvider';
import AssetsIoController from '../ControllerLists/AssetsIoController';
import OTCBotsBottomSheet from '../ControllerLists/OTCBotsBottomSheet';
import MenuItem from './MenuItem';

const SettingsSelector = ({
  setSelectedCategory,
  selectedCategory,
  selectedBrand,
}) => {
  const {cryptoTableData} = useContext(AppContext);

  const [isAssetIoControllerOpen, setIsAssetIoControllerOpen] = useState(false);
  const [isLiquid, setIsLiquid] = useState(false);
  const [isEditor, setIsEditor] = useState(false);

  const [isOTCBotsOpen, setIsOTCBotsOpen] = useState(false);
  const [otcData, setOtcData] = useState('');

  useEffect(() => {
    if (isAssetIoControllerOpen) {
      setIsOTCBotsOpen(false);
    }
  }, [isAssetIoControllerOpen]);

  useEffect(() => {
    if (isOTCBotsOpen) {
      setIsAssetIoControllerOpen(false);
    }
  }, [isOTCBotsOpen]);

  let fiatAssets = 0;
  let cryptoAssets = 0;

  if (cryptoTableData) {
    // console.log('cryptoTableData', cryptoTableData);

    fiatAssets = cryptoTableData.filter((item) => item.asset_type === 'Fiat')
      .length;

    cryptoAssets = cryptoTableData.length - fiatAssets;
  }

  const {navigate} = useNavigation();
  


  const instaProductsList = useMemo(() => {
    switch (selectedBrand.title) {
      case 'Bonds':
        return [
          {
            icon: require('../../assets/forex.png'),
            title: 'Forex Currencies',
            subTitle: 'When Your Customers Earn Interest From Forex ',
            onPress: () => {
              navigate('MoneyMarketFeeScreen', {
                type: 'fiat',
                selectedBrand,
                bonds: true,
              });
            },
          },
          {
            icon: require('../../assets/crypto.png'),
            title: 'Cryptocurrencies',
            subTitle: 'When Your Customers Earn Interest From Crypto ',
            onPress: () => {
              navigate('MoneyMarketFeeScreen', {
                type: 'crypto',
                selectedBrand,
                bonds: true,
              });
            },
          },
          {
            icon: require('../../assets/per-customer-icon.png'),
            title: 'Per Customer',
            isCustom: true,
            subTitle: 'Set Custom Exchange Fees For Specific Customers',
          },
          {
            icon: require('../../assets/bond.png'),
            title: 'Per Bond',
            isCustom: true,
            subTitle: 'Set Custom Bond Fees For Specific Bonds',
          },
        ];
      case 'Money Markets':
        return [
          {
            icon: require('../../assets/forex.png'),
            title: 'Forex Currencies',
            subTitle: 'When Your Customers Earn Interest From Forex ',
            onPress: () => {
              navigate('MoneyMarketFeeScreen', {
                type: 'fiat',
                selectedBrand,
              });
            },
          },
          {
            icon: require('../../assets/crypto.png'),
            title: 'Cryptocurrencies',
            subTitle: 'When Your Customers Earn Interest From Crypto ',
            onPress: () => {
              navigate('MoneyMarketFeeScreen', {
                type: 'crypto',
                selectedBrand,
              });
            },
          },
          {
            icon: require('../../assets/per-customer-icon.png'),
            title: 'Per Customer',
            isCustom: true,
            subTitle: 'Set Custom Exchange Fees For Specific Customers',
          },
        ];

      default:
        return [
          {
            icon: require('../../assets/fiat-crypto-icon.png'),
            title: 'Forex To Crypto',
            fromAssets: 'Fiat',
            toAsset: 'Crypto',
            subTitle: 'When Your Customers Trade From Forex To Crypto',
            onPress: () => {
              navigate('ExchangeFeePairScreen', {
                type: 'fiat_crypto',
                selectedBrand,
              });
            },
          },
          {
            icon: require('../../assets/crypto-fiat-icon.png'),
            title: 'Crypto To Forex',
            fromAssets: 'Crypto',
            toAsset: 'Fiat',
            subTitle: 'When Your Customers Trade From Crypto To Forex',
            onPress: () => {
              navigate('ExchangeFeePairScreen', {
                type: 'crypto_fiat',
                selectedBrand,
              });
            },
          },
          {
            icon: require('../../assets/fiat-fiat-icon.png'),
            title: 'Forex To Forex',
            fromAssets: 'Fiat',
            toAsset: 'Fiat',
            subTitle: 'When Your Customers Trade From Forex To Forex',
            onPress: () => {
              navigate('ExchangeFeePairScreen', {
                type: 'fiat_fiat',
                selectedBrand,
              });
            },
          },
          {
            icon: require('../../assets/crypto-crypto-icon.png'),
            title: 'Crypto To Crypto',
            fromAssets: 'Crypto',
            toAsset: 'Crypto',
            subTitle: 'When Your Customers Trade Crypto To Crypto',
            onPress: () => {
              navigate('ExchangeFeePairScreen', {
                type: 'crypto_crypto',
                selectedBrand,
              });
            },
          },
          {
            icon: require('../../assets/per-customer-icon.png'),
            title: 'Per Customer',
            isCustom: true,
            subTitle: 'Set Custom Exchange Fees For Specific Customers',
          },
        ];
    }
  }, []);

  const onInstaProductClick = (index, isEdit) => {
    console.log('Clicked');
    setOtcData(instaProductsList[index]);
    setIsEditor(isEdit || false);
    setIsOTCBotsOpen(true);
  };

  const viewContents = useMemo(() => {
    switch (selectedCategory) {
      case 'Update':
        return (
          <FlatList
            showsVerticalScrollIndicator={false}
            data={instaProductsList}
            keyExtractor={(item) => item.title}
            renderItem={({item}) => (
              <MenuItem
                icon={item.icon}
                title={item.title}
                subTitle={item.subTitle}
                onPress={item.onPress}
                disabled={item.disabled}
              />
            )}
          />
        );

      default:
        return (
          <ScrollView showsVerticalScrollIndicator={false}>
            <MenuItem
              icon={require('../../assets/change-fee-icon.png')}
              title={`Change ${selectedBrand.mtToolName}`}
              subTitle={`Change ${selectedBrand.mtToolName}`}
              onPress={() => setSelectedCategory('Update')}
            />
          </ScrollView>
        );
    }
  }, [
    instaProductsList,
    selectedBrand.mtToolName,
    selectedCategory,
    setSelectedCategory,
  ]);

  return (
    <View style={styles.container}>
      {viewContents}
      <AssetsIoController
        selectedApp={selectedBrand}
        isOpen={isAssetIoControllerOpen}
        setIsOpen={setIsAssetIoControllerOpen}
        isLiquid={isLiquid}
        isEditor={isEditor}
      />
      <OTCBotsBottomSheet
        setIsSheetOpen={setIsOTCBotsOpen}
        isSheetOpen={isOTCBotsOpen}
        data={otcData}
        selectedApp={selectedBrand}
        isEditor={isEditor}
      />
    </View>
  );
};

export default SettingsSelector;

const styles = StyleSheet.create({
  container: {},
});
