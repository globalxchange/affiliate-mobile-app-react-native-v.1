import {useRoute} from '@react-navigation/native';
import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {APP_CODE, GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import {formatterHelper, usdValueFormatter} from '../../utils';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import SkeltonItem from '../SkeltonItem';
import VaultChart from '../VaultChart';

const Overview = ({BalanceComponent, activeWallet, selectedApp}) => {
  const {params} = useRoute();

  const [moneyMarket, setMoneyMarket] = useState();

  useEffect(() => {
    (async () => {
      setMoneyMarket();
      if (params?.type === 'moneymarket') {
        const email = await AsyncStorageHelper.getLoginEmail();

        axios
          .get(`${GX_API_ENDPOINT}/coin/iced/liquid/earnings/per/user/get`, {
            params: {
              app_code: selectedApp?.app_code || APP_CODE,
              coin: activeWallet?.coinSymbol || '',
              email,
            },
          })
          .then(({data}) => {
            if (data.status) {
              setMoneyMarket(data.coins[0]);
            }
          })
          .catch((err) => {
            console.log('Error getting money market', err);
          });
      }
    })();
  }, [params, activeWallet]);

  return (
    <View style={styles.container}>
      {BalanceComponent}
      {params?.type === 'moneymarket' ? (
        <View style={styles.moneymarketContainer}>
          {moneyMarket ? (
            <>
              <View style={styles.marketItem}>
                <Text style={styles.marketValue}>{moneyMarket.txns_count}</Text>
                <Text style={styles.marketTitle}>Interest Payments</Text>
              </View>
              <View style={styles.marketItem}>
                <Text style={styles.marketValue}>
                  {formatterHelper(
                    moneyMarket.interest_earned,
                    activeWallet?.coinSymbol,
                  )}
                </Text>
                <Text style={styles.marketTitle}>
                  Total Earnings In {activeWallet?.coinName}
                </Text>
              </View>
              <View style={styles.marketItem}>
                <Text style={styles.marketValue}>
                  {usdValueFormatter.format(moneyMarket.interest_earned_usd)}
                </Text>
                <Text style={styles.marketTitle}>
                  Total {activeWallet?.coinSymbol} Earnings In USD
                </Text>
              </View>
            </>
          ) : (
            <>
              {[...Array(3)].map((_, index) => (
                <View key={index} style={styles.marketItem}>
                  <SkeltonItem itemWidth={100} itemHeight={40} />
                  <SkeltonItem itemWidth={150} itemHeight={15} />
                </View>
              ))}
            </>
          )}
        </View>
      ) : (
        <VaultChart activeWallet={activeWallet} selectedApp={selectedApp} />
      )}
    </View>
  );
};

export default Overview;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  moneymarketContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  marketItem: {
    alignItems: 'center',
  },
  marketValue: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.TEXT_COLOR,
    fontSize: 32,
  },
  marketTitle: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.TEXT_COLOR,
    fontSize: 13,
    textAlign: 'center',
  },
});
