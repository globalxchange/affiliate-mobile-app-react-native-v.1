import axios from 'axios';
import React, {useEffect, useMemo, useRef, useState} from 'react';
import {
  Image,
  Keyboard,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import Dropdown from '../Dropdown';
import Actions from './Actions';
import TransactionList from './TransactionList';
import AppSelector from './AppSelector';
import WalletTypeList from './WalletTypeList';
import CurrencyList from './CurrencyList';
import Overview from './Overview';
import ActionTab from './ActionTab';
import BalanceView from '../VaultComponents/BalanceView';

const VaultFragmentView = ({
  activeTab,
  activeWallet,
  setSearchList,
  openSearchList,
  isSearchOpen,
  setSelectedApp,
  selectedApp,
  setVaultType,
  setShowBalanceIn,
  setSearchCallback,
  closeSearch,
  walletTypes,
  actionList,
  vaultType,
  setActiveTab,
  toggleExpand,
  isFragmentExpanded,
  onCloseActionTab,
  walletBalances,
  showBalanceIn,
  isActionBlurEnabled,
  setIsActionBlurEnabled,
}) => {
  const [isTypeFilterOpen, setIsTypeFilterOpen] = useState(false);
  const [typeFilter, setTypeFilter] = useState(typeFilterItems[0]);
  const [isStatusFilterOpen, setIsStatusFilterOpen] = useState(false);
  const [statusFilter, setStatusFilter] = useState(statusFilterItems[0]);
  const [appList, setAppList] = useState();
  const [isKeyBoardOpen, setIsKeyBoardOpen] = useState(false);

  const headerText = useRef('');
  const searchPlaceholder = useRef('');

  useEffect(() => {
    (async () => {
      const email = await AsyncStorageHelper.getLoginEmail();

      axios
        .get(`${GX_API_ENDPOINT}/gxb/apps/registered/user`, {
          params: {email},
        })
        .then(({data}) => {
          const rawData = data?.userApps || [];

          const list = rawData.map((item) => ({
            ...item,
            _id: item.profile_id,
            name: item.app_name,
            profile_img: item.app_icon,
          }));

          setAppList(list);
        });
    })();
    Keyboard.addListener('keyboardDidShow', onKeyBoardShow);
    Keyboard.addListener('keyboardDidHide', onKeyBoardHidden);
    return () => {
      Keyboard.removeListener('keyboardDidShow', onKeyBoardShow);
      Keyboard.removeListener('keyboardDidHide', onKeyBoardHidden);
    };
  }, []);

  useEffect(() => {
    if (typeFilter === 'All') {
      setStatusFilter(statusFilterItems[0]);
    }
  }, [typeFilter]);

  useEffect(() => {
    setTypeFilter(typeFilterItems[0]);
    setStatusFilter(statusFilterItems[0]);
  }, [activeWallet]);

  const onKeyBoardShow = () => {
    setIsKeyBoardOpen(true);
  };

  const onKeyBoardHidden = () => {
    setIsKeyBoardOpen(false);
  };

  const BalanceComponent = useMemo(
    () => (
      <BalanceView
        setActiveAction={setActiveTab}
        activeAction={activeTab}
        activeWallet={activeWallet}
        isKeyBoardOpen={isKeyBoardOpen}
        walletBalances={walletBalances}
        selectedApp={selectedApp}
        setSelectedApp={setSelectedApp}
        vaultType={vaultType}
        showBalanceIn={showBalanceIn}
        isActionBlurEnabled={isActionBlurEnabled}
        setIsActionBlurEnabled={setIsActionBlurEnabled}
      />
    ),
    [
      activeTab,
      activeWallet,
      isActionBlurEnabled,
      isKeyBoardOpen,
      selectedApp,
      setActiveTab,
      setIsActionBlurEnabled,
      setSelectedApp,
      showBalanceIn,
      vaultType,
      walletBalances,
    ],
  );

  const renderFragment = () => {
    headerText.current = '';
    searchPlaceholder.current = '';
    switch (activeTab?.name) {
      case 'Transactions':
        searchPlaceholder.current = 'Search Transaction';
        return (
          <TransactionList
            key={activeTab.time}
            activeWallet={activeWallet}
            statusFilter={statusFilter}
            typeFilter={typeFilter}
            isSearchOpen={isSearchOpen}
            setSearchList={setSearchList}
            selectedApp={selectedApp}
          />
        );
      case 'Actions':
        searchPlaceholder.current = 'Search Actions';
        return (
          <Actions
            openSearchList={openSearchList}
            key={activeTab.time}
            vaultType={vaultType}
            activeWallet={activeWallet}
            actionList={actionList}
            isSearchOpen={isSearchOpen}
            setSearchList={setSearchList}
            setSearchCallback={setSearchCallback}
            closeSearch={closeSearch}
          />
        );
      case 'AppSelector':
        headerText.current = 'Your Apps';
        searchPlaceholder.current = 'Search Your Apps';
        return (
          <AppSelector
            key={activeTab.time}
            activeWallet={activeWallet}
            appList={appList}
            setSelectedApp={setSelectedApp}
            isSearchOpen={isSearchOpen}
            setSearchList={setSearchList}
            setSearchCallback={setSearchCallback}
            closeSearch={closeSearch}
            openSearchList={openSearchList}
            onCloseActionTab={onCloseActionTab}
          />
        );

      case 'WalletType':
        headerText.current = 'Vaults';
        searchPlaceholder.current = 'Search Vaults';
        return (
          <WalletTypeList
            key={activeTab.time}
            walletTypes={walletTypes}
            setVaultType={setVaultType}
            isSearchOpen={isSearchOpen}
            setSearchList={setSearchList}
            setSearchCallback={setSearchCallback}
            closeSearch={closeSearch}
          />
        );
      case 'CoinSelector':
        headerText.current = 'Currencies';
        searchPlaceholder.current = 'Search Currencies';
        return (
          <CurrencyList
            key={activeTab.time}
            setConvertCurrecy={setShowBalanceIn}
            isSearchOpen={isSearchOpen}
            setSearchList={setSearchList}
            setSearchCallback={setSearchCallback}
            closeSearch={closeSearch}
            openSearchList={openSearchList}
            onCloseActionTab={onCloseActionTab}
          />
        );

      case 'Overview':
      default:
        return (
          <Overview
            key={activeTab.time}
            activeWallet={activeWallet}
            BalanceComponent={BalanceComponent}
            selectedApp={selectedApp}
          />
        );
    }
  };

  const onSearchButton = () => {
    openSearchList();
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.pinchContainer} onPress={toggleExpand}>
        <View style={styles.pichThumb} />
      </TouchableOpacity>
      <ActionTab
        setActiveTab={setActiveTab}
        activeTab={activeTab}
        toggleExpand={toggleExpand}
        isFragmentExpanded={isFragmentExpanded}
      />
      {activeTab?.name === 'Transactions' && (
        <View style={styles.headerContainer}>
          {headerText.current ? (
            <Text style={styles.header}>{headerText.current}</Text>
          ) : (
            <View style={styles.filterContainer}>
              <Dropdown
                items={typeFilterItems}
                activeItem={typeFilter}
                setActiveItem={setTypeFilter}
                onChange={setIsTypeFilterOpen}
                closeOnChangeValue={isStatusFilterOpen}
                style={{width: 90, justifyContent: 'center'}}
                rounded
              />
              <Dropdown
                isDisabled={typeFilter === 'All'}
                items={statusFilterItems}
                activeItem={statusFilter}
                setActiveItem={setStatusFilter}
                onChange={setIsStatusFilterOpen}
                closeOnChangeValue={isTypeFilterOpen}
                style={{width: 90, marginLeft: 10, justifyContent: 'center'}}
                rounded
              />
            </View>
          )}
          <TouchableOpacity
            onPress={onSearchButton}
            style={styles.searchButton}>
            <Image
              source={require('../../assets/search-modern.png')}
              resizeMode="contain"
              style={styles.searchIcon}
            />
          </TouchableOpacity>
        </View>
      )}
      {renderFragment()}
    </View>
  );
};

export default VaultFragmentView;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
  },
  headerContainer: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingBottom: 10,
  },
  header: {
    flex: 1,
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.TEXT_COLOR,
    fontSize: 20,
  },
  filterContainer: {
    flexDirection: 'row',
  },
  searchButton: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    zIndex: 2,
    backgroundColor: 'white',
    paddingHorizontal: 10,
    height: 30,
    marginLeft: 'auto',
    borderRadius: 6,
  },
  searchText: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.TEXT_COLOR,
    fontSize: 10,
    flex: 1,
  },
  searchIcon: {
    width: 14,
    height: 14,
  },
  pinchContainer: {
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 5,
  },
  pichThumb: {
    backgroundColor: ThemeData.BORDER_COLOR,
    height: 4,
    width: 25,
    borderRadius: 5,
  },
});

const typeFilterItems = ['All', 'Deposits', 'Withdrawals'];

const statusFilterItems = [
  'All',
  'Pending',
  'Processing',
  'Processed',
  'Terminated',
];
