/* eslint-disable react-native/no-inline-styles */
import axios from 'axios';
import React, {useEffect} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {useQuery} from 'react-query';
import {GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import {formatterHelper} from '../../utils';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import SkeltonItem from '../SkeltonItem';

const {width} = Dimensions.get('window');

const getLiquidBalance = async ({queryKey}) => {
  const [_key, coin] = queryKey;

  const email = await AsyncStorageHelper.getLoginEmail();

  const {
    data,
  } = await axios.get(
    `${GX_API_ENDPOINT}/coin/vault/service/user/total/funds/data/get`,
    {params: {email}},
  );

  const coinData = data?.crypto?.coins?.find((x) => x.coin === coin);

  return coinData || '';
};

const AppSelector = ({
  activeWallet,
  appList,
  setSelectedApp,
  isSearchOpen,
  setSearchList,
  setSearchCallback,
  closeSearch,
  openSearchList,
  onCloseActionTab,
}) => {
  const {data, status, error} = useQuery(
    [`appLiquidBalance-${activeWallet?.coinSymbol}`, activeWallet?.coinSymbol],
    getLiquidBalance,
  );

  useEffect(() => {
    setSearchList(data?.liquid || []);
    setSearchCallback((item) => {
      setSelectedApp(item);
      closeSearch();
    });
    return () => {
      setSearchList();
      setSearchCallback(() => {});
    };
  }, [data, isSearchOpen]);

  return (
    <View style={styles.container}>
      {status === 'loading' && (
        <View>
          <SkeltonItem
            itemWidth={200}
            itemHeight={20}
            style={{marginBottom: 10}}
          />
          <SkeltonItem itemWidth={200} itemHeight={10} />
          <SkeltonItem
            itemWidth={width - 60}
            itemHeight={60}
            style={{marginBottom: 10, marginTop: 20}}
          />
          <SkeltonItem
            itemWidth={width - 60}
            itemHeight={60}
            style={{marginBottom: 10}}
          />
          <SkeltonItem
            itemWidth={width - 60}
            itemHeight={60}
            style={{marginBottom: 10}}
          />
          <SkeltonItem
            itemWidth={width - 60}
            itemHeight={60}
            style={{marginBottom: 10}}
          />
        </View>
      )}
      {status === 'success' && (
        <>
          <View style={styles.headerContainer}>
            <View style={{flex: 1}}>
              <Text style={styles.headingText}>Change Display App</Text>
              <Text style={styles.headingSubText}>
                Total Liquid {activeWallet?.coinSymbol} Is{' '}
                {formatterHelper(
                  data?.liquidTotal || 0,
                  activeWallet?.coinSymbol,
                )}{' '}
                {activeWallet?.coinSymbol}
              </Text>
            </View>
            <TouchableOpacity
              style={[styles.searchButton, {marginRight: 10}]}
              onPress={onCloseActionTab}>
              <Image
                source={require('../../assets/add-colored.png')}
                style={[styles.searchIcon, {transform: [{rotate: '45deg'}]}]}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.searchButton}
              onPress={openSearchList}>
              <Image
                source={require('../../assets/search-modern.png')}
                style={styles.searchIcon}
              />
            </TouchableOpacity>
          </View>
          <FlatList
            data={data?.liquid}
            keyExtractor={(item) => item.app_code}
            showsVerticalScrollIndicator={false}
            renderItem={({item}) => (
              <TouchableOpacity
                style={styles.listItem}
                onPress={() => setSelectedApp(item)}>
                <Image source={{uri: item.app_icon}} style={styles.itemIcon} />
                <Text style={[styles.itemName]}>{item.appName}</Text>
                <Text style={styles.itemValue}>
                  {formatterHelper(
                    item.nativeValue || 0,
                    activeWallet?.coinSymbol,
                  )}{' '}
                  {activeWallet?.coinSymbol}
                </Text>
              </TouchableOpacity>
            )}
          />
        </>
      )}
    </View>
  );
};

export default AppSelector;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 30,
    paddingTop: 10,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  headingText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.TEXT_COLOR,
    fontSize: 17,
    marginBottom: 5,
  },
  headingSubText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.TEXT_COLOR,
    fontSize: 10,
  },
  searchButton: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    width: 35,
    height: 35,
    padding: 10,
  },
  searchIcon: {
    resizeMode: 'contain',
    flex: 1,
    width: undefined,
    height: undefined,
  },
  listItem: {
    flexDirection: 'row',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 25,
    marginBottom: 20,
    alignItems: 'center',
  },
  itemIcon: {
    width: 28,
    height: 28,
  },
  itemName: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.TEXT_COLOR,
    fontSize: 14,
    flex: 1,
    marginHorizontal: 10,
  },
  itemValue: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.TEXT_COLOR,
    fontSize: 14,
  },
});
