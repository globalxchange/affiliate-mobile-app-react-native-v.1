import React, {useContext, useEffect, useMemo, useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import {usdValueFormatter} from '../../utils';

const CurrencyList = ({
  setConvertCurrecy,
  setSearchList,
  isSearchOpen,
  setSearchCallback,
  closeSearch,
  onCloseActionTab,
  openSearchList,
}) => {
  const {cryptoTableData} = useContext(AppContext);
  const [isFiat, setIsFiat] = useState(true);

  useEffect(() => {
    setSearchList(cryptoTableData);
    setSearchCallback((item) => {
      setConvertCurrecy(item);
      closeSearch();
    });
    return () => {
      setSearchList();
      setSearchCallback(() => {});
    };
  }, [isSearchOpen]);

  const filteredList = useMemo(() => {
    const list = cryptoTableData?.filter(
      (item) => item.asset_type === (isFiat ? 'Fiat' : 'Crypto'),
    );

    return list || [];
  }, [isFiat, cryptoTableData]);

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <View style={{flex: 1}}>
          <Text style={styles.headingText}>Change Display Currency</Text>
          <Text
            onPress={() => setIsFiat((value) => !value)}
            style={styles.headingSubText}>
            <Text style={[isFiat || {fontFamily: ThemeData.FONT_BOLD}]}>
              Crypto
            </Text>{' '}
            |{' '}
            <Text style={[isFiat && {fontFamily: ThemeData.FONT_BOLD}]}>
              Fiat
            </Text>
          </Text>
        </View>
        <TouchableOpacity
          style={[styles.searchButton, {marginRight: 10}]}
          onPress={onCloseActionTab}>
          <Image
            source={require('../../assets/add-colored.png')}
            style={[styles.searchIcon, {transform: [{rotate: '45deg'}]}]}
          />
        </TouchableOpacity>
        <TouchableOpacity style={styles.searchButton} onPress={openSearchList}>
          <Image
            source={require('../../assets/search-modern.png')}
            style={styles.searchIcon}
          />
        </TouchableOpacity>
      </View>
      <FlatList
        data={filteredList}
        keyExtractor={(item) => item._id}
        showsVerticalScrollIndicator={false}
        renderItem={({item}) => (
          <TouchableOpacity
            onPress={() => {
              setConvertCurrecy(item);
              onCloseActionTab();
            }}
            style={styles.appItem}>
            <Image
              source={{uri: item.coinImage}}
              resizeMode="contain"
              style={styles.appIcon}
            />
            <Text style={styles.appName}>{item.coinName}</Text>
            <Text style={styles.usdValue}>
              {usdValueFormatter.format(item.usd_price)}
            </Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

export default CurrencyList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    marginTop: 10,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  headingText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.TEXT_COLOR,
    fontSize: 17,
    marginBottom: 5,
  },
  headingSubText: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: ThemeData.TEXT_COLOR,
    fontSize: 11,
  },
  searchButton: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    width: 35,
    height: 35,
    padding: 10,
  },
  searchIcon: {
    resizeMode: 'contain',
    flex: 1,
    width: undefined,
    height: undefined,
  },
  appItem: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingVertical: 20,
    paddingHorizontal: 20,
    marginBottom: 10,
  },
  appIcon: {
    width: 30,
    height: 30,
  },
  appName: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.TEXT_COLOR,
    flex: 1,
    marginLeft: 10,
  },
  usdValue: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.TEXT_COLOR,
  },
});
