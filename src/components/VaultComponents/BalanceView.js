/* eslint-disable react-native/no-inline-styles */
import {useNavigation, useRoute} from '@react-navigation/native';
import axios from 'axios';
import React, {useContext, useEffect, useMemo, useRef, useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Animated, {Clock, set, useCode} from 'react-native-reanimated';
import {APP_CODE, GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import {DepositContext} from '../../contexts/DepositContext';
import WithdrawalContext from '../../contexts/WithdrawalContext';
import PopupLayout from '../../layouts/PopupLayout';
import {formatterHelper, percentageFormatter} from '../../utils';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import ReanimatedTimingHelper from '../../utils/ReanimatedTimingHelper';
import SkeltonItem from '../SkeltonItem';

const BalanceView = ({
  activeWallet,
  isKeyBoardOpen,
  activeAction,
  setActiveAction,
  walletBalances,
  selectedApp,
  setSelectedApp,
  vaultType,
  showBalanceIn,
  isActionBlurEnabled,
  setIsActionBlurEnabled,
}) => {
  const {params} = useRoute();

  const {navigate} = useNavigation();

  const {isCustomNumPadOpen} = useContext(AppContext);

  const {isGXVaultSelectorExpanded} = useContext(DepositContext);
  const withdrawContext = useContext(WithdrawalContext);

  const [activeWalletBalance, setActiveWalletBalance] = useState('');
  const [isLoading, setIsLoading] = useState(false);

  const [isPopupOpen, setIsPopupOpen] = useState(false);
  const [selectedPopupData, setSelectedPopupData] = useState('');

  const [coinHistory, setCoinHistory] = useState();

  const buttonAnimation = useRef(new Animated.Value(1));
  const expandAnimation = useRef(new Animated.Value(1));

  useEffect(() => {
    if (walletBalances) {
      setIsLoading(false);
      const balanceObj = walletBalances.find(
        (x) => x.coinSymbol === activeWallet?.coinSymbol,
      );
      setActiveWalletBalance(balanceObj);
    } else {
      setIsLoading(true);
    }
  }, [walletBalances, activeWallet, showBalanceIn]);

  useCode(
    () =>
      isKeyBoardOpen || isCustomNumPadOpen
        ? [
            set(
              buttonAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ]
        : [
            set(
              buttonAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ],
    [isKeyBoardOpen, isCustomNumPadOpen],
  );

  useCode(
    () =>
      isGXVaultSelectorExpanded || withdrawContext.isGXVaultSelectorExpanded
        ? [
            set(
              expandAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ]
        : [
            set(
              expandAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ],
    [isGXVaultSelectorExpanded, withdrawContext.isGXVaultSelectorExpanded],
  );

  useEffect(() => {
    let balanceBlur = false;

    const actionsList = actions.map((action) => action.title);

    if (actionsList.includes(activeAction?.name)) {
      balanceBlur = true;
    }
    setIsActionBlurEnabled(balanceBlur);
  }, [activeAction]);

  useEffect(() => {
    (async () => {
      if (params?.type !== 'moneymarket') {
        setCoinHistory(null);
        const email = await AsyncStorageHelper.getLoginEmail();

        axios
          .get(`${GX_API_ENDPOINT}/coin/vault/service/user/vault/coin/stats`, {
            params: {
              email,
              coin: activeWallet?.coinSymbol,
              displayCurrency: showBalanceIn?.coinSymbol,
              app_code: APP_CODE,
            },
          })
          .then(({data}) => {
            // console.log({data});
            if (data.status) {
              const history = [
                {
                  time: '24 Hours',
                  change: data._24hrchange_percentage,
                  changeValue: data._24hr_holdings_change,
                },
                {
                  time: '7 Days',
                  change: data._7daychange_percentage,
                  changeValue: data._7day_holdings_change,
                },
                {
                  time: '30 Days',
                  change: data._30daychange_percentage,
                  changeValue: data._30day_holdings_change,
                },
              ];

              setCoinHistory(history);
            }
          })
          .catch((error) => {});
      }
    })();
  }, [showBalanceIn, activeWallet, params]);

  const onInfoButtonClick = (item) => {
    setSelectedPopupData(item);
    setIsPopupOpen(true);
  };

  const actions = [
    {
      displayTitle:
        selectedApp?.app_name || selectedApp?.appName || 'Assets.io',
      title: 'AppSelector',
      icon: selectedApp
        ? {uri: selectedApp?.app_icon}
        : require('../../assets/assets-io-icon-dark.png'),
      desc: `This Button Allows You To Control Which Of Your Registered App’s ${
        showBalanceIn?.coinName || activeWallet?.coinName || 'Bitcoin'
      } Vault You See.`,
      header: 'Select The App ',
    },
    {
      title: 'CoinSelector',
      displayTitle:
        showBalanceIn?.coinName || activeWallet?.coinName || 'Bitcoin',
      icon: {uri: showBalanceIn?.coinImage || activeWallet?.coinImage},
      desc: `This Button Allows You To Control The The Currency In Which You See Your ${
        showBalanceIn?.coinName || activeWallet?.coinName || 'Bitcoin'
      } Balance`,
      header: 'Select Display Currency',
    },
  ];

  const change = useMemo(() => activeWalletBalance?._24hrchange || 0, [
    activeWalletBalance,
  ]);

  return (
    <View style={[styles.container]}>
      {activeWallet ? (
        <View
          style={[
            styles.walletViewContainer,
            (isGXVaultSelectorExpanded ||
              withdrawContext.isGXVaultSelectorExpanded) && {opacity: 0},
          ]}>
          <View
            style={{
              opacity: isActionBlurEnabled ? 0.3 : 1,
              marginVertical: 10,
            }}>
            <View style={styles.balanceView}>
              {!isLoading ? (
                <>
                  <Text style={styles.wallerBalance}>
                    {formatterHelper(
                      params?.type === 'moneymarket'
                        ? activeWalletBalance?.coinValue_dc
                        : activeWalletBalance?.coinValue_DC,
                      showBalanceIn?.coinSymbol,
                    )}
                  </Text>
                  {/* <Text style={styles.walletCoin}>
                    {showBalanceIn?.coinSymbol}
                  </Text> */}
                </>
              ) : (
                <>
                  <SkeltonItem
                    itemHeight={50}
                    itemWidth={130}
                    style={{marginRight: 5}}
                  />
                  <SkeltonItem itemHeight={20} itemWidth={40} />
                </>
              )}
            </View>
          </View>
          <Animated.View style={[styles.actionsContainer]}>
            {actions.map((item, index) => (
              <TouchableOpacity
                key={item.title}
                onPress={() => setActiveAction({name: item.title})}>
                <View
                  style={[
                    styles.actionsButton,
                    index === actions.length - 1 && {borderRightWidth: 0},
                    isActionBlurEnabled &&
                      item.title !== activeAction?.name && {
                        opacity: 0.3,
                      },
                  ]}>
                  <Image
                    source={item.icon}
                    resizeMode="contain"
                    style={styles.buttonIcon}
                  />
                  <Text style={styles.actionText}>{item.displayTitle}</Text>
                </View>
                {/* <TouchableOpacity
                  style={styles.iButton}
                  onPress={() => onInfoButtonClick(item)}>
                  <Image
                    source={require('../../assets/i-icon.png')}
                    style={styles.iIcon}
                  />
                </TouchableOpacity> */}
              </TouchableOpacity>
            ))}
          </Animated.View>
          {params?.type === 'moneymarket' || (
            <>
              {!coinHistory ? (
                <SkeltonItem
                  style={{marginTop: 30}}
                  itemHeight={40}
                  itemWidth={300}
                />
              ) : (
                <View style={styles.coinMetaContainer}>
                  {coinHistory?.map((item) => (
                    <View key={item.time} style={styles.metaItem}>
                      <Text
                        style={[
                          styles.coinNativeValue,
                          item.change < 0 && styles.coinNativeDownValue,
                        ]}>
                        {formatterHelper(
                          item.changeValue,
                          showBalanceIn?.coinSymbol,
                        )}{' '}
                        {showBalanceIn?.coinSymbol === 'USD' ||
                          showBalanceIn?.coinSymbol}
                      </Text>
                      <View style={styles.percentageContainer}>
                        <View
                          style={[
                            styles.itemChangeSign,
                            change < 0
                              ? styles.itemChangeSignDown
                              : styles.itemChangeSignUp,
                          ]}
                        />
                        <Text style={[styles.itemChange]}>
                          {percentageFormatter.format(item.change || 0)} % In{' '}
                          {item.time}
                        </Text>
                      </View>
                    </View>
                  ))}
                </View>
              )}
            </>
          )}
        </View>
      ) : null}
      <PopupLayout
        isOpen={isPopupOpen}
        onClose={() => setIsPopupOpen(false)}
        headerTitle={selectedPopupData?.header}
        autoHeight
        noScrollView>
        <View style={styles.popupContainer}>
          <TouchableOpacity
            style={[styles.actionsButton, styles.popupButton]}
            disabled>
            <Image
              source={selectedPopupData.icon}
              resizeMode="contain"
              style={styles.buttonIcon}
            />
            <Text
              numberOfLines={1}
              style={[styles.buttonText, styles.buttonTextActive]}>
              {selectedPopupData.displayTitle || selectedPopupData.title}
            </Text>
            <TouchableOpacity style={styles.iButton} disabled>
              <Image
                source={require('../../assets/i-icon.png')}
                style={styles.iIcon}
              />
            </TouchableOpacity>
          </TouchableOpacity>
          <Text style={styles.popupDesc}>{selectedPopupData.desc}</Text>
          <TouchableOpacity
            style={styles.supportButton}
            onPress={() => {
              setIsPopupOpen(false);
              navigate('Support', {openUserChat: true});
            }}>
            <Text style={styles.supportButtonText}>Speak To Support</Text>
          </TouchableOpacity>
        </View>
      </PopupLayout>
    </View>
  );
};

export default BalanceView;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    paddingHorizontal: 20,
    overflow: 'hidden',
    justifyContent: 'center',
    marginBottom: 10,
  },
  walletViewContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  balanceView: {
    flexDirection: 'row',
    alignItems: 'baseline',
  },
  wallerBalance: {
    color: ThemeData.TEXT_COLOR,
    fontFamily: ThemeData.FONT_BOLD,
    fontSize: 35,
  },
  walletCoin: {
    fontFamily: ThemeData.FONT_BOLD,
    marginLeft: 5,
    fontSize: 13,
  },
  actionsContainer: {
    flexDirection: 'row',
  },
  actionsButton: {
    backgroundColor: 'white',
    justifyContent: 'center',
    zIndex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    overflow: 'hidden',
    borderRightWidth: 2,
    borderRightColor: ThemeData.BORDER_COLOR,
    paddingHorizontal: 10,
  },
  actionText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    marginLeft: 10,
  },
  actionsButtonActive: {
    borderColor: '#F2A900',
  },
  buttonIcon: {
    width: 16,
    height: 16,
  },
  buttonText: {
    textAlign: 'center',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
    color: '#5F6163',
    opacity: 0.6,
    maxWidth: 80,
  },
  buttonTextActive: {
    opacity: 1,
  },
  iButton: {
    width: 22,
    height: 22,
    padding: 5,
    position: 'absolute',
    top: 0,
    right: 0,
    zIndex: 2,
  },
  iIcon: {
    flex: 1,
    width: undefined,
    height: undefined,
    resizeMode: 'contain',
  },
  popupContainer: {
    marginBottom: -10,
  },
  popupButton: {
    flexGrow: 0,
    width: '50%',
    height: 40,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  popupDesc: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: '#5F6163',
    fontSize: 13,
    textAlign: 'center',
    lineHeight: 20,
    marginVertical: 20,
  },
  supportButton: {
    borderTopColor: ThemeData.BORDER_COLOR,
    borderTopWidth: 1,
    marginLeft: -30,
    marginRight: -30,
    paddingTop: 20,
  },
  supportButtonText: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_BOLD,
    color: '#5F6163',
    fontSize: 15,
  },
  coinMetaContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 30,
  },
  coinNativeValue: {
    fontFamily: ThemeData.FONT_MEDIUM,
    fontSize: 16,
    color: '#0E9347',
  },
  coinNativeDownValue: {
    color: '#D32027',
  },
  itemChangeSign: {
    width: 0,
    height: 0,
    borderStyle: 'solid',
    borderLeftColor: 'transparent',
    borderLeftWidth: 4,
    borderRightColor: 'transparent',
    borderRightWidth: 4,
    marginHorizontal: 3,
  },
  itemChangeSignDown: {
    borderTopColor: ThemeData.TEXT_COLOR,
    borderTopWidth: 6,
  },
  itemChangeSignUp: {
    borderBottomColor: '#0E9347',
    borderBottomWidth: 6,
  },
  itemChange: {
    fontFamily: ThemeData.FONT_NORMAL,
    marginRight: 5,
    fontSize: 10,
  },
  downValue: {
    color: '#D32027',
  },
  walletName: {
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 13,
    color: '#5F6163',
  },
  metaItem: {
    alignItems: 'center',
    flex: 1,
  },
  percentageContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
  },
});
