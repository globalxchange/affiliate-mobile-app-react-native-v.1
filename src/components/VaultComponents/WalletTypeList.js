import React, {useEffect} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';

const WalletTypeList = ({
  walletTypes,
  setVaultType,
  isSearchOpen,
  setSearchList,
  setSearchCallback,
  closeSearch,
}) => {
  useEffect(() => {
    setSearchList(walletTypes || LIST);
    setSearchCallback((item) => {
      setVaultType(item);
      closeSearch();
    });
    return () => {
      setSearchList();
      setSearchCallback(() => {});
    };
  }, [isSearchOpen, walletTypes]);

  return (
    <View style={styles.container}>
      <FlatList
        data={walletTypes || LIST}
        keyExtractor={(item) => item.title}
        showsVerticalScrollIndicator={false}
        renderItem={({item}) => (
          <TouchableOpacity
            onPress={() => setVaultType(item)}
            style={styles.appItem}>
            <Image
              source={item.icon}
              resizeMode="contain"
              style={styles.appIcon}
            />
            <Text style={styles.appName}>{item.title}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

export default WalletTypeList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    marginTop: 10,
  },
  appItem: {
    flexDirection: 'row',
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    paddingVertical: 20,
    paddingHorizontal: 20,
    marginBottom: 10,
  },
  appIcon: {
    width: 30,
    height: 30,
  },
  appName: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.TEXT_COLOR,
    flex: 1,
    marginLeft: 10,
  },
});

const LIST = [
  {
    title: 'Liquid',
    icon: require('../../assets/liquid-earnings-icon.png'),
  },
  {
    title: 'Bonds',
    icon: require('../../assets/bonds-icon.png'),
  },
];
