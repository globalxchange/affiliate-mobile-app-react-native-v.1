import axios from 'axios';
import React, {useContext, useEffect, useMemo, useState} from 'react';
import {
  Image,
  SectionList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {APP_CODE, GX_API_ENDPOINT} from '../../configs';
import ThemeData from '../../configs/ThemeData';
import {AppContext} from '../../contexts/AppContextProvider';
import {
  formatterHelper,
  getAssetData,
  usdValueFormatterWithoutSign,
} from '../../utils';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import SkeltonItem from '../SkeltonItem';
import Moment from 'moment-timezone';

const TransactionList = ({
  activeWallet,
  typeFilter,
  statusFilter,
  openTxnAudit = () => {},
  isSearchOpen,
  setSearchList,
  selectedApp,
}) => {
  const {cryptoTableData} = useContext(AppContext);

  const [transactionsList, setTransactionsList] = useState();
  const [filteredTxnList, setFilteredTxnList] = useState();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    getTransactions();
  }, [activeWallet, selectedApp]);

  useEffect(() => {
    setSearchList(transactionsList);
    return () => {
      setSearchList();
    };
  }, [isSearchOpen]);

  useEffect(() => {
    if (transactionsList) {
      if (typeFilter === 'All') {
        setFilteredTxnList(transactionsList);
      } else {
        const newList = [];

        let type = statusFilter.toLowerCase();

        transactionsList.forEach((item) => {
          const isDeposit = typeFilter === 'Deposits';

          const currentStatus =
            type === 'all' ? item.status.toLowerCase() : type;

          if (
            item.deposit === isDeposit &&
            item.status.toLowerCase() === currentStatus
          ) {
            newList.push(item);
          }
        });

        setFilteredTxnList(newList);
      }
    }
  }, [typeFilter, statusFilter, transactionsList]);

  const getTransactions = async () => {
    setIsLoading(true);

    const profileId = await AsyncStorageHelper.getProfileId();

    const postData = {
      app_code: selectedApp?.app_code || APP_CODE,
      profile_id: selectedApp?.profile_id || profileId,
      coin: activeWallet.coinSymbol,
    };

    axios
      .post(`${GX_API_ENDPOINT}/coin/vault/service/txns/get`, postData)
      .then((resp) => {
        const {data} = resp;
        if (data.status) {
          const rawData = data.txns || [];

          const txns = rawData.map((txn) => ({
            ...txn,
            name: txn._id,
            email: ` ${formatterHelper(txn.amount, txn.coin)} ${txn.coin}`,
            profile_img: getAssetData(txn.coin, cryptoTableData)?.coinImage,
          }));

          setTransactionsList(txns);
        }
        setIsLoading(false);
        // console.log('TXN List', data.txns);
      })
      .catch((error) => console.log('Error getting txn list', error));
  };

  const groupedTxns = useMemo(() => {
    let tempList = [...(filteredTxnList || [])];
    const addedList = [];
    const sortedList = [];

    tempList.forEach((tmpItem) => {
      if (!addedList.includes(tmpItem)) {
        const date = Moment(tmpItem.timestamp).startOf('day');
        const subList = [];
        addedList.push(tmpItem);
        subList.push(tmpItem);

        tempList.forEach((item) => {
          if (!addedList.includes(item)) {
            const itemDate = Moment(item.timestamp);
            if (date.isSame(itemDate, 'day')) {
              addedList.push(item);
              subList.push(item);
            }
          }
        });

        let title = date.format('dddd MMMM Do YYYY');
        if (date.isSame(new Date(), 'day')) {
          title = 'Today';
        } else if (date.diff(new Date(), 'day') === -1) {
          title = 'Yesterday';
        }

        sortedList.push({
          title,
          data: subList,
        });
      }
    });

    return sortedList;
  }, [filteredTxnList]);

  return (
    <View style={styles.container}>
      {!isLoading ? (
        <SectionList
          style={styles.list}
          showsVerticalScrollIndicator={false}
          sections={groupedTxns}
          keyExtractor={(item, index) => item + index}
          renderSectionHeader={({section: {title}}) => (
            <View style={styles.transactionGroup}>
              <Text style={styles.dayHeader}>{title}</Text>
            </View>
          )}
          renderItem={({item}) => (
            <TouchableOpacity
              style={[styles.txnItem]}
              onPress={() => openTxnAudit(item)}>
              <Image
                style={styles.txnIcons}
                source={{uri: activeWallet.coinImage}}
                resizeMode="contain"
              />
              <View style={styles.nameContainer}>
                <Text numberOfLines={1} style={styles.name}>
                  {`${activeWallet?.coinName} ${
                    item.deposit ? 'Credit' : 'Debit'
                  }`}
                </Text>
                <Text style={styles.date}>{item.date}</Text>
              </View>
              <View style={styles.metaContainer}>
                <Text
                  style={[
                    styles.value,
                    item.deposit ? {color: '#59C36A'} : {color: '#D80027'},
                  ]}>
                  {item.deposit ? '+' : '-'}
                  {activeWallet.coinSymbol === 'USDT'
                    ? usdValueFormatterWithoutSign.format(item.amount)
                    : formatterHelper(item.amount, activeWallet.coinSymbol)}
                </Text>
                <Text style={[styles.updatedBalance]}>
                  Updated Balance :{' '}
                  {activeWallet.coinSymbol === 'USDT'
                    ? usdValueFormatterWithoutSign.format(item.updated_balance)
                    : formatterHelper(
                        item.updated_balance,
                        activeWallet.coinSymbol,
                      )}
                </Text>
              </View>
            </TouchableOpacity>
          )}
          ListEmptyComponent={
            <View style={styles.emptyContainer}>
              <Text style={styles.emptyText}>
                {`No${statusFilter !== 'All' ? ` ${statusFilter}` : ''}${
                  typeFilter !== 'All' ? ` ${typeFilter}` : ''
                } ${activeWallet.coinSymbol} Transactions Found`}
              </Text>
            </View>
          }
        />
      ) : (
        <SkeltonAnimation />
      )}
    </View>
  );
};

const SkeltonAnimation = () => {
  return (
    <View style={styles.loadingContainer}>
      {Array(5)
        .fill(1)
        .map((_, index) => (
          <View key={index} style={styles.txnItem}>
            <SkeltonItem itemWidth={30} style={{borderRadius: 15}} />
            <View style={styles.nameContainer}>
              <SkeltonItem
                itemWidth={100}
                itemHeight={10}
                style={styles.name}
              />
              <SkeltonItem itemWidth={70} itemHeight={8} style={styles.value} />
            </View>
            <View style={styles.metaContainer}>
              <SkeltonItem itemWidth={80} itemHeight={8} style={styles.date} />
              <SkeltonItem
                itemWidth={40}
                itemHeight={8}
                style={{marginLeft: 'auto', marginTop: 5}}
              />
            </View>
          </View>
        ))}
    </View>
  );
};

export default TransactionList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {flex: 1},
  txnItem: {
    flexDirection: 'row',
    paddingHorizontal: 25,
    paddingVertical: 25,
    backgroundColor: 'white',
    marginBottom: 10,
    alignItems: 'center',
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginHorizontal: 20,
    borderRadius: 10,
  },
  txnIcons: {
    width: 30,
    height: 30,
  },
  nameContainer: {flexGrow: 1, width: 0, paddingHorizontal: 15},
  name: {fontFamily: 'Montserrat-Bold', color: '#001D41'},
  value: {
    fontFamily: 'Montserrat-Bold',
    color: '#5F6163',
    fontSize: 14,
    marginTop: 5,
    textAlign: 'right',
  },
  metaContainer: {},
  date: {fontFamily: 'Montserrat', fontSize: 10},
  updatedBalance: {
    textAlign: 'right',
    fontFamily: ThemeData.FONT_NORMAL,
    fontSize: 9,
    marginTop: 5,
  },
  loadingContainer: {
    flex: 1,
    // backgroundColor: '#fff',
    overflow: 'hidden',
  },
  loadingText: {marginTop: 10, fontFamily: 'Montserrat'},
  emptyContainer: {
    flex: 1,
    paddingHorizontal: 35,
    marginTop: 40,
  },
  emptyText: {
    fontFamily: 'Montserrat-Bold',
    color: ThemeData.TEXT_COLOR,
    textAlign: 'center',
    fontSize: 16,
  },
  transactionGroup: {
    padding: 15,
    backgroundColor: 'white',
  },
  dayHeader: {
    fontFamily: 'Montserrat-SemiBold',
    color: ThemeData.TEXT_COLOR,
    fontSize: 12,
  },
});
