/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  Dimensions,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import ExpandIcon from '../../assets/VectorIcons/expand-triple-icon.svg';

const {width} = Dimensions.get('window');

const ActionTab = ({
  setActiveTab,
  activeTab,
  isDiabled,
  toggleExpand,
  isFragmentExpanded,
}) => {
  return (
    <View style={[styles.container, isDiabled && {opacity: 0.4}]}>
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        {TABS.map((tab) => (
          <TouchableOpacity
            disabled={isDiabled}
            style={[styles.tabButton]}
            key={tab}
            onPress={() => setActiveTab({name: tab, time: Date.now()})}>
            <Text
              style={[
                styles.tabName,
                activeTab.name === tab && {
                  fontSize: 15,
                  opacity: 1,
                  fontFamily: ThemeData.FONT_BOLD,
                },
              ]}>
              {tab}
            </Text>
          </TouchableOpacity>
        ))}
      </ScrollView>
    </View>
  );
};

export default ActionTab;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  tabButton: {
    paddingVertical: 15,
    borderBottomColor: '#F2A900',
    width: width / 3,
  },
  tabName: {
    textAlign: 'center',
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.TEXT_COLOR,
    opacity: 0.5,
    fontSize: 12,
  },
  expandToggle: {
    justifyContent: 'center',
    paddingHorizontal: 10,
    borderLeftColor: ThemeData.BORDER_COLOR,
    borderLeftWidth: 1,
  },
});

const TABS = ['Overview', 'Transactions', 'Actions'];
