import React, {useContext, useEffect, useState} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import {DepositContext} from '../../contexts/DepositContext';
import WalletTransfer from '../WalletDeposit/WalletTransfer';
import BTCAddressPopup from '../BTCAddressPopup';
import ExternalWithdrawSheet from '../ExternalWithdrawSheet';

const Actions = ({
  actionList,
  vaultType,
  isSearchOpen,
  setSearchList,
  setSearchCallback,
  closeSearch,
  openSearchList,
}) => {
  const {activeWallet} = useContext(DepositContext);

  const [activeView, setActiveView] = useState('');
  const [isBTCModalOpen, setIsBTCModalOpen] = useState(false);
  const [isWithdrawOpen, setIsWithdrawOpen] = useState(false);

  useEffect(() => {
    setSearchList(actions?.list || LIST);
    setSearchCallback((item) => {
      setActiveView(item);
      closeSearch();
    });
    return () => {
      setSearchList();
      setSearchCallback(() => {});
    };
  }, [actions, isSearchOpen]);

  const actions = actionList?.find(
    (x) => x.title === (vaultType?.title || 'Liquid'),
  );

  if (activeView?.title === 'View Address') {
    return <WalletTransfer />;
  }

  const onViewAddressClick = () => {
    if (activeWallet?.coinSymbol === 'BTC') {
      setIsBTCModalOpen(true);
    }
  };

  const onWithdrawSheetOpen = () => {
    setIsWithdrawOpen(true);
  };

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.vaultName}>
          Which Action Would You Like To Perform?
        </Text>
        <TouchableOpacity style={styles.searchButton} onPress={openSearchList}>
          <Image
            style={styles.searchIcon}
            source={require('../../assets/search-modern.png')}
          />
        </TouchableOpacity>
      </View>
      <FlatList
        data={actions?.list || LIST}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item) => item.title}
        renderItem={({item}) => (
          <TouchableOpacity
            disabled={item.disabled}
            onPress={
              item.onPress
                ? item.onPress
                : () =>
                    item?.title === 'View Address'
                      ? onViewAddressClick()
                      : item?.title === 'External Transfer'
                      ? onWithdrawSheetOpen()
                      : setActiveView(item)
            }>
            <View style={[styles.ationItem, item.disabled && {opacity: 0.3}]}>
              <Image
                style={styles.ationItemImage}
                source={item.icon}
                resizeMode="contain"
              />
              <Text style={styles.actionName}>{item.title}</Text>
            </View>
          </TouchableOpacity>
        )}
      />
      <BTCAddressPopup
        isOpen={isBTCModalOpen}
        onClose={() => setIsBTCModalOpen(false)}
        activeWallet={activeWallet}
      />
      <ExternalWithdrawSheet
        isOpen={isWithdrawOpen}
        onClose={() => setIsWithdrawOpen(false)}
      />
    </View>
  );
};

export default Actions;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
  },
  ationItem: {
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 25,
    marginBottom: 10,
  },
  ationItemImage: {
    width: 30,
    height: 30,
  },
  actionName: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.TEXT_COLOR,
    marginLeft: 10,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20,
  },
  headerText: {
    fontFamily: ThemeData.FONT_BOLD,
    color: ThemeData.TEXT_COLOR,
    fontSize: 22,
  },
  vaultName: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.TEXT_COLOR,
    fontSize: 12,
  },
  searchButton: {
    width: 35,
    height: 35,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    padding: 10,
  },
  searchIcon: {
    flex: 1,
    width: undefined,
    height: undefined,
  },
});

const LIST = [
  {
    title: 'Withdraw Interest',
    icon: require('../../assets/withdraw-intrest-icon.png'),
  },
];
