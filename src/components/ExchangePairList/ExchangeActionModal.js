import {
  Modal,
  StyleSheet,
  View,
  Image,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import React from 'react';
import {useNavigation, useRoute} from '@react-navigation/native';
import ThemeData from '../../configs/ThemeData';

const ExchangeActionModal = ({pair, setPair, base, refetchPairs}) => {
  const {navigate} = useNavigation();
  const {
    params: {selectedBrand},
  } = useRoute();
  return (
    <Modal
      animationType="fade"
      transparent
      visible={Boolean(pair)}
      hardwareAccelerated
      statusBarTranslucent
      onDismiss={() => setPair('')}
      onRequestClose={() => setPair('')}>
      <TouchableOpacity
        activeOpacity={1}
        style={styles.overlay}
        onPress={() => setPair('')}
        onPressOut={() => {}}>
        <TouchableWithoutFeedback>
          <View style={styles.modalContent}>
            <Image source={selectedBrand.icon} style={styles.icon} />
            <Text style={styles.question}>What Would You Like To Do To </Text>
            <Text style={styles.pair}>{pair}?</Text>
            <TouchableOpacity style={styles.btnAction} onPress={() => {}}>
              <Text style={styles.btnText}>Analyze Exchange Rate</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.btnAction}
              onPress={() => {
                navigate('ExchangeFeeUpdateScreen', {
                  selectedBrand,
                  pair,
                  refetchPairs,
                });
                setPair('');
              }}>
              <Text style={styles.btnText}>Change Fee For This Pair</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btnAction} onPress={() => {}}>
              <Text style={styles.btnText}>
                Change Fee For All {base} Pairs
              </Text>
            </TouchableOpacity>
          </View>
        </TouchableWithoutFeedback>
      </TouchableOpacity>
    </Modal>
  );
};

export default ExchangeActionModal;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    zIndex: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContent: {
    zIndex: 1,
    width: '85%',
    maxHeight: 500,
    backgroundColor: 'white',
    borderRadius: 50,
    padding: 40,
    alignItems: 'center',
  },
  icon: {
    height: 50,
    width: '80%',
    resizeMode: 'contain',
  },
  question: {
    marginTop: 20,
    fontSize: 16,
    textAlign: 'center',
    fontFamily: ThemeData.FONT_MEDIUM,
    color: ThemeData.TEXT_COLOR,
  },
  pair: {
    marginTop: 10,
    marginBottom: 15,
    fontSize: 18,
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.TEXT_COLOR,
  },
  btnAction: {
    width: '100%',
    height: 70,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    marginVertical: 10,
    borderRadius: 10,
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  btnText: {
    fontSize: 15,
    fontFamily: ThemeData.FONT_MEDIUM,
    color: ThemeData.TEXT_COLOR,
  },
});
