import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {usdValueFormatterWithoutSign} from '../../utils';
import ThemeData from '../../configs/ThemeData';
import {Fragment} from 'react';
import ExchangeActionModal from './ExchangeActionModal';
import {useState} from 'react';

const ExchangePairList = ({pairsObject, baseCoin, search, refetch}) => {
  const [pair, setPair] = useState('');
  return (
    <Fragment>
      <FlatList
        style={styles.listView}
        contentContainerStyle={styles.listViewIn}
        data={Object.keys(pairsObject).filter((itm) =>
          `${itm}/${baseCoin}`.toLowerCase().includes(search.toLowerCase()),
        )}
        keyExtractor={(item) => item}
        showsHorizontalScrollIndicator={false}
        renderItem={({item}) => (
          <TouchableOpacity
            onPress={() => setPair(`${item}/${baseCoin}`)}
            style={[styles.pairItm]}>
            <Text style={styles.pair}>
              {item}/{baseCoin}
            </Text>
            <Text style={styles.pair}>
              {usdValueFormatterWithoutSign.format(pairsObject[item])}%
            </Text>
          </TouchableOpacity>
        )}
      />
      <ExchangeActionModal
        pair={pair}
        setPair={setPair}
        base={pair.split('/')[1]}
        refetchPairs={refetch}
      />
    </Fragment>
  );
};

export default ExchangePairList;

const styles = StyleSheet.create({
  listView: {
    flex: 1,
  },
  pairItm: {
    height: 90,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: ThemeData.BORDER_COLOR,
    borderBottomWidth: 1,
    marginHorizontal: 15,
  },
  pair: {
    fontSize: 18,
    fontFamily: ThemeData.ROBOTO_MEDIUM,
    color: ThemeData.TEXT_COLOR,
  },
});
