import React, {useContext, useEffect, useState} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Clipboard from '@react-native-community/clipboard';
import {WToast} from 'react-native-smart-tip';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../configs';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import {AppContext} from '../../contexts/AppContextProvider';
import LoadingAnimation from '../LoadingAnimation';
import BottomSheetLayout from '../../layouts/BottomSheetLayout';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

const BrokerSyncConfigure = () => {
  const {bottom} = useSafeAreaInsets();

  const {userName, setLoginData} = useContext(AppContext);

  const [isOpen, setIsOpen] = useState(false);
  const [isEditorOpen, setIsEditorOpen] = useState(false);
  const [isCopied, setIsCopied] = useState(false);
  const [codeInput, setCodeInput] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);

  const code = userName;

  useEffect(() => {
    if (!isOpen) {
      resetState();
    }
  }, [isOpen]);

  const resetState = () => {
    setIsEditorOpen(false);
    setCodeInput('');

    setIsSuccess(false);
  };

  const copyHandler = () => {
    Clipboard.setString(code);
    setIsCopied(true);
    setTimeout(() => setIsCopied(false), 20000);
  };

  const updateUserDetails = (email) => {
    Axios.get(`${GX_API_ENDPOINT}/user/details/get`, {
      params: {email},
    }).then((res) => {
      const {data} = res;

      // console.log('Data', data);
      if (data.status) {
        AsyncStorageHelper.setUserName(data.user.username);
        setLoginData(data.user.username, true, data.user.profile_img);
      }
    });
  };

  const updateCode = async () => {
    const codeTrimmed = codeInput.trim();

    if (!codeTrimmed) {
      return WToast.show({
        data: 'Please Input A Code',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();
    const accessToken = await AsyncStorageHelper.getAccessToken();

    const postData = {
      email,
      accessToken,
      field: 'username',
      value: codeTrimmed,
    };

    // console.log('updateCode postData', postData);

    Axios.post(`${GX_API_ENDPOINT}/user/details/edit`, postData)
      .then((resp) => {
        const {data} = resp;

        // console.log('updateCode Resp', data);

        if (data.status) {
          updateUserDetails(email);
          setIsSuccess(true);
          // WToast.show({
          //   data: data.message || 'AffiliateSync Code Updated',
          //   position: WToast.position.TOP,
          // });
        } else {
          WToast.show({
            data: data.message || 'Error on updating AffiliateSync Code',
            position: WToast.position.TOP,
          });
        }
      })
      .catch((error) => {
        WToast.show({
          data: 'Error on updating AffiliateSync Code',
          position: WToast.position.TOP,
        });
        console.log('Error on updating AffiliateSync Code', error);
      })
      .finally(() => setIsLoading(false));
  };

  return (
    <>
      <TouchableOpacity
        style={[
          styles.button,
          {
            paddingBottom: bottom,
            marginBottom: -bottom,
            height: bottom + 55,
          },
        ]}
        onPress={() => setIsOpen(true)}>
        <Text style={styles.buttonText}>Configure AffiliateSync</Text>
      </TouchableOpacity>
      <BottomSheetLayout
        isOpen={isOpen}
        onClose={() => setIsOpen(false)}
        reactToKeyboard>
        <View style={styles.fragmentContainer}>
          <Image
            source={require('../../assets/broker-sync.png')}
            style={styles.headerImage}
            resizeMode="contain"
          />
          {isSuccess ? (
            <View style={styles.categorySelector}>
              <View style={styles.listContainer}>
                <Text style={styles.successText}>
                  Congratulations Your AffiliateSync Code Has Been Updated To
                </Text>
                <Text style={styles.newCode}>{codeInput}</Text>
              </View>
              <TouchableOpacity
                style={styles.button}
                onPress={() => setIsOpen(false)}>
                <Text style={styles.buttonText}>Close</Text>
              </TouchableOpacity>
            </View>
          ) : isEditorOpen ? (
            <View style={styles.categorySelector}>
              {isLoading ? (
                <View style={styles.loadingContainer}>
                  <LoadingAnimation />
                </View>
              ) : (
                <View style={styles.listContainer}>
                  <Text style={styles.title}>Enter New Code</Text>
                  <View style={styles.codeEditor}>
                    <TextInput
                      placeholder="Ex. Hamburger"
                      placeholderTextColor="#878788"
                      style={styles.input}
                      value={codeInput}
                      onChangeText={(text) => setCodeInput(text)}
                      returnKeyType="done"
                    />
                    <TouchableOpacity
                      style={styles.nextButton}
                      onPress={updateCode}>
                      <Image
                        source={require('../../assets/next-forward-icon-white.png')}
                        resizeMode="contain"
                        style={styles.copyIcon}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              )}
            </View>
          ) : (
            <View style={styles.categorySelector}>
              <View style={styles.listContainer}>
                <Text style={styles.title}>Current AffiliateSync Code</Text>
                <View style={styles.codeContainer}>
                  <Text style={styles.code}>{code}</Text>
                  <TouchableOpacity onPress={isCopied ? null : copyHandler}>
                    <Image
                      source={
                        isCopied
                          ? require('../../assets/check-icon-dark.png')
                          : require('../../assets/copy-icon.png')
                      }
                      resizeMode="contain"
                      style={styles.copyIcon}
                    />
                  </TouchableOpacity>
                </View>
              </View>
              <TouchableOpacity
                style={styles.button}
                onPress={() => setIsEditorOpen(true)}>
                <Text style={styles.buttonText}>Edit AffiliateSync Code</Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </BottomSheetLayout>
    </>
  );
};

export default BrokerSyncConfigure;

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 55,
    backgroundColor: '#08152D',
  },
  buttonText: {
    fontFamily: 'Montserrat-SemiBold',
    color: 'white',
  },
  fragmentContainer: {
    backgroundColor: 'white',
    paddingTop: 30,
  },
  categorySelector: {},
  headerImage: {
    height: 55,
    width: 200,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
  listContainer: {
    paddingHorizontal: 30,
    paddingVertical: 70,
  },
  title: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
  },
  codeContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#EBEBEB',
    borderRadius: 8,
    paddingHorizontal: 20,
    paddingVertical: 20,
    marginTop: 15,
  },
  code: {
    flex: 1,
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 16,
  },
  copyIcon: {
    height: 22,
    width: 22,
  },
  codeEditor: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#EBEBEB',
    borderRadius: 8,
    marginTop: 15,
    overflow: 'hidden',
    height: 60,
  },
  input: {
    flex: 1,
    paddingHorizontal: 20,
    fontFamily: 'Montserrat',
    color: '#08152D',
  },
  nextButton: {
    backgroundColor: '#08152D',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    width: 60,
  },
  loadingContainer: {
    justifyContent: 'center',
    height: 230,
  },
  successText: {
    textAlign: 'center',
    fontFamily: 'Montserrat',
    color: '#08152D',
    fontSize: 16,
  },
  newCode: {
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold',
    color: '#08152D',
    fontSize: 20,
    marginTop: 5,
  },
});
