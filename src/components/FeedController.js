import React, {useRef, useState} from 'react';
import {Dimensions, Image, StyleSheet, Text, View} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import {animatedStyles, scrollInterpolator} from '../utils/CarouselAnimation';
import LoadingAnimation from './LoadingAnimation';

const {width} = Dimensions.get('window');

const SLIDER_WIDTH = width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.5);
const ITEM_HEIGHT = Math.round((ITEM_WIDTH * 3) / 6);

const FeedController = ({carouselData, setActiveItem}) => {
  const [currentPos, setCurrentPos] = useState(0);

  const carouselRef = useRef();

  const onSnapToItem = (index) => {
    setCurrentPos(index);
    if (carouselData) {
      setActiveItem(carouselData[index]);
    }
  };

  return (
    <View>
      {carouselData ? (
        <Carousel
          ref={carouselRef}
          data={carouselData}
          sliderWidth={SLIDER_WIDTH}
          itemWidth={ITEM_WIDTH}
          containerCustomStyle={styles.carouselContainer}
          inactiveSlideShift={0}
          onSnapToItem={onSnapToItem}
          scrollInterpolator={scrollInterpolator}
          slideInterpolatedStyle={animatedStyles}
          useScrollView={true}
          loop
          inactiveSlideOpacity={0.5}
          inactiveSlideScale={0.8}
          renderItem={({item}) => (
            <View style={styles.itemContainer}>
              <Text style={styles.itemLabel}>{item.title}</Text>
            </View>
          )}
        />
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation height={70} width={70} />
        </View>
      )}
    </View>
  );
};

export default FeedController;

const styles = StyleSheet.create({
  carouselContainer: {
    marginTop: 20,
  },
  itemContainer: {
    zIndex: 3,
  },
  itemLabel: {
    color: '#08152D',
    fontSize: 20,
    fontFamily: 'Montserrat-Bold',
    textAlign: 'center',
    marginTop: 5,
  },
  loadingContainer: {
    height: ITEM_HEIGHT,
    justifyContent: 'center',
  },
});
