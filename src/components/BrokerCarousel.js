import React, {useState, useRef, useEffect} from 'react';
import {StyleSheet, Text, View, Dimensions, Image} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import {scrollInterpolator, animatedStyles} from '../utils/CarouselAnimation';
import LoadingAnimation from './LoadingAnimation';
import Axios from 'axios';
import FastImage from 'react-native-fast-image';
import {getUriImage} from '../utils';

const {width} = Dimensions.get('window');

const SLIDER_WIDTH = width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.5);
const ITEM_HEIGHT = Math.round((ITEM_WIDTH * 3) / 6);

const BrokerCarousel = ({setActiveCarousel}) => {
  const [currentPos, setCurrentPos] = useState(0);

  const [carouselData, setCarouselData] = useState();

  const carouselRef = useRef();

  useEffect(() => {
    Axios.get('https://teller2.apimachine.com/admin/allBankers')
      .then((resp) => {
        const {data} = resp;
        if (data.status) {
          setCarouselData(data.data);
          setActiveCarousel(data.data[0]);
        } else {
          setCarouselData([]);
        }
      })
      .catch((error) => {
        setCarouselData([]);
      });
  }, []);

  const onSnapToItem = (index) => {
    setCurrentPos(index);
    if (carouselData) {
      setActiveCarousel(carouselData[index]);
    }
  };

  return (
    <View>
      {carouselData ? (
        <Carousel
          ref={carouselRef}
          data={carouselData}
          sliderWidth={SLIDER_WIDTH}
          itemWidth={ITEM_WIDTH}
          containerCustomStyle={styles.carouselContainer}
          inactiveSlideShift={0}
          onSnapToItem={onSnapToItem}
          scrollInterpolator={scrollInterpolator}
          slideInterpolatedStyle={animatedStyles}
          useScrollView={true}
          loop
          inactiveSlideOpacity={0.5}
          inactiveSlideScale={0.8}
          renderItem={({item}) => (
            <View style={styles.itemContainer}>
              <View
                style={[
                  styles.imageContainer,
                  {backgroundColor: `#${item.colorCode}`},
                ]}>
                <FastImage
                  style={styles.carouselImage}
                  source={{uri: getUriImage(item.profilePicWhitePNG)}}
                  resizeMode="contain"
                />
              </View>

              <Text style={styles.itemLabel}>{item.displayName}</Text>
            </View>
          )}
        />
      ) : (
        <View style={styles.loadingContainer}>
          <LoadingAnimation height={70} width={70} />
        </View>
      )}
    </View>
  );
};

export default BrokerCarousel;

const styles = StyleSheet.create({
  carouselContainer: {
    marginTop: 10,
  },
  itemContainer: {
    zIndex: 3,
  },
  imageContainer: {
    width: ITEM_WIDTH,
    height: ITEM_HEIGHT,
    overflow: 'hidden',
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
  },
  carouselImage: {
    height: 50,
    width: 50,
  },
  itemLabel: {
    color: '#08152D',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
    marginTop: 5,
  },
  counter: {
    marginTop: 25,
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  loadingContainer: {
    height: ITEM_HEIGHT,
    justifyContent: 'center',
  },
});
