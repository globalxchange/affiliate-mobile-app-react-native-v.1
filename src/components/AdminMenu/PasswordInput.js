import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import ThemeData from '../../configs/ThemeData';
import {WToast} from 'react-native-smart-tip';
import Axios from 'axios';
import {GX_API_ENDPOINT} from '../../configs';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import {useNavigation} from '@react-navigation/native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {faEye, faEyeSlash} from '@fortawesome/free-solid-svg-icons';

const PasswordInput = ({setIsLoading, onClose}) => {
  const {navigate} = useNavigation();

  const [pinInput, setPinInput] = useState('');
  const [hidePass, setHidePass] = useState(true);

  const onProceedClick = async () => {
    if (!pinInput || pinInput.length < 4) {
      return WToast.show({
        data: 'Please Enter The PIN',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    const postData = {
      email,
      token,
      group_id: '66me7fdkhxsbtur',
      pin: pinInput,
    };

    Axios.post(`${GX_API_ENDPOINT}/gxb/apps/user/validate/group/pin`, postData)
      .then(({data}) => {
        // console.log('onProceedClick', data);
        if (data.status) {
          navigate('AdminPage');
          onClose();
        } else {
          WToast.show({data: data.message, position: WToast.position.TOP});
        }

        if (__DEV__) {
          navigate('AdminPage');
          onClose();
        }
      })
      .catch((error) => {
        WToast.show({
          data: 'Please Check Your Network Connection',
          position: WToast.position.TOP,
        });
        console.log('Error on Checking PIN', error);
      })
      .finally(() => setIsLoading(false));
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Please Enter Your OnHold Pin To Proceed</Text>
      <View style={styles.pinInputContainer}>
        <TextInput
          placeholder="PIN Code"
          style={styles.otpInput}
          value={pinInput}
          onChangeText={(text) => setPinInput(text)}
          keyboardType="decimal-pad"
          returnKeyType="done"
          maxLength={4}
          placeholderTextColor="#878788"
          secureTextEntry={hidePass}
        />
        <TouchableOpacity onPress={() => setHidePass(!hidePass)}>
          <FontAwesomeIcon
            icon={hidePass ? faEye : faEyeSlash}
            color={ThemeData.APP_MAIN_COLOR}
            size={22}
          />
        </TouchableOpacity>
      </View>
      <TouchableOpacity style={[styles.proceedButton]} onPress={onProceedClick}>
        <Text style={styles.text}>Proceed</Text>
      </TouchableOpacity>
    </View>
  );
};

export default PasswordInput;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 30,
  },
  header: {
    fontFamily: ThemeData.FONT_NORMAL,
    color: '#08152D',
    marginTop: 40,
  },
  underlineStyleBase: {
    borderWidth: 1,
    color: '#08152D',
  },
  underlineStyleHighLighted: {
    borderColor: '#999C9A',
    color: '#08152D',
  },
  pinInputContainer: {
    flexDirection: 'row',
    marginBottom: 40,
    marginTop: 10,
    alignItems: 'center',
  },
  otpInput: {
    height: 80,
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 20,
    textAlign: 'center',
    flex: 1,
  },
  proceedButton: {
    marginTop: 20,
    backgroundColor: '#08152D',
    height: 45,
    borderRadius: 6,
    justifyContent: 'center',
  },
  text: {
    color: 'white',
    textAlign: 'center',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
  },
});
