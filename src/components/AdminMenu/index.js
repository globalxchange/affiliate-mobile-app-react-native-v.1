import React, {useEffect, useState} from 'react';
import {Image, StyleSheet, View} from 'react-native';
import BottomSheetLayout from '../../layouts/BottomSheetLayout';
import LoadingAnimation from '../LoadingAnimation';
import PasswordInput from './PasswordInput';
import UsersList from './UsersList';

const AdminMenu = ({isOpen, setIsOpen}) => {
  const [isListOpen, setIsListOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (!isOpen) {
      resetState();
    }
  }, [isOpen]);

  const resetState = () => {
    setIsListOpen(false);
    setIsLoading(false);
  };

  return (
    <BottomSheetLayout
      isOpen={isOpen}
      onClose={() => setIsOpen(false)}
      reactToKeyboard>
      <View style={styles.header}>
        <Image
          style={styles.headerLogo}
          source={require('../../assets/on-hold-full-icon.png')}
          resizeMode="contain"
        />
      </View>
      {isLoading ? (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      ) : isListOpen ? (
        <UsersList
          setIsLoading={setIsLoading}
          onClose={() => setIsOpen(false)}
        />
      ) : (
        <PasswordInput
          setIsLoading={setIsLoading}
          onClose={() => setIsOpen(false)}
        />
      )}
    </BottomSheetLayout>
  );
};

export default AdminMenu;

const styles = StyleSheet.create({
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.75)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    // flex: 1,
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    overflow: 'hidden',
  },
  fragmentContainer: {
    backgroundColor: 'white',
    paddingHorizontal: 35,
    paddingBottom: 35,
    paddingTop: 15,
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 30,
    paddingBottom: 20,
  },
  headerLogo: {
    height: 40,
  },
  loadingContainer: {
    paddingVertical: 50,
  },
});
