import React, {useState, useRef, useEffect} from 'react';
import {StyleSheet, Image, View} from 'react-native';
import Breadcrumb from './Breadcrumb';
import TypeSelector from './Fragments/TypeSelector';
import SendingCurrency from './Fragments/SendingCurrency';
import RecipientCurrency from './Fragments/RecipientCurrency';
import DestinationAddress from './Fragments/DestinationAddress';
import PaymentQuote from './Fragments/PaymentQuote';
import FromAppSelector from './Fragments/FromAppSelector';
import ToAppSelector from './Fragments/ToAppSelector';
import BottomSheetLayout from '../../layouts/BottomSheetLayout';

const Connect = ({isOpen, onClose}) => {
  const [currentStep, setCurrentStep] = useState();

  const [transferType, setTransferType] = useState('');
  const [sendingCurrency, setSendingCurrency] = useState('');
  const [recipientCurrency, setRecipientCurrency] = useState('');
  const [destinationMail, setDestinationMail] = useState('');
  const [senderAppCode, setSenderAppCode] = useState('');
  const [senderProfileId, setSetSenderProfileId] = useState('');
  const [recipientAppCode, setRecipientAppCode] = useState('');
  const [recipientAppName, setRecipientAppName] = useState('');
  const [recipientProfileId, setRecipientProfileId] = useState('');
  const [recipientName, setRecipientName] = useState('');

  const onSetType = (type) => {
    setTransferType(type);
    setCurrentStep('FromAppSelect');
  };

  const resetState = () => {
    setCurrentStep();
    setTransferType('');
    setSendingCurrency('');
    setRecipientCurrency('');
    setDestinationMail('');
  };

  const clearForType = () => {
    setCurrentStep('TxnType');
    setSendingCurrency('');
    setRecipientCurrency('');
    setDestinationMail('');
  };

  const clearForSendingCurrency = () => {
    setCurrentStep('SendingCurrency');
    setRecipientCurrency('');
    setDestinationMail('');
  };

  const clearForToCurrency = () => {
    setCurrentStep('RecipientCurrency');
    setDestinationMail('');
  };

  const clearForRecipientAddress = () => {
    setCurrentStep('DestinationAddress');
  };

  const clearForTransact = () => {
    setCurrentStep('Quote');
  };

  const renderFragment = () => {
    switch (currentStep) {
      case 'FromAppSelect':
        return (
          <FromAppSelector
            setAppCode={setSenderAppCode}
            setSenderProfileId={setSetSenderProfileId}
            onNext={() => setCurrentStep('SendingCurrency')}
          />
        );

      case 'SendingCurrency':
        return (
          <SendingCurrency
            senderAppCode={senderAppCode}
            senderProfileId={senderProfileId}
            sendingCurrency={sendingCurrency}
            setSendingCurrency={(currency) => {
              setSendingCurrency(currency);
              setRecipientCurrency(currency);
            }}
            onNext={() => setCurrentStep('ToAppSelect')}
          />
        );

      case 'ToAppSelect':
        return (
          <ToAppSelector
            setAppCode={setRecipientAppCode}
            setSenderProfileId={setRecipientProfileId}
            setAppName={setRecipientAppName}
            onNext={() => setCurrentStep('RecipientCurrency')}
          />
        );

      case 'RecipientCurrency':
        return (
          <RecipientCurrency
            onNext={() =>
              transferType === 'vaults'
                ? setCurrentStep('Quote')
                : setCurrentStep('DestinationAddress')
            }
            recipientCurrency={recipientCurrency}
            setRecipientCurrency={setRecipientCurrency}
          />
        );
      case 'DestinationAddress':
        return (
          <DestinationAddress
            setRecipientAppCode={setRecipientAppCode}
            destinationMail={destinationMail}
            setDestinationMail={setDestinationMail}
            senderProfileId={recipientProfileId}
            setSenderProfileId={setRecipientProfileId}
            recipientAppCode={recipientAppCode}
            onNextClick={() => setCurrentStep('Quote')}
            setRecipientName={setRecipientName}
          />
        );
      case 'Quote':
        return (
          <PaymentQuote
            senderAppCode={senderAppCode}
            senderProfileId={senderProfileId}
            recipientAppCode={recipientAppCode}
            recipientAppName={recipientAppName}
            sendingCurrency={sendingCurrency}
            recipientCurrency={recipientCurrency}
            friendName={destinationMail}
            friendProfile={recipientProfileId}
            transferType={transferType}
            onClose={() => setIsOpen(false)}
            recipientName={recipientName}
          />
        );
      default:
        return (
          <TypeSelector
            setType={onSetType}
            type={transferType}
            onNext={() => setCurrentStep('FromAppSelect')}
          />
        );
    }
  };

  const onBackKeyPress = () => {};

  return (
    <BottomSheetLayout isOpen={isOpen} onClose={onClose} reactToKeyboard>
      <View style={[styles.container]}>
        <View style={styles.header}>
          <Image
            style={styles.headerLogo}
            source={require('../../assets/connect-icon-white.png')}
            resizeMode="contain"
          />
        </View>
        <Breadcrumb
          setCurrentStep={setCurrentStep}
          selectedType={transferType}
          sendCurrency={sendingCurrency}
          recipientCurrency={recipientCurrency}
          destinationMail={destinationMail}
          clearForType={clearForType}
          clearForSendingCurrency={clearForSendingCurrency}
          clearForToCurrency={clearForToCurrency}
          clearForRecipientAddress={clearForRecipientAddress}
          clearForTransact={clearForTransact}
        />
        <View style={styles.fragmentContainer}>{renderFragment()}</View>
      </View>
    </BottomSheetLayout>
  );
};

export default Connect;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    overflow: 'hidden',
  },
  fragmentContainer: {
    // flex: 1,
    paddingHorizontal: 40,
    paddingTop: 20,
    paddingBottom: 20,
  },
  header: {
    backgroundColor: '#08152D',
    shadowColor: '#000000',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 20,
  },
  headerLogo: {
    height: 20,
  },
  panelHeader: {
    alignItems: 'center',
  },
  panelHandle: {
    width: 40,
    height: 4,
    borderRadius: 4,
    backgroundColor: '#E4E9F2',
    marginBottom: 10,
  },
});

const stepKeys = [
  'SendingCurrency',
  'RecipientCurrency',
  'DestinationAddress',
  'Quote',
];
