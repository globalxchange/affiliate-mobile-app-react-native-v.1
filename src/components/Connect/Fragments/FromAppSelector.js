import Axios from 'axios';
import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import {APP_CODE, GX_API_ENDPOINT} from '../../../configs';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import CustomDropDown from '../../CustomDropDown';
import ActionButton from '../ActionButton';

const FromAppSelector = ({setAppCode, setSenderProfileId, onNext}) => {
  const [appList, setAppList] = useState();
  const [selectedApp, setSelectedApp] = useState('');

  useEffect(() => {
    (async () => {
      const email = await AsyncStorageHelper.getLoginEmail();

      Axios.get(`${GX_API_ENDPOINT}/gxb/apps/registered/user`, {
        params: {email},
      })
        .then((resp) => {
          const {data} = resp;

          if (data.status) {
            const list = [];

            data.userApps.forEach((item) => {
              list.push({...item, name: item.app_name, image: item.app_icon});
            });

            const currentApp = list.find((x) => x.app_code === APP_CODE);
            setSelectedApp(currentApp);

            setAppList(list);
          }
        })
        .catch((error) => {});
    })();
  }, []);

  const onNextClick = () => {
    if (!selectedApp) {
      return WToast.show({
        data: 'Please Select An App First',
        position: WToast.position.TOP,
      });
    }
    setSenderProfileId(selectedApp.profile_id);
    setAppCode(selectedApp.app_code);
    onNext();
  };

  const onSetApp = (item) => {
    setSelectedApp(item);
    setSenderProfileId(item.profile_id);
    setAppCode(item.app_code);
  };

  return (
    <View style={styles.container}>
      <Text numberOfLines={1} adjustsFontSizeToFit style={styles.header}>
        Select From App
      </Text>
      <Text style={styles.subHeader}>
        Which One Of Your GX Apps Are You Going To Send The Funds From
      </Text>
      <View style={styles.dropDownContainer}>
        <CustomDropDown
          placeholderIcon={require('../../../assets/default-breadcumb-icon/currency.png')}
          // label="Select Asset"
          items={appList}
          placeHolder="See All Registered Apps"
          onDropDownSelect={onSetApp}
          selectedItem={selectedApp}
          // numberLabel="Currencies"
          subTextKey="balance"
          onNext={onNext}
          headerIcon={require('../../../assets/connect-icon-white.png')}
          headerIconHeight={18}
        />
      </View>
      <ActionButton text="Proceed To Select Currency" onPress={onNextClick} />
    </View>
  );
};

export default FromAppSelector;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 20,
    fontSize: 30,
    fontFamily: 'Montserrat-Bold',
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    marginBottom: 30,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  dropDownContainer: {
    justifyContent: 'center',
    marginBottom: 30,
  },
});
