import React, {useState, useRef, useContext, useEffect} from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import QuoteInput from '../QuoteInput';
import {WModalShowToastView, WToast} from 'react-native-smart-tip';
import {formatterHelper} from '../../../utils';
import LoadingView from './LoadingView';
import CompleteView from './CompleteView';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import {GX_API_ENDPOINT} from '../../../configs';
import {v4 as uuidv4} from 'uuid';
import Axios from 'axios';
import CryHelper from '../../../utils/CryHelper';
import {AppContext} from '../../../contexts/AppContextProvider';
import {roundHelper} from '../../../utils';

const PaymentQuote = ({
  onClose,
  sendingCurrency,
  recipientCurrency,
  friendName,
  friendProfile,
  transferType,
  recipientAppCode,
  senderProfileId,
  senderAppCode,
  recipientAppName,
  recipientName,
}) => {
  const {updateWalletBalances} = useContext(AppContext);

  const [sendValue, setSpendValue] = useState('');
  const [gettingValue, setGettingValue] = useState('');
  const [bonusValue, setBonusValue] = useState('');
  const [errorText, setErrorText] = useState('');
  const [purchaseLoading, setPurchaseLoading] = useState(false);
  const [isSendLoading, setIsSpendLoading] = useState(false);
  const [isGettingLoading, setIsGettingLoading] = useState(false);
  const [spendEdited, setSpendEdited] = useState();
  const [convertRate, setConvertRate] = useState(0);

  const [isLoading, setIsLoading] = useState(false);
  const [isCompeted, setIsCompeted] = useState(false);

  const spendLoaded = useRef(false);
  const getLoaded = useRef(false);
  const toastRef = useRef();

  useEffect(() => {
    Axios.get(`${GX_API_ENDPOINT}/forex/convert`, {
      params: {
        buy: recipientCurrency.coinSymbol,
        from: sendingCurrency.coinSymbol,
      },
    })
      .then((resp) => {
        const {data} = resp;

        if (data.status) {
          const rate =
            data[
              `${sendingCurrency.coinSymbol.toLowerCase()}_${recipientCurrency.coinSymbol.toLowerCase()}`
            ];

          setConvertRate(rate || 1);
        } else {
          setConvertRate(1);
        }
      })
      .catch((error) => console.log('Conversion Rate error', error));
  }, [recipientCurrency, sendingCurrency]);

  const buyClickHandler = async () => {
    if (purchaseLoading) {
      return;
    }

    if (
      (isNaN(parseFloat(gettingValue)) || parseFloat(gettingValue) <= 0) &&
      (isNaN(parseFloat(sendValue)) || parseFloat(sendValue) <= 0)
    ) {
      setGettingValue('');
      return toastRef.current({
        data: 'Please Input A Valid value',
        position: WToast.position.TOP,
      });
    }

    setPurchaseLoading(true);

    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    let postData = {
      token,
      email,
      from: {
        app_code: senderAppCode,
        profile_id: senderProfileId,
        coin: sendingCurrency.coinSymbol,
      },
      to: {
        app_code: recipientAppCode,
        profile_id: friendProfile,
        coin: recipientCurrency.coinSymbol,
      },
      // to_amount: parseFloat(gettingValue),
      identifier: uuidv4(),
      transfer_for: `${transferType.name}`,
    };

    if (gettingValue) {
      postData = {...postData, to_amount: parseFloat(gettingValue)};
    } else {
      postData = {...postData, from_amount: parseFloat(sendValue)};
    }

    console.log('Post Data', postData);

    // const encryptedData = CryHelper.encryptPostData(postData);

    Axios.post(`${GX_API_ENDPOINT}/coin/vault/service/transfer`, postData)
      .then((resp) => {
        const {data} = resp;

        console.log('Quote Resp', data);

        if (data.status) {
          updateWalletBalances();
          setIsCompeted(true);
          setPurchaseLoading(false);
          toastRef.current({
            data: 'Transfer Was Initiated',
            position: WToast.position.TOP,
          });
        } else {
          setPurchaseLoading(false);
          toastRef.current({
            data: data.message,
            position: WToast.position.TOP,
          });
        }
      })
      .catch((error) => {
        setPurchaseLoading(false);
        toastRef.current({
          data: 'Some Error Occurred While Doing The Transaction',
          position: WToast.position.TOP,
        });
        console.log('Error on transaction', error);
      });
  };

  const resetFlags = () => {
    setTimeout(() => {
      getLoaded.current = false;
      spendLoaded.current = false;
    }, 200);
  };

  const onSendEdit = (value) => {
    setSpendValue(value);
    const parsedValue = parseFloat(value) || 0;
    setGettingValue(
      roundHelper(parsedValue * convertRate, recipientCurrency.coinSymbol),
    );
    setSpendEdited(true);
  };

  const onGetEdit = (value) => {
    setGettingValue(value);
    const parsedValue = parseFloat(value) || 0;
    setSpendValue(
      roundHelper(parsedValue / convertRate, sendingCurrency.coinSymbol),
    );
    setSpendEdited(false);
  };

  if (isLoading || purchaseLoading) {
    return <LoadingView />;
  }

  if (isCompeted) {
    return (
      <CompleteView
        sendCurrency={sendingCurrency}
        friendName={friendName}
        sendValue={`${formatterHelper(sendValue, sendingCurrency.coinSymbol)} ${
          sendingCurrency.coinSymbol
        }`}
        getValue={`${formatterHelper(
          gettingValue,
          recipientCurrency.coinSymbol,
        )} ${recipientCurrency.coinSymbol}`}
        setIsLoading={setIsLoading}
        openTxnAudit={() => onClose()}
        recipientAppName={recipientAppName}
        onClose={onClose}
        recipientName={recipientName}
      />
    );
  }

  return (
    <View style={styles.container}>
      <WModalShowToastView
        toastInstance={(toastInstance) => (toastRef.current = toastInstance)}
      />
      <Text style={styles.header}>Amount</Text>
      <View style={styles.quoteContainer}>
        <QuoteInput
          image={{
            uri: sendingCurrency.coinImage,
          }}
          unit={sendingCurrency.coinSymbol}
          enabled
          // title={`How Much ${paymentCurrency.coinSymbol} Do You Want To Sell?`}
          title="You Will Be Sending"
          placeholder={formatterHelper('0', sendingCurrency.coinSymbol)}
          value={sendValue}
          setValue={onSendEdit}
          isLoading={isSendLoading}
        />

        <QuoteInput
          image={{
            uri: recipientCurrency.coinImage,
          }}
          unit={recipientCurrency.coinSymbol}
          title={`${friendName} Should Be Receiving`}
          enabled
          value={gettingValue.toString()}
          setValue={onGetEdit}
          placeholder={formatterHelper('0', recipientCurrency.coinSymbol)}
          isLoading={isGettingLoading}
        />
        {/* <QuoteInput
        image={{
          uri: recipientCurrency.coinImage,
        }}
        unit={recipientCurrency.coinSymbol}
        // title={`This Is How Much ${selectedCrypto.coinName} You Will Be Getting`}
        title="#InitiatePlanB Bonus"
        enabled={false}
        value={bonusValue.toString()}
        setValue={onGetEdit}
        placeholder={formatterHelper('0', recipientCurrency.coinSymbol)}
        isLoading={isGettingLoading}
      /> */}
      </View>
      {errorText !== '' && (
        <View>
          <Text style={styles.errorText}>{errorText}</Text>
        </View>
      )}
      <TouchableOpacity
        style={[
          styles.buyBtn,
          (isSendLoading || isGettingLoading) && styles.disabled,
        ]}
        onPress={buyClickHandler}
        disabled={isSendLoading || isGettingLoading}>
        <Text style={styles.buyBtnText}>Send</Text>
      </TouchableOpacity>
    </View>
  );
};

export default PaymentQuote;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 10,
    fontSize: 35,
    fontFamily: 'Montserrat-Bold',
  },
  quoteContainer: {
    marginTop: 20,
    marginBottom: 30,
  },
  buyBtn: {
    backgroundColor: '#08152D',
    height: 45,
    borderRadius: 6,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  disabled: {
    opacity: 0.5,
  },
  errorText: {
    color: '#FF2D55',
    textAlign: 'center',
    marginBottom: 20,
    marginTop: -10,
  },
  buyBtnText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 14,
    fontFamily: 'Montserrat-SemiBold',
  },
  feeButton: {
    zIndex: 1,
    position: 'absolute',
    top: 0,
    right: 0,
  },
  feeText: {
    color: '#08152D',
    fontFamily: 'Montserrat',
  },
});
