import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import ActionButton from '../ActionButton';

const TypeSelector = ({setType, type}) => {
  const onNextClick = () => {
    WToast.show({
      data: 'Please Click On Any Option',
      position: WToast.position.TOP,
    });
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>I am Transferring</Text>

      <View style={styles.listContainer}>
        {types.map((item) => (
          <TouchableOpacity
            onPress={() => setType(item.key)}
            disabled={item.disabled}
            key={item.name}
            style={styles.typeItem}>
            <Image
              style={[styles.typeIcon, item.disabled && {opacity: 0.5}]}
              source={item.image}
              resizeMode="contain"
            />
            <Text style={[styles.typeName, item.disabled && {opacity: 0.5}]}>
              {item.name}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
      <ActionButton text="Proceed To Select Currency" onPress={onNextClick} />
    </View>
  );
};

export default TypeSelector;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 20,
    fontSize: 30,
    fontFamily: 'Montserrat-Bold',
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    marginBottom: 30,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  typeItem: {
    flexDirection: 'row',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    paddingHorizontal: 15,
    alignItems: 'center',
    height: 50,
    marginBottom: 20,
  },
  typeIcon: {
    height: 24,
    width: 24,
  },
  typeName: {
    flexGrow: 1,
    width: 0,
    paddingHorizontal: 15,
    fontFamily: 'Montserrat',
    color: '#788995',
  },
  listContainer: {
    // flex: 1,
    justifyContent: 'center',
  },
});

const types = [
  {
    name: 'Between My Vaults',
    image: require('../../../assets/wallet-icon.png'),
    key: 'vaults',
  },
  {
    name: 'To Another User',
    image: require('../../../assets/user-group-icon.png'),
    key: 'users',
  },
];
