import React, {useState, useRef, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import ActionButton from '../ActionButton';
import {emailValidator, getUriImage} from '../../../utils';
import {WToast, WModalShowToastView} from 'react-native-smart-tip';
import Axios from 'axios';
import {GX_API_ENDPOINT, APP_CODE} from '../../../configs';
import LoadingAnimation from '../../LoadingAnimation';
import FastImage from 'react-native-fast-image';

const DestinationAddress = ({
  onNextClick,
  destinationMail,
  setDestinationMail,
  senderProfileId,
  setSenderProfileId,
  setRecipientAppCode,
  recipientAppCode,
  setRecipientName,
}) => {
  const [emailInput, setEmailInput] = useState('');
  const [showValidate, setShowValidate] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [userData, setUserData] = useState();

  const toastRef = useRef();

  useEffect(() => {
    if (destinationMail) {
      setEmailInput(destinationMail);
    }
  }, [destinationMail]);

  const validateEmail = () => {
    const senderEmail = emailInput.trim().toLowerCase();

    if (!emailValidator(senderEmail)) {
      return toastRef.current({
        data: 'Enter A Valid Email ID',
        position: WToast.position.TOP,
      });
    }

    setIsLoading(true);

    const params = {
      email: senderEmail,
      app_code: recipientAppCode || APP_CODE,
    };
    Axios.get(`${GX_API_ENDPOINT}/user/details/get`, {params})
      .then((resp) => {
        const {data} = resp;

        console.log('Resp', data);
        console.log('recipientAppCode', recipientAppCode);

        if (data.status) {
          setUserData(data.user);
          setShowValidate(true);
          setDestinationMail(senderEmail);
          setRecipientName(data?.user?.name);
          const profileId = data.user[`${recipientAppCode}_profile_id`];

          // console.log('profileId', profileId);

          setSenderProfileId(profileId);
          setRecipientAppCode(recipientAppCode || APP_CODE);
        } else {
          toastRef.current({
            data: 'No User Found This Email',
            position: WToast.position.TOP,
          });
        }
      })
      .catch((error) => console.log('Error on validating Email', error))
      .finally(() => setIsLoading(false));
  };

  const onNext = () => {
    onNextClick();
  };

  return (
    <View style={styles.container}>
      <WModalShowToastView
        toastInstance={(toastInstance) => (toastRef.current = toastInstance)}
      />
      {isLoading ? (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      ) : (
        <>
          <Text style={styles.header}>
            {showValidate ? 'Great News' : 'Destination'}
          </Text>
          {showValidate && (
            <Text style={styles.subHeader}>
              We Found A Match. Is This Your Friend?
            </Text>
          )}
          <View style={styles.controlContainer}>
            {userData ? (
              <View style={styles.userContainer}>
                <FastImage
                  source={{uri: getUriImage(userData.profile_img)}}
                  style={styles.userAvatar}
                  resizeMode="contain"
                />
                <View style={styles.nameContainer}>
                  <Text style={styles.name}>{userData.name}</Text>
                  <Text style={styles.email}>{userData.email}</Text>
                </View>
              </View>
            ) : (
              <>
                <Text style={styles.label}>What Is Your Friends Email?</Text>
                <View style={styles.inputContainer}>
                  <TextInput
                    style={styles.input}
                    placeholderTextColor="#9A9A9A"
                    placeholder="Enter Email"
                    value={emailInput}
                    editable={!showValidate}
                    onChangeText={(text) => setEmailInput(text)}
                  />
                  <TouchableOpacity
                    style={styles.iconContainer}
                    onPress={validateEmail}>
                    <Image
                      style={styles.icon}
                      resizeMode="contain"
                      source={require('../../../assets/next-forward-icon-white.png')}
                    />
                  </TouchableOpacity>
                </View>
              </>
            )}
          </View>
          {showValidate && <ActionButton text="Next" onPress={onNext} />}
        </>
      )}
    </View>
  );
};

export default DestinationAddress;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-between',
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 20,
    fontSize: 30,
    fontFamily: 'Montserrat-Bold',
    marginTop: 20,
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    marginBottom: 30,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  controlContainer: {
    justifyContent: 'center',
    marginVertical: 40,
  },
  inputContainer: {
    flexDirection: 'row',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    paddingLeft: 15,
    alignItems: 'center',
    height: 50,
    marginBottom: 30,
    overflow: 'hidden',
  },
  label: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  input: {
    flexGrow: 1,
    width: 0,
    paddingHorizontal: 15,
    fontFamily: 'Montserrat',
    color: 'black',
  },
  iconContainer: {
    borderLeftColor: '#EBEBEB',
    borderLeftWidth: 1,
    width: 50,
    paddingHorizontal: 15,
    backgroundColor: '#08152D',
  },
  icon: {
    flex: 1,
    width: null,
  },
  buttonContainer: {
    flexDirection: 'row',
  },
  loadingContainer: {
    paddingVertical: 50,
    justifyContent: 'center',
  },
  userContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
    borderColor: '#EBEBEB',
    borderWidth: 1,
    borderRadius: 6,
    paddingHorizontal: 15,
    paddingVertical: 8,
  },
  userAvatar: {
    width: 40,
    height: 40,
    borderRadius: 20,
  },
  nameContainer: {
    marginLeft: 15,
    flex: 1,
  },
  name: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat-Bold',
    fontSize: 15,
  },
  email: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat',
    fontSize: 11,
  },
});
