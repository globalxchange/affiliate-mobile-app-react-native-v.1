import React, {useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Image,
} from 'react-native';
import * as WebBrowser from 'expo-web-browser';
import {formatterHelper} from '../../../utils';
import {AppContext} from '../../../contexts/AppContextProvider';
import Axios from 'axios';
import {GX_API_ENDPOINT, APP_CODE} from '../../../configs';
import AsyncStorageHelper from '../../../utils/AsyncStorageHelper';
import ThemeData from '../../../configs/ThemeData';
import {useNavigation} from '@react-navigation/native';

const CompleteView = ({
  sendCurrency,
  sendValue,
  openTxnAudit,
  setIsLoading,
  friendName,
  recipientAppName,
  getValue,
  onClose,
  recipientName,
}) => {
  const {navigate} = useNavigation();

  const {walletBalances} = useContext(AppContext);

  const updatedSendBalance =
    walletBalances[`${sendCurrency?.coinSymbol?.toLowerCase()}_balance`] || 0;

  const openTransactionAudit = async () => {
    setIsLoading(true);

    const profileId = await AsyncStorageHelper.getProfileId();

    const postData = {
      app_code: APP_CODE,
      profile_id: profileId,
      coin: sendCurrency.coinSymbol,
    };
    Axios.post(`${GX_API_ENDPOINT}/coin/vault/service/txns/get`, postData)
      .then((resp) => {
        const {data} = resp;
        if (data.status) {
          openTxnAudit(data.txns[0]);
        }
        setIsLoading(false);
        // console.log('TXN List', data.txns);
      })
      .catch((error) => console.log('Error getting txn list', error));
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Completed</Text>
      <Text style={styles.subHeader}>
        Great Job. You Have Sent {sendValue} And{' '}
        {recipientName ||
          friendName ||
          (recipientAppName ? `${recipientAppName}  Vault` : '' || '')}{' '}
        Has Received {getValue}
      </Text>
      <View style={styles.valuesContainer}>
        <View style={styles.valueItem}>
          <Text style={styles.title}>New {sendCurrency.coinName} Balance</Text>
          <View style={styles.inputContainer}>
            <Text style={styles.textInput}>
              {formatterHelper(updatedSendBalance, sendCurrency.coinSymbol)}
            </Text>
            <View style={styles.unitContainer}>
              <Text style={styles.unitValue}>{sendCurrency.coinSymbol}</Text>
              <Image
                source={{uri: sendCurrency.coinImage}}
                style={styles.unitImage}
                resizeMode="cover"
              />
            </View>
          </View>
        </View>
      </View>
      <View style={styles.actionContainer}>
        <TouchableOpacity style={styles.filledButton}>
          <Text style={styles.filledButtonText}>Send</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            onClose();
            navigate('Wallet', {coin: sendCurrency.coinSymbol});
          }}
          style={styles.outlinedButton}>
          <Text style={styles.outlinedButtonText}>
            {sendCurrency.coinSymbol} Vault
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default CompleteView;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    justifyContent: 'space-between',
    marginTop: -80,
    marginHorizontal: -30,
    backgroundColor: 'white',
    paddingHorizontal: 30,
    paddingTop: 35,
  },
  header: {
    color: '#08152D',
    textAlign: 'left',
    marginBottom: 10,
    fontSize: 30,
    fontFamily: 'Montserrat-Bold',
  },
  subHeader: {
    color: '#9A9A9A',
    textAlign: 'left',
    marginBottom: 20,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  valuesContainer: {
    marginVertical: 15,
  },
  actionContainer: {
    flexDirection: 'row',
    marginTop: 20,
    marginBottom: 20,
  },
  scrollView: {
    marginHorizontal: -10,
    // flex: 1,
  },
  action: {
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    backgroundColor: 'white',
    borderRadius: 8,
    paddingVertical: 10,
    paddingHorizontal: 20,
    flex: 1,
    marginVertical: 10,
    width: 170,
  },
  actionIcon: {
    width: 35,
    height: 35,
  },
  actionText: {
    marginTop: 10,
    fontFamily: 'Montserrat-SemiBold',
    color: '#08152D',
    textAlign: 'center',
    fontSize: 12,
  },
  actionTextBlack: {
    color: '#464B4E',
  },
  valueItem: {marginBottom: 10},
  title: {
    color: '#9A9A9A',
    marginBottom: 10,
    fontSize: 12,
    fontFamily: 'Montserrat',
  },
  inputContainer: {
    flexDirection: 'row',
    borderWidth: 2,
    borderRadius: 8,
    borderColor: '#EBEBEB',
    height: 50,
    alignItems: 'center',
  },
  textInput: {
    flexGrow: 1,
    width: 0,
    textAlign: 'center',
    fontFamily: 'Montserrat-Bold',
    fontSize: 18,
    paddingHorizontal: 20,
    color: 'black',
  },
  unitContainer: {
    width: '45%',
    flexDirection: 'row',
    alignItems: 'center',
    borderLeftColor: '#EBEBEB',
    borderLeftWidth: 2,
    justifyContent: 'center',
    height: '100%',
  },
  unitValue: {
    color: '#9A9A9A',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 16,
  },
  unitImage: {
    height: 24,
    width: 24,
    borderRadius: 12,
    marginLeft: 15,
  },
  filledButton: {
    backgroundColor: ThemeData.APP_MAIN_COLOR,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    marginRight: 10,
    borderRadius: 6,
  },
  filledButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: 'white',
    fontSize: 13,
  },
  outlinedButton: {
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    borderColor: ThemeData.BORDER_COLOR,
    borderWidth: 1,
    borderRadius: 6,
  },
  outlinedButtonText: {
    fontFamily: ThemeData.FONT_SEMI_BOLD,
    color: ThemeData.APP_MAIN_COLOR,
    fontSize: 12,
  },
});
