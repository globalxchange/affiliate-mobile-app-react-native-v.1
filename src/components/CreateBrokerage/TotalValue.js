import React, {useEffect, useRef, useState} from 'react';
import {
  Image,
  Keyboard,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Animated, {
  Clock,
  interpolate,
  set,
  useCode,
} from 'react-native-reanimated';
import {WToast} from 'react-native-smart-tip';
import ReanimatedTimingHelper from '../../utils/ReanimatedTimingHelper';

const TotalValue = ({totalValue, setTotalValue, onNext}) => {
  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);

  const chatKeyboardAnimation = useRef(new Animated.Value(0));

  useEffect(() => {
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
      onKeyBoardShow,
    );
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
      onKeyBoardHide,
    );
    return () => {
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
        onKeyBoardShow,
      );
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
        onKeyBoardHide,
      );
    };
  }, []);

  useCode(
    () =>
      isKeyboardOpen
        ? [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ]
        : [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ],
    [isKeyboardOpen],
  );

  const onKeyBoardShow = (e) => {
    setKeyboardHeight(e.endCoordinates.height - 30);
    setIsKeyboardOpen(true);
  };

  const onKeyBoardHide = () => {
    setIsKeyboardOpen(false);
  };

  const onNextClick = () => {
    const parsedValue = parseFloat(totalValue);
    if (parsedValue) {
      onNext();
    } else {
      WToast.show({
        data: 'Please Enter The Deal Total Value',
        position: WToast.position.TOP,
      });
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.container}>
        <Text style={styles.header}>Total Transactional Value</Text>
        <View style={styles.inputContainer}>
          <TextInput
            value={totalValue}
            onChangeText={(text) => setTotalValue(text)}
            style={styles.input}
            placeholder="$0.00"
            placeholderTextColor="#848A96"
            keyboardType="numeric"
            returnKeyType="done"
          />
          <Text style={styles.currencyName}>USD</Text>
          <TouchableOpacity onPress={onNextClick} style={styles.nextButton}>
            <Image
              source={require('../../assets/next-forward-icon.png')}
              style={styles.nextIcon}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
      </View>
      <Animated.View
        style={[
          {
            height: interpolate(chatKeyboardAnimation.current, {
              inputRange: [0, 1],
              outputRange: [0, keyboardHeight],
            }),
          },
        ]}
      />
    </View>
  );
};

export default TotalValue;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  header: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 22,
  },
  inputContainer: {
    borderColor: '#DCDCDC',
    borderWidth: 1,
    marginTop: 25,
    height: 60,
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    fontFamily: 'Montserrat',
    color: '#08152D',
    height: '100%',
    paddingHorizontal: 20,
    flex: 1,
    fontSize: 16,
  },
  nextButton: {
    backgroundColor: '#F1F4F6',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 22,
    height: '100%',
  },
  nextIcon: {
    width: 25,
    height: 25,
  },
  currencyName: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    paddingHorizontal: 20,
  },
});
