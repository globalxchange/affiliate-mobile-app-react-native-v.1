import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Moment from 'moment-timezone';
import {percentageFormatter, usdValueFormatter} from '../../utils';

const Summary = ({
  dealName,
  commissionType,
  totalValue,
  totalCommission,
  description,
  onEdit,
  onNext,
}) => {
  const currentDate = Moment().format('MM/DD/YYYY');

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Image
          source={require('../../assets/fire-yellow-icon.png')}
          style={styles.fireIcon}
          resizeMode="contain"
        />
        <Text style={styles.headerText}>Deal Summary</Text>
      </View>
      <View style={styles.summaryContainer}>
        <View style={styles.summaryHeader}>
          <Text style={styles.dealName}>{dealName}</Text>
        </View>
        <View style={styles.dealDetailsContainer}>
          <Text style={styles.desc}>Just You</Text>
          <Text style={styles.desc}>{currentDate}</Text>
          <Text style={styles.desc}>{commissionType}</Text>
          <Text style={styles.desc} />
          <Text style={styles.desc}>{description}</Text>
          <Text style={styles.totalVolume}>
            {usdValueFormatter.format(totalValue || 0)} USD
          </Text>
          <Text style={styles.valueLabel}>Total Transactional Volume</Text>
          <Text style={styles.totalCommission}>
            {percentageFormatter.format(totalCommission || 0)}%
          </Text>
          <Text style={styles.valueLabel}>Total Commissions</Text>
          <View style={styles.buttonContainer}>
            <TouchableOpacity onPress={onEdit} style={styles.outlinedButton}>
              <Text style={styles.outlinedButtonText}>Edit Deal</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={onNext} style={styles.filledButton}>
              <Text style={styles.filledButtonText}>Post To BrokerChain</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Summary;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: -30,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 30,
    marginTop: 30,
  },
  fireIcon: {
    width: 40,
    height: 40,
  },
  headerText: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 26,
    marginLeft: 10,
  },
  summaryContainer: {
    borderColor: '#DCDCDC',
    borderWidth: 1,
    marginTop: 'auto',
    marginBottom: 'auto',
    marginHorizontal: 30,
  },
  summaryHeader: {
    backgroundColor: '#08152D',
    borderRadius: 3,
  },
  dealName: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
    paddingVertical: 8,
    fontSize: 16,
  },
  dealDetailsContainer: {
    paddingHorizontal: 25,
    paddingTop: 25,
    paddingBottom: 35,
  },
  desc: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 11,
    marginBottom: 5,
  },
  totalVolume: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 24,
    marginTop: 22,
  },
  valueLabel: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 11,
  },
  totalCommission: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 18,
    marginTop: 12,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginTop: 35,
  },
  outlinedButton: {
    borderColor: '#E4E4E4',
    borderWidth: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginRight: 10,
  },
  outlinedButtonText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
  filledButton: {
    backgroundColor: '#08152D',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  filledButtonText: {
    color: 'white',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 12,
  },
});
