import React, {useEffect, useState} from 'react';
import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

const DealTypes = ({
  onNext,
  dealType,
  setDealType,
  whoWorkedOnTheDeal,
  setWhoWorkedOnTheDeal,
  industry,
  setIndustry,
  financialSector,
  setFinancialSector,
}) => {
  useEffect(() => {
    if (dealType && whoWorkedOnTheDeal && industry && financialSector) {
      onNext();
    }
  }, [dealType, whoWorkedOnTheDeal, industry, financialSector]);

  const onItemClick = (clickedItem, key) => {
    switch (key) {
      case 'dealType':
        setDealType(clickedItem);
        break;
      case 'whoWorkedOnTheDeal':
        setWhoWorkedOnTheDeal(clickedItem);
        break;
      case 'industry':
        setIndustry(clickedItem);
        break;
      case 'financialSector':
        setFinancialSector(clickedItem);
        break;
    }
  };

  const getActiveStyles = (item, key, isText) => {
    const activeContainer = {
      backgroundColor: '#08152D',
      height: 35,
      width: 120,
      alignItems: 'center',
      justifyContent: 'center',
      marginRight: 15,
    };

    const nonActiveContainer = {
      borderColor: '#B8B8B8',
      borderWidth: 1,
      height: 35,
      width: 120,
      alignItems: 'center',
      justifyContent: 'center',
      marginRight: 15,
    };

    const activeText = {
      color: 'white',
      fontFamily: 'Montserrat-SemiBold',
      textAlign: 'center',
      fontSize: 12,
    };

    const nonActiveText = {
      color: '#08152D',
      fontFamily: 'Montserrat-SemiBold',
      textAlign: 'center',
      fontSize: 12,
    };

    switch (key) {
      case 'dealType':
        return isText
          ? item === dealType
            ? activeText
            : nonActiveText
          : item === dealType
          ? activeContainer
          : nonActiveContainer;

      case 'whoWorkedOnTheDeal':
        return isText
          ? item === whoWorkedOnTheDeal
            ? activeText
            : nonActiveText
          : item === whoWorkedOnTheDeal
          ? activeContainer
          : nonActiveContainer;

      case 'industry':
        return isText
          ? item === industry
            ? activeText
            : nonActiveText
          : item === industry
          ? activeContainer
          : nonActiveContainer;

      case 'financialSector':
        return isText
          ? item === financialSector
            ? activeText
            : nonActiveText
          : item === financialSector
          ? activeContainer
          : nonActiveContainer;

      default:
        return {};
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Image
          source={require('../../assets/fire-yellow-icon.png')}
          style={styles.fireIcon}
          resizeMode="contain"
        />
        <Text style={styles.headerText}>New Deal Alert</Text>
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        {actions.map((action) => (
          <View key={action.title} style={styles.controlItem}>
            <Text style={styles.itemHeader}>{action.title}</Text>
            <ScrollView
              showsHorizontalScrollIndicator={false}
              style={styles.buttonsContainer}
              horizontal>
              {action.data.map((item) => (
                <TouchableOpacity
                  onPress={() => onItemClick(item, action.key)}
                  key={item}
                  style={[getActiveStyles(item, action.key, false)]}>
                  <Text style={[getActiveStyles(item, action.key, true)]}>
                    {item}
                  </Text>
                </TouchableOpacity>
              ))}
            </ScrollView>
          </View>
        ))}
      </ScrollView>
    </View>
  );
};

export default DealTypes;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: -30,
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 30,
    paddingTop: 30,
  },
  fireIcon: {
    width: 40,
    height: 40,
  },
  headerText: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 26,
    marginLeft: 10,
  },
  controlItem: {
    marginTop: 40,
    paddingLeft: 30,
  },
  itemHeader: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 16,
    paddingRight: 30,
  },
  buttonsContainer: {
    marginTop: 20,
  },
  itemButton: {
    borderColor: '#B8B8B8',
    borderWidth: 1,
    height: 35,
    width: 120,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
  },
  itemButtonText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
    fontSize: 12,
  },
});

const actions = [
  {
    key: 'dealType',
    title: 'What Type Of Deal Is This? ',
    data: ['One-Timer', 'Reoccurring'],
  },
  {
    key: 'whoWorkedOnTheDeal',
    title: 'Who Worked On The Deal?',
    data: ['Just Me', '1 Other Person', 'A Group'],
  },

  {
    key: 'industry',
    title: 'Which Industry Would It Be Classified Under? ',
    data: ['Just Me', '1 Other Person', 'A Group'],
  },
  {
    key: 'financialSector',
    title: 'Which Sector Of Financial Services Would You Classify It Under?',
    data: ['Just Me', '1 Other Person', 'A Group'],
  },
];
