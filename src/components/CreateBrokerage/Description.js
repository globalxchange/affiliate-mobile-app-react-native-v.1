import React from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {WToast} from 'react-native-smart-tip';

const {height} = Dimensions.get('window');

const Description = ({description, setDescription, onNext}) => {
  const onNextClick = () => {
    if (description) {
      onNext();
    } else {
      WToast.show({
        data: 'Please Enter A Deal Description',
        position: WToast.position.TOP,
      });
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <Text style={styles.header}>Describe The Deal</Text>
        {description.trim() !== '' && (
          <TouchableOpacity onPress={onNextClick} style={styles.nextButton}>
            <Image
              source={require('../../assets/next-forward-icon.png')}
              style={styles.nextIcon}
              resizeMode="contain"
            />
          </TouchableOpacity>
        )}
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          value={description}
          onChangeText={(text) => setDescription(text)}
          style={styles.input}
          multiline
          placeholder="Tell Us A Bit More About The Deal..."
          placeholderTextColor="#848A96"
        />
      </View>
    </View>
  );
};

export default Description;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 40,
  },
  header: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 22,
  },
  inputContainer: {
    borderColor: '#DCDCDC',
    borderWidth: 1,
    marginTop: 25,
    height: height * 0.4,
  },
  input: {
    fontFamily: 'Montserrat',
    color: '#08152D',
    paddingHorizontal: 20,
    maxHeight: height * 0.4 - 50,
  },
  nextButton: {
    backgroundColor: '#F1F4F6',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    width: 45,
  },
  nextIcon: {
    width: 18,
    height: 18,
  },
});
