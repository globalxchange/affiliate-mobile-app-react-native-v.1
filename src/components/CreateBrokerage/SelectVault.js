import React, {useContext, useEffect, useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {AppContext} from '../../contexts/AppContextProvider';
import {formatterHelper} from '../../utils';
import CustomDropDown from '../CustomDropDown';

const SelectVault = ({setVault, vault, onNext}) => {
  const {cryptoTableData, walletBalances} = useContext(AppContext);

  const [availableCurrencies, setAvailableCurrencies] = useState();

  useEffect(() => {
    if (cryptoTableData && walletBalances) {
      const filteredCurrencies = [];

      cryptoTableData.forEach((item) => {
        if (item.type === 'crypto') {
          filteredCurrencies.push({
            ...item,
            balance: formatterHelper(
              walletBalances[`${item.coinSymbol.toLowerCase()}_balance`],
              item.coinSymbol,
            ),
          });
        }
      });

      setAvailableCurrencies(filteredCurrencies);
    }
  }, [cryptoTableData, walletBalances]);

  return (
    <View style={styles.container}>
      <Text style={styles.header}>Source Of The Commissions</Text>
      <CustomDropDown
        placeholderIcon={require('../../assets/default-breadcumb-icon/currency.png')}
        // label="Select Asset"
        items={availableCurrencies}
        placeHolder="Select Vault"
        onDropDownSelect={setVault}
        selectedItem={vault}
        // numberLabel="Currencies"
        subTextKey="balance"
        onNext={onNext}
      />
    </View>
  );
};

export default SelectVault;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  header: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 22,
  },
});
