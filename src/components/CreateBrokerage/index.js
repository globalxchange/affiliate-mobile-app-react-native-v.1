import Axios from 'axios';
import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {WToast} from 'react-native-smart-tip';
import {GX_API_ENDPOINT, S3_CONFIG} from '../../configs';
import AsyncStorageHelper from '../../utils/AsyncStorageHelper';
import LoadingAnimation from '../LoadingAnimation';
import CommissionType from './CommissionType';
import Completed from './Completed';
import DealTypes from './DealTypes';
import Description from './Description';
import NameOfDeal from './NameOfDeal';
import SelectVault from './SelectVault';
import Summary from './Summary';
import TotalCommission from './TotalCommission';
import TotalValue from './TotalValue';
import {RNS3} from 'react-native-s3-upload';

const CreateBrokerage = () => {
  const [activeStep, setActiveStep] = useState();

  const [dealName, setDealName] = useState('');
  const [description, setDescription] = useState('');
  const [totalValue, setTotalValue] = useState('');
  const [totalCommission, setTotalCommission] = useState('');
  const [commissionType, setCommissionType] = useState('');
  const [selectedVault, setSelectedVault] = useState('');
  const [dealType, setDealType] = useState();
  const [whoWorkedOnTheDeal, setWhoWorkedOnTheDeal] = useState();
  const [industry, setIndustry] = useState();
  const [financialSector, setFinancialSector] = useState();
  const [icon, setIcon] = useState('');

  const createDeal = async () => {
    setActiveStep('Loading');

    try {
      const iconName = `${dealName.replace(' ', '_')}${Date.now()}`;
      const fileObj = {
        uri: icon,
        name: iconName,
        type: 'image/jpeg',
      };

      const email = await AsyncStorageHelper.getLoginEmail();
      const token = await AsyncStorageHelper.getAppToken();

      let postData = {
        email,
        token,
        txn_name: dealName,
        description: description,
        amount: parseFloat(totalValue),
        fees: parseFloat(totalCommission),
        coin_to_debit: selectedVault.coinSymbol,
        commission_type: commissionType === 'TokenBroker' ? 'token' : 'otc',
        deal_type: dealType,
        deal_industry: industry,
        deal_worked_on_by: whoWorkedOnTheDeal,
        deal_financial_sector: financialSector,
      };

      const options = {
        keyPrefix: 'dealIcons/',
        bucket: 'gxnitrousdata',
        region: 'us-east-2',
        accessKey: S3_CONFIG.accessKeyId,
        secretKey: S3_CONFIG.secretAccessKey,
        successActionStatus: 201,
      };

      RNS3.put(fileObj, options).then((resp) => {
        if (resp.status !== 201) {
          throw new Error('Failed to upload image to S3');
        }
        // console.log(resp.body);

        const {body} = resp;

        if (body?.postResponse?.location) {
          postData = {...postData, icon: body?.postResponse?.location};
          // console.log('PostData', postData);
          Axios.post(
            `${GX_API_ENDPOINT}/brokerage/create/commission/txn`,
            postData,
          )
            .then((res) => {
              const {data} = res;

              // console.log('Resp', data);

              if (data.status) {
                setActiveStep('Completed');
              } else {
                WToast.show({
                  data: data.message || 'Error On Creating Deal',
                  position: WToast.position.TOP,
                });
                setActiveStep('Summary');
              }
            })
            .catch((error) => {
              setActiveStep('Summary');
              WToast.show({data: 'API Error', position: WToast.position.TOP});
            });
        }
      });
    } catch (error) {
      console.log('Error on Uploading Image', error);
      WToast.show({
        data: 'Error On Uploading Icon',
        position: WToast.position.TOP,
      });
      setActiveStep('Summary');
    }
  };

  let activeView;

  switch (activeStep) {
    case 'Step':
      activeView = (
        <DealTypes
          dealType={dealType}
          setDealType={setDealType}
          whoWorkedOnTheDeal={whoWorkedOnTheDeal}
          setWhoWorkedOnTheDeal={setWhoWorkedOnTheDeal}
          industry={industry}
          setIndustry={setIndustry}
          financialSector={financialSector}
          setFinancialSector={setFinancialSector}
          onNext={() => setActiveStep('NameOfDeal')}
        />
      );
      break;

    case 'NameOfDeal':
      activeView = (
        <NameOfDeal
          dealName={dealName}
          setDealName={setDealName}
          icon={icon}
          setIcon={setIcon}
          onNext={() => setActiveStep('Description')}
        />
      );
      break;

    case 'Description':
      activeView = (
        <Description
          description={description}
          setDescription={setDescription}
          onNext={() => setActiveStep('TotalValue')}
        />
      );
      break;

    case 'TotalValue':
      activeView = (
        <TotalValue
          totalValue={totalValue}
          setTotalValue={setTotalValue}
          onNext={() => setActiveStep('TotalCommission')}
        />
      );
      break;

    case 'TotalCommission':
      activeView = (
        <TotalCommission
          totalCommission={totalCommission}
          setTotalCommission={setTotalCommission}
          onNext={() => setActiveStep('CommissionType')}
        />
      );
      break;

    case 'CommissionType':
      activeView = (
        <CommissionType
          setCommissionType={setCommissionType}
          onNext={() => setActiveStep('SelectVault')}
        />
      );
      break;

    case 'SelectVault':
      activeView = (
        <SelectVault
          vault={selectedVault}
          setVault={setSelectedVault}
          onNext={() => setActiveStep('Summary')}
        />
      );
      break;

    case 'Summary':
      activeView = (
        <Summary
          dealName={dealName}
          commissionType={commissionType}
          totalValue={totalValue}
          totalCommission={totalCommission}
          description={description}
          onEdit={() => setActiveStep('NameOfDeal')}
          onNext={createDeal}
        />
      );
      break;

    case 'Loading':
      activeView = (
        <View style={styles.loadingContainer}>
          <LoadingAnimation />
        </View>
      );
      break;

    case 'Completed':
      activeView = <Completed dealName={dealName} />;
      break;

    default:
      activeView = (
        <DealTypes
          dealType={dealType}
          setDealType={setDealType}
          whoWorkedOnTheDeal={whoWorkedOnTheDeal}
          setWhoWorkedOnTheDeal={setWhoWorkedOnTheDeal}
          industry={industry}
          setIndustry={setIndustry}
          financialSector={financialSector}
          setFinancialSector={setFinancialSector}
          onNext={() => setActiveStep('NameOfDeal')}
        />
      );
  }

  return <View style={styles.container}>{activeView}</View>;
};

export default CreateBrokerage;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 30,
  },
  loadingContainer: {
    flex: 1,
    justifyContent: 'center',
  },
});
