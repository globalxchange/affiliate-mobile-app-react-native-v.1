import React, {useEffect, useRef, useState} from 'react';
import {
  Image,
  Keyboard,
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Animated, {
  Clock,
  interpolate,
  set,
  useCode,
} from 'react-native-reanimated';
import {WToast} from 'react-native-smart-tip';
import ReanimatedTimingHelper from '../../utils/ReanimatedTimingHelper';
import * as ImagePicker from 'expo-image-picker';

const NameOfDeal = ({dealName, setDealName, onNext, icon, setIcon}) => {
  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);
  const [keyboardHeight, setKeyboardHeight] = useState(0);

  const chatKeyboardAnimation = useRef(new Animated.Value(0));

  useEffect(() => {
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
      onKeyBoardShow,
    );
    Keyboard.addListener(
      Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
      onKeyBoardHide,
    );
    return () => {
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow',
        onKeyBoardShow,
      );
      Keyboard.removeListener(
        Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide',
        onKeyBoardHide,
      );
    };
  }, []);

  useCode(
    () =>
      isKeyboardOpen
        ? [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(0, 1, new Clock(), 300),
            ),
          ]
        : [
            set(
              chatKeyboardAnimation.current,
              ReanimatedTimingHelper(1, 0, new Clock(), 300),
            ),
          ],
    [isKeyboardOpen],
  );

  const onKeyBoardShow = (e) => {
    setKeyboardHeight(e.endCoordinates.height - 30);
    setIsKeyboardOpen(true);
  };

  const onKeyBoardHide = () => {
    setIsKeyboardOpen(false);
  };

  const onNextClick = () => {
    if (dealName) {
      onNext();
    } else {
      WToast.show({
        data: 'Please Enter A Deal Name',
        position: WToast.position.TOP,
      });
    }
  };

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [1, 1],
      quality: 0.85,
    });

    console.log(result);

    if (!result.cancelled) {
      setIcon(result.uri);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.container}>
        <Text style={styles.header}>Name The Deal</Text>
        <View style={styles.inputContainer}>
          <TouchableOpacity onPress={pickImage} style={styles.iconContainer}>
            <Image
              source={
                icon
                  ? {uri: icon}
                  : require('../../assets/upload-icon-white.png')
              }
              resizeMode="contain"
              style={styles.icon}
            />
          </TouchableOpacity>
          <TextInput
            value={dealName}
            onChangeText={(text) => setDealName(text)}
            style={styles.input}
            placeholder="Ex. Sale Of BMW 6 Series"
            placeholderTextColor="#848A96"
          />
          {dealName.trim() !== '' && (
            <TouchableOpacity onPress={onNextClick} style={styles.nextButton}>
              <Image
                source={require('../../assets/next-forward-icon.png')}
                style={styles.nextIcon}
                resizeMode="contain"
              />
            </TouchableOpacity>
          )}
        </View>
        {dealName.trim() !== '' && (
          <Text style={styles.desc}>You Can Always Change It Later</Text>
        )}
      </View>
      <Animated.View
        style={[
          {
            height: interpolate(chatKeyboardAnimation.current, {
              inputRange: [0, 1],
              outputRange: [0, keyboardHeight],
            }),
          },
        ]}
      />
    </View>
  );
};

export default NameOfDeal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  header: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 22,
  },
  inputContainer: {
    borderColor: '#DCDCDC',
    borderWidth: 1,
    marginTop: 25,
    height: 60,
    flexDirection: 'row',
  },
  input: {
    fontFamily: 'Montserrat',
    color: '#08152D',
    height: '100%',
    paddingHorizontal: 20,
    flex: 1,
  },
  nextButton: {
    backgroundColor: '#F1F4F6',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 22,
  },
  nextIcon: {
    width: 25,
    height: 25,
  },
  desc: {
    color: '#08152D',
    fontFamily: 'Montserrat',
    fontSize: 12,
    marginTop: 15,
  },
  iconContainer: {
    borderRightColor: '#DCDCDC',
    borderRightWidth: 1,
    width: 60,
    paddingHorizontal: 12,
  },
  icon: {
    flex: 1,
    height: null,
    width: null,
  },
});
