import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

const CommissionType = ({setCommissionType, onNext}) => {
  const onItemClick = (clickedItem) => {
    setCommissionType(clickedItem);
    onNext();
  };

  return (
    <View style={styles.container}>
      <Text style={styles.header}>What Is The Total Commission Percentage</Text>
      <View style={styles.buttonContainer}>
        <TouchableOpacity
          onPress={() => onItemClick('TokenBroker')}
          style={styles.itemButton}>
          <Text style={styles.itemButtonText}>TokenBroker</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => onItemClick('OTCBroker')}
          style={styles.itemButton}>
          <Text style={styles.itemButtonText}>OTCBroker</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default CommissionType;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  header: {
    color: '#08152D',
    fontFamily: 'Montserrat-Bold',
    fontSize: 22,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginTop: 20,
  },
  itemButton: {
    borderColor: '#DCDCDC',
    borderWidth: 1,
    height: 45,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
  },
  itemButtonText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    textAlign: 'center',
    fontSize: 12,
  },
});
