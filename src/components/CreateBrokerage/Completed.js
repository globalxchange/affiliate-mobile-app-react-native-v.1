import {useNavigation} from '@react-navigation/native';
import React, {useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';

const Completed = ({dealName}) => {
  const {goBack} = useNavigation();

  useEffect(() => {
    setTimeout(() => goBack(), 3000);
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.competedText}>
        Congrats {dealName} Has Been Added To The BrokerChain
      </Text>
    </View>
  );
};

export default Completed;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  competedText: {
    color: '#08152D',
    fontFamily: 'Montserrat-SemiBold',
    fontSize: 20,
    textAlign: 'center',
  },
});
