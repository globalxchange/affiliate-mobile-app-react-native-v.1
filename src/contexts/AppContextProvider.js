import React, {Component, createContext} from 'react';
import Axios from 'axios';
import {
  listCategories,
  GX_API_ENDPOINT,
  APP_CODE,
  NEW_CHAT_API,
} from '../configs';
import AsyncStorageHelper from '../utils/AsyncStorageHelper';
import ArrayMove from 'array-move';
import {isCrypto} from '../utils';

export const AppContext = createContext();

class AppContextProvider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userName: null,
      isLoggedIn: false,
      avatar: '',
      profileId: '',
      userEmail: '',
      cryptoTableData: null,
      activeListCategory: listCategories[0],
      isCountrySelectorActive: false,
      filterActiveCountry: null,
      countryList: [],
      checkOutData: {
        selectedCrypto: '',
        walletAddress: '',
      },
      isCustomNumPadOpen: false,
      customKeyboardCallback: null,
      withdrawAddress: '',
      walletShowInUsd: false,
      activeRoute: 'Home',
      walletBalances: null,
      homeSearchInput: '',
      totalPaymentMethods: '',
      pathData: [],
      blockCheckData: {
        processingCryptoDeposit: [],
        processingCryptoWithdraw: [],
        processingFaitDeposit: [],
        processingFaitWithdraw: [],
      },
      affiliateBalance: '',
      withdrawableAffiliateBalance: '',
      showBCHelper: false,
      fullName: '',
      isVideoFullScreen: false,
      isBlockSheetOpen: false,
      isBlockCheckSend: false,
      walletCoinData: '',
      isAdminLoggedIn: false,
      isInstaCryptoFeeEditorHide: false,
      chatAppId: '',
    };
  }

  componentDidMount() {
    this.getCryptoData();

    // Getting App data from Async Storage
    this.getAppDataFromAsyncStorage();
    this.getCountryListFromApi();
    this.getTotalPaymentMethods();
    this.updatePaths();
    this.getBlockCheckData();
    this.updateBrokerBalances();
    this.getWalletCoinData();
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.isLoggedIn !== this.state.isLoggedIn ||
      prevState.isAdminLoggedIn !== this.state.isAdminLoggedIn
    ) {
      if (this.state.isLoggedIn) {
        this.updateWalletBalances();
        this.getBlockCheckData();
        this.updateBrokerBalances();
        setTimeout(() => {
          this.getUserData();
          this.getWalletCoinData();
          this.getChatAppId();
        }, 1000);
      }
    }
  }

  getChatAppId = async () => {
    const email = await AsyncStorageHelper.getLoginEmail();
    const token = await AsyncStorageHelper.getAppToken();

    Axios.post(
      `${NEW_CHAT_API}/get_application`,
      {code: APP_CODE},
      {headers: {email, token}},
    )
      .then(({data}) => {
        // console.log('Data', data);

        this.setState({chatAppId: data?.payload?.id});
      })
      .catch((error) => {
        console.log('Error on getting application', error);
      });
  };

  getWalletCoinData = async () => {
    const profileId = await AsyncStorageHelper.getProfileId();

    Axios.post(`${GX_API_ENDPOINT}/coin/vault/service/coins/get`, {
      app_code: APP_CODE,
      profile_id: profileId,
    })
      .then((resp) => {
        const {data} = resp;

        // console.log('Sending Balance', data);

        if (data.status) {
          const balances = data.coins_data || [];

          this.setState({walletCoinData: balances});
        }
      })
      .catch((error) => {});
  };

  setIsLoggedIn = (isLoggedIn) => {
    this.setState({isLoggedIn});
  };

  getUserData = async () => {
    const email = await AsyncStorageHelper.getLoginEmail();

    Axios.get(`${GX_API_ENDPOINT}/user/details/get`, {
      params: {email},
    })
      .then((res) => {
        const {data} = res;

        // console.log('Data', data);

        AsyncStorageHelper.setUserFullName(data.user?.name || '');
        AsyncStorageHelper.setUserName(data.user.username);
        AsyncStorageHelper.setUserAvatarUrl(data.user.profile_img);
        AsyncStorageHelper.setAffId(data.user.affiliate_id);
        this.setLoginData(
          data.user.username,
          true,
          data.user.profile_img,
          data.user?.name || '',
          email,
        );
      })
      .catch((error) => {
        console.log('getUserDetails Error', error);
      });
  };

  getCryptoData = () => {
    this.setState({cryptoTableData: null});

    Axios.get(`${GX_API_ENDPOINT}/coin/vault/get/all/coins`)
      .then((resp) => {
        const {data} = resp;

        if (data.status) {
          const coinData = data.coins || [];
          const finalList = [];
          const coinSymbolArray = [];

          // Parsing Array as UI was designed
          coinData.forEach((item) => {
            if (!coinSymbolArray.includes(item.coinSymbol)) {
              coinSymbolArray.push(item.coinSymbol);
              finalList.push({
                ...item,
                name: item.coinName,
                image: item.coinImage,
                price: {
                  USD: item.usd_price,
                },
                asset_type: item.type === 'fiat' ? 'Fiat' : 'Crypto',
              });
            }
          });

          // Sorting Array as crypto comes first
          finalList.sort((a, b) =>
            a.asset_type > b.asset_type
              ? 1
              : b.asset_type > a.asset_type
              ? -1
              : 0,
          );

          // console.log('CoinData', finalList);

          this.setState({cryptoTableData: finalList});
        } else {
          this.setState({cryptoTableData: []});
        }
      })
      .catch((error) => console.log('Error getting Crypto table data', error));
  };

  updatePaths = () => {
    Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/payment/paths/get`).then(
      (resp) => {
        const {data} = resp;

        const pathData = data.paths || [];

        this.setState({pathData});
      },
    );
  };

  getCountryListFromApi = () => {
    Axios.get(
      'https://storeapi.apimachine.com/dynamic/InstaCryptoPurchase/Countrydem?key=a8a29286-f1a1-453a-9509-20decce27a1c',
    )
      .then((resp) => {
        const {data} = resp;
        if (data.success) {
          const list = data.data;

          const formattedList = [];

          formattedList.push({
            Key: 'Worldwide',
            productName: 'InstaCryptoPurchase',
            formName: 'Countrydem',
            formData: {
              Name: 'Worldwide',
              CountryCode: 124,
              Flag: 'https://i.ibb.co/ggYnSwr/world.png',
              Processor: ' Both',
            },
            value: 'Worldwide',
            name: 'Worldwide',
            image: 'https://i.ibb.co/ggYnSwr/world.png',
          });

          list.forEach((item) => {
            const formattedItem = {
              ...item,
              value: item.Key,
              name: item.Key,
              image: item.formData.Flag,
            };
            formattedList.push(formattedItem);
          });

          const usIndex = formattedList.findIndex(
            (x) => x.Key === 'United States',
          );

          if (usIndex >= 0) {
            ArrayMove.mutate(formattedList, usIndex, 1);
          }

          const canadaIndex = formattedList.findIndex(
            (x) => x.Key === 'Canada',
          );

          if (usIndex >= 0) {
            ArrayMove.mutate(formattedList, canadaIndex, 2);
          }

          const ukIndex = formattedList.findIndex(
            (x) => x.Key === 'United Kingdom',
          );

          if (usIndex >= 0) {
            ArrayMove.mutate(formattedList, ukIndex, 3);
          }

          const indiaIndex = formattedList.findIndex((x) => x.Key === 'India');

          if (usIndex >= 0) {
            ArrayMove.mutate(formattedList, indiaIndex, 4);
          }

          const {checkOutData} = this.state;

          this.setState({
            countryList: formattedList,
            filterActiveCountry: formattedList[0],
            checkOutData: {...checkOutData, selectedCountry: formattedList[1]},
          });
        }
      })
      .catch((error) => console.log('Error getting country list', error));
  };

  getTotalPaymentMethods = () => {
    Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/methods/coins/get`, {
      params: {app_code: APP_CODE},
    })
      .then((totalPathsResp) => {
        const {data} = totalPathsResp;

        const pathList = data.status ? data.paths : [];
        Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/payment/methods/get`, {
          params: {},
        })
          .then((resp) => {
            const methodsData = resp.data;
            const methods = methodsData.status ? methodsData.methods : [];

            let parsedMethods = [];

            methods.forEach((item) => {
              let noOfBankers = 0;
              pathList.map((pathItem) => {
                if (pathItem.depositMethod === item.code) {
                  noOfBankers += 1;
                }
              });
              parsedMethods.push({...item, noOfBankers, image: item.icon});
            });

            this.setState({totalPaymentMethods: parsedMethods});
          })
          .catch((error) => console.log('Error getting methods list', error));
      })
      .catch((error) => console.log('Error getting methods list', error));
  };

  getBlockCheckData = async () => {
    const {blockCheckData} = this.state;

    const email = await AsyncStorageHelper.getLoginEmail();

    Axios.get(`${GX_API_ENDPOINT}/coin/vault/service/processing/txns/get`, {
      params: {email, app_code: APP_CODE},
    })
      .then((resp) => {
        const {data} = resp;
        if (data.status) {
          // console.log('BlockCheck', data);

          const deposits = data.deposits || [];
          const withdrawals = data.withdraws || [];

          const processingCryptoDeposit = [];
          const processingCryptoWithdraw = [];
          const processingFaitDeposit = [];
          const processingFaitWithdraw = [];

          deposits.forEach((depositItem) => {
            depositItem.txns.forEach((nestedItem) => {
              if (isCrypto(nestedItem.coin)) {
                processingCryptoDeposit.push(nestedItem);
              } else {
                processingFaitDeposit.push(nestedItem);
              }
            });
          });

          withdrawals.forEach((withdrawItem) => {
            withdrawItem.txns.forEach((nestedItem) => {
              if (isCrypto(nestedItem.coin)) {
                processingCryptoWithdraw.push(nestedItem);
              } else {
                processingFaitWithdraw.push(nestedItem);
              }
            });
          });

          this.setState({
            blockCheckData: {
              processingCryptoDeposit,
              processingCryptoWithdraw,
              processingFaitDeposit,
              processingFaitWithdraw,
            },
          });
        }
      })
      .catch((error) => {
        console.log('Error on BlockCheck Data', error);
      });
  };

  updateBrokerBalances = async () => {
    const email = await AsyncStorageHelper.getLoginEmail();
    if (email) {
      Axios.get(`${GX_API_ENDPOINT}/coin/vault/gxb/balance`, {
        params: {email},
      })
        .then((resp) => {
          const {data} = resp;
          // console.log('updateBrokerBalances', data);

          this.setState({
            affiliateBalance: data.gxbroker_account_balance || 0,
            withdrawableAffiliateBalance: data.gxbroker_balance || 0,
          });
        })
        .catch((e) => {
          console.log(e);
        });
    }
  };

  setTotalPaymentMethod = (totalPaymentMethods) => {
    this.setState({totalPaymentMethods});
  };

  getAppDataFromAsyncStorage = async () => {
    const isLoggedIn = await AsyncStorageHelper.getIsLoggedIn();
    if (isLoggedIn) {
      this.setState({isLoggedIn});
    }

    const userName = await AsyncStorageHelper.getUserName();
    if (userName) {
      this.setState({userName});
    }

    const userEmail = await AsyncStorageHelper.getLoginEmail();
    if (userEmail) {
      this.setState({userEmail});
    }

    const avatar = await AsyncStorageHelper.getUserAvatarUrl();
    if (avatar) {
      this.setState({avatar});
    }

    const profileId = await AsyncStorageHelper.getProfileId();
    if (profileId) {
      this.setState({profileId});
    }

    const showBCHelper = await AsyncStorageHelper.getBCHelperStatus();

    this.setState({showBCHelper});

    const isAdminLoggedIn = await AsyncStorageHelper.getIsAdminView();

    this.setState({isAdminLoggedIn});
  };

  setLoginData = (userName, isLoggedIn, avatar, fullName, userEmail = '') => {
    this.setState({userName, isLoggedIn, avatar, fullName});

    if (userEmail) {
      this.setState({userEmail});
    }
  };

  removeLoginData = () => {
    this.setState({
      userName: '',
      isLoggedIn: false,
      avatar: '',
      walletBalances: null,
    });
  };

  setActiveListCategory = (activeListCategory) => {
    this.setState({activeListCategory});
  };

  filterCryptoList = (query) => {
    const {cryptoTableData} = this.state;
    const queryString = query.toLowerCase();

    if (cryptoTableData) {
      const list = cryptoTableData.filter(
        (item) =>
          item.coinName.toLowerCase().includes(queryString) ||
          item.coinSymbol.toLowerCase().includes(queryString),
      );
      this.setState({filteredList: list});
    }
  };

  setFilterActiveCountry = (filterActiveCountry) => {
    this.setState({filterActiveCountry, isCountrySelectorActive: false});
  };

  toggleCountrySelector = () => {
    const {isCountrySelectorActive} = this.state;
    this.setState({isCountrySelectorActive: !isCountrySelectorActive});
  };

  setSelectedCrypto = (selectedCrypto) => {
    const {checkOutData} = this.state;

    this.setState({checkOutData: {...checkOutData, selectedCrypto}});
  };

  resetCheckoutData = () => {
    this.setState({
      checkOutData: {
        selectedCrypto: '',
        walletAddress: '',
      },
    });
  };

  setWalletAddress = (walletAddress) => {
    const {checkOutData} = this.state;

    this.setState({checkOutData: {...checkOutData, walletAddress}});
  };

  setIsNumPadOpen = (isCustomNumPadOpen) => {
    this.setState({isCustomNumPadOpen});
  };

  setCustomKeyBoardCallBack = (customKeyboardCallback) => {
    this.setState({customKeyboardCallback});
  };

  setWithdrawAddress = (withdrawAddress) => {
    this.setState({withdrawAddress});
  };

  setWalletShowInUsd = (walletShowInUsd) => {
    this.setState({walletShowInUsd});
  };

  setActiveRoute = (activeRoute) => {
    this.setState({activeRoute});
  };

  setWalletBalances = (walletBalances) => {
    this.setState({walletBalances});
  };

  updateWalletBalances = async () => {
    const profileId = await AsyncStorageHelper.getProfileId();

    const postData = {
      app_code: APP_CODE,
      profile_id: profileId,
    };

    // console.log('postData', postData);

    Axios.post(`${GX_API_ENDPOINT}/coin/vault/service/balances/get`, postData)
      .then((resp) => {
        const {data} = resp;

        // console.log('WalletData', data);

        if (data.status) {
          this.setWalletBalances(data.vault);
        }
      })
      .catch((error) => console.log('Error on getting Wallet Balance', error));
  };

  setHomeSearchInput = (homeSearchInput) => {
    this.setState({homeSearchInput});
  };

  setProfileId = (profileId) => {
    this.setState({profileId});
  };

  setBCHelperStatus = () => {
    const {showBCHelper} = this.state;

    this.setState({showBCHelper: !showBCHelper});
    AsyncStorageHelper.setBCHelperStatus(!showBCHelper);
  };

  setIsVideoFullScreen = (isVideoFullScreen) => {
    this.setState({isVideoFullScreen});
  };

  setIsBlockSheetOpen = (isBlockSheetOpen) => {
    this.setState({isBlockSheetOpen});
  };

  setIsBlockCheckSend = (isBlockCheckSend) => {
    this.setState({isBlockCheckSend});
  };

  forceRefreshApiData = () => {
    this.getCryptoData();

    // Getting App data from Async Storage
    this.getAppDataFromAsyncStorage();
    this.getCountryListFromApi();
    this.getTotalPaymentMethods();
    this.updatePaths();
    this.getBlockCheckData();
    this.updateBrokerBalances();
    this.updateWalletBalances();
    this.getWalletCoinData();
    this.forceUpdate();
  };

  setIsInstaCryptoFeeEditorHide = (isInstaCryptoFeeEditorHide) => {
    this.setState({isInstaCryptoFeeEditorHide});
  };

  render() {
    const {children} = this.props;

    return (
      <AppContext.Provider
        value={{
          ...this.state,
          setIsLoggedIn: this.setIsLoggedIn,
          setActiveListCategory: this.setActiveListCategory,
          filterCryptoList: this.filterCryptoList,
          setLoginData: this.setLoginData,
          removeLoginData: this.removeLoginData,
          toggleCountrySelector: this.toggleCountrySelector,
          setFilterActiveCountry: this.setFilterActiveCountry,
          setSelectedCrypto: this.setSelectedCrypto,
          resetCheckoutData: this.resetCheckoutData,
          setWalletAddress: this.setWalletAddress,
          setIsNumPadOpen: this.setIsNumPadOpen,
          setCustomKeyBoardCallBack: this.setCustomKeyBoardCallBack,
          setWithdrawAddress: this.setWithdrawAddress,
          setWalletShowInUsd: this.setWalletShowInUsd,
          setActiveRoute: this.setActiveRoute,
          setWalletBalances: this.setWalletBalances,
          updateWalletBalances: this.updateWalletBalances,
          setHomeSearchInput: this.setHomeSearchInput,
          setProfileId: this.setProfileId,
          updatePaths: this.updatePaths,
          getCryptoData: this.getCryptoData,
          setTotalPaymentMethod: this.setTotalPaymentMethod,
          getBlockCheckData: this.getBlockCheckData,
          updateBrokerBalances: this.updateBrokerBalances,
          setBCHelperStatus: this.setBCHelperStatus,
          setIsVideoFullScreen: this.setIsVideoFullScreen,
          setIsBlockSheetOpen: this.setIsBlockSheetOpen,
          forceRefreshApiData: this.forceRefreshApiData,
          setIsBlockCheckSend: this.setIsBlockCheckSend,
          setIsInstaCryptoFeeEditorHide: this.setIsInstaCryptoFeeEditorHide,
        }}>
        {children}
      </AppContext.Provider>
    );
  }
}

export default AppContextProvider;
